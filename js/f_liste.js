$(function() {
	$('#list .window').hide();

	bind_buttons();

	$('#list .close_button').click(function() {
		$('#list .window').hide();

		// Aktiviere Screen
		$('#overlay').hide();
		unlockScroll();
	})

	$('div.antwort').click(function(evt) {
		if ((evt.target.type === 'checkbox') || (evt.target.type === 'radio')) {
			evt.stopPropagation();
			return;
		} // if

		if ($(this).find(':checkbox').length > 0) {
			$checkbox = $(this).find(':checkbox');

			$checkbox.prop('checked', !$checkbox.prop('checked'));
			evt.stopPropagation();
			return false;
		} // if

		if ($(this).find(':radio').length > 0) {
			$radio = $(this).find(':radio');

			$('input[name="antwort_id"][type="radio"]', this).prop('checked', true);

			evt.stopPropagation();
			return false;
		} // if
	});

	if ($(".chosen-select").length ) {
		$(".chosen-select").chosen({allow_single_deselect : true});
		
		show_print_selection();
		show_print_selection_statistics();
	} // if

	$(".list_over tr").click(function(evt) {
		var str = evt.target.tagName;

		if ((str == "A") || (str == "DIV") || (str == "SPAN") || (evt.target.type === 'checkbox') || (evt.target.type === 'radio')) {
			evt.stopPropagation();
			return;
		} // if

		var checkbox = $(this).find("input[type='checkbox']");

		checkbox.each(function() {
			if ($(this).attr('name').substr(0, 2) == "id")
				$(this).prop('checked', !checkbox.prop('checked'));
		});

		evt.stopPropagation();
		return false;
	});

	// Fragebogen Eingabe: Klick in TD Checked Radio Button
	$(".list_top td.eingabe").click(function(evt) {
		var radio = $(this).find("input[type='radio']");
		radio.prop('checked', true);
	});

	// DatePicker
	$('.date_picker').datepicker({
		dateFormat : 'dd.mm.yy',
		dayNamesShort : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
		dayNamesMin : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
		monthNames : [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember' ],
		monthNamesShort : [ 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez' ],
		dayNames : [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ],
		prevText: "",
		nextText: "",
    firstDay: 1
	});

	$(document).tooltip({
		content : function() {
			var element = $(this);
			return element.attr("title");
		}
	});

	if ($('#indiv_popup').length) {
		$('#balken').hide();
		$('#search_info').hide();
		$('#user_menu').hide();
		$('.search_form').hide();
		$('nav').hide();
		$('.menu_form').css({
			'margin-right' : '0px'
		});

		$('#logo').hide();
		$('.menu_standard').hide();
		$('.menu_small').hide();
		$('#content').css({
			'margin-left' : '0px'
		});
	} // if
}) // document ready

function getScrollBarWidth () {
    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
        widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
}

function bind_buttons() {
	// Neu
	$('#new').click(function() {
		show_window(0, 1, 'edit=true');
	});

	// ändern
	$('.change').click(function() {
		$edit = $(this).attr('data-id');

		show_window($edit, 1, 'edit=true');
	});
	$('.change_other_list').click(function() {
		$edit = $(this).attr('data-id');
		$list_id = $(this).attr('data-list_id');

		show_window($edit, 1, 'edit=true', $list_id);
	});

	// Antworten kopieren von
	$('.answer_copy_from').click(function() {
		$dest_id = $(this).attr('data-id');

		show_window($dest_id, 2, 'answer_copy_from=true');
	});

	// Fragen kopieren von
	$('.questions_copy_from').click(function() {
		$dest_id = $(this).attr('data-id');

		show_window($dest_id, 3, 'questions_copy_from=true');
	});

	// Antworten kopieren nach
	$('.answer_copy_to').click(function() {
		$dest_id = $(this).attr('data-id');

		show_window($dest_id, 4, 'answer_copy_to=true');
	});

	$('.zu_kurs_zuordnen').click(function() {
		$teilnehmer_id = $(this).attr('data-id');

		show_window($teilnehmer_id, 5, 'zu_kurs_zuordnen=true');
	});

	$('.ams_mail_beginn').click(function() {
		$teilnehmer_id = $(this).attr('data-id');

		show_window($teilnehmer_id, 6, 'ams_mail_beginn=true');
	});

	$('.ams_mail_ende').click(function() {
		$teilnehmer_id = $(this).attr('data-id');

		show_window($teilnehmer_id, 8, 'ams_mail_ende=true');
	});

	$('.kurs_trainer_zuordnen').click(function() {
		$kurs_id = $(this).attr('data-id');

		show_window($kurs_id, 9, 'kurs_trainer_zuordnen=true');
	});

	$('.send_email_process').click(function() {
		show_window(0, 7, 'send_email=true');
	});

	$('.run_query').click(function() {
		$id = $(this).attr('data-id');
		$table = $(this).attr('data-table');

		$.post('/' + SUBDIR + 'classes/list_ajax_extend.php?run_query=true', {
			id : $id,
			table : $table,
			action : "run_query"
		}, function(data) {
			$output = 'Return: ' + data;

			$('#list .content').html($output);

			// Positionieren + Size
			$max = ($(window).height() - 80);
			$('#list .window').css({
				'height' : 'auto',
				'width' : 'auto',
				'max-height' : $max,
				'max-width' : 800
			});

			$top = 20;
			$left = $('#box1').position().left + 20;
			$('#list .window').css({
				'top' : $top,
				'left' : $left
			});

			// Anzeigen
			$('#list .window').show();
			$('#list .mail_div').hide();
			$('#list .window').draggable({
				start : function(event) {
					var content = $(this).children('.content');

					if (event.originalEvent.pageX - content.offset().left > content.innerWidth()) {
						$(this).trigger("mouseup");
						return false;
					} // if
				}
			});
		}, // function
		'');
	});

	$('#bas_settings_kurse').change(function() {
		var optionSelected = $(this).find("option:selected");
		var valueSelected = optionSelected.val();
		var textSelected = optionSelected.text();

		var href = $('#bas_settings_create_button').attr("href");
		var myarray = href.split('&');

		href = myarray[0] + '&kurs_id=' + valueSelected;

		$('#bas_settings_create_button').attr("href", href);
	});
}

function show_window($edit, $action, $action_string, $list_id=-1) {
	$bNewDialog = true;
	if ($action == 7)
		$bNewDialog = false;

	$bShowWindow = true;
	if( $list_id == -1 ) $list_id = $('#current_list').val(); 
		
	// Content laden
	$.post('/' + SUBDIR + 'classes/list_ajax.php?' + $action_string, {
		current_list : $list_id,
		action : $action,
		param : $edit
	}, function(data) {
		$('#list .content').html(data.resultData).ready(function() {
			// DateTimePicker
			$('#list .content .datetime_picker').datetimepicker({
				timeFormat : 'HH:mm',
				dateFormat : 'dd.mm.yy'
			});

			// DatePicker
			$('#list .content .date_picker').datepicker({
				dateFormat : 'dd.mm.yy',
				dayNamesShort : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
				dayNamesMin : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
				monthNames : [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember' ],
				monthNamesShort : [ 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez' ],
				dayNames : [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ],
				prevText: "",
				nextText: "",
		    firstDay: 1
			});

			// TimePicker
			$('#list .content .time_picker').timepicker({
				timeFormat : 'HH:mm',
				timeOnlyTitle : 'Zeit Wählen',
				timeText : 'Zeit',
				hourText : 'Stunde',
				minuteText : 'Minute',
				secondText : 'Sekunde',
				currentText : 'Jetzt',
				closeText : 'Fertig'
			});
		});

		// Textarea parsen
		$('#list .content textarea').each(function(index, element) {
			$html = $(this).html();
			$html = $html.replace(/&lt;br&gt;/g, "\n");
			$(this).html($html);
		});

		$('.fine-uploader').fineUploader({
			template : "qq-template",
			paramsInBody : true,
			request : {
				endpoint : '../classes/fine-uploader/endpoint.php'
			},
			deleteFile : {
				enabled : true,
				method : "POST",
				endpoint : '../classes/fine-uploader/endpoint.php'
			}
		}).on("complete", function(event, id, name, response, xhtml) {
			if (response.success) {
				$field_id = $('.field_id', $(this).parent()).val();

				$(this).parent().append('<input type="hidden" class="__image_new" name="__image_new[' + $field_id + '][' + id + ']" value="' + response.uuid + '/' + response.uploadName + '">');
			} else
				alert(response.error);
		});

		// Öffnen Button
		$('#list .open_button_div').hide();
		$('#list .select_rechnung').change(function() {
			$id = $(this).find('option:selected').attr('data-document_id');
			if ($id !== undefined) {
				$('#list .open_button_div').show();
				$('#list .open_button_div a').attr("href", "bas_drucken.php?open=1&id=" + $id);
			} else
				$('#list .open_button_div').hide();
		});

		// Geb Datum Button
		$('#button_svn').click(function() {
			create_geb_datum();
		});
		if( (typeof basilica_bind_buttons !== 'undefined') && (typeof basilica_bind_buttons === 'function') )
			basilica_bind_buttons();

		// Save Button
		form = $('#list .content form');
		$('#save').click(function() {
			postData = {};

			// Felder
			$('.img_attention').remove();
			$bError = false;
			form.find(':input').each(function() {
				if (this.type == 'checkbox') {
					$fieldval = 0;
					if ($(this).prop('checked'))
						$fieldval = 1;
				} else
					$fieldval = $(this).val();

				$name = $(this).attr('name');
				if (($name !== undefined) && ($name != "qqfile"))
					postData[$name] = $fieldval;

				if (($name !== undefined) && ($name.indexOf("[") == -1)) {
					$('input[name=' + $name + ']').css({
						'border' : '1px solid #aaa'
					});
					$mandatory = $(this).attr('data-mandatory');
					if (($mandatory == 1) && ($fieldval == "")) {
						$bError = true;
						$('input[name=' + $name + ']').css({
							'border' : '2px solid red'
						}).after('<span class="img_attention">&nbsp;<img src="/' + SUBDIR + 'themes/_default/icons/info_red.png" width="16px"></span>');
					} // if
				} // if
			});

			postData['current_list'] = $list_id;
			if ($('#tabs').val() !== undefined)
				postData['current_tab'] = $('#tabs').tabs('option', 'active');
			postData['action'] = $action;

			if (!$bError) {
				$.post('/' + SUBDIR + 'classes/list_ajax.php?save=true', postData, function(data) {
					// Reloaden
					if (data.reload_file !== undefined) {
						$('.window').dialog('open').dialog('widget').effect( {effect: 'fold', duration: 500, complete: function() { location.href = "/" + SUBDIR + "admin/" + data.reload_file; } } );
						
						//location.href = "/" + SUBDIR + "admin/" + data.reload_file;
						return;
					} else {
						$('#list .window').hide();

						// Messagebox
						if (data.message !== undefined)
							alert(data.message);
					}
					
					if ($bNewDialog)
						$('.window').dialog("close");

					// Aktiviere
					// Screen
					$('#overlay').hide();
					unlockScroll();
				}, 'json').fail(function(xhr, textStatus, errorThrown) {
					$str = xhr.responseText;

					$str2 = $str.split("\n");
					for ($i = 0; $i < $str2.length; $i++) {
						$iPos = $str2[$i].indexOf("[error]");
						if ($iPos > 0)
							alert($str2[$i]);
					} // for
				});
			} // if
		});
	}, 'json').done(function() {
		if ($action == 7) {
			run_email_save_ids();

			if (email_ids.length > 0)
				run_email_send();
			else
				$bShowWindow = false;
		} // if
	}).done(
			function() {
				if ($bShowWindow == true) {
					// Positionieren + Size
					$max = ($(window).height() - 80);
					$('#list .window').css({
						'height' : 'auto',
						'width' : 'auto'
					});

					$top = (self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop) + 20;
					$left = $('#box1').position().left + 20;

					// Anzeigen
					if ($bNewDialog) {
						$('#tabs').tabs();
						$('#list .window').dialog({
							width : 1024,
							// height: 800,
							// minHeight: 800,
							maxHeight : 800,
							zIndex: 998,
							title : '',
							modal: true,
							resizable: false,
							position: { my: "center", at: "center", of: window },
							show: {effect: 'fade', duration: 1000},
							hide: {effect: 'fold', duration: 500},
							dialogClass : 'list',
							open : function(event, ui) {
								$('#balken_overlay').hide();
								
								// Overlay Color
					      $(".ui-widget-overlay").css({
					        "opacity": "0.4",
					        "background": "#000000"
					      });
							},
							close : function(event, ui) {
								$(this).dialog("destroy");
								unlockScroll();
							},
							beforeClose : function(e, ui) {
								if (e.originalEvent && e.originalEvent.originalEvent && e.originalEvent.originalEvent.type == "click") {
									// Remove Temp
									// Pictures
									remove_temp_pictures();
								} // if
							}
						}).addClass("my_window").dialog('open');
						$('.my_window').dialog('widget').attr('id', 'popup_dialog');
						$('.my_window').dialog('widget').addClass("popup_dialog_class");
						$('.my_window').dialog('widget').addClass("popup_dialog_action_"+$action);

						$('.my_window .chosen-select').chosen({allow_single_deselect : true});
						
//						$("#popup_dialog").hide();
//						$("#popup_dialog").position({ my: "center", at: "center", of: window });
//						$("#popup_dialog").show();

						
						// Bugfix, weil manche Chosen Container width ==
						// 0
						$('.my_window .chosen-container').filter(function() {
							return $(this).width() == 0;
						}).each(function($index, $value) {
							$(this).css({
								width : "200"
							});
						});

						$('table.kal .chosen-container').css({
							width : "200"
						});
						
						$('.my_window .tiny').tinymce(
								{
									height: "150",
									menubar : false,
									statusbar : false,
									forced_root_block : false,
									language : "de",
									plugins : [ "advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen",
											"insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor" ],
									toolbar1 : "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code | forecolor backcolor emoticons",
									image_advtab : true,
									toolbar_items_size : 'small',
								});
						
						// DatePicker
						$('.my_window .date_picker').datepicker({
							dateFormat : 'dd.mm.yy',
							dayNamesShort : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
							dayNamesMin : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
							monthNames : [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember' ],
							monthNamesShort : [ 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez' ],
							dayNames : [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ],
							prevText: "",
							nextText: "",
					    firstDay: 1
						});
						
						// TimePicker
						$('.my_window .time_picker').timepicker({
							timeFormat : 'HH:mm',
							timeOnlyTitle : 'Zeit Wählen',
							timeText : 'Zeit',
							hourText : 'Stunde',
							minuteText : 'Minute',
							secondText : 'Sekunde',
							currentText : 'Jetzt',
							closeText : 'Fertig'
						});		
						
						$('.datetime_picker').datetimepicker({
							timeFormat : 'HH:mm',
							dateFormat : 'dd.mm.yy',
							dayNamesShort : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
							dayNamesMin : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
							monthNames : [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember' ],
							monthNamesShort : [ 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez' ],
							dayNames : [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ],
							prevText: "",
							nextText: "",
					    firstDay: 1,
							timeText : 'Zeit',
							hourText : 'Stunde',
							minuteText : 'Minute',
							secondText : 'Sekunde',
							currentText : 'Jetzt',
							closeText : 'Fertig'
						});
						
					} else {
						$('#list .window').css({
							'top' : $top,
							'left' : $left,
							'z-index' : 998
						});

						$('#list .window').show();
						$('#list .mail_div').hide();
						$('#list .window').draggable({
							start : function(event) {
								var content = $(this).children('.content');

								if (event.originalEvent.pageX - content.offset().left > content.innerWidth()) {
									$(this).trigger("mouseup");
									return false;
								} // if
							}
						});

						if ($('#list .window .chosen-select').size()) {
							var config = {
								allow_single_deselect : true
							}

							$('#list .window .chosen-select').chosen(config);
						} // if

						// Deaktiviere Screen
						$('#overlay').show().css({
							'top' : (self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop),
							'height' : 5000
						});
						lockScroll();
					} // if
				} // else
			});
} // show_window

var email_id_pointer;
var email_ids = new Array();
;

function run_email_save_ids() {
	// Reset
	email_ids = [];
	email_id_pointer = 0;

	$(".list_over input[type='checkbox']").each(function() {
		if ($(this).prop('checked') == true) {
			if ($(this).attr('data-id') !== undefined) {
				var z = $(this).attr('data-id') * 1;

				email_ids[email_id_pointer] = z;
				email_id_pointer++;
				$(this).prop('checked', true);
			} // if
		} // if
	});
	email_id_pointer = 0;

	$('.close_button').hide();
} // run_email_save_ids

function run_email_send() {
	$('#overlay').show();

	postData = {};
	postData['action'] = "get_email_data";
	postData['email_id'] = email_ids[email_id_pointer];
	postData['email_current_pointer'] = email_id_pointer;
	postData['email_max'] = email_ids.length;

	$.post('/' + SUBDIR + 'classes/list_ajax_extend.php?get_email_data=true', postData, function(data) {
		$('#list .window #email_id').html(data.email_id);
		$('#list .window #email_an').html(data.email_an);
		$('#list .window #email_status').html(data.email_status);

		postData = {};
		postData['action'] = "process_email";
		postData['email_id'] = data.email_id;

		$.post('/' + SUBDIR + 'classes/list_ajax_extend.php?process_email=true', postData, function(data) {
			email_id_pointer++;
			if (email_id_pointer < email_ids.length)
				run_email_send();
			else {
				$('.close_button').show();
				$('#overlay').hide();

				// Reload
				if (data.reload_file !== undefined) {
					location.href = "/" + SUBDIR + "admin/" + data.reload_file;
				} // if
			} // else
		}, 'json')
	}, 'json');
} // run_email_send

function delete_record($t, $i) {
//	$t.closest('form').append('<input type="hidden" value="1" name="del[' + $i + ']">');
//	$t.closest('form').submit();
	$('#print_list').append('<input type="hidden" value="1" name="del[' + $i + ']">');
	$('#print_list').submit();	
}

function show_print_selection() {
	$v = $('#type').val();
	$('#teilnehmer').hide();
	$('#kurs').hide();
	$('#trainer').hide();
	$('#fragebogen').hide();

	$('#bericht_teilnehmer').hide();
	$('#bericht_kurs').hide();
	$('#bericht_teilnehmer_kurs').hide();
	$('#bericht_trainer').hide();
	$('#bericht_trainer_kurs').hide();
	$('#bericht_fragebogen_kurs').hide();

	switch ($v) {
	case 'teilnehmer':
		$('#teilnehmer').show();
		$('#bericht_teilnehmer').show();
		break;
	case 'kurs':
		$('#kurs').show();
		$('#bericht_kurs').show();
		break;
	case 'teilnehmer_kurs':
		$('#teilnehmer').show();
		$('#kurs').show();
		$('#bericht_teilnehmer_kurs').show();
		break;
	case 'trainer':
		$('#trainer').show();
		$('#bericht_trainer').show();
		break;
	case 'trainer_kurs':
		$('#trainer').show();
		$('#kurs').show();
		$('#bericht_trainer_kurs').show();
		break;
	case 'fragebogen_kurs':
		$('#fragebogen').show();
		$('#kurs').show();
		$('#bericht_fragebogen_kurs').show();
		break;
	} // if
}

function show_print_selection_statistics() {
	$v = $('#statistic_id').val();

	$('#stat_herkunft').hide();
	$('#stat_teilnehmer_kurse').hide();

	switch ($v) {
	case '1':
		$('#stat_herkunft').show();
		break; // Herkunft der Teilnehmer
	case '2':
		$('#stat_teilnehmer_kurse').show();
		break; // Teilnehmer Kurse
	} // switch
} // show_print_selection_statistics

function RunExe(path) {
	try {
		var ua = navigator.userAgent.toLowerCase();
		if (ua.indexOf("msie") != -1) {
			MyObject = new ActiveXObject("WScript.Shell")
			MyObject.Run("file:///C:/Program%20Files/Microsoft%20Office/Office14/winword.exe " + path);
		} else {
			netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");

			var exe = window.Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsILocalFile);
			exe.initWithPath(path);
			var run = window.Components.classes['@mozilla.org/process/util;1'].createInstance(Components.interfaces.nsIProcess);
			run.init(exe);
			var parameters = [ "" ];
			run.run(false, parameters, parameters.length);
		}
	} catch (ex) {
		alert(ex.toString());
	}
}

function open_document($path) {
	RunExe($path);
} // open_document

function print_module($where, $which) {
	switch ($which) {
	case 1:
		$where.append('<input type="hidden" name="search" value="1">');
		break;
	case 2:
		$where.append('<input type="hidden" name="create" value="1">');
		break;
	case 3:
		$where.append('<input type="hidden" name="reset" value="1">');
		break;
	case 4:
		$where.append('<input type="hidden" name="batch_print" value="1">');
		break;
	case 5:
		$where.append('<input type="hidden" name="send_email" value="1">');
		break;
	} // switch

	$where.closest('form').submit();
} // print_module

function check_rows() {
	uncheck_rows();

	$(".list_over input[type='checkbox']").each(function() {
		var z = $(this).attr('data-printed') * 1;

		if (z == 0)
			$(this).prop('checked', true);
	});
}

function uncheck_rows() {
	$(".list_over input[type='checkbox']").each(function() {
		$(this).prop('checked', false);
	});
}

function print_batch($path) {
	var shell = new ActiveXObject('WScript.Shell');

	shell.Run($path);
} // print_batch

function lockScroll() {
	return;
	/*
	 * $html = $('html'); $div = $('#content_scroll'); $body = $('body'); var
	 * initWidth = $body.outerWidth(); var initHeight = $body.outerHeight();
	 * 
	 * var scrollPosition = [ self.pageXOffset ||
	 * document.documentElement.scrollLeft || document.body.scrollLeft ||
	 * document.getElementById("content_scroll").scrollLeft, self.pageYOffset ||
	 * document.documentElement.scrollTop || document.body.scrollTop ||
	 * document.getElementById("content_scroll").scrollTop, document.body.scrollTo ];
	 * //console.log( scrollPosition );
	 * 
	 * $html.data('scroll-position', scrollPosition);
	 * $html.data('previous-overflow', $html.css('overflow'));
	 * $html.css('overflow', 'hidden'); // window.scrollTo(scrollPosition[0],
	 * scrollPosition[1]);
	 * 
	 * var marginR = $body.outerWidth()-initWidth; var marginB =
	 * $body.outerHeight()-initHeight; // $body.css({'margin-right':
	 * marginR,'margin-bottom': marginB});
	 */
}

function unlockScroll() {
	return;
	/*
	 * 
	 * $html = $('html'); $body = $('body'); $html.css('overflow',
	 * $html.data('previous-overflow')); var scrollPosition =
	 * $html.data('scroll-position'); window.scrollTo(scrollPosition[0],
	 * scrollPosition[1]); $('#content_scroll').scrollTo(scrollPosition[0],
	 * scrollPosition[1]); //console.log( scrollPosition ); //
	 * $body.css({'margin-right': 0, 'margin-bottom': 0});
	 */
}

function create_geb_datum() {
	$svn = $('input[name=svn]').val();
	$geb_dat = $('input[name=geb_datum]');

	$day = $svn.substr($svn.length - 6, 2);
	$month = $svn.substr($svn.length - 4, 2);
	$year = '19' + $svn.substr($svn.length - 2, 2);

	$geb_dat.val($day + '.' + $month + '.' + $year);
} // create_geb_datum

function print_window(div_id) {
	/*
	 * w=window.open( '', 'Drucken',
	 * 'location=0,status=1,scrollbars=1,width=400,height=600' );
	 * w.document.write('<!DOCTYPE html><html lang="de"><head><link
	 * rel="stylesheet" type="text/css" href="/css/s.php"><link rel="stylesheet"
	 * type="text/css" media="print" href="/css/s.php"></head><body
	 * style="overflow:auto !important;"><div id="content_scroll">');
	 * w.document.write($('#'+div_id).html()); w.document.write('</div></body></html>');
	 * 
	 * 
	 * w.print(); w.close();
	 */
	var DocumentContainer = document.getElementById(div_id); // DocumentContainer.innerHTML
	
	$document_print = $('#'+div_id).clone();

	// Spalte Aktionen entfernen
	$('.list_th_columns_actions', $document_print).remove();
	$('.list_columns_actions', $document_print).remove();
	// Spalte Auswahl entfernen
	$('.list_th_columns_choose', $document_print).remove();
	$('.list_columns_choose', $document_print).remove();
	// Links in Text umwandeln
	$('a', $document_print).each( function (index, value) {		
		$text = $(this).text();
		if( $text == "auflösen" ) {
//			console.log( $text);
			$text = "";
			$(this).parent().html( $(this).parent().html().replace('(','').replace(')','').replace('auflösen','') );
		} // if 
		$(this).replaceWith( $text );
	});
	// Title entfernen
	$('*', $document_print).attr( "title", "" ).removeClass( "bg_dreieck" );
	// Pagination entfern
	$('#paginator', $document_print).remove();
	
	
	
	
	var html = '<html><head>' + '<link href="' + '/' + SUBDIR + 'css/s.php" rel="stylesheet" type="text/css" />' + '</head><body style="overflow:auto;height:auto;">' + $document_print.html() + '</body></html>';

	var top = window.top.outerHeight / 2 + window.top.screenY - ( 800 / 2)
	var left = window.top.outerWidth / 2 + window.top.screenX - ( 1200 / 2)
	var WindowObject = window.open("", "Drucken", "location=no,status=no,scrollbars=yes,toolbar=no,width=1200,height=800,top="+top+",left="+left);
	WindowObject.document.writeln(html);
	WindowObject.document.close();
	if (WindowObject.document.getElementById("mbmcpebul_table") !== null)
		WindowObject.document.getElementById("mbmcpebul_table").style.display = "none";

	WindowObject.focus();
	// WindowObject.print();
	// WindowObject.close();

} // print_windows

function list_new_window($strURL) {
	window.open($strURL, "", 'width=1024,height=800');
} // list_new_window

function delete_picture($picture_id) {
	$current = $('#picture_' + $picture_id + ' a img').css("opacity");
	if ($current == 1) {
		$new = 0.2;
		$('#__image_current_delete_' + $picture_id).val(1);
	} else {
		$new = 1;
		$('#__image_current_delete_' + $picture_id).val(0);
	} // else

	$('#picture_' + $picture_id + ' a img').css("opacity", $new);
} // delete_picture

function remove_temp_pictures() {
	$('.__image_new').each(function() {
		$.post('/' + SUBDIR + 'classes/list_ajax_extend.php?delete_temp_picture=true', {
			path : $(this).val(),
			action : "delete_temp_picture"
		}, function(data) {
		})
	});
} // remove_temp_pictures

function print_edit_list() {
	var $html = '';

	if ($('#tabs_table').html() !== undefined) {
		$html = $('#tabs_table').html();
	} else {
		$iTab = 0;
		$('#tabs ul li a').each(function() {
			$iTab++;
			$html = $html + $(this).html() + '<hr>' + $('#tab\\[' + $iTab + '\\]').html() + '<br>';
		});
	} // else

	var myWindow = window.open("", t('Drucken'), "location=no,status=no,scrollbars=yes,toolbar=no,width=800,height=600");

	var html = '<html><head>' + '<link href="/css/s.php" rel="stylesheet" type="text/css" />' + '</head><body style="overflow:auto;height:auto;">' + $html + '</body></html>';

  $(myWindow.document.body).html($html);
	myWindow.focus();
	// myWindow.print();
} // print_order
