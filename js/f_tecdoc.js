$(function() {
	// Tab Events
	$("#search_tabs").tabs({
    activate: function (event, ui) {
            $act = $("#search_tabs").tabs("option", "active");
            
            if( $act == "3" ) {
        			// Enable Tree View
        			$('#search_categories_kat').jstree({
        				"plugins" : [ "sort", "search" ]
        			});
        			$('#search_categories_kat').on('select_node.jstree', function (e, data) {
        			    data.instance.toggle_node(data.node);
        			});			
            	$('#search_categories_kat_div').show();
            } else 
            	$('#search_categories_kat_div').hide();
            
            $('#active_search').val( $act );
            $('#shortcut_list_hider').hide();
			      $('.banner-center').show();
			      
            $('#search_shortcuts_categories').hide();
            $('#search_articles').hide();
            $('#search_articles_infos_hider').hide();
            
            $('#search_shortcuts').empty();   
            $('#search_categories').empty();
            
            $('#search_articles_table tr td').closest('tr').remove();
            $('#search_articles_big').empty();
            
         // Cursor Input setzen
	    	switch( $('#active_search').val() ) {
					case "0": $("#search_pattern").focus(); break;
					case "1": ; break;
					case "2": $("#search_motor_code").focus(); break;
					case "3": $("#search_infos").focus(); $('#search_articles_infos_hider').show(); break;
					case "4": $("#search_fin").focus(); break;
					case "5": $("#search_fin_audatex").focus(); break;
					default: break;
	    	} // switch
        }
    });

	// Suche starten mit Return
	$("#search_tabs input, #search_tabs select").keydown( function( event ) {
	    if(event.which == 13) {
	    	switch( $('#active_search').val() ) {
				case "0": fill_articles(); break;
				case "1": fill_shortcuts(); fill_categories(0); break;
				case "2": fill_motor_vehicles(); break;
				case "3": fill_infos( -1, 1 ); break;
				case "4": fill_fin_vehicles(); break;
				case "5": open_3d_search(); break;
			default: break;
		} // switch
	    }
	});
	
	// Activate Field search_pattern
	$("#search_pattern").focus();
	
	$("#search_pattern").autocomplete({
	  source: '#list_autocomplete',
	  showButton: true
	});

	$("#search_fin_audatex").autocomplete({
		  source: '#list_autocomplete_audatex',
		  showButton: true
	});
	
	// Do Search -> automatische Suche (neues Fenster)
	if( $('#doSearch').val() == 1 )
		fill_articles();
	
	// Do Search für neues Fahrzeug
	if( $('#doSearch').val() == 2 ) {
		$( "#search_tabs" ).tabs( "option", "active", 1 );
		
		// Parallel ausschalten
		$.ajaxSetup( {async: false} );
		
		fill_model();
		$('#search_model').val( $('#modelId').val() ).trigger("chosen:updated");
		fill_vehicle();
		$('#search_vehicle').val( $('#vehicleId').val() ).trigger("chosen:updated");

		// Parallel einschalten
		$.ajaxSetup( {async: true} );

		fill_shortcuts(); 
		fill_categories(0);
	} // if
	
	// Chosen Breite
	$("#search_by_customerno").outerWidth(400);
	
	// Excel Export
  $("#export_excel").click( function () {
    $("#table_excel_export").excelexportjs({
        containerid: "table_excel_export" , 
        datatype: 'table'
    });
  });
  
  // Vorladen von Übersetzungen
  preload_translations();

  // nur Wenn Project = TecDoc
  if( $('#project_id').val() == "4" ) { 
		// Update Orders in Menu
		update_orders();
  } // if
  
	// Globales Errorhandling
	$(document).ajaxError( function( event, jqxhr, settings, exception ) {
		$('#ajax_loader').hide(); // Loader Picture
		
		$error = jqxhr.responseText;
		$iPos = $error.indexOf( '{"' );
		if( $iPos > 0 )
			$error = $error.substr( 0, $iPos );
		$error = $error.replace( /\n/g, '' ).replace( /\r/g, '' ).replace( /<br \/><br \/>/g, "<br \/>" ).replace( /<br><br>/g, "<br \/>" );
		 
		if( $('#show_ajax_error').val() == 1 )
			show_popup( '<div><div class="p-3 mb-2 bg-danger text-white">ERROR</div>'+$error+'</div>', 0, 900 );
  });
	
	$('.datetime_picker').datetimepicker({
		timeFormat : 'HH:mm',
		dateFormat : 'dd.mm.yy',
		/*dayNamesShort : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
		dayNamesMin : [ 'So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa' ],
		monthNames : [ 'Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember' ],
		monthNamesShort : [ 'Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez' ],
		dayNames : [ 'Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag' ],*/		
		prevText: "",
		nextText: "",
    firstDay: 1
  });
}) // document ready

function t( $str ) {
	if( _ENABLE_TRANSLATION == 0 )
		return( $str );
	if( cache_get( $str ) !== null ) 
		return( cache_get( $str ) );

	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?translate=true',
		{ action: "translate", 'input': $str },
		function( data ) {
			cache_set( $str, data.resultData );
		},
		'json'
	);  

	return( '' );
} // t

function update_orders() {
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_orders=true',
		{ action: "load_orders"	},
		function( data ) {
			$('.shopping_count').hide();
			$('.shopping_count').html( data.resultData.shopping_count );
			if( data.resultData.shopping_count > 0 )
				$('.shopping_count').show();
			
			$('.order_count').hide();
			$('.order_count').html( data.resultData.order_count);
			if( data.resultData.order_count > 0 )
				$('.order_count').show();
		},
		'json'
	);
} // update_orders

function fill_model() {
	// Clear Select
	$('#search_model').find('option').remove();
	$('#filter_fueltype').find('option').remove();
	$('#filter_pskw').find('option').remove();
	$('#filter_motorcode').find('option').remove();
	$('#search_vehicle').find('option').remove();

	// disable while loading
	$('#search_model').prop( "disabled", true ).trigger("chosen:updated");
	$('#filter_fueltype').prop( "disabled", true ).trigger("chosen:updated");
	$('#filter_pskw').prop( "disabled", true ).trigger("chosen:updated");
	$('#filter_motorcode').prop( "disabled", true ).trigger("chosen:updated");
	$('#search_vehicle').prop( "disabled", true ).trigger("chosen:updated");
	
	$manufacturer = $('#search_manufacturer').val();
	$from = $('#year_from').val();
	$to = $('#year_to').val();

	// Ajax Call
	return $.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_model=true',
		{ action: "load_model", manufacturer: $manufacturer, from: $from, to: $to	},
		function( data ) {
			$.each(data.resultData, function (index, value) {
			    $('#search_model').append($('<option/>', {
			        value: value.id,
			        text : value.name
			    }));
			});
			
			// enable after loading
			$('#search_model').prop( "disabled", false ).trigger("chosen:updated");
			$('#filter_fueltype').prop( "disabled", false ).trigger("chosen:updated");
			$('#filter_pskw').prop( "disabled", false ).trigger("chosen:updated");
			$('#filter_motorcode').prop( "disabled", false ).trigger("chosen:updated");
			$('#search_vehicle').prop( "disabled", false ).trigger("chosen:updated");
		},
		'json'
	);
} // fill_model

function fill_vehicle_reset_filter() {
	$('#filter_fueltype').val( "" );
	$('#filter_pskw').val( "" );
	$('#filter_motorcode').val( "" );
} // fill_vehicle_reset_filter

function fill_vehicle() {
	// Store Select Values
	$sel_fueltype = $('#filter_fueltype').val();
	$sel_pskw = $('#filter_pskw').val();
	$sel_motorcode = $('#filter_motorcode').val();
	if( $sel_fueltype == ' ' ) $sel_fueltype = null;
	if( $sel_pskw == ' ' ) $sel_pskw = null;
	if( $sel_motorcode == ' ' ) $sel_motorcode = null;
	
	// Clear Select
	$('#search_vehicle').find('option').remove();
	$('#filter_fueltype').find('option').remove();
	$('#filter_pskw').find('option').remove();
	$('#filter_motorcode').find('option').remove();
	
	// disable while loading
	$('#filter_fueltype').prop( "disabled", true ).trigger("chosen:updated");
	$('#filter_pskw').prop( "disabled", true ).trigger("chosen:updated");
	$('#filter_motorcode').prop( "disabled", true ).trigger("chosen:updated");
	$('#search_vehicle').prop( "disabled", true ).trigger("chosen:updated");
	
	$manufacturer = $('#search_manufacturer').val();
	$model = $('#search_model').val();

	// Ajax Call
	return $.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_vehicle=true',
		{ action: "load_vehicle", manufacturer: $manufacturer, model: $model, filter_fueltype: $sel_fueltype, filter_pskw: $sel_pskw, filter_motorcode: $sel_motorcode },
		function( data ) {
			$.each(data.resultData, function (index, value) {
				if( value.show == true )
			    $('#search_vehicle').append($('<option/>', {
		        value: value.id,
		        text : value.name
			    }));
		    
		    // Fill Fueltype
		    var exists = 
		    	$("#filter_fueltype option")
	        .filter(function (i, o) { return o.value === value.fueltype; })
	        .length > 0;
        if( !exists )
			  	$('#filter_fueltype').append($('<option/>', {
		        value: value.fueltype,
		        text : value.fueltype
			    }));

		    // Fill PS/KW
		    var exists = 
		    	$("#filter_pskw option")
	        .filter(function (i, o) { return o.value === value.pskw; })
	        .length > 0;
        if( !exists )
			  	$('#filter_pskw').append($('<option/>', {
		        value: value.pskw,
		        text : value.pskw
			    }));
        
		    // Fill Motorcode
        $.each( value.motorcode, function (index2, value2) {
  		    var exists = 
  		    	$("#filter_motorcode option")
  	        .filter(function (i, o) { return o.value === value2; })
  	        .length > 0;
          if( !exists )
	  		  	$('#filter_motorcode').append($('<option/>', {
	  	        value: value2,
	  	        text : value2
	  		    }));
        })     
			});
			
			// Sort
      sortSelect('#filter_fueltype', 'text', 'asc');
      sortSelect('#filter_pskw', 'text', 'asc');
      sortSelect('#filter_motorcode', 'text', 'asc');
      
  		// Restore Select Values
  		$('#filter_fueltype').val( $sel_fueltype ).trigger("chosen:updated");
  		$('#filter_pskw').val( $sel_pskw ).trigger("chosen:updated");
  		$('#filter_motorcode').val( $sel_motorcode ).trigger("chosen:updated");
  		
  		// enable after loading
  		$('#filter_fueltype').prop( "disabled", false ).trigger("chosen:updated");
  		$('#filter_pskw').prop( "disabled", false ).trigger("chosen:updated");
  		$('#filter_motorcode').prop( "disabled", false ).trigger("chosen:updated");
  		$('#search_vehicle').prop( "disabled", false ).trigger("chosen:updated");
		},
		'json'
	);  
} // fill_vehicle

function fill_motor_vehicles() {
	check_session();
	
	// Clear Select
	$('#search_motor_vehicle').find('option').remove();
	
	// disable while loading
	$('#search_motor_vehicle').prop( "disabled", true ).trigger("chosen:updated");
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_vehicle_motor=true',
		{ 
			action: "load_motor_vehicle", 
			motor_code: $('#search_motor_code').val()
		},
		function( data ) {
			$.each(data.resultData, function (index, value) {
			    $('#search_motor_vehicle').append( '<option value="'+value.id+'" data-manufacturer="'+value.manufacturer+'" data-model="'+value.model+'">'+value.name+'</option>' );
			});
		},
		'json'
	).done( function() {
		// enable after loading
		$('#search_motor_vehicle').prop( "disabled", false ).trigger("chosen:updated");
	});
} // fill_motor_vehicles

function fill_fin_vehicles() {
	check_session();
	
	$('#ajax_loader').show(); // Loader Picture	

	// Clear Select
	$('#search_fin_vehicle').find('option').remove();
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_vehicle_fin=true',
		{ 
			action: "load_fin_vehicle", 
			fin: $('#search_fin').val()
		},
		function( data ) {
			$.each(data.resultData, function (index, value) {
			    $('#search_fin_vehicle').append( '<option value="'+value.id+'" data-manufacturer="'+value.manufacturer+'" data-model="'+value.model+'">'+value.name+'</option>' );
			});
		},
		'json'
	).done( function() {
		$('#ajax_loader').hide(); // Loader Picture		
	});
} // fill_fin_vehicles

function get_vehicle() {
	switch( $('#active_search').val() ) {
		case "1": $vehicle = $('#search_vehicle').val(); break;
		case "2": $vehicle = $('#search_motor_vehicle').val(); break;
		case "3": $vehicle = 0; break;
		case "4": $vehicle = $('#search_fin_vehicle').val(); break;
		case "5": $vehicle = 0; break;
		default: $vehicle = ""; break;
	} // switch
	
	return( $vehicle );
} // get_vehicle

function get_manufacturer() {
	switch( $('#active_search').val() ) {
		case "1": $manufacturer = $('#search_manufacturer').val(); break;
		case "2": $manufacturer = $('#search_motor_vehicle').find(':selected').attr('data-manufacturer'); break;
		case "4": $manufacturer = $('#search_fin_vehicle').find(':selected').attr('data-manufacturer'); break;
		default: $manufacturer = ""; break;
	} // switch

	return( $manufacturer );
} // get_manufacturer

function get_model() {
	switch( $('#active_search').val() ) {
		case "1": $model = $('#search_model').val(); break;
		case "2": $model = $('#search_motor_vehicle').find(':selected').attr('data-model'); break;
		case "4": $model = $('#search_fin_vehicle').find(':selected').attr('data-model'); break;
		default: $model = ""; break;
	} // switch
	
	return( $model );
} // get_model();

function get_brand() {
	switch( $('#active_search').val() ) {
		default: $brand = ""; break;
	} // switch

	return( $brand );
} // get_brand

function get_pattern() {
	switch( $('#active_search').val() ) {
		case "0":
				fix_input( '#search_pattern' );
				$pattern = $('#search_pattern').val(); 
			break; 
		case "5": // Audatex
				$pattern = $('#active_tree_category').val();
			break;
		default: $pattern = ""; break;
	} // switch

	return( $pattern );
} // get_pattern

function get_pattern_type() {
	switch( $('#active_search').val() ) {
		case "0": $pattern_type = $('#search_pattern_type').val(); break; 
		case "5": $pattern_type = "1"; break; // Audatex
		default: $pattern_type = ""; break;
	} // switch

	return( $pattern_type );
} // get_pattern

function get_category() {
	switch( $('#active_search').val() ) {
		case "1": $category = $('#active_tree_category').val(); break; 
		case "2": $category = $('#active_tree_category').val(); break; 
		case "4": $category = $('#active_tree_category').val(); break;
		default: $category = ""; break;
	} // switch

	return( $category );
} // get_category

function get_similar() {
	$similar = "";
	switch( $('#active_search').val() ) {
		case "0": if( $('#search_pattern_similar').is(":checked") ) $similar = 1; break;
		default: $similar = ""; break;
	} // switch

	return( $similar );
} // get_similar

function fix_input( $field ) {
	$value = $.trim( $( $field ).val() );
	
	$( $field ).val( $value )
} // fix_input

function set_tree_category( $category, $description = '' ) {
	$('#active_tree_category').val( $category );
	$('#active_tree_category_description').val( $description );
} // set_tree_category

var $save_id = -1;
function create_element( $arr, $current_index ) {
	var $current_arr;
	var $element;
	
	$current_arr = $arr[$current_index];
	$element = $('#sub_'+$current_arr.parent);
	if( !$element.length ) { // Parent nicht gefunden
		if( $current_arr.parent == 0 ) { // Root anlegen
			$('#search_categories_div').append( '<ul id="sub_0"></ul>' );
		} else 
			create_element( $arr, $current_arr.parent );
	} 
	$element = $('#sub_'+$current_arr.parent);

	if( $('#element_'+$current_arr.id).length ) // Bereits da
		return;
	
	if( $current_arr.group ) {
		// Neue Gruppe
		$style = "";
		if( ($current_arr.name == "Zweirad") || ($current_arr.name == "Dvotockas") )
			$save_id = $current_arr.id;
		
		$current_arr.name = $current_arr.name.replace( /\/-/g, '/' ).replace( /\//g, ' / ' );
		$element.append( '<li id="element_'+$current_arr.id+'">'+$current_arr.name+'<ul id="sub_'+$current_arr.id+'"></ul></li>' );
	} else {
		// Neuer Link
		$current_arr.name = $current_arr.name.replace( /\/-/g, '/' ).replace( /\//g, ' / ' );
		$element.append( '<li id="element_'+$current_arr.id+'" data-jstree=\'{"icon":"/'+SUBDIR+'themes/_default/icons/search_16_tecdoc.png"}\'><a onClick="set_tree_category('+$current_arr.id+'); fill_articles();"><span style="word-break: break-all;text-decoration: none;">'+$current_arr.name+'</span></a></li>' );
//		$element.append( '<li id="element_'+$current_arr.id+'" data-jstree=\'{"icon":"/'+SUBDIR+'themes/_default/icons/search_16_tecdoc.png"}\'><a onClick="set_tree_category('+$current_arr.id+'); fill_articles();">'+$current_arr.name+'</a></li>' );		
	} // else
} // create_element

function fill_categories( $shortcut ) {
	$('#shortcut_list').find('div').removeClass( "active" );
	if( $shortcut != 0 ) {
		$('#shortcut_'+$shortcut).addClass( "active" );
	} // if
	
	$('#ajax_loader').show(); // Loader Picture
	
	$('#categories_title').html( t( 'Kategorien' ) );
	$('#search_shortcuts_categories').show();

	// Clear List
	$('#search_categories').empty();
	$('#search_categories').append( '<input type="text" id="search_categories_filter" style="margin-left: 11px; width: 90%; border: solid 1px #aaa;" placeholder="   <Filter>">' );
	$('#search_categories').append( '<div id="search_categories_div"></div>' );
	
	$vehicle = get_vehicle();

	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_categories=true',
		{ action: "load_categories", vehicle: $vehicle, shortcut: $shortcut },
		function( data ) {
			$save_id = -1;
			
			// Schleife Ordner anlegen
			$.each(data.resultData, function (index, value) {
				create_element( data.resultData, index );
			});
			
			// Enable Tree View
			$('#search_categories_div').jstree({
				"plugins" : [ "sort", "search" ],
			});
			$('#search_categories_div').on('select_node.jstree', function (e, data) {
			    data.instance.toggle_node(data.node);
			});
			
			var to = false;
		  $('#search_categories_filter').keyup(function () {
		    if(to) { clearTimeout(to); }
		    to = setTimeout(function () {
		      var v = $('#search_categories_filter').val();
		      $('#search_categories_div').jstree(true).search(v);
		    }, 250);
		  });
		  
			$('#element_'+$save_id).hide();
		},
		'json'
	).done( function() {
		$('#ajax_loader').hide(); // Loader Picture
	});
} // fill_categories

function fill_shortcuts() {
	check_session();
	
	$('#ajax_loader').show(); // Loader Picture
	
	$('#shortcut_list_hider').show();	
	$('#search_shortcuts').show();
	$('.banner-center').hide(); 

	// Clear List
	$('#search_shortcuts').empty();
	$('#search_shortcuts').removeClass();
	$('#shortcut_list').empty();

	// New Root Group
	$('#search_shortcuts').append( '<ul>' );
	
	$vehicle = get_vehicle();

	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_shortcuts=true',
		{ action: "load_shortcuts", vehicle: $vehicle },
		function( data ) {
			$.each(data.resultData, function (index, value) {
				$element = $('#search_shortcuts ul');
				$element.append( '<li id="shortcut_'+index+'"><a href="#" onClick="fill_categories('+index+');">'+value.name+'</a></li>' ); // +'
				
				$element = $('#shortcut_list'); 
				$element.append( '<div class="shortcut-container moj-flex-fill"><div id="shortcut_'+index+'" class="shortcut" title="'+value.name+'"><a href="#" onClick="fill_categories('+index+');"><img src="'+value.pic+'"></a></div></div>' );
			});

			$('#shortcut_list').tooltip({
	      position: {
	        my: "left top",
	        at: "left top-32",
	      },				
			});
			
			// Enable Tree View
			$('#search_shortcuts').jstree();
		},
		'json'
	).done( function() {
		$('#ajax_loader').hide(); // Loader Picture
	});
} // fill_shortcuts

function fill_articles() {
	check_session();
	
	// Hide iFrame
	$('#show_iframe').hide();
	
	// Show Loading Picture
	$('#search_articles').show();

	$('#ajax_loader').show(); // Loader Picture
	
	// Clear List
	$('#search_articles_table tr td').closest('tr').remove();
	$('#search_articles_big').empty();
	var table = $('#search_articles_table').stupidtable();
    var th = table.find("th");
    th.find(".arrow").remove();
    th.eq(6).append('<span class="arrow"><i class="fas fa-angle-down"></i></span>');

	$vehicle = get_vehicle();
	$manufacturer = get_manufacturer(); 
	$model = get_model();
	$brand = get_brand();
	$pattern = get_pattern();
	$pattern_type = get_pattern_type();
	$category = get_category();
	$similar = get_similar();
	
	if( $pattern != "" )
		add_search_pattern( $pattern );
	
	$param = {};
	$param['action'] = 'load_articles';
	if( $vehicle !== "" ) $param['vehicle'] = $vehicle;
	if( $manufacturer !== "" ) $param['manufacturer'] = $manufacturer;
	if( $model !== "" ) $param['model'] = $model;
	if( $brand !== "" ) $param['brand'] = $brand;
	if( $pattern !== "" ) $param['pattern'] = $pattern;
	if( $pattern_type !== "" ) $param['pattern_type'] = $pattern_type;
	if( $category !== "" ) $param['category'] = $category;
	if( $similar !== "" ) $param['similar'] = $similar;
	if( $('#search_by_customerno').length && ($('#search_by_customerno').val() != "") ) $param['customer_no'] = $('#search_by_customerno').val();
	if( $('#search_show_attributes').is(":checked") ) $param['search_show_attributes'] = 1; else $param['search_show_attributes'] = 0;
	if( $('#search_only_on_stock').is(":checked") ) $param['search_only_on_stock'] = 1; else $param['search_only_on_stock'] = 0;
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_articles=true',
		$param,
		function( data ) {
			$.each(data.resultData, function (index, value) {
				$amount = ConvertPriceToView( ConvertPriceToDB(value.price) * ConvertPriceToDB(value.in_order));
			  	$price = ConvertPriceToView( ConvertPriceToDB(value.price) );
        
			  	$c = "";
			  	$picture = "";
			  	if( (ConvertPriceToDB( value.stock ) >= 0.1) )
			  		$c = value.ampel;
			  	if( value.picture != "" )
			  		$picture = '<img src="'+value.picture+'" style="max-height: 50px; max-width: 50px;" title="'+escapeHtml( '<img src="'+value.picture+'" style="width: 250px;">' )+'">';
			  
			  	if( $manufacturer == "" ) $manufacturer = 0;
			  	if( $model == "" ) $model = 0;
			  	if( $vehicle == "" ) $vehicle = 0;
			  	
			  	$menge = '';
			  	$menge_big = '';
			  	$preis = '';
			  	$gesamt = ''; 
			  	$aktion = '';
			  	$preis_title = '';
			  	if( (ConvertPriceToDB( value.stock ) >= 0.1) || (ConvertPriceToDB( value.price ) >= 0.1) ) {
				  	$menge = '<input id="quantity_'+value.id+'_'+value.replace_id+'" name="quantity_'+value.id+'_'+value.replace_id+'" value="'+value.in_order+'" class="spinner_quantity" style="width: 30px;"/>';
				  	$menge_big = '<input id="quantity_big_'+value.id+'_'+value.replace_id+'" name="quantity_big_'+value.id+'_'+value.replace_id+'" value="'+value.in_order+'" class="spinner_quantity_big" style="width: 30px;"/>';
				  	$preis = $price;
				  	$gesamt = $amount; 
				  	$aktion = '<a href="#" onClick="buy_article('+value.link_id+','+value.id+','+$manufacturer+','+$model+','+ConvertPriceToDB(value.stock)+',\''+value.no+'\',\''+value.name+'\','+value.replace_id+');">'+value.button_buy+'</a>';
				  	$preis_title = 'title="'+value.title_price+'"';
			  	} // if
			  		
			  	$info = "&nbsp;";
			  	if( value.text2 != "" ) {
			  		$info = '<i class="fas fa-info-circle text-primary" title="'+value.text2+'"></i>';
			  	} // if

			  $location_id = '';
			  $location_id_big = '';
			  if( $('#enable_location_id').length ) {
			  	if( $menge != '' ) {
			  		// list
				  	$location_id = '<td><select class="mytec form-control form-control-sm chosen-select" id="location_'+value.id+'_'+value.replace_id+'"><option value="-1" data-quantity="'+ConvertPriceToDB(value.stock)+'" data-price="'+ConvertPriceToDB(value.price)+'" data-title="'+value.title_price+'">&nbsp;</option>';
				  	$.each(value.locations, function (index2, value2) {
				  		$location_id = $location_id + '<option value="'+value2.id+'" data-quantity="'+value2.quantity+'" data-price="'+value2.netto+'" data-title="'+value2.title_price+'" data-sub_shop="'+value2.sub_shop+'">'+value2.name+' ('+ConvertPriceToView(value2.netto)+')</option>';
				  	});
				  	$location_id = $location_id + '</select></td>';
				  	
				  	// card
				  	$location_id_big = '<select class="mytec form-control form-control-sm chosen-select" id="location_big_'+value.id+'_'+value.replace_id+'"><option value="-1" data-quantity="'+ConvertPriceToDB(value.stock)+'" data-price="'+ConvertPriceToDB(value.price)+'" data-title="'+value.title_price+'">&nbsp;</option>';
				  	$.each(value.locations, function (index2, value2) {
				  		$location_id_big = $location_id_big + '<option value="'+value2.id+'" data-quantity="'+value2.quantity+'" data-price="'+value2.netto+'" data-title="'+value2.title_price+'" data-sub_shop="'+value2.sub_shop+'">'+value2.name+' ('+ConvertPriceToView(value2.netto)+')</option>';
				  	});
				  	$location_id_big = $location_id_big + '</select>';
					}  else {
						$location_id = '<td></td>';
					} // else
			  } // if
			  
			  $internal_no = '';
			  if( value.internal_no != '' )
			  	$internal_no = '<br>' + value.internal_no;

			  $search_button = '';
			  $search_button = '<a href="#" onClick="search_for_infos(\''+value.no+'\');" class="link_click_button"><div class="very_small_button"><i class="fas fa-link"></i></div></a>';
			  
				// Ansicht Liste
				$element = $('#search_articles_table');
				$element.append(
					'<tr class="article-row" id="article_'+value.id+'_'+value.replace_id+'">'+
						'<td>'+$picture+'</td>'+
						'<td class="top-aligned"><a href="#" onClick="open_article_popup('+value.link_id+','+value.id+','+$manufacturer+','+$model+','+$vehicle+',\''+value.brand+'\','+value.replace_id+');"><strong><nobr>'+value.no+'</nobr></strong></a><nobr>'+$internal_no+'</nobr></br>'+$search_button+'</td>'+
						'<td class="top-aligned" id="brand_'+value.id+'_'+value.replace_id+'" data-brand_id="'+value.brand_id+'">'+value.brand+'</td>'+
						'<td class="top-aligned-8">'+$info+'</td>'+
						'<td style="border-left: none;">'+value.name+value.name_internal+value.bemerkung+'</td>'+
						'<td>'+value.position+'</td>'+
						'<td class="right top-aligned round-icon"><div style="text-align: left; float: left;">'+$c+'</div><div style="text-align: right; float: right;">'+value.stock_text+'</div></td>'+
						$location_id+
						'<td>'+$menge+'</td>'+
						'<td class="right top-aligned" id="price_'+value.id+'_'+value.replace_id+'" '+$preis_title+' style="font-weight: bold;">'+$preis+'</td>'+
						'<td style="display: none;" class="right" id="amount_'+value.id+'_'+value.replace_id+'">'+$gesamt+'</td>'+
						'<td class="right">'+$aktion+'</td>'+
					'</tr>' );

				// Ansicht Karte
				$picture = "";
				if( value.picture != "" )
					$picture = '<img src="'+value.picture+'" class="articles_big_img">';
				$c = '';
		  	if( (ConvertPriceToDB( value.stock ) >= 0.1) )
		  		$c = value.ampel;
				
				$element = $('#search_articles_big');
				$element.append(
					'<div class="col-12 col-sm-12 col-md-4 col-lg-3 new-article-card">'+
						'<div class="new-article-card-inner">'+
							'<div class="row">'+
								'<div class="col-5 col-sm-5 col-md-12 new-article-card-img-conrainer">'+
									'<a classhref="#" onClick="open_article_popup('+value.link_id+','+value.id+','+$manufacturer+','+$model+','+$vehicle+',\''+value.brand+'\','+value.replace_id+');">'+
										$picture+
									'</a>'+
								'</div>'+
								'<div class="col-7 col-sm-7 col-md-12 new-article-card-info">'+
									'<div style="padding: 0px 5px;">'+
										'<div class="new-article-card-description">'+$c+
											'<a href="#" onClick="open_article_popup('+value.link_id+','+value.id+','+$manufacturer+','+$model+','+$vehicle+',\''+value.brand+'\','+value.replace_id+');">'+
												'<strong>'+value.brand+' '+value.no+'</strong><br />'+
												value.name+
											'</a>'+
										'</div>'+
										'<div style="margin-bottom: 10px;">'+$location_id_big+
										'</div>'+
									'</div>'+
									'<div class="row">'+
										'<div class="no-pad col-5 new-article-price-container">'+
											'<div class="new-article-price" style="font-weight: bold;" id="price_big_'+value.id+'_'+value.replace_id+'" '+$preis_title+'>'+$preis+'</div>'+
										'</div>'+
										'<div class="no-pad col-7">'+
											'<div class="new-article-price-cart">'+
												'<div class="new-article-price-spinner">'+$menge_big+'</div>'+
												'<div class="new-article-price-button">'+$aktion+'</div>'+
										'</div>'+	
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'
				);
				
				// Cross Update List/Card
				$('#location_'+value.id+'_'+value.replace_id).change( function() {
					$id = $(this).attr('id').substr( 9 );
					$('#location_big_'+$id).val( $('#location_'+$id).val() );
					
					$decNewPrice = 0;
					$decNewPrice = $('option:selected', this).attr('data-price');
					$strNewTitle = $('option:selected', this).attr('data-title');
					
					$('#price_'+$id).html( ConvertPriceToView( $decNewPrice ) );
					$('#price_'+$id).attr('title', $strNewTitle );
					$('#price_big_'+$id).html( ConvertPriceToView( $decNewPrice ) );
					$('#price_big_'+$id).attr('title', $strNewTitle );
				});
				$('#location_big_'+value.id+'_'+value.replace_id).change( function() {
					$id = $(this).attr('id').substr( 13 );
					$('#location_'+$id).val( $('#location_big_'+$id).val() );
					
					$decNewPrice = 0;
					$decNewPrice = $('option:selected', this).attr('data-price');
					$strNewTitle = $('option:selected', this).attr('data-title');
					
					$('#price_'+$id).html( ConvertPriceToView( $decNewPrice ) );
					$('#price_'+$id).attr('title', $strNewTitle );
					$('#price_big_'+$id).html( ConvertPriceToView( $decNewPrice ) );
					$('#price_big_'+$id).attr('title', $strNewTitle );
				});
			});

			// Enable Spinner
			$('.spinner_quantity').spinner(
				{min: 0,
					spin: function(event, ui) { $(this).val(ui.value); $(this).change(); }
				}
			);
			$('.spinner_quantity').change(function() {
				$id = $(this).attr('id').substr( 9 );
				$price = ConvertPriceToDB( $('#price_'+$id).html() );
				$quantity = ConvertPriceToDB( $('#quantity_'+$id).val() );
				$amount = $price * $quantity;

				$('#amount_'+$id).html( ConvertPriceToView( $amount ) );
				$('#quantity_big_'+$id).val( $('#quantity_'+$id).val() );
			});
			
			$('.spinner_quantity_big').spinner(
					{min: 0,
						spin: function(event, ui) { $(this).val(ui.value); $(this).change(); }
					}
				);
			$('.spinner_quantity_big').change(function() {
				$id = $(this).attr('id').substr( 13 );
				$price = ConvertPriceToDB( $('#price_'+$id).html() );
				$quantity = ConvertPriceToDB( $('#quantity_big_'+$id).val() );
				$amount = $price * $quantity;

				$('#amount_'+$id).html( ConvertPriceToView( $amount ) );
				
				$('#quantity_'+$id).val( $('#quantity_big_'+$id).val() );
			});

			// Table Sort
			var table = $('#search_articles_table').stupidtable();
			table.on("aftertablesort", function (event, data) {
		          var th = $(this).find("th");
		          th.find(".arrow").remove();
		          var dir = $.fn.stupidtable.dir;
		          var arrow = data.direction === dir.ASC ? "fas fa-angle-up" : "fas fa-angle-down";
		          th.eq(data.column).append('<span class="arrow"><i class="'+ arrow +'"></i></span>');
		        });
			
			// kein Ergebnis
			if( data.resultData == "" ) {
				$element = $('#search_articles_table');
				$element.append(
					'<tr>'+
						'<td colspan="10" style="text-align: center;">'+t('kein Ergebnis gefunden')+'</td>'+
					'</tr>' );
				
				$element = $('#search_articles_big');
				$element.append(
					'<div class="articles_big shadow" style="text-align: center;">'+
						t('kein Ergebnis gefunden')+
					'</div>'
				);
			}
			
			$view_port = $(window).outerWidth();
			if( $view_port < 992 ) 
				show_articles_type(1); // Card View
			else 
				show_articles_type(0); // List View
			
			// Jump to Articles
			if( $('#active_search').val() == 1 )
				window.location.href = "#my_top";
		},
		'json'
	).done( function() {
		$('#ajax_loader').hide(); // Loader Picture
	});
} // fill_articles

function ConvertPriceToDB( $parStr ) {
	$str = $parStr+'';
  $str = $str.replace( '.', '' );
  $str = $str.replace( ',', '.' );
  $dec = parseFloat( $str );
  
  return( $dec );
} // ConvertPriceToDB

function ConvertPriceToView( $dec ) {
// $str = number_format($dec, 2, ",", "" );
	
  $dec = Math.round( $dec * 100 ) / 100;

  $str = $dec.toString();
  $str = $str.replace( ".", "," );

  if( $str.search( ',' ) == -1 )
    $str = $str + ",00";
  $decimals = $str.length - $str.search( ',' ) - 1;
  if( $decimals == 1 ) 
    $str = $str + "0";  

  return( $str );
} // ConvertPriceToView

function buy_article( $artikel_link_id, $artikel_id, $manufacturer, $model, $stock, $no, $name, $replace_id ) {
	$vehicle = get_vehicle();
	
	$quantity = $('#quantity_'+$artikel_id+'_'+$replace_id).val();
	if( $quantity == 0 ) 
		return;

	if( $('#enable_location_id').length ) {
		$blocked_locations = $('#enable_location_id').attr( 'data-blocked_locations' );
		$arrLocations = $blocked_locations.split( "," );
		
		$location = $('#location_'+$artikel_id+'_'+$replace_id).val();
		$location_name = $('#location_'+$artikel_id+'_'+$replace_id+' option:selected').text();

		if( $('.need_location').length && ($location < 0) ) {
			show_popup( t('Sie müssen einen Lagerort auswählen.') );
			return;
		} // if

		if( $arrLocations.indexOf( $location ) != -1 ) {
			show_popup( t('Bestellungen für diesen Lagerort können nicht über Warenkorb durchgeführt werden. Bitte nehmen Sie telefonisch Kontakt mit uns auf. Lagerort: ') + $location );
			
			return;
		} // if
		
		// Lagerort ausgewählt neuer max. Lagerstand
		$stock = $('#location_'+$artikel_id+'_'+$replace_id+' option:selected').attr( "data-quantity" );
		$sub_shop = $('#location_'+$artikel_id+'_'+$replace_id+' option:selected').attr( "data-sub_shop" );
	} else {
		$location = 0;
		$location_name = '';
		$sub_shop = 0;
	} // else
	
	if( Math.round( $quantity ) > Math.round( $stock ) ) {
		show_popup( t('Achtung Sie bestellen mehr als derzeit verfügbar. Trotzdem fortfahren?') );
		return;
	} // if
	
  lock_screen();
	$('#ajax_loader').show(); // Loader Picture
  
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?buy_article=true',
		{
			action: "buy_article",
			artikel_link_id: $artikel_link_id,
			artikel_id: $artikel_id,
			quantity: $quantity,
			manufacturer: $manufacturer,
			model: $model,
			vehicle: $vehicle,
			brand: $('#brand_'+$artikel_id+'_'+$replace_id).text(),
			brand_id: $('#brand_'+$artikel_id+'_'+$replace_id).attr( "data-brand_id" ),
			no: $no,
			name: $name,
			replace_id: $replace_id,
			location: $location,
			location_name: $location_name,
			sub_shop: $sub_shop,
			
			// Protocol Params
			active_search: $('#active_search').val(),
			audatex_vehicle: $('#categories_title').html(),
			audatex_oe: $('#active_tree_category').val(),
			audatex_item_description: $('#active_tree_category_description').val(),
		},
		function( data ) {
			show_popup( data.resultData, 2 );
			update_orders();
		},
		'json'
	).done( function() {
		$('#ajax_loader').hide(); // Loader Picture
	  setTimeout( unlock_screen );
  });
} // buy_article

/*
function show_popup_orig( $str ) {
	$('#list .window .content').empty();
	$('#list .window .content').append( $str );
	
	$('.window').dialog({
		buttons: [{
			text: t( "OK" ),
			icon: "ui-icon-circle-check",
			click: function() {
				$(this).dialog("destroy");
				unlock_screen();
			}
		}],
		close : function(event, ui) {
			$(this).dialog("destroy");
			unlock_screen();
		},
		create: function () {
			var buttons = $('.ui-dialog-buttonset').children('button');
      buttons.removeClass("ui-button ui-widget ui-state-default ui-state-active ui-state-focus ui-corner-all");
      buttons.addClass( "btn btn-primary moj-btn-sm" );
      $('.ui-icon-circle-check').css({'filter':'invert(100%) brightness(2)'});
		}		
	});
} // show_popup
*/  
function lock_screen() {
	$('#overlay').show().css({
		'top': (self.pageYOffset || document.documentElement.scrollTop  || document.body.scrollTop),
		'height': 5000
	});
} // lock_screen

function unlock_screen() {
	$('#overlay').hide();
//	$('#timeout_popup').hide();
//	$(".window").dialog("close");
} // unlock_screen

function open_article_popup( $articleLinkId, $articleId, $manufacturer, $model, $vehicle, $brand, $replace_id ) {
	$('#ajax_loader').show(); // Loader Picture
  
  show_window( [$articleLinkId, $articleId, $manufacturer, $model, $vehicle, $brand, $replace_id], 101, 'show_article=true' );

  $(document).ajaxStop(function () {
  	$(this).unbind("ajaxStop");
  	
    unlock_screen();
  	$('#ajax_loader').hide(); // Loader Picture
  });
} // open_article_popup

function search_oen( $value, $bNewWindow ) {
	if( !$bNewWindow ) { // Same Window
		if( $('.window').is(':visible') )
			$('.window').dialog( "destroy" );
		unlock_screen();

		// alte Tooltips
		$('.ui-tooltip').hide();

		$("#search_tabs").tabs("option", "active", 0);
		$('#search_pattern').val( $value );
		$('#search_pattern_type').val( "10" );
		
		fill_articles();
	} else { // New Window
		window.open( '../admin/tec_search.php?indiv_search_pattern='+$value );
	} // else
}

function fill_search_pattern() {
	$('#search_pattern').val( $('#combo_search_pattern').val() );
	$('#search_pattern').focus();
} // fill_search_pattern

function add_search_pattern( $str ) {
	$("#list_autocomplete li:contains('"+$str+"')").remove();
	
	$('#list_autocomplete').prepend( '<li>'+$str+'</li>' );
	$("#search_pattern").autocomplete({
		source: '#list_autocomplete',
		showButton: true
	});
} // add_search_pattern

function add_search_pattern_audatex( $str ) {
	$("#list_autocomplete_audatex li:contains('"+$str+"')").remove();
	
	$('#list_autocomplete_audatex').prepend( '<li>'+$str+'</li>' );
	$("#search_fin_audatex").autocomplete({
		source: '#list_autocomplete_audatex',
		showButton: true
	});
} // add_search_pattern

// -----------------------------------------------------------------------------------------------------------
// SUCHE NACH ARTIKELBEZEICHNUNG
// -----------------------------------------------------------------------------------------------------------

function fill4_categories() {
	$('#ajax_loader').show(); // Loader Picture
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load4_categories=true',
		{ 
			action: "load4_categories", 
			pattern: $('#search4_pattern').val()
		},
		function( data ) {
			// Clear Select
			$('#search4_categories').find('option').remove();

			$.each(data.resultData, function (index, value) {
			    $('#search4_categories').append($('<option/>', {
			        value: value.id,
			        text : value.name
			    }));
			});
			
			$('#ajax_loader').hide(); // Loader Picture
		},
		'json'
	);
} // fill4_categories

function fill4_model() {
	$manufacturer = $('#search4_manufacturer').val();

	$('#ajax_loader').show(); // Loader Picture

	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_model=true',
		{ 
			action: "load_model", 
			manufacturer: $manufacturer, 
			from: "", 
			to: ""
		},
		function( data ) {
			// Clear Select
			$('#search4_model').find('option').remove();

			$.each(data.resultData, function (index, value) {
			    $('#search4_model').append($('<option/>', {
			        value: value.id,
			        text : value.name
			    }));
			});

			$('#ajax_loader').hide(); // Loader Picture
		},
		'json'
	);
} // fill4_model

function fill5_categories() {
	$('#ajax_loader').show(); // Loader Picture
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load4_categories=true',
		{ 
			action: "load5_categories", 
			brand: $('#search5_brand').val(),
			category: $('#search5_categories').val(),
		},
		function( data ) {
			// Clear Select
			$('#search5_categories').find('option').remove();

			$.each(data.resultData, function (index, value) {
			    $('#search5_categories').append($('<option/>', {
			        value: value.id,
			        text : value.name
			    }));
			});
			
			$('#ajax_loader').hide(); // Loader Picture
		},
		'json'
	);
} // fill4_categories

function show_articles_type( $iType ) {
	switch( $iType ) {
		case 0:
			$('#search_articles_big').hide();
			$('#search_articles_table').show();
			break;
		case 1:
			$('#search_articles_table').hide();
			$('#search_articles_big').show();
			break;
	} // switch
} // show_articles_type

function fill_linked_vehicles( $articleId, $manufacturer, $vehicle, $start = 1 ) {
	$('#linked_vehicles table tr td').closest('tr').remove();
	
	$('#ajax_loader').show(); // Loader Picture
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?load_linked_vehicles=true',
		{ 
			action: "load_linked_vehicles", 
			articleId: $articleId,
			manufacturer: $manufacturer,
			vehicle: $vehicle,
			start: $start
		},
		function( data ) {
			// Umblättern
			$strOut = '';
			if( data.resultData[0].anz > data.resultData[0].limit ) {
			 	$prev_disabled = "";
			 	$prev_id = $start - data.resultData[0].limit;
		  	if( $prev_id < 1 ) $prev_disabled = ' disabled';
	
		  	$next_disabled = "";
		  	$next_id = $start + data.resultData[0].limit;
		  	if( $next_id > data.resultData[0].anz ) $next_disabled = ' disabled';
				
				$strOut = '<li class="page-item'+$prev_disabled+'"><a class="page-link" data-start="'+$prev_id+'" onClick="fill_linked_vehicles('+$articleId+', '+$manufacturer+', '+$vehicle+', '+$prev_id+');" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
				
		  	for( $i=1; $i<=data.resultData[0].anz; $i+=data.resultData[0].limit ) {
		  		$active = '';
		  		if( $i == $start )
		  			$active = ' active';
					$strOut = $strOut+'<li class="page-item'+$active+'" onClick="fill_linked_vehicles('+$articleId+', '+$manufacturer+', '+$vehicle+', '+$i+');"><a class="page-link" data-start="'+$i+'">'+(($i-1)/data.resultData[0].limit+1)+'</a></li>';
		  	} // for
				
		  	$strOut = $strOut+'<li class="page-item'+$next_disabled+'"><a class="page-link" data-start="'+$next_id+'" onClick="fill_linked_vehicles('+$articleId+', '+$manufacturer+', '+$vehicle+', '+$next_id+');" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>';
			} // if
			$('#linked_vehicles ul li').remove();					
			$('#linked_vehicles ul').append( $strOut );

			if( data.resultData[0].anz > 0 )
				$.each(data.resultData, function (index, value) {
					$element = $('#linked_vehicles table');
					$element.append( 
			  			'<tr>'+
		  					'<td>'+value.manufacturer+'</td>'+
		  					'<td>'+value.model+'</td>'+
		  					'<td>'+value.vehicle+'</td>'+
		  					'<td>'+value.kw+'</td>'+
		  					'<td>'+value.constrution_type+'</td>'+
		  					'<td>'+value.fuel_type+'</td>'+
		  					'<td>'+value.by_from+'</td>'+
		  					'<td>'+value.by_to+'</td>'+
		  					'<td>'+value.ccm+'</td>'+
		  					'<td>'+value.motorcode+'</td>'+
		  					'<td>'+value.zylinder+'</td>'+
		  				'</tr>'
		  			);
				});
			
			$('#ajax_loader').hide(); // Loader Picture
		},
		'json'
	);
} // fill_linked_vehicles

function save_korb() {
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?save_korb=true',
		{ 
			action: "save_korb", 
			text: $('#user_header_info').val(),
			document_type: $('#document_type').val(),
			delivery_method: $('#delivery_method').val(),
			delivery_method_date: $('#delivery_method_date').val()
		},
		function( data ) {
			$('#document_type_chosen').removeClass( "redOutlineTextBox" );
			$('#delivery_method_chosen').removeClass( "redOutlineTextBox" );
			$('#delivery_method_date').removeClass( "redOutlineTextBox" );
			
			$bError = false;
			if( $('#document_type_chosen').is(':visible') && ($('#document_type').val() == '') ) {
				$('#document_type_chosen').addClass( "redOutlineTextBox" );
				$bError = true;
			} // if
			if( $('#delivery_method_chosen').is(':visible') && ($('#delivery_method').val() == '') ) {
				$('#delivery_method_chosen').addClass( "redOutlineTextBox" );
				$bError = true;
			} // if			
			if( $('#delivery_method_date').is(':visible') && ($('#delivery_method_date').val() == '') ) {
				$('#delivery_method_date').addClass( "redOutlineTextBox" );
				$bError = true;
			} // if

			if( !$bError ) {
				$('#user_header_info').val( '' );
				location.href = "?indiv_project_id=4&indiv_list_id=31&indiv_order=1";
			} // if
		},
		'json'
	);
} // save_korb

function fill_infos( $subcategory_id_filter = 0, $start = 1 ) {
	check_session();
	
	$('#ajax_loader').show();
  $('#search_articles_infos_hider').show();
    
   if( $subcategory_id_filter == -1 ) { 
	  if( $('#search_categories_kat').jstree().get_selected(true)[0] !== undefined ) {
		  $subcategory_id_filter = $('#search_categories_kat').jstree().get_selected(true)[0].id;
		  $subcategory_id_filter = $subcategory_id_filter.substr( 9, 5 );
	  } // if
   } // if

 	$param = {};
	$param['action'] = 'get_infos';
	$param['text'] = $('#search_infos').val();
	$param['start'] = $start;
	$param['filter_width'] = $('#filter_width').val();
	$param['filter_height'] = $('#filter_height').val();
	$param['filter_length'] = $('#filter_length').val();
	$param['filter_diameter'] = $('#filter_diameter').val();
	$param['filter_viscosity'] = $('#filter_viscosity').val();
	$param['filter_kw'] = $('#filter_kw').val();
	$param['filter_ah'] = $('#filter_ah').val();
	$param['filter_supplier'] = $('#filter_supplier').val();
	$param['filter_belt_shafts'] = $('#filter_belt_shafts').val();
	$param['filter_volt'] = $('#filter_volt').val();
	$param['filter_amper'] = $('#filter_amper').val();
	$param['filter_season'] = $('#filter_season').val();
	
	if( $('#search_by_customerno').length && ($('#search_by_customerno').val() != "") ) $param['customer_no'] = $('#search_by_customerno').val();
	$param['subcategory_id_filter'] = $subcategory_id_filter;
   
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?get_infos=true',
		$param,
		function( data ) {
			$('#search_articles_infos').html( data.resultData );
			
			// Table Sort
			var table = $('#search_articles_infos_table').stupidtable();
			table.on("aftertablesort", function (event, data) {
		          var th = $(this).find("th");
		          th.find(".arrow").remove();
		          var dir = $.fn.stupidtable.dir;
		          var arrow = data.direction === dir.ASC ? "fas fa-angle-up" : "fas fa-angle-down";
		          th.eq(data.column).append('<span class="arrow"><i class="'+ arrow +'"></i></span>');
		        });
			
			// Enable Spinner
			$('.spinner_quantity').spinner(
				{min: 0,
					spin: function(event, ui) { $(this).val(ui.value); $(this).change(); }
				}
			);
			$('.spinner_quantity').change(function() {
				$id = $(this).attr('id').substr( 9 );
				$price = ConvertPriceToDB( $('#price_'+$id).html() );
				$quantity = ConvertPriceToDB( $('#quantity_'+$id).val() );
				$amount = $price * $quantity;

				$('#amount_'+$id).html( ConvertPriceToView( $amount ) );
			});
			// Enable Tree View
			$('#search_categories_kat').jstree({
				"plugins" : [ "sort", "search" ]
			});
			$('#search_categories_kat').on('select_node.jstree', function (e, data) {
			    data.instance.toggle_node(data.node);
			});			
			
			// Jump to Top
			window.location.href = "#my_top";
		},
		'json'
	).done( function() {
		// Umblättern bei Universalartikel
		$('#search_articles_infos nav a').click( function(evt) {
			$page = $( this ).attr( "data-page" );
			$start = $( this ).attr( "data-start" );
			
			fill_infos( $subcategory_id_filter, $start );
			
	    evt.stopPropagation();
	    return false;
		});
		
		$('#ajax_loader').hide();
	});
} // fill_infos

function search_for_infos( $value ) {
	$("#search_tabs").tabs("option", "active", 0);
	$('#search_pattern').val( $value );
	$('#search_pattern_type').val( "10" );
	
	fill_articles();
} // search_for_infos

function print_order( $id ) {
	var myWindow=window.open( "", t('Drucken'), "location=no,status=no,scrollbars=yes,toolbar=no,width=800,height=600" );
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?print_order=true',
		{ 
			action: "print_order", 
			id: $id,
		},
		function( data ) {
			var html =
			'<html><head>'+
		    '<link href="/css/s.php" rel="stylesheet" type="text/css" />'+
		    '</head><body style="overflow:auto;height:auto;">'+
		    	data.resultData+
		    '</body></html>';

		  $(myWindow.document.body).html(data.resultData);
	    myWindow.focus();
	    myWindow.print();
		},
		'json'
	);
} // print_order

function set_manufacturers() {
	$all = $('#all_manufacturers').is(":checked");

	// Hide all
    $('#search_shortcuts_categories').hide();
    $('#search_articles').hide();
    $('#search_articles_infos_hider').hide();

	// Clear Select
	$('#search_manufacturer').find('option').remove();
	$('#year_from').val( '0' );
	$('#search_model').find('option').remove();
	$('#search_vehicle').find('option').remove();
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?get_manufacturers=true',
		{ 
			action: "get_manufacturers", 
			all: $all,
		},
		function( data ) {
			$('#search_manufacturer').append( data.resultData );
			$('#search_manufacturer').trigger("chosen:updated");
		},
		'json'
	);
} // set_manufacturers

function save_search_history() {
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?save_history=true',
		{ 
			action: "save_history", 
			search_manufacturer: $('#search_manufacturer').val(),
			year_from: $('#year_from').val(),
			search_model: $('#search_model').val(),
			filter_fueltype: $('#filter_fueltype').val(),
			filter_pskw: $('#filter_pskw').val(),
			filter_motorcode: $('#filter_motorcode').val(),
			search_vehicle: $('#search_vehicle').val(),
			all_manufacturers: $('#all_manufacturers').val(),
			text: 
				$('#search_manufacturer option:selected').text() + ", " +	
				$('#search_model option:selected').text() + ", " +	
				$('#search_vehicle option:selected').text(),
		},
		function( data ) {
			reload_search_history();
		},
		'json'
	);
} // save_search_history

function restore_search_history() {
	$('#ajax_loader').show();
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?restore_history=true',
		{ 
			action: "restore_history", 
			id: $('#search_history').val(),
		},
		function( data ) {
      $('#search_manufacturer').val( data.resultData.search_manufacturer ).trigger("chosen:updated");
      $('#year_from').val( data.resultData.year_from ).trigger("chosen:updated");
      
			fill_model().done( function() {
				$('#ajax_loader').show();
				$('#search_model').val( data.resultData.search_model ).trigger("chosen:updated");

				fill_vehicle().done( function() {
					$('#ajax_loader').show();
					$('#filter_fueltype').val( data.resultData.filter_fueltype ).trigger("chosen:updated");
					$('#filter_pskw').val( data.resultData.filter_pskw ).trigger("chosen:updated");
					$('#filter_motorcode').val( data.resultData.filter_motorcode ).trigger("chosen:updated");
					
					$('#search_vehicle').val( data.resultData.search_vehicle ).trigger("chosen:updated");
					
					fill_shortcuts(); 
					fill_categories(0);
				})
			});
		},
		'json'
	).done( function() {
		$('#ajax_loader').hide();
  });  
} // save_search_history

function reload_search_history() {
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?reload_history=true',
		{ 
			action: "reload_history", 
		},
		function( data ) {
			// Clear Select
			$('#search_history').find('option').remove();
			
			$.each(data.resultData, function (index, value) {
		    $('#search_history').append($('<option/>', {
		        value: value.id,
		        text : value.text
		    }));
			});
			$('#search_history').trigger("chosen:updated");
		},
		'json'
	);
} // reload_search_history

var $window_3d; 
function open_3d_search() {
	$('#ajax_loader').show(); // Loader Picture
	
	$fin = $('#search_fin_audatex' ).val();
	if( $fin == "" )
		return;
	
	add_search_pattern_audatex( $fin );
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?open_3d_search=true',
		{ 
			action: "open_3d_search",
			fin: $fin
		},
		function( data ) {
			$('#ajax_loader').hide(); // Loader Picture
			
			// Open New Window with URL
			$window_3d = window.open( data.resultData.url, '3D', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=950,height=700' );
		},
		'json'
	);
} // open_3d_search

function import_from_3d_search() {
	if( ($window_3d !== undefined) && ($window_3d !== null) )
		$window_3d.close();
	
	$('#ajax_loader').show(); // Loader Picture
	$('#categories_title').html( '' );
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/tecdoc_ajax.php?import_from_3d_search=true',
		{ 
			action: "import_from_3d_search"
		},
		function( data ) {
			// Clear List
			$('#search_categories').empty();
			$('#search_categories').append( '<input type="text" id="search_categories_filter" style="margin-left: 11px; width: 90%; border: solid 1px #aaa;" placeholder="   <Filter>">' );
			$('#search_categories').append( '<div id="search_categories_div"></div>' );

			if( data.resultData.task.VehicleIdentification === undefined ) {
				$('#search_categories_filter').hide();
				$strTitle = t( "Es wurden keine Daten in der 3D Suche gefunden, die importiert werden können." );
			} else {
				$('#search_categories_filter').show();
				$strTitle = 
					data.resultData.task.VehicleIdentification.ManufacturerName+' '+
					data.resultData.task.VehicleIdentification.ModelName+' '+
					data.resultData.task.VehicleIdentification.SubModelName;
			} // else
			
			// Build Tree
			$('#categories_title').html( $strTitle );
			$('#search_shortcuts_categories').show();			

			if( data.resultData.task.SpareParts !== undefined ) {
				if( data.resultData.task.SpareParts.PartDtl.PartNo !== undefined )  
					create_element_audatex( data.resultData.task.SpareParts.PartDtl, 0 );
				else
					$.each(data.resultData.task.SpareParts.PartDtl, function (index, value) {
						create_element_audatex( value, index );
					});
			}
			
			// Enable Tree View
			$('#search_categories_div').jstree({
				"plugins" : [ "sort", "search" ],
			});
			$('#search_categories_div').on('select_node.jstree', function (e, data) {
			    data.instance.toggle_node(data.node);
			});
			
			var to = false;
			$('#search_categories_filter').keyup(function () {
			if(to) { clearTimeout(to); }
			to = setTimeout(function () {
				var v = $('#search_categories_filter').val();
				$('#search_categories_div').jstree(true).search(v);
				}, 250);
			});
			
			$('#ajax_loader').hide(); // Loader Picture
		},
		'json'
	);
} // import_from_3d_search

function create_element_audatex( $arr, $current_index ) {
	var $element;
	
	$element = $('#sub_0');
	if( !$element.length ) // Root nicht gefunden
		$('#search_categories_div').append( '<ul id="sub_0"></ul>' );

	$element = $('#sub_0');
	$element.append( '<li id="element_'+$current_index+'" data-jstree=\'{"icon":"/'+SUBDIR+'themes/_default/icons/search_16_tecdoc.png"}\'><a onClick="set_tree_category(\''+$arr.PartNo+'\',\''+$arr.PartDesc+'\'); fill_articles();"><span style="text-decoration: none;">'+$arr.PartNo+', '+$arr.PartDesc+'</span></a></li>' );
} // create_element

function preload_translations() {
	$str = t( 'Kategorien' );
	$str = t( 'Es wurden keine Daten in der 3D Suche gefunden, die importiert werden können.' );
	$str = t( 'Sie müssen einen Lagerort auswählen.' );
	$str = t( 'Bestellungen für diesen Lagerort können nicht über Warenkorb durchgeführt werden. Bitte nehmen Sie telefonisch Kontakt mit uns auf. Lagerort: ' );
	$str = t( 'Achtung Sie bestellen mehr als derzeit verfügbar. Trotzdem fortfahren?' );
	$str = t( 'Drucken' );
	$str = t( 'OK' );
	$str = t( 'Abbrechen' );
	$str = t( 'Datei wirklich löschen?' );
	$str = t( 'kein Ergebnis gefunden' );	
} // preload_translations

function change_delivery_method() {
	$need_date = $('#delivery_method option:selected').attr('data-need_date');
	
	if( $need_date == 1 ) 
		$('#delivery_method_date').show();
	else
		$('#delivery_method_date').hide();
} // change_delivery_method
