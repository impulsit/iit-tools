$(function() {
	show_menu();
	
	/*$('#tec_iframe').on('load',function() {
		switch_to_small_menu();
	});*/
	
	// Immer breites Menü anzeigen
	switch_to_big_menu();
	
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	  if (!$(this).next().hasClass('show')) {
	    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	  }
	  var $subMenu = $(this).next(".dropdown-menu");
	  $subMenu.toggleClass('show');

	  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
	    $('.dropdown-submenu .show').removeClass("show");
	  });

	  return false;
	});	

	// Eventhandler Console Fix
	jQuery.event.special.touchstart = {
	  setup: function( _, ns, handle ) {
	    if ( ns.includes("noPreventDefault") ) {
	      this.addEventListener("touchstart", handle, { passive: false });
	    } else {
	      this.addEventListener("touchstart", handle, { passive: true });
	    }
	  }
	};
	jQuery.event.special.mousewheel = {
	  setup: function( _, ns, handle ) {
	    if ( ns.includes("noPreventDefault") ) {
	      this.addEventListener("mousewheel", handle, { passive: false });
	    } else {
	      this.addEventListener("mousewheel", handle, { passive: true });
	    }
	  }
	};
	jQuery.event.special.touchmove = {
	  setup: function( _, ns, handle ) {
	    if ( ns.includes("noPreventDefault") ) {
	      this.addEventListener("touchmove", handle, { passive: false });
	    } else {
	      this.addEventListener("touchmove", handle, { passive: true });
	    }
	  }
	};		
}) // document ready

function switch_menu() {
	if( $('.menu_small').is(':visible') )
		localStorage.setItem( "menu_type", 1 ); // Switch to Standard
	else 
		localStorage.setItem( "menu_type", 2 ); // Switch to Small
	
	show_menu();
} // switch_menu

function switch_to_small_menu() {
	localStorage.setItem( "menu_type", 2 ); // Switch to Small
	
	show_menu();
} // switch_to_small_menu

function switch_to_big_menu() {
	localStorage.setItem( "menu_type", 1 ); // Switch to Standard
	
	show_menu();
} // switch_menu

function show_menu() {
	if( localStorage.getItem( "menu_type" ) == undefined )
		localStorage.setItem( "menu_type", 1 ); // Standard
	
	if( localStorage.getItem( "menu_type" ) == 1 ) { // Standard
		$('.menu_small').hide();
		$('.menu_standard').show();
		
		$('#logo a').show();
		$('#logo').width( 250 );
		$('#content').css( 'margin-left', '267px' ); 
	} else { // Small
		$('.menu_standard').hide();
		$('.menu_small').show();
		
		$('#logo a').hide();
		$('#logo').width( 34 );
		$('#content').css( 'margin-left', '51px' );
	} // else
} // show_menu

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
  };

function escapeHtml(string) {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}

function strip_tags( input, allowed ) {
  allowed = (((allowed || '') + '')
    .toLowerCase()
    .match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
} // strip_tags

var sortSelect = function (select, attr, order) {
  if(attr === 'text'){
      if(order === 'asc'){
          $(select).html($(select).children('option').sort(function (x, y) {
              return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
          }));
          $(select).get(0).selectedIndex = 0;
      }// end asc
      if(order === 'desc'){
          $(select).html($(select).children('option').sort(function (y, x) {
              return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
          }));
          $(select).get(0).selectedIndex = 0;
      }// end desc
  }
};

function check_session() {
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/list_ajax.php?check_session=true',
		{ 
			action: "check_session" 
		},
		function( data ) {
		},
		'json'
	).fail(function(response) {
		location.href = "/"+SUBDIR+"admin/login_login.php?error=3";
		return( false );
	});
} // check_session

function number_format (number, decimals, dec_point, thousands_sep) {
    var n = number, prec = decimals;

    var toFixedFix = function (n,prec) {
        var k = Math.pow(10,prec);
        return (Math.round(n*k)/k).toString();
    };

    n = !isFinite(+n) ? 0 : +n;
    prec = !isFinite(+prec) ? 0 : Math.abs(prec);
    var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
    var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;

    var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); 
    //fix for IE parseFloat(0.55).toFixed(0) = 0;

    var abs = toFixedFix(Math.abs(n), prec);
    var _, i;

    if (abs >= 1000) {
        _ = abs.split(/\D/);
        i = _[0].length % 3 || 3;

        _[0] = s.slice(0,i + (n < 0)) +
               _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
        s = _.join(dec);
    } else {
        s = s.replace('.', dec);
    }

    var decPos = s.indexOf(dec);
    if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
        s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
    }
    else if (prec >= 1 && decPos === -1) {
        s += dec+new Array(prec).join(0)+'0';
    }
    return s; 
}
