<?php
header("Content-Type: application/javascript");

require_once( "../classes/config_data.php" );
require_once( CLASS_DIR.'mysql.php' );

echo "var SUBDIR = '".SUBDIR."';";

$location = $_location;
if( defined( "SIMULATE_LOCATION" ) )
	$location  = SIMULATE_LOCATION;
if( strpos( $location, "_atit_at" ) !== false )
	echo "var _ENABLE_TRANSLATION = 1;";
else
	echo "var _ENABLE_TRANSLATION = 0;";

$db = mysql::getInstance();

$db->query( "SELECT version_type FROM CORE_SETUP WHERE id='1'" );
$setup = $db->getNext();

switch( $setup['version_type'] ) {
	case 0:
			// JQuery 2.2.1, Bootstrap 3.3.6, JQuery-UI 1.11.4, Chosen 1.8.3, Timepicker 1.3
			include( "jquery_v2.2.1/jquery-2.2.1.min.js" );
			include( "bootstrap_v3.3.6/js/bootstrap.min.js" );
			include( "jquery-ui_v1.11.4/jquery-ui.min.js" );
		break;
	case 1:
			// JQuery 3.2.1, Bootstrap 4.0.0-beta, JQuery-UI 1.12.1, Chosen 1.8.2, Timepicker 1.6.3
			include( "jquery_v3.2.1/jquery-3.2.1.min.js" );
			include( "jquery_v3.2.1/jquery-migrate-3.0.0.min.js" );
			include( "bootstrap_v4.0.0-beta/popper.js" ); // Need by bootstrap 4.0.0-beta
			include( "bootstrap_v4.0.0-beta/js/bootstrap.min.js" );
			include( "jquery-ui_v1.12.1/jquery-ui.min.js" );
		break;
	case 2:
			// JQuery 3.3.1, Bootstrap 4.1.3, JQuery-UI 1.12.1, Chosen 1.8.3, Timepicker 1.6.3
			include( "jquery_v3.3.1/jquery-3.3.1.min.js" );
			include( "bootstrap_v4.1.3/popper.min.js" ); // Need by bootstrap
			include( "bootstrap_v4.1.3/js/bootstrap.min.js" );
			include( "jquery-ui_v1.12.1/jquery-ui.min.js" );
		break;
} // switch

include( "chosen_v1.8.3/chosen.jquery.min.js" );
include( "jquery-ui-timepicker-addon_v1.6.3/jquery-ui-timepicker-addon.js" );
include( "fine-uploader_v5.16.2/jquery.fine-uploader.min.js" );
include( "stupid-table_v1.0.1/stupidtable.min.js" );

include( "tinymce_v4.9.2/js/tinymce/jquery.tinymce.min.js" );
include( "jstree_v3.3.7/dist/jstree.min.js" );
include( "richwidgets_v1.9.1/src/widgets/input/autocomplete.js" );

include( "readmore_v3/readmore.min.js" );
include( "tablesorter_v2.0/dist/js/jquery.tablesorter.min.js" );		 // DO NOT CHANGE, INDIV FIX INSIDE
include( "tablesorter_v2.0/dist/js/jquery.tablesorter.widgets.js" ); // DO NOT CHANGE, INDIV FIX INSIDE

//include( "toastui_codesnipped_v1.5.0/tui.code-snippet.js" );
//include( "toastui_calendar_v1.9.0/dist/tui-calendar.js" );

include( "f_main_functions.js" );
include( "f_liste.js" );
include( "f_tecdoc.js" );

if( file_exists( '../themes/basilica_at/js/f.php' ) ) include( '../themes/basilica_at/js/f.php' ); // Get Location JS

include( "../themes/_default/js/f.php" );
?>