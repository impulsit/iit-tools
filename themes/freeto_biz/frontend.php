<?php
if( !isset($_SESSION) ) session_start();

require_once( 'classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'mail.php' );

$db = mysql::getInstance();
$f = functions::getInstance();
$mes = messages::getInstance();
$mail = fb_mail::getInstance();

// Bestimme Subdomain
if( isset( $_POST['domain'] ) )
	$domain = $_POST['domain'];
else
	$domain = key( $_GET );

$strOut = '';

if( !$db->query( "
	SELECT d.fragebogen_id, f.titel, f.beschreibung, f.geschlecht_pflicht, f.typ_id
	FROM WHY_DOMAINS AS d
	LEFT JOIN WHY_FRAGEBOGEN AS f ON (f.fragebogen_id=d.fragebogen_id)
	WHERE d.domain LIKE '".$domain."' AND d.aktiv='1'" ) )
	$mes->addError( "Hier existiert kein aktiver Fragebogen." );
else
	$fragebogen = $db->getNext();

require_once( CLASS_DIR."templates/fe_header.php" );

if( $mes->noError() ) {
	if( isset( $_POST['id'] ) ) $id = $_POST['id']; else $id = 0;
	if( isset( $_POST['akt_frage'] ) ) $akt_frage = $_POST['akt_frage']; else $akt_frage = 0;
	if( isset( $_POST['next'] ) || isset( $_POST['prev'] ) || isset( $_POST['end'] ) ) {
		if( $akt_frage > 0 ) {
			// Frage
			$db->query( "SELECT frage_id, frage, antworttyp_id, kurz FROM WHY_FRAGEN WHERE fragebogen_id='".$fragebogen['fragebogen_id']."' AND frage_id='".$_POST['frage_id']."'" );
			$frage = $db->getNext();

			switch( $frage['antworttyp_id'] ) {
				case 1: case 4:
						// Texteingabe
						$antwort = $_POST['antwort_id'];
					break;
				case 2:
						// Radio
						if( !isset( $_POST['antwort_id'] ) ) $_POST['antwort_id'] = 0;
						$antwort = array( $_POST['antwort_id'] => 1 );
					break;
				case 3:
						// Checkboxen
						if( !isset( $_POST['antwort_id'] ) ) $_POST['antwort_id'] = 0;
						$antwort = $_POST['antwort_id'];
					break;
			} // switch

			$f->save_answer( $id, $fragebogen['fragebogen_id'], $_POST['frage_id'], $antwort, $frage['antworttyp_id'] );

			if( isset( $_POST['next'] ) ) $akt_frage++;
			if( isset( $_POST['prev'] ) ) $akt_frage--;
			if( isset( $_POST['end'] ) ) {
				include( "auswertung.php" );

				// Email senden
				$db->update( "WHY_USER_FRAGEBOGEN", array( "fertig_ausgefuellt" => 1 ), "id='".$id."'" );
				$db->commit();

				$f->send_fb_email( $id );
				echo "<br><br>Ihr Ergebnis wurde gespeichert.";

				exit;
			} // if
		} else {
			if( !isset( $_POST['email'] ) || ($_POST['email'] == "") ) {
				//$_POST['email'] = session_id();
				$mes->addError( 'Bitte Email erfassen.' );
			} // if
			if( ($fragebogen['geschlecht_pflicht'] == 1) && ($_POST['geschlecht'] != 1) && ($_POST['geschlecht'] != 2) )
				$mes->addError( 'F�r diesen Test m�ssen Sie Ihr Geschlecht angeben.' );

			if( $mes->noError() ) {
				if( $db->query( "SELECT id FROM WHY_USER_FRAGEBOGEN WHERE email='".$_POST['email']."' AND fragebogen_id='".$fragebogen['fragebogen_id']."'" ) ) {
					$r = $db->getNext();

					$id = $r['id'];
				} else {
					$id = $db->insert( "WHY_USER_FRAGEBOGEN", array(
						"email" => $_POST['email'],
						"ip" => $_SERVER['REMOTE_ADDR'],
						"zeit" => date( "Y-m-d H:i:s", time() ),
						"fragebogen_id" => $fragebogen['fragebogen_id'],
						"geschlecht" => $_POST['geschlecht']
					) );
					$db->commit();
				} // if

				$akt_frage++;
			} // if
		} // else
	} // if

	if( $akt_frage == 0 ) {
		// Fragebogen
		$strOut .= '
			<div class="content">
				<div class="fragebogen_beschreibung">'.
				'<p class="fragebogen_titel">'.$fragebogen['titel'].'</p>'.
				$fragebogen['beschreibung'].'</div>';

		// Daten abfragen
		$strOut .= $mes->printMessages();
		$strOut .= 'Email: <input type="text" value="" name="email">';

		if( $fragebogen['geschlecht_pflicht'] == 1 )
			$strOut .= '<br />Geschlecht: <input id="gw" type="radio" value="1" name="geschlecht"> <label for="gw">weiblich</label> <input id="gm" type="radio" value="2" name="geschlecht"> <label for="gm">m�nnlich</label>';
		else
			$strOut .= '<input type="hidden" value="0" name="geschlecht">';
	} // if

	if( $akt_frage > 0 ) {
		// Status
		$db->query( "SELECT COUNT( frage_id ) AS max FROM WHY_FRAGEN WHERE fragebogen_id='".$fragebogen['fragebogen_id']."'" );
		$status = $db->getNext();

		$prev = '';
		if( $akt_frage > 1 )
			$prev .= '<input class="button button_prev" type="submit" value="<--" name="prev">';

		$next = '';
		if( $akt_frage < $status['max'] )
			$next .= '<input class="button button_next" type="submit" value="-->" name="next">';

		// Fragebogen
		$strOut .= '
			<div class="content">'.$prev.$next.'
				<div class="fragebogen_beschreibung">'.
				'<p class="fragebogen_titel">'.$fragebogen['titel'].'</p>'.
				$fragebogen['beschreibung'].'</div>';

		// Frage
		$db->query( "SELECT frage_id, frage, antworttyp_id, kurz FROM WHY_FRAGEN WHERE fragebogen_id='".$fragebogen['fragebogen_id']."' ORDER BY position LIMIT ".($akt_frage-1).", 1" );
		$frage = $db->getNext();

		$strOut .= '<div class="frage_lang">'.
			'<table width="100%">
				<tr><td><p class="frage_kurz">'.$akt_frage.'. '.$frage['kurz'].'</p></td><td><div class="fragebogen_status">Frage '.$akt_frage.' von '.$status['max'].'</div></td></tr>
			</table>'.
			$frage['frage'].'</div>';

		// Antwort
		$strOut .= '<div class="antworten">';
		$db->query( "SELECT antwort_id, wert, antwort_titel FROM WHY_ANTWORTEN WHERE frage_id='".$frage['frage_id']."' ORDER BY position" );
		while( $db->isNext() ) {
			$antwort = $db->getNext();

			$value = $f->load_answer( $id, $fragebogen['fragebogen_id'], $frage['frage_id'], $antwort['antwort_id'], $frage['antworttyp_id'] );

			$width = '';
			switch( $frage['antworttyp_id'] ) {
				case 1:
						// Texteingabe
						$a = '<label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label> <input type="text" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id['.$antwort['antwort_id'].']" value="'.$value.'">';
					break;
				case 2:
						// Radio
						if( $value == 1 ) $c = ' checked'; else $c = '';
						$a = '<input type="radio" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id" value="'.$antwort['antwort_id'].'"'.$c.'> <label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label>';
					break;
				case 3:
						// Checkboxen
						if( $value == 1 ) $c = ' checked'; else $c = '';
						$a = '<input type="checkbox" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id['.$antwort['antwort_id'].']" value="1"'.$c.'> <label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label>';
					break;
				case 4:
					// Texteingabe
					$a = '<label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label><br /><textarea cols="30" rows="5" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id['.$antwort['antwort_id'].']">'.$value.'</textarea>';
					$width = ' style="width:auto;"';
					break;
			} // switch

			$strOut .= '<div class="antwort"'.$width.'>'.$a.'</div>';
		} // while
		$strOut .= '</div>';
	} // if

	echo '
		<form method="post" action="index.php">
			<input type="hidden" name="domain" value="'.$domain.'">
			<input type="hidden" name="id" value="'.$id.'">
			<input type="hidden" name="akt_frage" value="'.$akt_frage.'">';
	if( $akt_frage > 0 )
		echo '
			<input type="hidden" name="frage_id" value="'.$frage['frage_id'].'">';
	echo $strOut;

	// N�chste Frage
	if( $akt_frage == 0 ) {
		$v = "zur ersten Frage";
		$v1 = "next";
	} else {
		if( $akt_frage >= $status['max'] ) {
			$v = "absenden";
			$v1 = "end";
		} else {
			$v = "n�chste Frage";
			$v1 = "next";
		} // else
	} // if

	echo '<br style="clear:both;"><input class="button" type="submit" value="'.$v.'" name="'.$v1.'">
		</form></div>';
} else
	echo $mes->getErrorMessages();

require_once( CLASS_DIR."templates/fe_footer.php" );
?>