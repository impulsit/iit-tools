$(function() {
})

function basilica_bind_buttons() {
	$list_id = $('#current_list').val();
	
	if( $list_id == 1 ) { // Kurse
		if( !$.isNumeric( $('[name=kurs_id]').val() ) ) {
			$('#button_copy_from_template').click(function() {
				copy_from_template();
			});
			$('[name=kt_title]').change(function() {
				copy_from_template();
			});
		} // if
	} // if
}

function copy_from_template() {
	// Ajax Call
	$.post(
		'/'+SUBDIR+'themes/basilica_at/classes/ajax_call.php?copy_from_template=true',
		{ action: "copy_from_template", template_id: $('[name=kt_title]').find(':selected').val() },
		function( data ) {
			if( data.resultData.error ) {
				// Fehler
				show_popup( data.resultData.error );
			} else {
				// Setze Felder
				$.each( data.resultData.data, function (index, value) {
					switch( value.type ) {
						case "checkbox": $('[name='+value.name+']').prop('checked', true); break;
						case "select": $('[name='+value.name+']').val( value.value ).trigger("chosen:updated"); break;
						default: $('[name='+value.name+']').val( value.value ); break;
					} // switch
				});
			} // else
		},
		'json'
	);
}
