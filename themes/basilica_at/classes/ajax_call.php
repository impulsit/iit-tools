<?php
require_once( dirname( __FILE__ ).'/../../../classes/ajax_header.php' );

$result = array();

switch( $_POST['action'] ) {
	case "copy_from_template":
			if( isset( $_POST['template_id'] ) ) {
				if( $db->query( "SELECT * FROM BAS_KURSE_VORLAGEN WHERE kurstyp_id='".$_POST['template_id']."'" ) ) {
					$r = $db->getNext();

					$type = 'text';
					foreach( $r as $k => $v ) {
						$bStore = true;

						if( $db->query( "SELECT type FROM CORE_LISTS_FIELDS WHERE list_id='45' AND field='".$k."'" ) )
							$r2 = $db->getNext();
						if( $db->query( "SELECT type, field FROM CORE_LISTS_FIELDS WHERE list_id='45' AND save_field='".$k."'" ) ) {
							$r2 = $db->getNext();
							$k = $r2['field'];
						} // if

						switch( $r2['type'] ) {
							case 'checkbox': $type = 'checkbox'; break;
							case 'select': $type = 'select'; break;
							case 'time': $type = 'text'; $v = date( "H:i", strtotime( $v ) ); break;
							case 'date': $type = 'text'; $v = date( "d.m.Y", strtotime( $v ) ); break;
							default: $type = 'text';
						} // switch

						if( ($v == '0') || ($v == '00:00') || ($v == '01.01.1970') ) $bStore = false;
						if( ($k == 'kurs_id') || ($k == 'kt_title') ) $bStore = false;

						if( $bStore )
							$result['data'][] = array( "type" => $type, "name" => $k, "value" => $v );
					} // foreach
				} else {
					$result['error'] = "Keine Vorlage zum Kopieren gefunden.";
				} // else
			} // if
		break;
} // switch

$return['resultData'] = $result;

echo json_encode( $f->convert_ajax_remove( $return ) );
?>