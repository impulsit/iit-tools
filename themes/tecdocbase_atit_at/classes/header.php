<!DOCTYPE html>
<html lang="de">
<head>
	<title><? echo TITEL; ?></title>
	<meta charset="utf-8">
	<meta name="keywords" content="<? echo META; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- load favico -->
	<link rel="shortcut icon" href="<? echo $theme->get_favico(); ?>" />
	<!-- load javascript -->
	<script src="<?=DOMAIN_JS_DIR?>f.php?v=<?=$js_time?>"></script>
	<script type="text/javascript" src="<?=DOMAIN_JS_DIR?>tinymce_v4.9.2/js/tinymce/tinymce.min.js"></script>
	<!-- load css -->
	<link rel="stylesheet" href="<?=DOMAIN_CSS_DIR?>s.php?v=<?=$css_time?>">
	<link rel="stylesheet" href="<? echo "/".SUBDIR; ?>themes/tecdocbase_atit_at/css/jstree/atitec/style.min.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body onload="document_finished_loading();" style="background: url(<?php echo "/".SUBDIR."themes/tecdocbase_atit_at/img/body-background.jpg"; ?>); background-size: cover; background-attachment: fixed;">
<div id="overlay"></div>
<?
if( isset( $c_user_id ) && ($c_user_id > 0) ) $main_url = "/".SUBDIR."admin/core_backend.php"; else $main_url = "";
?>
<div class="wrapper">
	<div id="header" style="height: 60px !important;">
		<div class="container">
			<nav class="navbar navbar-expand-lg">
					<a class="navbar-brand" href="<?php echo "/".SUBDIR."admin/tec_search.php?project_id=4" ?>"><img src="<? echo $theme->get_logo(); ?>" style="padding-top: 0px; padding-bottom: 0px; padding-left: 5px; max-width: 188px; height: 40px; max-height: 40px;"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					  <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
					</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<div id="atitec-nav" style="width: 100%">
						<ul ul class="bg-darker navbar-nav mr-auto nav-pills nav-fill">
							<li class="nav-item"> <a class="nav-link" href="<?php echo "/".SUBDIR."admin/tec_search.php?project_id=4" ?>"><i class="fas fa-search"></i> <?php echo $t->t( "Suche" ); ?></a> </li>
							<li class="nav-item"> <a class="nav-link" href="<?php echo "/".SUBDIR."admin/list_redirect.php?indiv_project_id=4&indiv_list_id=31" ?>"><span class="shopping_count count_info"></span><i class="fas fa-shopping-cart"></i> <?php echo $t->t( "Warenkorb" ); ?></a> </li>
							<li class="nav-item"> <a class="nav-link" href="<?php echo "/".SUBDIR."admin/list_redirect.php?indiv_project_id=4&indiv_list_id=33" ?>"><i class="fas fa-list-alt"></i> <?php echo $t->t( "Bestellungen" ); ?></a> </li>
							<li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> <?php echo $userinfo['name']; ?></a>
								<div class="dropdown-menu dropdown-menu">
									<? echo $f->get_left_menu(); ?>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</div>
	</div>
	<div id="main-content">
		<div id="shop-container-before">
			<?php
			echo $tec->get_ad_picture( "banner-left-offside" );
			?>
		</div>
		<div id="shop-container-after">
			<?php
			echo $tec->get_ad_picture( "banner-right-offside" );
			?>
		</div>
		<div id="shop-container" class="container" style="<?php echo $tec->get_ad_picture( "banner-center-3" ); ?>">
			<div class="d-flex flex-row">
				<?php
				if( $tec->check_left_ad_showing() ) {
				?>
				<div class="col-xl-2" id="left-sidebar">
					<img src="<?php echo "/".SUBDIR."themes/tecdocbase_atit_at/logo/atitec-tecdoc-logo.png" ?>" class="logo_img" />
					<?php
					 if( ($userinfo['user_level'] >= 990) && ($_SESSION['project_id'] != 4) )
						echo $f->get_left_menu();
					else
							echo $tec->get_ad_picture( "banner-left-sidebar" );
					?>
				</div>
				<?php
				}
				?>
			<div class="flex-fill">
			<div class="d-flex flex-row">
				<div class="col-md-12">
						<section id="list">
							<div class="window" style="display:none">
								<div class="content"></div>
							</div>
						</section>
						<div id="timeout_popup" style="display:none"></div>
						<div id="box1"></div>
						<!-- <div id="box1"> -->
						<?
							$mes->restoreMessages();
							echo $mes->printMessages();
							$mes->resetError();
						?>
						<input type="hidden" id="project_id" name="project_id" value="<?php echo (isset($_SESSION['project_id'])?$_SESSION['project_id']:0); ?>">
