$(function() {
	// Spinner
	$('.spinner').spinner(
		{
			min: 1,
			spin: function(event, ui) { 
				$(this).val(ui.value); $(this).change(); 
			}
		}
	);

	// Plugin readmore 
	$('.text_readmore').readmore({
		collapsedHeight: 100,
		moreLink: '<a href="#" class="float-right text-info"><i class="far fa-plus-square"></i></a>',
		lessLink: '<a href="#" class="float-right text-info"><i class="far fa-minus-square"></i></a>',
	});
	
	// Paginator
	$('.paginator_page_input').width( $('#paginator_current_page').width() ) 	// Autosize Textbox
		// Autosize on Text Input
		.on('input', function () {
		  $('#paginator_current_page').text( $(this).val() );
		  $('.paginator_page_input').width( $('#paginator_current_page').width() ).val( $(this).val() );
		})
		// Check allowed Input Chars
		.keydown(function(event) {
		  if( !(event.keyCode == 8                                // backspace
	        || event.keyCode == 46                              // delete
	        || (event.keyCode >= 35 && event.keyCode <= 40)     // arrow keys/home/end
	        || (event.keyCode >= 48 && event.keyCode <= 57)     // numbers on keyboard
	        || (event.keyCode >= 96 && event.keyCode <= 105))   // number on keypad
	        )
				event.preventDefault();
		})

	// Catch Enter
	$(document).keyup(function (e) {
    if ($(".paginator_page_input").is(":focus") && (e.keyCode == 13)) {
			$page = $(".paginator_page_input").val();
      window.location.href="?indiv_page="+$page;
    }
	});

	// Autosize Content Scroll
	window.onload = function() {
		if( $('#content_scroll').length ) {
			//console.log( "trigger js onload" );
			//setTimeout(onAfterLoad, 10);
			onAfterLoad();
		} // if
	}

	$(window).resize(function() {
		if( $('#content_scroll').length ) {
			//console.log( "trigger resize" );
			//setTimeout(onAfterLoad, 10);
			onAfterLoad();
		} // if
	});
	
//	$(window).trigger( "resize" );
	
	console.log( "document ready finished" );
}) // Document Ready

function document_finished_loading() {
	if( $('#content_scroll').length ) {
		//console.log( "document finished loading" );
		//setTimeout(onAfterLoad, 10);
		onAfterLoad();
	} // if
}

function onAfterLoad() {
//	console.log( "onAfterLoad()" );

	// Table Sort
	$(".tablesorting").trigger("destroy");
	$(".tablesorting").tablesorter({
		usNumberFormat : false,
		dateFormat : "ddmmyyyy",
		widgets: ['stickyHeaders'],
		widgetOptions: {
      stickyHeaders_attachTo : '#content_scroll',
      stickyHeaders_yScroll : '#content_scroll',
		}
	});	

	// Calc neue Höhe
	$('#content_scroll').css({'height': Math.round($(window).height() - $('#content_scroll').offset().top - 15 - 20) }); // - ($('#footer').height()+20)

	// Calc neue Breite
	if( ($('#shop-container').css("max-width") != '1200px') && ($('#shop-container').css("max-width") != 'none') ) { // nur wenn Breites Layout, ansonst = 100%
		$('#content_scroll').css({'width': Math.round($(window).width() - $('#content_scroll').offset().left - 10) });
		$('#content_scroll').css({'padding-right': 10 }); // Avoid Horizontal Scrollbar
	} // if
	
	/* // Scrollbar vorhanden? -> Problem vernachlässigbar -> Performance
	var $ele = $('#content_scroll'),
  		overflowing = ($ele[0].scrollWidth-10) > $ele.width();
	if( overflowing ) // Calc neue Höhe + Scrollbar
		$('#content_scroll').css({'height': Math.round($('#content_scroll').height() + getScrollBarWidth()) });
	*/	

	// Check, Scrollbar vorhanden? 
	var $ele = $('#content_scroll'),
	overflowing = $ele[0].scrollWidth > $ele.width();
	if( overflowing ) { // Sticky Header entfernen, da kein Platz
		$(".tablesorting").trigger("destroy");
		$(".tablesorting").tablesorter({
			usNumberFormat : false,
			dateFormat : "ddmmyyyy",
		});
	} // if

	// Set Sticky Header Top
	$y = $('#content_scroll').offset().top - ($('#header').height()+20+10);
	$('head').append('<style>.tablesorter-sticky-wrapper{top:'+$y+'px !important}</style>');
	
	$('.paginator_page_input').width( $('#paginator_current_page').width() );
}

//-------------------------------------------------------------------------------------------------
// --- POPUP WINDOW
//-------------------------------------------------------------------------------------------------
function show_popup( $str, $iAutoCloseAfterSeconds = 0, $iWidth = 300 ) {
//	console.log("show");
	$('<div id="temp_popup_window" style="overflow-y: auto !important;"></div>').dialog({
		buttons: [{
			text: t( "OK" ),
			icon: "ui-icon-circle-check",
			click: function() {
				$('#temp_popup_window').dialog('open').dialog('widget').effect( {effect: 'fold', duration: 500, complete: function() {  $('#temp_popup_window').dialog("destroy").remove(); } } );
			}
		}],
		modal: true,
		width: $iWidth,
		maxHeight: 600,
		resizable: false,
		show: {effect: 'fade', duration: 1000},
		hide: {effect: 'fold', duration: 500},
		close: function(event, ui) {
			$(this).dialog("destroy").remove();
		},
		open: function (event, ui) {
			// Overlay Color
      $(".ui-widget-overlay").css({
        "opacity": "0.4",
        "background": "#000000"
      });
		},
		create: function () {
			// Close Button Layout
			var buttons = $('.ui-dialog-buttonset').children('button');
      buttons.removeClass("ui-button ui-widget ui-state-default ui-state-active ui-state-focus ui-corner-all");
      buttons.addClass( "btn btn-primary moj-btn-sm" );
      $('.ui-icon-circle-check').css({'filter':'invert(100%) brightness(2)'});
      
      // Text
      $(this).html( $str );
      
      // Autoclose
      if( $iAutoCloseAfterSeconds > 0 )
      	setTimeout( close_popup, $iAutoCloseAfterSeconds*1000 );
		}		
	});
} // show_popup

function close_popup() {
	$('#temp_popup_window').dialog('open').dialog('widget').effect( {effect: 'fold', duration: 500, complete: function() {  $('#temp_popup_window').dialog("destroy").remove(); } } );
} // close_popup

//-------------------------------------------------------------------------------------------------
//--- CONFIRM WINDOW
//-------------------------------------------------------------------------------------------------
function show_confirm( $str, $func_ok, $func_cancel ) {
	$strOK = t( "OK" );
	$strCancel = t( "Abbrechen" );
	
	$('<div id="temp_confirm_window"></div>').dialog({
		buttons: [{
				text: $strOK,
				icon: "ui-icon-circle-check",
				click: function() {
					if( typeof( $func_ok ) == 'function' )
		        setTimeout( $func_ok, 500 );
					$('#temp_confirm_window').dialog('open').dialog('widget').effect( {effect: 'fold', duration: 500, complete: function() {  $('#temp_confirm_window').dialog("destroy").remove(); } } );
				}
			},
			{
				text: $strCancel,
				icon: "ui-icon-circle-close",
				click: function() {
					if( typeof( $func_ok ) == 'function' )
		        setTimeout( $func_cancel, 500 );
					$('#temp_confirm_window').dialog('open').dialog('widget').effect( {effect: 'fold', duration: 500, complete: function() {  $('#temp_confirm_window').dialog("destroy").remove(); } } );
				}
			}],
		modal: true,
		resizable: false,
		show: {effect: 'fade', duration: 1000},
		hide: {effect: 'fold', duration: 500},
		close: function(event, ui) {
			if( typeof( $func_ok ) == 'function' )
        setTimeout( $func_cancel, 500 );
			$(this).dialog("destroy").remove();
		},
		open: function (event, ui) {
			// Overlay Color
      $(".ui-widget-overlay").css({
        "opacity": "0.4",
        "background": "#000000"
      });
      
      // Kein Focus auf Buttons
      $('.ui-dialog :button').blur();
		},
		create: function () {
			// Close Button Layout
			var buttons = $('.ui-dialog-buttonset').children('button');
      buttons.removeClass("ui-button ui-widget ui-state-default ui-state-active ui-state-focus ui-corner-all");
      buttons.addClass( "btn btn-primary moj-btn-sm" );
      $('.ui-icon-circle-check').css({'filter':'invert(100%) brightness(2)'});
      $('.ui-icon-circle-close').css({'filter':'invert(100%) brightness(2)'});

      // Text
      $(this).html( $str );
  	}		
	});
} // show_confirm

function close_confirm() {
	$('#temp_confirm_window').dialog('open').dialog('widget').effect( {effect: 'fold', duration: 500, complete: function() {  $('#temp_confirm_window').dialog("destroy").remove(); } } );
} // close_confirm

//-------------------------------------------------------------------------------------------------
//--- UPLOADER
//-------------------------------------------------------------------------------------------------
function show_file_uploader() {
	$type = "banner";
	
	$('<div id="temp_popup_window"></div>').dialog({
		width: 800,
		modal: true,
		resizable: false,
		close: function(event, ui) {
			$(this).dialog("destroy").remove();
		},
		open: function (event, ui) {
			// Overlay Color
      $(".ui-widget-overlay").css({
        "opacity": "0.4",
        "background": "#000000"
      });
      
      // Kein Focus auf Buttons
      $('#temp_popup_window :input').blur();
		},
		create: function () {
      // Text
			$('#temp_popup_window').append(  
				'<div id="template"></div>'+
				'<div id="fine-uploader-gallery"></div>'+
				'<div id="file_path"><select id="type" onChange="update_files();"><option value="banner">Banner</option><option value="univ_items">Universal Items</option></select></div>'+				
				'<div id="file_browser"></div>' );
			
			// Create Fine Uploader
		  $('#temp_popup_window #fine-uploader-gallery').fineUploader({
		  	template: 'qq-template',
		  	request: {
		  		endpoint: '../classes/fine-uploader/endpoint.php',
		  		params: {
		        type: function () {
		        	return $('#type').val();
		        }
		  		},		  		
		  	},
		  	thumbnails: {
		  		placeholders: {
		  			waitingPath: '/'+SUBDIR+'/themes/_default/icons/waiting-generic.png',
		  			notAvailablePath: '/'+SUBDIR+'/themes/_default/icons/not_available-generic.png'
		  		}
		  	},
		  	callbacks: {
		  		onSubmit: function(id, name) {
		  			this.setUuid(id, "")
		  		},
		  		onAllComplete: function(id, name) {
		  			update_files();
		  		},
	        onError: function(id, name, errorReason, xhrOrXdr) {
		    		  // Layout Bootstrap
		    		  $('.qq-upload-list li.qq-upload-fail').addClass( "danger" );
	        	
	            show_popup( qq.format("Error on file number {} - {}.  Reason: {}", id, name, errorReason) );
	        }
		  	}
		  });
		  
		  // Remove Button Title
		  $('#temp_popup_window #fine-uploader-gallery input').removeAttr( "title" );
		  
		  // Load Files
		  update_files();
		}		
	});
} // show_file_uploader

function update_files() {
	$('#file_browser').empty();
	
	// Ajax Call
	$.post(
		'/'+SUBDIR+'classes/file_browser_ajax.php?load_files=true',
		{ action: "load_files", type: $('#type').val() },
		function( data ) {
			$.each( data.resultData.files, function (index, value) {
				$('#file_browser').append( '<div class="file_line"><a onClick="delete_file(\''+value+'\')" style="cursor: pointer;"><i class="fas fa-trash-alt text-danger"></i></a> '+value+'</div>' );
			});
		},
		'json'
	);
} // update_files

function delete_file( $strFile ) {
	show_confirm( t( "Datei wirklich löschen?" ), function() {
		// Ajax Call
		$.post(
			'/'+SUBDIR+'classes/file_browser_ajax.php?delete_file=true',
			{ action: "delete_file", file: $strFile, type: $('#type').val()  },
			function( data ) {
			},
			'json'
		).done(function() {
			update_files();
		});
	}, function() {
		;
	} );
} // delete_file

//-------------------------------------------------------------------------------------------------
// --- CACHE
//-------------------------------------------------------------------------------------------------
var cache_jsdata = [];

function cache_clear() {
	cache_jsdata = [];
	localStorage.clear();
} // cache_clear

function cache_set( $key, $value ) {
	cache_jsdata[$key] = $value; 
	localStorage.setItem( $key, $value );
} // cache_set

function cache_get( $key ) {
	if( (cache_jsdata[$key] !== undefined) && (cache_jsdata[$key] !== null) && (cache_jsdata[$key] !== '') )
		return( cache_jsdata[$key] );
	
	return( localStorage.getItem( $key ) );
} // cache_get

// -------------------------------------------------------------------------------------------------
// --- PAGINATOR
//-------------------------------------------------------------------------------------------------
function paginator_focus_page_input( $element ) {
	$focus_element = $element.next().find('.paginator_page_input');

	$focus_element.val( '' ).val( $('#paginator_current_page').text() ).focus();
}
