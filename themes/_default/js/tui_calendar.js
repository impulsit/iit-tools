$(function() {
//	$.getScript( '/'+SUBDIR+"js/toastui_calendar_v1.9.0/dist/tui-calendar.min.js", function() {
//	   console.log( "loaded" );
//	});
	
	
	$('#calendar').tuiCalendar({
	  defaultView: 'month',
	  taskView: true,
	  template: {
	    monthGridHeader: function(model) {
	      var date = new Date(model.date);
	      var template = '<span class="tui-full-calendar-weekday-grid-date">' + date.getDate() + '</span>';
	      return template;
	    }
	  }
	});	
	
})
