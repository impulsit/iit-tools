<!DOCTYPE html>
<html lang="de"><head>
	<title><? echo TITEL; ?></title>
	<meta charset="utf-8">
	<meta name="keywords" content="<? echo META; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<? echo $theme->get_favico(); ?>" />
	<script src="<?=DOMAIN_JS_DIR?>jquery_v3.3.1/jquery-3.3.1.min.js"></script>
	<script src="<?=DOMAIN_JS_DIR?>bootstrap_v4.1.3/popper.min.js"></script>
	<script src="<?=DOMAIN_JS_DIR?>bootstrap_v4.1.3/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?=DOMAIN_JS_DIR?>bootstrap_v4.1.3/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body>
<style>
	.login-block{
		background: #0068b9;
		float:left;
		width:100%;
		height: 100vh;
		overflow: hidden;
		padding : 0px 0;
		background-size: cover;
		background-attachment: fixed;
		font-family: 'Roboto', sans-serif;
		font-size: 13px;
	}
	.banner-sec{background: url(<? echo "/".SUBDIR."themes/_default/img/login-banner.jpg" ?>), #fff; background-repeat: no-repeat; background-position: center top; no-repeat left bottom; background-size:cover; min-height:500px; border-radius: 0 10px 10px 0; padding:0;}
	.container{background:#fff; border-radius: 10px; box-shadow:10px 10px 20px rgba(0,0,0,0.1);}
	.carousel-inner{border-radius:0 10px 10px 0;}
	.carousel-caption{text-align:left; left:5%;}
	.login-sec{padding: 50px 30px 70px 30px; position:relative;}
	.login-sec .copy-text{position:absolute; width:80%; bottom:20px; font-size:13px; text-align:center;}
	.login-sec .copy-text i{color:#FEB58A;}
	.login-sec .copy-text a{color:#E36262;}
	.login-sec h2{margin-bottom:30px; font-weight:800; font-size:30px; color: #DE6262;}
	.login-sec h2:after{content:" "; width:100px; height:2px; background:#ddd; display:block; margin-top:20px; border-radius:1px; margin-left:auto;margin-right:auto}
	.btn-login{background: #DE6262; color:#fff; font-weight:600;}
	.banner-text{width:70%; position:absolute; bottom:40px; padding-left:20px;}
	.banner-text h2{color:#fff; font-weight:600;}
	.banner-text h2:after{content:" "; width:100px; height:5px; background:#FFF; display:block; margin-top:20px; border-radius:3px;}
	.banner-text p{color:#fff;}
	.center-content { margin-top: calc(50vh - 250px) }
	input:-webkit-autofill, input:-webkit-autofill:focus { -webkit-box-shadow: inset 0 0 0px 9999px #dff1ff; transition: background-color 5000s ease-in-out 0s;}
	.btn-primary {
		color: #fff;
		background-color: #0068b9;
		border-color: #0068b9;
		font-size: 1em;
	}
	.btn-primary:hover {
		background-color: #004990;
		border-color: #004990;
	}
	@media (max-width: 992px) {
		.banner-sec {
			display: none;
		}
		.login-block {
			padding-left: 30px;
			padding-right: 30px;
		}
	}
</style>

	<section class="login-block">
        <div class="container">
            <div class="row center-content">
                <div class="col-sm-12 col-md-4 login-sec">
                    <h2 class="text-center">
                        <img src="<? echo $theme->get_logo("_login"); ?>" style="width:100%" />
                    </h2>
					        	<?php
										$mes->restoreMessages();
										echo $mes->printMessages();
										$mes->resetError();
										?>
                    <form class="login-form" method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?reset_password=1">
                    		<input type="hidden" name="id" value="<?php echo $current_id; ?>">
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="text-uppercase"><? echo $t->t( "E-Mail" ); ?></label>
                            <input type="text" name="email" autocomplete="username" class="form-control" value="<?php echo $current_email; ?>" placeholder="" disabled>
                        </div>
                        <div class="form-group">
                                <label for="exampleInputPassword1" class="text-uppercase"><? echo $t->t( "neues Passwort" ); ?></label>
                                <input type="password" name="password" autocomplete="current-password" class="form-control" placeholder="">
                        </div>
                        <div class="form-group">
                                <label for="exampleInputPassword2" class="text-uppercase"><? echo $t->t( "neues Passwort wiederholen" ); ?></label>
                                <input type="password" name="password2" autocomplete="current-password" class="form-control" placeholder="">
                        </div>

                        <div class="form-check">
							<a href="#" style="float: right;" onClick="$(this).closest('form').submit()"><? $f->print_button( 'Passwort rücksetzen' ); ?></a>
							<input type="submit" value="" name="login_button" style="display: none;" />
                        </div>
                    </form>
                    <div class="copy-text">
                    		<a href="/<?php echo SUBDIR; ?>admin/login_login.php">zum Login</a><br>
                        <a href="http://www.impulsit.at" class="text-primary" target="_blank">www.impulsit.at</a><span> &vert; </span><span><a href="https://www.impulsit.at/impressum/" class="text-primary" target="_blank"><? echo $t->t( "Impressum" ); ?></a></span><span> &vert; </span><span><a href="https://www.impulsit.at/datenschutz/" class="text-primary" target="_blank"><? echo $t->t( "Datenschutz" ); ?></a></span>
                    </div>
                    <div class="text-primary float-right" style="font-size: 9px; position: absolute; bottom: 0px; right: 2px;">
                    	<?php echo str_replace( "VERSION", "", VERSION ); ?>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8 banner-sec">
               		<a href="http://www.impulsit.at" target="_blank" style="color: white; position: absolute; right: 15px; bottom: 10px; font-size: 32px; text-shadow: 4px 4px 10px white; text-decoration: none;">impuls Management</a>
	            </div>
    </section>
</body>
</html>
