<!DOCTYPE html>
<html lang="de">
<head>
	<title><? echo TITEL; ?></title>
	<meta charset="utf-8">
	<meta name="keywords" content="<? echo META; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- load favico -->
	<link rel="shortcut icon" href="<? echo $theme->get_favico(); ?>" />
	<!-- load javascript -->
	<script src="<?=DOMAIN_JS_DIR?>f.php?v=<? echo md5(time());?>"></script>
	<script type="text/javascript" src="<?=DOMAIN_JS_DIR?>tinymce_v4.9.2/js/tinymce/tinymce.min.js"></script>
	<!-- load css -->
	<link rel="stylesheet" href="<?=DOMAIN_CSS_DIR?>s.php?v=<? echo md5(time());?>">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<body onload="document_finished_loading();">
<div id="overlay"></div>
<?
if( isset( $c_user_id ) && ($c_user_id > 0) ) $main_url = "/".SUBDIR."admin/core_backend.php"; else $main_url = "";
?>
<div class="wrapper">
	<div id="header" style="height: 60px !important;">
		<div class="container">
			<nav class="navbar navbar-expand-lg">
					<img src="<? echo $theme->get_logo(); ?>" style="padding-top: 0px; padding-bottom: 0px; padding-left: 5px; max-width: 188px; height: 40px; max-height: 40px;">
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					  <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
					</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<div id="mobile-nav" style="float:right;">
						<? echo $f->get_left_menu(); ?>
					</div>
				</div>

			</nav>
		</div>
	</div>
	<div id="main-content">
		<div id="shop-container" class="container">
			<div class="d-flex flex-row">
				<div id="standard-nav" style="width: auto;">
				<?php
				echo $f->get_left_menu();
				?>
				</div>
			<div class="flex-fill">
			<div class="d-flex flex-row">
				<div class="col-md-12">
						<section id="list">
							<div class="window" style="display:none">
								<div class="content"></div>
							</div>
						</section>
						<div id="timeout_popup" style="display:none"></div>
						<div id="box1"></div>
						<!-- <div id="box1"> -->
						<?
							$mes->restoreMessages();
							echo $mes->printMessages();
							$mes->resetError();
						?>
						<input type="hidden" id="project_id" name="project_id" value="<?php echo (isset($_SESSION['project_id'])?$_SESSION['project_id']:0); ?>">
