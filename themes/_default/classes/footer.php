					</div> <!-- id=box -->
				<div id="shortcut_list" style="display: none;"></div>
				</div> <!-- class=col-md-12 -->
				</div>
				</div>
			</div> <!-- class=row -->
		</div>

		<?
		$system_info = '';
		if( isset( $_SESSION['c_user_id'] ) ) {
			$system_info = 'System Info <span> &vert; </span>';

			$setup = $f->load_setup( "CORE_SETUP" );
			$userinfo = $f->load_user( $_SESSION['c_user_id'], "user_level" );
			if( ($userinfo['user_level'] >= 990) && $setup['show_script_duration'] )
				$system_info .= 'Execution Time: '.round( (microtime(true) - $time_start), 4 ).'<span> &vert; </span>';

			$system_info .= 'PHP-Version: '.phpversion();

			$system_info .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		} // if
		?>

		<div class="footer">
			<?php echo $system_info; ?><a href="http://www.impulsit.at" target="_blank">www.impulsit.at</a><span> &vert; </span><span><a href="https://www.impulsit.at/impressum/" target="_blank">Impressum</a></span><span> &vert; </span><span><a href="https://www.impulsit.at/datenschutz/" target="_blank">Datenschutz</a></span> &vert; </span><span><a href="/<?php echo SUBDIR; ?>iit-tools/admin/login_logout.php" target="_blank">ausloggen</a></span>
		</div>
	</div>
</div> <!-- class=wrapper -->
</body>
</html>
