<?php
require_once( dirname(__FILE__).'/../../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );

class api_common {
	static private $instance = null;
	private $db = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_symbol_from_pair( $api, $pair ) {
		$this->db->query( "SELECT symbol FROM TRA_PAIR WHERE api='".$api."' AND pair='".$pair."'", "get_symbol_from_pair" );
		if( $r = $this->db->getNext( "get_symbol_from_pair" ) )
			return( $r['symbol'] );

		return( '' );
	} // get_symbol_from_pair

	function pair_allowed( $api, $pair ) {
		if( !$this->db->query( "SELECT pair FROM TRA_PAIR WHERE api='".$api."' AND pair='".$pair."'", "pair_allowed" ) )
			return( false );

		$r = $this->db->getNext( "pair_allowed" );
		if( $r['pair'] == '' )
			return( false );

		return( true );
	} // pair_allowed

} // api_common
