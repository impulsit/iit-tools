<?php
require_once( 'api/kraken_com.php' );
require_once( 'api/gate_io.php' );
require_once( 'api/huobi_pro.php' );
require_once( 'api/therocktrading_com.php' );
require_once( 'api/quadrigacx_com.php' );
require_once( 'api/hitbtc_com.php' );


$api = kraken_com::getInstance();
$api->get_exchange_rates();

$api = gate_io::getInstance();
$api->get_exchange_rates();

$api = huobi_pro::getInstance();
$api->get_exchange_rates();

$api = therocktrading_com::getInstance();
$api->get_exchange_rates();

$api = quadrigacx_com::getInstance();
$api->get_exchange_rates();

$api = hitbtc_com::getInstance();
$api->get_exchange_rates();
?>