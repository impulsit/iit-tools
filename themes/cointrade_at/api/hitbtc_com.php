<?php
class hitbtc_com_api {
	private $api_key = '';
	private $api_secret = '';

	function __construct( $key, $secret ) {
		$this->api_key = $key;
		$this->api_secret = $secret;
	}

	function get_ticker( $pair ) {
		$ch = curl_init('https://api.hitbtc.com/api/2/public/ticker/'.$pair);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		$result = curl_exec($ch);

		return( json_decode( $result ) );
	} // get_ticker
}

class hitbtc_com {
	static private $instance = null;
	private $db = null;
	private $api = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->api = api_common::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_exchange_rates() {
		$call_api = new hitbtc_com_api( '', '' );

		$time = date( "Y-m-d H:i:s", time() );
		$this->db->query( "SELECT pair FROM TRA_PAIR WHERE api='".get_class( $this )."' AND pair!=''" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$result = $call_api->get_ticker( $r['pair'] );
			if( $this->api->pair_allowed( get_class( $this ), $r['pair'] ) ) {
				$this->db->insert( "TRA_EXCHANGE_RATE", array(
						"timestamp" => $time,
						"symbol" => $this->api->get_symbol_from_pair( get_class( $this ), $r['pair'] ),
						"api" => get_class( $this ),
						"ask" => $result->ask,
						"bid" => $result->bid
				) );
			} // if
		} // while
	} // get_exchange_rates
}