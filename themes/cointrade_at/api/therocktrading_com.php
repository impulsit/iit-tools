<?php
class therocktrading_com_api {
	private $api_key = '';
	private $api_secret = '';

	function __construct( $key, $secret ) {
		$this->api_key = $key;
		$this->api_secret = $secret;
	}

	function get_tickers() {
		$url="https://api.therocktrading.com/v1/funds/tickers";

		$headers=array(
				"Content-Type: application/json"
		);

		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
		$callResult=curl_exec($ch);
		curl_close($ch);

		$result=json_decode($callResult,true);

		return( $result );
	} // get_tickers
}

class therocktrading_com {
	static private $instance = null;
	private $db = null;
	private $api = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->api = api_common::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_exchange_rates() {
		$call_api = new therocktrading_com_api( '', '' );

//		echo "<pre>";
//		print_r( $call_api->get_tickers() );
		/*
		$strPairs = '';
		$this->db->query( "SELECT pair FROM TRA_PAIR WHERE api='".get_class( $this )."'" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$strPairs .= $r['pair'].',';
		} // while
		$strPairs = substr( $strPairs, 0, -1 );

		$result = $call_api->QueryPublic( 'Ticker', array( 'pair' => $strPairs ) );
		$time = date( "Y-m-d H:i:s", time() );
		foreach( $result['result'] as $k => $v ) {
			if( $this->api->pair_allowed( get_class( $this ), $k ) ) {
				$this->db->insert( "TRA_EXCHANGE_RATE", array(
						"timestamp" => $time,
						"symbol" => $this->api->get_symbol_from_pair( get_class( $this ), $k ),
						"api" => get_class( $this ),
						"ask" => $v['a']['0'],
						"bid" => $v['b']['0']
				) );
			} // if
		} // foreach
		*/
	} // get_exchange_rates
}