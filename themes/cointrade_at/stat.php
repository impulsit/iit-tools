<?php
require_once( dirname(__FILE__).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );

$db = mysql::getInstance();

$db->query( "SELECT symbol, description FROM TRA_CURRENCY ORDER BY symbol", 1 );
while( $db->isNext( 1 ) ) {
	$c = $db->getNext( 1 );

	echo "<strong>".$c['symbol']."-".$c['description']."</strong><br />";

	$db->query( "SELECT api, ask FROM TRA_EXCHANGE_RATE WHERE symbol='".$c['symbol']."' ORDER BY ask", 2 );
	while( $db->isNext( 2 ) ) {
		$r = $db->getNext( 2 );

		echo $r['api']." ".$r['ask']."<br />";
	} // while

	// Min / Max
	$db->query( "SELECT api, ask FROM TRA_EXCHANGE_RATE WHERE symbol='".$c['symbol']."' AND ask=(SELECT MIN(ask) FROM TRA_EXCHANGE_RATE WHERE symbol='".$c['symbol']."')", 2 );
	$min = $db->getNext( 2 );
	$db->query( "SELECT api, ask FROM TRA_EXCHANGE_RATE WHERE symbol='".$c['symbol']."' AND ask=(SELECT MAX(ask) FROM TRA_EXCHANGE_RATE WHERE symbol='".$c['symbol']."')", 2 );
	$max = $db->getNext( 2 );

	echo "Min: ".$min['api']."... ".$min['ask']."<br />";
	echo "Max: ".$max['api']."... ".$max['ask']."<br />";
	echo "Diff: ".round( ($max['ask'] / $min['ask']-1)*100, 2)."<br />";


	echo "<hr>";
} // while
?>