<?php
ini_set( "memory_limit", "2048M" );

require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'PHPExcel/PHPExcel.php' );

$db = mysql::getInstance();
$f = functions::getInstance();

// läuft bereits
if( $db->query( "SELECT * FROM TEC_IMPORT_LOG WHERE start_import='1' AND finished='0' AND status='2' LIMIT 1", "main" ) )
	return;

// import
$db->query( "SELECT * FROM TEC_IMPORT_LOG WHERE start_import='1' AND finished='0' AND status='1' LIMIT 1", "main" );
while( $db->isNext( "main" ) ) {
	$r = $db->getNext( "main" );

	$db->update( "TEC_IMPORT_LOG", array( "status" => 2, "curr_line" => 0, "start_time" => $f->d( time() ) ), "id='".$r['id']."'" );
	$db->commit();

	// Status = 1
	$highestRow = 0;
	$row = 0;
	if( file_exists( BASE_DIR.'upload/'.$r['filename'] ) ) {
		$objPHPExcel = PHPExcel_IOFactory::load( BASE_DIR.'upload/'.$r['filename'] );

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		//$highestColumn = $sheet->getHighestColumn();

		$db->update( "TEC_IMPORT_LOG", array( "max_lines" => $highestRow ), "id='".$r['id']."'" );
		$db->commit();

		switch( $r['type'] ) {
			case 1:
					$db->query( "TRUNCATE TEC_ITEMS" );
					$db->query( "TRUNCATE TEC_ITEMS_CATEGORY" );
					$db->query( "TRUNCATE TEC_ITEMS_SUBCATEGORY" );
				break;
			case 2:
					$db->query( "TRUNCATE TEC_REPLACE_ITEMS" );
				break;
			case 4:
					$db->query( "TRUNCATE TEC_DATACONNECTOR" );
				break;
			} // switch

		// Status = 2
		for ($row = 4; $row <= $highestRow; $row++) {
			switch( $r['type'] ) {
				case 1: // Artikel
						$rowData = $sheet->rangeToArray( 'A'.$row.':AB'.$row, NULL, TRUE, FALSE );
						for( $i=0; $i<=27; $i++ ) $rowData[0][$i] = trim( str_replace( "'", "", $rowData[0][$i] ) );

						// Category
						$category_id = 0;
						if( $rowData[0][10] != '' ) {
							if( $db->query( "SELECT id FROM TEC_ITEMS_CATEGORY WHERE category LIKE '".$rowData[0][10]."'" ) ) {
								$r2 = $db->getNext();
								$category_id = $r2['id'];
							} else {
								$category_id = $db->insert( "TEC_ITEMS_CATEGORY", array ( "id" => '', "category" => $rowData[0][10] ) );
							} // else
						} // If

						// Subcategory
						$subcategory_id = 0;
						if( $rowData[0][11] != '' ) {
							if( $db->query( "SELECT id FROM TEC_ITEMS_SUBCATEGORY WHERE category_id='".$category_id."' AND subcategory LIKE '".$rowData[0][11]."'") ) {
								$r2 = $db->getNext();
								$subcategory_id = $r2['id'];
							} else {
								$subcategory_id = $db->insert( "TEC_ITEMS_SUBCATEGORY", array ( "category_id" => $category_id, "subcategory" => $rowData[0][11] ) );
							} // else
						} // If

						$db->insert( "TEC_ITEMS", array(
								"hersteller" => $rowData[0][0],
								"hersteller_nr" => $rowData[0][1],
								"interne_nr" => $rowData[0][2],
								"bemerkung" => $rowData[0][3],
								"preis" => $rowData[0][4],
								"lagerstand" => $rowData[0][5],
								"zusatzinfo" => $rowData[0][6],
								"description_2" => $rowData[0][7],
								"brand_id" => $rowData[0][8],
								"item_picture" => $rowData[0][9],
								"category_id" => $category_id,
								"subcategory_id" => $subcategory_id,
								"sorting" => $rowData[0][12],
								"description_3" => $rowData[0][13],
								"item_picture_2" => $rowData[0][14],
								"item_picture_3" => $rowData[0][15],
								"width" => $rowData[0][16],
								"height" => $rowData[0][17],
								"length" => $rowData[0][18],
								"diameter" => str_replace( ".", ",", $rowData[0][19] ),
								"viscosity" => $rowData[0][20],
								"kw" => $rowData[0][21],
								"ah" => $rowData[0][22],
								"supplier" => $rowData[0][23],
								"belt_shafts" => $rowData[0][24],
								"volt" => $rowData[0][25],
								"amper" => $rowData[0][26],
								"season" => $rowData[0][27],
						) );
					break;
				case 2: // Ersatzartikel
						$rowData = $sheet->rangeToArray( 'A'.$row.':E'.$row, NULL, TRUE, FALSE );
						for( $i=0; $i<=4; $i++ ) $rowData[0][$i] = trim( str_replace( "'", "", $rowData[0][$i] ) );

						$db->insert( "TEC_REPLACE_ITEMS", array(
								"hersteller" => $rowData[0][0],
								"hersteller_nr" => $rowData[0][1],
								"interne_nr" => $rowData[0][2],
								"ersatznr" => $rowData[0][3],
								"hersteller_ersatznr" => $rowData[0][4],
						) );
					break;
				case 3: // WebService Mapping
						$rowData = $sheet->rangeToArray( 'A'.$row.':E'.$row, NULL, TRUE, FALSE );
						for( $i=0; $i<=4; $i++ ) $rowData[0][$i] = trim( str_replace( "'", "", $rowData[0][$i] ) );

						$db->replace( "TEC_WEBSERVICE_MAPPING", array(
							"item_no" => $rowData[0][0],
							"brand_id" => $rowData[0][1],
							"local_item_no" => $rowData[0][2],
							"description" => $rowData[0][3],
							"a_id" => $rowData[0][4],
						) );
					break;
				case 4: // DataConnector
						$rowData = $sheet->rangeToArray( 'A'.$row.':C'.$row, NULL, TRUE, FALSE );
						for( $i=0; $i<=2; $i++ ) $rowData[0][$i] = trim( str_replace( "'", "", $rowData[0][$i] ) );

						$db->replace( "TEC_DATACONNECTOR", array(
								"hersteller_nr" => $rowData[0][0],
								"verbund_nr" => $rowData[0][1],
								"info" => $rowData[0][2],
						) );
					break;
				} // switch

			$db->update( "TEC_IMPORT_LOG", array( "status" => 2, "curr_line" => $row-1 ), "id='".$r['id']."'" );
			$db->commit();
		} // for
	} // if

	// Status = 3
	$db->update( "TEC_IMPORT_LOG", array( "status" => 3, "finished" => 1, "curr_line" => $row-1, "max_lines" => $highestRow, "end_time" => $f->d( time() ) ), "id='".$r['id']."'" );
	$db->commit();
} // while
?>
