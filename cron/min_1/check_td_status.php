<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'tecdoc.php' );

// cd /share/CACHEDEV1_DATA/Web/iit-tools/cron/min_1
// /mnt/ext/opt/apache/bin/php check_td_status.php

$db = mysql::getInstance();
$f = functions::getInstance();
$tecdoc = tecdoc::getInstance();

//$db->update( "CORE_JOBS", array( "running" => 0 ), "id='4'" );
//$db->commit();

// läuft bereits
if( $db->query( "SELECT * FROM CORE_JOBS WHERE description='Check TD Items' AND start='1' AND finished='0' AND running='1' LIMIT 1", "main" ) )
	return;

// start
$db->query( "SELECT * FROM CORE_JOBS WHERE description='Check TD Items' AND start='1' AND finished='0' AND running='0' LIMIT 1", "main" );
while( $db->isNext( "main" ) ) {
	$r = $db->getNext( "main" );

	$db->update( "CORE_JOBS", array( "counter" => 0, "running" => 1, "start_time" => $f->d( time() ), "status" => 'running...' ), "id='".$r['id']."'" );
	$db->commit();

	$i = 0;
	$max = $db->query( "SELECT brand_id, local_item_no, item_no FROM TEC_WEBSERVICE_MAPPING", "sub" );
	while( $db->isNext( "sub" ) ) {
		$r2 = $db->getNext( "sub" );

		$i++;

		// Prüfe Artikel
		$check_item_no = trim( str_replace( "'", "", $r2['item_no'] ) );

		$bFound = $tecdoc->check_item_no( $check_item_no, $r2['brand_id'] );
		if( $bFound ) $str = 'TecDocOK'; else $str = 'TecDocNOTOK';

		$db->update( "TEC_WEBSERVICE_MAPPING", array( "td_status" => $str ), "item_no='".$check_item_no."' AND brand_id='".$r2['brand_id']."'" );
		$db->update( "CORE_JOBS", array( "status" => 'working '.$i.'/'.$max.' ('.round( $i/$max*100, 2 ).'%)', "counter" => $i ), "id='".$r['id']."'" );
		$db->commit();
	} // while

	$db->update( "CORE_JOBS", array( "finished" => 1, "running" => 0, "end_time" => $f->d( time() ), "status" => 'finished' ), "id='".$r['id']."'" );
	$db->commit();
} // while
?>
