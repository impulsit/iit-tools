<?php
require_once( dirname( __FILE__ ).'/classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$f = functions::getInstance();

$db->query( "SELECT * FROM SETUP WHERE id='1'" );
$setup = $db->getNext();

$db->query( "SELECT * FROM LOG_PAYMENT_CALL WHERE finished=0 AND error=0", "ha_paypal" );
while( $db->isNext( "ha_paypal" ) ) {
	$r = $db->getNext( "ha_paypal" );

	$anzahl = -1;
	for( $i=1; $i<=3; $i++ ) {
		if( $r['amount'] == ($setup['preis_'.$i]*100) ) {
			$anzahl = $setup['anzahl_'.$i];
			$preis =  $setup['preis_'.$i];
		} // if
	} // for

	if( $anzahl == -1 ) {
		$db->update( "LOG_PAYMENT_CALL", array( "error" => 1, "error_text" => 'Betrag nicht gefunden', "finished" => 1 ), "id='".$r['id']."'" );
	} else {
		$db->insert( "LOG_PAYMENT_DETAIL", array(
			"user_id" => $r['user_id'],
			"anzahl" => $anzahl,
			"preis" => $preis,
			"zeit" => date( "Y-m-d H:i:s", time() ),
			"typ" => AKTION_EINKAUF
		) );
		$db->query( "UPDATE CORE_USER_INFO SET rest_anzahl=rest_anzahl+".$anzahl." WHERE user_id='".$r['user_id']."'" );
		$db->update( "LOG_PAYMENT_CALL", array( "error" => 0, "error_text" => '', "finished" => 1 ), "id='".$r['id']."'" );
	} // if
	$db->commit();
} // while
?>