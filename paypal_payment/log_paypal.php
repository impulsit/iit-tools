<?php
if( !isset( $_POST['item_name'] ) )
	exit;

require_once( dirname( __FILE__ ).'/../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );

$db = mysql::getInstance();

// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';

foreach ($_POST as $key => $value) {
	$value = urlencode(stripslashes($value));
	$req .= "&$key=$value";
}

// post back to PayPal system to validate
$header  = "";
$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Host: www.paypal.com\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n";
$header .= "Connection: close\r\n\r\n";
$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);

$bFinished = false;

// assign posted variables to local variables
$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_amount = $_POST['mc_gross'] * 100;
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];

// FB
$user_id = $_POST['option_selection1'];
$typ = $_POST['option_selection2'];

$strPost = "POST:\n";
$arr = $_POST;
foreach( $arr as $v => $k)
	$strPost .= $v.":".$k."\n";
if( isset( $_GET ) ) {
	$strPost .= "GET:\n";
	$arr = $_GET;
	foreach( $arr as $v => $k)
		$strPost .= $v.":".$k."\n";
} // if

if (!$fp) {
	// HTTP Error
	$db->insert( "LOG_PAYMENT_CALL",
		array(
		  "createtime" => date( "Y-m-d H:i:s", time() ),
		  "amount" => $payment_amount,
		  "title" => "HTTP ERROR",
		  "auth" => $txn_id,
		  "payer_email" => $payer_email,
		  "receiver_email" => $receiver_email,
		  "currency" => $payment_currency,
		  "status" => $payment_status,
		  "item_name" => $item_name,
		  "finished" => 1,
		  "error" => 1,
		  "error_text" => "http error",
		  "typ" => $typ,
		  "user_id" => $user_id,
		  "POST" => $strPost,
		) );
	$db->commit();
} else {
	fputs ($fp, $header . $req);
	while (!feof($fp)) {
		$res = fgets ($fp, 1024);
		if (strcmp ($res, "VERIFIED") == 0) {
			// check the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
			$db->insert( "LOG_PAYMENT_CALL",
				array(
				  "createtime" => date( "Y-m-d H:i:s", time() ),
				  "amount" => $payment_amount,
				  "title" => "Freischalten: User ".$user_id,
				  "auth" => $txn_id,
				  "payer_email" => $payer_email,
				  "receiver_email" => $receiver_email,
				  "currency" => $payment_currency,
				  "status" => $payment_status,
				  "item_name" => $item_name,
				  "finished" => 0,
					"typ" => $typ,
					"user_id" => $user_id,
					"POST" => $strPost,
				) );
			$db->commit();

			$bFinished = true;
		} else if (strcmp ($res, "INVALID") == 0) {
			// log for manual investigation
			$db->insert( "LOG_PAYMENT_CALL",
				array(
				  "createtime" => date( "Y-m-d H:i:s", time() ),
				  "amount" => $payment_amount,
				  "title" => "MANUAL MODIFICATION",
				  "auth" => $txn_id,
				  "payer_email" => $payer_email,
				  "receiver_email" => $receiver_email,
				  "currency" => $payment_currency,
				  "status" => $payment_status,
				  "item_name" => $item_name,
				  "finished" => 1,
				  "error" => 1,
				  "error_text" => "manual modification",
					"typ" => $typ,
					"user_id" => $user_id,
					"POST" => $strPost,
				) );
			$db->commit();
		}
	} // while
	fclose ($fp);
} // else

if( $bFinished )
	include( dirname( __FILE__ ).'/paypal_finished.php' );
?>