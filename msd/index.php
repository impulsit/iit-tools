<?php
require_once( dirname( __FILE__ )."/../classes/config_data.php" );
require_once( dirname( __FILE__ )."/../classes/basis.php" );

$userinfo = $f->load_user( $_SESSION['c_user_id'], "user_level" );
if( $userinfo['user_level'] < 990 ) {
	exit;
} // if

$arr = array( "/work", "/work/backup", "/work/config", "/work/log" );
foreach( $arr as $k => $v ) {
	$path = dirname( __FILE__ ).$v;
	if( !file_exists( $path ) )
		mkdir( $path );
} // foreach

// Config Files anpassen
$conf = '<?php
#Vars - written at 2017-11-22
$dbhost="localhost";
$dbname="'.MYSQL_DB.'";
$dbuser="'.MYSQL_USER.'";
$dbpass="'.str_replace( "$", "\\$", MYSQL_PASSWORD ).'";
$dbport=3306;
$dbsocket="";
$compression=1;
$backup_path="'.BASE_DIR.'msd/work/backup/";
$logdatei="'.BASE_DIR.'msd/work/log/mysqldump_perl.log.gz";
$completelogdatei="'.BASE_DIR.'msd/work/log/mysqldump_perl.complete.log.gz";
$sendmail_call="/usr/lib/sendmail -t -oi -oem";
$nl="\n";
$cron_dbindex=0;
$cron_printout=1;
$cronmail=0;
$cronmail_dump=0;
$cronmailto="";
$cronmailto_cc="";
$cronmailfrom="";
$cron_use_sendmail=1;
$cron_smtp="localhost";
$cron_smtp_port="25";
@cron_db_array=("'.MYSQL_DB.'");
@cron_dbpraefix_array=("","","","","","","","","","","","","","","","","","","");
@cron_command_before_dump=("","","","","","","","","","","","","","","","","","","");
@cron_command_after_dump=("","","","","","","","","","","","","","","","","","","");
@ftp_server=("","","");
@ftp_port=(21,21,21);
@ftp_mode=(0,0,0);
@ftp_user=("","","");
@ftp_pass=("","","");
@ftp_dir=("/","/","/");
@ftp_timeout=(30,30,30);
@ftp_useSSL=(0,0,0);
@ftp_transfer=(0,0,0);
$mp=0;
$multipart_groesse=0;
$email_maxsize=3145728;
$auto_delete=1;
$max_backup_files=10;
$perlspeed=10000;
$optimize_tables_beforedump=0;
$logcompression=1;
$log_maxsize=1048576;
$complete_log=1;
$my_comment="";
?>';
$file = file_put_contents( dirname( __FILE__ )."/work/config/mysqldumper.conf.php", $conf );

$conf = '<?php
$config[\'ignore_enable_keys\'] = \'0\';
$config[\'language\'] = \'de\';
$config[\'dbhost\'] = \'localhost\';
$config[\'dbuser\'] = \''.MYSQL_USER.'\';
$config[\'dbpass\'] = \''.MYSQL_PASSWORD.'\';
$config[\'dbport\'] = \'3306\';
$config[\'dbsocket\'] = \'\';
$config[\'manual_db\'] = \''.MYSQL_DB.'\';
$config[\'minspeed\'] = \'100\';
$config[\'maxspeed\'] = \'50000\';
$config[\'theme\'] = \'msd_green\';
$config[\'interface_server_caption\'] = \'1\';
$config[\'interface_server_captioncolor\'] = \'#ff9966\';
$config[\'interface_server_caption_position\'] = \'0\';
$config[\'interface_sqlboxsize\'] = \'70\';
$config[\'interface_table_compact\'] = \'0\';
$config[\'memory_limit\'] = \'100000\';
$config[\'compression\'] = \'1\';
$config[\'processlist_refresh\'] = \'3000\';
$config[\'empty_db_before_restore\'] = \'0\';
$config[\'optimize_tables_beforedump\'] = \'0\';
$config[\'stop_with_error\'] = \'1\';
$config[\'send_mail\'] = \'0\';
$config[\'send_mail_dump\'] = \'0\';
$config[\'email_recipient\'] = \'\';
$config[\'email_recipient_cc\'] = \'\';
$config[\'email_sender\'] = \'\';
$config[\'email_maxsize1\'] = \'3\';
$config[\'email_maxsize2\'] = \'2\';
$config[\'ftp_transfer\']=array();
$config[\'ftp_transfer\'][0] = \'0\';
$config[\'ftp_transfer\'][1] = \'0\';
$config[\'ftp_transfer\'][2] = \'0\';
$config[\'ftp_timeout\']=array();
$config[\'ftp_timeout\'][0] = \'30\';
$config[\'ftp_timeout\'][1] = \'30\';
$config[\'ftp_timeout\'][2] = \'30\';
$config[\'ftp_useSSL\']=array();
$config[\'ftp_useSSL\'][0] = \'0\';
$config[\'ftp_useSSL\'][1] = \'0\';
$config[\'ftp_useSSL\'][2] = \'0\';
$config[\'ftp_mode\']=array();
$config[\'ftp_mode\'][0] = \'0\';
$config[\'ftp_mode\'][1] = \'0\';
$config[\'ftp_mode\'][2] = \'0\';
$config[\'ftp_server\']=array();
$config[\'ftp_server\'][0] = \'\';
$config[\'ftp_server\'][1] = \'\';
$config[\'ftp_server\'][2] = \'\';
$config[\'ftp_port\']=array();
$config[\'ftp_port\'][0] = \'21\';
$config[\'ftp_port\'][1] = \'21\';
$config[\'ftp_port\'][2] = \'21\';
$config[\'ftp_user\']=array();
$config[\'ftp_user\'][0] = \'\';
$config[\'ftp_user\'][1] = \'\';
$config[\'ftp_user\'][2] = \'\';
$config[\'ftp_pass\']=array();
$config[\'ftp_pass\'][0] = \'\';
$config[\'ftp_pass\'][1] = \'\';
$config[\'ftp_pass\'][2] = \'\';
$config[\'ftp_dir\']=array();
$config[\'ftp_dir\'][0] = \'/\';
$config[\'ftp_dir\'][1] = \'/\';
$config[\'ftp_dir\'][2] = \'/\';
$config[\'multi_part\'] = \'0\';
$config[\'multipartgroesse1\'] = \'0\';
$config[\'multipartgroesse2\'] = \'0\';
$config[\'multipart_groesse\'] = \'0\';
$config[\'auto_delete\'] = \'1\';
$config[\'max_backup_files\'] = \'10\';
$config[\'cron_perlpath\'] = \'C:/wamp/bin/perl\';
$config[\'cron_use_sendmail\'] = \'1\';
$config[\'cron_sendmail\'] = \'/usr/lib/sendmail -t -oi -oem\';
$config[\'cron_smtp\'] = \'localhost\';
$config[\'cron_smtp_port\'] = \'25\';
$config[\'cron_extender\'] = \'0\';
$config[\'cron_compression\'] = \'1\';
$config[\'cron_printout\'] = \'1\';
$config[\'cron_completelog\'] = \'1\';
$config[\'cron_comment\'] = \'\';
$config[\'multi_dump\'] = \'0\';
$config[\'logcompression\'] = \'1\';
$config[\'log_maxsize1\'] = \'1\';
$config[\'log_maxsize2\'] = \'2\';
$config[\'log_maxsize\'] = \'1048576\';
$config[\'cron_dbindex\'] = \'4\';
$config[\'email_maxsize\'] = \'3145728\';
$config[\'cron_execution_path\'] = \'msd_cron/\';
$config[\'sql_limit\'] = \'30\';
$config[\'bb_width\'] = \'300\';
$config[\'bb_textcolor\'] = \'#000000\';
$databases[\'Name\']=array();
$databases[\'Name\'][0] = \''.MYSQL_DB.'\';
$databases[\'Name\'][1] = \'information_schema\';
$databases[\'praefix\']=array();
$databases[\'praefix\'][0] = \'\';
$databases[\'praefix\'][1] = \'\';
$databases[\'praefix\'][2] = \'\';
$databases[\'praefix\'][3] = \'\';
$databases[\'praefix\'][4] = \'\';
$databases[\'praefix\'][5] = \'\';
$databases[\'praefix\'][6] = \'\';
$databases[\'praefix\'][7] = \'\';
$databases[\'praefix\'][8] = \'\';
$databases[\'praefix\'][9] = \'\';
$databases[\'praefix\'][10] = \'\';
$databases[\'praefix\'][11] = \'\';
$databases[\'praefix\'][12] = \'\';
$databases[\'praefix\'][13] = \'\';
$databases[\'praefix\'][14] = \'\';
$databases[\'praefix\'][15] = \'\';
$databases[\'praefix\'][16] = \'\';
$databases[\'praefix\'][17] = \'\';
$databases[\'praefix\'][18] = \'\';
$databases[\'praefix\'][19] = \'\';
$databases[\'command_before_dump\']=array();
$databases[\'command_before_dump\'][0] = \'\';
$databases[\'command_before_dump\'][1] = \'\';
$databases[\'command_before_dump\'][2] = \'\';
$databases[\'command_before_dump\'][3] = \'\';
$databases[\'command_before_dump\'][4] = \'\';
$databases[\'command_before_dump\'][5] = \'\';
$databases[\'command_before_dump\'][6] = \'\';
$databases[\'command_before_dump\'][7] = \'\';
$databases[\'command_before_dump\'][8] = \'\';
$databases[\'command_before_dump\'][9] = \'\';
$databases[\'command_before_dump\'][10] = \'\';
$databases[\'command_before_dump\'][11] = \'\';
$databases[\'command_before_dump\'][12] = \'\';
$databases[\'command_before_dump\'][13] = \'\';
$databases[\'command_before_dump\'][14] = \'\';
$databases[\'command_before_dump\'][15] = \'\';
$databases[\'command_before_dump\'][16] = \'\';
$databases[\'command_before_dump\'][17] = \'\';
$databases[\'command_before_dump\'][18] = \'\';
$databases[\'command_before_dump\'][19] = \'\';
$databases[\'command_after_dump\']=array();
$databases[\'command_after_dump\'][0] = \'\';
$databases[\'command_after_dump\'][1] = \'\';
$databases[\'command_after_dump\'][2] = \'\';
$databases[\'command_after_dump\'][3] = \'\';
$databases[\'command_after_dump\'][4] = \'\';
$databases[\'command_after_dump\'][5] = \'\';
$databases[\'command_after_dump\'][6] = \'\';
$databases[\'command_after_dump\'][7] = \'\';
$databases[\'command_after_dump\'][8] = \'\';
$databases[\'command_after_dump\'][9] = \'\';
$databases[\'command_after_dump\'][10] = \'\';
$databases[\'command_after_dump\'][11] = \'\';
$databases[\'command_after_dump\'][12] = \'\';
$databases[\'command_after_dump\'][13] = \'\';
$databases[\'command_after_dump\'][14] = \'\';
$databases[\'command_after_dump\'][15] = \'\';
$databases[\'command_after_dump\'][16] = \'\';
$databases[\'command_after_dump\'][17] = \'\';
$databases[\'command_after_dump\'][18] = \'\';
$databases[\'command_after_dump\'][19] = \'\';
$databases[\'db_selected_index\'] = \'0\';
$databases[\'db_actual\'] = \''.MYSQL_DB.'\';
$databases[\'multi\']=array();
$databases[\'multi_praefix\']=array();
$databases[\'multisetting\'] = \'\';
$databases[\'multi_commandbeforedump\']=array();
$databases[\'multi_commandafterdump\']=array();
$databases[\'multisetting_praefix\'] = \'\';
$databases[\'multisetting_commandbeforedump\'] = \'\';
$databases[\'multisetting_commandafterdump\'] = \'\';
$databases[\'db_actual_tableselected\'] = \'\';
$databases[\'cron_dbindex\'] = \'-3\';
$databases[\'db_actual_cronindex\'] = \'0\';
?>
';
$file = file_put_contents( dirname( __FILE__ )."/work/config/mysqldumper.php", $conf );


//--------------------------------------------------------------------------------

if (!@ob_start("ob_gzhandler")) @ob_start();
include ('./inc/functions.php');
$page=(isset($_GET['page'])) ? $_GET['page'] : 'main.php';
if (!file_exists("./work/config/mysqldumper.php"))
{
	header("location: install.php");
	ob_end_flush();
	die();
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
        "http://www.w3.org/TR/html4/frameset.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Author" content="Daniel Schlichtholz">
<title>MySQLDumper</title>
</head>

<frameset border=0 cols="190,*">
	<frame name="MySQL_Dumper_menu" src="menu.php" scrolling="no" noresize
		frameborder="0" marginwidth="0" marginheight="0">
	<frame name="MySQL_Dumper_content" src="<?php
	echo $page;
	?>"
		scrolling="auto" frameborder="0" marginwidth="0" marginheight="0">
</frameset>
</html>
<?php
ob_end_flush();
