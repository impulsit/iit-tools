@echo off

REM ---
REM --- INIT
REM ---
set _msd_dir=C:\_www\msd\work\backup
set _path_googledrive="C:\Users\Speedy\Google Drive\_backup"

for /f "usebackq delims=" %%i in (`dir /a /o-d /b %_msd_dir%`) do (
  set _latest_file=%%i
  goto cont
)

:cont
echo Latest Files: %LETESTDIR%

echo Kopieren von %_msd_dir%\%_latest_file%
echo nach 
echo %_path_googledrive%

if not exist %_path_googledrive% mkdir %_path_googledrive%
xcopy /Y %_msd_dir%\%_latest_file% %_path_googledrive%  > nul

pause