<?php
header( "Content-Type: text/css" );

require_once ("../classes/config_data.php");
require_once (CLASS_DIR . 'theme_functions.php');
require_once( CLASS_DIR.'mysql.php' );

if( !isset( $_SESSION ) ) session_start();

$theme = theme_functions::getInstance();
$db = mysql::getInstance();

$db->query( "SELECT version_type, color_theme, color_theme_external_css FROM CORE_SETUP WHERE id='1'" );
$setup = $db->getNext();

switch( $setup['version_type'] ) {
	case 0:
			// JQuery 2.2.1, Bootstrap 3.3.6, JQuery-UI 1.11.4, Chosen 1.8.3, Timepicker 1.3
			include ("../js/bootstrap_v3.3.6/css/bootstrap.min.css");
			include ("../js/jquery-ui_v1.11.4/jquery-ui.css");
		break;
	case 1:
			// JQuery 3.2.1, Bootstrap 4.0.0-beta, JQuery-UI 1.12.1, Chosen 1.8.2, Timepicker 1.6.3
			include ("../js/bootstrap_v4.0.0-beta/css/bootstrap.min.css");
			include ("../js/jquery-ui_v1.12.1/jquery-ui.css");
		break;
	case 2:
			// JQuery 3.3.1, Bootstrap 4.1.3, JQuery-UI 1.12.1, Chosen 1.8.3, Timepicker 1.6.3
			include ("../js/bootstrap_v4.1.3/css/bootstrap.min.css");
			include ("../js/jquery-ui_v1.12.1/jquery-ui.min.css");
		break;
} // switch

include ("../js/chosen_v1.8.3/chosen.min.css");
include ("../js/jquery-ui-timepicker-addon_v1.6.3/jquery-ui-timepicker-addon.css");
include ("../js/fine-uploader_v5.16.2/fine-uploader-new.min.css");
include ("../js/tablesorter_v2.0/css/theme.iit-tools.css" );

// Load Theme Style
echo "/* Theme _default */";
include ("../themes/_default/css/style.php");

// Load Theme Style - Indiv
echo "/* Theme base: ".$theme->get_theme_style('base')." */";
if( file_exists( $theme->get_theme_style('base') ) ) include ($theme->get_theme_style('base')); // Get Base Theme
echo "/* Theme location: ".$theme->get_theme_style('location')." */";
if( file_exists( $theme->get_theme_style('location') ) ) include ($theme->get_theme_style('location')); // Get Location Theme

// Load Color Theme
switch( $setup['color_theme'] ) {
	/*
	case 0: break;
		include ("../themes/_default/css/color_themes/color_theme__blue.css");
		break;
	case 1: // red
		include ("../themes/_default/css/color_themes/color_theme__red.css");
		break;
	case 2: // grey
		include ("../themes/_default/css/color_themes/color_theme__grey.css");
		break;
		*/
	case 100: // external css
		$external_css = @file_get_contents( $setup['color_theme_external_css'] );
		echo $external_css;
		break;
	default:
			$file = glob( BASE_DIR.'themes/_default/css/color_themes/color_theme__'.str_pad( $setup['color_theme'], 3, '0', STR_PAD_LEFT ).'*.css' );
			$theme = basename( $file[0] );
			include( '../themes/_default/css/color_themes/'.$theme );
		break;
}
?>