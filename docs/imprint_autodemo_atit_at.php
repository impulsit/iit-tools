<?php
require_once( dirname( __FILE__ )."/../classes/config_data.php" );
require_once( CLASS_DIR."templates/header.php" );

?>
<div id="content_scroll">
<h2>OFFENLEGUNG GEMÄSS &5 E-COMMERCE-GESETZ</h2>
Medieninhaber, Herausgeber:<br>
at iT Informationstechnologie GmbH<br>
1130 Wien, Hermesstrasse 22<br>
Österreich<br>
<br>
Telefon: +43 1 890 29 56<br>
e-Mail: office(et)atit.at<br>
<br>
Geschäftsführer Richard Stubenvoll<br>
<br>
Firmenbuch FN 131.447f<br> 
Handelsgericht Wien<br>
UID-Nummer ATU38395303<br>
<br>
Bankinstitut Raiffeisenbank Wienerwald<br>
IBAN: AT363266700000238212<br>
BIC: RLNWATWWPRB<br>
<br>
Gewerbe Dienstleistungen in der automatischen Datenverarbeitung,<br>
Unternehmensberatung<br>
Redaktion Richard Stubenvoll<br>
<br>
Diese Seite ist eine Webapplikation, erstellt und gehostet für die Firma<br> 
Elpida d.o.o.<br>
Dubička bb<br>
78000 Banja Luka<br>
<br>
<a href="/docs/agb_en_elpida_atit_at.pdf">Allgemeine Geschäftsbedingungen</a><br>
<br>
<h2>Imprint</h2>
at iT Informationstechnologie GmbH<br>
1130 Wien, Hermesstrasse 22<br>
Austria<br>
<br>
Phone: +43 1 890 29 56<br>
e-Mail: office(et)atit.at<br>
<br>
Managing Director Richard Stubenvoll<br>
<br>
Registration No. FN 131.447f<br>
Register Court Vienna Innere Stadt<br>
VAT-ID ATU38395303<br>
<br>
Bank Raiffeisenbank Wienerwald<br>
IBAN: AT363266700000238212<br>
BIC: RLNWATWWPRB<br>
<br>
Responsible for the content Richard Stubenvoll<br>
<br>
This site is a web application created and hosted for the company<br> 
Elpida d.o.o.<br>
Dubička bb<br>
78000 Banja Luka<br>
<br>
<h2>Impresum</h2>
at iT Informationstechnologie GmbH<br>
1130 Wien, Hermesstrasse 22<br>
Austria<br>
<br>
Tel: +43 1 890 29 56<br>
e-Mail: office(et)atit.at<br>
<br>
Izvršni direktor Richard Stubenvoll<br>
<br>
Registration No. FN 131.447f<br>
Register Court Vienna Innere Stadt<br>
VAT-ID ATU38395303<br>
<br>
Bankarski institut Raiffeisenbank Wienerwald<br>
IBAN: AT363266700000238212<br>
BIC: RLNWATWWPRB<br>
<br>
Odgovoran za sadržaj Richard Stubenvoll<br>
<br>
Ova stranica je web-aplikacija napravljena za kompaniju<br> 
Elpida d.o.o.<br>
Dubička bb<br>
78000 Banja Luka<br>


</div>
<?
require_once( CLASS_DIR."templates/footer.php" );
?>