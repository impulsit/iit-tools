--------------------------------------------------------------------------------
COPYRIGHT - Urheberrechtshinweis
--------------------------------------------------------------------------------

Die Software steht unter Lizenz der Closed Software.

Alle Inhalte dieses Softwareangebotes, insbesondere Quellcode, spezielle Klassen
und Texte, sind urheberrechtlich geschuetzt (Copyright).
Das Urheberrecht liegt, soweit nicht ausdruecklich anders gekennzeichnet, bei
iiT impuls IT (http://www.impulsIT.at).
Eine Aenderung des Quellcodes ist ausdruecklich verboten.

Wer gegen das Urheberrecht verstoesst (z.B. die Inhalte unerlaubt kopiert), macht
sich gemaeß dem Urhebergesetz strafbar.
Er wird zudem kostenpflichtig abgemahnt und muss Schadensersatz leisten.

01.01.2015

--------------------------------------------------------------------------------
Michael Sordje
iiT impuls IT e.U.
Parkgasse 40
2443 Deutsch Brodersdorf
--------------------------------------------------------------------------------
Tel             : 0650 / 728 30 43
Web             : http://www.impulsIT.at
E-Mail          : office@impulsIT.at
Firmenbuchnummer: 404541p
UID-Nr.         : AT U67739478
--------------------------------------------------------------------------------
Informationspflicht nach Par. 5 Abs. 1 E-Commerce-Gesetz:
https://firmen.wko.at/Web/DetailsInfos.aspx?FirmaID=76bff65c-645d-48f4-9788-544ccd33a987
--------------------------------------------------------------------------------