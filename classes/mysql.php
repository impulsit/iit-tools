<?php
require_once( dirname( __FILE__ )."/config_data.php" );

class mysql {
	static private $instance = null;

	public $conn;
	private $result;
	private $count;

	public $db_server = "";
	public $db_user = "";
	public $db_password = "";
	public $db_database = "";

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}

		return self::$instance;
	} // getInstance

	public function __construct( $server = "", $user = "", $password = "", $database = "" ) {
		if( $server != "" ) {
			$this->db_server = $server;
			$this->db_user = $user;
			$this->db_password = $password;
			$this->db_database = $database;
		} // if

		$this->conn = null;
		$this->result = array ();
		$this->count = array ();

		mt_srand((double) microtime()*1000000);

		$this->query( "SET AUTOCOMMIT=0" );
	}

	private function __clone(){
	} // __clone

	public function __destruct() {
	}

	function get_connection_vars( &$server, &$user, &$password, &$database ) {
		if( $this->db_server != "" ) $server = $this->db_server; else $server = MYSQL_SERVER;
		if( $this->db_user != "" ) $user = $this->db_user; else $user = MYSQL_USER;
		if( $this->db_password != "" ) $password = $this->db_password; else $password = MYSQL_PASSWORD;
		if( $this->db_database != "" ) $database = $this->db_database; else $database = MYSQL_DB;
	} // get_connection_vars

	private function connect() {
		if ( $this->conn )
			return true;

		$this->get_connection_vars( $server, $user, $password, $database );

		$this->conn = mysqli_connect( $server, $user, $password );

		if ( $this->conn == false ) {
			trigger_error( "No connection to sql-server", E_USER_ERROR );
			return false;
		}

		$i = $this->selectDB( $database );

		mysqli_autocommit( $this->conn, false );
		mysqli_query( $this->conn, "SET NAMES 'utf8'" );

		return $i;
	}

	public function selectDB( $database ) {
		$this->db_database = $database;

		if ( !@mysqli_select_db( $this->conn, $database ) ) {
			trigger_error( "Cannot choose database", E_USER_ERROR );
			return false;
		}

		return true;
	}

	public function commit() {
		$this->connect();

		mysqli_commit( $this->conn );
	}

	public function begin() {
		$this->connect();
		$this->query( "BEGIN", 'begin' );
	}

	public function rollback() {
		$this->connect();

		mysqli_rollback( $this->conn );
	}

	public function log_error( $query, $error ) {
		$query = trim( $query );
		$query = preg_replace( "/[\n\r]+/", "", $query );
		$query = preg_replace( "/[\t]+/", " ", $query );

		global $c_user_id;

		$trace = debug_backtrace();
		$str_trace = print_r( $trace, true );
		$file = $trace[sizeof( $trace )-1]['file'];
		$file = str_replace( BASE_DIR, '', $file );

		if( !isset( $c_user_id ) || ($c_user_id == 0) || ($c_user_id == "") ) $c_user_id = 0;

		$detail = array(
			"query" => $query,
			"error" => $error,
			"file" => $file
		);
		$error = 'SQL';

		include( dirname( __FILE__ ).'/templates/error.php' );
	} // log_error

	public function query( $query, $nummer = 0 ) {
		$this->last = $query;
		$this->connect();

		$this->result[$nummer] = @mysqli_query( $this->conn, $query );

		$upperCaseQuery = strtoupper( trim( $query ) );
		if ( ( $this->result[$nummer] === false ) ) {
			$error = mysqli_error( $this->conn );

			if( $error != "" ) {
				$this->log_error( $query, $error );
				return;
			} // if
		}

		if( is_object( $this->result[$nummer] ) )
			$count = @mysqli_num_rows( $this->result[$nummer] );

		if( !isset( $count) || !$count ) {
			unset ( $this->result[$nummer] );
			$this->count[$nummer] = 0;
		} else
			$this->count[$nummer] = $count;
		if( !isset( $count) || !$count )
			$count = intval( @mysqli_affected_rows( $this->conn ) );

		return $count;
	}

	public function insert( $tabelle, $array, $getID = true ) {
		$this->connect();
		foreach ( $array as $key => $value ) {
			$array[mysqli_real_escape_string( $this->conn, $key )] = mysqli_real_escape_string( $this->conn, $value );
			if ( $key !== mysqli_real_escape_string( $this->conn, $key ) )
				unset ( $array[$key] );
		}
		$query = 'INSERT INTO ' . $tabelle . " (" . implode( ", ", array_keys( $array ) ) . ") VALUES ('" . implode( "', '", $array ) . "')";
		if ( $getID ) {
			$this->query( $query, 'insert' );
			$returnid = @mysqli_insert_id( $this->conn );
			return $returnid;
		} else {
			$this->query( $query, 'insert' );
			return true;
		}
	}

	public function replace( $tabelle, $array ) {
		$this->connect();
		foreach ( $array as $key => $value ) {
			$array[mysqli_real_escape_string( $this->conn, $key )] = mysqli_real_escape_string( $this->conn, $value );
			if ( $key !== mysqli_real_escape_string( $this->conn, $key ) )
				unset ( $array[$key] );
		}
		$query = 'REPLACE INTO ' . $tabelle . " (" . implode( ", ", array_keys( $array ) ) . ") VALUES ('" . implode( "', '", $array ) . "')";
		$this->query( $query, 'replace' );
		return true;
	}

	public function update( $tabelle, $array, $condition = '1' ) {
		$this->connect();
		foreach ( $array as $key => $value ) {
			$array[] = mysqli_real_escape_string( $this->conn, $key ) . ' = \'' . mysqli_real_escape_string( $this->conn, $value ) . "'";
			unset ( $array[$key] );
		}
		$query = 'UPDATE ' . $tabelle . ' SET ' . implode( ", ", $array ) . ' WHERE ' . $condition;

		return $this->query( $query, 'update' );
	}

	public function isNext( $nummer = 0 ) {
		if ( !isset ( $this->count[$nummer] ) || ( $this->count[$nummer] <= 0 ) ) {
			if( isset( $this->result[$nummer] ) ) {
				@mysqli_free_result( $this->result[$nummer] );
				unset ( $this->result[$nummer] );
			} // if
			return false;
		} else {
			return true;
		}
	}

	public function getNext( $nummer = 0 ) {
		if( !isset( $this->count[$nummer] ) ) {
			$this->log_error( $this->last, "getNext Error: Index not found." );
			return;
		} // if

		$this->count[$nummer]--;

		if( $this->count[$nummer] < 0 ) {
			if( isset( $this->result[$nummer] ) ) {
				@mysqli_free_result( $this->result[$nummer] );
				unset ( $this->result[$nummer] );
				unset ( $this->count[$nummer] );

				$this->log_error( $this->last, "getNext Error: ..." );
				return;
			} // if
		}
		if( isset( $this->result[$nummer] ) )
			return @mysqli_fetch_assoc( $this->result[$nummer] );

		return false;
	}

	public function getAll( $nummer = 0 ) {
		$i = 0;
		$return = array ();
		while ( $return[$i ++] = @mysqli_fetch_assoc( $this->result[$nummer] ) );
		@mysqli_free_result( $this->result[$nummer] );
		unset ( $this->result[$nummer] );
		array_pop( $return );
		return $return;
	}

	public function getSingle( $nummer = 0 ) {
		return @reset( @reset( $this->getAll( $nummer ) ) );
	}

	public function delete( $table, $bedingung ) {
		$this->connect();

		$query = 'DELETE FROM ' . $table . ' WHERE ' . $bedingung;

		return $this->query( $query, 'delete' );
	}

	public function affected_rows() {
		$count = intval( @mysqli_affected_rows( $this->conn ) );

		return $count;
	}

	function version() {
		return( "5.0" );
	}

	public function printLastQuery() {
		echo $this->last."<br />";
	} // printLastQuery

	public function get_last_insert_id() {
		$returnid = @mysqli_insert_id( $this->conn );
		return $returnid;
	} // get_last_insert_id
}
?>