<?php
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'PHPExcel/PHPExcel.php' );
require_once( CLASS_DIR.'translate.php' );

class sync_interface {
	static private $instance = null;
	private $db = null;
	private $mes = null;
	private $f = null;
	private $t = null;
	private $stream = "";
	private $objPHPExcel = null;
	private $_TRENN = '<br>';
	private $kurs_title = '';
	public $wkn_error = array();

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->mes = messages::getInstance();
		$this->f = functions::getInstance();
		$this->t = translate::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_url( $url ) {
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)" );
		$data = curl_exec( $ch );
		curl_close($ch);

		return( $data );
	} // get_url

	function sync_kurse_to_wordpress() {
		$setup = $this->f->load_setup( "BAS_SETUP" );

		$i = 0;

		$delete = $setup['wordpress_delete'].'?id='.md5( $setup['wordpress_api_password'] );
		$ret = $this->get_url( $delete );
		if( $ret == -1 ) {
			$this->mes->addError( "Das API-Passwort ist nicht korrekt." );
			return( $ret );
		} // if

		$add = $setup['wordpress_add'].'?id='.md5( $setup['wordpress_api_password'] );

		$this->db->query( "
			SELECT
				k.nummer, k.startdatum, k.enddatum, t.title AS kurstyp, b.title AS bundesland, k.kosten, k.kosten_wirtschaftskammer,
				k.mo, k.di, k.mi, k.do, k.fr, k.sa, k.so, k.kurstyp_id,
				k.mo_startzeit, k.di_startzeit, k.mi_startzeit, k.do_startzeit, k.fr_startzeit, k.sa_startzeit, k.so_startzeit
			FROM BAS_KURSE AS k
			LEFT JOIN BAS_KURSTYP AS t ON (t.id=k.kurstyp_id)
			LEFT JOIN GLOBAL_BUNDESLAND AS b ON (b.id=k.bundesland_id)
			WHERE (k.kurs_nicht_auf_homepage='0') AND (k.startdatum>='".date( "Y-m-d", time() )."')
			ORDER BY k.startdatum DESC", "sync_kurse_to_wordpress" );
		while( $this->db->isNext( "sync_kurse_to_wordpress" ) ) {
			$r = $this->db->getNext( "sync_kurse_to_wordpress" );

			$r['nummer'] = str_replace( " ", "%20", $r['nummer'] );
			$r['kurstyp'] = str_replace( " ", "%20", $r['kurstyp'] );
			$r['bundesland'] = str_replace( " ", "%20", $r['bundesland'] );
			$r['kosten'] = $r['kosten'] + $r['kosten_wirtschaftskammer'];

			$abend = 1;
			foreach( array( 'mo', 'di', 'mi', 'do', 'fr', 'sa', 'so' ) as $k => $v ) {
				if( (substr( $r[$v.'_startzeit'], 0, 2 ) > 0) && (substr( $r[$v.'_startzeit'], 0, 2 ) < 17) ) {
					$abend = 0;
				} // if
			} // foreach

			$ret = $this->get_url( $add.'&'.
				'nummer='.$r['nummer'].'&'.
				'kurstyp='.$r['kurstyp'].'&'.
				'startdatum='.$r['startdatum'].'&'.
				'abendkurs='.$abend.'&'.
				'enddatum='.$r['enddatum'].'&'.
				'bundesland='.$r['bundesland'].'&'.
				'preis='.$r['kosten'].'&'.
				'mo='.$r['mo'].'&'.
				'di='.$r['di'].'&'.
				'mi='.$r['mi'].'&'.
				'do='.$r['do'].'&'.
				'fr='.$r['fr'].'&'.
				'sa='.$r['sa'].'&'.
				'so='.$r['so'].'&'.
				'kurstyp_id='.$r['kurstyp_id']
			);
			if( $ret > 0 )
				$i++;
		} // while

		return( $i );
	} // function

	function create_ams_file() {
		$this->stream = "";
		$setup = $this->f->load_setup( "BAS_SETUP" );

		$header = array(
			"Seminartitel",
			"Bildungsbereich",
			"Datum Beginn",
			"Datum Ende",
			"Uhrzeit",
			"Seminardauer",
			"Inhalt",
			"Voraussetzungen",
			"Ziele",
			"Zielgruppe",
			"Kosten in Euro - für Suche",
			"Angezeigte Kosten (Text)",
			"Zertifikat",
			"Fördermöglichkeit",
			"Seminarplätze",
			"Kontaktperson",
			"eMail",
			"Veranstaltungsort",
			"Referent",
			"Seminarnummer",
			"Anmeldelink",
		);

		$line = "";
		foreach( $header as $k => $v ) {
			if( $line != "" ) $line .= ';';
			$line .= utf8_decode( $v );
		} // foreach
		$this->stream .= $line."\n";

		$this->db->query( "
			SELECT
				t.title AS kurstyp, k.startdatum, k.enddatum, k.startzeit, k.endzeit, k.dauer_gesamt, k.kosten, k.lehrabschluss, b.title AS bundesland,
				k.nummer
			FROM BAS_KURSE AS k
			LEFT JOIN BAS_KURSTYP AS t ON (t.id=k.kurstyp_id)
			LEFT JOIN GLOBAL_BUNDESLAND AS b ON (b.id=k.bundesland_id)
			WHERE
				startdatum>'".date( "Y-m-d", time() )."' AND
			  ((b.id=1) OR (b.id=2))" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$line = "";
			$line .= utf8_decode( $r['kurstyp'] ).';';
			$line .= utf8_decode( "Erziehung, Unterricht" ).';';
			$line .= date( "d.m.Y", strtotime( $r['startdatum'] ) ).';';
			$line .= date( "d.m.Y", strtotime( $r['enddatum'] ) ).';';
			if( $r['startzeit'] != "00:00:00" ) $line .= 'ab '.$r['startzeit'].' ';
			if( $r['endzeit'] != "00:00:00" ) $line .= 'bis '.$r['endzeit'];
			$line .= ';';
			$line .= $r['dauer_gesamt'].' LE;';
			$line .= ';'; // Inhalt
			$line .= ';'; // Voraussetzungen
			$line .= ';'; // Ziele
			$line .= ';'; // Zielgruppe
			$line .= (int)$r['kosten'].';';
			$line .= 'in Euro, bei Beginn;'; // Kosten Zusatzinfo
			if( $r['lehrabschluss'] == 1 ) $line .= 'Lehrabschluss;'; else $line .= ';';
			$line .= ';'; // Fördermöglichkeit
			$line .= '18;'; // Seminarplätze
			$line .= 'Manuela Bankhofer;';
			$line .= $setup['ams_email_bcc'].';';
			$line .= utf8_decode( $r['bundesland'] ).';';
			$line .= ';'; // Referent
			$line .= $r['nummer'].';'; // Seminarplätze
			$line .= 'http://basilica.at/portfolio-view/'.str_replace( " ", "-", $r['nummer'] ).'/;'; // Anmeldelink

			$this->stream .= $line."\n";
		} // while
	} // create_ams_file

	function open_ams_file() {
		header('Content-Type: text/csv');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="AMS_import_file_'.date( "Ymd", time() ).'.csv"');
		header('Content-length: '.strlen($this->stream));

		echo $this->stream;
	} // create_ams_file

	function create_waff_file() {
		$setup = $this->f->load_setup( "BAS_SETUP" );

		$inputFileName = BASE_DIR.'themes/basilica_at/reports/waff_schnittstellenvorlage.xlsx';

		$this->objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

		$arrayData = array();
		$this->db->query( "
			SELECT
				t.title AS kurstyp, t.description, b.title AS bundesland,
				k.*
			FROM BAS_KURSE AS k
			LEFT JOIN BAS_KURSTYP AS t ON (t.id=k.kurstyp_id)
			LEFT JOIN GLOBAL_BUNDESLAND AS b ON (b.id=k.bundesland_id)
			WHERE startdatum>'".date( "Y-m-d", time() )."' AND
				((b.id=1) OR (b.id=2))" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$line = array();
			$line[] = $r['nummer']; 																					// A  Angebotsnummer
			$line[] = $r['kurstyp']; 																					// B  Angebotstitel
			$line[] = ""; 																										// C  Untertitel
			$line[] = $r['description']; 																			// D  Beschreibung
			$line[] = $r['kosten'] + $r['kosten_wirtschaftskammer'];				 	// E  Preis brutto EUR
			$line[] = ""; 																										// F  Anmerkung zum Preis
			$line[] = $r['dauer_gesamt']; 																		// G  Übungseinheiten (UE)
			$line[] = "0103,0814"; 																						// H  Klassenzuordnung/Themenbaum
			$line[] = date( "d.m.Y", strtotime( $r['startdatum'] ) ); 				// I  Erster Kurstag
			$line[] = ""; 																										// J  Erster Kurstag Beginn - Uhrzeit
			$line[] = date( "d.m.Y", strtotime( $r['enddatum'] ) ); 					// K  Letzter Kurstag
			$line[] = ""; 																										// L  Letzter Kurstag Ende - Uhrzeit
			$line[] = "0"; 																										// M  Termin auf Anfrage
			$line[] = ""; 																										// N  Einstieg möglich bis - Datum
			$line[] = "Hirschstettner Straße 86"; 														// O  Angebotsort - Anschrift
			$line[] = "1220"; 																								// P  Angebotsort - PLZ
			$line[] = "Wien"; 																								// Q  Angebotsort - Ort
			$line[] = ""; 																										// R  Angebotsort - Raum/Anmerkung
			$line[] = "10"; 																									// S  TeilnehmerInnen min.
			$line[] = "16"; 																									// T  TeilnehmerInnen max.
			$line[] = ""; 																										// U  ReferentIn
			$line[] = ""; 																										// V  Unterrichtssprache
			$line[] = ""; 																										// W  Teilnahmevoraussetzungen
			$line[] = ""; 																										// X  Zielgruppe
			$line[] = ""; 																										// Y  Lehrmethode
			$line[] = ""; 																										// Z  Lernziel
			$line[] = ""; 																										// AA Ermäßigungen
			$line[] = ""; 																										// AB Abschlüsse
			$line[] = "http://www.basilica.at"; 															// AC Direktlink zum Angebot
			$line[] = ""; 																										// AD Ansprechperson - Titel vorgestellt
			$line[] = "Manuela"; 																							// AE Ansprechperson - Vorname
			$line[] = "Bankhofer"; 																						// AF Ansprechperson - Nachname
			$line[] = ""; 																										// AG Ansprechperson - Titel nachgestellt
			$line[] = ""; 																										// AH Ansprechperson - Funktion
			$line[] = "office@basilica.at"; 																	// AI Ansprechperson - Emailadresse
			$line[] = "01 89 00 912"; 																				// AJ Ansprechperson - Telefonnummer
			$line[] = ""; 																										// AK barrierefreier Zugang
			$line[] = ""; 																										// AL Kinderbetreuung
			$line[] = ""; 																										// AM spezielles Übungsangebot
			$line[] = ""; 																										// AN ISCED Kategorie
			$line[] = ""; 																										// AO NQR-Kategorie

			$arrayData[] = $line;
		} // while

		$this->objPHPExcel->getActiveSheet()->fromArray( $arrayData, "", 'A2' );
	} // create_waff_file

	function open_waff_file() {
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="WAFF_import_file_'.date( "Ymd", time() ).'.xlsx"');

		$objWriter = PHPExcel_IOFactory::createWriter($this->objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	} // create_waff_file

	function correct_svn( $svn, $geb_datum ) {
		$svn_constant = '3790584216';

		$svn = str_replace( " ", "", $svn );
		if( strlen( $svn ) != 10 ) {
			$geb_datum = date( "dmy", strtotime( $geb_datum ) );

			if( strlen( $svn ) == 4 )
				$svn = $svn.$geb_datum;
			else
				$svn = '1110'.$geb_datum;
		} // if

		$summe = 0;
		for( $i=0; $i<strlen( $svn_constant ); $i++ ) {
			$summe += $svn_constant[$i] * $svn[$i];
		} // for
		$check_sum = $summe % 11;

		if( $svn['3'] != $check_sum )
			$svn['3'] = $check_sum;

		$svn = substr( $svn, 0, 4 );

		return( $svn );
	} // svn

	function create_wkw_file( $kurs_id ) {
		$this->stream = "";
		$this->wkn_error = array();
		$bAutoCorrect = false;

		$this->db->query( "
			SELECT
				k.nummer, t.vorname, t.nachname, t.geschlecht_id, t.geb_datum, t.svn, t.geb_ort, t.adresse, t.plz, t.ort, ktt.title, k.startdatum, k.enddatum, k.termin
			FROM BAS_KURSTEILNEHMER AS kt
			LEFT JOIN BAS_TEILNEHMER AS t ON (t.teilnehmer_id=kt.teilnehmer_id)
			LEFT JOIN BAS_KURSE AS k ON (k.kurs_id=kt.kurs_id)
			LEFT JOIN BAS_KURSTYP AS ktt ON (ktt.id=k.kurstyp_id)
			WHERE kt.kurs_id='".$kurs_id."'", "create_wkw_file" );
		while( $this->db->isNext( "create_wkw_file" ) ) {
			$r = $this->db->getNext( "create_wkw_file" );

			if( ($i = strrpos( $r['adresse'], " " )) > 0 ) {
				$strasse = substr( $r['adresse'], 0, $i );
				$hausnummer = substr( $r['adresse'], $i + 1 );
			} else {
				$strasse = $r['adresse'];
				$hausnummer = '';
			} // else
			$this->kurs_title = trim( $r['nummer'] );

			if( $r['vorname'] == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => 'Fehlerhafte Teilnehmerzuordnung, bzw. Vorname ist leer.' );
				if( $bAutoCorrect ) $r['vorname'] = '<unbekannt>';
			} // if
			if( $r['nachname'] == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => 'Fehlerhafte Teilnehmerzuordnung, bzw. Nachname ist leer.' );
				if( $bAutoCorrect ) $r['nachname'] = '<unbekannt>';
			} // if
			if( $r['geb_ort'] == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => $r['nachname'].' '.$r['vorname'].': Geburtsort ist leer.' );
				if( $bAutoCorrect ) $r['geb_ort'] = '<unbekannt>';
			} // if
			if( $strasse == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => $r['nachname'].' '.$r['vorname'].': Strasse ist leer.' );
				if( $bAutoCorrect ) $strasse = '<unbekannt>';
			} // if
			if( $hausnummer == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => $r['nachname'].' '.$r['vorname'].': Hausnummer kann nicht bestimmt werden.' );
				if( $bAutoCorrect ) $hausnummer = '<unbekannt>';
			} // if
			if( $r['plz'] == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => $r['nachname'].' '.$r['vorname'].': PLZ ist leer.' );
				if( $bAutoCorrect ) $r['plz'] = '<unbekannt>';
			} // if
			if( $r['ort'] == "" ) {
				$this->wkn_error[] = array( "kurs" => $r['nummer'], "text" => $r['nachname'].' '.$r['vorname'].': Wohnort ist leer.' );
				if( $bAutoCorrect ) $r['ort'] = '<unbekannt>';
			} // if

			$line = '';
			$line .= $this->correct_csv( $r['vorname'] ).';';
			$line .= $this->correct_csv( $r['nachname'] ).';';
			$line .= (($r['geschlecht_id']<=1)?'M':'W').';';
			$line .= date( 'd.m.Y', strtotime( $r['geb_datum'] ) ).';';
			$line .= $this->correct_svn( $r['svn'], $r['geb_datum'] ).';';
			$line .= $this->correct_csv( $r['geb_ort'] ).';';
			$line .= 'AT;'; // ISO Geburtsstaat
			$line .= $this->correct_csv( $strasse ).';';
			$line .= $hausnummer.';';
			$line .= $r['plz'].';';
			$line .= $this->correct_csv( $r['ort'] ).';';
			$line .= 'AT;'; // ISO Wohnort
			$line .= $this->correct_csv( $r['title'] ).';';
			$line .= date( 'd.m.Y', strtotime( $r['startdatum'] ) ).';';
			$line .= date( 'd.m.Y', strtotime( $r['enddatum'] ) ).';';
			$line .= date( 'd.m.Y', strtotime( $r['termin'] ) ).';'; // Termin

			$this->stream .= $line."\n";
		} // while

		if( $bAutoCorrect )
			$this->wkn_error = array();
	} // create_wkw_file

	function open_wkw_file() {
		if( sizeof( $this->wkn_error ) > 0 ) {
			$str = 'Datei konnte nicht erzeugt werden:<br />';
			foreach( $this->wkn_error as $k => $v ) {
				$str .= '- '.$v['kurs'].': '.$v['text'].'<br />';
			} // foreach
			$str = substr( $str, 0, -6 );
			$this->mes->addError( $str );
		} else {
			header('Content-Type: text/csv');
			header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
			header('Pragma: public');
			header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
			header('Content-Disposition: inline; filename="WKW_import_file_'.$this->kurs_title.'_'.date( "Ymd", time() ).'.csv"');
			header('Content-length: '.strlen($this->stream));

			echo $this->stream;
			exit;
		} // else
	} // open_wkw_file

	function import_fragebogen() {
		if( !isset( $_FILES['datei']['tmp_name'] ) || ($_FILES['datei']['tmp_name'] == '') )
			return( "Bitte eine Datei auswählen." );

		$content = file_get_contents( $_FILES['datei']['tmp_name'] );
		$lines = explode( $this->_TRENN, $content );

		foreach( $lines as $k => $v ) {
			if( strpos( $v, "WHY_FRAGEBOGEN" ) ) {
				$v = str_replace( '[fragebogen_id]', '', $v );

				$this->db->query( $v );
				$fragebogen_id = $this->db->get_last_insert_id();
			} // if

			if( strpos( $v, "WHY_FRAGEN" ) ) {
				$v = str_replace( '[fragebogen_id]', $fragebogen_id, $v );
				$v = str_replace( '[frage_id]', '', $v );

				$this->db->query( $v );
				$frage_id = $this->db->get_last_insert_id();
			} // if

			if( strpos( $v, "WHY_ANTWORTEN" ) ) {
				$v = str_replace( '[fragebogen_id]', $fragebogen_id, $v );
				$v = str_replace( '[frage_id]', $frage_id, $v );
				$v = str_replace( '[antwort_id]', '', $v );

				$this->db->query( $v );
				$antwort_id = $this->db->get_last_insert_id();
			} // if

			$this->db->commit();
		} // foreach

		return( "Import erfolgreich." );
	} // import_fragebogen

	function export_fragebogen( $fragebogen_id ) {
		$content = '';

		// Fragebogen
		$this->db->query( "SELECT * FROM WHY_FRAGEBOGEN WHERE fragebogen_id='".$fragebogen_id."'" );
		$r = $this->db->getNext();

		$titel = $r['titel'];

		$fields = '';
		$values = '';
		foreach( $r as $k => $v ) {
			if( $fields != '' ) $fields .= ',';
			$fields .= "`".$k."`";

			if( $k == "fragebogen_id" ) $v = '[fragebogen_id]';

			if( $values != '' ) $values .= ',';
			$values .= "'".$v."'";
		} // foreach

		$content .= 'INSERT INTO WHY_FRAGEBOGEN ('.$fields.') VALUES ('.$values.')'.$this->_TRENN;

		// Fragen
		$this->db->query( "SELECT * FROM WHY_FRAGEN WHERE fragebogen_id='".$fragebogen_id."'", "fragen" );
		while( $this->db->isNext( "fragen" ) ) {
			$r = $this->db->getNext( "fragen" );

			$fields = '';
			$values = '';
			foreach( $r as $k => $v ) {
				if( $fields != '' ) $fields .= ',';
				$fields .= "`".$k."`";

				if( $k == "fragebogen_id" ) $v = '[fragebogen_id]';
				if( $k == "frage_id" ) $v = '[frage_id]';

				if( $values != '' ) $values .= ',';
				$values .= "'".$v."'";
			} // foreach

			$content .= 'INSERT INTO WHY_FRAGEN ('.$fields.') VALUES ('.$values.')'.$this->_TRENN;

			// Antworten
			$this->db->query( "SELECT * FROM WHY_ANTWORTEN WHERE frage_id='".$r['frage_id']."'", "antworten" );
			while( $this->db->isNext( "antworten" ) ) {
				$r = $this->db->getNext( "antworten" );

				$fields = '';
				$values = '';
				foreach( $r as $k => $v ) {
					if( $fields != '' ) $fields .= ',';
					$fields .= "`".$k."`";

					if( $k == "fragebogen_id" ) $v = '[fragebogen_id]';
					if( $k == "frage_id" ) $v = '[frage_id]';
					if( $k == "antwort_id" ) $v = '[antwort_id]';

					if( $values != '' ) $values .= ',';
					$values .= "'".$v."'";
				} // foreach

				$content .= 'INSERT INTO WHY_ANTWORTEN ('.$fields.') VALUES ('.$values.')'.$this->_TRENN;
			} // while
		} // while

		header('Content-Type: text/csv');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="Fragebogen_'.$titel.'_'.date( "Ymd", time() ).'.export"');
		header('Content-length: '.strlen($content));

		echo $content;
	} // export_fragebogen

	function correct_csv( $str ) {
		$str = str_replace( ";", "", $str );
		$str = utf8_decode( $str );

		return( $str );
	} // correct_csv

	function create_outlook_file() {
		$this->stream = "";

		$header = array(
			"Betreff",
			"Beginnt am",
			"Beginnt um",
			"Endet am",
			"Endet um",
			"Ganztägiges Ereignis",
			"Erinnerung Ein/Aus",
			"Erinnerung am",
			"Erinnerung um",
			"Besprechungsplanung",
			"Erforderliche Teilnehmer",
			"Optionale Teilnehmer",
			"Besprechungsressourcen",
			"Abrechnungsinformationen",
			"Beschreibung",
			"Kategorien",
			"Ort",
			"Priorität",
			"Privat",
			"Reisekilometer",
			"Vertraulichkeit",
			"Zeitspanne zeigen als",
		);

		$line = "";
		foreach( $header as $k => $v ) {
			if( $line != "" ) $line .= ',';
			$line .= '"'.utf8_decode( $v ).'"';
		} // foreach
		$this->stream .= $line."\n";

		$this->db->query( "
			SELECT
				k.nummer, t.title AS kurstyp, k.startdatum, k.enddatum, k.startzeit, k.endzeit,
				k.mo, k.di, k.mi, k.do, k.fr, k.sa, k.so
			FROM BAS_KURSE AS k
			LEFT JOIN BAS_KURSTYP AS t ON (t.id=k.kurstyp_id)
			WHERE enddatum>=NOW()", "create_outlook_file" );
		while( $this->db->isNext( "create_outlook_file" ) ) {
			$r = $this->db->getNext( "create_outlook_file" );

			$s = strtotime( $r['startdatum'] );
			$e = strtotime( $r['enddatum'] );

			while( $s <= $e ) {
				$bWriteLine = false;
				$weekday = date( "w", $s );

				switch( $weekday ) {
					case 0: if( $r['so'] == 1 ) $bWriteLine = true; break;
					case 1: if( $r['mo'] == 1 ) $bWriteLine = true; break;
					case 2: if( $r['di'] == 1 ) $bWriteLine = true; break;
					case 3: if( $r['mi'] == 1 ) $bWriteLine = true; break;
					case 4: if( $r['do'] == 1 ) $bWriteLine = true; break;
					case 5: if( $r['fr'] == 1 ) $bWriteLine = true; break;
					case 6: if( $r['sa'] == 1 ) $bWriteLine = true; break;
				} // switch

				if( $bWriteLine ) {
					$line = "";
					$line .= '"'.utf8_decode( $r['nummer'] ).'",';
					$line .= '"'.date( "d.m.Y", $s ).'",';
					$line .= '"'.date( "H:i:s", strtotime( $r['startzeit'] ) ).'",';
					$line .= '"'.date( "d.m.Y", $s ).'",';
					$line .= '"'.date( "H:i:s", strtotime( $r['endzeit'] ) ).'",';
					$line .= '"Aus",';
					$line .= '"Aus",';
					$line .= '"01.01.2099",';
					$line .= '"00:00:00",';
					$line .= ',,,,,,';
					$line .= '"'.utf8_decode( $r['kurstyp'] ).'",';
					$line .= '"",';
					$line .= '"Normal",';
					$line .= '"Aus",';
					$line .= ',';
					$line .= '"Normal",';
					$line .= '"3"';

					$this->stream .= $line."\r\n";
				} // if

				$s += 60*60*24;
			} // while
		} // while
	} // create_outlook_file

	function open_outlook_file() {
		header('Content-Type: text/csv');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="outlook_import_file_'.date( "Ymd", time() ).'.csv"');
		header('Content-length: '.strlen($this->stream));

		echo $this->stream;
	} // create_outlook_file

	function TD_create_excel( $list_id ) {
		$this->stream = "";
		$arrFields = array();
		$setup = $this->f->load_setup( "TEC_SETUP" );

		// Header
		$line = "";
		$this->db->query( "SELECT * FROM CORE_LISTS_FIELDS WHERE list_id='".$list_id."' ORDER BY pos" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			if( $line != "" ) $line .= ";";

			$bShow = true;
			if( ($list_id == 36) && ($r['pos'] == 60) && ($setup['item_check_active'] == 0) )
				$bShow = false;

			if( $bShow ) {
				$line .= $this->t->t( $r['title'] );

				$arrFields[] = $r['field'];
			} // if
		} // while
		$this->stream .= $line."\n";

		// Zeilen
		$this->db->query( "SELECT delete_table, sort_order FROM CORE_LISTS WHERE list_id='".$list_id."'" );
		$r = $this->db->getNext();

		$this->db->query( "SELECT ".implode( ",", $arrFields )." FROM ".$r['delete_table']." ORDER BY ".$r['sort_order'] );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$line = "";
			foreach( $arrFields as $k => $v ) {
				if( $line != "" ) $line .= ";";
				$r[$v] = str_replace( "\n", " ", $r[$v] );
				$r[$v] = str_replace( ";", ", ", $r[$v] );

				$line .= $r[$v];
			} // foreach
			$this->stream .= $line."\n";
		} // while
	} // create_excel

	function TD_open_excel_file() {
		header('Content-Type: text/csv');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: inline; filename="Export'.date( "Ymd", time() ).'.csv"');
		header('Content-length: '.strlen($this->stream));

		echo $this->stream;
	} // open_excel_file



} // sync_interface
?>