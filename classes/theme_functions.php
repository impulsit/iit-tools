<?php
class theme_functions {
	static private $instance = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
	} // __construct

	function get_header() {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$arr = array();
		$arr[] = 'themes/'.$location.'/classes/';
		if( strpos( $location, "_atit_at" ) !== false )
			$arr[] = 'themes/tecdocbase_atit_at/classes/';
		$arr[] = 'themes/_default/classes/';

		foreach( $arr as $k => $v ) {
			$path = $v.'header.php';

			if( file_exists( BASE_DIR.$path ) )
				break;
		} // foreach

		if( !isset( $_SESSION['project_id'] ) ) $_SESSION['project_id'] = 0;
		switch( $_SESSION['project_id'] ) {
			case 4:  $path = 'themes/tecdocbase_atit_at/classes/header.php'; break;
			default: $path = 'themes/_default/classes/header.php';
		} // switch

		return( BASE_DIR.$path );
	} // get_header

	function get_footer() {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

			$arr = array();
			$arr[] = 'themes/'.$location.'/classes/';
			if( strpos( $location, "_atit_at" ) !== false )
				$arr[] = 'themes/tecdocbase_atit_at/classes/';
			$arr[] = 'themes/_default/classes/';

			foreach( $arr as $k => $v ) {
				$path = $v.'footer.php';

				if( file_exists( BASE_DIR.$path ) )
					break;
			} // foreach

			if( !isset( $_SESSION['project_id'] ) ) $_SESSION['project_id'] = 0;
			switch( $_SESSION['project_id'] ) {
				case 4:  $path = 'themes/tecdocbase_atit_at/classes/footer.php'; break;
				default: $path = 'themes/_default/classes/footer.php';
			} // switch

			return( BASE_DIR.$path );
	} // get_footer

	function get_icon_path( $iconname, $strExt = ".png" ) {
		global $_location;

		// Correct iconname
		$iconname = basename( $iconname );
		$iconname = str_replace( $strExt, '', $iconname );

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;
		$arr = array( 'themes/'.$location.'/icons/', 'themes/_default/icons/', 'themes/_default/icons/flags/' );

		foreach( $arr as $k => $v ) {
			$pic_path = $v.$iconname.$strExt;

			if( file_exists( BASE_DIR.$pic_path ) )
				break;
		} // foreach

		return( '/'.SUBDIR.$pic_path );
	} // get_icon_path

	function get_logo($strWhich = "") {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$arr[] = 'themes/'.$location.'/logo/';
		if( strpos( $location, "_atit_at" ) !== false )
			$arr[] = 'themes/tecdocbase_atit_at/logo/';
		$arr[] = 'themes/_default/logo/';

		foreach( $arr as $k => $v ) {
			$pic_path = $v.'main_logo'.$strWhich.'.png';

			if( file_exists( BASE_DIR.$pic_path ) )
				break;
		} // foreach

		return( '/'.SUBDIR.$pic_path );
	} // get_logo

	function get_logo_left() {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$pic_path = "";
		$arr = array( 'themes/'.$location.'/logo/', 'themes/tecdocbase_atit_at/logo/' );

		foreach( $arr as $k => $v ) {
			if( ((strpos( $location, "_atit_at" ) === false) && (strpos( $v, "_atit_at" ) === false)) ||
					((strpos( $location, "_atit_at" ) !== false) && (strpos( $v, "_atit_at" ) !== false)) ) {
				$pic_path = $v.'left_logo.png';

				if( file_exists( BASE_DIR.$pic_path ) )
					break;

				$pic_path = "";
			} // if
		} // foreach

		if( $pic_path == "" )
			return( "" );

		return( '/'.SUBDIR.$pic_path );
	} // get_logo_left

	function get_favico() {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

			$arr[] = 'themes/'.$location.'/favicon/';
			if( strpos( $location, "_atit_at" ) !== false )
				$arr[] = 'themes/tecdocbase_atit_at/favicon/';
			$arr[] = 'themes/_default/favicon/';

			foreach( $arr as $k => $v ) {
				$pic_path = $v.'favicon.ico';

				if( file_exists( BASE_DIR.$pic_path ) )
					break;
			} // foreach

			return( '/'.SUBDIR.$pic_path );
	} // get_logo

	function get_theme_style( $strWhich = 'location' ) {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$path = "---";
		switch( $strWhich ) {
			case 'location': $arr = array( 'themes/'.$location.'/css/' ); break;
			case 'base':
			default: $arr = array( 'themes/tecdocbase_atit_at/css/' ); break;
		} // switch

		foreach( $arr as $k => $v ) {
			if( ((strpos( $location, "_atit_at" ) === false) && (strpos( $v, "_atit_at" ) === false)) ||
					((strpos( $location, "_atit_at" ) !== false) && (strpos( $v, "_atit_at" ) !== false)) ) {
				$path = $v.'style.php';

				if( file_exists( BASE_DIR.$path ) )
					break;
			} // if
		} // foreach

		if( !isset( $_SESSION['project_id'] ) ) $_SESSION['project_id'] = 0;
		switch( $_SESSION['project_id'] ) {
			case 4:  $path = 'themes/tecdocbase_atit_at/css/style.php'; break;
			default: $path = 'themes/_default/classes/css/style.php';
		} // switch

		return( BASE_DIR.$path );
	} // get_theme_style

	function get_shortcut_icon( $shortcut ) {
		global $_location;

		if( strlen( $shortcut ) == 1 ) $shortcut = '0'.$shortcut;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$arr = array( 'themes/'.$location.'/icons/group/', 'themes/tecdocbase_atit_at/icons/group/' );

		foreach( $arr as $k => $v ) {
			$path = SUBDIR.$v.'group_'.$shortcut.'.jpg';
			if( file_exists( BASE_DIR.$path ) )
				break;

			$path = SUBDIR.$v.'group_'.$shortcut.'.png';
			if( file_exists( BASE_DIR.$path ) )
				break;
		} // foreach

		return( '/'.$path );
	} // get_shortcut_icon

} // theme_functions
?>