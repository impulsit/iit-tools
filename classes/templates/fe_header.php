<!DOCTYPE html>
<html lang="de"><head>
<title>Fragebogen</title>
<meta charset="utf-8">
<meta name="keywords" content="Fragebogen">
<meta name="description" content="Fragebogen">
<meta name="author" content="Fragebogen">

<meta http-equiv="X-UA-Compatible" content="IE=9">
<script src="<?=DOMAIN_JS_DIR?>f.php"></script>

<link rel="shortcut icon" href="<?=DOMAIN?>favicon.ico" />
<link rel="stylesheet" href="<?=DOMAIN_CSS_DIR?>fragebogen.css">
<link rel="stylesheet" href="<?=DOMAIN_CSS_DIR?>indiv/<?=$fragebogen['fragebogen_id']?>.css">
<!--[if IE 9]><link rel="stylesheet" href="<?=DOMAIN_CSS_DIR?>ie_fix.css"><![endif]-->
<meta name="viewport" content="initial-scale=1, maximum-scale=5, minimum-scale=1, width=device-width">
</head>
<body>
