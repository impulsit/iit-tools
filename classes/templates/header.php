<?
if( !isset($_SESSION) ) session_start();

$time_start = microtime(true);

require_once( dirname( __FILE__ ).'/../config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'mail.php' );
require_once( CLASS_DIR.'theme_functions.php' );
require_once( CLASS_DIR.'translate.php' );
require_once( CLASS_DIR.'tecdoc.php' );

$db = mysql::getInstance();
$f = functions::getInstance();
$mes = messages::getInstance();
$mail = fb_mail::getInstance();
$theme = theme_functions::getInstance();
$t = translate::getInstance();
$tec = tecdoc::getInstance();

$js_time = 0;
$files = glob( JS_DIR.'*' );
foreach( $files as $k => $v ) {
	$zeit = filemtime( $v );
	if( $zeit > $js_time )
		$js_time = $zeit;
} // foreach

$css_time = 0;
$files = glob( CSS_DIR.'*' );
foreach( $files as $k => $v ) {
	$zeit = filemtime( $v );
	if( $zeit > $css_time )
		$css_time = $zeit;
} // foreach

$latest_version = '';
$lastest_backup = '';

if( !isset( $_SESSION['c_user_id'] ) )
	$_SESSION['c_user_id'] = -1;

$setup = $f->load_setup( "CORE_SETUP" );
$userinfo = $f->load_user( $_SESSION['c_user_id'], "user_level, name" );
if( $userinfo['user_level'] >= 990 ) {
  if( ($_location != "vmware_home") && ($_location != "qnap_home") ) {
  	$server_version = @file_get_contents( "http://iit-tools.sordje.at/updates/".$_location."/last_version.txt" );

  	$latest_version = str_replace( "_", " ", $server_version );
  	if( ($server_version != "") && ($latest_version != VERSION) ) {
			$latest_version = ' - <span class="blink" style="color:#FF0000;"><a href="/admin/core_update.php" style="color:#FF0000; text-decoration: underline;">UPDATE verfügbar: '.$latest_version.'</a></span>';
  	} else
  		$latest_version = '';
  } // if

  // Backup Info
  $path = BASE_DIR."msd/work/backup/";

  $latest_mtime = 0;
  $latest_filename = '';

  if( file_exists( $path ) ) {
	  $d = dir($path);
	  while( false !== ($entry = $d->read()) ) {
	  	$filepath = "{$path}/{$entry}";

	  	// could do also other checks than just checking whether the entry is a file
	  	if( is_file( $filepath ) && filemtime( $filepath ) > $latest_mtime ) {
	  		$latest_mtime = filemtime( $filepath );
	  		$latest_filename = $entry;
	  	} // if
	  } // while

	  $days = ceil( abs( time() - $latest_mtime) / 86400 );
	  if( $setup['show_backup_info'] )
		  if( $days >= 3 ) {
		  	$lastest_backup = date( "d.m.y", $latest_mtime );
		  	$lastest_backup = ' - <span style="color:#8a0808;">letztes <a href="/msd/dump.php?config=mysqldumper" style="color:#8a0808; text-decoration: underline;" target="_blank">BACKUP vom '.$lastest_backup.' ('.$days.' Tage alt)</a></span>';
		  } // if
  } // if
} // if

header( "Referrer-Policy: no-referrer-when-downgrade" );
header( "Strict-Transport-Security: max-age=31536000; includeSubDomains" );
header( "X-Content-Type-Options: nosniff" );
header( "X-Frame-Options: SAMEORIGIN" );
header( "X-XSS-Protection: 1; mode=block" );
header( "Expect-CT: max-age=604800" );
//header( "Content-Security-Policy: default-src 'self' 'unsafe-inline' 'unsafe-eval' data: *" );
//header( "X-Content-Security-Policy: default-src 'self' 'unsafe-inline' 'unsafe-eval' data: * " );

if( isset( $_GET['project_id'] ) )
	$_SESSION['project_id'] = $_GET['project_id'];

if( defined( "COPYRIGHT" ) )
	echo '<!-- '.COPYRIGHT.' -->
';
?>
<!-- author: Michael Sordje, http://www.impulsit.at -->
<?php
require_once( $theme->get_header() );

if( $userinfo['user_level'] >= 990 )
	echo '<input type="hidden" value="1" id="show_ajax_error" name="show_ajax_error">';
?>
