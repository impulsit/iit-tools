<?php
error_reporting( E_ALL );
ini_set( 'display_errors', 1);

$_current_version = file_get_contents( dirname( __FILE__ ).'/config_data/current_version.txt' );
define( "VERSION", $_current_version );

global $_location;

if( !file_exists( dirname( __FILE__ ).'/../location.txt' ) ) {
	echo 'FATAL ERROR: project location not found.';
	exit;
} // if
$_location = file_get_contents( dirname( __FILE__ ).'/../location.txt' );

if( !file_exists( dirname( __FILE__ ).'/config_data/location_'.$_location.'.php' ) ) {
	echo 'FATAL ERROR: project configuration not found.';
	exit;
} // if
include( dirname( __FILE__ ).'/config_data/location_'.$_location.'.php' );

// Filesserver
define( "CLASS_DIR", BASE_DIR."classes/" );
define( "PIC_DIR", BASE_DIR."pics/" );
define( "JS_DIR", BASE_DIR."js/" );
define( "CSS_DIR", BASE_DIR."css/" );

// Domain
define( "DOMAIN_JS_DIR", DOMAIN."js/" );
define( "DOMAIN_PIC_DIR", DOMAIN."pics/" );
define( "DOMAIN_CSS_DIR", DOMAIN."css/" );

// Aktionen
define( "AKTION_EINKAUF", 1 );
define( "AKTION_SEND", 2 );

// Konstanten
define( "ADMIN", 1 );
define( "PW_LOST", 2 );
define( "NEW_PW", 3 );
define( "REGISTER", 4 );
define( "NEW_FB", 5 );
define( "USER", 7 );
define( "ALL", 8 );

$_LISTEN = array();

include( "config_data/menu_login.php" );
include( "config_data/menu_verwaltung.php" );
?>