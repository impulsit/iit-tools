<?php // <!-- GROUP: DEMO -->
// mysql
define( "MYSQL_SERVER", "localhost" );
define( "MYSQL_USER", "db_autodemo" );
define( "MYSQL_PASSWORD", "Super77Nav!!" );
define( "MYSQL_DB", "autodemo" );

// Fileserver
define( "SUBDIR", "" );

// Domain
define( "DOMAIN", "http://autodemo.atit-solutions.eu/".SUBDIR."" );
define( "BASE_DIR", "/var/www/autodemo/".SUBDIR );

// Titel
define( "TITEL", "Autodemo" );
define( "META", "Autodemo" );
define( "COPYRIGHT", "copyright by at iT Informationstechnologie GmbH 2016, www.atit.at" );

// Menu
define( "PROJECT_ALLOWED", 4 );
?>