<?php // <!-- GROUP: OLD -->
// mysql
define( "MYSQL_SERVER", "localhost" );
define( "MYSQL_USER", "root" );
define( "MYSQL_PASSWORD", "admin" );
define( "MYSQL_DB", "development" );

// Fileserver
define( "SUBDIR", "iit-tools/" );
define( "FILE_SAVE_DIR", "/share/CACHEDEV1_DATA/Web/iit-tools/_files" );
define( "FILE_SAVE_DIR_FOR_BATCH", "\\\\qnap\\Web\\iit-tools\\_files" );

// Domain
define( "DOMAIN", "http://192.168.0.21/".SUBDIR."" );
define( "BASE_DIR", "/share/CACHEDEV1_DATA/Web/".SUBDIR );

// Titel
define( "TITEL", "impuls Management" );
define( "META", "impuls Management" );
define( "COPYRIGHT", "copyright by at impuls IT, www.impulsit.at" );

// Menu
define( "PROJECT_ALLOWED", 4 );

//define( "SIMULATE_LOCATION", "basilica_at" );
//define( "SIMULATE_LOCATION", "autodemo_atit_at" );
//define( "SIMULATE_LOCATION", "test-v2_atit_at" );
//define( "SIMULATE_LOCATION", "arvcentar_atit_at" );
?>