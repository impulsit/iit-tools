<?php
// Menu
$_MENU_MODUL_VERWALTUNG = array(
	array( "title" => "Hauptmenü", "url" => "/".SUBDIR."admin/core_backend.php", "pic" => "pics/hauptmenu.png", "min_user_level" => 0, "fa_class" => "fas fa-home text-success" ),
	array( "title" => "Projekte", "url" => "/".SUBDIR."admin/core_projects.php", "pic" => "pics/project.png", "min_user_level" => 900, "fa_class" => "fas fa-project-diagram text-primary" ),
	array( "title" => "Benutzer", "url" => "/".SUBDIR."admin/core_users.php", "pic" => "pics/user.png", "min_user_level" => 900, "fa_class" => "fas fa-users text-primary" ),
	array( "title" => "Update", "url" => "/".SUBDIR."admin/core_update.php", "pic" => "pics/update.png", "min_user_level" => 900, "fa_class" => "fas fa-sync-alt text-primary" ),
	array( "title" => "Change Log", "url" => "/".SUBDIR."admin/core_change_log.php", "pic" => "pics/change_log.png", "min_user_level" => 900, "fa_class" => "fab fa-stack-exchange text-primary" ),
	array( "title" => "Backup", "url" => "/".SUBDIR."msd/index.php", "pic" => "pics/backup.png", "min_user_level" => 900, "new_window" => true, "fa_class" => "fas fa-hdd text-primary" ),
	array( "title" => "PHP-Info", "url" => "/".SUBDIR."admin/phpinfo.php", "pic" => "pics/info.png", "min_user_level" => 900, "new_window" => true, "fa_class" => "fas fa-info-circle text-primary" ),
	array( "title" => "Einstellungen", "url" => "/".SUBDIR."admin/core_settings.php", "pic" => "pics/settings.png", "min_user_level" => 900, "fa_class" => "fas fa-cog text-primary" ),
);

// Listen
define( "LIST_CORE_USERS", 											-1 );
define( "LIST_CORE_PROJECTS", 									-2 );
define( "LIST_CORE_PROJECTS_MAINMENU", 					-3 );
define( "LIST_CORE_PROJECTS_LISTS", 						-4 );
define( "LIST_CORE_PROJECTS_ACTION_BUTTONS", 		-5 );
define( "LIST_CORE_PROJECTS_LISTS_BUTTONS", 		-6 );
define( "LIST_CORE_PROJECTS_LISTS_FIELDS", 			-7 );
define( "LIST_CORE_PROJECTS_LISTS_FIELDS_FIX",	-8 );

// Buttons
define( "BUTTON_HAUPTMENU", 					-1 );
define( "BUTTON_LISTS",								-2 );
define( "BUTTON_ACTION_BUTTONS",			-3 );
define( "BUTTON_BUTTONS_ZUORDNEN",		-4 );
define( "BUTTON_FELDER", 							-5 );
define( "BUTTON_FELDER_FIX",					-6 );

$_LISTEN[LIST_CORE_USERS] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "User ID", "field" => "user_id" ),
		array( "type" => "text", "title" => "Login / E-Mail", "field" => "email", "mandatory" => true ),
		array( "type" => "text", "title" => "Name", "field" => "name", "mandatory" => true ),
		array( "type" => "password", "title" => "Passwort", "field" => "password" ),
		array( "type" => "select", "title" => "Rolle", "field" => "user_level_title", "where_field" => "ul.title", "save_field" => "user_level", "select_query" => "SELECT id, title AS user_level_title FROM CORE_USER_LEVEL", "select_id" => "id" ),
		array( "type" => "select", "title" => "Hauptmenü", "field" => "project_title", "where_field" => "p.title", "save_field" => "start_menu", "select_query" => "SELECT project_id, title AS project_title FROM CORE_PROJECTS", "select_id" => "project_id" ),
	),
	"list_query" => "
		SELECT user_id, email, name, admin, password, list_register, winword_path, start_menu, valid_to, topmotive_kundennr, p.title AS project_title, ul.title AS user_level_title, vin_search_inactive
		FROM CORE_USER_INFO AS u
		LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=u.start_menu)
		LEFT JOIN CORE_USER_LEVEL AS ul ON (ul.id=u.user_level)",
	"sort_order" => "name ASC",
	"delete_primary_key" => "user_id",
	"delete_table" => "CORE_USER_INFO",
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
	"file" => "core_users.php"
);

$_LISTEN[LIST_CORE_PROJECTS] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "Projekt ID", "field" => "project_id" ),
		array( "type" => "text", "title" => "Name", "field" => "title" ),
//		array( "type" => "select_file", "title" => "Bild", "field" => "picture", "path" => "themes/_default/icons/", "pattern" => "*.png", "show_picture" => true ),
		array( "type" => "fa_class", "title" => "Bild Klasse (FA)", "field" => "m.fa_class" ),
	),
	"list_query" => "
		SELECT project_id, title, picture, fa_class
		FROM CORE_PROJECTS",
	"sort_order" => "project_id ASC",
	"delete_primary_key" => "project_id",
	"delete_table" => "CORE_PROJECTS",
	"file" => "core_projects.php",
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
	"action_buttons" => array( BUTTON_HAUPTMENU, BUTTON_LISTS, BUTTON_ACTION_BUTTONS ),
);

$_LISTEN[LIST_CORE_PROJECTS_MAINMENU] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "Menü ID", "field" => "menu_id" ),
		array( "type" => "integer", "title" => "Pos", "field" => "pos", "fill_fix_type" => "AUTOINC", "fill_fix_param" => "CORE_MAINMENU AS m" ),
		array( "type" => "text", "title" => "Name", "field" => "m.title" ),
		array( "type" => "select_file", "title" => "Bild", "field" => "m.picture", "path" => "themes/_default/icons/", "pattern" => "*.png", "show_picture" => true ),
		array( "type" => "fa_class", "title" => "Bild Klasse (FA)", "field" => "m.fa_class" ),
		array( "type" => "select_file", "title" => "Weiterleitung", "field" => "m.file", "path" => "admin/", "pattern" => "*.php" ),
		array( "type" => "select", "title" => "Weiterleitung Liste", "field" => "list_title", "where_field" => "l.title", "save_field" => "list_id", "select_query" => "SELECT list_id, title AS list_title FROM CORE_LISTS ORDER BY title", "select_id" => "list_id" ),
		array( "type" => "select", "title" => "min. Rolle", "field" => "user_level_title", "where_field" => "ul.title", "save_field" => "min_user_level", "select_query" => "SELECT id, title AS user_level_title FROM CORE_USER_LEVEL", "select_id" => "id" ),
		array( "type" => "select", "title" => "Projekt", "field" => "project_title", "where_field" => "p.title", "save_field" => "project_id", "select_query" => "SELECT project_id, title AS project_title FROM CORE_PROJECTS", "select_id" => "project_id", "fill_fix_type" => "PARAMETER", "fill_fix_param" => "m.project_id" ),
	),
	"list_query" => "
		SELECT m.menu_id, m.project_id, m.title, m.picture, m.file, m.pos, m.fa_class, p.title AS project_title, l.title AS list_title, ul.title AS user_level_title
		FROM CORE_MAINMENU AS m
		LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=m.project_id)
		LEFT JOIN CORE_LISTS AS l ON (l.list_id=m.list_id)
		LEFT JOIN CORE_USER_LEVEL AS ul ON (ul.id=m.min_user_level)",
	"sort_order" => "pos ASC",
	"delete_primary_key" => "menu_id",
	"delete_table" => "CORE_MAINMENU",
	"file" => "core_list_redirect.php",
	"back_up_file" => "core_projects.php",
	"back_up_list" => 0,
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
);

$_LISTEN[LIST_CORE_PROJECTS_LISTS] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "List ID", "field" => "l.list_id" ),
		array( "type" => "text", "title" => "Name", "field" => "l.title" ),
		array( "type" => "textarea_query", "title" => "List Query", "field" => "l.list_query" ),
		array( "type" => "text", "title" => "Order", "field" => "l.sort_order" ),
		array( "type" => "text", "title" => "Primärschlüssel", "field" => "l.delete_primary_key" ),
		array( "type" => "text", "title" => "Tabelle", "field" => "l.delete_table" ),
		array( "type" => "select_file", "title" => "Reload File", "field" => "l.file", "path" => "admin/", "pattern" => "*.php" ),
		array( "type" => "select_file", "title" => "zurück File", "field" => "l.back_up_file", "path" => "admin/", "pattern" => "*.php" ),
		array( "type" => "select", "title" => "zurück zu Liste", "field" => "list_title", "where_field" => "l2.title", "save_field" => "back_up_list", "select_query" => "SELECT list_id AS back_up_list, title AS list_title FROM CORE_LISTS ORDER BY title", "select_id" => "back_up_list" ),
		array( "type" => "checkbox", "title" => "Umschaltbar", "field" => "l.switchable" ),
		array( "type" => "checkbox", "title" => "Zeilen auswählbar", "field" => "l.lines_selectable" ),
		array( "type" => "checkbox", "title" => "Einfügen", "field" => "l.insert_allowed" ),
		array( "type" => "checkbox", "title" => "Ändern", "field" => "l.change_allowed" ),
		array( "type" => "checkbox", "title" => "Löschen", "field" => "l.delete_allowed" ),
		array( "type" => "select", "title" => "Projekt", "field" => "project_title", "where_field" => "p.title", "save_field" => "project_id", "select_query" => "SELECT project_id, title AS project_title FROM CORE_PROJECTS", "select_id" => "project_id", "fill_fix_type" => "PARAMETER", "fill_fix_param" => "l.project_id" ),
	),
	"list_query" => "
		SELECT l.*, p.title AS project_title, l2.title AS list_title
		FROM CORE_LISTS AS l
		LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=l.project_id)
		LEFT JOIN CORE_LISTS AS l2 ON (l2.list_id=l.back_up_list)",
	"sort_order" => "title ASC",
	"delete_primary_key" => "list_id",
	"delete_table" => "CORE_LISTS",
	"file" => "core_list_redirect.php",
	"action_buttons" => array( BUTTON_BUTTONS_ZUORDNEN, BUTTON_FELDER ),
	"back_up_file" => "core_projects.php",
	"back_up_list" => 0,
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
);

$_LISTEN[LIST_CORE_PROJECTS_ACTION_BUTTONS] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "Button ID", "field" => "button_id" ),
		array( "type" => "text", "title" => "Name", "field" => "l.title" ),
		array( "type" => "text", "title" => "Beschreibung", "field" => "description" ),
		array( "type" => "select_file", "title" => "Bild", "field" => "picture", "path" => "themes/_default/icons/", "pattern" => "*.png", "show_picture" => true ),
		array( "type" => "select", "title" => "Projekt", "field" => "project_title", "where_field" => "p.title", "save_field" => "project_id", "select_query" => "SELECT project_id, title AS project_title FROM CORE_PROJECTS", "select_id" => "project_id", "fill_fix_type" => "PARAMETER", "fill_fix_param" => "l.project_id" ),
	),
	"list_query" => "
		SELECT l.*, p.title AS project_title
		FROM CORE_ACTION_BUTTONS AS l
		LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=l.project_id)",
	"sort_order" => "title ASC",
	"delete_primary_key" => "button_id",
	"delete_table" => "CORE_ACTION_BUTTONS",
	"file" => "core_list_redirect.php",
	"back_up_file" => "core_projects.php",
	"back_up_list" => 0,
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
);

$_LISTEN[LIST_CORE_PROJECTS_LISTS_BUTTONS] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "List Button ID", "field" => "list_button_id" ),
		array( "type" => "integer", "title" => "Pos", "field" => "pos", "fill_fix_type" => "AUTOINC", "fill_fix_param" => "CORE_LISTS_BUTTONS AS l" ),
		array( "type" => "select", "title" => "Button", "field" => "button_title", "where_field" => "b.title", "save_field" => "button_id", "select_query" => "SELECT button_id, title AS button_title FROM CORE_ACTION_BUTTONS ORDER BY title", "select_id" => "button_id" ),
		array( "type" => "select_file", "title" => "Weiterleitung", "field" => "lb.file", "path" => "admin/", "pattern" => "*.php" ),
		array( "type" => "select", "title" => "Weiterleitung Liste", "field" => "file_list_title", "where_field" => "l2.title", "save_field" => "file_list_id", "select_query" => "SELECT list_id AS file_list_id, title AS file_list_title FROM CORE_LISTS ORDER BY title", "select_id" => "file_list_id" ),
		array( "type" => "checkbox", "title" => "Button für alle Zeilen", "field" => "main_button" ),
		array( "type" => "select", "title" => "Liste", "field" => "list_title", "where_field" => "l.title", "save_field" => "list_id", "select_query" => "SELECT list_id, title AS list_title FROM CORE_LISTS", "select_id" => "list_id", "fill_fix_type" => "PARAMETER", "fill_fix_param" => "l.list_id" ),
	),
	"list_query" => "
		SELECT lb.*, b.title AS button_title, l.title AS list_title, l2.title AS file_list_title
		FROM CORE_LISTS_BUTTONS AS lb
		LEFT JOIN CORE_ACTION_BUTTONS AS b ON (b.button_id=lb.button_id)
		LEFT JOIN CORE_LISTS AS l ON (l.list_id=lb.list_id)
		LEFT JOIN CORE_LISTS AS l2 ON (l2.list_id=lb.file_list_id)",
	"sort_order" => "pos ASC",
	"delete_primary_key" => "list_button_id",
	"delete_table" => "CORE_LISTS_BUTTONS",
	"file" => "core_list_redirect.php",
	"back_up_file" => "core_list_redirect.php",
	"back_up_list" => LIST_CORE_PROJECTS_LISTS,
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
);

$_LISTEN[LIST_CORE_PROJECTS_LISTS_FIELDS] = array(
	"fields" => array(
		array( "type" => "integer", "title" => "Feld ID", "field" => "field_id" ),
		array( "type" => "integer", "title" => "Pos", "field" => "pos", "fill_fix_type" => "AUTOINC", "fill_fix_param" => "CORE_LISTS_FIELDS AS l" ),
		array( "type" => "select", "title" => "Feldtyp", "field" => "type", "save_field" => "type", "select_query" => "SELECT type FROM CORE_FIELDTYPS ORDER BY type", "select_id" => "type" ),
		array( "type" => "text", "title" => "Titel", "field" => "l.title" ),
		array( "type" => "checkbox", "title" => "ReadOnly", "field" => "l.read_only" ),
		array( "type" => "text", "title" => "Feld", "field" => "field" ),
		array( "type" => "checkbox", "title" => "Sichtbar", "field" => "visible" ),
		array( "type" => "checkbox", "title" => "in Kurz", "field" => "in_kurz" ),
		array( "type" => "checkbox", "title" => "Pflicht", "field" => "mandatory" ),
		array( "type" => "text", "title" => "Save Feld", "field" => "save_field" ),
		array( "type" => "text", "title" => "Where Feld", "field" => "where_field" ),
		array( "type" => "textarea_query", "title" => "Select Query", "field" => "select_query" ),
		array( "type" => "text", "title" => "Select ID", "field" => "select_id" ),
		array( "type" => "text", "title" => "Path", "field" => "path" ),
		array( "type" => "text", "title" => "Pattern", "field" => "pattern" ),
		array( "type" => "checkbox", "title" => "Bild in Select", "field" => "show_picture" ),
		array( "type" => "text", "title" => "Funktion", "field" => "function" ),
		array( "type" => "select", "title" => "Fillfix Typ", "field" => "fill_fix_type", "save_field" => "fill_fix_type", "select_query" => "SELECT fill_fix_type FROM CORE_FILLFIXTYPS ORDER BY fill_fix_type", "select_id" => "fill_fix_type" ),
		array( "type" => "text", "title" => "Fillfix Parameter", "field" => "fill_fix_param" ),
		array( "type" => "select", "title" => "Textfarbe", "field" => "color", "save_field" => "color", "select_query" => "SELECT color FROM CORE_COLORS ORDER BY color", "select_id" => "color" ),
		array( "type" => "select", "title" => "Liste", "field" => "list_title", "where_field" => "l.title", "save_field" => "list_id", "select_query" => "SELECT list_id, title AS list_title FROM CORE_LISTS", "select_id" => "list_id", "fill_fix_type" => "PARAMETER", "fill_fix_param" => "l.list_id" ),
	),
	"list_query" => "
		SELECT l.*, l2.title AS list_title
		FROM CORE_LISTS_FIELDS AS l
		LEFT JOIN CORE_LISTS AS l2 ON (l.list_id=l2.list_id)",
	"sort_order" => "pos ASC",
	"delete_primary_key" => "field_id",
	"delete_table" => "CORE_LISTS_FIELDS",
	"file" => "core_list_redirect.php",
	"back_up_file" => "core_list_redirect.php",
	"back_up_list" => LIST_CORE_PROJECTS_LISTS,
	"insert_allowed" => true,
	"change_allowed" => true,
	"delete_allowed" => true,
);

?>