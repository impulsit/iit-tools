<?php // <!-- GROUP: PRIVATE -->
// mysql
define( "MYSQL_SERVER", "localhost" );
define( "MYSQL_USER", "root" );
define( "MYSQL_PASSWORD", "" );
define( "MYSQL_DB", "basilica" );

// Fileserver
define( "SUBDIR", "" );
define( "FILE_SAVE_DIR", "d:/_files" );
define( "FILE_SAVE_DIR_FOR_BATCH", "y:\_files" );
define( "WINWORD", "C:\Program Files (x86)\Microsoft Office\Office14\winword.exe" );

// Domain
define( "DOMAIN", "http://192.168.178.2/".SUBDIR."" );

define( "BASE_DIR", "D:/_www/".SUBDIR );

// Logo
define( "LOGO_FILE", "logo_basilica.png" );

// Titel
define( "TITEL", "Basilica" );
define( "META", "Basilica" );
?>