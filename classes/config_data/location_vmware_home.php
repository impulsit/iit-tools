<?php // <!-- GROUP: OLD -->
// mysql
define( "MYSQL_SERVER", "localhost" );
define( "MYSQL_USER", "root" );
define( "MYSQL_PASSWORD", "" );
define( "MYSQL_DB", "development" );

// Fileserver
define( "SUBDIR", "" );
define( "FILE_SAVE_DIR", "C:/_www/_files" );
define( "FILE_SAVE_DIR_FOR_BATCH", "\\\\VM-WIN-2\\_www\\_files" );

// Domain
define( "DOMAIN", "http://VM-WIN-2/".SUBDIR."" );
define( "BASE_DIR", "C:/_www/".SUBDIR );

// Logo
define( "LOGO_FILE", "logo_impulsIT.png" );
define( "EXT_LOGO1", "http://www.sordje.at/basilica/basilica_logo_1.png" );
define( "EXT_LOGO2", "http://www.sordje.at/basilica/basilica_logo_2.png" );

// Titel
define( "TITEL", "impuls IT Tools - ".VERSION );
define( "META", "" );
define( "COPYRIGHT", "copyright by at impuls IT, www.impulsit.at" );
?>