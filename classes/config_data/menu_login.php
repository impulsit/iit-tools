<?php
require_once( CLASS_DIR."mysql.php" );

$db = mysql::getInstance();

$db->query( "SELECT register_allowed, impressum_link FROM CORE_SETUP WHERE id='1'" );
$r = $db->getNext();

$_MENU_ADMIN_LOGIN = array();
$_MENU_ADMIN_LOGIN[] = array( "title" => "Login", "url" => "/".SUBDIR."admin/login_login.php", "pic" => "login.png", "min_user_level" => 0 );

if( $r['register_allowed'] == 1 ) {
	$_MENU_ADMIN_LOGIN[] = array( "title" => "Passwort vergessen", "url" => "/".SUBDIR."admin/login_pw_lost.php", "pic" => "pw_lost.png", "min_user_level" => 0 );
	$_MENU_ADMIN_LOGIN[] = array( "title" => "Registrieren", "url" => "/".SUBDIR."admin/login_register.php", "pic" => "register.png", "min_user_level" => 0 );
} // if

if( $r['impressum_link'] != "" )
	$_MENU_ADMIN_LOGIN[] = array( "title" => "Impressum", "url" => $r['impressum_link'], "pic" => "info_blue.png", "min_user_level" => 0 );
?>