<?php
require_once( 'config_data.php' );
require_once( CLASS_DIR.'mysql.php' );

class report {
	static private $instance = null;
	private $db = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_report_id( $report_text ) {
		$i = 0;

		if( $this->db->query( "SELECT id FROM BAS_REPORTS WHERE report_constant LIKE '".$report_text."'", "get_report_id" ) ) {
			$r = $this->db->getNext( "get_report_id" );
			$i = $r['id'];
		} else
			echo "Report ID für Report ".$report_text." nicht gefunden.<br>";
		return( $i );
	} // get_report_id

	function get_report_name( $report_id ) {
		switch( $report_id ) {
			case 49: return( 'WAFF Erlagschein Teiln.' );
			case 48: return( 'WAFF Rechnung Teiln.' );
			case 26: return( 'Kostenvoranschlag' );
			case 23: return( 'Kurszeitbestätigung AMS' );
			case 25: return( 'Rechnung komplett' );
			case 46: return( 'Teilnahmebestätigung' );
			case 44: return( 'Anwesenheitsliste/Protokoll/Kran/Stapler' );
			case 24: return( 'WAFF Zahlungsbestätigung Teiln.' );
		} // switch

		if( $this->db->query( "SELECT title FROM BAS_REPORTS WHERE id='".$report_id."'", "get_report_name" ) ) {
			$r = $this->db->getNext( "get_report_name" );

			$str = $r['title'];
		} else
			$str = "Report ID ".$report_id. " nicht gefunden.";

		return( $str );
	} // get_report_name

} // report
?>