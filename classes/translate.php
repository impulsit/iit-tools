<?php
require_once( CLASS_DIR.'mysql.php' );

class translate {
	static private $instance = null;
	private $db = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function update_display_language() {
		// Setze Sprache
		if( isset( $_SESSION['c_user_id'] ) && isset( $_SESSION['set_language'] ) ) {
			$_GET['language'] = $_SESSION['language'];
			unset( $_SESSION['set_language'] );
		} // if

		if( isset( $_GET['language'] ) ) {
			$bOK = false;

			if( $this->db->query( "SELECT layout_code FROM CORE_LANGUAGE_SETUP WHERE layout_code='".$_GET['language']."' AND active='1'") )
				$bOK = true;

			if( $bOK ) {
				$_SESSION['language'] = $_GET['language'];

				if( isset( $_SESSION['c_user_id'] ) ) {
					$this->db->update( "CORE_USER_INFO", array( "language" => $_GET['language'] ), "user_id='".$_SESSION['c_user_id']."'" );
					$this->db->commit();
				} else
					$_SESSION['set_language'] = true;
			} // if
		} // if

		// Lade vom User
		if( isset( $_SESSION['c_user_id'] ) && ($_SESSION['c_user_id'] > 0) ) {
			$this->db->query( "SELECT language FROM CORE_USER_INFO WHERE user_id='".$_SESSION['c_user_id']."'" );
			$u = $this->db->getNext();

			$_SESSION['language'] = $u['language'];
		} // if

		// aktuelle Sprache gültig?
		$bOK = false;
		if( isset( $_SESSION['language'] ) ) {
			if( $this->db->query( "SELECT layout_code FROM CORE_LANGUAGE_SETUP WHERE layout_code='".$_SESSION['language']."' AND active='1'") )
				$bOK = true;
		} // if

		// Default Sprache
		$this->db->query( "SELECT default_language FROM CORE_SETUP WHERE id='1'");
		$r = $this->db->getNext();
		$default_language = $r['default_language'];
		if( $default_language == "" ) $default_language = "de";

		if( !$bOK )
			$_SESSION['language'] = $default_language;
	} // update_display_language

	function t( $str, $arrReplace = array() ) {
		if( isset( $_SESSION['project_id'] ) && (($_SESSION['project_id'] == 1) || ($_SESSION['project_id'] == 2) || ($_SESSION['project_id'] == 3)) )
			return( $this->replace_variablen( $str, $arrReplace ) );

		if( $str == "" )
			return( $this->replace_variablen( $str, $arrReplace ) );

		if( !isset( $_SESSION['language'] ) ) {
			$this->db->query( "SELECT default_language FROM CORE_SETUP WHERE id='1'");
			$r = $this->db->getNext();
			$default_language = $r['default_language'];
			if( $default_language == "" ) $default_language = "de";

			$_SESSION['language'] = $default_language;
		} // if

		$bTranslate = true;

		if( substr( $_SERVER['REQUEST_URI'], 0, 12 + strlen( SUBDIR ) ) == '/'.SUBDIR.'admin/core_' ) $bTranslate = false;
		if( strpos( $_SERVER['REQUEST_URI'], "core_backend.php" ) !== false ) $bTranslate = false;
		if( strpos( $_SERVER['REQUEST_URI'], "core_entry.php" ) !== false ) $bTranslate = false;
		if( (strpos( $_SERVER['REQUEST_URI'], "core_entry.php" ) !== false) && ($_SESSION['project_id'] == 4) ) $bTranslate = true;

		if( !$bTranslate )
			return( $this->replace_variablen( $str, $arrReplace ) );

		// Übersetzung vorhanden? -> anlegen
		if( !$this->db->query( "SELECT id FROM CORE_LANGUAGE WHERE search='".$str."'", "t" ) ) {
			$this->db->insert( "CORE_LANGUAGE", array( "search" => $str, "de" => $str ) );
			$this->db->commit();
		} // if

		// Übersetzung laden
		$this->db->query( "SELECT ".$_SESSION['language']." FROM CORE_LANGUAGE WHERE search='".$str."'", "t" );
		$l = $this->db->getNext( "t" );

		if( $l[$_SESSION['language']] == '' )
			return( '['.$str.']' );

		return( $this->replace_variablen( $l[$_SESSION['language']], $arrReplace ) );
	} // t

	function replace_variablen( $str, $arrReplace ) {
		if( !is_array( $arrReplace ) )
			return( $str );

		for( $i=sizeof( $arrReplace ); $i>=1; $i-- ) {
			$str = str_replace( '%'.$i, $arrReplace[$i-1], $str );
		} // for

		return( $str );
	} // replace_variablen
} // translate
?>