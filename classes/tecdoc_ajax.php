<?php
require_once( dirname( __FILE__ ).'/ajax_header.php' );
require_once( dirname( __FILE__ ).'/tecdoc.php' );
require_once( dirname( __FILE__ ).'/tecdoc_web_api.php' );

$tec = tecdoc::getInstance();
$ws = tecdoc_web_api::getInstance();

$result = array();

switch( $_POST['action'] ) {
	case "load_model":
			if( !isset( $_POST['to'] ) ) $_POST['to'] = 0;
			$result = $tec->get_model( $_POST['manufacturer'], $_POST['from'], $_POST['to'] );
		break;
	case "load_vehicle":
			$result = $tec->get_vehicle( $_POST['manufacturer'], $_POST['model'], $_POST['filter_fueltype'], $_POST['filter_pskw'], $_POST['filter_motorcode'] );
		break;
	case "load_motor_vehicle":
			$result = $tec->get_vehicle_by_motor_code( $_POST['motor_code'] );
		break;
	case "load_fin_vehicle":
			$result = $tec->get_vehicle_by_fin( $_POST['fin'] );
		break;
	case "load_categories":
			$result = $tec->get_categories( $_POST['vehicle'], $_POST['shortcut'] );
		break;
	case "load_shortcuts":
			$result = $tec->get_shortcuts( $_POST['vehicle'] );
		break;
	case "load_articles":
			unset( $_SESSION['tecdoc']['search_by_customerno'] );
			if( isset( $_POST['customer_no'] ) ) $_SESSION['tecdoc']['search_by_customerno'] = $_POST['customer_no'];

			if( isset( $_POST['pattern'] ) ) {
				if( !isset( $_POST['similar'] ) ) $_POST['similar'] = 0;

				$db->update( "CORE_USER_INFO",
					array(
						"search_show_attributes" => $_POST['search_show_attributes'],
						"search_only_on_stock" => $_POST['search_only_on_stock'],
					), "user_id='".$_SESSION['c_user_id']."'" );
				$db->commit();

				$result = $tec->get_articles_by_pattern( $_POST['pattern'], $_POST['pattern_type'], $_POST['similar'] );

				// Suche Loggen
				$str = '';
				foreach( $result as $k => $v ) {
					if( $str != "" ) $str .= ',';
					$str .= '('.$v['no'].')';
				} // foreach
				$item_no = str_replace( ' (*)', '', $_POST['pattern'] );

				$db->insert( "TEC_LOG_SEARCH", array(
						"user_id" => $_SESSION['c_user_id'],
						"act_time" => $f->d( time() ),
						"action" => 0,
						"search_pattern" => $item_no,
						"search_result" => str_replace( ' (*)', '', $str )
				) );
				$db->commit();
			} else {
				$db->query( "SELECT * FROM TEC_SETUP" );
				$td_setup = $db->getNext();

				if( isset( $_POST['manufacturer'] ) ) $type = $td_setup['td_linkingtargettype']; else $type = 'U';
				if( !isset( $_POST['brand'] ) ) $_POST['brand'] = null;
				if( !isset( $_POST['vehicle'] ) ) $_POST['vehicle'] = null;
				if( !isset( $_POST['category'] ) ) $_POST['category'] = null;
				if( !isset( $_POST['manufacturer'] ) ) $_POST['manufacturer'] = null;
				if( !isset( $_POST['model'] ) ) $_POST['model'] = null;

				$result = $tec->get_articles( $_POST['vehicle'], $_POST['category'], $_POST['manufacturer'], $_POST['model'], $type, $_POST['brand'] );
			} // else
		break;
	case "buy_article":
		if( !isset( $_POST['sub_shop'] ) ) $_POST['sub_shop'] = 0;

		$result = $tec->add_item_to_shopping( $_POST['artikel_link_id'], $_POST['artikel_id'], $_POST['quantity'], $_POST['manufacturer'], $_POST['model'], $_POST['vehicle'], $_POST['brand'], $_POST['brand_id'], $_POST['no'], $_POST['replace_id'], $_POST['location'], $_POST['location_name'], $_POST['sub_shop'] );

			// Kaufen Loggen
			$item_no = str_replace( ' (*)', '', $_POST['no'] );

			$id = $db->insert( "TEC_LOG_SEARCH", array(
					"user_id" => $_SESSION['c_user_id'],
					"act_time" => $f->d( time() ),
					"action" => 1,
					"buyed_article_no" => $item_no
			) );

			// Eigentlich bei Warenkorb bestellen
			$db->query( "
				UPDATE TEC_LOG_SEARCH
				SET buyed_from_result='".$item_no."', buy_id='".$id."'
				WHERE
					action='0' AND
					search_result LIKE '%(".$item_no.")%' AND
					user_id='".$_SESSION['c_user_id']."' AND
					buyed_from_result='' AND
					DATE(act_time)=DATE(NOW())" );
			$db->commit();

			// Protokoll schreiben
			if( $_POST['active_search'] == "4" ) {
				$db->query( "SELECT user_id, name, topmotive_kundennr, last_vin FROM CORE_USER_INFO WHERE user_id='".$_SESSION['c_user_id']."'");
				$userinfo = $db->getNext();

				$db->insert( "TEC_VIN_PROTOCOL", array(
					"id" => '',
					"vin" => $userinfo['last_vin'],
					"search_date" => $f->d( time() ),
					"user_id" => $userinfo['user_id'],
					"user_description" => $userinfo['name'],
					"user_erp_no" => $userinfo['topmotive_kundennr'],
					"audatex_vehicle" => $_POST['audatex_vehicle'],
					"audatex_oe" => $_POST['audatex_oe'],
					"audatex_item_description" => $_POST['audatex_item_description'],
					"td_manufacturer_id" =>  $_POST['brand_id'],
					"td_manufacturer" => $_POST['brand'],
					"td_item_no" => $_POST['no'],
					"td_item_description" => $_POST['name'],
				)	);
				$db->commit();
			} // if
		break;
	case "load_orders":
			$result = $tec->load_orders();
		break;
	case "load4_categories":
			$result = $tec->get_categories_by_pattern( $_POST['pattern'] );
		break;
	case "load5_categories":
			$result = $tec->get_categories_by_brand( $_POST['brand'] );
		break;
	case "load_linked_vehicles":
			$result = $tec->get_linked_vehicles( $_POST['articleId'], $_POST['manufacturer'], $_POST['vehicle'], $count, $_POST['start'], $limit );
			$result['0']['anz'] = $count;
			$result['0']['limit'] = $limit;
			break;
	case "save_korb":
			if( !isset( $_POST['document_type'] ) ) $_POST['document_type'] = "";
			if( !isset( $_POST['delivery_method'] ) ) $_POST['delivery_method'] = "";
			if( !isset( $_POST['delivery_method_date'] ) ) $delivery_method_date = "0000-00-00 00:00:00"; else $delivery_method_date = date( "Y-m-d H:i:s", strtotime( $_POST['delivery_method_date'] ) );

			$db->update( "CORE_USER_INFO", array(
				"warenkorb_header_info" => $_POST['text'],
				"warenkorb_document_type" => $_POST['document_type'],
				"warenkorb_delivery_method" => $_POST['delivery_method'],
				"warenkorb_delivery_method_date" => $delivery_method_date,
			), "user_id='".$_SESSION['c_user_id']."'" );
			$db->commit();

			$result = "saved";
		break;
	case "get_infos":
			unset( $_SESSION['tecdoc']['search_by_customerno'] );
			if( isset( $_POST['customer_no'] ) ) $_SESSION['tecdoc']['search_by_customerno'] = $_POST['customer_no'];
			if( !isset( $_POST['subcategory_id_filter'] ) ) $_POST['subcategory_id_filter'] = 0;
			if( !isset( $_POST['start'] ) ) $_POST['start'] = 1;

			$result = $tec->get_articles_infos( $_POST['text'], $_POST['subcategory_id_filter'], $_POST['start'],
					$_POST['filter_width'], $_POST['filter_height'], $_POST['filter_length'], $_POST['filter_diameter'], $_POST['filter_viscosity'],
					$_POST['filter_kw'], $_POST['filter_ah'], $_POST['filter_supplier'], $_POST['filter_belt_shafts'], $_POST['filter_volt'], $_POST['filter_amper'], $_POST['filter_season']
			);
		break;
	case "print_order":
			$result = $tec->print_order( $_POST['id'] );
		break;
	case "get_manufacturers":
			if( $_POST['all'] == "false" ) $_POST['all'] = false;
			$result = $tec->get_manufacturer_select( 0, $_POST['all'] );
		break;
	case "save_history":
			if( !isset( $_POST['all_manufacturers'] ) )
				$_POST['all_manufacturers'] = 0;
			$tec->save_search_history( $_POST['search_manufacturer'], $_POST['year_from'], $_POST['search_model'], $_POST['filter_fueltype'], $_POST['filter_pskw'], $_POST['filter_motorcode'], $_POST['search_vehicle'], $_POST['all_manufacturers'], $_POST['text'] );
		break;
	case "restore_history":
			$result = $tec->restore_search_history( $_POST['id'] );
		break;
	case "reload_history":
			$result = $tec->reload_search_history();
		break;
	case "translate":
			$result = $t->t( $_POST['input'] );
		break;
	case "open_3d_search":
			$taskId = $ws->get_fin_audatex_taskId( $_POST['fin'] );
			$url = $ws->get_fin_audatex_url( $taskId );

			$id = $db->insert( "TEC_LOG_SEARCH", array(
					"user_id" => $_SESSION['c_user_id'],
					"act_time" => $f->d( time() ),
					"action" => 2,
					"search_pattern" => $_POST['fin']
			) );
			$db->commit();

			$result = array( "task_id" => $taskId, "url" => $url );
		break;
	case "import_from_3d_search":
		$task = $ws->get_fin_audatex_gettask( '' );

		$result = array( "task" => $task );
		break;
} // switch

$return['resultData'] = $result;

echo json_encode( $f->convert_ajax_remove( $return ) );
?>