<?php
require_once( CLASS_DIR.'mysql.php' );

class update {
	static private $instance = null;
	private $db = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function new_main_menu_button( $iProjectID, $iPos, $strName, $strPicturePath, $strRedirect, $iRedirectList ) {
		if( !$this->db->query( "SELECT menu_id FROM CORE_MAINMENU WHERE project_id='".$iProjectID."' AND title='".$strName."'" ) ) {
			$this->db->insert( "CORE_MAINMENU", array(
				"project_id" => $iProjectID,
				"title" => $strName,
				"picture" => $strPicturePath,
				"file" => $strRedirect,
				"pos" => $iPos,
				"list_id" => $iRedirectList
			) );
			$this->db->commit();
		} // if
	} // new_main_menu_button

	function renumber_pos( $list_id ) {
		global $db;

		$i = 0;
		$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE list_id='".$list_id."' ORDER BY pos ASC", "renumber_pos" );
		while( $db->isNext( "renumber_pos" ) ) {
			$r = $db->getNext( "renumber_pos" );

			$i += 10;

			$db->update( "CORE_LISTS_FIELDS", array( "pos" => $i ), "field_id='".$r['field_id']."'" );
		} // while
		$db->commit();
	} // renumber_pos

	function add_new_field( $data ) {
		// Feld in Datenbank einfügen
		if( isset( $data['db'] ) ) {
			$this->db->query( "SELECT * FROM ".$data['db']['table']." LIMIT 1", "add_new_field" );
			$r = $this->db->getNext( "add_new_field" );
			if( !isset( $r[$data['field']['field']] ) )
				$this->db->query( "ALTER TABLE `".$data['db']['table']."` ADD `".$data['field']['field']."` ".$data['db']['fieldtype']." NOT NULL", "add_new_field" );
		} // if

		// Feld bei Liste einfügen
		if( !$this->db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE field LIKE '".$data['field']['field']."' AND list_id='".$data['field']['list_id']."'", "add_new_field" ) ) {
			$field = "";
			$value = "";
			foreach( $data['field'] as $k => $v ) {
				if( $field != "" )
					$field .= ', ';
				$field .= '`'.$k.'`';

				if( $value != "" )
					$value .= ', ';
				$value .= "'".$v."'";
			} // foreach
			$field = '`field_id`, '.$field;
			$value = "'', ".$value;

			$this->db->query( "INSERT INTO `CORE_LISTS_FIELDS` (".$field.") VALUES (".$value.")" );
		} // if

		$this->db->commit();
	} // add_new_field

	function add_new_field_only_database( $data ) {
		// Feld in Datenbank einfügen
		$this->db->query( "SELECT * FROM ".$data['table']." LIMIT 1", "add_new_field_only_database" );
		$r = $this->db->getNext( "add_new_field_only_database" );
		if( !isset( $r[$data['field']] ) ) {
			$this->db->query( "ALTER TABLE `".$data['table']."` ADD `".$data['field']."` ".$data['fieldtype']." NOT NULL", "add_new_field_only_database" );

			// Init Wert setzen
			if( isset( $data['init_value'] ) && ($data['init_value'] != "") )
				$this->db->update( $data['table'], array( $data['field'] => $data['init_value'] ), $data['where'] );
		} // if

		$this->db->commit();
	} // add_new_field_only_database

	function insert_record( $data ) {
		// Feld bei Liste einfügen
		if( !$this->db->query( "SELECT ".$data['check_field']." FROM ".$data['table']." WHERE ".$data['check_field']."='".$data['field'][$data['check_field']]."'", "add_new_field" ) ) {
			$field = "";
			$value = "";
			foreach( $data['field'] as $k => $v ) {
				if( $field != "" )
					$field .= ', ';
				$field .= '`'.$k.'`';

				if( $value != "" )
					$value .= ', ';
				$value .= "'".$v."'";
			} // foreach

			$this->db->query( "INSERT INTO `".$data['table']."` (".$field.") VALUES (".$value.")" );
		} // if

		$this->db->commit();
	} // insert_record

	function get_latest_button_id() {
		$this->db->query( "SELECT MAX( button_id ) AS button_id FROM CORE_ACTION_BUTTONS" );
		$r = $this->db->getNext();

		return( $r['button_id'] + 1 );
	} // get_latest_button_id

	function write_change( $strFile ) {
		echo "Write Changelog: ".BASE_DIR.'admin/update/'.$strFile."...<br>";

		//$strVersion = substr( $strFile, 7, 4 );
		$strVersion = $this->get_version_no_from_version( $strFile );

		if( $this->db->query( "SELECT version FROM CORE_UPDATES WHERE version='".$strVersion."'", "write_change" ) ) {
			$this->db->update( "CORE_UPDATES", array(
				"processed" => 1,
				"installed" => date( "Y-m-d H:i:s", time() )
			), "version='".$strVersion."'" );
		} else {
			$this->db->insert( "CORE_UPDATES",
				array(
					"version" => $strVersion,
					"update_file" => $strFile,
					"processed" => 1,
					"installed" => date( "Y-m-d H:i:s", time() )
				) );
		} // else

		$this->db->commit();
	} // write_change

	function insert_update_file( $strFile ) {
		//$strVersion = substr( $strFile, 7, 4 );
		$strVersion = $this->get_version_no_from_version( $strFile );

		if( $strVersion < '2.00' )
			return;

		if( !$this->db->query( "SELECT version FROM CORE_UPDATES WHERE version='".$strVersion."'", "insert_update_file" ) ) {
			$this->db->insert( "CORE_UPDATES",
				array(
					"version" => $strVersion,
					"update_file" => $strFile,
				) );
		} // else

		$this->db->commit();
	} // insert_update_file

	function check_updates_to_process( $current_version ) {
		// Alle Update Files in Datenbank
		$handle = opendir( BASE_DIR.'admin/update/' );
		while( $datei = readdir( $handle ) ) {
			if( (pathinfo( $datei, PATHINFO_EXTENSION) == "php") && ($datei != ".") && ($datei != "..") && ($datei != "index.php") ) {
				$this->insert_update_file( basename( $datei ) );
			} // if
		} // while

		$this->db->delete( "CORE_UPDATES", "version=''" );

		// Check Update Ausführung
		$this->db->query( "
			SELECT update_file
			FROM CORE_UPDATES
			WHERE (processed='0') OR (version='".$current_version."')
			ORDER BY version ASC", "check_updates_to_process" );
		while( $this->db->isNext( "check_updates_to_process" ) ) {
			$rrr = $this->db->getNext( "check_updates_to_process" );

			echo "process Update: ".BASE_DIR.'admin/update/'.$rrr['update_file']."...<br>";

			include( BASE_DIR.'admin/update/'.$rrr['update_file'] );
			$this->write_change( $rrr['update_file'] );
		} // while
	} // check_updates_to_process

	function get_version_no_from_version( $versionstring ) {
		$versionstring = str_replace( ".php", "", $versionstring );

		$i = strpos( $versionstring, "_" );
		$j = strpos( $versionstring, "_", $i + 1 );

		if( $j > $i )
			$v = substr( $versionstring, $i + 1, $j - $i - 1);
		else
			$v = substr( $versionstring, $i + 1);

		return( $v );
	}	// get_version_no_from_version
} // update
?>