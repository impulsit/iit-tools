<?php
class kalender {
	static private $instance = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function print_kalender( $start, $ende, $output ) {
		setlocale(LC_TIME, 'deu', 'de_DE');

		$strOut = '';

		$strOut .= '
			<style>
				table.kal .chosen-container {
  			  font-size: 10px;
				}
				table.kal .chosen-results {
  			  font-size: 13px;
				}
				table.kal td, table.kal th { border: 1px solid black; width: 130px; height: 50px; padding: 2px; }
			</style>
		';
		$strOut .= '<table class="kal" style="border: none;">';

		// Wochentage
		$tag = array( "", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag" );
		$strOut .= '<tr><td style="border: none; width: 30px;" rowspan="2"></td><td style="border: none; width: 10px;" rowspan="2"></td>';
		for( $i=1; $i<=7; $i++ ) {
			$strOut .= '<th>'.$tag[$i].'</th>';
		} // for
		$strOut .= '</tr>';

		// Leerzeile
		$strOut .= '<tr><td colspan="7" style="border: 0px; height: 10px;"></tr>';

		// Wochenzeilen
		$s = strtotime( $start );
		$e = strtotime( $ende );

		while( $s <= $e ) {
			$curCol = 1;
			$weekNumber = date( "W", $s );
			$strOut .= '<tr><th style="width: 30px;">'.$weekNumber.'</th><td style="border: none; width: 10px;">';

			$weekday = date( "w", $s );
			$dateCol = $weekday;
			if( $dateCol == 0 ) $dateCol = 7;

			for( $i=1; $i<=7; $i++ ) {
				if( ($dateCol == $curCol) && ($s <= $e) ) {
					if( !isset( $output[$s] ) ) $output[$s] = "";

					$strOut .= '<td'.((date( "m", $s )%2==0)?' style="background: #999999;"':'').'><div style="display:block;width: 90px;">'.utf8_encode( strftime( "%d.%B %Y", $s ) ).'<br>'.$output[$s].'</div></td>';
					$s += 60*60*24;
				} else {
					$strOut .= '<td style="border: none;"></td>';
					$curCol++;
				} // else
			} // for
		} // while

		$strOut .= '</table>';

		return( $strOut );
	} // print_kalender
} // kalender
?>