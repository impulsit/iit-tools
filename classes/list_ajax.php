<?php
require_once( dirname( __FILE__ ).'/ajax_header.php' );

if( !isset( $_POST['current_list'] ) ) $_POST['current_list'] = -1;

if( $_POST['current_list'] > 0 ) {
	$_LISTEN[$_POST['current_list']] = $list->load_list( $_POST['current_list'] );
} // if

$list->set_list( $_POST['current_list'] );

switch( $_POST['action'] ) {
	case 1: // ändern
			if( isset( $_POST['param'] ) ) {
				$strOut = $list->edit( $_POST['param'] );
			} // if

			if( isset( $_GET['save'] ) ) {
				$list->save();

				$return['reload_file'] = basename( $list->file ).$list->get_query_string();
			} // if
		break;
	case 2: // Antworten kopieren von
			if( isset( $_POST['param'] ) ) {
				$strOut = $list->answer_copy_from_form( $_POST['param'] );
			} // if
			if( isset( $_GET['save'] ) ) {
				$list->answer_copy_from();

				$return['reload_file'] = basename( $list->file ).$list->get_query_string();
			} // if
		break;
	case 3: // Fragen kopieren von
			if( isset( $_POST['param'] ) ) {
				$strOut = $list->questions_copy_from_form( $_POST['param'] );
			} // if
			if( isset( $_GET['save'] ) ) {
				$list->questions_copy_from();

				$return['reload_file'] = basename( $list->file ).$list->get_query_string();
			} // if
		break;
	case 4: // Antworten kopieren nach
			if( isset( $_POST['param'] ) ) {
				$strOut = $list->answer_copy_to_form( $_POST['param'] );
			} // if
			if( isset( $_GET['save'] ) ) {
				$list->answer_copy_to();

				$return['reload_file'] = basename( $list->file ).$list->get_query_string();
			} // if
		break;
	case 5: // zu Kurs zuordnen
			if( isset( $_POST['param'] ) ) {
				$strOut = $list->form_zu_kurs_zuordnen( $_POST['param'] );
			} // if
			if( isset( $_GET['save'] ) ) {
				$list->zu_kurs_zuordnen();
				$return['reload_file'] = basename( $list->file ).$list->get_query_string();
			} // if
		break;
	case 6: // AMS Mail Beginn
	case 8: // AMS Mail Ende
		if( isset( $_POST['param'] ) ) {
			$strOut = $list->form_ams_mail( $_POST['param'] );
		} // if
		if( isset( $_GET['save'] ) && isset( $_POST['create_mail'] ) ) {
			$i = 0;

			$setup = $f->load_setup( "BAS_SETUP" );

			switch( $_POST['action'] ) {
				case 6: $report_id = $report->get_report_id( "REP_AMS_EMAIL_BEGINN" ); $title_prefix = "Kursbeginn für "; $bRechnung = false; break;
				case 8: $report_id = $report->get_report_id( "REP_AMS_EMAIL_ENDE" ); $title_prefix = "Kursende für "; $bRechnung = true; break;
			} // switch

			$db->query( "
				SELECT k.teilnehmer_id, t.svn, ams1.email AS ams_amt, ams2.email AS ams_betreuer
				FROM BAS_KURSTEILNEHMER AS k
				LEFT JOIN BAS_TEILNEHMER AS t ON (t.teilnehmer_id=k.teilnehmer_id)
				LEFT JOIN BAS_AMS_KONTAKTE AS ams1 ON (ams1.id=t.ams_amt_id)
				LEFT JOIN BAS_AMS_KONTAKTE AS ams2 ON (ams2.id=t.ams_betreuer_id)
				WHERE k.kurs_id='".$_POST['kurs_id']."'", "ajax_list_loop" );
			while( $db->isNext( "ajax_list_loop" ) ) {
				$r = $db->getNext( "ajax_list_loop" );

				$print->set_report( "teilnehmer_kurs", $r['teilnehmer_id'], $_POST['kurs_id'], 0, 0, $report_id );
				$print->generate_rtf();

				$title = $title_prefix.$print->get_teilnehmer( $r['teilnehmer_id'], false )." (SVNR: ".$r['svn'].")";
				$body = utf8_encode( $print->filecontens );

				$filename = "";
				$send_filename = "";
				if( $bRechnung ) {
					// Rechnung
//					$print->set_report( "teilnehmer_kurs", $r['teilnehmer_id'], $_POST['kurs_id'], 0, 0, 25 );
//					$print->generate_rtf();
//					$print->save_file();

					$send_filename = '';
					if( $db->query( "
						SELECT f.filename, r.datum, r.re_nr
						FROM BAS_LOG_FILES AS f
						LEFT JOIN BAS_AMS_RECHNUNGEN AS r ON (r.id=f.rechnung_id)
						WHERE f.rechnung_id!='0' AND f.teilnehmer_id='".$r['teilnehmer_id']."' AND f.kurs_id='".$_POST['kurs_id']."' AND f.bericht_id='".$report->get_report_id( "REP_AMS_RECHNUNG" )."'
						ORDER BY f.id DESC
						LIMIT 1", "ajax_list_loop_sub1" ) ) {
						$r2 = $db->getNext( "ajax_list_loop_sub1" );

						$filename = $r2['filename'];
						$send_filename = 'AR'.date( "Y", strtotime( $r2['datum'] ) )."-".str_pad( $r2['re_nr'], 4, "0", STR_PAD_LEFT );
					} // if
					// Teilnahmebestätigung
					//					$print->set_report( "teilnehmer_kurs", $r['teilnehmer_id'], $_POST['kurs_id'], 0, 0, 46 );
					//					$print->generate_rtf();
					//					$print->save_file();

					if( $db->query( "
						SELECT f.filename, r.datum, r.re_nr
						FROM BAS_LOG_FILES AS f
						LEFT JOIN BAS_AMS_RECHNUNGEN AS r ON (r.id=f.rechnung_id)
						WHERE f.teilnehmer_id='".$r['teilnehmer_id']."' AND f.kurs_id='".$_POST['kurs_id']."' AND f.bericht_id='46'
						ORDER BY f.id DESC
						LIMIT 1", "ajax_list_loop_sub1" ) ) {
						$r2 = $db->getNext( "ajax_list_loop_sub1" );

						$filename2 = $r2['filename'];
						$send_filename .= ';Teilnahmebestätigung';
					} // if
				} // if

				if( $send_filename != 'AR1970-0000' ) {
					$db->insert( "BAS_EMAIL_QUEUE", array(
						"receiver_to" => $r['ams_amt'],
						"receiver_cc" => $r['ams_betreuer'],
						"receiver_bcc" => $setup['ams_email_bcc'],
						"title" => $title,
						"filename" => $filename,
						"filename2" => $filename2,
						"email_status" => 0,
						"send_time" => "1970-01-01 00:00:00",
						"create_time" => $f->now(),
						"body" => $body,
						"sended" => 0,
						"error" => "",
						"send_filename" => $send_filename
					) );
					$db->commit();

					$i++;
				} // if
			} // while

			/*


			$anhang = "";
			if( $_POST['rechnung_id'] != 0 ) {
				$data = $print->get_rechnung_data( $_POST['rechnung_id'] );

				$anhang = array(
					"title" => $data['title'],
					"send_filename" => $data['send_filename'],
					"local_filename" => $data['local_filename'],
					"content" => $data['content'],
					"rechnung_id" => $data['rechnung_id']
				);
			} // if

			$f->log_mail( $anhang );
*/
			$return['message'] = $i." E-Mails wurden in die Warteschlange gestellt.";
		} // if
		break;
	case 7:
			if( isset( $_POST['param'] ) ) {
				$strOut = $list->form_emails_senden();
			} // if
		break;
	case 9: // Trainer zuordnen
		if( isset( $_POST['param'] ) ) {
			$strOut = $list->form_trainer_zuordnen( $_POST['param'] );
		} // if
		if( isset( $_GET['save'] ) ) {
			$list->trainer_zuordnen( $_POST['current_tab'] );
			$return['reload_file'] = basename( $list->file ).$list->get_query_string();
		} // if
		break;
	case 10: // Bild löschen
			$db->query( "SELECT link_image_table FROM CORE_LISTS_FIELDS WHERE field_id='".$_POST['field_id']."'" );
			$r = $db->getNext();

			$db->delete( $r['link_image_table'], "id='".$_POST['param']."'" );
			$db->commit();
		break;

	case 101: // TecDoc Show Article Detail
			$strOut = $tec->form_article_detail( $_POST['param'] );
		break;
} // switch

$return['resultData'] = $strOut;

echo json_encode( $f->convert_ajax_remove( $return ) );
?>