$(function() {
	$("#dialog_progress").hide();
}) // document ready

// Konstanten
var $_DELAY = 0; // Delay für Loop Aufruf
var $_MAX_INTERATIONS = 1000; // Abbruch nach Durchläufe

// Globals
var $iLoop = 0;
var $iMax = 0;
var $strProgressAction = "";
var $start_time = 0;

// Dialog anzeigen, Paramter: Function progressbar_sub_<paramter>.php
function open_progressbar( $progress_action ) {
	$strProgressAction = $progress_action;
	$iLoop = 0;
	$iMax = 0;
	$start_time = 0;

	$( "#dialog_progress" ).dialog({
		autoOpen: true,
		modal: true,
		width: 300,
		draggable: true,
		resizable: false,
		close: function( event, ui ) {
			$iLoop = $_MAX_INTERATIONS;
		}
	});

	setTimeout( run_loop_once, $_DELAY );
} // open_progressbar

// Einmal Durchlauf von Schleife, ruft sich selbst wieder auf, wenn kein Ende
function run_loop_once() {
	$bFinished = false;
	$iLoop++;

	get_dialog_updates();
	$bFinished = process();

	if( !$bFinished && ($iLoop < $_MAX_INTERATIONS) )
		setTimeout( run_loop_once, $_DELAY );
	else
		$( "#dialog_progress" ).dialog( 'destroy' );
} // run_loop_once

// hole aktuelle Texte und Prozente von php
function get_dialog_updates() {
	$function = "get_dialog_updates";

	$.ajax({
		method: "POST",
		url: '/classes/progressbar/classes/progressbar_ajax.php?'+$function+'/'+$iLoop,
		dataType: 'json',
		async: false,
		data: {
			progress_action: $strProgressAction,
			action: $function,
			current_loop: $iLoop,
			max: $iMax,
			start_time: $start_time,
			param: $( '#dialog_param' ).val()
		},
		success: function( result ) {
			for( $i=0; $i<=2; $i++ ) {
				set_text( $i, result.text[$i] );
			} // if
			set_percent( result.percent );
			set_loop( $iLoop, result.max, result.remaining );
			$iMax = result.max;
			$start_time = result.start_time;
		},
		error: function( result ) {
			console.log( strip_tags( result.responseText ) );
		},
	});
} // ajax_get_variables

// verarbeite php Schleife einmal
function process() {
	var $bEnd = false;

	$function = "process";
	$.ajax({
		method: "POST",
		url: '/classes/progressbar/classes/progressbar_ajax.php?'+$function+'/'+$iLoop,
		dataType: 'json',
		async: false,
		data: {
			progress_action: $strProgressAction,
			action: $function,
			current_loop: $iLoop,
			max: $iMax,
			start_time: $start_time,
			param: $( '#dialog_param' ).val()
		},
		success: function( result ) {
			$bEnd = result.end;
		},
		error: function( result ) {
			$bEnd = true;

			console.log( strip_tags( result.responseText ) );
		},
	});

	return( $bEnd );
} // process

// Dialog Update für Textzeilen
function set_text( $id, $text ) {
	if( ($text !== undefined) && ($text != "") )
		$( "#dialog_progress #dialog_text_"+$id ).html( $text + "<br>" );
} // set_text

// Dialog Update für Prozent
function set_percent( $percent ) {
	if( $percent !== undefined ) {
		$( "#dialog_progress #dialog_progressbar" ).progressbar({ value: parseInt( $percent ) });
		$( "#dialog_progress #dialog_progressbar span" ).html( $percent+"%" );
	} // if
} // set_percent

// Dialog Update für Loop
function set_loop( $loop, $max, $remaining ) {
	$( "#dialog_progress #dialog_loop" ).html( $loop );
	$( "#dialog_progress #dialog_max" ).html( $max );
	$( "#dialog_progress #dialog_remaining" ).html( $remaining );
} // set_percent
