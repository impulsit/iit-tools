<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8">
	<title>Progress Bar - Demo</title>

	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

	<script src="js/f.php"></script>
	<link rel="stylesheet" href="/progressbar/css/s.php">
</head>
<body>
<h1>Progressbar - Demo</h1>

<br />
Param: <input type="text" id="dialog_param" value="basilica/VERSION_2.38_(26.06.2015).zip"><br /><br />

<button onClick="open_progressbar( 'demo_sleep' );">Sleep</button>
<button onClick="open_progressbar( 'download_file' );">Download File</button>

<div id="dialog_progress" title="Dialog">
	<div id="dialog_text_0"></div>
	<div id="dialog_text_1"></div>
	<div id="dialog_text_2"></div>

	<br />
	<table width="100%" border="1" cellspacing="0" style="margin-bottom: 2px;">
		<tr>
			<th align="left" width="34%">Data:</th>
			<td align="right" width="33%"><span id="dialog_loop">0</span></td>
			<td align="right" width="33%"><span id="dialog_max">0</span></td>
		</tr>
		<tr>
			<th align="left">Remaining:</th>
			<td align="right" colspan="2"><span id="dialog_remaining">00:00:00</span></td>
		</tr>
	</table>
	<div id="dialog_progressbar"><span></span></div>
</div>
</body>
</html>