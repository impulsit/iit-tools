<?php
require_once( "progressbar.php" );

$result = array();
$progress_action = $_POST['progress_action'];
$action = $_POST['action'];
$current_loop = $_POST['current_loop'];
$start_time = $_POST['start_time'];
$param = $_POST['param'];
$max = $_POST['max'];

$pb = progressbar::getInstance();
$pb->progress_action = $progress_action;
$pb->current_loop = $current_loop;
$pb->start_time = $start_time;
$pb->param = $param;
$pb->max = $max;
$pb->load();

switch( $action ) {
	case "get_dialog_updates":
		$result['start_time'] = $pb->get_start_time();
		$result['text'] = $pb->get_text();
		$result['percent'] = $pb->get_percent();
		$result['max'] = $pb->get_max();
		$result['remaining'] = $pb->get_remaining();
		break;
	case "process":
		$result['end'] = $pb->process();
		break;
} // switch

echo json_encode( $pb->convert_ajax_remove( $result ) );
?>