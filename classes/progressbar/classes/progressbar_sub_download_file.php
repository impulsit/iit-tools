<?php
class progressbar_sub {
	static private $instance = null;
	public $current_loop = 0;
	public $max = 0;
	public $max_filesize = 0;
	public $param = null;
	public $segments = 100;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_max() {
		$size = get_headers( "http://iit-tools.sordje.at/updates/".$this->param, 1);

		if( $this->max <= 0 ) {
			$this->max_filesize = $size['Content-Length'];
			$this->max = $this->segments;
		} // if

		return( $this->max );
	} // init_max

	function get_text() {
		// 3 Textzeilen für Statusinfo
		$this->get_max();

		return( array(
			$this->param,
			(($this->current_loop - 1) * floor( $this->max_filesize / $this->segments ))." / ".$this->max_filesize,
			""
		) );
	} // get_text

	function process() {
		$bEnd = false;

		if( $this->current_loop >= $this->get_max() )
			$bEnd = true;

		// Processing
//		$local_file_size = filesize( "C:/_www/test2.zip" );

		$iBytes = floor( $this->max_filesize / $this->segments );
		$iOffset = ($this->current_loop - 1) * $iBytes;
//		if( $this->current_loop >= $this->segments )
//			$iBytes = -1;

		$conn_id = ftp_connect( "ftp.sordje.at" );
		$login_result = ftp_login( $conn_id, "290652-iit_tools", "-iittools123" );

//		$filePart = file_get_contents( $this->param, false, null, $iOffset, $iBytes);

//		ftp_get( $conn_id, "C:/_www/test2.zip", $this->param, FTP_BINARY, $local_file_size );
/*
		$stream = fopen( $this->param, 'r' );
		$content = stream_get_contents( $stream, $iBytes, $iOffset );
		fclose($stream);

		$fileOutput = fopen( "C:/_www/test.zip", 'a' );
		fwrite( $fileOutput, $content );
		fclose( $fileOutput );
*/
		ftp_close( $conn_id );

		return( $bEnd );
	} // process

} // progressbar_sub
?>