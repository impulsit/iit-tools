<?php
require_once( "../../../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );
require_once( CLASS_DIR.'PHPExcel/PHPExcel.php' );

class progressbar {
	static private $instance = null;
	public $sub = null;
	public $progress_action = "";
	public $current_loop = 0;
	public $max = 0;
	public $start_time = 0;
	public $param = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function convert_ajax_remove( $return ) {
		foreach( $return as $k => $v ) {
			$return[$k] = str_replace( "\n", "", $return[$k] );
			$return[$k] = str_replace( "\t", "", $return[$k] );
			$return[$k] = str_replace( "\r", "", $return[$k] );
		} // foreach
		return( $return );
	} // convert_ajax_remove

	function load() {
		require_once( "progressbar_sub_".$this->progress_action.".php" );

		$this->sub = progressbar_sub::getInstance();
	} // set_action

	function get_text() {
		$this->sub->param = $this->param;
		$this->sub->current_loop = $this->current_loop;

		return( $this->sub->get_text() );
	} // get_text

	function get_percent() {
		return( round( $this->current_loop / $this->get_max() * 100 ) );
	} // get_percent

	function get_max() {
		return( $this->sub->get_max() );
	} // get_max

	function get_start_time() {
		if( $this->start_time == 0 )
			$this->start_time = time();

		return( $this->start_time );
	} // get_max

	function get_remaining() {
		$decPercentFinished = $this->current_loop / $this->get_max() * 100;
		$iGoneSeconds = time() - $this->start_time;
		$iRemainSeconds = round( $iGoneSeconds * ( 100 / $decPercentFinished ) - $iGoneSeconds );

		return( gmdate( "H:i:s", $iRemainSeconds) );
	} // get_remaining

	function process() {
		$this->sub->param = $this->param;
		$this->sub->current_loop = $this->current_loop;
		$this->sub->max = $this->max;
		
		return( $this->sub->process() );
	} // process
} // progressbar
?>