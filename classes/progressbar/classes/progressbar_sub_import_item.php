<?php
class progressbar_sub {
	static private $instance = null;
	public $current_loop = 0;
	public $max = 0;
	public $param = null;
	
	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function get_max() {
		if( $this->max <= 0 ) {
			$objPHPExcel = PHPExcel_IOFactory::load( BASE_DIR.'upload/'.$this->param );
			
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();

			$this->max = $highestRow;
		} // if
		

		return( $this->max );
	} // init_max

	function get_text() {
		// 3 Textzeilen für Statusinfo

		return( array(
			"Durchlauf: ".$this->current_loop." / ".$this->get_max(),
			"",
			""
		) );
	} // get_text

	function process() {
		$bEnd = false;

		if( $this->current_loop >= $this->get_max() )
			$bEnd = true;

		// Processing
		sleep( 1 );

		return( $bEnd );
	} // process

} // progressbar_sub
?>