<?php
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'translate.php' );
require_once( CLASS_DIR.'theme_functions.php' );

class list_vmware_home {
	static private $instance = null;
	private $db = null;
	private $f = null;
	private $t = null;
	private $theme = null;
	
	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance
	
	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
		$this->t = translate::getInstance();
		$this->theme = theme_functions::getInstance();
	} // __construct
	
	function print_menu_info( $list_id ) {
		switch( $list_id ) {
		} // switch
	} // print_menu_info
	
	function get_spezial_filter( $list_id ) {
		switch( $list_id ) {
		} // switch
	
		return( null );
	} // get_spezial_filter
	
	function insert_special_list_line( $list_id, $values, $bLastLine = false ) {
		switch( $list_id ) {
		} // switch
	} // insert_special_list_line
	
	function special_save( $list_id ) {
		switch( $list_id ) {
		} // switch
	} // special_save
	
	function add_special_line_buttons( $list_id, $values ) {
		switch( $list_id ) {
		} // switch
	} // add_special_line_buttons
	
	function add_special_action_buttons_main( $list_id ) {
		$strOut = null;
		 
		switch( $list_id ) {
		} // switch
		 
		return( $strOut );
	} // add_special_action_buttons_main
	
	function parse_special_buttons( $list_id, $param) {
		$infotext = null;
	
		switch( $list_id ) {
		} // switch
		 
		return( $infotext );
	} // parse_special_line_buttons
	
	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$output = null;
		$c = "left";
		
		switch( $list_id ) {
		} // switch
		 
		return( $output );
	} // get_special_functions
} // list_vmware_home
?>