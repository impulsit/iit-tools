<?php
require_once( 'config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'mail.php' );
require_once( CLASS_DIR.'PasswordHash.php' );
require_once( CLASS_DIR.'translate.php' );
require_once( CLASS_DIR.'theme_functions.php' );
require_once( CLASS_DIR."../themes/_default/classes/default_functions.php" );
if( file_exists( CLASS_DIR."../themes/basilica_at/classes/basilica_functions.php" ) )
	require_once( CLASS_DIR."../themes/basilica_at/classes/basilica_functions.php" );
if( file_exists( CLASS_DIR."../themes/tecdocbase_atit_at/classes/tecdoc_functions.php" ) )
	require_once( CLASS_DIR."../themes/tecdocbase_atit_at/classes/tecdoc_functions.php" );
if( file_exists( CLASS_DIR."../themes/impulsit_at/classes/impulsit_functions.php" ) )
	require_once( CLASS_DIR."../themes/impulsit_at/classes/impulsit_functions.php" );

class functions {
	static private $instance = null;
	private $db = null;
	private $mes = null;
	private $mail = null;
	private $t = null;
	private $theme = null;
	public $pw = null;
	public $set_back_up_file = "";

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->mes = messages::getInstance();
		$this->mail = fb_mail::getInstance();
		$this->pw = new PasswordHash( 8, false );
		$this->t = translate::getInstance();
		$this->theme = theme_functions::getInstance();

		date_default_timezone_set( "Europe/Vienna" );
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function convert_ajax_remove( $return ) {
		foreach( $return as $k => $v ) {
			$return[$k] = str_replace( "\n", "", $return[$k] );
			$return[$k] = str_replace( "\t", "", $return[$k] );
			$return[$k] = str_replace( "\r", "", $return[$k] );
		} // foreach
		return( $return );
	} // convert_ajax_remove

	function generatePassword( $length = 8 ) {
		$password = "";
		$possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
		$i = 0;
		while ( $i < $length ) {
			$char = substr( $possible, mt_rand( 0, strlen( $possible ) - 1 ), 1 );
			if ( !strstr( $password, $char ) ) {
				$password .= $char;
				$i++;
			} // if
		} // while
		return $password;
	} // generatePassword

	function load_user( $user_id, $str ) {
		$this->db->query( "SELECT ".$str." FROM CORE_USER_INFO WHERE user_id='".$user_id."'", "load_user" );
		$r = $this->db->getNext( "load_user" );

		return( $r );
	} // load_user

	function project_allowed( $project_id ) {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$b = false;

		switch( $location ) {
			case 'qnap_home':
			case 'impulsit_at':  				if( in_array( $project_id, array( 2, 3, 4, 5 ) ) ) $b = true;
				break;
			case 'freeto_biz':	 				if( in_array( $project_id, array( 3 ) ) ) 			$b = true;
				break;
			case 'basilica_at':  				if( in_array( $project_id, array( 2, 3 ) ) ) 		$b = true;
				break;
			default:
				if( in_array( $project_id, array( PROJECT_ALLOWED ) ) ) $b = true;
		} // switch

		// Listen immer
		if( in_array( $project_id, array( 1 ) ) ) $b = true;

		return( $b );
	} // project_allowed

	function list_allowed( $list_id ) {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		$b = false;

		$why = array( 14, 16, 17 );
		$basilica_at = array( 4, 5, 6, 24, 27, 28, 41, 7, 8, 9, 11, 45 );
		$tecdocbase_atit_at = array( 28, 41, 37, 38, 43, 44 );
		$dsgvo = array( 46, 47, 48, 49 );

		switch( $location ) {
			case 'qnap_home':
			case 'impulsit_at': if( in_array( $list_id, array_merge( $why, $basilica_at, $tecdocbase_atit_at, $dsgvo ) ) ) $b = true; break;
			case 'freeto_biz': if( in_array( $list_id, $why ) ) $b = true; break;
			case 'basilica_at': if( in_array( $list_id, $basilica_at ) ) $b = true; break;
			default: if( in_array( $list_id, $tecdocbase_atit_at ) ) $b = true; break;
		} // switch

		return( $b );
	} // function

	function get_menu( $project_id ) {
		global $c_user_id;

		$arr = array();

		if( $project_id == 0 ) {
			$arr[] = array( "title" => str_replace( "VERSION", "", VERSION ), "url" => "core_update.php", "min_user_level" => 990, "fa_class" => 'fa fa-info-circle text-primary' );

			$this->db->query( "
				SELECT project_id, title, picture, fa_class FROM CORE_PROJECTS
				WHERE project_id!='1'
				UNION
				SELECT project_id, title, picture, fa_class FROM CORE_PROJECTS WHERE project_id='1'" );
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();

				if( $this->project_allowed( $r['project_id'] ) ) {
					if( $r['project_id'] == 1 ) // Globale Listen
						$min_user_level = 100; else $min_user_level = 0;

					switch( $r['project_id'] ) {
						case 4: $arr[] = array( "title" => $r['title'], "url" => "/".SUBDIR.'admin/tec_search.php?project_id='.$r['project_id'], "pic" => $r['picture'], "min_user_level" => $min_user_level, "fa_class" => $r['fa_class'] ); break;
						default: $arr[] = array( "title" => $r['title'], "url" => "/".SUBDIR.'admin/core_entry.php?project_id='.$r['project_id'], "pic" => $r['picture'], "min_user_level" => $min_user_level, "fa_class" => $r['fa_class'] );
					} // switch
				} // if
			} // while
			$arr[] = array( "title" => "Projekt Verwaltung", "url" => "/".SUBDIR."admin/core_verwaltung.php", "pic" => "pics/settings.png", "min_user_level" => 990, "fa_class" => "fas fa-cog text-primary" );
		} else {
			$userinfo = $this->load_user( $c_user_id, "start_menu, user_level" );
			if( $userinfo['start_menu'] == 0 )
				$arr[] = array( "title" => "Hauptmenü", "url" => "/".SUBDIR."admin/core_backend.php", "pic" => "pics/hauptmenu.png", "min_user_level" => 0, "fa_class" => "fas fa-home text-success" );

			$order = 'pos';
			if( $project_id == 1 ) $order = 'title';
			$this->db->query( "SELECT menu_id, title, picture, file, list_id, min_user_level, fa_class FROM CORE_MAINMENU WHERE project_id='".$project_id."' AND min_user_level<='".$userinfo['user_level']."' ORDER BY ".$order );
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();

				$bShow = true;
				if( $project_id == 1 )
					$bShow = $this->list_allowed( $r['list_id'] );

				if( $project_id == 4 ) { // TecDoc
					if( ($r['list_id'] == 31) || ($r['list_id'] == 33) || ($r['title'] == "Suche") )
						$bShow = false;
				} // if

				if( $bShow )
					$arr[] = array( "title" => $r['title'], "url" => "/".SUBDIR.$r['file'].(($r['list_id']>0)?'?indiv_list_id='.$r['list_id']:''), "pic" => $r['picture'], "min_user_level" => $r['min_user_level'], "list_id" => $r['list_id'], "fa_class" => $r['fa_class'] );
			} // while
		} // else

		$arr[] = array( "title" => "ausloggen", "url" => "/".SUBDIR."admin/login_logout.php", "pic" => "pics/logout.png", "min_user_level" => 0, "fa_class" => "fas fa-sign-out-alt text-danger" );

		return( $arr );
	} // get_menu

	function insertArrayAtPosition( $array, $insert, $position ) {
		/*
		 $array : The initial array i want to modify
		 $insert : the new array i want to add, eg array('key' => 'value') or array('value')
		 $position : the position where the new array will be inserted into. Please mind that arrays start at 0
		 */
		return array_slice($array, 0, $position, TRUE) + array( $insert ) + array_slice($array, $position, NULL, TRUE);
	}

	function get_left_menu() {
		global
			$_MENU_ADMIN_LOGIN, $_MENU_MODUL_VERWALTUNG,
			$c_user_id, $lang, $_location;

		$strfile = $_SERVER['REQUEST_URI'];
		$strfile = explode( "?", $strfile );
		$file = $strfile[0];
		$param = "";
		if( isset( $strfile[1] ) )
			$param = $strfile[1];

		$m = array();
		$m1 = array();

		// Ebene hoch
		if( isset( $this->set_back_up_file ) && ($this->set_back_up_file != "") ) {
			$m1[] = array( "title" => "hoch", "url" => basename( $this->set_back_up_file ), "pic" => "pics/folder_up.png", "min_user_level" => 0, "fa_class" => "fas fa-level-up-alt" );
		} // if

		// Login
		if( (basename( $file ) == "login_login.php") || (basename( $file ) == "login_pw_lost.php") || (basename( $file ) == "login_register.php") ||
				(strpos( basename( $file ), "imprint_" ) !== false) )
			$m = $_MENU_ADMIN_LOGIN;

		// Verwaltung
		if( ((substr( basename( $file ), 0, 5 ) == "core_") && (basename( $file ) != "core_entry.php")) || ($param == "indiv_list_id=39") || ($param == "indiv_list_id=40") ||
				(isset( $_SESSION['list_id'] ) AND (($_SESSION['list_id'] == 39) || ($_SESSION['list_id'] == 40)) )
			) {
			$m = array_merge( $m1, $_MENU_MODUL_VERWALTUNG );

			if( $this->project_allowed( 2 ) )
				array_splice( $m, 3, 0, array( array( "title" => "Basilica - Benutzer", "url" => "/".SUBDIR."admin/list_redirect.php?indiv_list_id=39", "pic" => "pics/user.png", "min_user_level" => 900, "list_id" => 39, "fa_class" => "fas fa-users text-primary" ) ) );
			if( $this->project_allowed( 4 ) )
				array_splice( $m, 3, 0, array( array( "title" => "TecDoc - Benutzer", "url" => "/".SUBDIR."admin/list_redirect.php?indiv_list_id=40", "pic" => "pics/user.png", "min_user_level" => 900, "list_id" => 40, "fa_class" => "fas fa-users text-primary" ) ) );

			if( $c_user_id != 1 ) // Wenn nicht SuperAdmin, dann lösche Core Benutzer
				unset( $m['2'] );

			if( $c_user_id == 1 ) {
				$iPos = -1;
				for( $i=0; $i<sizeof( $m ); $i++ ) {
					if( $m[$i]['title'] == "Update" ) {
						if( file_exists( BASE_DIR."admin/core_update_".$_location.".php" ) ) {
							$m_save = $m[$i];
							$m_save['url'] = "/".SUBDIR."admin/core_update_".$_location.".php";
							$m_save['title'] = "Update erstellen";
							$iPos = $i;
						} // if
					} // if
				} // for
				if( $iPos > -1 )
					array_splice( $m, $iPos + 1, 0, array( $m_save ) );
			} // if

			$m[] = array( "title" => "ausloggen", "url" => "/".SUBDIR."admin/login_logout.php", "pic" => "pics/logout.png", "min_user_level" => 0, "fa_class" => "fas fa-sign-out-alt text-danger" );
		} // if

		$userinfo = $this->load_user( $c_user_id, "admin, name, start_menu, user_level" );

		// Backend Root
		if( basename( $file ) == "core_backend.php" ) {
			if( $userinfo['start_menu'] == 0 ) {
				$m = $this->get_menu( 0 );
			} else {
				$m = $this->get_menu( $userinfo['start_menu'] );
				$_SESSION['project_id'] = $userinfo['start_menu'];
			} // else
		} // if

		// TecDoc
		if( !isset( $_SESSION['project_id'] ) ) $_SESSION['project_id'] = 0;

		// Projekte
		if( $m == array() )
			$m = array_merge( $m1, $this->get_menu( $_SESSION['project_id'] ) );

		$bFirst = true;

		$str = 			'<div class="sidebar1 menu_standard" class="shadow"><nav role="navigation">';
//		$str_small = 	'<div class="sidebar1 menu_small" class="shadow" style="display: none;">';

		if( $userinfo['name'] != "" )
			$str .= '<h3>'.$userinfo['name'].'</h3>';
		else
			$str .= '<h3>'.TITEL.'</h3>';
//		$str_small .= '<h3>&nbsp;</h3>';

		$str .= 		'<ul class="linkedList">';
//		$str_small .=	'<ul class="linkedList">';

		// Language Switch
//		$str .= 		'<div class="language_switch"><a onClick="switch_menu();"><i class="fas fa-bars fa-2x"></i></a>';
//		$str_small .= 	'<div class="language_switch"><a onClick="switch_menu();"><i class="fas fa-bars fa-2x"></i></a>';

		/*
		$bShowFlags = true;
		$setup = $this->load_setup( "CORE_SETUP" );
		if( isset( $setup['hide_flags'] ) && ($setup['hide_flags'] == 1) && ($userinfo['user_level'] < 100) ) $bShowFlags = false;
		if( $bShowFlags ) {
			if( isset( $lang ) ) {
				$iCountFlags = sizeof( $lang );
				$width = 34;
				if( $iCountFlags >= 7 ) $width = 30 - ($iCountFlags - 7) * 3;
				if( $iCountFlags == 11 ) $width = 19;
				if( $iCountFlags == 12 ) $width = 17;

				foreach( $lang as $k => $v ) {
					if( isset( $_SESSION['language'] ) && ($_SESSION['language'] == $k) ) $c = 'active'; else $c = '';
					$str .= '<a href="?language='.$k.'"><img class="'.$c.' flag" style="height: 34px; width: '.$width.'px;" src="/'.SUBDIR.'themes/_default/icons/flags/'.$v['flag'].'" title="'.$v['name'].'"></a>';
				} // foreach
			} // if
		} // if
		*/
//		$str .= 		'</div>';
//		$str_small .=	'</div>';

		// Sprache Submenü
		if( isset( $_SESSION['project_id'] ) && ($_SESSION['project_id'] == 4) ) {
//			$pic = "background-image: url( '".$this->theme->get_icon_path( "pics/sprache.png" )."' ); background-position: left center;";

			$str .= '
				<li class="dropdown-submenu"><a class="link_menu dropdown-item dropdown-toggle" href="#"><i class="fas fa-globe-americas text-primary fa-fw"></i> '.$this->t->t( "Sprache" ).'</a>
					<ul class="dropdown-menu">';

			$this->db->query( "SELECT layout_code, title, flag FROM CORE_LANGUAGE_SETUP WHERE active='1'" );
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();

				$pic = "background-image: url( '".$this->theme->get_icon_path( $r['flag'] )."' ); background-position: left center;";
				$pic = ""; // keine Sprachen

				if( isset( $_SESSION['language'] ) && ($_SESSION['language'] == $r['layout_code']) ) {
					$r['title'] = '<strong>'.$r['title'].'</strong>';
					$c = 'active';
				} else
					$c = '';
				$str .= '<li class="'.$c.'"><a style="'.$pic.'" class="link_menu dropdown-item" href="?language='.$r['layout_code'].'">'.$r['title'].'</a></li>';
			} // while

			$str .= '
					</ul>
				</li>';
		} // if

		$bSubOpen = false;
		$lastMenu = '';
		foreach( $m as $k => $v ) {
			$c = '';
			if( $k == (sizeof( $m )-1) ) $c .= 'last ';

			$bShow = false;
			if( $userinfo['user_level'] >= $v['min_user_level'] ) $bShow = true;

			if( $bShow ) {
				if( $bFirst ) {
					$bFirst = false;
					$c .= 'first ';
				} // if

				$pic = '';
				$fa_icon = '';
				if( isset( $v['pic'] ) ) {
					//$pic = "background-image: url( '/".SUBDIR.$v['pic']."' );";
					$pic = "background-image: url( '".$this->theme->get_icon_path( $v['pic'] )."' ); background-position: left center;";
				} // if
				if( isset( $v['fa_class'] ) ) {
					$pic = '';
					$fa_icon = '<i class="'.$v['fa_class'].' fa-fw"></i> ';
				} // if

				$bActive = false;
				if( basename( $v['url'] ) == basename( $file ) )
					$bActive = true;
				if( basename( $v['url'] ) == basename( $_SERVER['REQUEST_URI'] ) )
					$bActive = true;
   			if( isset( $v['list_id'] ) && isset( $_SESSION['list_id'] ) && ($_SESSION['list_id'] == $v['list_id']) )
     			$bActive = true;

				// Translate Menu Items
				$v['title'] = $this->t->t( $v['title'] );

				$strCount = '';
				if( isset( $v['list_id'] ) ) {
					if( $v['list_id'] == 31 ) // TecDoc - Warenkorb
						$strCount = '<span class="shopping_count count_info"></span>';
					if( $v['list_id'] == 32 ) // TecDoc - Bestellungen (Admin)
						$strCount = '<span class="order_count count_info_menu"></span>';
				} // if

				$title = explode( "_", $v['title'] );
				if( sizeof( $title ) == 1 ) $title = $v['title'];

				// Gruppiertes SubMenü
				if( !$bSubOpen && (strpos( $v['title'], "_" ) !== false) ) {
					$last_menu = $title[0];
					$bSubOpen = true;
					$bSubOpenOnLoad = false;
					$strSub = '
						<li class="nav-item dropdown _show_">
							<a class="nav-link dropdown-toggle" href="#pageSubmenu_'.$title[0].'" id="navbarDropdown_'.$title[0].'" role="button" data-toggle="collapse" aria-expanded="true">'.$title[0].'</a>
						<ul class="collapse _show_" id="pageSubmenu_'.$title[0].'" aria-labelledby="navbarDropdown_'.$title[0].'">'; // dropdown-menu
				} // if
				if( $bSubOpen && ($last_menu != $title[0]) ) {
					$bSubOpen = false;
					$strSub .= '</ul></li>';

					$bSubOpenOnLoad = true;
					if( $bSubOpenOnLoad ) $strSub = str_replace( "_show_", "show", $strSub ); else $strSub = str_replace( "_show_", "", $strSub );
					$str .= $strSub;
				} // if

				// Subliste -> Menü aktivieren
				if( isset( $_SESSION['list_id'] ) && ($_SESSION['list_id'] == 51) && isset( $v['list_id'] ) && ($v['list_id'] == 50) ) $bActive = true;

				if( $bActive ) {
					$v['title'] = '<strong>'.$v['title'].'</strong>';
					$title[1] = '<strong>'.$title[1].'</strong>';
					$c .= 'active ';
				} // if

				if( $bSubOpen ) {
					$strSub .= '<a style="'.$pic.'" class="'.$c.' link_menu dropdown-item _show_" href="'.$v['url'].'"'.(isset($v['new_window'])?' target="_blank"':'').'>'.$fa_icon.' '.$strCount.$title[1].'</a>';
					$bSubOpenOnLoad = $bSubOpenOnLoad | $bActive;
				} else
					$str .= '<li class="'.$c.'"><a style="'.$pic.'" class="link_menu dropdown-item" href="'.$v['url'].'"'.(isset($v['new_window'])?' target="_blank"':'').'>'.$fa_icon.' '.$strCount.$v['title'].'</a></li>';


//				$str_small .= '<li class="'.$c.'"><a class="link_menu" style="'.$pic.'" href="'.$v['url'].'" '.(isset($v['new_window'])?'target="_blank"':'').' title="'.$v['title'].'">'.$fa_icon.$strCount.'</a></li>';
			} // if
		} // foreach

		// Left Logo
		if( ($this->theme->get_logo_left() != "") && ($userinfo['user_level'] < 100) )
			$str .= '</ul><ul class="linkedList"><li class="logo_left"><a href="http://www.atit.at/" target="_blank"><img src="'.$this->theme->get_logo_left().'"></a></li>';

		$str .= 			'</ul></nav></div>';
//		$str_small .=	'</ul></div>';

		$str = $str; //.$str_small;

		if( (basename( $file ) == "core_backend.php") && ($userinfo['user_level'] < 990) ) $str = '';

		return( $str );
	} // get_left_menu

	function valid_email( $email ) {
		$isValid = true;
		$atIndex = strrpos($email, "@");
		if (is_bool($atIndex) && !$atIndex)
		{
			$isValid = false;
		}
		else
		{
			$domain = substr($email, $atIndex+1);
			$local = substr($email, 0, $atIndex);
			$localLen = strlen($local);
			$domainLen = strlen($domain);
			if ($localLen < 1 || $localLen > 64)
			{
				// local part length exceeded
				$isValid = false;
			}
			else if ($domainLen < 1 || $domainLen > 255)
			{
				// domain part length exceeded
				$isValid = false;
			}
			else if ($local[0] == '.' || $local[$localLen-1] == '.')
			{
				// local part starts or ends with '.'
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $local))
			{
				// local part has two consecutive dots
				$isValid = false;
			}
			else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
			{
				// character not valid in domain part
				$isValid = false;
			}
			else if (preg_match('/\\.\\./', $domain))
			{
				// domain part has two consecutive dots
				$isValid = false;
			}
			else if
				(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
				str_replace("\\\\","",$local)))
			{
				// character not valid in local part unless
				// local part is quoted
				if (!preg_match('/^"(\\\\"|[^"])+"$/',
					str_replace("\\\\","",$local)))
				{
					$isValid = false;
				}
			}
/*
			if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
			{
				// domain not found in DNS
				$isValid = false;
			}
*/
		}

		return $isValid;
	} // valid_email

	function password_lost( $email ) {
		if( !$this->valid_email( $email ) ) {
			$this->mes->addError( "Diese E-Mail ist nicht gültig." );
			return;
		} // if
		if( !$this->db->query( "SELECT email FROM CORE_USER_INFO WHERE email='".$email."'" ) ) {
			$this->mes->addError( "Diese E-Mail existiert nicht." );
			return;
		} // if

		if( $this->mes->noError() ) {
			$this->mail->mail_password_lost( $email );

			$this->mes->addInfo( "Passwort-Link wurde erfolgreich versendet. Sie erhalten in Kürze eine E-Mail mit einem Link um ein neues Passwort zu vergeben." );
		} // if
	} // password_lost

	function password_lost_finished( $email ) {
		$this->mail->mail_password_lost_finished( $email );
	} // password_lost_finished

	function send_password( $email, $type ) {
		$password = $this->generatePassword();

		$this->db->update( "CORE_USER_INFO", array( "password" => $this->pw->HashPassword( $password ) ), "email='".$email."'" );
		$this->db->commit();

		// Mail Password
		switch( $type ) {
			case PW_LOST: $this->mail->mail_password( $email, $password ); break;
			case REGISTER: $this->mail->mail_register( $email, $password ); break;
		} // switch

		$this->mes->addInfo( "Sie erhalten in Kürze eine E-Mail an ".$email." mit Ihrem Passwort." );
	} // password_lost_finished

	function mail_register( $email ) {
		if( !$this->valid_email( $email ) )
			$this->mes->addError( "Diese E-Mail ist nicht gültig: ".$email );
		if( !$this->db->query( "SELECT email FROM CORE_USER_INFO WHERE email='".$email."'" ) )
			$this->mes->addError( "Diese E-Mail existiert nicht: ".$email );

		if( $this->mes->noError() ) {
			$this->mail->mail_register( $email );

			$this->mes->addInfo( "Registrier E-Mail wurde erfolgreich versendet: ".$email );
		} // if
	} // mail_register

	function login_user() {
		$this->db->query( "SELECT user_id, password, anzahl_logins, valid_to FROM CORE_USER_INFO WHERE email='".$_POST['email']."'" );
		$userinfo = $this->db->getNext();

		if( !$this->pw->CheckPassword( $_POST['password'], $userinfo['password'] ) )
			$this->mes->addError( "Die Zugangsdaten stimmen nicht." );

		if( ($userinfo['valid_to'] != '0000-00-00') && ($userinfo['valid_to'] != '1970-01-01') && (strtotime( $userinfo['valid_to'].' 23:59:59' ) < time()) ) {
			$strOut = 'Der Benutzer ist abgelaufen.';
			$setup = $this->load_setup( "CORE_SETUP" );
			if( $setup['contact_email'] != '' )
				$strOut .= ' Bitte setzen Sie sich mit uns in Verbindung. E-Mail: '.$setup['contact_email'].' (Tel: '.$setup['contact_phone'].')';
			$this->mes->addError( $strOut );
		} // if

		if( $this->mes->noError() ) {
			$_SESSION['c_id'] = $this->pw->HashPassword( microtime().$userinfo['user_id'] );

			$this->db->update( "CORE_USER_INFO", array(
				"show_iframe" => 1,
				"session_id" => $_SESSION['c_id'],
				"ip" => $_SERVER['REMOTE_ADDR'],
				"anzahl_logins" => $userinfo['anzahl_logins'] + 1,
				"last_login" => $this->d( time() ) ),
				"user_id='".$userinfo['user_id']."'" );
			$this->db->commit();

			return( true );
		} // if

		return( false );
	} // login_user

	function register_user() {
		if( !$this->valid_email( $_POST['email'] ) )
			$this->mes->addError( "Diese E-Mail ist nicht gültig." );
		if( $this->db->query( "SELECT email FROM CORE_USER_INFO WHERE email='".$_POST['email']."'" ) )
			$this->mes->addError( "Diese E-Mail existiert breits." );

		if( $this->mes->noError() ) {
			$this->db->insert( "CORE_USER_INFO", array( "email" => $_POST['email'], "name" => $_POST['name'] ) );
			$this->db->commit();

			$this->send_password( $_POST['email'], REGISTER );

			return( true );
		} // if

		return( false );
	} // register_user

	function load_answer( $id, $fragebogen_id, $frage_id, $antwort_id, $typ ) {
		if( $this->db->query( "
			SELECT antwort_value, antwort_text
			FROM WHY_USER_ANTWORTEN
			WHERE id='".$id."' AND frage_id='".$frage_id."' AND antwort_id='".$antwort_id."'", "load_answer" ) ) {
			$r = $this->db->getNext( "load_answer" );

			if( $typ == 4 ) $value = $r['antwort_text']; else $value = $r['antwort_value'];

			return( $value );
		} // if

		return( false );
	} // load_answer

	function save_answer( $id, $fragebogen_id, $frage_id, $antwort, $typ ) {
		if( is_array( $antwort ) ) {
			// vorige Antworten löschen
			$this->db->delete( "WHY_USER_ANTWORTEN", "id='".$id."' AND frage_id='".$frage_id."'" );

			// Antworten speichern
			if( $typ == 4 ) $store_field = "antwort_text"; else $store_field = "antwort_value";

			foreach( $antwort as $k => $v ) {
				$this->db->insert( "WHY_USER_ANTWORTEN",
					array(
						"id" => $id,
						"frage_id" => $frage_id,
						"antwort_id" => $k,
						$store_field => $v,
					) );
				} // foreach
			$this->db->commit();
		} // if
	} // save_answer

	function t( $zeit ) {
		if( $zeit == "0000-00-00 00:00:00" )
			return( "---" );

		if( !is_numeric( $zeit ) )
			$zeit = strtotime( $zeit );

		return( date( "d.m.Y, H:i", $zeit ) );
	} // t

	function d( $iZeit ) {
		$iZeit = intval( $iZeit );
		if( $iZeit < 0 )
			$iZeit = 0;

		if( $iZeit == 0 )
			return( '0000-00-00 00:00:00' );

		return( date( "Y-m-d H:i:s", $iZeit ) );
	} // d

	function SQLDate( $iZeit ) {
		$iZeit = strtotime( $iZeit );

		$iZeit = intval( $iZeit );
		if( $iZeit < 0 )
			$iZeit = 0;

		if( $iZeit == 0 )
			return( '0000-00-00' );

		return( date( "Y-m-d", $iZeit ) );
	} // d

	function now() {
		return( $this->d( time() ) );
	} // now

	function ansicht( $id ) {
		$this->db->query( "
			SELECT uf.email, uf.zeit, uf.fragebogen_id, f.titel
			FROM WHY_USER_FRAGEBOGEN AS uf
			LEFT JOIN WHY_FRAGEBOGEN AS f ON (f.fragebogen_id=uf.fragebogen_id)
			WHERE id='".$id."'" );
				$r = $this->db->getNext();

		$strOut = '
					<table class="list">
						<tr><th>Fragebogen</th><td>'.$id.' - '.$r['titel'].'</td></tr>
						<tr><th>E-Mail</th><td>'.$r['email'].'</td></tr>
						<tr><th>Zeit</th><td>'.$this->t( $r['zeit'] ).'</td></tr>
					</table><hr>';

		$this->db->query( "
			SELECT f.frage_id, f.kurz, f.frage, f.antworttyp_id
			FROM WHY_FRAGEN AS f
			WHERE f.fragebogen_id='".$r['fragebogen_id']."'", 1 );
		while( $this->db->isNext( 1 ) ) {
			$frage = $this->db->getNext( 1 );

			$strOut .= '<strong>'.$frage['kurz'].'</strong> - '.$frage['frage'].'<br />';

			$this->db->query( "SELECT antwort_id, wert, antwort_titel FROM WHY_ANTWORTEN WHERE frage_id='".$frage['frage_id']."' ORDER BY position" );
			while( $this->db->isNext() ) {
				$antwort = $this->db->getNext();

				$value = $this->load_answer( $id, $r['fragebogen_id'], $frage['frage_id'], $antwort['antwort_id'], $frage['antworttyp_id'] );

				$a = '';
				switch( $frage['antworttyp_id'] ) {
					case 1:
						// Texteingabe
						$a = '<label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label> <input readonly type="text" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id['.$antwort['antwort_id'].']" value="'.$value.'">';
						break;
					case 2:
						// Radio
						if( $value == 1 ) $c = ' checked'; else $c = '';
						$a = '<input disabled="disabled" type="radio" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id['.$frage['frage_id'].']" value="'.$antwort['antwort_id'].'"'.$c.'> <label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label>';
						break;
					case 3:
						// Checkboxen
						if( $value == 1 ) $c = ' checked'; else $c = '';
						$a = '<input disabled type="checkbox" id="antwort_'.$antwort['antwort_id'].'" name="antwort_id['.$antwort['antwort_id'].']" value="1"'.$c.'> <label for="antwort_'.$antwort['antwort_id'].'">'.$antwort['antwort_titel'].'</label>';
						break;
				} // switch

				$strOut .= '<div class="antwort'.($value == 1?' active':'').'">'.$a.'</div>';
			} // while

			$strOut .= '<hr>';
		} // while

		return( $strOut );
	} // ansicht

	function send_fb_email( $id ) {
		$this->db->query( "
			SELECT u.user_id, uf.versendet, uf.fragebogen_id
			FROM USER_FRAGEBOGEN AS uf
			LEFT JOIN DOMAINS AS d ON (d.fragebogen_id=uf.fragebogen_id)
			LEFT JOIN CORE_USER_INFO AS u ON (u.user_id=d.user_id)
			WHERE uf.id='".$id."'" ); //  AND uf.versendet='0'
		$u = $this->db->getNext();

		$this->db->query( "SELECT u.user_id, u.email_fragebogen, u.rest_anzahl, u.name FROM CORE_USER_INFO AS u WHERE u.user_id='".$u['user_id']."' FOR UPDATE" );
		$userinfo = $this->db->getNext();

		if( $userinfo['rest_anzahl'] > 0 ) {
			if( $u['versendet'] == 0 ) {
				// Nur verrechnen, wenn noch nicht versendet war
				$this->db->query( "UPDATE CORE_USER_INFO SET rest_anzahl=rest_anzahl-1 WHERE user_id='".$userinfo['user_id']."'" );
				$this->db->insert( "LOG_PAYMENT_DETAIL", array(
					"user_id" => $userinfo['user_id'],
					"anzahl" => -1,
					"zeit" => $this->d( time() ),
					"send_id" => $id,
					"typ" => AKTION_SEND )
				);
			} // if
			$this->db->update( "USER_FRAGEBOGEN", array(
				"versendet" => 1,
				"versendet_am" => $this->d( time() ),
				"versendet_an_user_id" => $userinfo['user_id'],
				"versendet_an_email" => $userinfo['email_fragebogen'] ),
				"id='".$id."'"
			);
			$this->db->commit();

			$text = $this->ansicht( $id );

			// Mail senden
			$this->mail->mail_fragen( $userinfo['email_fragebogen'], $userinfo['name'], $text, $u['fragebogen_id'] );
		} // if
	} // send_fb_email

	function get_kurs_titel( $titel, $start, $end ) {
		$str = $titel.' ('.date( "d.m.Y", strtotime( $start ) ).' bis '.date( "d.m.Y", strtotime( $end ) ).')';

		return( $str );
	} // get_kurs_titel

	function get_teilnehmer_name( $teilnehmer_id ) {
		$str = '';

		if( $this->db->query( "SELECT nachname, vorname FROM BAS_TEILNEHMER WHERE teilnehmer_id='".$teilnehmer_id."'", "get_teilnehmer_name" ) ) {
			$r = $this->db->getNext( "get_teilnehmer_name" );

			$str = $r['nachname'].' '.$r['vorname'];
		} // if

		return( $str );
	} // get_teilnehmer_name

	function get_trainer_name( $trainer_id ) {
		$str = '';

		if( $this->db->query( "SELECT vorname, nachname FROM BAS_TRAINER WHERE trainer_id='".$trainer_id."'", "get_trainer_name" ) ) {
			$r = $this->db->getNext( "get_trainer_name" );

			$str = $r['nachname'].' '.$r['vorname'];
		} // if

		return( $str );
	} // get_trainer_name

	function get_fragebogen_name( $fragebogen_id ) {
		$str = '';

		if( $this->db->query( "SELECT titel FROM WHY_FRAGEBOGEN WHERE fragebogen_id='".$fragebogen_id."'", "get_fragebogen_name" ) ) {
			$r = $this->db->getNext( "get_fragebogen_name" );

			$str = $r['titel'];
		} // if

		return( $str );
	} // get_fragebogen_name

	function get_type_name( $type ) {
		$str = '';

		switch( $type ) {
			case 'teilnehmer':				$str = 'Teilnehmer'; break;
			case 'kurs':							$str = 'Kurs'; break;
			case 'teilnehmer_kurs':		$str = 'Teilnehmer + Kurs'; break;
			case 'trainer':						$str = 'Trainer'; break;
			case 'trainer_kurs':			$str = 'Trainer + Kurs'; break;
			case 'fragebogen_kurs':		$str = 'Fragebogen + Kurs'; break;
		} // switch

		return( $str );
	} // get_type_name

	function load_setup( $table = "SETUP" ) {
		if( !$this->db->query( "SELECT * FROM ".$table." WHERE id='1'", "load_setup" ) ) {
			$this->db->insert( $table, array( "id" => 1 ) );
			$this->db->commit();

			$this->db->query( "SELECT * FROM ".$table." WHERE id='1'", "load_setup" );
		} // if

		$r = $this->db->getNext( "load_setup" );

		return( $r );
	} // load_setup

	function save_setup( $table = "SETUP" ) {
		if( isset( $_GET['table'] ) && ($table != $_GET['table']) )
			return;

		if( isset( $_GET['save_setup'] ) ) {
			foreach( $_POST as $k => $v ) {
				$this->db->update( $table , array( $k => $v ), "id='1'" );
			} // foreach
			$this->db->commit();

			$this->mes->addInfo( "Einstellungen gespeichert." );
		} // if
	} // function

	function dir_rekursiv( $verzeichnis, $ab_datum, $skip, $skip_file_pattern, $include_class, $initial_update ) {
		global $files, $skip_dirs;

		$skip_dirs = array( "updates/", "menumaker/", "_db/", "_docs/", "_software/", "tcpdf/", "upload/", "_files/", "msd/work/" );
		if( $initial_update == 0 )
			; // $skip_dirs[] = "msd/work/";
		else
			$ab_datum = 0;

		if( $skip != array() )
			$skip_dirs = array_merge( $skip_dirs, $skip );

		$handle = opendir( $verzeichnis );
		while( $datei = readdir( $handle ) ) {
			$bSkip = false;
			foreach( $skip_file_pattern as $k => $v )
				if( strpos( $verzeichnis.$datei, $v ) !== false )
					$bSkip = true;
			foreach( $include_class as $k => $v )
				if( (strpos( $verzeichnis.$datei, $v ) !== false) ) {
					$bSkip = false;
				} // if
			foreach( $skip_dirs as $k => $v )
				if( strpos( $verzeichnis.$datei, $v ) !== false )
					$bSkip = true;

			if( !$bSkip && ($datei != ".") && ($datei != "..") && (substr( $datei, 0, 1 ) != '.') ) {
				if( is_dir( $verzeichnis.$datei ) ) {
					$this->dir_rekursiv( $verzeichnis.$datei.'/', $ab_datum, $skip, $skip_file_pattern, $include_class, $initial_update );
				} else {
					$str = $verzeichnis.$datei;

					$last = 0; // filectime( $verzeichnis.$datei );
					if( filemtime( $verzeichnis.$datei ) > $last )
						$last = filemtime( $verzeichnis.$datei );

					$bOK = true;
					if( $last < $ab_datum )
						$bOK = false;
					if( ($datei == "location.txt") || ($datei == "location_vmware_home.php") || ($datei == "location_qnap_home.php") )
						$bOK = false;
					if( $bOK ) {
						foreach( $skip_file_pattern as $k2 => $v2 ) {
							$tempdir = str_replace( BASE_DIR, "", $verzeichnis.$datei );

							if( strpos( $tempdir, $v2 ) !== false )
								$bOK = false;
						} // foreach
						foreach( $include_class as $k2 => $v2 ) {
							$tempdir = str_replace( BASE_DIR, "", $verzeichnis.$datei );

							if( (strpos( $tempdir, $v2 ) !== false) ) {
								$bOK = true;
							} // if
						} // if
					} // foreach

					if( $bOK )
						$files[$str] = array( "file" => $str, "change_time" => date( "d.m.Y H:i:s", $last ), "size" => filesize( $verzeichnis.$datei ) );
				} // else
			} // if
		} // while

		closedir( $handle );
	} // dir_rekursiv

	function get_button_id( $str ) {
		$i = 0;

		if( $this->db->query( "SELECT button_id FROM CORE_ACTION_BUTTONS WHERE title LIKE '".$str."'", "get_button_id" ) ) {
			$r = $this->db->getNext( "get_button_id" );
			$i = $r['button_id'];
		} else {
			// $this->mes->addError( "Button ID für Button ".$str." nicht gefunden." );
		} // else

		return( $i );
	} // get_button_id

	function delete_linked_document( $id ) {
		$this->db->query( "SELECT fullpath FROM GLOBAL_DOKUMENTE WHERE id='".$id."'", "delete_linked_document" );
		$r = $this->db->getNext( "delete_linked_document" );

		// Filesystem
		@unlink( BASE_DIR.$r['fullpath'] );

		// Datenbank
		$this->db->delete( "GLOBAL_DOKUMENTE", "id='".$id."'" );
		$this->db->commit();
	} // delete_linked_document

	function convert_version_filename( $str ) {
		$str = str_replace( "_", " ", $str );
		$str = str_replace( ".zip", "", $str );

		return( $str );
	} // convert_version_filename

	function load_current_version() {
		$_current_version = file_get_contents( dirname( __FILE__ ).'/config_data/current_version.txt' );

		return( $_current_version );
	} // load_current_version

	function save_current_version( $strVersion ) {
		file_put_contents( BASE_DIR.'classes/config_data/current_version.txt', $strVersion );
	} // save_current_version

	function create_version_info( $path, $version ) {
		// Old
		file_put_contents( $path.'/last_version.txt', $_POST['version'] );

		/*
		// New
		$strVersion = substr( $_POST['version'], 8, 4 );
		$strDate =  substr( $_POST['version'], 14, 10 );

		$change_log = array();
		include( BASE_DIR."/admin/core_change_log_array.php" );
		if( isset( $update[$strVersion] ) )
			foreach( $update[$strVersion]['items'] as $k => $line ) {
				$change_log['change['.$k.']'] = $line;
			} // foreach

		$update_file = '';
		if( file_exists( BASE_DIR.'admin/update/update_'.$strVersion.'.php' ) )
			$update_file = 'update_'.$strVersion.'.php';

		$arr = array(
			"version" => array(
				"version" => $this->convert_version_filename( $_POST['version'] ),
				"version_filename" => $_POST['version'].".zip",
				"version_no" => $strVersion,
				"version_date" => $strDate,
				"version_creation" => date( "d.m.Y", time() ),
			),
			"update" => array(
				"update_file" => $update_file,
			),
			"change_log" => $change_log
		);

		$this->write_ini_file( $arr, $path.'/version_info.txt', true );

//		$arr = parse_ini_file( $path.'/version_info.txt' );
 		*/
	} // create_version_info

	function write_ini_file( $assoc_arr, $path, $has_sections=false ) {
		$content = "";
		if( $has_sections ) {
			foreach( $assoc_arr as $key=>$elem ) {
				$content .= "[".$key."]\n";
				foreach( $elem as $key2 => $elem2 ) {
					if( is_array( $elem2 ) ) {
						for($i=0;$i<count($elem2);$i++) {
							$content .= $key2."[] = \"".$elem2[$i]."\"\n";
						} // for
					} else
						if($elem2=="")
							$content .= $key2." = \n";
						else
							$content .= $key2." = \"".$elem2."\"\n";
				} // foreach
				$content .= "\n";
			} // foreach
		} else {
			foreach( $assoc_arr as $key => $elem ) {
				if( is_array( $elem ) ) {
					for($i=0;$i<count($elem);$i++) {
						$content .= $key."[] = \"".$elem[$i]."\"\n";
					} // for
				} else
					if($elem=="")
						$content .= $key." = \n";
					else
						$content .= $key." = \"".$elem."\"\n";
			} // foreach
		} // else

		if( !$handle = fopen( $path, 'w' ) ) {
			return false;
		} // if

		$content = substr( $content, 0, -1 );

		$success = fwrite( $handle, $content );
		fclose( $handle );

		return $success;
	} // write_ini_file

	function print_button( $str ) {
		echo $this->get_button( $str );
	} // print_button

	function check_user_access() {
		$userinfo = $this->load_user( $_SESSION['c_user_id'], "user_level" );

		$file = str_replace( SUBDIR, '', $_SERVER['REQUEST_URI'] );
		if( strpos( $file, "_ajax" ) !== false )
			return;

		if( $file[0] == '/' ) $file = substr( $file, 1 );
		$bFound = false;
		$str2 = array( "", 0 );

		$r['min_user_level'] = 990;
		$r['project_id'] = 0;
		if( !$bFound && $this->db->query( "SELECT project_id, min_user_level FROM CORE_MAINMENU WHERE file='".$file."'" ) ) {
			$r = $this->db->getNext();
			$bFound = true;
		} // if
		if( !$bFound ) { // File mit ? im Namen
			$str = explode( "?", $file );
			if( isset( $str[1] ) ) {
				$str2 = explode( "=", $str[1] );

				if( $str2[0] != "indiv_list_id" ) {
					if( isset( $_SESSION['list_id'] ) )
						$str2[1] = $_SESSION['list_id'];
					else
						$str2[1] = 0;
				} // if

				if( $this->db->query( "SELECT project_id, min_user_level FROM CORE_MAINMENU WHERE file='".$str[0]."' AND list_id='".$str2[1]."'" ) ) {
					$r = $this->db->getNext();
					$bFound = true;
				} // if

				if( $str2[0] == 'project_id' )
					if( $this->db->query( "SELECT project_id, min_user_level FROM CORE_MAINMENU WHERE file='".$str[0]."'" ) ) {
						$r = $this->db->getNext();
						$bFound = true;
					} // if
			} // if
		} // if

		if( !$this->project_allowed( $r['project_id'] ) )
			$r['min_user_level'] = 990;

		if( (strpos( $file, "admin/core_entry.php" ) !== false) || ((strpos( $file, "admin/core_backend.php" ) !== false) || (strpos($_SERVER['REQUEST_URI'], '/admin/list_redirect.php') !== false)) )
			$r['min_user_level'] = 0;

		if( $userinfo['user_level'] < $r['min_user_level'] ) {
			echo '
				<div style="display: none;">
					FileToAccess: '.$file.'<br>
					REQUEST_URI: '.$_SERVER['REQUEST_URI'].'<br>
					CurrentUserLevel: '.$userinfo['user_level'].'<br>
					FoundProjectID: '.$r['project_id'].'<br>
					FoundFileInDatabase: '.$bFound.'<br>
					FileMinUserLevel: '.$r['min_user_level'].'<br>
					CheckedListID: '.$str2[1].'<br>
					<pre>'.
						print_r( $_SESSION, true ).'
					</pre>
				</div>';

			echo "ERROR...".$userinfo['user_level']." ".$r['min_user_level'].' <a href="/'.SUBDIR.'admin/login_logout.php">Logout</a>';
			exit;
		} // if
	} // check_user_access

	function array_sort_by_column( &$arr, $col, $dir = SORT_ASC ) {
		$sort_col = array();
		foreach( $arr as $key => $row ) {
			$sort_col[$key] = $row[$col];
		} // foreach

		array_multisort($sort_col, $dir, $arr);
	} // array_sort_by_column

	function show_backend_text() {
		echo '<div id="content_scroll">';
		//echo 'das ist ein beispieltext';
		echo '</div>';
	} // show_backend_text

	function clean_filename( $str ) {
		$bad='/[\/:*?"<>|]/';

		$str = str_replace( "+", "_", $str );
		$str = str_replace( " ", "_", $str );
		$str = preg_replace( $bad ,"_", $str );

		return( $str );
	} // clean_filename

	function recursive_array_search($needle,$haystack) {
		foreach($haystack as $key=>$value) {
			$current_key=$key;
			if($needle===$value OR (is_array($value) && $this->recursive_array_search($needle,$value) !== false)) {
				return $current_key;
			}
		}
		return false;
	} // recursive_array_search

	function get_button( $str, $bOnlyIcon = false ) {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location  = SIMULATE_LOCATION;

		switch( $str ) {
			case 'abweisen': 								$img = '<i class="far fa-times-circle" style="color: #dc3545;"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'alle anzeigen': 					$img = '<i class="fas fa-sync-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'alles abweisen': 					$img = '<i class="far fa-times-circle" style="color: #dc3545;"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'alles OK': 								$img = '<i class="far fa-check-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'ändern': 									$img = '<i class="fas fa-pencil-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'anmelden': 								$img = '<i class="fas fa-sign-in-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'anzeigen': 								$img = '<i class="fas fa-search"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Auswahl (gedruckt = 0)': 	$img = '<i class="fas fa-plus-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Auswahl aufheben': 				$img = '<i class="fas fa-minus-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'bestellen': 							$img = '<i class="fas fa-list-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Beispiel':								$img = '<i class="fas fa-info-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Datei erzeugen': 					$img = '<i class="fas fa-file-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Download':		 						$img = '<i class="fas fa-download"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Druck erstellen': 				$img = '<i class="fas fa-print"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Drucken': 								$img = '<i class="fas fa-print"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'E-Mail senden': 					$img = '<i class="far fa-envelope"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'E-Mails erstellen': 			$img = '<i class="far fa-envelope"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Exportieren':				 			$img = '<i class="fas fa-file-download"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'hoch':							 			$img = '<i class="fas fa-arrow-alt-circle-up"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Importieren': 						$img = '<i class="fas fa-upload"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'installieren': 						$img = '<i class="fas fa-download"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Karte': 									$img = '<i class="fas fa-images"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Kaufen Grün': 						$img = '<i class="fas fa-cart-arrow-down"></i>'; $str = ''; $mybtn = 'btn btn-success moj-btn-sm cart'; break;
			case 'Kaufen Rot': 							$img = '<i class="fas fa-minus"></i>'; $str = ''; $mybtn = 'btn btn-danger moj-btn-sm cart'; break;
			case 'Kaufen': 									$img = '<i class="fas fa-cart-arrow-down"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'kopieren': 								$img = '<i class="far fa-save"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'kurz': 										$img = '<i class="fas fa-list-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Liste': 									$img = '<i class="fas fa-list-ul"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Liste drucken': 					$img = '<i class="fas fa-print"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'löschen': 								$img = '<i class="fas fa-trash-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Menü': 										$img = '<i class="fas fa-bars"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'neu': 										$img = '<i class="far fa-file-alt"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'nur laufende': 						$img = '<i class="fas fa-list-ul"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'öffnen': 									$img = '<i class="far fa-file-word"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'OK': 											$img = '<i class="fas fa-check-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Passwort rücksetzen': 		$img = '<i class="far fa-question-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Prüfen': 									$img = '<i class="far fa-hdd"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'registrieren': 						$img = '<i class="fas fa-user-plus"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'speichern': 							$img = '<i class="far fa-save"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'suchen': 									$img = '<i class="fas fa-search"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'synchronisieren': 				$img = '<i class="fas fa-globe-americas"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'Überprüfungen': 					$img = '<i class="far fa-calendar-check"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'upload': 									$img = '<i class="fas fa-upload"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'upload (zip)':						$img = '<i class="fas fa-upload"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'upload (enc)':						$img = '<i class="fas fa-upload"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'upload (enc_v2)':					$img = '<i class="fas fa-upload"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'TD upload alle (enc)':		$img = '<i class="fas fa-upload"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'URL öffnen': 							$img = '<i class="fas fa-file-import"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'zuordnen': 								$img = '<i class="fas fa-plus-circle"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case '3D Suche starten': 				$img = '<i class="fas fa-search"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;
			case 'OEM aus 3D Suche importieren': $img = '<i class="fas fa-download"></i>'; $mybtn = 'btn btn-primary moj-btn-sm'; break;

			default: $img = '<i class="far fa-times-circle"></i>'; $str = 'Button nicht definiert: '.$str; break;
		} // switch

		if( $bOnlyIcon ) $str = '';

		return( '<div class="'.$mybtn.'" style="min-width: 80px;">'.$img.' '.$this->t->t( $str ).'</div>' );
	} // get_button

	function get_location_password( $location = "core" ) {
		$passphrase = '';

		if( strpos( $location, "_atit_at" ) !== false ) $location = 'tecdocbase_at';

		switch( $location ) {
			case 'basilica_at':
					if( class_exists( 'basilica_functions' ) ) {
						$f = basilica_functions::getInstance();
						$passphrase = $f->get_location_password( $location );
					} // if
					break;
			case 'tecdocbase_at':
					if( class_exists( 'tecdoc_functions' ) ) {
						$f = tecdoc_functions::getInstance();
						$passphrase = $f->get_location_password( $location );
					} // if
				break;
			case 'impulsit_at':
					if( class_exists( 'impulsit_functions' ) ) {
						$f = impulsit_functions::getInstance();
						$passphrase = $f->get_location_password( $location );
					} // if
				break;
			default:
					$f = default_functions::getInstance();
					$passphrase = $f->get_location_password( $location );
				break;
		} // switch

		if( $passphrase == '' )
			echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> password can not be determined for location: '.$location.'</div>';

		return( $passphrase );
	}

	function crypt_file( $strFile, $location = '', $passphrase = '' ) {
		if( $passphrase == '' )
			$passphrase = $this->get_location_password( $location );

		$iv = substr(md5('iv'.$passphrase, true), 0, 8);
		$key = substr(md5('pass1'.$passphrase, true) .
				md5('pass2'.$passphrase, true), 0, 24);
		$opts = array('iv'=>$iv, 'key'=>$key);

		// Read Source File
		$fp = fopen( $strFile, 'rb' );
		$data = stream_get_contents($fp);
		fclose( $fp );

		// Write Crypted File
		$fp = fopen( $strFile.'_enc', 'wb' );
		stream_filter_append( $fp, 'mcrypt.tripledes', STREAM_FILTER_WRITE, $opts );
		fwrite( $fp, $data );
		fclose( $fp );
	} // crypt_file

	function decrypt_file( $strFile, $location = '', $passphrase = '' ) {
		if( $passphrase == '' )
			$passphrase = $this->get_location_password( $location );

		$iv = substr(md5('iv'.$passphrase, true), 0, 8);
		$key = substr(md5('pass1'.$passphrase, true) .
				md5('pass2'.$passphrase, true), 0, 24);
		$opts = array('iv'=>$iv, 'key'=>$key);

		// Read Crypted File
		$fp = fopen( $strFile."_enc", 'rb' );
		stream_filter_append( $fp, 'mdecrypt.tripledes', STREAM_FILTER_READ, $opts );
		$data = stream_get_contents( $fp );
		fclose( $fp );

		// Write Back to Source File
		$fp = fopen( $strFile, 'wb' );
		fwrite( $fp, $data );
		fclose( $fp );

		// Delete Crypted File
		unlink( $strFile."_enc" );
	} // decrypt_file

	function crypt_file_v2( $source, $location = '', $passphrase = '' ) {
		if( $passphrase == '' )
			$passphrase = $this->get_location_password( $location );

		$_FILE_ENCRYPTION_BLOCKS = 10000;
		$dest = $source.'_enc_v2';

		$passphrase = substr(sha1($passphrase, true), 0, 16);
		$iv = openssl_random_pseudo_bytes(16);

		$error = false;
		if ($fpOut = fopen($dest, 'w')) {
			// Put the initialzation vector to the beginning of the file
			fwrite($fpOut, $iv);
			if ($fpIn = fopen($source, 'rb')) {
				while (!feof($fpIn)) {
					$plaintext = fread($fpIn, 16 * $_FILE_ENCRYPTION_BLOCKS);
					$ciphertext = openssl_encrypt($plaintext, 'AES-128-CBC', $passphrase, OPENSSL_RAW_DATA, $iv);
					// Use the first 16 bytes of the ciphertext as the next initialization vector
					$iv = substr($ciphertext, 0, 16);
					fwrite($fpOut, $ciphertext);
				}
				fclose($fpIn);
			} else {
				$error = true;
			}
			fclose($fpOut);
		} else {
			$error = true;
		}

		return $error ? false : $dest;
	}

	function decrypt_file_v2($source, $location = '', $passphrase = '') {
		if( $passphrase == '' )
			$passphrase = $this->get_location_password( $location );

		$_FILE_ENCRYPTION_BLOCKS = 10000;
		$dest = $source;
		$source = $source.'_enc_v2';

		$passphrase = substr(sha1($passphrase, true), 0, 16);

		$error = false;
		if ($fpOut = fopen($dest, 'w')) {
			if ($fpIn = fopen($source, 'rb')) {
				// Get the initialzation vector from the beginning of the file
				$iv = fread($fpIn, 16);
				while (!feof($fpIn)) {
					$ciphertext = fread($fpIn, 16 * ($_FILE_ENCRYPTION_BLOCKS + 1)); // we have to read one block more for decrypting than for encrypting
					$plaintext = openssl_decrypt($ciphertext, 'AES-128-CBC', $passphrase, OPENSSL_RAW_DATA, $iv);
					// Use the first 16 bytes of the ciphertext as the next initialization vector
					$iv = substr($ciphertext, 0, 16);
					fwrite($fpOut, $plaintext);
				}
				fclose($fpIn);
			} else {
				$error = true;
			}
			fclose($fpOut);
		} else {
			$error = true;
		}

		return $error ? false : $dest;
	}

	function remote_file_exists( $url ) {
		$b = @file_get_contents( $url, false, null, 0, 1 );

		return( $b );
	}

	function fn( $number, $decimals = 2 ) {
		$dec_point = ",";
		$thousands_sep = "";

		return( number_format( $number, $decimals, $dec_point, $thousands_sep) );
	}

	function load_projects( &$projects, &$old_projects ) {
		$projects = array();
		$old_projects = array();
		foreach( glob( BASE_DIR.'classes/config_data/location_*.php' ) as $strFilename ) {
			$str = fgets( fopen( $strFilename, 'r' ) );

			$group = '';
			$project = '';
			sscanf( $str, '<?php // <!-- GROUP: %s -->', $group );
			sscanf( basename( $strFilename ), 'location_%s', $project );
			$project = substr( $project, 0, -4 );

			if( strpos( $strFilename, 'atit_at' ) !== false ) $group = 'OLD';

			switch( $group ) {
				case 'LIVE': $projects[] = $project; break;
				case 'PRIVATE': $projects[] = $project; break;
				case 'DEMO': $projects[] = $project; break;
				case 'SOON': $projects[] = $project; break;
				default: $old_projects[] = $project; break;
			}
		} // foreach

		sort( $projects );
	}

	// -------------------------------------------------------------------------------------------------
	// --- PAGINATOR
	//-------------------------------------------------------------------------------------------------
	function paginator_get_start( $iCurrentValue ) {
		return( ($iCurrentValue - 1) * $this->paginator_max_list + 1);
	} // paginator_get_start

	function paginator_print_input_box( $iCurrentValue ) {
		if( $this->paginator_current_page == $iCurrentValue ) {
			// aktive Seite

			if( !$this->paginator_echo ) {
				// nur Aktiv
				$output = '<li class="page-item active"><div class="page-link">'.$iCurrentValue.'</div></li>';
			} else {
				// direkt Sprung auf Seite
				$output = '<li class="page-item"><i onClick="paginator_focus_page_input($(this));" class="far fa-edit bg-primary text-white border border-primary p-1" style="line-height: 24px; font-size: 10px; cursor: pointer;"></i><div class="float-right"><input class="page-link bg-primary text-white border border-primary paginator_page_input" type="text" name="paginator_page" size="1" value="'.$iCurrentValue.'"></div></li>';
			} // else
		} else {
			// nicht aktive Seite
			$output = '<li class="page-item"><a class="page-link text-primary" '.
			($this->paginator_links?'href="?indiv_page='.$iCurrentValue.'"':'data-page="'.$iCurrentValue.'" data-start="'.$this->paginator_get_start( $iCurrentValue ).'"').
			'>'.$iCurrentValue.'</a></li>';
		} // else

				return( $output );
	} // paginator_print_input_box

	function paginator_print_dots( $strWhich ) {
		$output = '';
		$bShow = false;
		switch( $strWhich ) {
			case 'prev':
				if( $this->paginator_current_page > 4 ) $bShow = true;
				break;
			case 'next':
				if( $this->paginator_current_page < ($this->paginator_max_pages - 3) ) $bShow = true;
				break;
		} // switch

		if( $bShow )
			$output = '<li class="page-item disabled"><div class="page-link">...</div></li>';

			return( $output );
	} // paginator_print_dots

	function paginator_print_scroller( $strWhich ) {
		$disabled = '';
		switch( $strWhich ) {
			case 'prev':
				$iDestinationPage = $this->paginator_current_page - 1;
				$strSymbol = '&laquo;';
				if( $this->paginator_current_page <= 1 ) $disabled = ' disabled';
				break;
			case 'next':
				$iDestinationPage = $this->paginator_current_page + 1;
				$strSymbol = '&raquo;';
				if( $this->paginator_current_page >= $this->paginator_max_pages ) $disabled = ' disabled';
				break;
		} // switch

		$output = '<li class="page-item'.$disabled.'"><a class="page-link" '.
			($this->paginator_links?'href="?indiv_page='.$iDestinationPage.'"':'data-page="'.$iDestinationPage.'" data-start="'.$this->paginator_get_start( $iDestinationPage ).'"').
			'><span aria-hidden="true">'.$strSymbol.'</span></a></li>';

		return( $output );
	} // paginator_print_scroller

	function paginator_print( $iCurrentPage, $iMaxItems, $iMaxList = 100, $bEcho = true, $bLinks = true ) {
		$this->paginator_links = $bLinks;
		$this->paginator_max_list = $iMaxList;
		$this->paginator_current_page = $iCurrentPage;
		$this->paginator_max_pages = floor(($iMaxItems-1)/$iMaxList+1);
		$this->paginator_echo = $bEcho;

		if( $this->paginator_current_page <= 1 ) $this->paginator_current_page = 1;
		if( $this->paginator_current_page >= $this->paginator_max_pages ) $this->paginator_current_page = $this->paginator_max_pages;

		$output = '';

		if( !isset( $this->paginator_printed ) || ($this->paginator_printed !== true) ) {
			$this->paginator_printed = true;
			$output .= '<span id="paginator_current_page" style="display: none;">'.$this->paginator_current_page.'</span>';
		} // if

		$output .= '
			<nav id="paginator" aria-label="Page navigation">
			  <ul class="pagination mt-1 mb-1 justify-content-center">'.
					$this->paginator_print_scroller( 'prev' ).
					$this->paginator_print_input_box( 1 ).
					$this->paginator_print_dots( 'prev' );

		$min = $this->paginator_current_page - 2;
		$max = $this->paginator_current_page + 2;
		if( $min < 2 ) $min = 2;
		if( $max > ($this->paginator_max_pages - 1) ) $max = ($this->paginator_max_pages - 1);

		for( $i = $min; $i <= $max; $i++ )
			$output .= $this->paginator_print_input_box( $i );

		$output .=
					$this->paginator_print_dots( 'next' ).
					$this->paginator_print_input_box( $this->paginator_max_pages ).
					$this->paginator_print_scroller( 'next' ).'
			  </ul>
			</nav>
		';

		if( $bEcho )
			echo $output;
		else
			return( $output );
	} // paginator_print

	function zip_message($code) {
		switch( $code ) {
			case 0: return 'No error';
			case 1: return 'Multi-disk zip archives not supported';
			case 2: return 'Renaming temporary file failed';
			case 3: return 'Closing zip archive failed';
			case 4: return 'Seek error';
			case 5: return 'Read error';
			case 6: return 'Write error';
			case 7: return 'CRC error';
			case 8: return 'Containing zip archive was closed';
			case 9: return 'No such file';
			case 10: return 'File already exists';
			case 11: return 'Can\'t open file';
			case 12: return 'Failure to create temporary file';
			case 13: return 'Zlib error';
			case 14: return 'Malloc failure';
			case 15: return 'Entry has been changed';
			case 16: return 'Compression method not supported';
			case 17: return 'Premature EOF';
			case 18: return 'Invalid argument';
			case 19: return 'Not a zip archive';
			case 20: return 'Internal error';
			case 21: return 'Zip archive inconsistent';
			case 22: return 'Can\'t remove file';
			case 23: return 'Entry has been deleted';
			default: return 'An unknown error has occurred('.intval($code).')';
		}
	}
} // functions
?>