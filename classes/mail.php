<?php
require_once( 'config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'phpmailer_v6.0.6/src/Exception.php' );
require_once( CLASS_DIR.'phpmailer_v6.0.6/src/PHPMailer.php' );
require_once( CLASS_DIR.'phpmailer_v6.0.6/src/SMTP.php' );

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class fb_mail {
	static private $instance = null;
	private $db = null;
	private $mail = null;
	private $mes = null;
	private $f = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->mes = messages::getInstance();
		$this->mail = new PHPMailer;
	} // __construct

	private function __clone() {
	} // __clone

	public function __destruct() {
	} // __destruct

	function mail_header( $id = 0 ) {
		$str = '<html><body>';

		return( $str );
	} // mail_header

	function mail_footer() {
		$str = '</body></html>';

		return( $str );
	} // mail_footer

	function footer_mailtext() {
		$str = '';

		return( $str );
	} // footer_mailtext

	function mail_new_user_subadmin( $user_id ) {
		global $_location;

		$this->db->query( "SELECT email_adress FROM CORE_SETUP WHERE id='1'" );
		$core_setup = $this->db->getNext();

		$this->db->query( "SELECT sub_admin_email FROM TEC_SETUP WHERE id='1'" );
		$setup = $this->db->getNext();

		$this->db->query( "
			SELECT cr.email AS admin_email, cr.name AS admin_name, u.*
			FROM CORE_USER_INFO AS u
			LEFT JOIN CORE_USER_INFO AS cr ON (cr.user_id=u.sub_created_by_user_id)
			WHERE u.user_id='".$user_id."'" );
		$r = $this->db->getNext();

		$text =
			$this->mail_header().
			'New user created by '.DOMAIN.' ('.$_location.')'.': '.$r['sub_created_by_user_id'].' - '.$r['admin_email'].' ('.$r['admin_name'].')<br /><br />'.
			'user id: '.$r['user_id'].'<br />'.
			'user login/email: '.$r['email'].'<br />'.
			'user name: '.$r['name'].'<br />'.
			'first name: '.$r['sub_first_name'].'<br />'.
			'last name: '.$r['sub_last_name'].'<br />'.
			'adress: '.$r['sub_adress'].'<br />'.
			'phone: '.$r['sub_phone'].'<br />'.
			'email: '.$r['sub_email'].
			$this->mail_footer();

		if( $this->send_mail( $setup['sub_admin_email'], $text, 'atitec - new user', '', '', $core_setup['email_adress'] ) ) {
			$this->mes->addInfo( "mail for new user sent to ".$setup['sub_admin_email'] );
		} // if
		$this->mes->saveMessages();
	} // mail_new_user_subadmin

	function mail_testmail( $email ) {
		$text =
			$this->mail_header().
			'Testmail'.
			$this->mail_footer();

		if( $this->send_mail( $email, $text, 'Testmail' ) ) {
			$this->mes->addInfo( "Testmail versendet an ".$email );
		} // if
		$this->mes->saveMessages();
	} // mail_test

	function mail_password( $email, $password ) {
		$this->db->query( "SELECT name, email FROM CORE_USER_INFO WHERE email='".$email."'" );
		$userinfo = $this->db->getNext();

		$url = DOMAIN.'admin/login_login.php';

		$text  = 'Hallo '.$userinfo['name'].',<br ><br >';
		$text .= 'dein Passwort wurde neugesetzt.<br /><br />';
		$text .= 'E-Mail: '.$userinfo['email'].'<br />';
		$text .= 'Passwort: '.$password.'<br />';
		$text .= 'zum Einloggen: <a href="'.$url.'">'.$url.'</a><br />';
		$text .= $this->footer_mailtext();

		$this->send_mail( $userinfo['email'], $text, "Passwort zurücksetzen" );
	} // mail_password

	function mail_register( $email ) {
		$this->db->query( "SELECT name, email FROM CORE_USER_INFO WHERE email='".$email."'" );
		$userinfo = $this->db->getNext();

		$id = md5( $userinfo['email'].functions::getInstance()->get_location_password() );
		$url = DOMAIN.'admin/login_login.php';
		if( strpos( DOMAIN, "192.168" ) !== false ) $url = 'http://qnap.sordje.home/'.SUBDIR.'admin/reset_password.php';

		$text = $this->mail_header().
		'Guten Tag,<br>'.
		'<br>'.
		'nachstehend erhalten Sie den Link zur Neusetzung Ihres Passworts. Klicken Sie jetzt auf diesen Link um Ihr Passwort zu ändern und die Registrierung abzuschliessen:<br>'.
		'<br>'.
		'<a href="'.$url.'?id='.$id.'">'.$url.'?id='.$id.'</a><br>'.
		'<br>'.
		'Wenn die Weiterleitung nicht funktioniert, kopieren Sie bitte den Link in die Adressleiste Ihres Browsers.<br>'.
		'<br>'.
		'Um für Ihr Konto optimale Sicherheit zu gewährleisten, sollten Sie jedoch innerhalb der nächsten 24 Stunden ein neues, persönliches Passwort vergeben.<br>'.
		'<br>'.
		'Sollten Sie keine Registrierung beantragt haben, können Sie dieses Mail als gegenstandslos betrachten.<br>'.
		'<br>'.
		'Mit freundlichen Grüßen<br>'.
		'Ihr impuls IT Team'.$this->mail_footer();

		$this->send_mail( $userinfo['email'], $text, "Registrierung" );
	} // mail_password

	function mail_password_lost( $email ) {
		$this->db->query( "SELECT name, email FROM CORE_USER_INFO WHERE email='".$email."'" );
		$userinfo = $this->db->getNext();

		$id = md5( $userinfo['email'].functions::getInstance()->get_location_password() );
		$url = DOMAIN.'admin/reset_password.php';
		if( strpos( DOMAIN, "192.168" ) !== false ) $url = 'http://qnap.sordje.home/'.SUBDIR.'admin/reset_password.php';

		$text = $this->mail_header().
			'Guten Tag,<br>'.
			'<br>'.
			'nachstehend erhalten Sie den Link zur Änderung/Neusetzung Ihres Passworts. Klicken Sie jetzt auf diesen Link um Ihr Passwort zu ändern:<br>'.
			'<br>'.
			'<a href="'.$url.'?id='.$id.'">'.$url.'?id='.$id.'</a><br>'.
			'<br>'.
			'Wenn die Weiterleitung nicht funktioniert, kopieren Sie bitte den Link in die Adressleiste Ihres Browsers.<br>'.
			'<br>'.
			'Um für Ihr Konto optimale Sicherheit zu gewährleisten, sollten Sie jedoch innerhalb der nächsten 24 Stunden ein neues, persönliches Passwort vergeben.<br>'.
			'<br>'.
			'Sollten Sie keine Passwortänderung beantragt haben, können Sie dieses Mail als gegenstandslos betrachten.<br>'.
			'<br>'.
			'Mit freundlichen Grüßen<br>'.
			'Ihr impuls IT Team'.$this->mail_footer();

		$this->send_mail( $userinfo['email'], $text, "Passwort zurücksetzen" );
	} // mail_password_lost

	function mail_password_lost_finished( $email ) {
		$text = $this->mail_header().
		'Guten Tag,<br>'.
		'<br>'.
		'Sie haben Ihr Passwort erfolgreich geändert.<br>'.
		'Falls Sie dieses Mail irrtümlich erhalten bzw. Ihr Passwort nicht geändert haben, kontaktieren Sie bitte unseren Kundenservice unter kundenservice@impulsit.at.<br>'.
		'<br>'.
		'Mit freundlichen Grüßen<br>'.
		'Ihr impuls IT Team'.$this->mail_footer();

		$this->send_mail( $email, $text, "Passwort Änderung bestätigt" );
	} // mail_password_lost_finished

	function mail_fragen( $email, $name, $str_text, $id ) {
		$text  = 'Hallo '.$name.',<br ><br >';
		$text .= 'Ein Fragebogen wurde ausgef&uuml;llt.<br /><br />';
		$text .= $this->footer_mailtext();

		$this->send_mail( $email, $text, NEW_FB, $this->mail_header( $id ).$str_text.$this->mail_footer() );
	} // mail_fragen

	function send_mail( $to, $text, $typ = "Mail", $attachment = "", $cc = "", $bcc = "" ) {
		$str = $typ;
		$attach = false;

		switch( $typ ) {
			case NEW_PW: $str = "neues Passwort"; break;
			case REGISTER: $str = "Registrierung"; break;
			case PW_LOST: $str = "Passwort vergessen"; break;
			case NEW_FB:
					$str = "neuer Fragebogen";
					$attach = true;
				break;
			default: $str = $typ;
		} // switch

		$setup = $this->load_setup( "CORE_SETUP" );
		$i = false;

		try {
			if( $setup['email_extern_smtp'] == 1 ) {
				$this->mail->IsSMTP();
				$this->mail->SMTPAuth = true;
				$this->mail->SMTPOptions = array(
					'ssl' => array(
						'verify_peer' => false,
						'verify_peer_name' => false,
						'allow_self_signed' => true
					)
				);
				$this->mail->Host = $setup['email_smtp_server'];
				$this->mail->Port = $setup['email_smtp_port'];
				$this->mail->Username = $setup['email_smtp_user'];
				$this->mail->Password = $setup['email_smtp_password'];
				$this->mail->SMTPSecure = $setup['email_smtp_secure'];
			} // if

			// From
			$this->mail->SetFrom( $setup['email_adress'], $setup['email_name'] );
			$this->mail->AddReplyTo( $setup['email_adress'], $setup['email_name'] );

			// To
			$this->mail->AddAddress( $to, $to );
			if( $cc != "" ) $this->mail->AddCC( $cc, $cc );
			if( $bcc != "" ) $this->mail->AddBCC( $bcc, $bcc );

			// Message
			$this->mail->CharSet = "UTF-8";
			$this->mail->isHTML( true );
			$this->mail->Subject = $str;
			$this->mail->MsgHTML( $text );

			// Attachments
			$this->mail->ClearAttachments();
			if( $attach )
				$this->mail->AddStringAttachment( $attachment, 'auswertung.html', 'base64', 'text/html' );
			if( is_array( $attachment ) ) {
				foreach( $attachment as $k => $v ) {
					if( $v['send_filename'] != ".rtf")
						$this->mail->AddStringAttachment( $v['content'], $v['send_filename'], 'base64', 'text/rtf' );
				} // foreach
			}

//			print_r( $this->mail ); exit;
			// Send
			$i = $this->mail->send();

			if( !$i ) $this->mes->addError( "ERROR: mail->send(): ".$this->mail->ErrorInfo );
		} catch (phpmailerException $e) {
			$this->mes->addError( "ERROR: ".$e->errorMessage() );

			return( false );
		} catch (Exception $e) {
			$this->mes->addError( "ERROR: ".$e->getMessage() );

			return( false );
		}

		return( $i );
	} // send_mail

	function send_mail_contact( $to, $from, $from_name, $text ) {
		try {
			$this->mail->AddReplyTo( $from, $from_name );
			$this->mail->AddAddress( $to, $to );
			$this->mail->SetFrom( $from, $from_name );
			$this->mail->AddReplyTo( $from, $from_name );
			$this->mail->Subject = 'RRR Kontakt Mail';
			$this->mail->MsgHTML( $text );
			$this->mail->Send();
		} catch (phpmailerException $e) {
			$this->mes->addError( $e->errorMessage() ); //Pretty error messages from PHPMailer
		} catch (Exception $e) {
			$this->mes->addError( $e->getMessage() ); //Boring error messages from anything else!
		}
	} // send_mail_contact

	function load_setup( $table = "SETUP" ) {
		if( !$this->db->query( "SELECT * FROM ".$table." WHERE id='1'" ) ) {
			$this->db->insert( $table, array( "id" => 1 ) );
			$this->db->commit();

			$this->db->query( "SELECT * FROM ".$table." WHERE id='1'" );
		} // if

		$r = $this->db->getNext();

		return( $r );
	} // load_setup

} // fb_mail
?>