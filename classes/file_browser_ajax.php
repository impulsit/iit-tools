<?php
require_once( dirname( __FILE__ ).'/ajax_header.php' );
$result = array();

global $_location;

$location = $_location;
if( defined( "SIMULATE_LOCATION" ) )
	$location  = SIMULATE_LOCATION;

$strPath = "";

switch( $_POST['type'] ) {
	case "banner":
		$strPath = 'upload/'.$location.'/banner/';
		break;
	case "univ_items":
		$strPath = 'upload/'.$location.'/univ_items/';
		break;
} // switch

switch( $_POST['action'] ) {
	case "load_files":
			$files = array();
			if( $strPath != "" )
				foreach( glob( BASE_DIR.$strPath."*.*" ) as $k => $v ) {
					$files[] = basename( $v );
				} // foreach

			$result = array( "files" => $files );
		break;
	case "delete_file":
			if( ($strPath != "") && file_exists( BASE_DIR.$strPath.$_POST['file'] ) )
				unlink( BASE_DIR.$strPath.$_POST['file'] );
		break;
} // switch

$return['resultData'] = $result;

echo json_encode( $f->convert_ajax_remove( $return ) );
?>