<?php
require_once( 'config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR."core_functions.php" );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'print.php' );
require_once( CLASS_DIR.'report.php' );
require_once( CLASS_DIR."kalender.php" );
require_once( CLASS_DIR."translate.php" );
require_once( CLASS_DIR."list_project_functions.php" );
require_once( CLASS_DIR."theme_functions.php" );

class liste {
	static private $instance = null;
	private $db = null;
	private $f = null;
	private $print = null;
	private $report = null;
	private $mes = null;
	private $interface = null;
	private $kal = null;
	private $t = null;
	private $list_project_functions = null;
	private $theme = null;

	public $field = array();
	private $list_query = "";
	private $delete_primary_key = "";
	private $delete_table = "";
	private $current_list = 0;
	private $action_buttons = array();
	private $action_buttons_main = array();
	public $file;
	private $start = 0;
	private $max_list = 100;
	private $lines_selectable = 0, $delete_allowed = false, $change_allowed = false, $insert_allowed = false;
	private $infotext = "";

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		global $_location;

		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
		$this->print = druck::getInstance();
		$this->mes = messages::getInstance();
		$this->report = report::getInstance();
		$this->kal = kalender::getInstance();
		$this->t = translate::getInstance();
		$this->list_project_functions = list_project_functions::getInstance();
		$this->theme = theme_functions::getInstance();

		core_functions::getInstance()->init();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function clear_query( $query ) {
		$query = str_replace( "\n", " ", $query );
		$query = str_replace( "\t", " ", $query );#305833
		$query = str_replace( "\r", " ", $query );
		$query = str_replace( "<br />", " ", $query );
		$query = str_replace( "<br>", " ", $query );

		return( $query );
	} // clear_query

	function load_list( $list_id ) {
		$arr = array();

		// Allgemeines
		if( $this->db->query( "SELECT list_query, sort_order, delete_primary_key, delete_table, switchable, back_up_file, back_up_list, file, lines_selectable, insert_allowed, change_allowed, delete_allowed FROM CORE_LISTS WHERE list_id='".$list_id."'" ) ) {
			$r = $this->db->getNext();
			foreach( $r as $k => $v ) {
				$arr[$k] = $v;
			} // foreach

			$arr['list_query'] = $this->clear_query( $arr['list_query'] );
		} else
			$this->mes->addError( "Liste kann nicht geladen werden." );

		// Action Buttons
		$but = array();
		$this->db->query( "
			SELECT b.title
			FROM CORE_LISTS_BUTTONS AS l
			LEFT JOIN CORE_ACTION_BUTTONS AS b ON (b.button_id=l.button_id)
			WHERE l.list_id='".$list_id."' AND l.main_button='0' ORDER BY l.pos" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$but[] = $r['title'];
		} // while
		$arr['action_buttons'] = $but;

		$but = array();
		$this->db->query( "
			SELECT b.title
			FROM CORE_LISTS_BUTTONS AS l
			LEFT JOIN CORE_ACTION_BUTTONS AS b ON (b.button_id=l.button_id)
			WHERE l.list_id='".$list_id."' AND l.main_button='1' ORDER BY l.pos" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$but[] = $r['title'];
		} // while
		$arr['action_buttons_main'] = $but;

		// Felder
		$but = array();
		if( $this->db->query( "
			SELECT *
			FROM CORE_LISTS_FIELDS
			WHERE list_id='".$list_id."'
			ORDER BY pos ASC" ) ) {
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();

				$r['select_query'] = $this->clear_query( $r['select_query'] );

				foreach( $r as $k => $v ) {
					$but[$k] = $v;
				} // foreach
				$arr['fields'][] = $but;
			} // while
		} else
			$this->mes->addError( "Felder von Liste können nicht geladen werden." );

		return( $arr );
	} // function

	function print_menu() {
		// Sortierung
		if( isset( $_GET['indiv_sort_order'] ) ) {
			$_SESSION['list_'.$this->current_list]['sort_order'] = $_GET['indiv_sort_order'];
		} // if

		if( isset( $_GET['indiv_max_list'] ) ) {
			$this->max_list = $_GET['indiv_max_list'];
		} // if

		// Filter
		$filter_text = '';
		if( isset( $_SESSION['list_'.$this->current_list]['filter_text'] ) ) {
			$_SESSION['list_'.$this->current_list]['filter_text'] = trim( $_SESSION['list_'.$this->current_list]['filter_text'] );

			$filter_text = $_SESSION['list_'.$this->current_list]['filter_text'];
			$_SESSION['list_'.$this->current_list]['start'] = 1;
			$_SESSION['list_'.$this->current_list]['page'] = 1;
		} // if

		// Verlinkung
		foreach( $_GET as $k => $v ) {
			if( (strpos( $k, "indiv" ) === false) && ($k != 'language') ) {
				unset( $_SESSION['list_'.$this->current_list]['param'] );
				if( $k[1] == "_" ) $k[1] = '.';
				if( !is_array( $_SESSION['list_'.$this->current_list] ) ) $_SESSION['list_'.$this->current_list]['param'] = array();
				if( isset( $_SESSION['list_'.$this->current_list]['param'] ) && !is_array( $_SESSION['list_'.$this->current_list]['param'] ) ) $_SESSION['list_'.$this->current_list]['param'] = array();
				$_SESSION['list_'.$this->current_list]['param'][$k] = $v;
			} // if
		} // foreach

		// Detail + Indiv Button
		if( isset( $_GET['indiv_detail'] ) ) {
			if( $_GET['indiv_detail'] == 0 )
				$_SESSION['list_'.$this->current_list]['show'] = "kurz";
				else
					$_SESSION['list_'.$this->current_list]['show'] = "detail";
				$this->show = $_SESSION['list_'.$this->current_list]['show'];
		} // if

		$detail_buttons = '';
		if( isset( $_SESSION['list_'.$this->current_list]['show'] ) ) {
			$detail_buttons = '<a href="?indiv_detail='.(($_SESSION['list_'.$this->current_list]['show']=="kurz")?'1':'0').'" class="link_click_button '.(($_SESSION['list_'.$this->current_list]['show']=="kurz")?'button_checked':'').'">'.$this->f->get_button( 'kurz' ).'</a>';
		} // if

		// Browser Start
		if( isset( $_GET['indiv_start'] ) )
			$_SESSION['list_'.$this->current_list]['start'] = $_GET['indiv_start'];
		if( isset( $_GET['indiv_page'] ) ) {
			$_SESSION['list_'.$this->current_list]['page'] = $_GET['indiv_page'];
			$_SESSION['list_'.$this->current_list]['start'] = ($_SESSION['list_'.$this->current_list]['page'] - 1) * $this->max_list + 1;
		} // if

		echo '
			<input type="hidden" name="current_list" id="current_list" value="'.$this->current_list.'">
			<div class="menu_form shadow">';

		// Hoch
		if( $this->f->set_back_up_file != "" )
			echo '<a href="'.basename( $this->f->set_back_up_file ).'" class="link_click_button">'.$this->f->get_button( 'hoch' ).'</a>';

		if( $this->insert_allowed )
			echo '<a id="new" class="link_click_button">'.$this->f->get_button( 'neu' ).'</a>';

		echo $detail_buttons.$this->list_project_functions->add_special_action_buttons_main( $this->current_list ).'
			</div>

			<form method="post" action="'.$_SERVER['SCRIPT_NAME'].'">
				<div class="search_form shadow">
					<div id="search_form_filter" style="float:left;">'.$this->t->t( "Filter" ).' <input style="height:25px; width:140px;" type="text" id="filter_text" name="filter_text" value="'.$filter_text.'">&nbsp;</div>
					<div style="float:right;">
						<a href="#" onClick="$(this).closest(\'form\').submit()" class="link_click_button">'.$this->f->get_button( "suchen" ).'</a>
						<a href="#" onClick="$(\'#filter_text\').val(\'\');$(this).closest(\'form\').submit()" class="link_click_button">'.$this->f->get_button( "alle anzeigen" ).'</a>
					</div>
				</div>
			</form>';

		if( $this->infotext != "" )
			echo $this->infotext;

    // Info über Liste
    $this->list_project_functions->print_menu_info( $this->current_list );
	} // print_menu

	function parse_commands() {
		if( isset( $_POST['filter_text'] ) ) {
			$_SESSION['list_'.$this->current_list]['filter_text'] = $_POST['filter_text'];
		} // if

		if( isset( $_POST['del'] ) ) {
			$del = key( $_POST['del'] );

			$this->db->query( "SELECT * FROM ".$this->delete_table." WHERE ".$this->delete_primary_key."='".$del."'" );
			$r = $this->db->getNext();
			$this->list_project_functions->write_changelog( $this->current_list, $del, $this->delete_table, $r, array() );

			$this->db->delete( $this->delete_table, $this->delete_primary_key."='".$del."'" );

			// Verbundene Tabellen mitlöschen
			switch( $this->delete_table ) {
				case 'WHY_FRAGEN':
						$this->db->delete( "WHY_ANTWORTEN", "frage_id='".$del."'" );
				  break;
				case 'WHY_FRAGEBOGEN':
						$this->db->query( "SELECT frage_id FROM WHY_FRAGEN WHERE fragebogen_id='".$del."'" );
						while( $this->db->isNext() ) {
							$r = $this->db->getNext();

							$this->db->delete( "WHY_ANTWORTEN", "frage_id='".$r['frage_id']."'" );
						} // while
						$this->db->delete( "WHY_FRAGEN", "fragebogen_id='".$del."'" );
					break;
			} // switch

			// Eventuelle Bilder mitlöschen
			if( $this->db->query( "
				SELECT id
				FROM GLOBAL_DOKUMENTE
				WHERE main_id='".$del."' AND list_id='".$this->current_list."'", "parse_commands" ) ) {
				$r = $this->db->getNext( "parse_commands" );

				$this->f->delete_linked_document( $r['id'] );
			} // if

			$this->db->commit();
		} // if

		$this->parse_action_buttons();

		$this->infotext = $this->list_project_functions->parse_special_buttons( $this->current_list, $_GET );
	} // parse_commands

	function get_var_text( $var, $value ) {
		$str = '';
		foreach( $this->field as $k => $v ) {
			if( isset( $v['fill_fix_type'] ) && ($v['fill_fix_type'] != "") && ($v['fill_fix_param'] == $var) ) {
				$this->db->query( $v['select_query']." WHERE ".$v['select_id']."='".$value."'" );
				$r = $this->db->getNext();

				$str = '<span class="filter">'.$v['title'].'</span> = <span class="filter">'.$r[$this->get_field_name( $v['field'] )].'</span>';
			} // if
		} // foreach

		return( $str );
	} // get_var_text

	function get_filter_where( $bShowText = true, $bSearchFilter = true ) {
		$filter_where = '';
		if( !isset( $_SESSION['list_'.$this->current_list]['filter_text'] ) ) $_SESSION['list_'.$this->current_list]['filter_text'] = "";

		if( $bSearchFilter && $_SESSION['list_'.$this->current_list]['filter_text'] != "" ) {
			foreach( $this->field as $k => $v ) {
				if( ($v['type'] != "count") && ($v['type'] != "function") && ($v['type'] != "seperator") ) {
					if( $filter_where != "" ) $filter_where .= ' OR ';
					if( isset( $v['where_field'] ) && ($v['where_field'] != "") )
						$v['field'] = $v['where_field'];
					$filter_where .= "(".$v['field']." LIKE '%".$_SESSION['list_'.$this->current_list]['filter_text']."%')";
				} // if
			} // foreach

			$filter_where = "(".$filter_where.")";
		} // if

		if( $filter_where == "" ) $filter_where = '1';

		// Fixe Einschänkung
		if( isset( $_GET['indiv_delete_filter'] ) )
			unset( $_SESSION['list_'.$this->current_list]['param'] );

		if( isset( $_SESSION['list_'.$this->current_list]['param'] ) && is_array($_SESSION['list_'.$this->current_list]['param']) ) {
			$var = '';
			foreach( $_SESSION['list_'.$this->current_list]['param'] as $k => $v ) {
				$filter_where .= " AND (".$k."='".$v."')";
				$var .= $k;
			} // foreach

			if( $bShowText )
				echo '<div class="alert alert-info" role="alert">Eingeschränkte Sicht auf '.$this->get_var_text( $var, $v ).' (<a href="?indiv_delete_filter=1">auflösen</a>)</div>';

			// Verbindungsfeld unsichtbar machen
			foreach( $this->field as $k2 => $v2 ) {
				if( isset( $v2['save_field'] ) && ($v2['save_field'] == $this->get_field_name( $k )) )
					$this->field[$k2]['link_field'] = true;
			} // foreach
		} // if

		// Spezielle Filter
    $filter_where .= $this->list_project_functions->get_spezial_filter( $this->current_list );

		return( $filter_where );
	} // get_filter_where

	function get_query_string() {
		$str = '';
		if( isset( $_SESSION['list_'.$this->current_list]['param'] ) && is_array($_SESSION['list_'.$this->current_list]['param']) ) {
			$str = '?';
			foreach( $_SESSION['list_'.$this->current_list]['param'] as $k => $v ) {
				$str .= $k.'='.$v.'&';
			} // foreach
			$str = substr( $str, 0, -1 );
		} // if

		return( $str );
	} // if

	function set_fields( $arr ) {
		foreach( $arr['fields'] as $k => $v ) {
			$this->field[] = $v;
		} // foreach

		$this->list_query = $arr['list_query'];
		if( !isset( $_SESSION['list_'.$this->current_list]['sort_order'] ) )
			$_SESSION['list_'.$this->current_list]['sort_order'] = $arr['sort_order'];

		$this->delete_primary_key = $arr['delete_primary_key'];
		$this->delete_table = $arr['delete_table'];
		$this->file = $arr['file'];
		if( isset( $arr['lines_selectable'] ) )
			$this->lines_selectable = $arr['lines_selectable'];
		if( isset( $arr['switchable'] ) && ($arr['switchable'] == true) ) {
			if( !isset( $_SESSION['list_'.$this->current_list]['show'] ) )
				$_SESSION['list_'.$this->current_list]['show'] = "kurz";
		} // if
		if( isset( $arr['action_buttons'] ) && ($arr['action_buttons'] != array()) ) $this->action_buttons = $arr['action_buttons'];
		if( isset( $arr['action_buttons_main'] ) && ($arr['action_buttons_main'] != array()) ) $this->action_buttons_main = $arr['action_buttons_main'];

		if( !isset( $_SESSION['list_'.$this->current_list]['start'] ) )
			$_SESSION['list_'.$this->current_list]['start'] = 1;
		if( !isset( $_SESSION['list_'.$this->current_list]['page'] ) )
			$_SESSION['list_'.$this->current_list]['page'] = 1;

		if( !isset( $_SESSION['list_'.$this->current_list]['nur_laufende'] ) )
			$_SESSION['list_'.$this->current_list]['nur_laufende'] = true;

		$this->delete_allowed = $arr['delete_allowed'];
		$this->change_allowed = $arr['change_allowed'];
		$this->insert_allowed = $arr['insert_allowed'];

		// Info über Liste
		$this->list_project_functions->set_fields_special( $this->current_list, $this->field );
	} // set_fields

	function set_list( $list ) {
		global $_LISTEN;

		$this->current_list = $list;
		$this->set_fields( $_LISTEN[$list] );
	} // set_list

	function get_field_name( $v ) {
		$field = explode( ".", $v );
		$field = $field[sizeof( $field )-1];

		return( $field );
	} // get_field_name

	function print_browser( $filter ) {
		$anz = $this->db->query(
			$this->list_query."
			WHERE ".$filter );
		if( $anz <= $this->max_list )
			return;

		$this->f->paginator_print( $_SESSION['list_'.$this->current_list]['page'], $anz, $this->max_list, true, true );

		return;

		$prev_disabled = "";
		$prev_id = $_SESSION['list_'.$this->current_list]['start'] - $this->max_list;
		if( $prev_id < 1 ) $prev_disabled = ' disabled';

		$next_disabled = "";
		$next_id = $_SESSION['list_'.$this->current_list]['start'] + $this->max_list;
		if( $next_id > $anz ) $next_disabled = ' disabled';

		$max_reached = false;
		if( $anz > (20 * $this->max_list) ) {
			$anz = (20 * $this->max_list);
			$max_reached = true;
		} // if

		echo '
			<nav aria-label="Page navigation example">
			  <ul class="pagination flew-wrap" style="margin: 0;">
			    <li class="page-item'.$prev_disabled.'">
			      <a class="page-link" href="?indiv_start='.$prev_id.'" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>';
		for( $i=1; $i<=$anz; $i+=$this->max_list ) {
			$active = '';
			if( $i == $_SESSION['list_'.$this->current_list]['start'] )
				$active = ' active';

			echo '<li class="page-item'.$active.'"><a class="page-link" href="?indiv_start='.$i.'">'.(($i-1)/$this->max_list+1).'</a></li>';
		} // for
		if( $max_reached ) {
			echo '<li class="page-item disabled"><a class="page-link" href="">+++</a></li>';
		} // if

		echo '
			    <li class="page-item'.$next_disabled.'">
			      <a class="page-link" href="?indiv_start='.$next_id.'" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>';
	} // print_browser

	function print_list( $filter = '1' ) {
		echo '<div id="content_scroll">';

		if( $filter == "" ) $filter = '1';
		$filter = $this->get_filter_where()." AND ".$filter;

		$this->print_browser( $filter );

		echo '
			<form method="post" id="print_list" action="'.basename( $this->file ).'?indiv_list_id='.$this->current_list.'">
				<table id="table_excel_export" class="tablesorting list shadow table table-striped table-sm'.(($this->lines_selectable == 1)?' list_over':'').'">
					<thead class="article-row-head">
					<tr>';
		if( $this->lines_selectable == 1 )
			echo '<th class="sorter-false list_th_columns_choose">Auswahl</th>';
		$iCol = 0;
		$bSortPrinted = false;
		foreach( $this->field as $k => $v ) {
			$bShow = true;
			if( isset( $_SESSION['list_'.$this->current_list]['show'] ) && ($_SESSION['list_'.$this->current_list]['show'] == "kurz") && (!isset( $v['in_kurz'] ) || ($v['in_kurz'] == false)) )
				$bShow = false;
			if( (isset( $v['link_field'] ) && ($v['link_field'] != "")) || ($v['type'] == "seperator") || ($v['type'] == "textarea") )
				$bShow = false;
			if( $v['type'] == "select_function" )
				$bShow = false;
			if( $v['type'] == "image" )
				$bShow = false;
			if( !isset( $v['visible'] ) ) $v['visible'] = 1;
			if( $v['visible'] == 0 )
				$bShow = false;

			if( $bShow ) {
				$c = "";
				$v['title'] = $this->t->t( $v['title'] );

				if( ($v['type'] == "decimal") || ($v['type'] == "integer") || ($v['type'] == "datetime") || ($v['type'] == "date") || ($v['type'] == "time") ) $c .= " right";

				echo '<th class="'.$c.'">'.$v['title'].'</th>';

				$iCol++;
			} // if
		} // foreach

		if( $this->line_buttons_available() ) {
			echo '<th class="list_th_columns_actions sorter-false">'.$this->t->t( 'Aktion' ).'</th>';
			$iCol++;
		} // if
		echo '</tr></thead><tbody>';

		if( $this->db->query(
			$this->list_query."
			WHERE ".$filter."
			ORDER BY ".$_SESSION['list_'.$this->current_list]['sort_order']."
			LIMIT ".($_SESSION['list_'.$this->current_list]['start']-1).", ".$this->max_list ) ) {
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();

        // Insert Special Line
        $this->list_project_functions->insert_special_list_line( $this->current_list, $r );

				echo '<tr valign="top">';
				if( $this->lines_selectable == 1 )
					echo '<td class="center list_columns_choose"><input type="checkbox" value="1" name="id['.$r['id'].']" data-id="'.$r['id'].'"></td>';
				foreach( $this->field as $k => $v ) {
					$bShow = true;
					if( isset( $_SESSION['list_'.$this->current_list]['show'] ) && ($_SESSION['list_'.$this->current_list]['show'] == "kurz") && (!isset( $v['in_kurz'] ) || ($v['in_kurz'] == false)) )
						$bShow = false;
					if( (isset( $v['link_field'] ) && ($v['link_field'] != "")) || ($v['type'] == "seperator") || ($v['type'] == "textarea") )
						$bShow = false;
					if( $v['type'] == "select_function" )
						$bShow = false;
					if( $v['type'] == "image" )
						$bShow = false;
					if( !isset( $v['visible'] ) ) $v['visible'] = 1;
					if( $v['visible'] == 0 )
						$bShow = false;

					if( $bShow ) {
						if( isset( $v['field'] ) && ($v['field'] != "") )
							$field = $this->get_field_name( $v['field'] );

						$c = '';
						switch( $v['type'] ) {
							case "text": case "select": case "select_file": case "lookup":
									$output = $r[$field];
                  if( $v['type'] == "select" )
                    $output = $this->t->t( $output );

									// Farbe
									if( $this->current_list == LIST_CORE_PROJECTS_LISTS_FIELDS ) {
										if( $field == "color" ) {
											$output = '<span style="background-color: #'.$r[$v['select_id']].';">'.$output.'</span>';
										} // if
									} // if

									if( isset( $v['show_picture'] ) && ($v['show_picture'] == 1) && ($output != "") )
										$output = '<img src="'.DOMAIN.$r[$field].'" style="vertical-align: top; width: 16px; height: 16px;"> '.$r[$field];

									$output .= $this->list_project_functions->get_special_field_value( $this->current_list, $r, $field );
								break;
							case "select_multi":
									$output = $this->list_project_functions->get_select_multi_for_list( $this->current_list, $r[$this->delete_primary_key], $field );
								break;
							case "fa_class":
									$output = $r[$field];
									if( $output != "" )
										$output = '<i class="'.$output.'"></i> '.$output;
								break;
							case "password":
									$output = '******';
								break;
							case "decimal":
  								$output = $r[$field];
  								$output = number_format( $output, 2, ",", "." );//( ".", ",", $output );
  								$c = "right";
								break;
              case "integer":
  								$output = $r[$field];
  								$output = number_format( $output, 0, ",", "." );//( ".", ",", $output );
  								$c = "right";
                break;
							case "checkbox":
								$output = "";
									if( $r[$field]==1 ) {
										$output = '<span style="display:none">1</span><input name="'.$field.'" type="checkbox" checked onClick="return false;">';
									} // if
									$c = 'center';
								break;
							case "textarea":
									$output = $r[$field];
									$output = htmlspecialchars( $output);

									$output = '<div class="text_readmore" style="overflow: hidden">'.$output.'</div';
								break;
							case "textarea_query":
									$output =
//										'<a href="#" class="run_query" data-table="'.$this->delete_table.'" data-id="'.$r[$this->delete_primary_key].'"><img src="'.DOMAIN_PIC_DIR.'run_query.png" style="vertical-align: top; width: 16px; height: 16px;"></a>'.
									$r[$field];
								break;
							case "count":
									$this->db->query( $v['select_query']."'".$r[$this->delete_primary_key]."'", 2 );
									$r2 = $this->db->getNext( 2 );

									$output = '<span class="summe">'.$r2['anz'].'</span>';
									$c = "right";
								break;
							case "function":
									if( substr( $v['function'], 0, 17 ) == "show_field_images" ) {
										$for_field_id = substr( $v['function'], 18 );
										$output = $this->show_field_images( $r[$this->delete_primary_key], $for_field_id ); $c = "center";
									} else
										switch( $v['function'] ) {
											case "get_why_url": $output = $this->get_why_url( $r[$this->delete_primary_key] ); $c = "left"; break;
											case "get_email_queue_status": $output = $this->get_email_queue_status( $r[$this->delete_primary_key] ); $c = "right"; break;
											case "notiz_vorhanden": $output = $this->notiz_vorhanden( $r[$this->delete_primary_key] ); $c = "center"; break;
											case "single_document_preview": $output = $this->single_document_preview( $r[$this->delete_primary_key] ); $c = "center"; break;
											default:
													$output = $this->list_project_functions->get_special_functions( $this->current_list, $v['function'], $r[$this->delete_primary_key], $c );
													if( $output === null )
														$output = 'Funktion "'.$v['function'].'" nicht definiert';
												break;
										} // switch
								break;
							case "datetime":
									$output = date( "d.m.Y H:i", strtotime( $r[$field] ) );
									if( $output == "01.01.1970 00:00" ) $output = '---';
									if( $r[$field] == "0000-00-00 00:00:00" ) $output = '---';

									$c = "right";
								break;
							case "date":
									$output = date( "d.m.Y", strtotime( $r[$field] ) );
									if( $output == "01.01.1970" ) $output = '';

									$c = "right";
								break;
							case "time":
									$output = date( "H:i", strtotime( $r[$field] ) );

									$c = "right";
								break;
							default:
								$output = 'Ausgabeoption fehlt';
						} // switch

						if( isset( $_SESSION['list_'.$this->current_list]['filter_text'] ) && ($_SESSION['list_'.$this->current_list]['filter_text'] != "") && (stripos( $output, $_SESSION['list_'.$this->current_list]['filter_text'] ) !== false) ) {
							$strSource = substr( $output, stripos( $output, $_SESSION['list_'.$this->current_list]['filter_text'] ), strlen( $_SESSION['list_'.$this->current_list]['filter_text'] ) );
							$output = str_ireplace( $strSource, '<span class="text-danger">'.$strSource.'</span>', $output );
						} // if

						if( isset( $v['color'] ) && ($v['color'] != '') )
							$output = '<span style="color: #'.$v['color'].'; font-weight: bolder;">'.$output.'</span>';

						if( ($this->current_list == 3) && ($field == "teilnehmer_name") ) { // direkt Edit aktivieren
							$output = '<a class="change_other_list" href="#" data-id="'.$r['teilnehmer_id'].'" data-list_id="2"><i class="fas fa-pencil-alt"></i> '.$output.'</a>';
						} // if

						echo '<td class="'.$c.'">'.$output.'</td>';
					} // if
				} // foreach

				if( $this->line_buttons_available() ) {
					// Menü
					echo '<td valign="top" class="list_columns_actions">';

					$this->print_action_buttons( $r[$this->delete_primary_key] );
					$this->list_project_functions->add_special_line_buttons( $this->current_list, $r );

					if( $this->change_allowed ) {
						echo '<a class="change link_click_button" data-id="'.$r[$this->delete_primary_key].'">'.$this->f->get_button( 'ändern' ).'</a>';
					} // if
					if( $this->delete_allowed ) {
//						echo '<a class="link_click_button" href="#" onClick=\'if( confirm("'.$this->t->t( "Datensatz wirklich löschen?" ).'") ) delete_record($(this),'.$r[$this->delete_primary_key].'); else return( false );\'>'.$this->f->get_button( 'löschen' ).'</a>';
						echo '<a class="link_click_button" href="#" onClick=\'show_confirm( "'.$this->t->t( "Datensatz wirklich löschen?" ).'", function() { delete_record($(this),'.$r[$this->delete_primary_key].'); }, function() { return( false ); } );\'>'.$this->f->get_button( 'löschen' ).'</a>';
					} // if

					echo '</td>';
				} // if

				echo '</tr>';
			} // while
		} else { // Keine Daten
			echo '<tr><td colspan="'.$iCol.'">'.$this->t->t( "keine Daten" ).'</td></tr>';
		} // else

    // Insert Special Line
    if( isset( $r ) )
      $this->list_project_functions->insert_special_list_line( $this->current_list, $r, true );

		echo '</tbody></table></form>';

		$this->print_browser( $filter );

		echo '</div>';
	} // print_list

	function edit( $edit ) {
		// Edit
		if( $edit > 0 ) {
			$this->db->query( "SELECT * FROM ".$this->delete_table." WHERE ".$this->delete_primary_key."='".$edit."'", "edit" );
			$current = $this->db->getNext( "edit" );
		} // if

		// New + Edit
		if( $edit >= 0 ) {
			foreach( $this->field as $k => $v ) {
				if( ($v['type'] != "count") && ($v['type'] != "function") && ($v['type'] != "seperator") ) {
					$field = $this->get_field_name( $v['field'] );
					if( isset( $v['save_field'] ) && ($v['save_field'] != "") ) $field = $v['save_field'];

					if( !isset( $current[$field] ) ) $current[$field] = "";

					if( $edit == 0 ) {
						if( isset( $v['fill_fix_type'] ) ) {
							switch( $v['fill_fix_type'] ) {
								case "PARAMETER":
										if( isset( $_SESSION['list_'.$this->current_list]['param'][$v['fill_fix_param']] ) )
											$current[$field] = $_SESSION['list_'.$this->current_list]['param'][$v['fill_fix_param']];
										else
											$this->field[$k]['read_only'] = false;
									break;
								case "AUTOINC":
										$this->db->query( "SELECT MAX( ".$v['field']." ) AS max FROM ".$v['fill_fix_param']." WHERE ".$this->get_filter_where( false, false ), "edit" );
										$r = $this->db->getNext( "edit" );

										$current[$field] = $r['max']+10;
									break;
								case "AUTOTEXT": $current[$field] = $v['fill_fix_param']; break;
								case "SESSION_USER_ID": $current[$field] = $_SESSION['c_user_id']; break;
								case "AUTOINC_STRING_PREFIX":
									$strFilterWhere = $this->list_project_functions->get_special_select_where( $this->current_list, $field );
									if( $this->db->query( "SELECT ".$v['field']." AS max FROM ".$this->delete_table.$strFilterWhere." ORDER BY ".$this->delete_primary_key." DESC LIMIT 1", "edit" ) )
										$r = $this->db->getNext( "edit" );
									else
										$r['max'] = $v['fill_fix_param'].'-'.str_pad( 0, 6, "0", STR_PAD_LEFT );

									$r['max'] = substr( $r['max'], -6 );

									$current[$field] = $v['fill_fix_param'].'-'.str_pad( ($r['max']+1), 6, "0", STR_PAD_LEFT );
									break;
							} // switch
						} // if
					} // if
				} // if
			} // foreach

			$user_info = $this->f->load_user( $_SESSION['c_user_id'], "list_register" );
			$bTabmode = true; // $user_info['list_register'];

			$lines = array('','','','','','','','','','');
			$tabs = array();
			$current_tab = 0;

			foreach( $this->field as $k => $v ) {
			  if( !isset( $v['read_only'] ) ) $v['read_only'] = false;
			  if( !isset( $v['mandatory'] ) ) $v['mandatory'] = 0;
			  if( !isset( $v['visible'] ) ) $v['visible'] = 1;

			  $this->list_project_functions->change_field_values( $v );

				if( ($v['type'] != "count") && ($v['type'] != "function") && ($v['type'] != "lookup") ) {
					$field = $this->get_field_name( $v['field'] );

					if( $this->delete_primary_key == $field ) { // Primary Key nie editierbar
						$output = $current[$field];
						$pk = $current[$field];
						$v['read_only'] = 1;
					} //else

						switch( $v['type'] ) {
						case "text": case "fa_class":
								$style_indiv = '';
								if( $v['field'] == 'svn' ) $style_indiv = ' float-left';

								$output = '<input type="text" class="form-control form-control-sm'.$style_indiv.'" id="'.$field.'" name="'.$field.'" value="'.$current[$field].'" data-mandatory="'.$v['mandatory'].'" '.($v['read_only']?'readonly':'').'>';
							break;
						case "password":
								$output = '<input type="password" class="form-control form-control-sm" name="'.$field.'" value="" data-mandatory="'.$v['mandatory'].'" '.($v['read_only']?'readonly':'').'>';
							break;
						case "decimal": case "integer":
								$output = $current[$field];
								$output = str_replace( ".", ",", $output );
									// (($this->delete_primary_key==$field)?$field:'pk_'.$field)
								$output = '<input type="text" class="form-control form-control-sm" name="'.$field.'" value="'.$output.'" style="width:200px;" '.($v['read_only']?'readonly':'').'>';
							break;
						case "checkbox":
								$output = '<input id="'.$field.'" type="checkbox" name="'.$field.'" value="1"'.(($current[$field]==1)?' checked':'').' '.($v['read_only']?'onClick="return false;"':'').'>
									<label for="'.$field.'" class="css-label"></label>'; // class="css-checkbox"
							break;
						case "select": case "select_function":
								$op_value = '';
								$style = '';
								$output = '<select name="'.$field.'" data-placeholder="wählen..." class="chosen-select" style="width:200px" '.($v['read_only']?'disabled="true"':'').'><option value=""></option>';
								$this->db->query( $v['select_query'].$this->list_project_functions->get_special_select_where( $this->current_list, $field ) );
								while( $this->db->isNext() ) {
									$r = $this->db->getNext();

									// Farbe
									if( $this->current_list == LIST_CORE_PROJECTS_LISTS_FIELDS ) {
										if( $field == "color" ) {
											$style = 'style="background-color: #'.$r[$v['select_id']].';"';
										} // if
									} // if

									$sel = '';
									if( $r[$v['select_id']] == $current[$v['save_field']] )
										$sel = ' selected';
									$output .= '<option '.$style.'value="'.$r[$v['select_id']].'"'.$sel.'>'.$r[$field].'</option>';
								} // while
								$output .= '</select>';
							break;
						case "select_multi":
								$output = '<select name="'.$field.'" data-placeholder="wählen..." class="chosen-select" multiple style="width:200px" '.($v['read_only']?'disabled="true"':'').'><option value=""></option>';

								$this->db->query( $v['select_query'].$this->list_project_functions->get_special_select_where( $this->current_list, $field ) );
								while( $this->db->isNext() ) {
									$r = $this->db->getNext();

									$sel = $this->list_project_functions->check_multi_select_link_table( $this->current_list, $pk, $field, $r[$v['select_id']] );
									$output .= '<option '.$style.'value="'.$r[$v['select_id']].'" '.$sel.'>'.$r[$field].'</option>';
								} // while
								$output .= '</select>';
							break;
						case "textarea":
								$current[$field] = str_replace( "<br>", "<br />", $current[$field] );

								$output = '<textarea class="tiny" name="'.$field.'" rows="10">'.$current[$field].'</textarea>';
							break;
						case "textarea_query":
								$current[$field] = str_replace( "<br />", "<br>", $current[$field] );

								$output = '<textarea name="'.$field.'" rows="10" style="width: 100%;">'.$current[$field].'</textarea>';
							break;
						case "datetime":
							$value = date( "d.m.Y H:i", strtotime( $current[$field] ) );
							if( $value == "01.01.1970 00:00" ) $value = "";

							$output = '<input class="datetime_picker" type="text" name="'.$field.'" value="'.$value.'" style="width:200px;" '.($v['read_only']?'readonly':'').'>';
							break;
						case "date":
								$value = date( "d.m.Y", strtotime( $current[$field] ) );
								if( $value == "01.01.1970" ) $value = "";

								$output = '<input class="date_picker form-control form-control-sm" type="text" name="'.$field.'" value="'.$value.'" '.($v['read_only']?'readonly':'').'>';
							break;
						case "time":
								if( $current[$field] == "" ) $current[$field] = "00:00:00";

								$value = date( "H:i", strtotime( $current[$field] ) );
								$output = '<input class="time_picker form-control form-control-sm" type="text" name="'.$field.'" value="'.$value.'" style="width:100%;" '.($v['read_only']?'readonly':'').'>';
							break;
						case "select_file":
								$output = '<select name="'.$field.'" data-placeholder="wählen..." class="chosen-select" style="width:200px" '.($v['read_only']?'disabled="true"':'').'><option value=""></option>';
								foreach( glob( BASE_DIR.$v['path'].$v['pattern'] ) as $k2 => $v2 ) {
									$file = str_replace( BASE_DIR, "", $v2 );

									$style = "";
									if( isset( $v['show_picture'] ) && ($v['show_picture'] == 1) ) $style = 'style="background-image: url('.DOMAIN.$file.'); background-position: 0px 4px; background-repeat: no-repeat; background-size:16px 16px; padding-left: 20px;"';

									$bShow = true;
									if( (basename( $file ) == 'index.php') || (substr( basename( $file ), 0, 5) == 'core_') || (substr( basename( $file ), 0, 6) == 'login_') )
										$bShow = false;
									if( $bShow )
										$output .= '<option '.$style.'value="'.$file.'"'.(($file==$current[$field])?' selected':'').'>'.basename( $file ).'</option>';
								} // foreach
								$output .= '</select>';
							break;
						case "image":
								$output = '<div id="pictures_'.$v['field_id'].'">';

								// Zeige vorhandene Bilder
								$this->db->query( "
									SELECT id, fullpath, filename, extension
									FROM GLOBAL_DOKUMENTE AS d
									WHERE main_id='".$pk."' AND list_id='".$this->current_list."' AND field_id='".$v['field_id']."'", "edit" );
								while( $this->db->isNext( "edit" ) ) {
									$r = $this->db->getNext( "edit" );

									if( in_array( $r['extension'], array( "jpg", "jpeg", "bmp", "png", "gif" ) ) )
										$icon = '<img src="'.DOMAIN.$r['fullpath'].'" width="100" title="'.$r['filename'].'">';
									else {
										if( file_exists( PIC_DIR.'documents/'.$r['extension'].'.png' ) )
											$icon = DOMAIN_PIC_DIR.'documents/'.$r['extension'].'.png';
										else
											$icon = DOMAIN_PIC_DIR.'documents/unknown.png';

										$icon = '<img src="'.$icon.'" height="50" title="'.$r['filename'].'">';
									} // else

									$output .= '
										<input type="hidden" name="__image_current['.$r['id'].']" value="'.$r['fullpath'].'">
										<input type="hidden" id="__image_current_delete_'.$r['id'].'" name="__image_current_delete['.$r['id'].']" value="0">
										<div id="picture_'.$r['id'].'">
											<a href="'.DOMAIN.$r['fullpath'].'" target="_blank"><u>'.$r['filename'].'</u></a><br>
											<a href="#" onClick=\'delete_picture('.$r['id'].');\'>'.$icon.'</a>
										</div>';
								} // while
								$output .= "</div>";

								// Bildupload
								$output .= $this->fine_uploader_html( $v['field_id'] );
							break;
						default:
							$output = 'Eingabeoption fehlt';
					} // switch

					if( $v['visible'] == 1 ) {
						if( $v['type'] == "seperator" ) {
							if( $bTabmode ) {
								$current_tab++;
								$tabs[] = '<li class="nav-item"><a class="nav-link" href="#tab['.$current_tab.']">'.$v['title'].'</a></li>';
							} else
								$lines[$current_tab] .= '<tr><td colspan="2"><h2>'.$v['title'].'</h2></td></tr>';
						} else {
							// Eingabezeile

							if( $v['field'] == 'svn' ) {
								$output .= '<input type="button" id="button_svn" value="Geb. Datum" class="btn btn-primary moj-btn-sm" style="padding: .25rem .5rem;">';
							} // if
							if( ($this->current_list == 1) && ($v['field'] == 'kt_title') && ($pk == '') ) {
								$output .= '<input type="button" id="button_copy_from_template" value="von Vorlage kopieren" class="btn btn-primary moj-btn-sm" style="padding: .25rem .5rem;">';
							} // if

							// TecDoc: neuer Benutzer
							if( $this->current_list == 34 ) $v['title'] = $this->t->t( $v['title'] );

							// Basilica: Kurse
							$bDoLineBreak = true;
							if( ($this->current_list == 1) || ($this->current_list == 45) ) {
								if( ($field == 'mo') || ($field == 'di') || ($field == 'mi') || ($field == 'do') || ($field == 'fr') || ($field == 'sa') || ($field == 'so') ) {
									$bDoLineBreak = false;
									$lines[$current_tab] .= '<tr><th>'.$v['title'].'</th><td>'.$output.'</td>';
								} // if
								if( ($field == 'mo_startzeit') || ($field == 'di_startzeit') || ($field == 'mi_startzeit') || ($field == 'do_startzeit') || ($field == 'fr_startzeit') || ($field == 'sa_startzeit') || ($field == 'so_startzeit') ) {
									$bDoLineBreak = false;
									$lines[$current_tab] .= '<td style="width: 60px;">'.$output.'</td>';
								} // if
								if( ($field == 'mo_endzeit') || ($field == 'di_endzeit') || ($field == 'mi_endzeit') || ($field == 'do_endzeit') || ($field == 'fr_endzeit') || ($field == 'sa_endzeit') || ($field == 'so_endzeit') ) {
									$bDoLineBreak = false;
									$lines[$current_tab] .= '<td style="width: 60px;">'.$output.'</td><td style="width: 500px;"></td></tr>';
								} // if
							} // if

							if( $bDoLineBreak )
								$lines[$current_tab] .= '<tr><th>'.$v['title'].'</th><td>'.$output.'</td></tr>';
						} // else
					} // if
				} // if
			} // foreach

			$strOut = '
				<form method="post" action="#">
					<input type="hidden" name="'.$this->delete_primary_key.'" value="'.$current[$this->delete_primary_key].'">';

			if( $bTabmode ) {
				if( sizeof( $tabs ) == 0 ) {
					$tabs[] = '<li class="nav-item"><a class="nav-link" href="#tab[1]">'.$this->t->t( 'Allgemein' ).'</a></li>';
					$lines[1] = $lines[0];
					$current_tab = 1;
				} // if
				$strOut .= '<div id="tabs" class="popup-tabs" style="min-height: 500px;"><ul>'.implode($tabs).'</ul>';
				for( $i=1; $i<=$current_tab; $i++ ) {
					$strOut .= '<div id="tab['.$i.']" class="popup-tab accent-cell"><table class="list list_card table table-sm">'.$lines[$i].'</table></div>';
				} // for
				$strOut .= '</div>';
			} else
				$strOut .= '<div id="tabs_table"><table class="list list_card table table-sm">'.$lines[0].'</table></div>';

			$strOut .= '
				<div style="float:right;">
					<a class="link_click_button" onClick="print_edit_list();">'.$this->f->get_button( 'Drucken' ).'</a>
					<a id="save" class="link_click_button">'.$this->f->get_button( 'speichern' ).'</a>
				</div>
				</form>';
		} // if

		return( $strOut );
	} // edit

	function save() {
		if( !$this->db->query( "SELECT * FROM ".$this->delete_table." WHERE ".$this->delete_primary_key."='".$_POST[$this->delete_primary_key]."'" ) ) {
			if( $this->delete_table == "CORE_USER_INFO" ) {
				$this->db->delete( $this->delete_table, "email=''" );
			} // if
			$i = $this->db->insert( $this->delete_table, array( $this->delete_primary_key => '' ) );
			$this->db->commit();

			$old_values = array();
		} else {
			$i = $_POST[$this->delete_primary_key];

			$r = $this->db->getNext();
			$old_values = $r;
		} // else

		$arr_update = array();
		foreach( $this->field as $k => $v ) {
		  if( !isset( $v['read_only'] ) ) $v['read_only'] = false;

			if( ($v['type'] != "image") && ($v['type'] != "count") && ($v['type'] != "function") && ($v['type'] != "seperator") ) {
				$field = $this->get_field_name( $v['field'] );
				$field_save = $field;
				if( isset( $v['save_field'] ) && ($v['save_field'] != "") ) $field_save = $v['save_field'];

				$bFieldUpdate = true;
				switch( $v['type'] ) {
					case "password":
							if( $_POST[$field] != "" )
								$_POST[$field] = $this->f->pw->HashPassword( $_POST[$field] );
							else
								$bFieldUpdate = false;
						break;
					case "textarea": case "textarea_query":
							$_POST[$field] = str_replace( "\n", "<br>", $_POST[$field] );
							$_POST[$field] = str_replace( '\"', '"', $_POST[$field] );
						break;
					case "datetime":
							$_POST[$field] = date( "Y-m-d H:i:s", strtotime( $_POST[$field] ) );
						break;
					case "date":
							$_POST[$field] = date( "Y-m-d", strtotime( $_POST[$field] ) );
						break;
					case "decimal":
							$_POST[$field] = str_replace( ",", ".", $_POST[$field] );
						break;
					case "select_function":
							switch( $v['function'] ) {
								case "save_kurs_zuordnung": $this->save_kurs_zuordnung( $i, $_POST[$field] ); break;
							} // switch
						break;
					case "select_multi":
							$bFieldUpdate = false;
							$sel = $this->list_project_functions->write_multi_select_link_table( $this->current_list, $i, $field, $v );
						break;
				} // switch

				if( $bFieldUpdate && ($field != $this->delete_primary_key) && ($v['type'] != "select_function") && ($v['type'] != "lookup") ) {
					$arr_update[$field_save] = $_POST[$field];
				} // if
			} // if
		} // foreach

		$this->db->update( $this->delete_table, $arr_update, $this->delete_primary_key."='".$i."'" );
		$this->db->commit();
		$this->db->query( "SELECT * FROM ".$this->delete_table." WHERE ".$this->delete_primary_key."='".$i."'" );
		$r = $this->db->getNext();
		$new_values = $r;

		// Images
		foreach( $_POST as $k => $v ) {
			if( $k == "__image_current" ) {
				foreach( $v as $k2 => $v2 ) {
					if( $_POST['__image_current_delete'][$k2] == 1 ) {
						$this->f->delete_linked_document( $k2 );
					} // if
				} // foreach
			} // if

			if( $k == "__image_new" ) {
				foreach( $v as $field_id => $v3 ) {
					foreach( $v3 as $k2 => $v2 ) {
						$path = explode( '/', $v2 );

						if( $this->db->query( "SELECT filename FROM GLOBAL_DOKUMENTE WHERE filename='".$path[1]."'", "save" ) ) {
							// Datei existiert bereits unter diesem Namen
							$path[1] = $this->get_new_filename( $path[1] );
						} // if

						if( file_exists( BASE_DIR.'upload/'.$v2 ) ) { // Wenn false, dann Bild nicht mehr da
							rename( BASE_DIR.'upload/'.$v2, BASE_DIR.'upload/'.$path[1] );
							rmdir( BASE_DIR.'upload/'.$path[0] );

							$this->db->insert( "GLOBAL_DOKUMENTE", array(
								"list_id" => $this->current_list,
								"main_id" => $i,
								"field_id" => $field_id,
								"name" => $path[1],
								"path" => 'upload/',
								"filename" => $path[1],
								"fullpath" => 'upload/'.$path[1],
								"extension" => pathinfo( BASE_DIR.'upload/'.$path[1], PATHINFO_EXTENSION) ) );
							$this->db->commit();
						} // if
					} // foreach
				} // foreach
			} // if
		} // foreach

    $this->list_project_functions->special_save( $this->current_list, $i );
    $this->list_project_functions->write_changelog( $this->current_list, $i, $this->delete_table, $old_values, $new_values );
	} // save

	function get_new_filename( $str ) {
		$ext = substr( $str, strrpos( $str, '.' ) + 1 );
		$filename = substr( $str, 0, strrpos( $str, '.' ) );

		$bExit = false;
		$i = 0;
		while( !$bExit || ($i > 100) ) {
			$i++;

			$new_filename = $filename.'_'.$i.'.'.$ext;

			if( !$this->db->query( "SELECT filename FROM GLOBAL_DOKUMENTE WHERE filename='".$new_filename."'", "get_new_filename" ) )
				$bExit = true;
		} // while

		return( $new_filename );
	} // function

	function line_buttons_available() {
		if(
			(is_array( $this->action_buttons ) && (sizeof( $this->action_buttons ) > 0)) ||
			$this->change_allowed || $this->delete_allowed )
			return( true );
		return( false );
	} // line_buttons_available

	// INDIV
	function answer_copy_from_form( $to ) {
		// Dest
		$this->db->query( "SELECT fragebogen_id FROM WHY_FRAGEN WHERE frage_id='".$to."'" );
		$current = $this->db->getNext();

		$strOut = '<br />
			<form method="post" action="#">
				<input type="hidden" name="frage_id" value="'.$to.'">
				<table class="list">
					<tr><th>aktuelle Antworten löschen</th><td><input type="checkbox" value="1" name="over"></td></tr>
					<tr><th>Kopieren von</th><td><select name="from"><option value=""></option>';

		$this->db->query( "SELECT frage_id, kurz FROM WHY_FRAGEN WHERE fragebogen_id='".$current['fragebogen_id']."' AND frage_id!='".$to."'" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$strOut .= '<option value="'.$r['frage_id'].'">'.$r['kurz'].'</option>';
		} // while

		$strOut .= '
					</select></td></tr>
					</table>

					<a id="save" class="link_click_button_right">'.$this->f->get_button( 'kopieren' ).'</a>
				</form>';

		return( $strOut );
	} // answer_copy_from_form

	function answer_copy_from() {
		$source_frage_id = $_POST['from'];
		$dest_frage_id = $_POST['frage_id'];
		$overwrite = $_POST['over'];

		if( $overwrite == 1 )
			$this->db->delete( "WHY_ANTWORTEN", "frage_id='".$dest_frage_id."'" );
		$this->db->query( "
			INSERT INTO WHY_ANTWORTEN
				(
					SELECT '' AS antwort_id, ".$dest_frage_id." AS frage_id, antwort_titel, position, wert, antwort_text
					FROM WHY_ANTWORTEN WHERE frage_id='".$source_frage_id."'
				)" );
		$this->db->commit();
	} // answer_copy_from

	function answer_copy_to_form( $to ) {
		$strOut = '<br />
			<form method="post" action="#">
				<input type="hidden" name="frage_id" value="'.$to.'">
				<table class="list">
					<tr><th>alle anderen Antworten überschreiben</th><td><input type="checkbox" value="1" name="over"></td></tr>
				</table>

				<a id="save" class="link_click_button_right">'.$this->f->get_button( 'kopieren' ).'</a>
			</form>';

		return( $strOut );
	} // answer_copy_to_form

	function answer_copy_to() {
		$source_frage_id = $_POST['frage_id'];
		$overwrite = $_POST['over'];

		if( $overwrite == 1 ) {
			$this->db->query( "SELECT fragebogen_id, antworttyp_id, frage_id FROM WHY_FRAGEN WHERE frage_id='".$source_frage_id."'" );
			$source = $this->db->getNext();

			$this->db->query( "SELECT frage_id FROM WHY_FRAGEN WHERE fragebogen_id='".$source['fragebogen_id']."' AND frage_id!='".$source['frage_id']."'", 1 );
			while( $this->db->isNext( 1 ) ) {
				$dest = $this->db->getNext( 1 );

				$this->db->delete( "WHY_ANTWORTEN", "frage_id='".$dest['frage_id']."'" );
				$this->db->query( "
					INSERT INTO WHY_ANTWORTEN
					(
						SELECT '' AS antwort_id, ".$dest['frage_id']." AS frage_id, antwort_titel, position, wert, antwort_text
						FROM WHY_ANTWORTEN WHERE frage_id='".$source['frage_id']."'
					)" );
				$this->db->update( "WHY_FRAGEN", array( "antworttyp_id" => $source['antworttyp_id'] ), "frage_id='".$dest['frage_id']."'" );
				$this->db->commit();
			} // while
		} // if
	} // answer_copy_to

	function questions_copy_from_form( $to ) {
		global $c_user_id;

		$userinfo = $this->f->load_user( $c_user_id, "admin" );

		$filter = "1";
		if( $userinfo['admin'] == 0 )
			$filter = "d.user_id='".$c_user_id."'";

		// Dest
		$this->db->query( "SELECT fragebogen_id FROM WHY_FRAGEN WHERE frage_id='".$to."'" );
		$current = $this->db->getNext();

		$strOut = '<br />
			<form method="post" action="#">
				<input type="hidden" name="fragebogen_id" value="'.$to.'">
				<table class="list">
					<tr><th>aktuelle Fragen löschen</th><td><input type="checkbox" value="1" name="over"></td></tr>
					<tr><th>Kopieren von</th><td><select name="from"><option value=""></option>';

		$this->db->query( "
			SELECT f.fragebogen_id, f.titel
			FROM WHY_FRAGEBOGEN AS f
			LEFT JOIN WHY_DOMAINS AS d ON (d.fragebogen_id=f.fragebogen_id)
			WHERE f.fragebogen_id!='".$to."' AND ".$filter );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$strOut .= '<option value="'.$r['fragebogen_id'].'">'.$r['titel'].'</option>';
		} // while

		$strOut .= '
					</select></td></tr>
					</table>

					<a id="save" class="link_click_button_right">'.$this->f->get_button( 'kopieren' ).'</a>
				</form>';

		return( $strOut );
	} // questions_copy_from_form

	function questions_copy_from() {
		$source = $_POST['from'];
		$dest = $_POST['fragebogen_id'];
		$overwrite = $_POST['over'];

		if( $overwrite == 1 ) {
			$this->db->query( "SELECT frage_id FROM WHY_FRAGEN WHERE fragebogen_id='".$dest."'" );
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();

				$this->db->delete( "WHY_ANTWORTEN", "frage_id='".$r['frage_id']."'" );
			} // while
			$this->db->delete( "WHY_FRAGEN", "fragebogen_id='".$dest."'" );
		} // if

		$this->db->query( "SELECT * FROM WHY_FRAGEN WHERE fragebogen_id='".$source."'", 1 );
		while( $this->db->isNext( 1 ) ) {
			$r = $this->db->getNext( 1 );

			// Frage kopieren
			$dest_frage_id = $this->db->insert( "WHY_FRAGEN", array(
				"frage_id" => '',
				"fragebogen_id" => $dest,
				"position" => $r['position'],
				"frage" => $r['frage'],
				"antworttyp_id" => $r['antworttyp_id'],
				"kurz" => $r['kurz']
			) );

			// zugehörige Antworten kopieren
			$this->db->query( "
			INSERT INTO WHY_ANTWORTEN
				(
					SELECT '' AS antwort_id, ".$dest_frage_id." AS frage_id, antwort_titel, position, wert, antwort_text
					FROM WHY_ANTWORTEN WHERE frage_id='".$r['frage_id']."'
				)" );
		} // while
		$this->db->commit();
	} // questions_copy_from

	function print_action_buttons( $i ) {
		if( is_array( $this->action_buttons ) && (sizeof( $this->action_buttons ) > 0) ) {
			echo '
					<ul class="dropdown_menu dropdown_button" style="float: left;">
				<li><a>'.$this->f->get_button( 'Menü' ).'</a>
					<ul>';

			foreach( $this->action_buttons as $k => $v ) {
				$this->db->query( "SELECT button_id, description, picture FROM CORE_ACTION_BUTTONS WHERE title LIKE '".$v."'", "print_action_buttons" );
				$r = $this->db->getNext( "print_action_buttons" );

				switch( $v ) {
					// Core Verwaltung
					case BUTTON_HAUPTMENU: 				echo '<li><a href="/'.SUBDIR.'admin/core_list_redirect.php?m.project_id='.$i.'&indiv_list_id=-3"><i class="fas fa-home text-success fa-fw"></i>Hauptmenü</a></li>'; break;
					case BUTTON_LISTS: 						echo '<li><a href="/'.SUBDIR.'admin/core_list_redirect.php?l.project_id='.$i.'&indiv_list_id=-4"><i class="fas fa-list-ul text-primary fa-fw"></i>Listen verwalten</a></li>'; break;
					case BUTTON_ACTION_BUTTONS: 	echo '<li><a href="/'.SUBDIR.'admin/core_list_redirect.php?l.project_id='.$i.'&indiv_list_id=-5"><i class="fab fa-stack-exchange text-primary fa-fw"></i>Action Buttons</a></li>'; break;
					case BUTTON_BUTTONS_ZUORDNEN: echo '<li><a href="/'.SUBDIR.'admin/core_list_redirect.php?l.list_id='.$i.'&indiv_list_id=-6"><i class="fas fa-sign-in-alt text-primary fa-fw"></i>Buttons zuordnen</a></li>'; break;
					case BUTTON_FELDER: 					echo '<li><a href="/'.SUBDIR.'admin/core_list_redirect.php?l.list_id='.$i.'&indiv_list_id=-7"><i class="fas fa-list-ul text-primary fa-fw"></i>Felder</a></li>'; break;

/*
					case BUTTON_HAUPTMENU: 				echo '<li><a href="?indiv['.$v.']['.$i.']=1"><i class="fas fa-home text-success fa-fw"></i>Hauptmenü</a></li>'; break;
					case BUTTON_LISTS: 						echo '<li><a href="?indiv['.$v.']['.$i.']=1"><i class="fas fa-list-ul text-primary fa-fw"></i>Listen verwalten</a></li>'; break;
					case BUTTON_ACTION_BUTTONS: 	echo '<li><a href="?indiv['.$v.']['.$i.']=1"><i class="fab fa-stack-exchange text-primary fa-fw"></i>Action Buttons</a></li>'; break;

					case BUTTON_BUTTONS_ZUORDNEN: echo '<li><a href="?indiv['.$v.']['.$i.']=1"><i class="fas fa-sign-in-alt text-primary fa-fw"></i>Buttons zuordnen</a></li>'; break;
					case BUTTON_FELDER: 					echo '<li><a href="?indiv['.$v.']['.$i.']=1"><i class="fas fa-list-ul text-primary fa-fw"></i>Felder</a></li>'; break;
*/
/*
					case BUTTON_HAUPTMENU: 				echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?m.project_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_MAINMENU.'";</script>'; exit; break;
					case BUTTON_LISTS: 						echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.project_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_LISTS.'";</script>'; exit; break;
					case BUTTON_ACTION_BUTTONS: 	echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.project_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_ACTION_BUTTONS.'";</script>'; exit; break;
					case BUTTON_BUTTONS_ZUORDNEN:	echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.list_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_LISTS_BUTTONS.'";</script>'; exit; break;
					case BUTTON_FELDER: 					echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.list_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_LISTS_FIELDS.'";</script>'; exit; break;
*/


//					<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=19"><i class="fas fa-print text-success fa-fw"></i> Anmeldeformular</a></li>


					// Kurse
					/*
					case "KURS_TEILNEHMER_ANZEIGEN":	echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/user.png" alt="" />Teilnehmer anzeigen</a>'; break;
					case "KURS_AMS_MAIL_BEGINN":			echo '<li><a class="with_img_16 ams_mail_beginn" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/email.png" alt="" />AMS Mail Kursbeginn (Warteschlange)</a>'; break;
					case "KURS_AMS_MAIL_ENDE":				echo '<li><a class="with_img_16 ams_mail_ende" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/email.png" alt="" />AMS Mail Kursende (Warteschlange)</a>'; break;
					case "KURS_TRAINER_ZUORDNEN":			echo '<li><a class="with_img_16 kurs_trainer_zuordnen" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.$r['picture'].'" alt="" />'.$r['description'].'</a>'; break;

					case "KURS_ANMELDEFORMULAR_DRUCKEN": 				echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Anmeldeformulare</a>'; break;
					case "KURS_ERLAGSCHEIN_DRUCKEN":						echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Erlagscheine</a>'; break;
					case "KURS_ANWESENHEIT_DRUCKEN":						echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Anwesenheitsliste</a>'; break;
					case "KURS_PROT_KRAN_STAPLER":							echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />'.$r['description'].'</a>'; break;
					case "KURS_KURSBESTAETIGUNG_DRUCKEN": 			echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Kursbestätigung</a>'; break;
					case "KURS_ZAHLUNGSBESTAETIGUNG_DRUCKEN": 	echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Zahlungsbestätigung</a>'; break;
					case "KURS_AMS_KOSTENVORANSCHLAG_DRUCKEN":	echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />AMS Kostenvoranschlag</a>'; break;
					case "KURS_AMS_KURSBESTAETIGUNG_DRUCKEN":		echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />AMS Kurszeitbestätigung</a>'; break;
					case "KURS_AMS_RECHNUNG_DRUCKEN":						echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Rechnung</a>'; break;
					case "KURS_TEILNEHMERLISTE":								echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />Teilnehmerliste</a>'; break;
					case "KURS_TEILNEHMERLISTE_2":							echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/drucken.png" alt="" />'.$r['description'].'</a>'; break;
					case "KURS_WKW_ERSTELLEN":
							$this->db->query( "SELECT lehrabschluss FROM BAS_KURSE WHERE kurs_id='".$i."'", "link_kurs" );
							$r2 = $this->db->getNext( "link_kurs" );

							if( $r2['lehrabschluss'] == 1 )
								echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.$r['picture'].'" alt="" />'.$r['description'].'</a></li>';
						break;

					// Teilnehmer
					case "TN_ZU_KURS_ZUORDNEN": 	echo '<li><a class="with_img_16 zu_kurs_zuordnen" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/kurse_zuordnen.png" alt="" />zu Kurs zuordnen</a>'; break;
					case "TN_ZUORDNUNG_LOESCHEN":	echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/kurse_loeschen.png" alt="" />alle Kurszuordnungen löschen</a>'; break;

					// Fragebogen
					case "WHY_FB_FRAGEN": 							echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/fragen.png" alt="" />Fragen</a>'; break;
					case "WHY_FB_AUSWERTUNG": 					echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/auswertung.png" alt="" />Auswertungen</a>'; break;
					case "WHY_FB_STATISTIK": 						echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/statistik.png" alt="" />Statistik</a>'; break;
					case "WHY_FB_NUTZER": 							echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/nutzer.png" alt="" />Nutzer</a>'; break;
					case "WHY_FB_FRAGEN_KOPIEREN_VON": 	echo '<li><a class="with_img_16 questions_copy_from" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/copy.png" alt="" />Fragen kopieren von</a>'; break;
					case "WHY_FB_DRUCKEN_KURSE":
							$this->db->query( "SELECT link_kurs FROM WHY_FRAGEBOGEN WHERE fragebogen_id='".$i."'", "link_kurs" );
							$r2 = $this->db->getNext( "link_kurs" );

							if( $r2['link_kurs'] == 1 )
								echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.$r['picture'].'" alt="" />'.$r['description'].'</a></li>';
						break;

					// Fragen
					case "WHY_FR_ANTWORTEN":					echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/antworten.png" alt="" />Antworten</a>'; break;
					case "WHY_FR_ANT_KOPIEREN_VON":		echo '<li><a class="with_img_16 answer_copy_from" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/copy.png" alt="" />Antw. kopieren von</a>'; break;
					case "WHY_FR_ANT_KOPIEREN_NACH":	echo '<li><a class="with_img_16 answer_copy_to" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/copy.png" alt="" />Antw. kopieren nach</a>';	break;
					case "WHY_FR_ANT_KOPIEREN_NACH":	echo '<li><a class="with_img_16 answer_copy_to" href="#" data-id="'.$i.'"><img width="16" src="/'.SUBDIR.'pics/copy.png" alt="" />Antw. kopieren nach</a>';	break;
					// Nutzer
					case "WHY_NUTZER_ANSICHT": echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.'pics/ansicht.png" alt="" />Anzeige</a>'; break;
					default: //echo '<li><a href="?indiv['.$r['button_id'].']['.$i.']=1"><img width="16" src="/'.SUBDIR.$r['picture'].'" alt="" />'.$r['description'].'</a></li>'; break;
*/
				} // switch
			} // foreach

			echo '
					</ul></li>
				</li>';
			echo '</ul>';
		} // if
	} // print_action_buttons

	function parse_action_buttons() {
		if( is_array( $this->action_buttons ) ) {
			foreach( $this->action_buttons as $k => $v ) {
				$this->db->query( "SELECT button_id FROM CORE_ACTION_BUTTONS WHERE title LIKE '".$v."'", "print_action_buttons" );
				$r = $this->db->getNext( "print_action_buttons" );
				if( $r['button_id'] == 0 )
					$r['button_id'] = $v;

				if( isset( $_GET['indiv'][$r['button_id']] ) ) {
					$id = key( $_GET['indiv'][$r['button_id']] );

					$this->db->query( "
						SELECT file, file_list_id
						FROM CORE_LISTS_BUTTONS
						WHERE button_id='".$r['button_id']."' AND list_id='".$this->current_list."'", "print_action_buttons" );
					$r = $this->db->getNext( "print_action_buttons" );

					switch( $v ) {
						// Core Verwaltung
						case BUTTON_HAUPTMENU: 				echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?m.project_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_MAINMENU.'";</script>'; exit; break;
						case BUTTON_LISTS: 						echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.project_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_LISTS.'";</script>'; exit; break;
						case BUTTON_ACTION_BUTTONS: 	echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.project_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_ACTION_BUTTONS.'";</script>'; exit; break;
						case BUTTON_BUTTONS_ZUORDNEN:	echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.list_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_LISTS_BUTTONS.'";</script>'; exit; break;
						case BUTTON_FELDER: 					echo '<script>location.href="/'.SUBDIR.'admin/core_list_redirect.php?l.list_id='.$id.'&indiv_list_id='.LIST_CORE_PROJECTS_LISTS_FIELDS.'";</script>'; exit; break;
/*
						// Kurse
						case "KURS_TEILNEHMER_ANZEIGEN":						echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&t.kurs_id='.$id.'";</script>'; exit; break;

						case "KURS_ANMELDEFORMULAR_DRUCKEN":				echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=kurs&kurs_id='.$id.'&bericht_id_kurs='.$this->report->get_report_id( "REP_ANMELDEFORMULAR" ).'";</script>'; exit; break;
						case "KURS_ERLAGSCHEIN_DRUCKEN":						echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=kurs&kurs_id='.$id.'&bericht_id_kurs='.$this->report->get_report_id( "REP_ERLAGSCHEIN" ).'";</script>'; exit; break;
						case "KURS_ANWESENHEIT_DRUCKEN":						echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=kurs&kurs_id='.$id.'&bericht_id_kurs='.$this->report->get_report_id( "REP_ANWESENHEITSLISTE" ).'";</script>'; exit; break;
						case "KURS_PROT_KRAN_STAPLER":							echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=trainer_kurs&kurs_id='.$id.'&bericht_id_trainer_kurs='.$this->report->get_report_id( "REP_PROT_KRAN_STAPLER" ).'";</script>'; exit; break;
						case "KURS_KURSBESTAETIGUNG_DRUCKEN":				echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$id.'&bericht_id_teilnehmer_kurs='.$this->report->get_report_id( "REP_KURSBESTAETIGUNG" ).'";</script>'; exit; break;
						case "KURS_ZAHLUNGSBESTAETIGUNG_DRUCKEN":		echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$id.'&bericht_id_teilnehmer_kurs='.$this->report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG" ).'";</script>'; exit; break;
						case "KURS_AMS_KOSTENVORANSCHLAG_DRUCKEN":	echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$id.'&bericht_id_teilnehmer_kurs='.$this->report->get_report_id( "REP_AMS_KOSTENVORANSCHLAG" ).'";</script>'; exit; break;
						case "KURS_AMS_KURSBESTAETIGUNG_DRUCKEN":		echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$id.'&bericht_id_teilnehmer_kurs='.$this->report->get_report_id( "REP_AMS_KURSBESTAETIGUNG" ).'";</script>'; exit; break;
						case "KURS_AMS_RECHNUNG_DRUCKEN":						echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$id.'&bericht_id_teilnehmer_kurs='.$this->report->get_report_id( "REP_AMS_RECHNUNG" ).'";</script>'; exit; break;
						case "KURS_TEILNEHMERLISTE":								echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=kurs&kurs_id='.$id.'&bericht_id_kurs='.$this->report->get_report_id( "REP_TEILNEHMERLISTE" ).'";</script>'; exit; break;
						case "KURS_TEILNEHMERLISTE_2":							echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=kurs&kurs_id='.$id.'&bericht_id_kurs='.$this->report->get_report_id( "REP_TEILNEHMERLISTE_2" ).'";</script>'; exit; break;
						case "KURS_DRUCKEN_HONORAR":								echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=trainer_kurs&kurs_id='.$id.'&bericht_id_trainer_kurs='.$this->report->get_report_id( "REP_HONORAR" ).'";</script>'; exit; break;
						case "KURS_DRUCKEN_WERKVERTRAG":						echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=trainer_kurs&kurs_id='.$id.'&bericht_id_trainer_kurs='.$this->report->get_report_id( "REP_WERKVERTRAG" ).'";</script>'; exit; break;
						case "KURS_WKW_ERSTELLEN":									// Wird in list_redirect.php erstellt
							break;

						// Teilnehmer
						case "TN_ZUORDNUNG_LOESCHEN": $this->db->delete( "BAS_KURSTEILNEHMER", "status_id!='2' AND teilnehmer_id='".$id."'" ); $this->db->commit(); break;
						case "TN_KURSE_ANZEIGEN":			echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&t.teilnehmer_id='.$id.'";</script>'; exit; break;
*/
						// Fragebogen
						case "WHY_FB_FRAGEN": 				echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&f.fragebogen_id='.$id.'";</script>'; exit; break;
						case "WHY_FB_AUSWERTUNG":			echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&a.fragebogen_id='.$id.'";</script>'; exit; break;
						case "WHY_FB_STATISTIK":			echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&fragebogen_id='.$id.'";</script>'; exit; break;
						case "WHY_FB_NUTZER":					echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&n.fragebogen_id='.$id.'";</script>'; exit; break;
						case "WHY_FB_DRUCKEN_KURSE":	echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=fragebogen_kurs&fragebogen_id='.$id.'&bericht_id_fragebogen_kurs='.$this->report->get_report_id( "REP_FRAGEBOGEN" ).'";</script>'; exit; break;

						// Fragen
						case "WHY_FR_ANTWORTEN":	echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&a.frage_id='.$id.'";</script>'; exit; break;

						// Nutzer
						case "WHY_NUTZER_ANSICHT": echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&id='.$id.'";</script>'; exit; break;
/*
						// Trainer
						case "TR_WERKVERTRAG_DRUCK": 	echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=trainer_kurs&trainer_id='.$id.'&bericht_id_trainer_kurs='.$this->report->get_report_id( "REP_WERKVERTRAG" ).'";</script>'; exit; break;
						case "TR_HONORAR_DRUCK":			echo '<script>location.href="/'.SUBDIR.'admin/bas_drucken.php?type=trainer_kurs&trainer_id='.$id.'&bericht_id_trainer_kurs='.$this->report->get_report_id( "REP_HONORAR" ).'";</script>'; exit; break;
						case "TR_KURS_ZUORDNUNG":			echo '<script>location.href="/'.SUBDIR.$r['file'].'?indiv_list_id='.$r['file_list_id'].'&t.trainer_id='.$id.'";</script>'; exit; break;
*/
					} // switch
				} // if
			} // foreach
		} // if
	} // parse_action_buttons

	function get_why_url( $id ) {
		if( $this->db->query( "SELECT domain FROM WHY_DOMAINS WHERE fragebogen_id='".$id."'", 1 ) ) {
			$r = $this->db->getNext( 1 );

			$url = '<a href="'.DOMAIN.'?'.$r['domain'].'" target="_blank">'.DOMAIN.'?'.$r['domain'].'</a>';
		} else
			$url = "keine Domain";

		return( $url );
	} // get_why_url

	// Project
	function form_zu_kurs_zuordnen( $teilnehmer_id ) {
		$op_kurse = '<select name="kurs_id" data-placeholder="wählen..." class="chosen-select" style="width:400px"><option value="0"></option>';
		$this->db->query( "
			SELECT k.kurs_id, k.startdatum, k.enddatum, k.nummer
			FROM BAS_KURSE AS k
			WHERE k.enddatum+INTERVAL 30 DAY>='".$this->f->now()."'" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$op_kurse .= '<option value="'.$r['kurs_id'].'">'.$r['nummer'].' ('.date( "d.m.Y", strtotime( $r['startdatum'] ) ).' bis '.date( "d.m.Y", strtotime( $r['enddatum'] ) ).')</option>';
		} // while
		$op_kurse .= '</select>';

		$strOut = '<div>
			<form method="post" action="#">
				<input type="hidden" name="teilnehmer_id" value="'.$teilnehmer_id.'">
				<div id="tabs" class="popup-tabs" style="min-height: 500px;">

					<ul>
						<li class="nav-item"><a class="nav-link" href="#tab[1]">Zuordnung</a></li>
					</ul>

					<div id="tab[1]" class="popup-tab accent-cell" style="padding-left:1px;">
						<table class="list">
							<tr><th>Teilnehmer</th><td>'.$this->print->get_teilnehmer( $teilnehmer_id, false ).'</td></tr>
							<tr><th>Kurs</th><td>
								'.$op_kurse.'
							</td></tr>
						</table>
					</div>
				</div>

				<a id="save" class="link_click_button_right">'.$this->f->get_button( 'zuordnen' ).'</a>
			</form></div>';

		return( $strOut );
	} // form_zu_kurs_zuordnen

	function zu_kurs_zuordnen() {
		if( ($_POST['teilnehmer_id'] != 0) && ($_POST['kurs_id'] != "") &&
			(!$this->db->query( "SELECT kurs_id FROM BAS_KURSTEILNEHMER WHERE kurs_id='".$_POST['kurs_id']."' AND teilnehmer_id='".$_POST['teilnehmer_id']."'") ) ) {
			$this->db->insert( "BAS_KURSTEILNEHMER", array( "kurs_id" => $_POST['kurs_id'], "teilnehmer_id" => $_POST['teilnehmer_id'], "status_id" => 1 ) );
			$this->db->commit();
		} // if
	} // zu_kurs_zuordnen

	function form_trainer_zuordnen( $kurs_id ) {
		$output = array();
		$list = '';
		// Wochentage-Datum für Kurs
		$i = 0;
		$this->db->query( "SELECT startdatum, enddatum, startzeit, endzeit, mo, di, mi, do, fr, sa, so, stunden_pro_tag FROM BAS_KURSE WHERE kurs_id='".$kurs_id."'", "form_trainer_zuordnen" );
		$k = $this->db->getNext( "form_trainer_zuordnen" );

		$bOverflow = false;
		$currDate = 0;
		while( !$bOverflow && (strtotime( $currDate ) <= strtotime( $k['enddatum'])) ) {
			$i++;

			if( $currDate == 0 ) $currDate = $k['startdatum']; else $currDate = date( "Y-m-d", strtotime( "+1 day", strtotime( $currDate ) ) );

			$j = 0;
			$bShow = false;
			while( !$bShow && (strtotime( $currDate ) <= strtotime( $k['enddatum']) ) && !$bOverflow ) {
				$j++;
				if( $j > 100 ) $bOverflow = true;

				$weekday = date( "w", strtotime( $currDate ) );
				if( ($weekday == 0) && ($k['so'] == 1) ) $bShow = true;
				if( ($weekday == 1) && ($k['mo'] == 1) ) $bShow = true;
				if( ($weekday == 2) && ($k['di'] == 1) ) $bShow = true;
				if( ($weekday == 3) && ($k['mi'] == 1) ) $bShow = true;
				if( ($weekday == 4) && ($k['do'] == 1) ) $bShow = true;
				if( ($weekday == 5) && ($k['fr'] == 1) ) $bShow = true;
				if( ($weekday == 6) && ($k['sa'] == 1) ) $bShow = true;

				if( !$bShow )
					$currDate = $currDate = date( "Y-m-d", strtotime( "+1 day", strtotime( $currDate ) ) );
			} // while

			if( strtotime( $currDate ) <= strtotime( $k['enddatum']) ) {
				$tag = array( "Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam" );

				$strDate =
					$tag[date( "w", strtotime( $currDate ) )].' '.
					date( "d.m.Y", strtotime( $currDate ) );

				$op_current_trainer = '<option value="">&nbsp;</option>';
				$decManuelleStunden = 0;
				$decPauschale = 0;
				$this->db->query( "
					SELECT tr.trainer_id, tr.vorname, tr.nachname, tk.datum, tk.manuelle_stunden, tk.pauschale
					FROM BAS_TRAINER AS tr
					LEFT JOIN BAS_TRAINER_KURSE AS tk ON (tk.trainer_id=tr.trainer_id AND tk.kurs_id='".$kurs_id."' AND tk.datum='".date( "Y-m-d", strtotime( $currDate ) )."')
					ORDER BY tr.nachname, tr.vorname", "form_trainer_zuordnen2" );
				while( $this->db->isNext( "form_trainer_zuordnen2" ) ) {
					$r = $this->db->getNext( "form_trainer_zuordnen2" );

					$sel = '';
					if( $r['datum'] == date( "Y-m-d", strtotime( $currDate ) ) ) {
						$sel = ' selected';
						$decManuelleStunden = str_replace( ".", ",", $r['manuelle_stunden'] );
						$decPauschale = str_replace( ".", ",", $r['pauschale'] );
					} // if

					$op_current_trainer .= '<option value="'.$r['trainer_id'].'"'.$sel.'>'.$r['nachname'].' '.$r['vorname'].'</option>';
				} // while

				$output[strtotime( $currDate )] = '
					<select name="kal_trainer_id['.strtotime( $currDate ).']" data-placeholder="Trainer wählen..." class="chosen-select" style="width: 200px">
						<option value=""></option>
						'.$op_current_trainer.'
					</select>
				';

				if( $decManuelleStunden == '0,00' )
					$decManuelleStunden = '';
				if( $decPauschale == '0,00' )
					$decPauschale = '';
				$list .= '
					<tr><th>'.$strDate.'</th>
						<td>
							<select name="trainer_id['.strtotime( $currDate ).']" data-placeholder="Trainer wählen..." class="chosen-select" style="width: 300px">
								<option value=""></option>
								'.$op_current_trainer.'
							</select>
						</td>
						<td class="right">
							'.$k['stunden_pro_tag'].'
						</td>
						<td>
							<input type="text" value="'.$decManuelleStunden.'" name="manuelle_stunden['.strtotime( $currDate ).']">
						</td>
						<td>
							<input type="text" value="'.$decPauschale.'" name="pauschale['.strtotime( $currDate ).']">
						</td>
					</tr>';
			} // if
		} // while

		$strOut = '
			<form method="post" action="#">
				<input type="hidden" name="kurs_id" value="'.$kurs_id.'">
				<div id="tabs" class="popup-tabs" style="min-height: 500px;">
					<ul>
						<li class="nav-item"><a class="nav-link" href="#tab[1]">Liste</a></li>
						<li class="nav-item"><a class="nav-link" href="#tab[2]">Kalender</a></li>
					</ul>

					<div id="tab[1]" class="popup-tab accent-cell" style="padding-left:1px;">
						<table class="list">
							<tr><th>Tag</th><th>Trainer</th><th>Standard h</th><th>Manuelle h</th><th>Pauschale</th></tr>
							'.$list.'
						</table>
					</div>
					<div id="tab[2]" class="popup-tab accent-cell" style="padding-left:1px;">
						'.$this->kal->print_kalender( $k['startdatum'], $k['enddatum'], $output ).'
					</div>
				</div>

				<a id="save" class="link_click_button_right">'.$this->f->get_button( 'zuordnen' ).'</a>
			</form>';

		return( $strOut );
	} // form_trainer_zuordnen

	function trainer_zuordnen( $iCurrentTab ) {
		$kurs_id = $_POST['kurs_id'];

		// alte löschen
		$this->db->delete( "BAS_TRAINER_KURSE", "kurs_id='".$kurs_id."'" );

		switch( $iCurrentTab ) {
			case 0: // Liste
					foreach( $_POST['trainer_id'] as $k => $v ) {
						if( $v != "" )
							$this->db->insert( "BAS_TRAINER_KURSE",
								array(
									"trainer_id" => $v,
									"kurs_id" => $kurs_id,
									"datum" => date( "Y-m-d", $k ),
									"manuelle_stunden" => str_replace( ",", ".", $_POST['manuelle_stunden'][$k] ),
									"pauschale" => str_replace( ",", ".", $_POST['pauschale'][$k] )
								) );
					} // foreach
				break;
			case 1: // Kalender
					foreach( $_POST['kal_trainer_id'] as $k => $v ) {
						if( $v != "" )
							$this->db->insert( "BAS_TRAINER_KURSE", array( "trainer_id" => $v, "kurs_id" => $kurs_id, "datum" => date( "Y-m-d", $k ) ) );
					} // foreach
				break;
		} // switch

		$this->db->commit();
	} // trainer_zuordnen

	function save_kurs_zuordnung( $teilnehmer_id, $kurs_id ) {
		if( ($teilnehmer_id != 0) && ($kurs_id != "") &&
			(!$this->db->query( "SELECT kurs_id FROM BAS_KURSTEILNEHMER WHERE kurs_id='".$kurs_id."' AND teilnehmer_id='".$teilnehmer_id."'" )) ) {
			$this->db->insert( "BAS_KURSTEILNEHMER", array( "kurs_id" => $kurs_id, "teilnehmer_id" => $teilnehmer_id, "status_id" => 1 ) );
			$this->db->commit();
		} // if
	} // save_kurs_zuordnung

	function form_emails_senden() {
		$strOut = '
			E-Mails werden versendet<br />
			<input type="hidden" name="id" value="0">
			<table class="list">
				<tr><th>ID</th><td><span id="email_id"></span></td></tr>
				<tr><th>An</th><td><span id="email_an"></span></td></tr>
				<tr><th>Status</th><td><span id="email_status"></span></td></tr>
			</table>';

		return( $strOut );
	} // form_emails_senden

	function form_ams_mail( $kurs_id ) {
		$strOut = '<br />
			<form method="post" action="#">
				<input type="hidden" name="kurs_id" value="'.$kurs_id.'">
				<table class="list">
					<tr><th>Kurs</th><td>'.$this->print->get_kurs( $kurs_id, false ).'</td></tr>
					<tr><th><label for="value">E-Mails für Kurs erstellen</label></th><td><input id="value" type="checkbox" value="1" name="create_mail"></td></tr>
				</table>

				<a id="save" class="link_click_button_right">'.$this->f->get_button( 'E-Mails erstellen' ).'</a>
			</form>';

		return( $strOut );
	} // form_ams_mail


	function get_email_queue_status( $id ) {
		$output = '';
		$this->db->query( "
			SELECT s.title, e.error
			FROM BAS_EMAIL_QUEUE AS e
			LEFT JOIN GLOBAL_EMAIL_STATUS AS s ON (s.id=e.email_status)
			WHERE e.id='".$id."'", "get_email_queue_status" );
		$r = $this->db->getNext( "get_email_queue_status" );

		$output = $r['title'];
		if( $r['error'] != "" )
			$output .= ": ".$r['error'];

		return( $output );
	} // get_email_queue_status

	function notiz_vorhanden( $id ) {
		$output = '';

		$this->db->query( "SELECT notiz_text FROM BAS_TEILNEHMER WHERE teilnehmer_id='".$id."'", "notiz_vorhanden" );
		$r = $this->db->getNext( "notiz_vorhanden" );

		if( $r['notiz_text'] != '' ) {
			$name = "tmp_".$id;
			$output = '<span style="display:none">1</span><input id="'.$name.'" name="'.$name.'" type="checkbox" checked onClick="return false;">';
		} // if

		return( $output );
	} // notiz_vorhanden

	function single_document_preview( $id ) {
		$output = '';

		$this->db->query( "SELECT fullpath FROM GLOBAL_DOKUMENTE WHERE id='".$id."'", "single_document_preview" );
		$r = $this->db->getNext( "single_document_preview" );

		if( $r['fullpath'] != "" )
			$output = '<img src="'.DOMAIN.$r['fullpath'].'" width="200">';

		return( $output );
	} // single_document_preview

	function show_field_images( $id, $field_id ) {
		$output = '';

		$this->db->query( "
			SELECT fullpath, extension, filename
			FROM GLOBAL_DOKUMENTE
			WHERE list_id='".$this->current_list."' AND main_id='".$id."' AND field_id='".$field_id."'", "show_kunden_images" );
		while( $this->db->isNext( "show_kunden_images" ) ) {
			$r = $this->db->getNext( "show_kunden_images" );

			if( in_array( $r['extension'], array( "jpg", "jpeg", "bmp", "png", "gif" ) ) )
				$output .= '<img src="'.DOMAIN.$r['fullpath'].'" width="100" title="'.$r['filename'].'">';
			else {
				if( file_exists( PIC_DIR.'documents/'.$r['extension'].'.png' ) )
					$icon = DOMAIN_PIC_DIR.'documents/'.$r['extension'].'.png';
				else
					$icon = DOMAIN_PIC_DIR.'documents/unknown.png';

				$output .= '<img src="'.$icon.'" height="50" title="'.$r['filename'].'">';
			} // else
		} // while

		return( $output );
	} // show_kunden_images

	function fine_uploader_html( $field_id ) {
		$output = '
<script type="text/template" id="qq-template">
  <div class="qq-uploader-selector qq-uploader">
    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
      <span>Drag-and-Drop die Dateien hierher für den Upload</span>
    </div>
    <div class="qq-upload-button-selector qq-upload-button">
      <div>Dateien auswählen</div>
    </div>
    <span class="qq-drop-processing-selector qq-drop-processing">
      <span>Verarbeite Dateien...</span>
      <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
    </span>
    <ul class="qq-upload-list-selector qq-upload-list">
      <li>
        <div class="qq-progress-bar-container-selector">
          <div class="qq-progress-bar-selector qq-progress-bar"></div>
        </div>
        <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
        <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
        <span class="qq-edit-filename-icon-selector qq-edit-filename-icon"></span>
        <span class="qq-upload-file-selector qq-upload-file"></span>
        <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
        <span class="qq-upload-size-selector qq-upload-size"></span>
        <a class="qq-upload-cancel-selector qq-upload-cancel" href="#">Cancel</a>
        <a class="qq-upload-retry-selector qq-upload-retry" href="#">Retry</a>
        <a class="qq-upload-delete-selector qq-upload-delete" href="#">Delete</a>
        <span class="qq-upload-status-text-selector qq-upload-status-text"></span>
      </li>
    </ul>
  </div>
</script>

<div class="fine-uploader"></div>

<input type="hidden" class="field_id" value="'.$field_id.'">
		';

		return( $output );
	} // fine_uploader_html
} // list
?>