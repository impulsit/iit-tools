<?php
class logtime {
	static private $instance = null;
	private $current_time = 0;
	private $sum_duration = 0;
	private $arr_log = array();
	private $user_id = 0;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		if( isset( $_SESSION ) && isset( $_SESSION['c_user_id'] ) )
			$this->user_id = $_SESSION['c_user_id'];
	} // __construct

	function log_start() {
		$this->current_time = microtime( true );
	} // log_start

	function log_end( $bPrintAjax = false ) {
		$str = '<pre>';
		foreach( $this->arr_log as $k => $v ) {
			$v['percent'] = round( $v['duration'] / $this->sum_duration * 100, 2 );

			$str .=
				str_pad( $v['text'], 30, ' ', STR_PAD_RIGHT ).
				str_pad( $v['duration'], 10, ' ', STR_PAD_LEFT ).
				str_pad( $v['percent'], 10, ' ', STR_PAD_LEFT ).'%'.
				'<br>';
		} // foreach

		$str .= str_pad( round( $this->sum_duration, 2 ), 40, ' ', STR_PAD_LEFT );
		$str .= '</pre>';

		$bAjax = false;
		foreach( debug_backtrace() as $k => $v ) {
			if( strpos( $v['file'], '_ajax' ) )
				$bAjax = true;
		} // foreach

		if( $this->user_id == 1 ) {
			if( $bAjax && $bPrintAjax )
				$str = str_replace( '<br>', '\n', $str );
			if( !$bAjax || $bPrintAjax )
				echo $str;
		} // if
	} // log_end

	function log( $str ) {
		$duration = microtime( true ) - $this->current_time;
		$this->arr_log[] = array(
			"text" => $str,
			"duration" => round( $duration, 2 ),
			"percent" => 0
		);
		$this->sum_duration += $duration;

		$this->log_start();
	} // log

}
?>