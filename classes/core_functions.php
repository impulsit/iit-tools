<?php
require_once ('config_data.php');
require_once (CLASS_DIR . 'mysql.php');

class core_functions {
	private static $instance = null;
	private $db = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function __construct() {
		$this->db = mysql::getInstance();
	}

	public function init() {
	}
}

?>