<?php
require_once ('config_data.php');
require_once (CLASS_DIR . 'mysql.php');
require_once (CLASS_DIR . 'functions.php');

class tecdoc_web_api {
	private static $instance = null;
	private $db = null;
	private $f = null;
	public $Debug = false;
	private $td_setup = array();
	private $tm_endpoint = "";
	private $tm_endpoint_location = "";
	private $tm_no = "";
	private $tm_name = "";

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	// getInstance
	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();

		$this->db->query( "SELECT * FROM TEC_SETUP" );
		$this->td_setup = $this->db->getNext();

		if( isset( $_SESSION['c_user_id'] ) ) {
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin, sub_created_by_user_id' );
			if( $this->td_setup['sub_module_active'] && ($userinfo['sub_admin'] == 0) && ($userinfo['sub_created_by_user_id'] > 0) )
				$this->td_setup['sub_module_active'] = true;
			else
				$this->td_setup['sub_module_active'] = false;
		} // if
	}

	// __construct
	private function __clone() {
	}

	// __clone
	public function __destruct() {
	}

	// __destruct
	function PrintDebug( $str ) {
		if( !$this->Debug ) return;

		echo "[DEBUG] ";
		if( gettype( $str ) == "object" ) {
			echo "<pre>";
			print_r( $str );
			echo "</pre>";
		} else {
			echo $str . "<br>";
		} // else
	}

	// PrintDebug
	function get_fin_audatex_taskId( $strFin ) {
		$xml = '
			<ns1:createTaskRequest>
				<ns1:parameter xsltParameter="?" context="?">
					<ns1:name>loginId</ns1:name>
					<ns1:value>' . $this->td_setup['audatex_username'] . '</ns1:value>
				</ns1:parameter>
				<ns1:parameter xsltParameter="?" context="?">
					<ns1:name>password</ns1:name>
					<ns1:value>' . $this->td_setup['audatex_password'] . '</ns1:value>
				</ns1:parameter>
				<ns1:payload>
					<Task xmlns="http://www.audatex.com/SAXIF">
						<Versions>
							<Generator>AXN</Generator>
							<GeneratorVersion>16.08</GeneratorVersion>
							<SAXIFVersion>29.1</SAXIFVersion>
						</Versions>
						<ClaimNumber>User_' . $_SESSION['c_user_id'] . '</ClaimNumber>
						<BasicClaimData>
						<Vehicle>
							<VehicleIdentification>
								<VIN>' . $strFin . '</VIN>
							</VehicleIdentification>
						</Vehicle>
						</BasicClaimData>
					</Task>
				</ns1:payload>
			</ns1:createTaskRequest>';

		$client = new SoapClient( $this->td_setup['audatex_url'], array(
				"soap_version" => SOAP_1_2,
				"location" => $this->td_setup['audatex_location'],
				"trace" => true,
				"exceptions" => 1
		) );

		$soapBody = new SoapVar( $xml, XSD_ANYXML );

		try {
			$response = $client->__SoapCall( 'createTask', array(
					$soapBody
			) );
		} catch( Exception $e ) {
			echo "<pre>";
			echo "ERROR:<br>";
			print_r( $e );
			echo "<hr>";
			echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
			echo "/<pre>";
			exit();
		}

		$this->db->update( "CORE_USER_INFO", array(
				"last_task_id" => $response->message->taskId,
				"last_vin" => $strFin
		), "user_id='" . $_SESSION['c_user_id'] . "'" );
		$this->db->commit();

		return ($response->message->taskId);
	}

	// get_fin_audatex_taskId
	function get_fin_audatex_url( $taskId ) {
		$url = 'https://www.audanet.de/axnlogin/opentask.do?usebreworkpage=true&username=' . $this->td_setup['audatex_username'] . '&password=' . $this->td_setup['audatex_password'] . '&work=fastCalculationWorkflow&step=damage_capturing_minimal&task=' . $taskId;

		return ($url);
	}

	// get_fin_audatex_url
	function get_fin_audatex_gettask( $taskId ) {
		if( $taskId == '' ) {
			$this->db->query( "SELECT last_task_id FROM CORE_USER_INFO WHERE user_id='" . $_SESSION['c_user_id'] . "'" );
			$r = $this->db->getNext();

			$taskId = $r['last_task_id'];
		} // if

		$xml = '
			<ns1:getTaskRequest>
				<ns1:parameter xsltParameter="?" context="?">
					<ns1:name>loginId</ns1:name>
					<ns1:value>' . $this->td_setup['audatex_username'] . '</ns1:value>
				</ns1:parameter>
				<ns1:parameter xsltParameter="?" context="?">
					<ns1:name>password</ns1:name>
					<ns1:value>' . $this->td_setup['audatex_password'] . '</ns1:value>
				</ns1:parameter>
				<ns1:parameter>
					<ns1:name>taskId</ns1:name>
					<ns1:value>' . $taskId . '</ns1:value>
				</ns1:parameter>
				<ns1:parameter>
					<ns1:name>returnPayloadAsXML</ns1:name>
					<ns1:value>true</ns1:value>
				</ns1:parameter>
			</ns1:getTaskRequest>';

		$client = new SoapClient( $this->td_setup['audatex_url'], array(
				"soap_version" => SOAP_1_2,
				"location" => $this->td_setup['audatex_location'],
				"trace" => true,
				"exceptions" => 1
		) );

		$soapBody = new SoapVar( $xml, XSD_ANYXML );

		try {
			$response = $client->__SoapCall( 'getTask', array(
					$soapBody
			) );
		} catch( Exception $e ) {
			echo "<pre>";
			echo "ERROR:<br>";
			print_r( $e );
			echo "<hr>";
			echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
			echo "/<pre>";
			exit();
		}

		return ($response->payload);
	}

	// get_fin_audatex_gettask
	function get_fin( $strFin ) {
		$client = new SoapClient( $this->td_setup['fin_url'], array(
				"soap_version" => SOAP_1_2,
				"location" => $this->td_setup['fin_location'],
				"trace" => true,
				"exceptions" => 1
		) );

		// 1. Abfrage
		$arr = array(
				"CompanyName" => $this->td_setup['fin_company'],
				"UserName" => $this->td_setup['fin_username'],
				"Password" => $this->td_setup['fin_password'],
				"ModuleFilter" => 'None',
				"ShowCar" => true,
				"ShowTruck" => true,
				"ShowBike" => true,
				"CountryCode" => "DE",
				"LanguageCode" => "de",
				"AddInfoKeyFilter" => array(
						"AddInfoKeyFilterParameter" => array(
								"AddInfoKeyId" => -8,
								"AddInfoKeyFilterValue" => $strFin
						)
				)
		);

		try {
			$response = $client->GetVtMakeList( array(
					"input" => $arr
			) );
		} catch( Exception $e ) {
			return (array(
					"Error" => $e->getMessage()
			));

			echo "<pre>";
			echo "ERROR:<br>";
			print_r( $e );
			echo "<hr>";
			echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
			echo "/<pre>";
			exit();
		}

		$MakeId = $response->GetVtMakeListResult->VtMake->MakeId;
		$MakeName = $response->GetVtMakeListResult->VtMake->MakeName;

		// Umwandlen Make
		$arr = array(
				"CompanyName" => $this->td_setup['fin_company'],
				"UserName" => $this->td_setup['fin_username'],
				"Password" => $this->td_setup['fin_password'],
				"MakeId" => $MakeId
		);

		try {
			$response = $client->GetVtTcdMakeIdByAdcMakeId( array(
					"input" => $arr
			) );
		} catch( Exception $e ) {
			echo "<pre>";
			echo "ERROR:<br>";
			print_r( $e );
			echo "<hr>";
			echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
			echo "/<pre>";
			exit();
		}

		$TecDoc_MakeId = $response->GetVtTcdMakeIdByAdcMakeIdResult;

		$arrResult = array(
				"Error" => '',
				"MakeID" => $MakeId,
				"MakeName" => $MakeName,
				"TD_MakeID" => $TecDoc_MakeId
		);

		// 2. Abfrage
		$arr = array(
				"CompanyName" => $this->td_setup['fin_company'],
				"UserName" => $this->td_setup['fin_username'],
				"Password" => $this->td_setup['fin_password'],
				"ModuleFilter" => 'None',
				"ShowCar" => true,
				"ShowTruck" => true,
				"ShowBike" => true,
				"CountryCode" => "DE",
				"LanguageCode" => "de",
				"AddInfoKeyFilter" => array(
						"AddInfoKeyFilterParameter" => array(
								"AddInfoKeyId" => -8,
								"AddInfoKeyFilterValue" => $strFin
						)
				),
				"MakeId" => $MakeId
		);

		try {
			$response = $client->GetVtRangeList( array(
					"input" => $arr
			) );
		} catch( Exception $e ) {
			echo "<pre>";
			echo "ERROR:<br>";
			print_r( $e );
			echo "<hr>";
			echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
			echo "/<pre>";
			exit();
		}

		if( sizeof( $response->GetVtRangeListResult->VtRange ) == 1 ) {
			$response_range[0] = $response->GetVtRangeListResult->VtRange;
		} else
			$response_range = $response->GetVtRangeListResult->VtRange;

			$iRangeID = 0;
			foreach( $response_range as $k => $v ) {
				// 3. Abfrage
				$arr = array(
						"CompanyName" => $this->td_setup['fin_company'],
						"UserName" => $this->td_setup['fin_username'],
						"Password" => $this->td_setup['fin_password'],
						"ModuleFilter" => 'None',
						"ShowCar" => true,
						"ShowTruck" => true,
						"ShowBike" => true,
						"CountryCode" => "DE",
						"LanguageCode" => "de",
						"AddInfoKeyFilter" => array(
								"AddInfoKeyFilterParameter" => array(
										"AddInfoKeyId" => -8,
										"AddInfoKeyFilterValue" => $strFin
								)
						),
						"RangeId" => $v->RangeId
				);

				try {
					$response = $client->GetVtTypeList( array(
							"input" => $arr
					) );
				} catch( Exception $e ) {
					echo "<pre>";
					echo "ERROR:<br>";
					print_r( $e );
					echo "<hr>";
					echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
					echo "/<pre>";
					exit();
				}

				if( sizeof( $response->GetVtTypeListResult->VtType ) == 1 ) {
					$response_type[0] = $response->GetVtTypeListResult->VtType;
				} else
					$response_type = $response->GetVtTypeListResult->VtType;

					// Umwandlen Range
					$arr = array(
							"CompanyName" => $this->td_setup['fin_company'],
							"UserName" => $this->td_setup['fin_username'],
							"Password" => $this->td_setup['fin_password'],
							"RangeId" => $v->RangeId
					);

					try {
						$response = $client->GetVtTcdRangeIdByAdcRangeId( array(
								"input" => $arr
						) );
					} catch( Exception $e ) {
						echo "<pre>";
						echo "ERROR:<br>";
						print_r( $e );
						echo "<hr>";
						echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
						echo "/<pre>";
						exit();
					}

					$TecDoc_RangeId = $response->GetVtTcdRangeIdByAdcRangeIdResult;

					$iRangeID++;
					$arrResult['Range'][$iRangeID] = array(
							"RangeID" => $v->RangeId,
							"RangeName" => $v->RangeName,
							"TD_RangeID" => $TecDoc_RangeId
					);

					$iTypeID = 0;
					foreach( $response_type as $k2 => $v2 ) {
						// Umwandlen Type
						$arr = array(
								"CompanyName" => $this->td_setup['fin_company'],
								"UserName" => $this->td_setup['fin_username'],
								"Password" => $this->td_setup['fin_password'],
								"TypeId" => $v2->TypeId
						);

						try {
							$response = $client->GetVtTcdTypeAndClassByAdcTypeId( array(
									"input" => $arr
							) );
						} catch( Exception $e ) {
							echo "<pre>";
							echo "ERROR:<br>";
							print_r( $e );
							echo "<hr>";
							echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
							echo "/<pre>";
							exit();
						}

						$TecDoc_TypeId = $response->GetVtTcdTypeAndClassByAdcTypeIdResult->TcdTypeId;

						$strInfo = "";
						foreach( $v2->TypeDetails->VtTypeAdditionalDetail as $k_detail => $v_detail ) {
							if( $k_detail <= 3 ) {
								if( isset( $v_detail->AddInfoKeyName ) && isset( $v_detail->AddInfoKeyValue ) && ($v_detail->AddInfoKeyName != "") && ($v_detail->AddInfoKeyValue != "") ) {
									if( $strInfo != "" ) $strInfo .= ', ';
									$strInfo .= $v_detail->AddInfoKeyName . ': ' . $v_detail->AddInfoKeyValue;
								} // if
							} // if
						} // foreach

						$iTypeID++;
						$arrResult['Range'][$iRangeID]['Type'][$iTypeID] = array(
								"TypeID" => $v2->TypeId,
								"TypeName" => $v2->TypeName,
								"TD_TypeID" => $TecDoc_TypeId,
								"Info" => $strInfo
						);
					} // foreach
					$arrResult['Range'][$iRangeID]['TypeCount'] = $iTypeID;
			} // foreach
			$arrResult['RangeCount'] = $iRangeID;

			return ($arrResult);
	}

	// get_fin_tecrmi
	function get_mapping_no( $brand_id, $item_id ) {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) ) $location = SIMULATE_LOCATION;

		$this->db->query( "SELECT enable_webservice_mapping FROM TEC_SETUP WHERE id='1'", "get_mapping_no" );
		$setup = $this->db->getNext( "get_mapping_no" );
		if( !$setup['enable_webservice_mapping'] ) return ($item_id);

		$mapping_no = null;
		if( $this->db->query( "SELECT local_item_no FROM TEC_WEBSERVICE_MAPPING WHERE UPPER(item_no)='" . strtoupper( $item_id ) . "' AND UPPER(brand_id)='" . strtoupper( $brand_id ) . "'", "get_mapping_no" ) ) {
			$r = $this->db->getNext( "get_mapping_no" );

			$mapping_no = $r['local_item_no'];
		} // if

		if( $mapping_no == null ) $mapping_no = ""; // Wenn kein Mapping Artikelnr. muss leer sein; alt: $item_id;

		return ($mapping_no);
	} // get_mapping_no

	function get_price_by_text( $strWhich, $obPrice ) {
		if( !isset( $price->Text ) )
			return( 0 );

		switch( $strWhich ) {
			case "Brutto":
					$bFound = (strpos( $price->Text, "Brutto" ) !== false) || (strpos( $price->Text, "Maloprodajna cena sa PDV" ) !== false) || (strpos( $price->Text, "Redna cena brez DDV" ) !== false);
					if( $bFound )
						return( $price->Value );
				break;
			case "Netto":
					$bFound = (strpos( $price->Text, "Netto" ) !== false) || (strpos( $price->Text, "Veleprodajna cena sa popustom i PDV" ) !== false) || (strpos( $price->Text, "Vaša cena brez DDV" ) !== false);
					if( $bFound )
						return( $price->Value );
				break;
		}

		return( 0 );
	} // get_price_by_text

	function get_topmotive_api_list( $brand_id, $item_id, $arr_items = array() ) {
		global $_location;

		$this->db->query( "SELECT enable_topmotive_api FROM TEC_SETUP WHERE id='1'" );
		$setup = $this->db->getNext();
		if( !$setup['enable_topmotive_api'] ) return (array());

		$parUser = new User( $this->get_tm_no(), $this->get_tm_name() );
		$parItems = new Items();

		if( sizeof( $arr_items ) > 0 ) {
			foreach( $arr_items as $k => $v ) {
				$mapping_no = $this->get_mapping_no( $v['brand_id'], $v['item_id'] );

				$parItem = new Item( $v['brand_id'], $v['item_id'], 1, null, $mapping_no );
				$parItems->add_item( $parItem );
			} // foreach
		} else {
			$mapping_no = $this->get_mapping_no( $brand_id, $item_id );

			$parItem = new Item( $brand_id, $item_id, 1, null, $mapping_no );
			$parItems->add_item( $parItem );
		} // else

		$client = new SoapClient( $this->get_tm_endpoint(), array(
				"soap_version" => SOAP_1_2,
				"location" => $this->get_tm_endpoint_location(),
				"trace" => true,
				"exceptions" => 1
		) );

		$this->PrintDebug( "User: " . $this->get_tm_no() );
		if( isset( $arr_items[0] ) ) $this->PrintDebug( "Artikel: " . $arr_items[0]['item_id'] );
		if( isset( $arr_items[0] ) ) $this->PrintDebug( "Marke: " . $arr_items[0]['brand_id'] );
		$this->PrintDebug( "Mapping Artikel: " . $mapping_no );
		$this->PrintDebug( "URL: " . $this->get_tm_endpoint() );
		$this->PrintDebug( "Location: " . $this->get_tm_endpoint_location() );

		try {
			$response = $client->GetArticleInformation( array(
					"user" => $parUser,
					"items" => $parItems
			) );
		} catch( Exception $e ) {
			echo "<pre>";
			echo "ERROR:<br>";
			print_r( $e );
			echo "<hr>";
			echo "PARAMETERS:<br>";
			print_r( array(
					"user" => $parUser,
					"items" => $parItems
			) );
			echo "<hr>";
			echo "REQUEST:<br>" . htmlentities( $client->__getLastRequest() );
			echo "/<pre>";
			exit();
		}

		$this->PrintDebug( "REQUEST:<br>" . htmlentities( $client->__getLastRequest() ) );
		$this->PrintDebug( $response );

		$ret = array();
		if( (isset( $response->GetArticleInformationResult->ErrorId ) && ($response->GetArticleInformationResult->ErrorId == 3)) || // Nuic
				(isset( $response->GetArticleInformationResult->ErrorID ) && ($response->GetArticleInformationResult->ErrorID != 0)) ) // Euroauto
		{ // no Articles found
			$ret[] = array(
					"id" => 0,
					"brand_id" => $brand_id,
					"item_id" => $item_id,
					"text" => "",
					"quantity" => 0,
					"brutto" => 0,
					"netto" => 0,
					"default_brutto" => 0,
					"default_netto" => 0
			);
		} else {
			if( !isset( $response->GetArticleInformationResult ) ) trigger_error( "XML Response: 'GetArticleInformationResult' not found.<br><br>RESPONSE: ".$response."<br>", E_USER_ERROR );
			if( !isset( $response->GetArticleInformationResult->Items ) ) trigger_error( "XML Response: 'GetArticleInformationResult->Items' not found.<br><br>RESPONSE: ".$response."<br>", E_USER_ERROR );
			if( !isset( $response->GetArticleInformationResult->Items->Item ) ) trigger_error( "XML Response: 'GetArticleInformationResult->Items->Item' not found.<br><br>RESPONSE: ".$response."<br>", E_USER_ERROR );
			if( !isset( $response->GetArticleInformationResult->Items->Item->Item ) ) trigger_error( "XML Response: 'GetArticleInformationResult->Items->Item->Item' not found.<br><br>RESPONSE: ".$response."<br>", E_USER_ERROR );

			$result_items = $response->GetArticleInformationResult->Items->Item->Item;
			if( isset( $result_items->EinspNr ) || isset( $result_items->WholesalerArtNr ) ) // 1 Ergebnis
				$result_items_final[0] = $result_items;
				else
					$result_items_final = $result_items;
		} // else

		if( isset( $result_items_final ) ) {
			foreach( $result_items_final as $k2 => $v2 ) {
				if( (isset( $response->GetArticleInformationResult->ErrorId ) && ($response->GetArticleInformationResult->ErrorId == 0)) || // Nuic
						(isset( $response->GetArticleInformationResult->ErrorID ) && ($response->GetArticleInformationResult->ErrorID == 0)) ) { // Euroauto
							$quantity = array();
							if( $v2->AvailState == "nichtverfuegbar" ) {
								;
							} else {
								if( isset( $v2->Locations->Location ) ) $quantity = $v2->Locations->Location;
//								if( ($_location == "gmt_atit_at") && isset( $v2->Quantity->Quantity ) ) $quantity = $v2->Quantity->Quantity;
							} // else

							$price = array();
							if( isset( $v2->Prices->Price ) ) $price = $v2->Prices->Price;

							$decBrutto = 0;
							$decNetto = 0;
							if( (sizeof( $price ) == 1) ) {
								if( isset( $price->Text ) ) {
									if( (strpos( $price->Text, "Brutto" ) !== false) || (strpos( $price->Text, "Maloprodajna cena sa PDV" ) !== false) || (strpos( $price->Text, "Redna cena brez DDV" ) !== false) ) $decBrutto = $price->Value;
									if( (strpos( $price->Text, "Netto" ) !== false) || (strpos( $price->Text, "Veleprodajna cena sa popustom i PDV" ) !== false ) || (strpos( $price->Text, "Vaša cena brez DDV" ) !== false ) ) $decNetto = $price->Value;
								} // if
							} else {
								if( isset( $price ) ) {
									foreach( $price as $k => $v ) {
										if( isset( $v->Text ) ) {
											if( (strpos( $v->Text, "Brutto" ) !== false) || (strpos( $v->Text, "Maloprodajna cena sa PDV" ) !== false) || (strpos( $v->Text, "Redna cena brez DDV" ) !== false) ) $decBrutto = $v->Value;
											if( (strpos( $v->Text, "Netto" ) !== false) || (strpos( $v->Text, "Veleprodajna cena sa popustom i PDV" ) !== false) || (strpos( $v->Text, "Vaša cena brez DDV" ) !== false ) ) $decNetto = $v->Value;
										} // if
									} // foreach
								} // if
							} // else

							if( !isset( $quantity ) || ($quantity == array()) ) { // || !isset( $v2->Locations->Location )
								// Keine Lagerorte
								$value_quantity = 0;
								if( isset( $v2->AvailState ) && ($v2->AvailState == "verfuegbar") ) $value_quantity = 1;

								$ret[] = array(
										"id" => 0,
										"brand_id" => $v2->EinspNr,
										"item_id" => $v2->EinspArtNr,
										"text" => "",
										"quantity" => $value_quantity,
										"brutto" => $decBrutto,
										"netto" => $decNetto,
										"default_brutto" => $decBrutto,
										"default_netto" => $decNetto
								);
							} else {
								// Lagerorte gefunden
								if( sizeof( $quantity ) == 1 ) {
									$quantity = array();
									if( isset( $v2->Locations->Location ) ) $quantity[0] = $v2->Locations->Location;
//									if( ($_location == "gmt_atit_at") && isset( $v2->Quantity->Quantity ) ) $quantity[0] = $v2->Quantity->Quantity;
								} // if

								// Schleife über Lagerorte
								foreach( $quantity as $k => $v ) {
									if( !isset( $v->Text ) ) $v->Text = "...";
									if( !isset( $v->Id ) || ($v->Id == "") ) $v->Id = $k;

									// Menge
									$value_quantity = 0;
									if( isset( $v->Quantity->Quantity->Value ) ) $value_quantity = $v->Quantity->Quantity->Value;
//									if( ($_location == "gmt_atit_at") && isset( $v->Value ) ) $value_quantity = $v->Value;

									// Preise
									$decBruttoLocation = -1;
									$decNettoLocation = -1;
									if( isset( $v->Prices->Price ) ) {
										foreach( $v->Prices->Price as $k_price => $v_price ) {
											if( strpos( $v_price->Text, "Brutto" ) !== false ) $decBruttoLocation = $v_price->Value;
											if( strpos( $v_price->Text, "Netto" ) !== false ) $decNettoLocation = $v_price->Value;
										} // foreach
									} // if
									if( $decBruttoLocation == -1 ) $decBruttoLocation = $decBrutto;
									if( $decNettoLocation == -1 ) $decNettoLocation = $decNetto;

									$ret[] = array(
											"id" => $v->Id,
											"brand_id" => $v2->EinspNr,
											"item_id" => $v2->EinspArtNr,
											"text" => $v->Text,
											"quantity" => $value_quantity,
											"brutto" => $decBruttoLocation,
											"netto" => $decNettoLocation,
											"default_brutto" => $decBrutto,
											"default_netto" => $decNetto
									);
								} // foreach
							} // else
				} // if
			} // foreach
		} // if

		return ($ret);
	}

	// get_api_topmotive
	function get_topmotive_sum_infos( $brand_id, $item_id, &$quantity, &$price, &$brutto, $for_location_code = "xyz-1" ) {
		$quantity = 0;
		$price = 0;
		$brutto = 0;

		// Standard Abfrage
		if( isset( $_SESSION['tecdoc']['search_by_customerno'] ) ) { // Kundennummer bestimmen
			$userinfo['topmotive_kundennr'] = $_SESSION['tecdoc']['search_by_customerno'];
			$userinfo['name'] = $_SESSION['tecdoc']['search_by_customerno'];
		} else {
			$this->db->query( "SELECT topmotive_kundennr, name, sub_created_by_user_id FROM CORE_USER_INFO WHERE user_id='" . $_SESSION['c_user_id'] . "'" );
			$userinfo = $this->db->getNext();

			// Daten vom Sub Admin holen
			if( $userinfo['sub_created_by_user_id'] > 0 ) {
				$this->db->query( "SELECT topmotive_kundennr, name FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" );
				$r = $this->db->getNext();

				$userinfo['topmotive_kundennr'] = $r['topmotive_kundennr'];
				$userinfo['name'] = $r['name'];
			} // if
		} // else

		$this->set_tm_endpoint( $this->td_setup['tm_endpoint'], $this->td_setup['tm_endpoint_location'], $userinfo['topmotive_kundennr'], $userinfo['name'] );

		$ret = $this->get_topmotive_api_list( $brand_id, $item_id );

		$price = -1;
		$brutto = -1;
		foreach( $ret as $k => $v ) {
			$quantity = $quantity + $v['quantity'];
			if( $for_location_code == $v['id'] ) {
				$price = $v['netto'];
				$brutto = $v['brutto'];
			} // if
		} // foreach
		if( $price == -1 ) {
			$price = $v['default_netto'];
			$brutto = $v['default_brutto'];
		} // if
		$sub_shop = 0;

		// Sub Abfrage
		if( $this->td_setup['sub_module_active'] ) {
			// Endpoint von SubAdmin holen
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'topmotive_kundennr, name, sub_created_by_user_id' );
			$this->db->query( "SELECT sub_price_leader, sub_ws_link, sub_ws_location FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" );
			$r = $this->db->getNext();
			$this->set_tm_endpoint( $r['sub_ws_link'], $r['sub_ws_location'], $userinfo['topmotive_kundennr'], $userinfo['name'] );
			// Endpoint ---

			$ret = $this->get_topmotive_api_list( $brand_id, $item_id );

			$bPriceFound = false;
			$price2 = -1;
			$brutto2 = -1;
			foreach( $ret as $k => $v ) {
				$quantity = $quantity + $v['quantity'];
				if( $for_location_code == 'sub_'.$v['id'] ) {
					$price2 = $v['netto'];
					$brutto2 = $v['brutto'];
					$bPriceFound = true;
				} // if
			} // foreach
			if( $price2 == -1 ) {
				$price2 = $v['default_netto'];
				$brutto2 = $v['default_brutto'];
			} // if
			if( $r['sub_price_leader'] && $bPriceFound ) {
				$price = $price2;
				$brutto = $brutto2;
				$sub_shop = 1;
			} // if
		} // if

		if( ($quantity == 0) && ($price != 0) ) $quantity = 0.1;
	} // get_topmotive_sum_infos

	function get_topmotive_sum_infos_all( $arr_items ) {
		$arr = array();

		// Standard Abfrage
		if( isset( $_SESSION['tecdoc']['search_by_customerno'] ) ) { // Kundennummer bestimmen
			$userinfo['topmotive_kundennr'] = $_SESSION['tecdoc']['search_by_customerno'];
			$userinfo['name'] = $_SESSION['tecdoc']['search_by_customerno'];
		} else {
			$this->db->query( "SELECT topmotive_kundennr, name, sub_created_by_user_id FROM CORE_USER_INFO WHERE user_id='" . $_SESSION['c_user_id'] . "'" );
			$userinfo = $this->db->getNext();

			// Daten vom Sub Admin holen
			if( $userinfo['sub_created_by_user_id'] > 0 ) {
				$this->db->query( "SELECT topmotive_kundennr, name FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" );
				$r = $this->db->getNext();

				$userinfo['topmotive_kundennr'] = $r['topmotive_kundennr'];
				$userinfo['name'] = $r['name'];
			} // if
		} // else

		$this->set_tm_endpoint( $this->td_setup['tm_endpoint'], $this->td_setup['tm_endpoint_location'], $userinfo['topmotive_kundennr'], $userinfo['name'] );

		$ret = $this->get_topmotive_api_list( '', '', $arr_items );

		$postfix = '';
		if( $_SESSION['c_user_id'] <= 10 )
			$postfix = ' (M)';

		foreach( $ret as $k => $v ) {
			if( $v['brand_id'] != "" ) {
				if( !isset( $arr[$v['brand_id'] . "_" . $v['item_id']] ) ) $arr[$v['brand_id'] . "_" . $v['item_id']] = array(
						"auf_lager" => 0,
						"price_intern" => 0,
						"brutto" => 0,
						"locations" => array()
				);

				$arr[$v['brand_id'] . "_" . $v['item_id']]['auf_lager'] += $v['quantity'];
				$arr[$v['brand_id'] . "_" . $v['item_id']]['price_intern'] = $v['netto'];
				$arr[$v['brand_id'] . "_" . $v['item_id']]['brutto'] = $v['brutto'];
				if( $v['text'] != "" ) $arr[$v['brand_id'] . "_" . $v['item_id']]['locations'][] = array(
						"id" => $v['id'],
						"name" => $v['text'].$postfix,
						"quantity" => $v['quantity'],
						"brutto" => $v['brutto'],
						"netto" => $v['netto'],
						"sub_shop" => 0
				);
			} // if
		} // foreach

		// Sub Abfrage
		if( $this->td_setup['sub_module_active'] ) {
			$postfix = '';
			if( $_SESSION['c_user_id'] <= 10 )
				$postfix = ' (S)';

			// Endpoint von SubAdmin holen
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'topmotive_kundennr, name, sub_created_by_user_id' );
			$this->db->query( "SELECT sub_price_leader, sub_ws_link, sub_ws_location FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" );
			$r = $this->db->getNext();
			$this->set_tm_endpoint( $r['sub_ws_link'], $r['sub_ws_location'], $userinfo['topmotive_kundennr'], $userinfo['name'] );
			// Endpoint ---

			$ret = $this->get_topmotive_api_list( '', '', $arr_items );

			foreach( $ret as $k => $v ) {
				if( $v['brand_id'] != "" ) {
					if( !isset( $arr[$v['brand_id'] . "_" . $v['item_id']] ) ) $arr[$v['brand_id'] . "_" . $v['item_id']] = array(
							"auf_lager" => 0,
							"price_intern" => 0,
							"brutto" => 0,
							"locations" => array()
					);

					$arr[$v['brand_id'] . "_" . $v['item_id']]['auf_lager'] += $v['quantity'];
					if( ($r['sub_price_leader'] && ($v['netto'] > 0)) || ($arr[$v['brand_id'] . "_" . $v['item_id']]['price_intern'] == 0) ) {
						$arr[$v['brand_id'] . "_" . $v['item_id']]['price_intern'] = $v['netto'];
						$arr[$v['brand_id'] . "_" . $v['item_id']]['brutto'] = $v['brutto'];
					} // if
					if( $v['text'] != "" ) $arr[$v['brand_id'] . "_" . $v['item_id']]['locations'][] = array(
							"id" => 'sub_'.$v['id'],
							"name" => $v['text'].$postfix,
							"quantity" => $v['quantity'],
							"brutto" => $v['brutto'],
							"netto" => $v['netto'],
							"sub_shop" => 1
					);
				} // if
			} // foreach
		} // if

		// Alle restlichen Eintragen
		foreach( $arr_items as $k => $v ) {
			if( !isset( $arr[$v['brand_id'] . "_" . $v['item_id']] ) ) {
				$arr[$v['brand_id'] . "_" . $v['item_id']] = array(
						"auf_lager" => 0,
						"price_intern" => 0,
						"brutto" => 0,
						"locations" => array()
				);
			} else {
				if( ($arr[$v['brand_id'] . "_" . $v['item_id']]['auf_lager'] == 0) && ($arr[$v['brand_id'] . "_" . $v['item_id']]['price_intern'] != 0) ) $arr[$v['brand_id'] . "_" . $v['item_id']]['auf_lager'] = 0.1;
			} // else
		} // foreach

		return ($arr);
	} // get_topmotive_sum_infos_all

	function send_topmotive_order( $iOrderID, &$ws_result, &$ws_xml, $sub_shop ) {
		$ws_result = '';
		$ws_xml = '';

		if( $sub_shop == 0 ) {
			if( isset( $_SESSION['tecdoc']['search_by_customerno'] ) ) { // Kundennummer bestimmen
				$userinfo['topmotive_kundennr'] = $_SESSION['tecdoc']['search_by_customerno'];
				$userinfo['name'] = $_SESSION['tecdoc']['search_by_customerno'];
			} else {
				$this->db->query( "SELECT topmotive_kundennr, name, sub_created_by_user_id FROM CORE_USER_INFO WHERE user_id='" . $_SESSION['c_user_id'] . "'" );
				$userinfo = $this->db->getNext();

				// Daten vom Sub Admin holen
				if( $userinfo['sub_created_by_user_id'] > 0 ) {
					$this->db->query( "SELECT topmotive_kundennr, name FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" );
					$r = $this->db->getNext();

					$userinfo['topmotive_kundennr'] = $r['topmotive_kundennr'];
					$userinfo['name'] = $r['name'];
				} // if
			} // else
			$this->set_tm_endpoint( $this->td_setup['tm_endpoint'], $this->td_setup['tm_endpoint_location'], $userinfo['topmotive_kundennr'], $userinfo['name'] );
		} else {
			// Endpoint von SubAdmin holen
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'topmotive_kundennr, name, sub_created_by_user_id' );
			$this->db->query( "SELECT sub_price_leader, sub_ws_link, sub_ws_location FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" );
			$r = $this->db->getNext();
			$this->set_tm_endpoint( $r['sub_ws_link'], $r['sub_ws_location'], $userinfo['topmotive_kundennr'], $userinfo['name'] );
			// Endpoint ---
		} // else

		$response = '';
		$parUser = new User( $userinfo['topmotive_kundennr'], $userinfo['name'] );
		$parItems = new Items();

		$this->db->query( "SELECT * FROM CORE_USER_INFO WHERE user_id='" . $_SESSION['c_user_id'] . "'", "load_user" );
		$userinfo = $this->db->getNext( "load_user" );

		$parOrder = new Order( $userinfo['warenkorb_header_info'], $iOrderID, $userinfo );
		if( !$this->td_setup['delivery_method_active'] ) unset( $parOrder->DeliveryMethod );
		if( !$this->td_setup['document_type_active'] ) unset( $parOrder->DocumentType );

		$this->db->query( "
					SELECT article_no, brand_id, quantity, location_id
					FROM TEC_WARENKORB
					WHERE bestell_id='0' AND quantity>0 AND user_id='" . $_SESSION['c_user_id'] . "' AND sub_shop='".$sub_shop."'", "send_topmotive_order" );
		while( $this->db->isNext( "send_topmotive_order" ) ) {
			$r = $this->db->getNext( "send_topmotive_order" );

			$mapping_no = $this->get_mapping_no( $r['brand_id'], $r['article_no'] );
			$parItem = new Item( $r['brand_id'], $r['article_no'], $r['quantity'], $r['location_id'], $mapping_no );
			$parItems->add_item( $parItem );
		} // while

		$client = new SoapClient( $this->get_tm_endpoint(), array(
				"soap_version" => SOAP_1_2,
				"location" => $this->get_tm_endpoint_location(),
				"trace" => true,
				"exceptions" => 1
		) );

		try {
			$response = $client->SendOrder( array(
					"user" => $parUser,
					"order" => $parOrder,
					"items" => $parItems
			) );
		} catch( Exception $e ) {
			$response = new stdClass();
			$response->SendOrderResult = new stdClass();
			$response->SendOrderResult->ErrorMessage = $e->getMessage();
			$response->SendOrderResult->ErrorCode = 1;
			$response->SendOrderResult->ErrorId = 1;
		} // catch

		$ws_result = print_r( $response, true );
		$ws_xml = $client->__getLastRequest();

		return ($response);
	} // send_topmotive_order

	function get_tm_endpoint() {
		return( $this->tm_endpoint );
	}

	function get_tm_endpoint_location() {
		return( $this->tm_endpoint_location );
	}

	function set_tm_endpoint( $url, $location, $no, $name ) {
		$this->tm_endpoint = $url;
		$this->tm_endpoint_location = $location;
		$this->tm_no = $no;
		$this->tm_name = $name;
	}

	function get_tm_no() {
		return( $this->tm_no );
	}

	function get_tm_name() {
		return( $this->tm_name );
	}
}

// tecdoc_web_api
class User {
	private $CustomerId;
	private $PassWord;
	private $UserName;
	private $SID;

	public function __construct( $CustomerId, $UserName ) {
		$this->CustomerId = $CustomerId;
		$this->UserName = $UserName;
	}
}

class Item {
	private $Locations;
	private $Price;
	private $Suppliers;
	private $Customers;
	private $WholesalerArtNr;
	private $EinspNr;
	private $EinspArtNr;
	private $RequestedQuantity;
	private $Quantity;
	private $Memo;
	private $AlternativeItems;
	private $AvailState;

	public function __construct( $EinspNr, $EinspArtNr, $quantity = 1, $location_id = null, $WholesalerArtNr = null ) {
		$this->EinspNr = $EinspNr;
		$this->EinspArtNr = $EinspArtNr;
		$this->AvailState = "unbekannt";
		$this->RequestedQuantity = array(
				"Value" => $quantity
		);
		if( $WholesalerArtNr != null ) $this->WholesalerArtNr = $WholesalerArtNr;
		if( $location_id != null ) $this->Locations = array(
				"Location" => array(
						"Id" => $location_id,
						"Quantity" => array(
								"Quantity" => array(
										"Value" => $quantity
								)
						)
				)
		);
	} // Item
}

class Items {
	private $Item = array();

	public function __construct( $parItem = array() ) {
		if( $parItem != array() ) $this->Item[] = $parItem;
	}

	// Item
	function add_item( $parItem ) {
		$this->Item[] = $parItem;
	} // add_item
}

class Order {
	private $OrderMemo;
	private $OrderID;
	private $DeliveryAddress;
	public $DocumentType;
	public $DeliveryMethod;

	public function __construct( $strMemo, $iOrder_id, $userinfo ) {
		$this->OrderMemo = $strMemo." - (".$userinfo['name'].", ".$userinfo['sub_first_name']." ".$userinfo['sub_last_name'].", ".$userinfo['sub_adress'].", ".$userinfo['sub_phone'].", ".$userinfo['sub_email'].")";
		$this->OrderID = $iOrder_id;
		$this->DeliveryAddress = array(
				"FirstName" => $userinfo['sub_first_name'],
				"LastName" => $userinfo['sub_last_name'],
				"CompanyName" => $userinfo['name'],
				"Contact" => $userinfo['email'],
				"Street" => $userinfo['sub_adress'],
				"Phone" => $userinfo['sub_phone'],
				"EMail" => $userinfo['sub_email']
				// "ZipCode" => "",
				// "City" => "",
				// "Country" => "",
				// "Fax" => "",
		);

		// Document Type
		if( $userinfo['warenkorb_document_type'] != '' ) {
			$this->DocumentType = array(
				"Code" => $userinfo['warenkorb_document_type']
			);
		} // if
		// Delivery Method
		if( $userinfo['warenkorb_delivery_method'] != '' ) {
			$this->DeliveryMethod = array(
					"Code" => $userinfo['warenkorb_delivery_method'],
					"DateTime" => $userinfo['warenkorb_delivery_method_date'],
			);
		} // if
	} // Item
}
?>