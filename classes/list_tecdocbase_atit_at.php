<?php
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'translate.php' );
require_once( CLASS_DIR.'tecdoc_web_api.php' );
require_once( CLASS_DIR.'tecdoc.php' );
require_once( CLASS_DIR.'mail.php' );

class list_tecdocbase_atit_at {
	static private $instance = null;
	private $db = null;
	private $f = null;
	private $t = null;
	private $old_value = null;
	private $ws = null;
	private $stream = "";
	private $tec = null;
	private $mail = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
		$this->t = translate::getInstance();
		$this->ws = tecdoc_web_api::getInstance();
		$this->tec = tecdoc::getInstance();
		$this->mail = fb_mail::getInstance();
	} // __construct

	function print_menu_info( $list_id ) {
		switch( $list_id ) {
			case 31: // TecDoc - Warenkorb (User)
				$userinfo = $this->f->load_user( $_SESSION['c_user_id'], "warenkorb_header_info, warenkorb_document_type, warenkorb_delivery_method, warenkorb_delivery_method_date" );
				$setup = $this->f->load_setup( "TEC_SETUP" );

				$select_document_type = '';
				if( $setup['document_type_active'] == 1 ) {
					$options = '<option value=""></option>';
					$this->db->query( "SELECT * FROM TEC_DOCUMENT_TYPE WHERE active='1'" );
					while( $this->db->isNext() ) {
						$r = $this->db->getNext();

						if( $userinfo['warenkorb_document_type'] == $r['code_for_xml'] ) $sel = " selected"; else $sel = "";
						$options .= '<option value="'.$r['code_for_xml'].'"'.$sel.'>'.$this->t->t( $r['title'] ).'</option>';
					} // while

					$select_document_type = '
						<div class="col-md-4 sr">
							<select id="document_type" name="document_type" class="form-control form-control-sm chosen-select" id="document_type" data-placeholder="'.$this->t->t( "Belegart" ).'">
								'.$options.'
							</select>
						</div>';
				} // if

				$select_delivery_method = '';
				if( $setup['delivery_method_active'] == 1 ) {
					$bShowDateField = false;
					$options = '<option value=""></option>';
					$this->db->query( "SELECT * FROM TEC_DELIVERY_METHOD WHERE active='1'" );
					while( $this->db->isNext() ) {
						$r = $this->db->getNext();

						if( $userinfo['warenkorb_delivery_method'] == $r['code_for_xml'] ) $sel = " selected"; else $sel = "";
						if( ($sel == " selected") && ($r['date_mandatory'] == 1) )
							$bShowDateField = true;
							$options .= '<option value="'.$r['code_for_xml'].'" data-need_date="'.$r['date_mandatory'].'"'.$sel.'>'.$this->t->t( $r['title'] ).'</option>';
					} // while

					if( ($userinfo['warenkorb_delivery_method_date'] == "0000-00-00 00:00:00") || ($userinfo['warenkorb_delivery_method_date'] == "1970-01-01 01:00:00") )
						$userinfo['warenkorb_delivery_method_date'] = '';
					else
						$userinfo['warenkorb_delivery_method_date'] = date( "d.m.Y H:i", strtotime( $userinfo['warenkorb_delivery_method_date'] ) );

					$select_delivery_method = '
						<div class="col-md-4 sr">
							<select id="delivery_method" name="delivery_method" class="form-control form-control-sm chosen-select" id="document_type" data-placeholder="'.$this->t->t( "Lieferart" ).'" onChange="change_delivery_method();">
								'.$options.'
							</select>
							</div>
							<div class="col-md-4 sr">
								<input class="form-control form-control-sm datetime_picker" placeholder="'.$this->t->t( "Lieferzeit" ).'" type="text" value="'.$userinfo['warenkorb_delivery_method_date'].'" name="delivery_method_date" id="delivery_method_date"'.(($bShowDateField)?'':' style="display: none;"').'>
							</div>';
				} // if

				$order_options = '';
				if( ($select_document_type != '') || ($select_delivery_method != '') )
					$order_options = '<div class="row">'.$select_document_type.$select_delivery_method.'</div>';

				echo '
      			<form method="post" action="'.$_SERVER['SCRIPT_NAME'].'?indiv_project_id=4&indiv_list_id=31&indiv_save_header_info=1">
							<div class="order_info">
								<h2>'.$this->t->t( "Bestell-Information" ).'</h2>
								'.$order_options.'
								<textarea class="form-control form-control-sm sr" style="resize: none;" cols="80" rows="5" id="user_header_info" name="user_header_info">'.$userinfo['warenkorb_header_info'].'</textarea><br />
								<a href="#" onClick="$(this).closest(\'form\').submit();" style="float: right;">'.$this->f->get_button( 'speichern' ).'</a><br />
							</div>
						</form>';
				break;
			case 36:
				if( $this->db->query( "SELECT status FROM CORE_JOBS WHERE description='Check TD Items' AND finished='0' LIMIT 1" ) ) {
					$r = $this->db->getNext();

					echo '<div class="alert alert-info" role="alert">'.$r['status'].'</div>';
				} // if
				break;
			case 40: // TecDoc - Benutzer
				?>
				<form id="menu_info_block" method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>" enctype="multipart/form-data">
					<input type="hidden" name="MAX_FILE_SIZE" value="100000000">
					<input type="hidden" name="upload" value="1">
					<h2>Import</h2>
					<p>
					Benutzer Datei muss 4 Spalten haben: Login / E-Mail, Passwort, TMWS Kundennummer, Name<br />
					</p>
					<table class="list_left shadow" style="width: 700px;">
						<tr><th>Datei</th><td><input type="file" name="datei" value=""></td></tr>
						<tr><td colspan="2" class="right">
							<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><? $this->f->print_button( 'upload' ); ?></a>
							<a href="/<? echo SUBDIR; ?>themes/_default/examples/User_Import_Sample.xlsx" class="link_click_button_right"><?php  echo $this->f->get_button( 'Beispiel' ); ?></a>
						</td></tr>
					</table>
				</form>
				<br>
				<?php
				break;
		} // switch
	} // print_menu_info

	function get_spezial_filter( $list_id ) {
		switch( $list_id ) {
			case 31: // TecDoc - Warenkorb (User)
					return( " AND ((user_id='".$_SESSION['c_user_id']."') AND (bestell_id='0'))" );
				break;
			case 32: // TecDoc - Bestellungen (Admin)
					return( " AND (w.bestell_id!='0')" );
				break;
			case 33: // TecDoc - Bestellungen (User)
					return( " AND ((w.user_id='".$_SESSION['c_user_id']."') AND (w.bestell_id!='0'))" );
				break;
			case 34: // TecDoc - (Admin) Benutzer
					$this->db->query( "SELECT user_level FROM CORE_USER_INFO WHERE user_id='".$_SESSION['c_user_id']."'" );
					$userinfo = $this->db->getNext();

					$_WHERE = " AND (u.user_level<=100)";
					if( $this->tec->sub_module_active() && ($userinfo['user_level'] <= 50) ) {
						$_WHERE .= " AND (u.user_level<".$userinfo['user_level'].") AND (u.sub_created_by_user_id='".$_SESSION['c_user_id']."')";
					} // if

					return( $_WHERE );
				break;
		} // switch

		return( null );
	} // get_spezial_filter

	function insert_special_list_line( $list_id, $values, $bLastLine = false ) {
		switch( $list_id ) {
			case 31: // TecDoc - Warenkorb
				if( $bLastLine ) {
					$this->db->query( "
              SELECT SUM( amount ) AS amount, SUM( amount_brutto ) AS amount_brutto
              FROM TEC_WARENKORB
              WHERE user_id='".$_SESSION['c_user_id']."' AND bestell_id='0'", "insert_special_list_line" );
					$r = $this->db->getNext( "insert_special_list_line" );

					echo '
						<tbody class="tablesorter-no-sort">
              <tr style="background: #f0f2f5; border: none; font-weight: bold;">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="right">'.number_format( $r['amount'], 2, ",", "." ).'</td>
                <td></td>
                <td class="right">'.number_format( $r['amount_brutto'], 2, ",", "." ).'</td>
               </tr>
              <tr style="background: #FFFFFF; border: none;">
                <td colspan="7"></td>
              </tr>
						</tbody>';
				} // if
				break;

			case 33: case 32: // TecDoc - Bestellungen (User)
				$bFirstLine = false;
				if( $this->old_value == null ) {
					$this->old_value = $values['bestell_id'];
					$bFirstLine = true;
				} // if
				if( $bLastLine )
					$values['bestell_id'] = null;

					if( $values['bestell_id'] != $this->old_value ) {
						$this->db->query( "
              SELECT SUM( amount ) AS amount
              FROM TEC_WARENKORB WHERE bestell_id='".$this->old_value."'", "insert_special_list_line" );
						$r = $this->db->getNext( "insert_special_list_line" );

						echo '
							<tbody class="tablesorter-no-sort">
								<tr style="background: #f0f2f5; border: none; font-weight: bold;">
	                <td class="right">'.$this->old_value.'</td>
	                <td></td>
	                <td></td>
	                <td></td>
	                <td></td>
	                <td></td>
	                <td></td>
	                <td class="right">'.number_format( $r['amount'], 2, ",", "." ).'</td>
	              </tr>
	              <tr style="background: #FFFFFF; border: none;">
	                <td colspan="8"></td>
	              </tr>
							</tbody>';

					} // if

					if( ($list_id == 32) && !$bLastLine && ($bFirstLine || ($values['bestell_id'] != $this->old_value)) ) {
						$userinfo = $this->f->load_user( $values['user_id'], "name, info" );
						$this->db->query( "SELECT warenkorb_header_info, sub_shop, warenkorb_document_type, warenkorb_delivery_method, warenkorb_delivery_method_date FROM TEC_BESTELLUNG WHERE id='".$values['bestell_id']."'", "insert_special_list_line" );
						$b = $this->db->getNext( "insert_special_list_line" );

						$strHeader = '<strong>'.$userinfo['name'].'</strong>';
						if( $userinfo['info'] != "" ) $strHeader .= ' ('.$userinfo['info'].')';
						if( $b['sub_shop'] ) $strHeader .= ', '.$this->t->t( 'Sub Shop' ).': '.$this->t->t( 'ja' );
						if( $b['warenkorb_document_type'] ) $strHeader .= ', '.$this->t->t( "Belegart" ).': <strong>'.$b['warenkorb_document_type'].'</strong>';
						if( $b['warenkorb_delivery_method'] ) $strHeader .= ', '.$this->t->t( "Lieferart" ).': <strong>'.$b['warenkorb_delivery_method'].'</strong>';
						if( ($b['warenkorb_delivery_method_date'] != "0000-00-00 00:00:00") && ($b['warenkorb_delivery_method_date'] != "1970-01-01 01:00:00") ) $strHeader .= ', '.$this->t->t( "Lieferzeit" ).': <strong>'.date( "d.m.Y H:i", strtotime( $b['warenkorb_delivery_method_date'] ) ).'</strong>';

						if( $b['warenkorb_header_info'] != "" ) $strHeader .= '<hr>'.$b['warenkorb_header_info'];

						echo '
							<tbody class="tablesorter-no-sort">
	          		<tr>
	          			<td colspan="11">'.$strHeader.'</td>
	          			<td class="list_columns_actions">
					  				<a href="?indiv_set_all_ok='.$values['bestell_id'].'" class="link_click_button">'.$this->f->get_button( 'alles OK' ).'</a>
					  				<a href="?indiv_set_all_not_ok='.$values['bestell_id'].'" class="link_click_button">'.$this->f->get_button( 'alles abweisen' ).'</a>
					  				<a class="link_click_button" onClick="print_order('.$values['bestell_id'].')">'.$this->f->get_button( 'Drucken' ).'</a>
					  			</td>
	          		</tr>
							</tbody>';
					} // if

					$this->old_value = $values['bestell_id'];
					break;
		} // switch
	} // insert_special_list_line

	function special_save( $list_id, $primary_key ) {
		switch( $list_id ) {
			case 31: // TecDoc - Warenkorb
				$this->db->query( "SELECT article_price, article_price_brutto FROM TEC_WARENKORB WHERE id='".$_POST['id']."'" );
				$r = $this->db->getNext();

				$this->db->update( "TEC_WARENKORB", array(
						"amount" => $_POST['quantity'] * $r['article_price'],
						"amount_brutto" => $_POST['quantity'] * $r['article_price_brutto'],
				), "id='".$_POST['id']."'" );
				$this->db->query( "UPDATE TEC_WARENKORB set bestellte_menge=quantity WHERE id='".$_POST['id']."'" );
				$this->db->commit();
				break;
			case 32: // TecDoc - Bestellungen (Admin)
				$this->db->query( "SELECT article_price, article_price_brutto FROM TEC_WARENKORB WHERE id='".$_POST['id']."'" );
				$r = $this->db->getNext();

				$this->db->update( "TEC_WARENKORB", array(
						"amount" => $_POST['quantity'] * $r['article_price'],
						"amount_brutto" => $_POST['quantity'] * $r['article_price_brutto']
				), "id='".$_POST['id']."'" );
				$this->db->commit();
				break;
			case 34: // TecDoc - (Admin) Benutzer
				$this->db->query( "SELECT user_id, user_level, sub_created_by_user_id FROM CORE_USER_INFO WHERE user_id='".$primary_key."'" );
				$r = $this->db->getNext();

				if( $r['user_level'] <= 10 ) {
					$this->db->update( "CORE_USER_INFO", array(
						"list_register" => 1,
						"start_menu" => 4,
						"user_level" => 10
					), "user_id='".$primary_key."'" );
					$this->db->commit();
				} // if

				$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin' );
				if( $userinfo['sub_admin'] == 1 ) {
					if( $r['sub_created_by_user_id'] == 0 ) {
						$this->db->update( "CORE_USER_INFO", array(
								"sub_created_at" => $this->f->d( time() ),
								"sub_created_by_user_id" => $_SESSION['c_user_id']
						), "user_id='".$primary_key."'" );
						$this->db->commit();

						$this->mail->mail_new_user_subadmin( $r['user_id'] );
					} // if
				} // if

				break;
		} // switch
	} // special_save

	function add_special_line_buttons( $list_id, $values ) {
		switch( $list_id ) {
			case 32:
				echo '<a href="?indiv_set_ok='.$values['id'].'" class="link_click_button">'.$this->f->get_button( 'OK' ).'</a>';
				echo '<a href="?indiv_set_not_ok='.$values['id'].'" class="link_click_button">'.$this->f->get_button( 'abweisen' ).'</a>';
				break;
		} // switch
	} // add_special_line_buttons

	function add_special_action_buttons_main( $list_id ) {
		$strOut = null;

		switch( $list_id ) {
			case 31:
				$bShowButton = true;
				$setup = $this->f->load_setup( "TEC_SETUP" );

				if( $setup['enable_topmotive_api'] == 1 ) {
					$userinfo = $this->f->load_user( $_SESSION['c_user_id'], "topmotive_kundennr" );
					if( $userinfo['topmotive_kundennr'] == '' ) {
						$strOut = '<div class="text-danger">'.$this->t->t( 'Sie müssen eine Kundennummer in den Einstellungen hinterlegen.' ).'</div>';
						$bShowButton = false;
					} // if
				} // if

				if( $bShowButton )
					$strOut = '<a onClick="save_korb();" class="link_click_button">'.$this->f->get_button( 'bestellen' ).'</a>';
				break;
			case 29: case 30: case 35:
					$strOut = '<a href="'.$_SERVER['REQUEST_URI'].'&indiv_export_excel=1" class="link_click_button">'.$this->f->get_button( 'Exportieren' ).'</a>';
				break;
			case 36:
					$strOut  = '<a href="'.$_SERVER['REQUEST_URI'].'&indiv_export_excel=1" class="link_click_button">'.$this->f->get_button( 'Exportieren' ).'</a>';

					$setup = $this->f->load_setup( "TEC_SETUP" );
					if( $setup['item_check_active'] == 1 ) {
						$strOut .= '<a href="'.$_SERVER['REQUEST_URI'].'&indiv_start_check=1" class="link_click_button">'.$this->f->get_button( 'Prüfen' ).'</a>';
						$strOut .= '<a class="link_click_button" onClick=\'show_confirm( "'.$this->t->t( "Datensatz wirklich löschen?" ).'", function() { window.location.href="'.DOMAIN."admin/list_redirect.php?indiv_list_id=36&indiv_delete_all=1".'"; }, function() { return( false ); } );\'>'.$this->f->get_button( 'löschen' ).'</a>';
					} // if
				break;
			case 42:
					$strOut  = '<a href="#" onClick="show_file_uploader()">'.$this->f->get_button( 'upload' ).'</a>';

					require_once( JS_DIR."fine-uploader_v5.16.2/templates/tecdoc.html" );

					global $_location;

					$location = $_location;
					if( defined( "SIMULATE_LOCATION" ) )
						$location  = SIMULATE_LOCATION;

					$this->db->update( "CORE_LISTS_FIELDS", array( "path" => 'upload/'.$location.'/banner/' ), "list_id='42' AND pos='30'" );
					$this->db->commit();
				break;
		} // switch

		return( $strOut );
	} // add_special_action_buttons_main

	function parse_special_buttons( $list_id, $param) {
		$infotext = null;

		switch( $list_id ) {
			case 31:
				// Bestellung abschicken
				if( isset( $param['indiv_order'] ) ) {
					if( $this->db->query( "SELECT id FROM TEC_WARENKORB WHERE user_id='".$_SESSION['c_user_id']."' AND bestell_id='0' LIMIT 1" ) ) {
						$bAllOK = true;
						$strError = '';
						$ws_result = '';
						$ws_xml = '';

						// Sub Order Leader
						if( $this->tec->sub_module_active() ) {
							$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin, sub_created_by_user_id' );
							if( $userinfo['sub_admin'] == 1 ) $userinfo['sub_created_by_user_id'] = $_SESSION['c_user_id'];
							if( $this->db->query( "SELECT sub_order_leader FROM CORE_USER_INFO WHERE user_id='".$userinfo['sub_created_by_user_id']."'" ) ) {
								$r = $this->db->getNext();

								if( $r['sub_order_leader'] == 1 ) { // alles bekommt Sub Shop
									$this->db->query( "UPDATE TEC_WARENKORB SET sub_shop=1 WHERE user_id='".$_SESSION['c_user_id']."' AND bestell_id='0'" );
									$this->db->commit();
								} // if
							} // if
						} // if

						$bClearInfo = false;
						for( $i=0; $i<=1; $i++ ) {
							// Main Shop oder Sub Shop aktiv
							if( ($i == 0) || (($i == 1) && $this->tec->sub_module_active()) ) {
								// Zeilen da?
								$this->db->query( "SELECT COUNT( id ) AS anz FROM TEC_WARENKORB WHERE user_id='".$_SESSION['c_user_id']."' AND bestell_id='0' AND sub_shop='".$i."'" );
								$r = $this->db->getNext();
								if( $r['anz'] > 0 ) {
									// Immer neue Nr. vergeben
									$id = $this->db->insert( "TEC_BESTELLUNG", array( "user_id" => $_SESSION['c_user_id'] ) );
									$this->db->commit();

									// Per WS abschicken
									$this->db->query( "SELECT enable_topmotive_api FROM TEC_SETUP WHERE id='1'" );
									$setup = $this->db->getNext();

									if( $setup['enable_topmotive_api'] == 1 ) {
										$result = $this->ws->send_topmotive_order( $id, $ws_result, $ws_xml, $i );
										if( !isset( $result->SendOrderResult->ErrorCode ) || ($result->SendOrderResult->ErrorCode != 0) ) {
											$bAllOK = false;
											if( isset( $result->SendOrderResult->ErrorMessage ) )
												$strError = $result->SendOrderResult->ErrorMessage;
											else
												$strError = "unknown error";
										} // if
									} // if

									if( $bAllOK ) {
										$this->db->query( "SELECT name, email, warenkorb_header_info, warenkorb_document_type, warenkorb_delivery_method, warenkorb_delivery_method_date FROM CORE_USER_INFO WHERE user_id='".$_SESSION['c_user_id']."'", "load_user" );
										$userinfo = $this->db->getNext( "load_user" );

										$this->db->update( "TEC_BESTELLUNG",
											array(
												"user_id" => $_SESSION['c_user_id'],
												"bestellt_am" => $this->f->d( time() ),
												"warenkorb_header_info" => $userinfo['warenkorb_header_info'],
												"warenkorb_document_type" => $userinfo['warenkorb_document_type'],
												"warenkorb_delivery_method" => $userinfo['warenkorb_delivery_method'],
												"warenkorb_delivery_method_date" => $userinfo['warenkorb_delivery_method_date'],
												"ws_result" => $ws_result,
												"ws_xml" => $ws_xml,
												"sub_shop" => $i
											), "id='".$id."'"
										);
										$this->db->query( "UPDATE TEC_WARENKORB SET bestell_id='".$id."' WHERE user_id='".$_SESSION['c_user_id']."' AND bestell_id='0' AND sub_shop='".$i."'" );
										$this->db->commit();

										$bClearInfo = true;

										$infotext = '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> '.$this->t->t( "Bestellung wurde abgeschickt." ).'</div>';
									} else {
										$this->db->update( "TEC_BESTELLUNG",
											array(
												"user_id" => $_SESSION['c_user_id'],
												"ws_result" => $ws_result,
												"ws_xml" => $ws_xml
											), "id='".$id."'"
										);
										$this->db->commit();

										$infotext = '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> '.$this->t->t( "Bestellung konnte nicht verarbeitet werden" );
										if( $strError != '' )
											$infotext .= ': '.$strError;
										$infotext .= '</div>';

										$bClearInfo = false;
									} // else
								} // if
							} // if
						} // for
						if( $bClearInfo ) {
							$this->db->update( "CORE_USER_INFO",
								array(
									"warenkorb_header_info" => '',
									"warenkorb_document_type" => '',
									"warenkorb_delivery_method" => '',
									"warenkorb_delivery_method_date" => '0000-00-00 00:00:00'
							), "user_id='".$_SESSION['c_user_id']."'" );
							$this->db->commit();
						} // if
					} // if
				} // if

				// Info speichern
				if( isset( $param['indiv_save_header_info'] ) ) {
					if( !isset( $_POST['document_type'] ) ) $_POST['document_type'] = "";
					if( !isset( $_POST['delivery_method'] ) ) $_POST['delivery_method'] = "";
					if( !isset( $_POST['delivery_method_date'] ) ) $delivery_method_date = "0000-00-00 00:00:00"; else $delivery_method_date = date( "Y-m-d H:i:s", strtotime( $_POST['delivery_method_date'] ) );

					$this->db->update( "CORE_USER_INFO",
						array(
							"warenkorb_header_info" => $_POST['user_header_info'],
							"warenkorb_document_type" => $_POST['document_type'],
							"warenkorb_delivery_method" => $_POST['delivery_method'],
							"warenkorb_delivery_method_date" => $delivery_method_date,
						), "user_id='".$_SESSION['c_user_id']."'" );
					$this->db->commit();

					$infotext = '<div class="alert alert-success" role="alert"><span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> '.$this->t->t( "Text für Bestellung wurde gespeichert." ).'</div>';
				} // if
				break;
			case 32:
				if( isset( $param['indiv_set_ok'] ) ) {
					$this->db->query( "SELECT article_no, brand, quantity FROM TEC_WARENKORB WHERE id='".$param['indiv_set_ok']."' AND bestell_status_id<>'1'" );
					if( $r = $this->db->getNext() ) {
						$this->db->query( "UPDATE TEC_ITEMS set lagerstand=lagerstand-".$r['quantity']." WHERE UPPER(hersteller)='".strtoupper( $r['brand'] )."' AND UPPER(hersteller_nr)='".strtoupper( $r['article_no'] )."'" );
						$this->db->query( "UPDATE TEC_ITEMS set lagerstand=0 WHERE UPPER(hersteller)='".strtoupper( $r['brand'] )."' AND UPPER(hersteller_nr)='".strtoupper( $r['article_no'] )."' AND lagerstand<0" );
					} // if

					$this->db->update( "TEC_WARENKORB", array( "bestell_status_id" => 1 ), "id='".$param['indiv_set_ok']."'" );
					$this->db->commit();
				} // if
				if( isset( $param['indiv_set_not_ok'] ) ) {
					$this->db->update( "TEC_WARENKORB", array( "bestell_status_id" => 2 ), "id='".$param['indiv_set_not_ok']."'" );
					$this->db->commit();
				} // if
				if( isset( $param['indiv_set_all_ok'] ) ) {
					$this->db->query( "SELECT article_no, brand, quantity FROM TEC_WARENKORB WHERE bestell_id='".$param['indiv_set_all_ok']."' AND bestell_status_id<>'1'", 1 );
					while( $this->db->isNext( 1 ) ) {
						$r = $this->db->getNext( 1 );

						$this->db->query( "UPDATE TEC_ITEMS set lagerstand=lagerstand-".$r['quantity']." WHERE UPPER(hersteller)='".strtoupper( $r['brand'] )."' AND UPPER(hersteller_nr)='".strtoupper( $r['article_no'] )."'" );
						$this->db->query( "UPDATE TEC_ITEMS set lagerstand=0 WHERE UPPER(hersteller)='".strtoupper( $r['brand'] )."' AND UPPER(hersteller_nr)='".strtoupper( $r['article_no'] )."' AND lagerstand<0" );
					} // while

					$this->db->update( "TEC_WARENKORB", array( "bestell_status_id" => 1 ), "bestell_id='".$param['indiv_set_all_ok']."'" );
					$this->db->commit();
				} // if
				if( isset( $param['indiv_set_all_not_ok'] ) ) {
					$this->db->update( "TEC_WARENKORB", array( "bestell_status_id" => 2 ), "bestell_id='".$param['indiv_set_all_not_ok']."'" );
					$this->db->commit();
				} // if
				break;
			case 36:
					$setup = $this->f->load_setup( "TEC_SETUP" );
					if( $setup['item_check_active'] == 1 ) {
						if( isset( $param['indiv_start_check'] ) ) {
							if( !$this->db->query( "SELECT id FROM CORE_JOBS WHERE description='Check TD Items' AND finished='0'" ) ) {
								$this->db->insert( "CORE_JOBS", array( "description" => "Check TD Items", "start" => 1, "status" => 'starting job...' ) );
								$this->db->commit();
							} // if
						} // if
						if( isset( $param['indiv_delete_all'] ) ) {
								$this->db->query( "TRUNCATE TEC_WEBSERVICE_MAPPING" );
								$this->db->commit();
						} // if
					} // if
				break;
			case 40: // TecDoc - Benutzer
				ini_set('max_execution_time', 300);

				if( isset( $_POST['upload'] ) ) {
					$dest_filename = $this->f->clean_filename( $_FILES['datei']['name'] );

					if( $dest_filename != "" ) {
						if( !is_dir( BASE_DIR.'upload' ) ) mkdir( BASE_DIR.'upload', 0755 );

						move_uploaded_file( $_FILES['datei']['tmp_name'], BASE_DIR.'upload/'.$dest_filename );

						$objPHPExcel = PHPExcel_IOFactory::load( BASE_DIR.'upload/'.$dest_filename );

						$sheet = $objPHPExcel->getSheet(0);
						$highestRow = $sheet->getHighestRow();
						$highestColumn = $sheet->getHighestColumn();

						for( $row=2; $row<=$highestRow; $row++ ) {
							$rowData = $sheet->rangeToArray('A'.$row.':'.'D'.$row,
									NULL,
									TRUE,
									FALSE);
							if( $rowData[0][0] != "" ) {
								$this->db->delete( "CORE_USER_INFO", "email='".$rowData[0][0]."'" );
								$this->db->insert( "CORE_USER_INFO", array(
										"email" => $rowData[0][0],
										"password" => $this->f->pw->HashPassword( $rowData[0][1] ),
										"name" =>  $rowData[0][3],
										"list_register" => 1,
										"start_menu" => 4,
										"user_level" => 10,
										"topmotive_kundennr" => $rowData[0][2],
										"valid_to" => '1970-01-01',
								) );
								$this->db->commit();
							} // if
						} // for
					} // if
				} // if
				break;
		} // switch

		return( $infotext );
	} // parse_special_line_buttons

	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$output = null;
		$c = "left vertical-middle";

		switch( $list_id ) {
			case 32: case 33:
				if( $function == "tec_get_order_status" ) {
					$this->db->query( "
  						SELECT gbs.id, gbs.title
  						FROM TEC_WARENKORB AS tw
  						LEFT JOIN GLOBAL_BESTELL_STATUS AS gbs ON (gbs.id=tw.bestell_status_id)
  						WHERE tw.id='".$primary_key."'", "get_special_functions" );
					$r = $this->db->getNext( "get_special_functions" );

					switch( $r['id'] ) {
						case 0: $color = "DarkGreen"; $class = 'list-group-item-success'; break;
						case 1: $color = "blue"; $class = 'list-group-item-info'; break;
						case 2: $color = "DarkRed"; $class = 'list-group-item-danger'; break;
					} // switch

					$output = '<div style="color: '.$color.'; font-weight: bold;">'.$this->t->t( $r['title'] ).'</div>';

					$output = '<div class="list-group-item '.$class.'" style="padding: 2px 15px;">'.$this->t->t( $r['title'] ).'</div>';
				} // if
				break;
		} // switch

		return( $output );
	} // get_special_functions

	function get_special_field_value( $list_id, $values, $field ) {
		$output = "";

		switch( $list_id ) {
			case 31: // TecDoc - Warenkorb
				$setup = $this->f->load_setup( "TEC_SETUP" );

				// $values['location_name'] = filter_var($values['location_name'], FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);

				switch( $field ) {
					case "article_name":
							$values['location_name'] = trim( $values['location_name'], ' ' . chr(194) . chr(160) );

							if( ($setup['enable_topmotive_api'] == 1) && ($values['location_name'] != "") )
								$output .= ' ('.trim( $values['location_name'] ).')';

							$str = $this->tec->get_internal_item_no( $values['article_no'], $values['article_no'], $values['brand_id'] );
							if( $str != "" )
								$output .= '<br>'.$str;

								$str = $this->tec->get_internal_description( $values['article_name'], $values['article_no'], $values['brand_id']);
							if( $str != "" )
								$output .= '<br>'.$str;
						break;
				} // switch
				break;
		} // switch

		return( $output );
	} // get_special_field_value

	function change_field_values( &$arrFields ) {
		switch( $arrFields['field'] ) {
			case 'sub_first_name': case 'sub_last_name': case 'sub_adress': case 'sub_phone': case 'sub_email':
					if( $this->tec->sub_module_active() )
						$arrFields['mandatory'] = 1;
				break;
		} // switch
	} // change_field_values

	function set_fields_special( $list_id, &$arrFields ) {
		switch( $list_id ) {
			case 36: // TecDoc - interne Nr.
					$setup = $this->f->load_setup( "TEC_SETUP" );

					for( $i=0; $i<sizeof( $arrFields); $i++ ) {
						if( $arrFields[$i]['pos'] == 60 ) {
							$arrFields[$i]['visible'] = $setup['item_check_active'];
						} // if
					} // for
				break;
		} // switch
	} // set_fields_special

} // list_elpida_atit_at
?>