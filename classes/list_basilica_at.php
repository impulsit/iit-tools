<?php
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'translate.php' );
require_once( CLASS_DIR.'theme_functions.php' );

class list_basilica_at {
	static private $instance = null;
	private $db = null;
	private $f = null;
	private $t = null;
	private $theme = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
		$this->t = translate::getInstance();
		$this->theme = theme_functions::getInstance();
	} // __construct

	function print_menu_info( $list_id ) {
		switch( $list_id ) {
		} // switch
	} // print_menu_info

	function get_spezial_filter( $list_id ) {
		switch( $list_id ) {
			case 1: // Kurse
					// nur Laufende Kurse anzeigen
					if( $_SESSION['list_'.$list_id]['nur_laufende'] )
						return( ' AND (enddatum>=DATE(NOW()))' );
				break;
		} // switch

		return( null );
	} // get_spezial_filter

	function insert_special_list_line( $list_id, $values, $bLastLine = false ) {
		switch( $list_id ) {
		} // switch
	} // insert_special_list_line

	function special_save( $list_id ) {
		switch( $list_id ) {
		} // switch
	} // special_save

	function add_special_line_buttons( $list_id, $values ) {
		switch( $list_id ) {
			case 1: // Kurs
				$this->db->query( "SELECT lehrabschluss FROM BAS_KURSE WHERE kurs_id='".$values['kurs_id']."'", "link_kurs" );
				$r2 = $this->db->getNext( "link_kurs" );

				$wkw = '';
				if( $r2['lehrabschluss'] == 1 )
					$wkw = '<li><a href="?indiv_create_wkw='.$values['kurs_id'].'"><i class="fas fa-download text-primary fa-fw"></i> WKW-Datei erstellen</a></li>';

				echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=19"><i class="fas fa-print text-success fa-fw"></i> Anmeldeformular</a></li>
									<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=22"><i class="fas fa-print text-success fa-fw"></i> Erlagschein</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=49"><i class="fas fa-print text-success fa-fw"></i> WAFF Erlagschein Teiln.</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=48"><i class="fas fa-print text-success fa-fw"></i> WAFF Rechnung Teiln.</a></li>
									<li><a href="list_redirect.php?indiv_list_id=3&t.kurs_id='.$values['kurs_id'].'"><i class="fas fa-users text-primary fa-fw"></i> Teilnehmer anzeigen</a></li>
									<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=45"><i class="fas fa-print text-success fa-fw"></i> Teilnehmerliste</a></li>
									<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=58"><i class="fas fa-print text-success fa-fw"></i> WAFF Teilnehmerliste</a></li>
									<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=60"><i class="fas fa-print text-success fa-fw"></i> ADR Teilnehmerliste</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=26"><i class="fas fa-print text-success fa-fw"></i> Kostenvoranschlag</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=23"><i class="fas fa-print text-success fa-fw"></i> Kurszeitbestätigung AMS</a></li>
									<li><a class="ams_mail_beginn" data-id="'.$values['kurs_id'].'"><i class="fas fa-envelope text-primary fa-fw"></i> Kursbeginn MAIL AMS</a></li>
									<li><a class="ams_mail_ende" data-id="'.$values['kurs_id'].'"><i class="fas fa-envelope text-primary fa-fw"></i> Kursende MAIL AMS</a>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=25"><i class="fas fa-print text-success fa-fw"></i> Rechnung komplett</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=47"><i class="fas fa-print text-success fa-fw"></i> WAFF Rechnung</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=57"><i class="fas fa-print text-success fa-fw"></i> WAFF LAP Rechnung</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=46"><i class="fas fa-print text-success fa-fw"></i> Teilnahmebestätigung</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=18"><i class="fas fa-print text-success fa-fw"></i> Kursbestätigung</a></li>
									<li><a href="bas_drucken.php?type=kurs&kurs_id='.$values['kurs_id'].'&bericht_id_kurs=21"><i class="fas fa-print text-success fa-fw"></i> Anwesenheitsliste</a></li>
									<li><a href="bas_drucken.php?type=trainer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_trainer_kurs=44"><i class="fas fa-print text-success fa-fw"></i> Anwesenheitsliste/Protokoll/Kran/Stapler</a></li>
									<li><a class="kurs_trainer_zuordnen" data-id="'.$values['kurs_id'].'"><i class="fas fa-user-graduate text-primary fa-fw fa-fw"></i> Trainer zuordnen</a></li>
									<li><a href="bas_drucken.php?type=trainer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_trainer_kurs=35"><i class="fas fa-print text-success fa-fw"></i> Werkvertrag</a></li>
									<li><a href="bas_drucken.php?type=trainer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_trainer_kurs=34"><i class="fas fa-print text-success fa-fw"></i> Honorarnote</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=24"><i class="fas fa-print text-success fa-fw"></i> WAFF Zahlungsbestätigung Teiln.</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&bericht_id_teilnehmer_kurs=54"><i class="fas fa-print text-success fa-fw"></i> Zahlungsbestätigung komplett</a></li>
									'.$wkw.'
								</ul>
							</li>
						</ul>';
				break;
			case 2: // Teilnehmer
					echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a class="zu_kurs_zuordnen" data-id="'.$values['teilnehmer_id'].'"><i class="fas fa-plus-circle text-success fa-fw"></i> zu Kurs zuordnen</a></li>
									<li><a href="?indiv_del_tn_kurszuordnung='.$values['teilnehmer_id'].'"><i class="fas fa-times-circle text-danger fa-fw"></i> alle Kurszuordnungen löschen</a></li>
									<li><a href="list_redirect.php?indiv_list_id=25&t.teilnehmer_id='.$values['teilnehmer_id'].'"><i class="fas fa-warehouse text-primary fa-fw"></i> Kurse anzeigen</a></li>
								</ul>
							</li>
						</ul>';
				break;
			case 21: // Trainer
					echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a href="list_redirect.php?indiv_list_id=22&t.trainer_id='.$values['trainer_id'].'"><i class="fas fa-plus-circle text-success fa-fw"></i> Kurszeiten zuordnen</a></li>
									<li><a href="bas_drucken.php?type=trainer_kurs&trainer_id='.$values['trainer_id'].'&bericht_id_trainer_kurs=35"><i class="fas fa-print text-success fa-fw"></i> Werkvertrag drucken</a></li>
									<li><a href="bas_drucken.php?type=trainer_kurs&trainer_id='.$values['trainer_id'].'&bericht_id_trainer_kurs=34"><i class="fas fa-print text-success fa-fw"></i> Honorar drucken</a></li>
								</ul>
							</li>
						</ul>';
				break;
			case 25: // zugeordnete Kurse
					echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&teilnehmer_id='.$values['teilnehmer_id'].'&bericht_id_teilnehmer_kurs=26"><i class="fas fa-print text-success fa-fw"></i> Kostenvoranschlag</a></li>
									<li><a href="bas_drucken.php?type=teilnehmer_kurs&kurs_id='.$values['kurs_id'].'&teilnehmer_id='.$values['teilnehmer_id'].'&bericht_id_teilnehmer_kurs=23"><i class="fas fa-print text-success fa-fw"></i> Kurszeitbestätigung</a></li>
								</ul>
							</li>
						</ul>';
				break;
		} // switch
	} // add_special_line_buttons

	function add_special_action_buttons_main( $list_id ) {
		$strOut = null;

		switch( $list_id ) {
			case 1: // Kurse
					$strOut .= '<a href="?indiv_nur_laufende='.($_SESSION['list_'.$list_id]['nur_laufende']?'0':'1').'" class="link_click_button '.($_SESSION['list_'.$list_id]['nur_laufende']?'button_checked':'').'">'.$this->f->get_button( 'nur laufende' ).'</a>';
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
			case 2: // Teilnehmer
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
			case 3: // Kursteilnehmer
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
			case 10: // AMS Kontakte
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
			case 21: // Trainer
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
			case 23: // E-Mail Warteschlange
					$strOut .= '<a class="send_email_process link_click_button">'.$this->f->get_button( 'E-Mail senden' ).'</a>';
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
			case 25: // Kursteilnehmer
					$strOut .= '<a href="#" onclick="print_window(\'content_scroll\')">'.$this->f->get_button( 'Liste drucken' ).'</a>';
				break;
		} // switch

		return( $strOut );
	} // add_special_action_buttons_main

	function parse_special_buttons( $list_id, $param) {
		$infotext = null;

		switch( $list_id ) {
			case 1: // Kurse
					// Nur Laufende Kurse
					if( isset( $_GET['indiv_nur_laufende'] ) ) {
						if( $_GET['indiv_nur_laufende'] == 0 )
							$_SESSION['list_'.$list_id]['nur_laufende'] = false;
						else
							$_SESSION['list_'.$list_id]['nur_laufende'] = true;
					} // if
				break;
			case 2: // Teilnehmer
					if( isset( $_GET['indiv_del_tn_kurszuordnung'] ) ) {
						$this->db->delete( "BAS_KURSTEILNEHMER", "status_id!='2' AND teilnehmer_id='".$_GET['indiv_del_tn_kurszuordnung']."'" );
						$this->db->commit();
					} // if
					if( isset( $_GET['indiv_del_kurszuordnung'] ) ) {
						$this->db->delete( "BAS_KURSTEILNEHMER", "kursteilnehmer_id='".$_GET['indiv_del_kurszuordnung']."'" );
						$this->db->commit();
					} // if
				break;
		} // switch

		return( $infotext );
	} // parse_special_line_buttons

	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$output = null;
		$c = '';

		switch( $list_id ) {
			case 1: // Kurse
					switch( $function ) {
						case "calc_kurs_einnahmen": $output = $this->calc_kurs_einnahmen( $primary_key, false ); $c = "right no_linebreak"; break;
						case "calc_kurs_ausgaben": $output = $this->calc_kurs_ausgaben( $primary_key, false ); $c = "right no_linebreak"; break;
						case "calc_kurs_saldo": $output = $this->calc_kurs_saldo( $primary_key ); $c = "right"; break;
						case "get_kurs_teilnehmer_anzahl": $output = $this->get_kurs_teilnehmer_anzahl( $primary_key, false ); $c = "right"; break;
						case "calc_kosten_gesamt": $output = $this->calc_kosten_gesamt( $primary_key, false ); $c = "right no_linebreak"; break;
					} // switch
				break;
			case 2: // Teilnehmer
					switch( $function ) {
						case "get_teilnehmer_kurs_zuordnungen": $output = $this->get_teilnehmer_kurs_zuordnungen( $primary_key ); $c = "left"; break;
					} // switch
				break;
		} // switch

		return( $output );
	} // get_special_functions

	function get_teilnehmer_kurs_zuordnungen( $id ) {
		$output = "";

		$this->db->query( "
			SELECT
				kt.kursteilnehmer_id, k.nummer, k.startdatum, k.enddatum, kt.status_id
			FROM BAS_KURSTEILNEHMER AS kt
			LEFT JOIN BAS_KURSE AS k ON (k.kurs_id=kt.kurs_id)
			WHERE kt.teilnehmer_id='".$id."'", "get_teilnehmer_kurs_zuordnungen" );
		while( $this->db->isNext( "get_teilnehmer_kurs_zuordnungen" ) ) {
			$r = $this->db->getNext( "get_teilnehmer_kurs_zuordnungen" );

			$str = $r['nummer'].' ('.date( "d.m.Y", strtotime( $r['startdatum'] ) ).' bis '.date( "d.m.Y", strtotime( $r['enddatum'] ) ).')';
			if( $r['status_id'] == 2 )
				$output .= '<span class="green">'.$str.' (bezahlt)</span>';
				else
					$output .= '<span class="red" style="vertical-align: top;">'.$str.'</span>';

					if( $r['status_id'] != 2 ) {
						// Lösch Button
						$output .= '<a href="?indiv_del_kurszuordnung='.$r['kursteilnehmer_id'].'" onClick=\'if( confirm("Kurszuordnung wirklich löschen?") ); else return( false );\'><i class="fas fa-times-circle text-danger fa-fw"></i></a>';
					} // else

					$output .= ', ';
		} // while
		$output = substr( $output, 0, -2 );

		return( $output );
	} // get_teilnehmer_kurs_zuordnungen

	function calc_kurs_einnahmen( $id, $bGetValue = false ) {
		$this->db->query( "SELECT kosten, kosten_wirtschaftskammer FROM BAS_KURSE WHERE kurs_id='".$id."'", "calc_kurs_einnahmen" );
		$r = $this->db->getNext( "calc_kurs_einnahmen" );

		$summe = $r['kosten'] * $this->get_kurs_teilnehmer_anzahl( $id, true );
		$summe2 = ($r['kosten'] + $r['kosten_wirtschaftskammer']) * $this->get_kurs_teilnehmer_anzahl( $id, true );

		$tip = '
			<table class=list_left>
				<tr><th>Kosten Kurs:</th><td class=right>'.number_format( $r['kosten'], 2, ',', '.' ).'</td></tr>
				<tr><th>Teilnehmer:</th><td class=right>'.number_format( $this->get_kurs_teilnehmer_anzahl( $id, true ), 2, ',', '.' ).'</td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><th>Summe:</th><td class=right>'.number_format( $summe, 2, ',', '.' ).'</td></tr>
				<tr><td colspan=2>&nbsp</td></tr>

				<tr><th>Kosten Gesamt:</th><td class=right>'.number_format( $r['kosten'] + $r['kosten_wirtschaftskammer'], 2, ',', '.' ).'</td></tr>
				<tr><th>Teilnehmer:</th><td class=right>'.number_format( $this->get_kurs_teilnehmer_anzahl( $id, true ), 2, ',', '.' ).'</td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><th>Summe:</th><td class=right>'.number_format( $summe2, 2, ',', '.' ).'</td></tr>
			</table>
		';

		$output = '<div class="bg_dreieck"><span class="summe" title="'.$tip.'">'.number_format( $summe2, 2, ',', '.' ).'</span></div>';
		if( $summe2 == 0 )
			$output = '';

		if( $bGetValue )
			$output = $summe2;

		return( $output );
	} // calc_kurs_einnahmen

	function calc_kurs_ausgaben( $id, $bGetValue = false ) {
		$this->db->query( "SELECT abw_trainer_stundensatz, stunden_pro_tag, kosten_wirtschaftskammer, raummiete, kosten_material FROM BAS_KURSE WHERE kurs_id='".$id."'", "calc_kurs_ausgaben" );
		$r = $this->db->getNext( "calc_kurs_ausgaben" );

		if( $r['abw_trainer_stundensatz'] > 0 )
			$field = $r['abw_trainer_stundensatz'];
		else
			$field = "t.stundensatz";

		// Anzahl Stunden Trainer
		$this->db->query(
			"SELECT
				SUM(IF(tk.manuelle_stunden=0,".$r['stunden_pro_tag'].",tk.manuelle_stunden)*".$field.") AS kosten
				FROM BAS_TRAINER_KURSE AS tk
				LEFT JOIN BAS_TRAINER AS t ON (t.trainer_id=tk.trainer_id)
				WHERE tk.kurs_id='".$id."'", "calc_kurs_ausgaben" );
		$r2 = $this->db->getNext( "calc_kurs_ausgaben" );

		// Pauschale
		$this->db->query(
			"SELECT
				SUM(pauschale) AS kosten
				FROM BAS_TRAINER_KURSE AS tk
				LEFT JOIN BAS_TRAINER AS t ON (t.trainer_id=tk.trainer_id)
				WHERE tk.kurs_id='".$id."'", "calc_kurs_ausgaben" );
		$r3 = $this->db->getNext( "calc_kurs_ausgaben" );
		if( $r3['kosten'] > 0 ) $r2['kosten'] = $r3['kosten'];

		$summe = $r['kosten_wirtschaftskammer'] * $this->get_kurs_teilnehmer_anzahl( $id, true ) + $r['raummiete'] + $r2['kosten'] + $r['kosten_material'] * $this->get_kurs_teilnehmer_anzahl( $id, true );

		$tip = '
			<table class=list_left>
				<tr><th>Kosten Prüfung pro TeilnehmerIn:</th><td class=right>'.number_format( $r['kosten_wirtschaftskammer'], 2, ',', '.' ).'</td></tr>
				<tr><th>Zwischensumme x TeilnehmerIn:</th><td class=right>'.number_format( $r['kosten_wirtschaftskammer'] * $this->get_kurs_teilnehmer_anzahl( $id, true ), 2, ',', '.' ).'</td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><th>Kosten Material pro TeilnehmerIn:</th><td class=right>'.number_format( $r['kosten_material'], 2, ',', '.' ).'</td></tr>
				<tr><th>Zwischensumme x TeilnehmerIn:</th><td class=right>'.number_format( $r['kosten_material'] * $this->get_kurs_teilnehmer_anzahl( $id, true ), 2, ',', '.' ).'</td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><th>Kosten Räume:</th><td class=right>'.number_format( $r['raummiete'], 2, ',', '.' ).'</td></tr>
				<tr><th>Kosten Trainer:</th><td class=right>'.number_format( $r2['kosten'], 2, ',', '.' ).'</td></tr>
				<tr><td colspan=2><hr></td></tr>
				<tr><th>Summe:</th><td class=right>'.number_format( $summe, 2, ',', '.' ).'</td></tr>
			</table>
		';

		$output = '<div class="bg_dreieck"><span class="summe_minus" title="'.$tip.'">-'.number_format( $summe, 2, ',', '.' ).'</span></div>';
		if( $summe == 0 )
			$output = '';

		if( $bGetValue )
			$output = $summe;

		return( $output );
	} // calc_kurs_ausgaben

	function calc_kurs_saldo( $id ) {
		$saldo = $this->calc_kurs_einnahmen( $id, true ) - $this->calc_kurs_ausgaben( $id, true );

		if( $saldo >= 0 )
			$output = '<span class="summe">'.number_format( $saldo, 2, ',', '.' ).'</span>';
		else
			$output = '<span class="summe_minus">'.number_format( $saldo, 2, ',', '.' ).'</span>';

		return( $output );
	} // calc_kurs_saldo


	function get_kurs_teilnehmer_anzahl( $id, $bGetValue = false ) {
		$summe = 0;

		$this->db->query( "
			SELECT COUNT( kurs_id ) AS anz
			FROM BAS_KURSTEILNEHMER
			WHERE kurs_id='".$id."'", "get_kurs_teilnehmer_anzahl" );
		$r = $this->db->getNext( "get_kurs_teilnehmer_anzahl" );

		$output = '<span class="summe">'.$r['anz'].'</span>';

		if( $bGetValue )
			$output = $r['anz'];

		return( $output );
	} // get_kurs_teilnehmer_anzahl

	function calc_kosten_gesamt(  $id, $bGetValue =false ) {
		$this->db->query( "SELECT kosten, kosten_wirtschaftskammer FROM BAS_KURSE WHERE kurs_id='".$id."'", "calc_kurs_einnahmen" );
		$r = $this->db->getNext( "calc_kurs_einnahmen" );

		$summe = $r['kosten'] + $r['kosten_wirtschaftskammer'];

		$tip = '
			<table class=list_left>
				<tr><th>Kosten Kurs:</th><td class=right>'.number_format( $r['kosten'], 2, ',', '.' ).'</td></tr>
				<tr><th>Kosten Prüfung:</th><td class=right>'.number_format( $r['kosten_wirtschaftskammer'], 2, ',', '.' ).'</td></tr>
			</table>
		';

		$output = '<div class="bg_dreieck"><span class="summe" title="'.$tip.'">'.number_format( $summe, 2, ',', '.' ).'</span></div>';

		if( $bGetValue )
			$output = $summe;

		return( $output );
	} // calc_kosten_gesamt

} // list_elpida_atit_at

?>