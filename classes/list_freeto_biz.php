<?php
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'translate.php' );
require_once( CLASS_DIR.'theme_functions.php' );

class list_freeto_biz {
	static private $instance = null;
	private $db = null;
	private $f = null;
	private $t = null;
	private $theme = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
		$this->t = translate::getInstance();
		$this->theme = theme_functions::getInstance();
	} // __construct

	function print_menu_info( $list_id ) {
		switch( $list_id ) {
		} // switch
	} // print_menu_info

	function get_spezial_filter( $list_id ) {
		switch( $list_id ) {
		} // switch

		return( null );
	} // get_spezial_filter

	function insert_special_list_line( $list_id, $values, $bLastLine = false ) {
		switch( $list_id ) {
		} // switch
	} // insert_special_list_line

	function special_save( $list_id ) {
		switch( $list_id ) {
		} // switch
	} // special_save

	function add_special_line_buttons( $list_id, $values ) {
		switch( $list_id ) {
			case 13: // Fragebogen
					$this->db->query( "SELECT link_kurs FROM WHY_FRAGEBOGEN WHERE fragebogen_id='".$values['fragebogen_id']."'", "link_kurs" );
					$r2 = $this->db->getNext( "link_kurs" );

					$print = '';
					if( $r2['link_kurs'] == 1 )
						$print = '<li><a href="bas_drucken.php?type=fragebogen_kurs&fragebogen_id='.$values['fragebogen_id'].'&bericht_id_fragebogen_kurs=37"><img src="'.$this->theme->get_icon_path( 'drucken' ).'" width="16">Drucken</a></li>';

					echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a href="list_redirect.php?indiv_list_id=15&f.fragebogen_id='.$values['fragebogen_id'].'"><img src="'.$this->theme->get_icon_path( 'fragebogen' ).'" width="16">Fragen</a></li>
									<li><a href="list_redirect.php?indiv_list_id=18&a.fragebogen_id='.$values['fragebogen_id'].'"><img src="'.$this->theme->get_icon_path( 'erfassung' ).'" width="16">Auswertungen</a></li>
									<li><a href="why_statistik.php?indiv_list_id=0&fragebogen_id='.$values['fragebogen_id'].'"><img src="'.$this->theme->get_icon_path( 'statistik' ).'" width="16">Statistik</a></li>
									<li><a href="list_redirect.php?indiv_list_id=19&n.fragebogen_id='.$values['fragebogen_id'].'"><img src="'.$this->theme->get_icon_path( 'user' ).'" width="16">Nutzer</a></li>
									<li><a class="questions_copy_from" data-id="'.$values['fragebogen_id'].'"><img src="'.$this->theme->get_icon_path( 'login' ).'" width="16">Fragen kopieren von</a></li>
									'.$print.'
								</ul>
							</li>
						</ul>';
				break;
			case 15: // Fragen
					echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a href="list_redirect.php?indiv_list_id=20&a.frage_id='.$values['frage_id'].'"><img src="'.$this->theme->get_icon_path( 'fragebogen' ).'" width="16">Antworten</a></li>
									<li><a class="answer_copy_from" data-id="'.$values['frage_id'].'"><img src="'.$this->theme->get_icon_path( 'login' ).'" width="16">Antworten kopieren von</a></li>
									<li><a class="answer_copy_to" data-id="'.$values['frage_id'].'"><img src="'.$this->theme->get_icon_path( 'logout' ).'" width="16">Antworten kopieren nach</a></li>
								</ul>
							</li>
						</ul>';
				break;
			case 19: // Nutzer
				echo '
						<ul class="dropdown_menu dropdown_button">
							<li><a>'.$this->f->get_button( 'Menü' ).'</a>
								<ul>
									<li><a href="why_ansicht.php?id='.$values['id'].'"><img src="'.$this->theme->get_icon_path( 'search' ).'" width="16">Anzeige</a></li>
								</ul>
							</li>
						</ul>';
				break;
		} // switch
	} // add_special_line_buttons

	function add_special_action_buttons_main( $list_id ) {
		$strOut = null;

		switch( $list_id ) {
		} // switch

		return( $strOut );
	} // add_special_action_buttons_main

	function parse_special_buttons( $list_id, $param) {
		$infotext = null;

		switch( $list_id ) {
		} // switch

		return( $infotext );
	} // parse_special_line_buttons

	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$output = null;
		$c = "left";

		switch( $list_id ) {
		} // switch

		return( $output );
	} // get_special_functions
} // list_freeto_biz
?>