<?php
require_once( dirname( __FILE__ ).'/ajax_header.php' );

if( $_POST['action'] == "run_query" ) {
	switch( $_POST['table'] ) {
		case 'CORE_LISTS': $db->query( "SELECT list_query AS query FROM ".$_POST['table']." WHERE list_id='".$_POST['id']."'" ); break;
		case 'CORE_LISTS_FIELDS': $db->query( "SELECT select_query AS query FROM ".$_POST['table']." WHERE field_id='".$_POST['id']."'" ); break;
	} // switch
	$r = $db->getNext();

	$r['query'] = $list->clear_query( $r['query'] );
	if( substr( $r['query'], strlen( $r['query'] ) - 1, 1 ) == '=' )
		$r['query'] .= "''";

	$i = $db->query( $r['query'] );

	$return['count'] = $i;
} // if

if( $_POST['action'] == "get_email_data" ) {
	$db->query( "SELECT id, receiver_to FROM BAS_EMAIL_QUEUE WHERE id='".$_POST['email_id']."'" );
	$r = $db->getNext();

	$return['email_id'] = $_POST['email_id'];
	$return['email_an'] = $r['receiver_to'];
	$return['email_status'] = ($_POST['email_current_pointer'] + 1).' / '.$_POST['email_max'];
} // if

if( $_POST['action'] == "process_email" ) {
	$db->query( "SELECT send_filename, receiver_to, receiver_cc, receiver_bcc, filename, filename2, body, title FROM BAS_EMAIL_QUEUE WHERE id='".$_POST['email_id']."'" );
	$r = $db->getNext();

	if( strpos( $r['send_filename'], ";" ) !== false )
		$filename = explode( ";", $r['send_filename'] );
	else
		$filename[0] = $r['send_filename'];
	
	$anhang = array();
	$anhang[] = array( 'content' => @file_get_contents( FILE_SAVE_DIR.'/'.$r['filename'] ),
										 'send_filename' => utf8_decode( $filename[0] ).".rtf" );
	if( isset( $filename[1] ) )
		$anhang[] = array( 'content' => @file_get_contents( FILE_SAVE_DIR.'/'.$r['filename2'] ),
											 'send_filename' => utf8_decode( $filename[1] ).".rtf" );

	$mail->send_mail( $r['receiver_to'], utf8_decode( $r['body'] ), utf8_decode( $r['title'] ), $anhang, $r['receiver_cc'], $r['receiver_bcc'] );

	if( $mes->noError() ) {
		$db->update( "BAS_EMAIL_QUEUE", array(
			"send_time" => $f->now(),
			"sended" => 1,
			"email_status" => 1
		), "id='".$_POST['email_id']."'" );
	} else {
		$db->update( "BAS_EMAIL_QUEUE", array(
			"send_time" => $f->now(),
			"sended" => 0,
			"email_status" => 2,
			"error" => $mes->getErrorMessages( false )
		), "id='".$_POST['email_id']."'" );
	} // else
	$db->commit();

	$return['reload_file'] = "list_redirect.php?indiv_list_id=23";
} // if

if( $_POST['action'] == "delete_temp_picture" ) {
	$path = $_POST['path'];

	$path = explode( '/', $path );

	@unlink( BASE_DIR.'upload/'.$path[0].'/'.$path[1] );
	@rmdir( BASE_DIR.'upload/'.$path[0] );
} //

$return['resultData'] = $strOut;

echo json_encode( $f->convert_ajax_remove( $return ) );
?>