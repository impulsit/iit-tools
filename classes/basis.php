<?php
ini_set( "max_execution_time", "600" );

if( !isset($_SESSION) ) session_start();

require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'mail.php' );
require_once( CLASS_DIR."list.php" );
require_once( CLASS_DIR.'print.php' );
require_once( CLASS_DIR.'report.php' );
require_once( CLASS_DIR."interface.php" );
require_once( CLASS_DIR."translate.php" );
require_once( CLASS_DIR."theme_functions.php" );

$print = druck::getInstance();
$report = report::getInstance();
$db = mysql::getInstance();
$f = functions::getInstance();
$mes = messages::getInstance();
$mail = fb_mail::getInstance();
$list = liste::getInstance();
$interface = sync_interface::getInstance();
$t = translate::getInstance();
$theme = theme_functions::getInstance();

global $c_id, $c_user_id;

if( !isset( $_SESSION['c_id'] ) ) {
	echo '<script language="javascript">location.href="'.DOMAIN.'admin/login_login.php?error=2";</script>';
	exit;
} // if

if( !$db->query( "
	SELECT
		user_id, email
	FROM CORE_USER_INFO
	WHERE session_id='".$_SESSION['c_id']."' AND ip='".$_SERVER['REMOTE_ADDR']."'" ) ) {
	echo '<script language="javascript">location.href="'.DOMAIN.'admin/login_login.php?error=3";</script>';
	exit;
} // if
$userinfo = $db->getNext();

$_SESSION['c_user_id'] = $userinfo['user_id'];
$c_user_id = $userinfo['user_id'];

$t->update_display_language();

// Datei Rechte prüfen
$f->check_user_access();
?>