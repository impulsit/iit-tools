<?php
require_once( CLASS_DIR.'translate.php' );

class messages {
	static private $instance = null;

	public $error = array();
	public $info = array();
	public $file;
	public $before_error = false;
	public $errorID = 0;
	private $t = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->t = translate::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function resetError() {
		$this->before_error = $this->noError();

		$this->error = array();
		$this->errorID = 0;
		unset( $_SESSION['mes'][$this->file]['error'] );
	} // ResetError

	function resetInfo() {
		$this->info = array();
		unset( $_SESSION['mes'][$this->file]['info'] );
	} // ResetInfo

	function resetMessages() {
		$this->resetError();
		$this->resetInfo();
	} // resetMessages

	function addError( $str ) {
		$this->error[] = $this->t->t( $str );
	} // AddError

	function addInfo( $str ) {
		$this->info[] = $this->t->t( $str );
	} // AddInfo

	function noError() {
		if( (sizeof( $this->error ) == 0) && ($this->errorID == 0) )
			return( true );

		return( false );
	} // NoError

	function beforeOutputNoError() {
		return( $this->before_error );
	} // NoError

	function getErrorMessages( $bWithDIV = true ) {
		$msg = '';

		if( sizeof( $this->error ) > 0 ) {
			if( $bWithDIV )
				$msg .= '<div class="alert alert-danger" role="alert">'; // <div class="errortext">';
			foreach( $this->error as $k => $v ) {
				$msg .= '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '.$v.'<br />';
			} // foreach
			if( $bWithDIV )
				$msg .= '</div>';
		} // if

		$this->resetError();

		return( $msg );
	} // getErrorMessages

	function getInfoMessages() {
		$msg = '';

		if( sizeof( $this->info ) > 0 ) {
			$msg .= '<div class="alert alert-success" role="alert">'; // <div class="infotext">';
			foreach( $this->info as $k => $v ) {
				$msg .= '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span> '.$v.'<br />';
			} // foreach
			$msg .= '</div>';
		} // if

		$this->resetInfo();

		return( $msg );
	} // getInfoMessages

	function printMessages() {
		$msg = '';

		$msg .= $this->getErrorMessages();
		$msg .= $this->getInfoMessages();

		return( $msg );
	} // printMessages

	function saveMessages( $file = "default" ) {
		$file = basename( $file );
		$file = str_replace( "_ajax", "", $file );
		$file = str_replace( ".php", "", $file );

		$this->file = $file;

		$_SESSION['mes'][$file]['error'] = $this->error;
		$_SESSION['mes'][$file]['info'] = $this->info;
		session_write_close();
	} // saveMessages

	function restoreMessages( $file = "default" ) {
		$file = basename( $file );
		$file = str_replace( "_ajax", "", $file );
		$file = str_replace( ".php", "", $file );

		$this->file = $file;

		if( isset( $_SESSION['mes'][$file]['error'] ) && (sizeof( $_SESSION['mes'][$file]['error'] ) > 0)  )
			$this->error = $_SESSION['mes'][$file]['error'];
		if( isset( $_SESSION['mes'][$file]['info'] ) && (sizeof( $_SESSION['mes'][$file]['info'] ) > 0) )
			$this->info = $_SESSION['mes'][$file]['info'];

		unset( $_SESSION['mes'][$file]['error'] );
		unset( $_SESSION['mes'][$file]['info'] );
	} // restoreMessages

	function setErrorMessageID( $i ) {
		$this->errorID = $i;
	} // setErrorMessageID

	function getErrorMessageID() {
		return( $this->errorID );
	} // setErrorMessageID

} // messages
?>