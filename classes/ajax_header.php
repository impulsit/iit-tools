<?php
if( !isset($_SESSION) ) session_start();

require_once( dirname( __FILE__ ).'/config_data.php' );
require_once( CLASS_DIR.'basis.php' );
require_once( CLASS_DIR.'print.php' );
if( file_exists( CLASS_DIR."list_tecdocbase_atit_at.php" ) ) {
	require_once( CLASS_DIR.'tecdoc.php' );
	$tec = tecdoc::getInstance();
} // if

$print = druck::getInstance();

$return = array();
$strOut = '';

/*
if( isset( $_POST ) ) {
	if ( is_array ( $GLOBALS['_POST'] ) ) {
		foreach ( $GLOBALS['_POST'] as $k => $v ) {
			if( strpos( $k, "[" ) ) {
				$var = substr( $k, 0, strpos( $k, "[" ) );
				$index = substr( $k, strpos( $k, "[" ) + 1 );

				$GLOBALS[$var][$index] = $v;
			} else
				$GLOBALS[$k] = $v;
		} // foreach
	} // if
} // if
*/
?>