<?php
require_once (CLASS_DIR . 'mysql.php');
require_once (CLASS_DIR . 'functions.php');
require_once (CLASS_DIR . 'translate.php');
require_once (CLASS_DIR . 'theme_functions.php');
require_once (CLASS_DIR . 'logtime.php');
require_once (CLASS_DIR . 'tecdoc_web_api.php');

class tecdoc {
	private static $instance = null;
	private $db = null;
	private $f = null;
	private $t = null;
	private $theme = null;
	private $log = null;
	private $web_api = null;
	private $arr_all_items = array();
	private $arr_brands = array();
	private $td_setup = array();

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
		$this->t = translate::getInstance();
		$this->theme = theme_functions::getInstance();
		$this->log = logtime::getInstance();
		$this->web_api = tecdoc_web_api::getInstance();

		$this->td_setup = $this->f->load_setup( "TEC_SETUP" );
	}

	private function __clone() {
	}

	public function __destruct() {
	}

	// ----------------------------------------------------------------------------------------
	// WEBSERVICE FUNCTION
	// ----------------------------------------------------------------------------------------
	function getContext( $data, $optional_headers ) {
		$params = array(
			'http' => array(
				'method' => 'POST',
				'content' => $data
			)
		);

		if( $optional_headers !== null ) {
			$params['http']['header'] = $optional_headers;
		} // if

		return (stream_context_create( $params ));
	}

	function createRequest( $functionName, $requestParams ) {
		return array(
			$functionName => $requestParams
		);
	}

	function callJSON( $function, $request, $optional_headers = null ) {
		$jsonRequest = json_encode( $request );

		$ctx = $this->getContext( $jsonRequest, $optional_headers );
		$fp = @fopen( $this->td_setup['td_service_url'], 'rb', false, $ctx );
		if( !$fp ) {
			// throw new Exception( "Problem with ".$this->td_setup['td_service_url'] );
		} // if

		$jsonResponse = @stream_get_contents( $fp );
		if( $jsonResponse === false ) {
			// throw new Exception( "Problem reading data from ".$this->td_setup['td_service_url'] );
		} // if

		$response = json_decode( $jsonResponse );

		return ($response);
	}

	// ----------------------------------------------------------------------------------------
	// GET FUNCTION
	// ----------------------------------------------------------------------------------------
	function get_search_countriesCarSelection() {
		return ($this->td_setup['td_countriesCarSelection']);
	}

	function get_search_articleCountry() {
		return ($this->td_setup['td_articleCountry']);
	}

	function get_search_country() {
		return ($this->td_setup['td_country']);
	}

	function get_search_tecdocid() {
		return ($this->td_setup['td_mandator']);
	}

	function get_search_linkingtargettype() {
		return ($this->td_setup['td_linkingtargettype']);
	}

	function get_search_language() {
		// Default Sprache
		$lang = $this->td_setup['td_language'];

		// Sub Shop Sprache für TecDoc ermitteln
		if( $this->sub_module_active() ) {
			// selbst SubAdmin
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin, sub_created_by_user_id' );
			if( $userinfo['sub_admin'] == 1 )
				$userinfo['sub_created_by_user_id'] = $_SESSION['c_user_id'];

			// Infos von SubAdmin holen
			if( $this->db->query( "SELECT sub_language FROM CORE_USER_INFO WHERE user_id='" . $userinfo['sub_created_by_user_id'] . "'" ) ) {
				$r = $this->db->getNext();

				if( $r['sub_language'] != '' )
					$lang = $r['sub_language'];
			} // if
		} // if

		// Sprache aus Sprachauswahl
		if( isset( $_SESSION['language'] ) ) {
			$this->db->query( "SELECT tecdoc_code FROM CORE_LANGUAGE_SETUP WHERE layout_code='".$_SESSION['language']."' AND active='1'" );
			$r = $this->db->getNext();
			if( $r['tecdoc_code'] != '' )
				$lang = $r['tecdoc_code'];
		} // if

		return ($lang);
	}

	function get_manufacturer() {
		$function = 'addDynamicAddress';
		$params = array(
			'address' => $_SERVER['REMOTE_ADDR'],
			'provider' => $this->get_search_tecdocid(),
			'validityHours' => 24
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$function = 'getManufacturers';
		$params = array(
			'country' => $this->get_search_country(),
			'lang' => $this->get_search_language(),
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr[] = array(
			"id" => 0,
			"name" => " "
		);
		foreach( $result->data->array as $k => $v ) {
			$arr[] = array(
				"id" => $v->manuId,
				"name" => $v->manuName
			);
		} // foreach

		$this->f->array_sort_by_column( $arr, "name" );

		return ($arr);
	}

	function get_manufacturer_select( $selected, $all ) {
		$arrAllowed = array(
			'ALFA ROMEO',
			'AUDI',
			'BMW',
			'CITROËN',
			'DACIA',
			'FIAT',
			'FORD',
			'IVECO',
			'LANCIA',
			'MERCEDES-BENZ',
			'OPEL',
			'PEUGEOT',
			'RENAULT',
			'SEAT',
			'SKODA',
			'SMART',
			'VW'
		);
		$result = $this->get_manufacturer();

		$str = '';

		$all = false;
		$str .= '<optgroup label="'.$this->t->t( "Häufigste Auswahl" ).'">';
		foreach( $result as $k => $v ) {
			if( $v['id'] == $selected )
				$sel = " selected";
			else
				$sel = "";

			if( $all || (!$all && (in_array( $v['name'], $arrAllowed ))) || ($v['name'] == " ") )
				$str .= '<option value="' . $v['id'] . '"' . $sel . '>' . $v['name'] . '</option>';
		} // foreach
		$str .= '</optgroup>';

		$all = true;
		$str .= '<optgroup label="'.$this->t->t( "Alle Hersteller" ).'">';
		foreach( $result as $k => $v ) {
			if( $v['id'] == $selected )
				$sel = " selected";
				else
					$sel = "";

					if( $all || (!$all && (in_array( $v['name'], $arrAllowed ))) || ($v['name'] == " ") )
						$str .= '<option value="' . $v['id'] . '"' . $sel . '>' . $v['name'] . '</option>';
		} // foreach
		$str .= '</optgroup>';

		return ($str);
	}

	function get_brand() {
		$function = 'getAmBrands';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr[] = array(
			"id" => 0,
			"name" => " "
		);
		foreach( $result->data->array as $k => $v ) {
			$arr[] = array(
				"id" => $v->brandId,
				"name" => $this->fix_brandname( $v->brandName )
			);
		} // foreach

		$this->f->array_sort_by_column( $arr, "name" );

		return ($arr);
	}

	function get_brand_id( $brandname, $bForceReload = false ) {
		$brand_id = 0;

		if( $this->db->query( "SELECT brand_id FROM TEC_ITEMS WHERE UPPER(hersteller)='" . $this->fix_brandname( $brandname ) . "' LIMIT 1", "get_brand_id" ) ) {
			$r = $this->db->getNext( "get_brand_id" );

			$brand_id = $r['brand_id'];
		} // if

		if( $brand_id == 0 ) {
			if( !isset( $this->arr_brands ) || $bForceReload || (sizeof( $this->arr_brands ) == 0) ) {
				$function = 'getAmBrands';
				$params = array(
					'articleCountry' => $this->get_search_articleCountry(),
					'lang' => $this->get_search_language(),
					'provider' => $this->get_search_tecdocid()
				);
				$request = $this->createRequest( $function, $params );
				$result = $this->callJSON( $function, $request );

				$this->arr_brands = $result->data->array;
			} // if

			foreach( $this->arr_brands as $k => $v ) {
				if( strtoupper( $v->brandName ) == strtoupper( $brandname ) )
					return ($v->brandId);
			} // foreach
		} // if

		return ($brand_id);
	}

	function fix_brandname( $brandname ) {
		$brandname = strtoupper( $brandname );
		$brandname = str_replace( "'", "", $brandname );

		return( $brandname );
	}

	function get_brand_select() {
		$result = $this->get_brand();

		$str = '';
		foreach( $result as $k => $v ) {
			$str .= '<option value="' . $v['id'] . '">' . $v['name'] . '</option>';
		} // foreach

		return ($str);
	}

	function get_model( $manufacturer, $year_from_input, $year_to_input ) {
		$function = 'getModelSeries';
		$params = array(
			'country' => $this->get_search_country(),
			'lang' => $this->get_search_language(),
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'manuId' => $manufacturer,
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr[] = array(
			"id" => 0,
			"name" => ""
		);
		if( isset( $result->data->array ) ) {
			foreach( $result->data->array as $k => $v ) {
				$bShow = true;

				if( $year_from_input != 0 ) {
					if( isset( $v->yearOfConstrFrom ) )
						$year_from = (int)substr( $v->yearOfConstrFrom, 0, 4 );
					else
						$year_from = 1900;
					if( isset( $v->yearOfConstrTo ) )
						$year_to = (int)substr( $v->yearOfConstrTo, 0, 4 );
					else
						$year_to = date( "Y" );
					if( $year_from_input < $year_from )
						$bShow = false;
					if( $year_from_input > $year_to )
						$bShow = false;
				} // if

				if( $bShow ) {
					$from = "";
					if( isset( $v->yearOfConstrFrom ) )
						$from = $this->ConvertYearToView( $v->yearOfConstrFrom );
					$to = "";
					if( isset( $v->yearOfConstrTo ) )
						$to = $this->ConvertYearToView( $v->yearOfConstrTo );
					$from_to = $from . " - " . $to;

					$arr[] = array(
						"id" => $v->modelId,
						"name" => $v->modelname . ' (' . $from_to . ')'
					);
				} // if
			} // foreach
		} // if

		$this->f->array_sort_by_column( $arr, "name" );

		return ($arr);
	}

	function get_vehicle( $manufacturer, $model, $filter_fueltype, $filter_pskw, $filter_motorcode ) {
		$function = 'getVehicleIdsByCriteria';
		$params = array(
			'countriesCarSelection' => $this->get_search_countriesCarSelection(),
			'lang' => $this->get_search_language(),
			'carType' => $this->get_search_linkingtargettype(),
			'manuId' => $manufacturer,
			'modId' => $model,
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr2 = array();
		$arr2[] = array(
			"id" => 0,
			"name" => " ",
			"fueltype" => " ",
			"pskw" => " ",
			"motorcode" => array(
				" "
			),
			"show" => true
		);
		if( isset( $result->data->array ) ) {
			foreach( $result->data->array as $k => $v ) {
				$arr[] = $v->carId;

				if( sizeof( $arr ) >= 25 ) {
					$arr2 = $this->get_vehicle_details( $arr2, $arr, $filter_fueltype, $filter_pskw, $filter_motorcode );
					$arr = array();
				} // if
			} // foreach
			$arr2 = $this->get_vehicle_details( $arr2, $arr, $filter_fueltype, $filter_pskw, $filter_motorcode );

			$this->f->array_sort_by_column( $arr, "name" );
		} // if

		return ($arr2);
	}

	function get_vehicle_details( $arr2, $arr, $filter_fueltype, $filter_pskw, $filter_motorcode ) {
		$function = 'getVehicleByIds3';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'countriesCarSelection' => $this->get_search_countriesCarSelection(),
			'lang' => $this->get_search_language(),
			'country' => $this->get_search_country(),
			'carIds' => array(
				'array' => $arr
			),
			'motorCodes' => true,
			'provider' => $this->get_search_tecdocid()
		);

		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		if( isset( $result->data->array ) )
			foreach( $result->data->array as $k => $v ) {
				$from = "";
				if( isset( $v->vehicleDetails->yearOfConstrFrom ) )
					$from = $this->ConvertYearToView( $v->vehicleDetails->yearOfConstrFrom );

				$to = "";
				if( isset( $v->vehicleDetails->yearOfConstrTo ) )
					$to = $this->ConvertYearToView( $v->vehicleDetails->yearOfConstrTo );
				$from_to = $from . " - " . $to . ", ";

				$pskw_var = "";
				$ps = "";
				if( isset( $v->vehicleDetails->powerHpFrom ) ) {
					if( $v->vehicleDetails->powerHpFrom == $v->vehicleDetails->powerHpTo ) {
						$ps = $v->vehicleDetails->powerHpFrom . $this->t->t( "PS" ) . ", ";
						if( strlen( $v->vehicleDetails->powerHpFrom ) == 2 )
							$pskw_var .= " ";
						$pskw_var .= $v->vehicleDetails->powerHpFrom . $this->t->t( "PS" ) . " / ";
					} else {
						$ps = $v->vehicleDetails->powerHpFrom . $this->t->t( "PS" ) . " - " . $v->vehicleDetails->powerHpTo . $this->t->t( "PS" ) . ", ";
						if( strlen( $v->vehicleDetails->powerHpFrom ) == 2 )
							$pskw_var .= " ";
						$pskw_var .= $v->vehicleDetails->powerHpFrom . " - " . $v->vehicleDetails->powerHpTo . $this->t->t( "PS" ) . " / ";
					} // else
				} // if

				$kw = "";
				if( isset( $v->vehicleDetails->powerKwFrom ) ) {
					if( $v->vehicleDetails->powerKwFrom == $v->vehicleDetails->powerKwTo ) {
						$kw = $v->vehicleDetails->powerKwFrom . $this->t->t( "KW" ) . ", ";
						$pskw_var .= $v->vehicleDetails->powerKwFrom . $this->t->t( "KW" );
					} else {
						$kw = $v->vehicleDetails->powerKwFrom . $this->t->t( "KW" ) . " - " . $v->vehicleDetails->powerKwTo . $this->t->t( "KW" ) . ", ";
						$pskw_var .= $v->vehicleDetails->powerKwFrom . " - " . $v->vehicleDetails->powerKwTo . $this->t->t( "KW" );
					} // else
				} // if

				$cylinder = "";
				if( isset( $v->vehicleDetails->cylinder ) )
					$cylinder = $v->vehicleDetails->cylinder . " " . $this->t->t( "Zylinder" ) . ", ";

				$ccm = "";
				if( isset( $v->vehicleDetails->cylinderCapacityCcm ) )
					$ccm = $v->vehicleDetails->cylinderCapacityCcm . $this->t->t( "ccm" ) . ", ";

				$fueltype = "";
				$fueltype_var = "";
				if( isset( $v->vehicleDetails->fuelType ) ) {
					$fueltype = $v->vehicleDetails->fuelType . ", ";
					$fueltype_var = $v->vehicleDetails->fuelType;
				} // if

				$constype = "";
				if( isset( $v->vehicleDetails->constructionType ) )
					$constype = $v->vehicleDetails->constructionType;

				$bInsert1 = false;
				$motorcodes = array();
				if( isset( $v->motorCodes->array ) ) {
					foreach( $v->motorCodes->array as $k2 => $v2 ) {
						$motorcodes[] = $v2->motorCode;

						if( ($filter_motorcode != "") && ($filter_motorcode == $v2->motorCode) )
							$bInsert1 = true;
					} // foreach
				} // if
				if( $filter_motorcode == "" )
					$bInsert1 = true;

				$bInsert2 = false;
				if( ($filter_fueltype != "") && ($filter_fueltype == $fueltype_var) )
					$bInsert2 = true;
				if( $filter_fueltype == "" )
					$bInsert2 = true;

				$bInsert3 = false;
				if( ($filter_pskw != "") && (trim( $filter_pskw ) == trim( $pskw_var )) )
					$bInsert3 = true;
				if( $filter_pskw == "" )
					$bInsert3 = true;

				if( isset( $v->vehicleDetails->typeName ) )
					$arr2[] = array(
						"id" => $v->carId,
						"name" => /*str_pad( $v->vehicleDetails->typeName, 30, '_' )*/ $v->vehicleDetails->typeName . ", " . join( ", ", $motorcodes ) . " (" . $from_to . $ccm . $kw . $ps . $cylinder . $fueltype . $constype . ")",
						"fueltype" => $fueltype_var,
						"pskw" => $pskw_var,
						"motorcode" => $motorcodes,
						"show" => $bInsert1 && $bInsert2 && $bInsert3
					);
			} // foreach

		return ($arr2);
	}

	function get_vehicle_by_motor_code( $motor_code ) {
		$function = 'getMotorIdsByManuIdCriteria2';
		$params = array(
			'country' => $this->get_search_country(),
			'lang' => $this->get_search_language(),
			'motorCode' => $motor_code,
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr2 = array();
		$arr2[0] = array(
			"id" => 0,
			"name" => " ",
			"manufacturer" => 0,
			"model" => 0
		);
		if( isset( $result->data->array ) ) {
			foreach( $result->data->array as $k => $v ) {
				$arr[] = $v->motorId;
			} // foreach

			$function = 'getMotorByIds2';
			$params = array(
				'lang' => $this->get_search_language(),
				'country' => $this->get_search_country(),
				'motorIds' => array(
					'array' => $arr
				),
				'provider' => $this->get_search_tecdocid()
			);
			$request = $this->createRequest( $function, $params );
			$result = $this->callJSON( $function, $request );

			$arr = array();
			if( isset( $result->data->array ) ) {
				foreach( $result->data->array as $k => $v ) {
					$function = 'getVehicleIdsByMotor2';
					$params = array(
						'countriesCarSelection' => $this->get_search_countriesCarSelection(),
						'lang' => $this->get_search_language(),
						'provider' => $this->get_search_tecdocid(),
						'carType' => $this->get_search_linkingtargettype(),
						'motorId' => $v->motorId
					);
					$request = $this->createRequest( $function, $params );
					$result = $this->callJSON( $function, $request );

					$arr = array();
					if( isset( $result->data->array ) ) {
						foreach( $result->data->array as $k => $v ) {
							$arr[] = $v->carId;
						} // foreach

						$function = 'getVehicleByIds3';
						$params = array(
							'articleCountry' => $this->get_search_articleCountry(),
							'countriesCarSelection' => $this->get_search_countriesCarSelection(),
							'lang' => $this->get_search_language(),
							'country' => $this->get_search_country(),
							'carIds' => array(
								'array' => $arr
							),
							'provider' => $this->get_search_tecdocid()
						);
						$request = $this->createRequest( $function, $params );
						$result = $this->callJSON( $function, $request );

						if( isset( $result->data->array ) )
							foreach( $result->data->array as $k => $v ) {
								$from = "";
								if( isset( $v->vehicleDetails->yearOfConstrFrom ) )
									$from = $this->ConvertYearToView( $v->vehicleDetails->yearOfConstrFrom );
								$to = "";
								if( isset( $v->vehicleDetails->yearOfConstrTo ) )
									$to = $this->ConvertYearToView( $v->vehicleDetails->yearOfConstrTo );
								$from_to = $from . " - " . $to . ", ";
								$kw = "";
								if( $v->vehicleDetails->powerKwFrom == $v->vehicleDetails->powerKwTo )
									$kw = $v->vehicleDetails->powerKwFrom . $this->t->t( "KW" ) . ", ";
								else
									$kw = $v->vehicleDetails->powerKwFrom . $this->t->t( "KW" ) . " - " . $v->vehicleDetails->powerKwTo . $this->t->t( "KW" ) . ", ";
								$ps = "";
								if( $v->vehicleDetails->powerHpFrom == $v->vehicleDetails->powerHpTo )
									$ps = $v->vehicleDetails->powerHpFrom . $this->t->t( "PS" ) . ", ";
								else
									$ps = $v->vehicleDetails->powerHpFrom . $this->t->t( "PS" ) . " - " . $v->vehicleDetails->powerHpTo . $this->t->t( "PS" ) . ", ";

								$ccm = "";
								if( isset( $v->vehicleDetails->cylinderCapacityCcm ) )
									$ccm = $v->vehicleDetails->cylinderCapacityCcm . $this->t->t( "ccm" ) . ", ";

								$fueltype = "";
								if( isset( $v->vehicleDetails->fuelType ) )
									$fueltype = $v->vehicleDetails->fuelType . ", " . $constype = "";
								if( isset( $v->vehicleDetails->constructionType ) )
									$constype = $v->vehicleDetails->constructionType;

								$arr2[] = array(
									"id" => $v->carId,
									"name" => $v->vehicleDetails->manuName . " " . $v->vehicleDetails->modelName . " " . $v->vehicleDetails->typeName . " (" . $from_to . $ccm . $kw . $ps . $fueltype . $constype . ")",
									"manufacturer" => $v->vehicleDetails->manuId,
									"model" => $v->vehicleDetails->modId
								);
							} // foreach
						/* foreach( $result->data->array as $k => $v ) {
						 * $arr2[] = array(
						 * "id" => $v->carId,
						 * "name" => $v->vehicleDetails->typeName." (".$this->ConvertYearToView( $v->vehicleDetails->yearOfConstrFrom ).")",
						 * "manufacturer" => $v->vehicleDetails->manuId,
						 * "model" => $v->vehicleDetails->modId
						 * );
						 * } // foreach */
					} // if
				} // foreach
			} // if
		} // if

		$this->f->array_sort_by_column( $arr2, "name" );

		return ($arr2);
	}

	function get_vehicle_by_fin( $fin ) {
		$arr = array();
		$arr[0] = array(
			"id" => 0,
			"name" => " ",
			"manufacturer" => 0,
			"model" => 0
		);

		$manufacturer = $this->web_api->get_fin( trim( $fin ) );

		if( isset( $manufacturer['Range'] ) ) {
			foreach( $manufacturer['Range'] as $key2 => $model ) {
				foreach( $model['Type'] as $key3 => $vehicle ) {
					$arr[] = array(
						"id" => $vehicle['TD_TypeID'],
						"name" => $manufacturer['MakeName'] . " " . $model['RangeName'] . " " . $vehicle['TypeName'] . " (" . $vehicle['Info'] . ")",
						"manufacturer" => $manufacturer['TD_MakeID'],
						"model" => $model['TD_RangeID']
					);
				} // foreach
			} // foreach

			$this->f->array_sort_by_column( $arr, "name" );
		} // if

		if( isset( $manufacturer['Error'] ) )
			$arr[] = array(
					"id" => -1,
					"name" => $manufacturer['Error'],
					"manufacturer" => '',
					"model" => ''
			);

		return ($arr);
	}

	function get_categories( $vehicle, $shortcut ) {
		$function = 'getChildNodesAllLinkingTarget2';
		$params = array(
			'linked' => true,
			'linkingTargetId' => $vehicle,
			'linkingTargetType' => 'P', // $this->get_search_linkingtargettype(), POMAKU
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'childNodes' => true,
			'provider' => $this->get_search_tecdocid()
		);
		if( $shortcut != 0 )
			$params['shortCutId'] = $shortcut;

		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		if( isset( $result->statusText ) && (strpos( $result->statusText, 'has an invalid value' ) !== false) ) {
			$function = 'getChildNodesAllLinkingTarget2';
			$params = array(
				'linked' => true,
				'linkingTargetId' => $vehicle,
				'linkingTargetType' => 'O',
				'articleCountry' => $this->get_search_articleCountry(),
				'lang' => $this->get_search_language(),
				'childNodes' => true,
				'provider' => $this->get_search_tecdocid()
			);
			if( $shortcut != 0 )
				$params['shortCutId'] = $shortcut;

			$request = $this->createRequest( $function, $params );
			$result = $this->callJSON( $function, $request );
		} // if

		$arr = array();
		if( isset( $result->data->array ) )
			foreach( $result->data->array as $k => $v ) {
				if( !isset( $v->parentNodeId ) )
					$v->parentNodeId = 0;

				$arr[$v->assemblyGroupNodeId] = array(
					"id" => $v->assemblyGroupNodeId,
					"parent" => $v->parentNodeId,
					"name" => $v->assemblyGroupName,
					"group" => $v->hasChilds
				);
			} // foreach

		return ($arr);
	}

	function get_categories_by_pattern( $pattern ) {
		$function = 'getGenericArticles';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$function = 'getArticleDirectSearchAllNumbersWithState';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'numberType' => 10,
			'articleNumber' => $pattern,
			'provider' => $this->get_search_tecdocid(),
			'searchExact' => false,
			'sortType' => 1
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr2 = array();
		if( isset( $result->data->array ) )
			foreach( $result->data->array as $k => $v ) {
				$arr2[$v->genericArticleId] = $v->masterDesignation;
			} // foreach

		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr[] = array(
			"id" => 0,
			"name" => ""
		);
		if( isset( $result->data->array ) )
			foreach( $result->data->array as $k => $v ) {

				$arr[] = array(
					"id" => $v->genericArticleId,
					"name" => $arr2[$v->genericArticleId]
				);
			} // foreach

		$this->f->array_sort_by_column( $arr, 'name' );

		return ($arr);
	}

	function get_categories_by_brand( $brand ) {
		$function = 'getChildNodesAllLinkingTarget2';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'linkingTargetType' => 'U',
			'provider' => $this->get_search_tecdocid(),
			'linked' => false,
			'childNodes' => true
		);

		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr[] = array(
			"id" => 0,
			"name" => ""
		);
		if( isset( $result->data->array ) )
			foreach( $result->data->array as $k => $v ) {
				if( !isset( $v->parentNodeId ) )
					$v->parentNodeId = 0;

				if( !$v->hasChilds )
					$arr[] = array(
						"id" => $v->assemblyGroupNodeId,
						"name" => $v->assemblyGroupName
					);
			} // foreach

		$this->f->array_sort_by_column( $arr, 'name' );

		return ($arr);
	}

	function get_shortcuts( $vehicle ) {
		$function = 'getShortCuts2';
		$params = array(
			'linked' => true,
			'linkingTargetId' => $vehicle,
			'linkingTargetType' => 'P',
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		if( isset( $result->statusText ) && (strpos( $result->statusText, 'has an invalid value' ) !== false) ) {
			$function = 'getShortCuts2';
			$params = array(
				'linked' => true,
				'linkingTargetId' => $vehicle,
				'linkingTargetType' => 'O',
				'articleCountry' => $this->get_search_articleCountry(),
				'lang' => $this->get_search_language(),
				'provider' => $this->get_search_tecdocid()
			);
			$request = $this->createRequest( $function, $params );
			$result = $this->callJSON( $function, $request );
		} // if

		$arr = array();
		$arr[0] = array(
			"name" => 'RESET',
			"pic" => $this->theme->get_shortcut_icon( 0 )
		);
		if( isset( $result->data->array ) )
			foreach( $result->data->array as $k => $v ) {
				$idBild = $v->shortCutId;
				if( $idBild > 18 )
					$idBild -= 17;
				$arr[$v->shortCutId] = array(
					"name" => $v->shortCutName,
					"pic" => $this->theme->get_shortcut_icon( $idBild )
				);
			} // foreach

		return ($arr);
	}

	function get_articles( $vehicle, $category, $manufacturer, $model, $type, $brand ) {

		function sort_object_stock( $a, $b ) {
			return $a->stock < $b->stock;
		}

		// sort_stock
		function sort_array_stock( $a, $b ) {
			return $a['stock'] < $b['stock'];
		} // sort_stock

		$arr_result_main = array();
		$arr = array();
		$function = 'getArticleIdsWithState';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'assemblyGroupNodeId' => $category,
			'linkingTargetId' => $vehicle,
			'linkingTargetType' => 'P', // $type,
			'lang' => $this->get_search_language(),
			'provider' => $this->get_search_tecdocid()
		);
		if( $brand > 0 )
			$params['brandNo'] = array(
				'array' => $brand
			);

		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		if( isset( $result->statusText ) && (strpos( $result->statusText, 'has an invalid value' ) !== false) ) {
			$function = 'getArticleIdsWithState';
			$params = array(
				'articleCountry' => $this->get_search_articleCountry(),
				'assemblyGroupNodeId' => $category,
				'linkingTargetId' => $vehicle,
				'linkingTargetType' => 'O',
				'lang' => $this->get_search_language(),
				'provider' => $this->get_search_tecdocid()
			);
			if( $brand > 0 )
				$params['brandNo'] = array(
					'array' => $brand
				);

			$request = $this->createRequest( $function, $params );
			$result = $this->callJSON( $function, $request );
		} // if

		// Lagerstand ins array speichern
		if( isset( $result->data->array ) ) {
			// alle Artikel laden und speichern
			$this->get_article_stock_price_all( $result->data->array );

			$result_list = $result->data->array;

			$item_list = array();
			$item_list_link_ids = array();
			foreach( $result_list as $k => $v ) {
				// Lagerstand und Preis holen
				$this->get_article_stock_price( $v->brandName, $v->articleNo, $r['lagerstand'], $price_intern, $brutto );

				if( !$this->field_with_value_in_array( $item_list, "articleNo", $v->articleNo, "brandName", $v->brandName ) ) {
					$item_list[] = array(
						"articleId" => $v->articleId,
						"articleNo" => $v->articleNo,
						"brandName" => $v->brandName,
						"stock" => $r['lagerstand']
					);
					$item_list_link_ids[$v->articleId] = array();
				} // if
				$item_list_link_ids[$v->articleId][] = $v->articleLinkId;
			} // foreach

			usort( $item_list, 'sort_array_stock' );

			$result1 = array();
			for( $j=0; $j<=$this->td_setup['additional_requests']; $j++) {
				$lines = sizeof( $item_list ) - ($j*25);
				if( $lines > 25 ) $lines = 25;

				if( $lines > 0 ) {
					$query_item_list = array();
					for( $i = 0; $i < $lines; $i++ ) {
						$query_item_list[$i] = $item_list[$i+($j*25)]['articleId'];
					} // for

					$function = 'getDirectArticlesByIds6';
					$params = array(
						'articleId' => array(
							'array' => $query_item_list
						),
						'articleCountry' => $this->get_search_articleCountry(),
						'lang' => $this->get_search_language(),
						'basicData' => false,
						'thumbnails' => true,
						'attributs' => true,
						'provider' => $this->get_search_tecdocid()
					);
					$request = $this->createRequest( $function, $params );
					$result = $this->callJSON( $function, $request );

					if( isset( $result->data->array ) )
						$result1 = array_merge( $result1, $result->data->array );
				} // if
			} // for
			$result->data->array = $result1;

			// Merge Lists
			foreach( $result->data->array as $k => $v ) {
				$key = $this->search_for_articleid( $v->directArticle->articleId, $result_list );

				$v->directArticle->articleId_orig = $result_list[$key]->articleId;
				$v->directArticle->articleNo = $result_list[$key]->articleNo;
				$v->directArticle->brandName = $result_list[$key]->brandName;
				$v->directArticle->articleName = $result_list[$key]->genericArticleName;
				$v->directArticle->manufacturer = $manufacturer;
				$v->directArticle->vehicle = $vehicle;
				$v->directArticle->link_ids = $item_list_link_ids[$result_list[$key]->articleId];
			} // foreach

			$arr = $this->get_articles_return( $result );
		} // if

		// Save Result
		foreach( $arr as $k => $v ) {
			if( !$this->field_with_value_in_array( $arr_result_main, "no", $v['no'], "brand", $v['brand'] ) )
				$arr_result_main[] = $v;
		} // foreach

		// $this->f->array_sort_by_column( $arr_result_main, 'stock', SORT_DESC );

		return ($arr_result_main);
	}

	function search_for_articleid( $id, $array ) {
		foreach( $array as $key => $val ) {
			if( $val->articleId === $id )
				return $key;
		} // foreach

		return (null);
	}

	function search_for_articleid2( $id, $array ) {
		foreach( $array as $key => $val ) {
			if( $val['id'] === $id )
				return $key;
		} // foreach

		return (null);
	}

	function debug( $str ) {
		if( $_SESSION['c_user_id'] == 1 )
			print_r( $str );
	}

	function field_with_value_in_array( $arr, $field1, $value1, $field2, $value2 ) {
		foreach( $arr as $k => $v ) {
			if( ($v[$field1] === $value1) && ($v[$field2] === $value2) )
				return (true);
		} // foreach

		return (false);
	}

	function get_articles_by_pattern( $pattern1, $pattern_type, $similar ) {

		function sort_object_stock( $a, $b ) {
			return $a->stock < $b->stock;
		}

		// sort_stock
		function sort_array_stock( $a, $b ) {
			return $a['stock'] < $b['stock'];
		} // sort_stock

		$userinfo = $this->f->load_user( $_SESSION['c_user_id'], "search_only_on_stock, search_show_attributes" );

		$pattern1 = strtoupper( $pattern1 );
		$pattern_search = array();

		// Eingabe
		$pattern_search[] = $pattern1;

		// Ersatznummer
		if( $this->db->query( "SELECT ersatznr FROM TEC_REPLACE_ITEMS WHERE UPPER(hersteller_nr)='" . strtoupper( $pattern1 ) . "' AND (ersatznr!=hersteller_nr)", "get_articles_by_pattern" ) ) {
			while( $this->db->isNext( "get_articles_by_pattern" ) ) {
				$r = $this->db->getNext( "get_articles_by_pattern" );

				if( !in_array( strtoupper( $r['ersatznr'] ), $pattern_search ) )
					$pattern_search[] = strtoupper( $r['ersatznr'] );
			} // while
		} // if

		// interne Nummer
		$setup = $this->f->load_setup( "TEC_SETUP" );
		if( $setup['show_internal_item_no'] == 1 ) {
			if( $this->db->query( "SELECT item_no FROM TEC_WEBSERVICE_MAPPING WHERE UPPER(local_item_no) LIKE '" . $pattern1 . "' AND (item_no!=local_item_no)" ) ) {
				$r = $this->db->getNext();

				if( !in_array( strtoupper( $r['item_no'] ), $pattern_search ) )
					$pattern_search[] = strtoupper( $r['item_no'] );
				// Ist das eine Ersatznummer?
				if( $this->db->query( "SELECT ersatznr FROM TEC_REPLACE_ITEMS WHERE UPPER(hersteller_nr)='" . strtoupper( $r['item_no'] ) . "' AND (ersatznr!=hersteller_nr)", "get_articles_by_pattern" ) ) {
					while( $this->db->isNext( "get_articles_by_pattern" ) ) {
						$r = $this->db->getNext( "get_articles_by_pattern" );

						if( !in_array( strtoupper( $r['ersatznr'] ), $pattern_search ) )
							$pattern_search[] = strtoupper( $r['ersatznr'] );
					} // while
				} // if
			} // if
		} // if

		// DataConnector
		if( $this->db->query( "SELECT verbund_nr FROM TEC_DATACONNECTOR WHERE UPPER(hersteller_nr)='" . strtoupper( $pattern1 ) . "' AND (verbund_nr!=hersteller_nr)", "get_articles_by_pattern" ) ) {
			while( $this->db->isNext( "get_articles_by_pattern" ) ) {
				$r = $this->db->getNext( "get_articles_by_pattern" );

				if( !in_array( strtoupper( $r['verbund_nr'] ), $pattern_search ) )
					$pattern_search[] = strtoupper( $r['verbund_nr'] );
			} // while
		} // if
		if( $this->db->query( "SELECT hersteller_nr FROM TEC_DATACONNECTOR WHERE UPPER(verbund_nr)='" . strtoupper( $pattern1 ) . "' AND (hersteller_nr!=verbund_nr)", "get_articles_by_pattern" ) ) {
			while( $this->db->isNext( "get_articles_by_pattern" ) ) {
				$r = $this->db->getNext( "get_articles_by_pattern" );

				if( !in_array( strtoupper( $r['hersteller_nr'] ), $pattern_search ) )
					$pattern_search[] = strtoupper( $r['hersteller_nr'] );
			} // while
		} // if

		$arr_result_main = array();
		$result1 = array();
		foreach( $pattern_search as $k => $pattern ) {
			if( $pattern != "" ) {
				$arr = array();
				$function = 'getArticleDirectSearchAllNumbersWithState';
				$params = array(
					'articleCountry' => $this->get_search_articleCountry(),
					'lang' => $this->get_search_language(),
					'numberType' => $pattern_type,
					'articleNumber' => $pattern,
					'provider' => $this->get_search_tecdocid(),
					'searchExact' => (($similar == 1)?false:true),
					'sortType' => 1
				);
				$request = $this->createRequest( $function, $params );
				$result = $this->callJSON( $function, $request );

				if( isset( $result->data->array ) )
					$result1 = array_merge( $result1, $result->data->array );
			} // if
		} // foreach

		// Lagerstand ins array speichern
		if( sizeof( $result1 ) > 0 ) {
			// alle Artikel laden und speichern
			$this->get_article_stock_price_all( $result1 );

			$result_list = $result1;

			$item_list = array();
			foreach( $result_list as $k => $v ) {
				// Lagerstand und Preis holen
				$this->get_article_stock_price( $v->brandName, $v->articleNo, $r['lagerstand'], $price_intern, $brutto );

				// Suchpattern immer anzeigen, Ersatzartikel (nach dem gesucht wurde) könnte Lagerstand haben
				if( in_array( $v->articleNo, $pattern_search ) ) {
					if( $r['lagerstand'] == 0 )
						$r['lagerstand'] = 0.1;
				} // if

				if( !$this->field_with_value_in_array( $item_list, "articleNo", $v->articleNo, "brandName", $v->brandName ) )
					$item_list[] = array(
						"articleId" => $v->articleId,
						"articleNo" => $v->articleNo,
						"brandName" => $v->brandName,
						"stock" => $r['lagerstand']
					);
			} // foreach

			usort( $item_list, 'sort_array_stock' );

			$result1 = array();
			for( $j=0; $j<=$this->td_setup['additional_requests']; $j++) {
				$lines = sizeof( $item_list ) - ($j*25);
				if( $lines > 25 ) $lines = 25;

				if( $lines > 0 ) {
					$query_item_list = array();
					for( $i = 0; $i < $lines; $i++ ) {
						$query_item_list[$i] = $item_list[$i+($j*25)]['articleId'];
					} // for

					$function = 'getDirectArticlesByIds6';
					$params = array(
						'articleId' => array(
							'array' => $query_item_list
						),
						'articleCountry' => $this->get_search_articleCountry(),
						'lang' => $this->get_search_language(),
						'basicData' => false,
						'thumbnails' => true,
						'attributs' => true,
						'provider' => $this->get_search_tecdocid()
					);
					$request = $this->createRequest( $function, $params );
					$result = $this->callJSON( $function, $request );

					if( isset( $result->data->array ) )
						$result1 = array_merge( $result1, $result->data->array );
				} // if
			} // for
			$result->data->array = $result1;

			// Merge Lists
			foreach( $result->data->array as $k => $v ) {
				$key = $this->search_for_articleid( $v->directArticle->articleId, $result_list );

				$v->directArticle->articleId_orig = $result_list[$key]->articleId;
				$v->directArticle->articleNo = $result_list[$key]->articleNo;
				$v->directArticle->brandName = $result_list[$key]->brandName;
				$v->directArticle->articleName = $result_list[$key]->articleName;
				$v->directArticle->manufacturer = 0;
				$v->directArticle->vehicle = 0;
				$v->directArticle->link_ids = array();
			} // foreach

			$arr = $this->get_articles_return( $result, $userinfo['search_show_attributes'] );
		} // if

		// Save Result
		foreach( $arr as $k => $v ) {
			if( !$this->field_with_value_in_array( $arr_result_main, "no", $v['no'], "brand", $v['brand'] ) ) {
				$arr_result_main[] = $v;
//				$arr_result_main[$k]['brand'] = $arr_result_main[$k]['brand'];
			} // if
		} // foreach

		$this->f->array_sort_by_column( $arr_result_main, 'stock', SORT_DESC );

		// nur verfügbare Artikel anzeigen?
		if( $userinfo['search_only_on_stock'] == 1 ) {
			for( $i=0; $i<sizeof( $arr_result_main ); $i++ ) {
				if( $this->ConvertQuantityToDB( $arr_result_main[$i]['stock'] ) <= 0 ) {
					array_splice( $arr_result_main, $i, (count($arr_result_main) - $i));
					break;
				} // if
			} // for
		} // if

		return ($arr_result_main);
	}

	function get_articles_return( $result, $search_show_attributes = 0 ) {
		global $c_user_id;
		global $_location;

		$setup = $this->f->load_setup( "TEC_SETUP" );

		$arr = array();
		foreach( $result->data->array as $k => $v ) {
			if( $this->db->query( "SELECT quantity FROM TEC_WARENKORB WHERE user_id='" . $c_user_id . "' AND articleId='" . $v->directArticle->articleId . "' AND bestell_id='0'" ) )
				$r = $this->db->getNext();
			else
				$r['quantity'] = 0;

			$this->get_article_infos( $v->directArticle->brandName, $v->directArticle->articleNo, $auf_lager, $price_intern, $auf_lager_text, $action_button, $ampel, $brutto );

			// Attribute
			$text2 = '';
			$text3 = '';
			$position = '';
			if( isset( $v->articleAttributes->array ) ) {
				$text2 .= '<table class=\'table table-striped table-sm\'>';
				foreach( $v->articleAttributes->array as $k2 => $v2 ) {
					if( isset( $v2->attrValue ) ) {
						$text2 .= '<tr><th>' . $v2->attrName . '</th><td>' . htmlspecialchars( $v2->attrValue ) . '</td></tr>';
						if( $search_show_attributes == 1 ) $text3 .= $v2->attrName.': '.htmlspecialchars( $v2->attrValue ).', ';

						if( ($v2->attrName == "Einbauseite") || ($v2->attrName == "Strana ugradnje") ) {
							$position = $v2->attrValue;
						} // if
					} // if
				} // foreach
				$text2 .= '</table>';
			} // if
			if( $text3 != "" ) $text3 = "<br>".substr( $text3, 0, -2);

			// Nur internen Preis andrucken
			$price = $price_intern * 100;

			$picture = "";
			if( isset( $v->articleThumbnails->array[0]->thumbDocId ) )
				$picture = $this->td_setup['td_image_url'] . $this->get_search_tecdocid() . '/' . $v->articleThumbnails->array[0]->thumbDocId . '/0';

			$bemerkung = "";
			if( $this->db->query( "SELECT bemerkung, zusatzinfo FROM TEC_ITEMS WHERE UPPER(hersteller)='" . $this->fix_brandname( $v->directArticle->brandName ) . "' AND UPPER(hersteller_nr)='" . $v->directArticle->articleNo . "'" ) ) {
				$bem = $this->db->getNext();

				if( $bem['bemerkung'] != '' )
					$bemerkung .= '<br><span class=red>' . $bem['bemerkung'] . '</span>';
				if( $bem['zusatzinfo'] != '' )
					$bemerkung .= '<br>' . $bem['zusatzinfo'];
			} // if

			$locations = array();
			if( isset( $this->arr_all_items[$this->get_brand_id( strtoupper( $v->directArticle->brandName ) ) . "_" . $v->directArticle->articleNo]['locations'] ) ) {
				$locations = $this->arr_all_items[$this->get_brand_id( strtoupper( $v->directArticle->brandName ) ) . "_" . $v->directArticle->articleNo]['locations'];
				foreach( $locations as $k_location => $v_location ) {
					$locations[$k_location]['title_price'] = $this->get_tooltip_price( $v_location['netto'], $v_location['brutto'] );
				} // foreach
			} // if

			if( /*($auf_lager > 0) &&*/ ($v->directArticle->vehicle > 0) && ($v->directArticle->manufacturer > 0) && (sizeof( $v->directArticle->link_ids ) > 0) ) {
				$bemerkung .= '<br>' . $this->get_linked_attributes( $v->directArticle->articleId, $v->directArticle->vehicle, $v->directArticle->manufacturer, $v->directArticle->link_ids );
			} // if
			/* if( $_location == "qnap_home" ) {
			 * $locations = array(
			 * 1 => array(
			 * "id" => 1,
			 * "name" => "1",
			 * "quantity" => 1,
			 * "brutto" => 7.89,
			 * "netto" => 7.89,
			 * "sub_shop" => 0
			 * ),
			 * 2 => array(
			 * "id" => 2,
			 * "name" => "2",
			 * "quantity" => 2,
			 * "brutto" => 7.89,
			 * "netto" => 7.89,
			 * "sub_shop" => 1
			 * )
			 * );
			 * $auf_lager = 100;
			 * $action_button = $this->f->get_button( 'Kaufen Grün' );
			 * $ampel = $this->theme->get_icon_path( 'kreis_green' );
			 * $price = 2.34 * 100;
			 * } // if */
			$temp_arr = array(
				"id" => $v->directArticle->articleId,
				"link_id" => 0,
				"no" => $v->directArticle->articleNo,
				"internal_no" => $this->get_internal_item_no( $v->directArticle->articleNo, $v->directArticle->articleNo, $this->get_brand_id( $v->directArticle->brandName ) ),
				"brand" => $v->directArticle->brandName,
				"brand_id" => $this->get_brand_id( $v->directArticle->brandName ),
				"name" => $v->directArticle->articleName,
				"name_internal" => $this->get_internal_description( $v->directArticle->articleName, $v->directArticle->articleNo, $this->get_brand_id( $v->directArticle->brandName ) ),
					"bemerkung" => $bemerkung.$text3,
				"text2" => $text2,
				"price" => $this->ConvertPriceToView( $price ),
				"title_price" => $this->get_tooltip_price( $price / 100, $brutto ),
				"price_intern" => $this->ConvertQuantityToView( $price_intern ),
				"in_order" => $this->ConvertQuantityToView( $r['quantity'] ), // Bereits in Warenkorb
				"stock" => $this->ConvertQuantityToView( $auf_lager ), // auf Lager
				"stock_text" => $auf_lager_text,
				"picture" => $picture,
				"ampel" => $ampel,
				"button_buy" => $action_button,
				"replace_id" => 0,
				"position" => $position,
				"locations" => $locations
			);
			$arr[] = $temp_arr;

			// Ersatznummer
			$repArr = $this->get_replace_number( $v->directArticle->brandName, $v->directArticle->articleNo );
			if( sizeof( $repArr ) > 0 ) {
				foreach( $repArr as $k2 => $v2 ) {
					$temp_arr['no'] = $v2['no'] . ' (*)'; // '.$v->directArticle->articleNo.'
					$temp_arr['brand'] = $v2['brand'];
					$temp_arr['brand_id'] = $this->get_brand_id( $v2['brand'] );
					$temp_arr['stock'] = $v2['stock'];
					$temp_arr['stock_text'] = $v2['stock_text'];
					$temp_arr['price_intern'] = $v2['price_intern'];
					$temp_arr['price'] = $v2['price'];
					$temp_arr['title_price'] = $v2['title_price'];
					$temp_arr['ampel'] = $v2['ampel'];
					$temp_arr['button_buy'] = $v2['action_button'];
					$temp_arr['replace_id'] = $v2['replace_id'];
					$temp_arr['in_order'] = $v2['in_order'];
					$temp_arr['internal_no'] = $this->get_internal_item_no( $v2['no'], $v2['no'], $this->get_brand_id( $v2['brand'] ) );
					$temp_arr['name_internal'] = $this->get_internal_description( $v2['no'], $v2['no'], $this->get_brand_id( $v2['brand'] ) );
					$temp_arr['locations'] = $v2['locations'];

					// Bild Ersatzartikel
					$bFound = false;
					if( $this->db->query( "SELECT item_picture FROM TEC_ITEMS WHERE hersteller='" . $v2['brand'] . "' AND hersteller_nr='" . $v2['no'] . "'" ) ) {
						$r = $this->db->getNext();
						if( file_exists( BASE_DIR . 'upload/' . $_location . '/univ_items/' . $r['item_picture'] . '.png' ) ) {
							$picture_path = DOMAIN . 'upload/' . $_location . '/univ_items/' . $r['item_picture'] . '.png';

							$temp_arr['picture'] = $picture_path;
							$bFound = true;
						} // if
					} // if
					if( !$bFound && ($setup['copy_picture_from_original'] == 0) )
						$temp_arr['picture'] = "";

					$arr[] = $temp_arr;
				} // foreach
			} // if
		} // foreach

		$this->f->array_sort_by_column( $arr, 'stock', SORT_DESC );

		return ($arr);
	}

	function get_linked_attributes( $articleId, $vehicle, $manufacturer, $link_ids ) {
		$arr = array();
		foreach( $link_ids as $k => $v ) {
			$function = 'getArticleLinkedAllLinkingTargetsByIds3';
			$params2 = array(
				'articleCountry' => $this->get_search_articleCountry(),
				'articleId' => $articleId,
				'lang' => $this->get_search_language(),
				'immediateAttributs' => true,
				'linkingTargetType' => $this->get_search_linkingtargettype(),
				'provider' => $this->get_search_tecdocid(),
				'linkedArticlePairs' => array(
					'array' => array( array(
						'articleLinkId' => $v,
						'linkingTargetId' => $vehicle
					)
				) )
			);
			$request2 = $this->createRequest( $function, $params2 );
			$result2 = $this->callJSON( $function, $request2 );

			if( isset( $result2->data->array[0]->linkedArticleImmediateAttributs->array ) ) {
				foreach( $result2->data->array[0]->linkedArticleImmediateAttributs->array as $k2 => $v2 ) {
					if( isset( $v2->attrName ) && isset( $v2->attrValue ) /*&&
					(($v2->attrId == 20) || ($v2->attrId == 21) || ($v2->attrId == 25) || ($v2->attrId == 26))*/
							) {
						if( !isset( $arr[$v2->attrName] ) )
							$arr[$v2->attrName] = array();
						if( !in_array( $v2->attrValue, $arr[$v2->attrName] ) )
							$arr[$v2->attrName][] = $v2->attrValue;
					} // if
				} // foreach
			} // if
		} // foreach

		$str = '';
		foreach( $arr as $k => $v ) {
			asort( $arr[$k] );
			if( $str != '' )
				$str .= '<br>';
			$str .= $k . ': ' . implode( ', ', $v );
		} // foreach

		return ($str);
	}

	function get_linked_attributes_old( $articleId, $vehicle, $manufacturer ) {
		$function = 'getArticleLinkedAllLinkingTarget3';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'articleId' => $articleId,
			'lang' => $this->get_search_language(),
			'linkingTargetId' => $vehicle,
			'linkingTargetManuId' => $manufacturer,
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		foreach( $result->data->array[0]->articleLinkages->array as $k => $v ) {
			if( true || ($v->linkingTargetId == $vehicle) ) {
				$function = 'getArticleLinkedAllLinkingTargetsByIds3';
				$params2 = array(
					'articleCountry' => $this->get_search_articleCountry(),
					'articleId' => $articleId,
					'lang' => $this->get_search_language(),
					'immediateAttributs' => true,
					'linkingTargetType' => $this->get_search_linkingtargettype(),
					'provider' => $this->get_search_tecdocid(),
					'linkedArticlePairs' => array(
						'array' => array(
							'articleLinkId' => $v->articleLinkId,
							'linkingTargetId' => $v->linkingTargetId
						)
					)
				);
				$request2 = $this->createRequest( $function, $params2 );
				$result2 = $this->callJSON( $function, $request2 );

				if( isset( $result2->data->array[0]->linkedArticleImmediateAttributs->array ) ) {
					foreach( $result2->data->array[0]->linkedArticleImmediateAttributs->array as $k2 => $v2 ) {
						if( isset( $v2->attrName ) && isset( $v2->attrValue ) && (($v2->attrId == 20) || ($v2->attrId == 21) || ($v2->attrId == 25) || ($v2->attrId == 26)) ) {
							if( !isset( $arr[$v2->attrName] ) )
								$arr[$v2->attrName] = array();
							if( !in_array( $v2->attrValue, $arr[$v2->attrName] ) )
								$arr[$v2->attrName][] = $v2->attrValue;
						} // if
					} // foreach
				} // if
			} // if
		} // foreach

		$str = '';
		foreach( $arr as $k => $v ) {
			asort( $arr[$k] );
			if( $str != '' )
				$str .= '<br>';
			$str .= $k . ': ' . implode( ', ', $v );
		} // foreach

		return ($str);
	}

	function get_replace_number( $brand, $articleno ) {
		$arr3 = array();

		if( $this->db->query( "SELECT id, hersteller_nr, hersteller FROM TEC_REPLACE_ITEMS WHERE UPPER(ersatznr)='" . strtoupper( $articleno ) . "' AND UPPER(hersteller_ersatznr)='" . strtoupper( $brand ) . "'", "get_replace_number" ) ) {
			$setup = $this->f->load_setup( "TEC_SETUP" );

			while( $this->db->isNext( "get_replace_number" ) ) {
				$r = $this->db->getNext( "get_replace_number" );

				$r['hersteller_nr'] = strtoupper( $r['hersteller_nr'] );
				$r['hersteller'] = strtoupper( $r['hersteller'] );

				$this->get_article_infos( $r['hersteller'], $r['hersteller_nr'], $auf_lager, $price_intern, $auf_lager_text, $action_button, $ampel, $brutto );
				$price = $price_intern * 100;

				if( $this->db->query( "SELECT quantity FROM TEC_WARENKORB WHERE user_id='" . $_SESSION['c_user_id'] . "' AND replace_id='" . $r['id'] . "'", 2 ) )
					$r2 = $this->db->getNext( 2 );
				else
					$r2['quantity'] = 0;

				$locations = array();
				if( isset( $this->arr_all_items[$this->get_brand_id( strtoupper( $r['hersteller'] ) ) . "_" . $r['hersteller_nr']]['locations'] ) )
					$locations = $this->arr_all_items[$this->get_brand_id( strtoupper( $r['hersteller'] ) ) . "_" . $r['hersteller_nr']]['locations'];

				$arr3[] = array(
					"no" => $r['hersteller_nr'],
					"brand" => $r['hersteller'],
					"stock" => $this->ConvertQuantityToView( $auf_lager ),
					"stock_text" => $auf_lager_text,
					"price_intern" => $this->ConvertQuantityToView( $price_intern ),
					"price" => $this->ConvertPriceToView( $price ),
					"title_price" => $this->get_tooltip_price( $price / 100, $brutto ),
					"action_button" => $action_button,
					"ampel" => $ampel,
					"replace_id" => $r['id'],
					"in_order" => $r2['quantity'],
					"locations" => $locations
				);
			} // while
		} // if

		return ($arr3);
	}

	function get_tooltip_price( $netto, $brutto ) {
		$str =
			$this->t->t( "Netto-Preis" ) . ': ' . $this->ConvertPriceToView( 100 * $netto ) . '<br />' .
			$this->t->t( "Brutto-Preis" ) . ': ' . $this->ConvertPriceToView( 100 * $netto * (1 + $this->get_vat() / 100) ) . '<br />' .
			$this->t->t( "Normal-Preis" ) . ': ' . $this->ConvertPriceToView( 100 * $brutto ) . '<br />' .
			$this->t->t( "Brutto (inkl. Ust.)" ) . ': ' . $this->ConvertPriceToView( 100 * $brutto * (1 + $this->get_vat() / 100) );

		return( $str );
	}

	function get_article_stock_price_all( $arr_items ) {
		$this->arr_all_items = array();

		$setup = $this->f->load_setup( "TEC_SETUP " );
		if( $setup['enable_topmotive_api'] == 1 ) {
			$arr = array();
			foreach( $arr_items as $k => $v ) {
				if( !$this->field_with_value_in_array( $arr, "item_id", $v->articleNo, "brand_id", $this->get_brand_id( $v->brandName ) ) )
					$arr[] = array(
						"item_id" => $v->articleNo,
						"brand_id" => $this->get_brand_id( $v->brandName )
					);

					// Ersatzartikel auch gleich abholen
					if( $this->db->query( "SELECT id, hersteller_nr, hersteller FROM TEC_REPLACE_ITEMS WHERE UPPER(ersatznr)='" . strtoupper( $v->articleNo ) . "' AND UPPER(hersteller_ersatznr)='" . strtoupper( $v->brandName ) . "'", "get_replace_number" ) ) {
						while( $this->db->isNext( "get_replace_number" ) ) {
							$r = $this->db->getNext( "get_replace_number" );

							if( !$this->field_with_value_in_array( $arr, "item_id", strtoupper( $r['hersteller_nr'] ), "brand_id", $this->get_brand_id( strtoupper( $r['hersteller'] ) ) ) )
								$arr[] = array(
									"item_id" => strtoupper( $r['hersteller_nr'] ),
									"brand_id" => $this->get_brand_id( strtoupper( $r['hersteller'] ) )
							);
						} // while
					} // if
			} // foreach

			$this->arr_all_items = $this->web_api->get_topmotive_sum_infos_all( $arr );
		} // if
	}

	function get_article_stock_price( $brand, $articleno, &$auf_lager, &$price_intern, &$brutto, $for_location_code = 0 ) {
		if( isset( $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno] ) ) {
			$auf_lager = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['auf_lager'];
			$price_intern = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['price_intern'];
			$brutto = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['brutto'];
		} else {
			$setup = $this->f->load_setup( "TEC_SETUP " );

			$auf_lager = 0;
			$price_intern = 0;
			$brutto = 0;
			$locations = array();

			if( $setup['enable_topmotive_api'] == 1 ) {
				//echo "Single Abfrage: ".$articleno." ";
				//$this->web_api->get_topmotive_sum_infos( $this->get_brand_id( strtoupper( $brand ) ), $articleno, $auf_lager, $price_intern, $brutto, $for_location_code );
				// Neue Abfrage inkl. Lagerorte -> hier darf man eigentlich nie herkommen...
				$arr = array();
				$arr[] = array(
						"item_id" => $articleno,
						"brand_id" => $this->get_brand_id( strtoupper( $brand ) )
				);
				$this->arr_all_items = $this->web_api->get_topmotive_sum_infos_all( $arr );

				$auf_lager = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['auf_lager'];
				$price_intern = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['price_intern'];
				$brutto = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['brutto'];
				$locations = $this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['locations'];
			} else {
				if( $this->db->query( "
					SELECT lagerstand, preis
					FROM TEC_ITEMS
					WHERE UPPER(hersteller)='" . $this->fix_brandname( strtoupper( $brand ) ) . "' AND UPPER(hersteller_nr)='" . strtoupper( $articleno ) . "'", "get_replace_number2" ) ) {
					$r2 = $this->db->getNext( "get_replace_number2" );
					if( $r2['lagerstand'] == 0 )
						$r2['lagerstand'] = 0.1;
					$auf_lager = $r2['lagerstand'];
					$price_intern = $r2['preis'];
				} // if
			} // else

			// Speichern, falls 2. Anfrage kommt
			$this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['auf_lager'] = $auf_lager;
			$this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['price_intern'] = $price_intern;
			$this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['brutto'] = $brutto;
			$this->arr_all_items[$this->get_brand_id( strtoupper( $brand ) ) . "_" . $articleno]['locations'] = $locations;
		} // else
	}

	function get_article_infos( $brand, $articleno, &$auf_lager, &$price_intern, &$auf_lager_text, &$action_button, &$ampel, &$brutto ) {
		$userinfo = $this->f->load_user( $_SESSION['c_user_id'], "user_level" );

		// Lagerstand und Preis holen
		$this->get_article_stock_price( $brand, $articleno, $auf_lager, $price_intern, $brutto );

		if( $userinfo['user_level'] >= 100 )
			$auf_lager_text = $this->f->fn( round( $auf_lager, 0 ), 0 );
		else {
			if( $auf_lager > 0.1 )
				$auf_lager_text = $this->t->t( "ja" );
			else
				$auf_lager_text = $this->t->t( "nein" );
			$auf_lager_text = "";
		} // else

		$action_button = '';
		$ampel = '';
		if( $auf_lager == 0.1 ) {
			$action_button = $this->f->get_button( 'Kaufen Rot' );
			$ampel = '<i class="fas fa-times-circle text-danger"></i> ';
		} // if
		if( $auf_lager > 0.1 ) {
			$action_button = $this->f->get_button( 'Kaufen Grün' );
			$ampel = '<i class="fas fa-check-circle text-success"></i> ';
		} // if
	}

	function get_article( $articleLinkId, $articleId, $manufacturer, $model, $vehicle ) {
		if( $articleLinkId != 0 ) { // $articleId ) {
			$function = 'getAssignedArticlesByIds6';
			$params = array(
				'articleCountry' => $this->get_search_articleCountry(),
				'lang' => $this->get_search_language(),
				'provider' => $this->get_search_tecdocid(),
				'articleIdPairs' => array(
					'array' => array(
						'articleId' => $articleId,
						'articleLinkId' => $articleLinkId
					)
				),

				'manuId' => $manufacturer,
				'modId' => $model,
				'linkingTargetId' => $vehicle,
				'linkingTargetType' => $this->get_search_linkingtargettype(),

				'basicData' => true,
				'attributs' => true,
				'documents' => true,
				'immediateAttributs' => true,
				'oeNumbers' => true,
				'thumbnails' => true,
				'replacedByNumbers' => true,
				'replacedNumbers' => true,
				'usageNumbers' => true,
				'immediateInfo' => true,
				'documents' => true,
				'mainArticles' => true,
				'eanNumbers' => true,
				/* 'attributs' => true,
			 * 'basicData' => true,
			 * 'immediateAttributs' => true,
			 * 'info' => true,
			 * 'normalAustauschPrice' => true,
			 * 'oeNumbers' => true,
			 * 'prices' => true,
			 * 'thumbnails' => true, */
			);
		} else {
			$function = 'getDirectArticlesByIds6';
			$params = array(
				'articleCountry' => $this->get_search_articleCountry(),
				'lang' => $this->get_search_language(),
				'provider' => $this->get_search_tecdocid(),

				'articleId' => array(
					'array' => array(
						$articleId
					)
				),
				'basicData' => true,
				'attributs' => true,
				'documents' => true,
				'immediateAttributs' => true,
				'oeNumbers' => true,
				'thumbnails' => true,
				'replacedByNumbers' => true,
				'replacedNumbers' => true,
				'usageNumbers' => true,
				'immediateInfo' => true,
				'documents' => true,
				'mainArticles' => true,
				'eanNumbers' => true,
			);
		} // else
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = json_decode( json_encode( $result->data->array[0] ), true );

		if( $articleLinkId == 0 )
			$arr['assignedArticle'] = $arr['directArticle'];

		return ($arr);
	}

	function get_article_price( $articleLinkId, $articleId, $manufacturer, $model, $vehicle ) {
		$function = 'getAssignedArticlesByIds6';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'articleIdPairs' => array(
				'array' => array(
					'articleId' => $articleId,
					'articleLinkId' => $articleLinkId
				)
			),
			'lang' => $this->get_search_language(),
			'linkingTargetId' => $vehicle,
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'manuId' => $manufacturer,
			'modId' => $model,
			'normalAustauschPrice' => true,
			'prices' => true,
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		if( !isset( $result->data->array[0]->articlePrices->array[0]->price ) )
			$price = 0;
		else
			$price = $result->data->array[0]->articlePrices->array[0]->price;

		return ($price);
	}

	// ----------------------------------------------------------------------------------------
	// BUY FUNCTION
	// ----------------------------------------------------------------------------------------
	function add_item_to_shopping( $articleLinkId, $articleId, $quantity, $manufacturer, $model, $vehicle, $brand, $brand_id, $item_no, $replace_id, $location_id, $location_name, $sub_shop ) {
		global $c_user_id;

		$setup = $this->f->load_setup( "TEC_SETUP" );

		// echo $articleId."...";

		if( $replace_id > 0 ) {
			$this->db->query( "SELECT hersteller, hersteller_nr FROM TEC_REPLACE_ITEMS WHERE id='" . $replace_id . "'" );
			$r = $this->db->getNext();
			/* strpos( $item_no, ' (*)' ) > 0 ) {
			 * $itemno = str_replace( ' (*)', '', $item_no ); */
			// Ersatzartikel
			$this->db->query( "SELECT id FROM TEC_ITEMS WHERE UPPER(hersteller_nr)='" . $this->fix_brandname( $r['hersteller_nr'] ) . "' AND UPPER(hersteller)='" . strtoupper( $r['hersteller'] ) . "'" );
			$r = $this->db->getNext();

			$articleId = -$r['id'];
		} // if

		// Hole Artikel Details
		if( $articleId > 0 ) {
			$article = $this->get_article( $articleLinkId, $articleId, $manufacturer, $model, $vehicle );

			// Lagerstand und Preis holen
			$this->get_article_stock_price( $brand, $article['assignedArticle']['articleNo'], $auf_lager, $price_intern, $brutto, $location_id );
		} else {
			// Ersatzartikel
			$this->db->query( "SELECT hersteller, hersteller_nr, preis, lagerstand, 0 AS brutto, bemerkung FROM TEC_ITEMS WHERE id='" . -$articleId . "'", "add_item_to_shopping" );
			$r2 = $this->db->getNext( "add_item_to_shopping" );

			// Lagerstand und Preis holen
			if( $setup['enable_topmotive_api'] == 1 ) {
				$this->get_article_stock_price( $r2['hersteller'], $r2['hersteller_nr'], $r2['lagerstand'], $r2['preis'], $r2['brutto'], $location_id );
			} // if
			$auf_lager = $r2['lagerstand'];
			$price_intern = $r2['preis'];
			$brutto = $r2['brutto'];

			$article['assignedArticle']['articleNo'] = $r2['hersteller_nr'];
			$article['assignedArticle']['articleName'] = $this->t->t( 'Artikel' ) . ' ' . strtoupper( $r2['hersteller_nr'] ) . ' (' . strtoupper( $r2['hersteller'] ) . ') - ' . $r2['bemerkung'];
		} // else

		if( ($location_name != '') && (strrpos( $location_name, '(' ) > 2) )
			$location_name = substr( $location_name, 0, strrpos( $location_name, '(' ) - 2 );

		$this->db->delete( "TEC_WARENKORB", "user_id='" . $c_user_id . "' AND articleId='" . $articleId . "' AND location_id='" . $location_id . "' AND bestell_id='0'" );
		$this->db->insert( "TEC_WARENKORB", array(
			"user_id" => $c_user_id,
			"articleLinkId" => $articleLinkId,
			"articleId" => $articleId,
			"replace_id" => $replace_id,
			"brand" => strtoupper( $brand ),
			"brand_id" => $brand_id,
			"manufacturer" => $manufacturer,
			"model" => $model,
			"vehicle" => $vehicle,
			"article_no" => strtoupper( $article['assignedArticle']['articleNo'] ),
			"article_name" => $article['assignedArticle']['articleName'],
			"article_price" => $price_intern,
			"article_price_brutto" => round( $price_intern * (1 + $this->get_vat() / 100), 2 ),
			"currency_code" => $setup['currency_code'],
			"bestellte_menge" => $this->ConvertQuantityToDB( $quantity ),
			"quantity" => $this->ConvertQuantityToDB( $quantity ),
			"amount" => $price_intern * $this->ConvertQuantityToDB( $quantity ),
			"amount_brutto" => round( $price_intern * (1 + $this->get_vat() / 100), 2 ) * $this->ConvertQuantityToDB( $quantity ),
			"location_id" => $location_id,
			"location_name" => $location_name,
			"sub_shop" => $sub_shop
		) );
		$this->db->commit();

		return ($this->t->t( 'Zum Warenkorb wurde hinzugefügt' ) . ': <strong>' . $quantity . '</strong> ' . $this->t->t( 'Stück' ) . ' <strong>' . strtoupper( $article['assignedArticle']['articleNo'] ) . '-' . strtoupper( $brand ) . '</strong>');
	}

	// ----------------------------------------------------------------------------------------
	// ARTICLE POPUP
	// ----------------------------------------------------------------------------------------
	function form_article_detail( $param ) {
		$articleLinkId = $param['0'];
		$articleId = $param['1'];
		$manufacturer = $param['2'];
		$model = $param['3'];
		$vehicle = $param['4'];
		$brand = $param['5'];
		$replace_id = $param['6'];

		$show_register = true;
		if( ($replace_id > 0) && ($this->td_setup['copy_picture_from_original'] == 0) )
			$show_register = false;

		$art = $this->get_article( $articleLinkId, $articleId, $manufacturer, $model, $vehicle );

		$strOut = '';
		$tabs = array();
		$lines = array();
		$current_tab = 0;

		// Attribute
		if( isset( $art['articleAttributes']['array'] ) ) {
			$current_tab++;
			$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "Attribute" ) . '</a></li>';

			$lines[$current_tab] = '<table class="table table-striped table-sm">';
			foreach( $art['articleAttributes']['array'] as $k => $v ) {
				if( isset( $v['attrValue'] ) )
					$lines[$current_tab] .= '<tr><th>' . $v['attrName'] . '</th><td>' . $v['attrValue'] . '</td></tr>';
			} // foreach

			// EAN Nummer
			if( isset( $art['eanNumber']['array']['0']['eanNumber'] ) ) {
				$lines[$current_tab] .= '<tr><th>' . $this->t->t( "EAN Nummer" ) . '</th><td>';
				foreach( $art['eanNumber']['array'] as $k => $v ) {
					$lines[$current_tab] .= $v['eanNumber'].' ';
				} // foreach
				$lines[$current_tab] .= '</td></tr>';
			} // if

			$lines[$current_tab] .= '</table>';
		} // if

		// Artikel
		if( $show_register ) {
			$current_tab++;
			$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "Artikel" ) . '</a></li>';

		$lines[$current_tab] = '<table class="table table-striped table-sm">';
			$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Artikelname" ) . '</th><td>' . $art['assignedArticle']['articleName'] . '</td></tr>';

			// Ersetzt durch
			if( isset( $art['replacedByNumber']['array'] ) ) {
				$str = '';
				foreach( $art['replacedByNumber']['array'] as $k => $v ) {
					if( $str != '' )
						$str .= ', ';
					$str .= $v['replaceNumber'];
				} // foreach

				$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Ersetzt durch" ) . '</th><td>' . $str . '</td></tr>';
			} // if

			// Ersetzt durch
			if( isset( $art['replacedNumber']['array'] ) ) {
				$str = '';
				foreach( $art['replacedNumber']['array'] as $k => $v ) {
					if( $str != '' )
						$str .= ', ';
					$str .= $v['replaceNumber'];
				} // foreach

				$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Ersatz für" ) . '</th><td>' . $str . '</td></tr>';
			} // if

			// Hauptartikel
			if( isset( $art['mainArticle']['array'] ) ) {
				$str = '';
				foreach( $art['mainArticle']['array'] as $k => $v ) {
					if( $str != '' )
						$str .= '<br>';
					$str .= $v['articleNumber'] . ' ' . $v['articleName'];
				} // foreach

				$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Hauptartikel" ) . '</th><td>' . $str . '</td></tr>';
			} // if

			// verwendete Nummern
			if( isset( $art['usageNumbers2']['array'] ) ) {
				$str = '';
				foreach( $art['usageNumbers2']['array'] as $k => $v ) {
					if( $str != '' )
						$str .= ', ';
					$str .= $v['usageNumber'];
				} // foreach

				$lines[$current_tab] .= '<tr><th>' . $this->t->t( "verwendete Nummern" ) . '</th><td>' . $str . '</td></tr>';
			} // if Artikel

			$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Hersteller" ) . '</th><td>' . $brand . '</td></tr>';
			$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Hersteller Artikelnummer" ) . '</th><td>' . $art['assignedArticle']['articleNo'] . '</td></tr>';

			/* if( isset( $art['articlePrices']['array']['0']['price'] ) )
			 * $strPrice = $this->ConvertPriceToView( $art['articlePrices']['array']['0']['price'] ).
			 * ' '.$art['articlePrices']['array']['0']['currency'].' '.$art['articlePrices']['array']['0']['priceUnitName'];
			 * else
			 * $strPrice = $this->t->t( "kein Preis" );
			 * $lines[$current_tab] .= '<tr><th>'.$this->t->t( "Preis (unverbindliche Preisempfehlung)" ).'</th><td>'.$strPrice.'</td></tr>'; */
			$lines[$current_tab] .= '</table>';
		} // if

		// Bilder
		if( $show_register ) {
			if( isset( $art['articleDocuments']['array'] ) ) {
				$current_tab++;
				$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "Bilder" ) . '</a></li>';

			$lines[$current_tab] = '<table class="table table-sm my-image-table">';
				$bOK = false;
				foreach( $art['articleDocuments']['array'] as $k => $v ) {
					if( @getimagesize( $this->td_setup['td_image_url'] . $this->get_search_tecdocid() . '/' . $v['docId'] . '/0' ) ) {
						$lines[$current_tab] .= '<tr><td><img src="' . $this->td_setup['td_image_url'] . $this->get_search_tecdocid() . '/' . $v['docId'] . '/0"></td></tr>';
						$bOK = true;
					} // if
				} // foreach
				$lines[$current_tab] .= '</table>';

				if( !$bOK ) {
					unset( $tabs[$current_tab] );
					unset( $lines[$current_tab] );
					$current_tab--;
				} // if
			} // if
		} // if Bilder

		// Vergleichsnummern
		$marken = array();
		if( isset( $art['oenNumbers']['array'] ) ) {
			$current_tab++;
			$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "Vergleichsnummern" ) . '</a></li>';

			$lines[$current_tab] = '<div class="alternate_nos row">'; // '<table class="list_left shadow">';
			$i = 0;
			foreach( $art['oenNumbers']['array'] as $k => $v ) {
				$i++;
//				if( ($i % 3) == 1 )
//					$lines[$current_tab] .= '<tr>';

//				$lines[$current_tab] .= '<th>' . $v['brandName'] . '</th>' . '<td>' . '<a onClick="search_oen(\'' . $v['oeNumber'] . '\', false);" style="cursor: pointer;" title="' . $this->t->t( "Suche aktuelles Fenster" ) . '"><img src="' . $this->theme->get_icon_path( 'search.png' ) . '" style="width: 12px; margin-right: 3px;"></a>' . '<a onClick="search_oen(\'' . $v['oeNumber'] . '\', true);" style="cursor: pointer;" title="' . $this->t->t( "Suche neues Fenster" ) . '"><img src="' . $this->theme->get_icon_path( 'search_new_window' ) . '" style="width: 12px; margin-right: 3px;">' . $v['oeNumber'] . '</a>' . '</td>';
				$lines[$current_tab] .= '
					<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3 alternate_nos_card_holder">
						<div class="alternate_nos_card">
							<div class="alternate_nos_brand">' . $v['brandName'] . '</div>
							<div class="row alternate_nos_actions">
								<div class="col-8 alternate_nos_number">
									<a onClick="search_oen(\'' . $v['oeNumber'] . '\', true);" style="cursor: pointer;" title="' . $this->t->t( "Suche neues Fenster" ) . '">' . $v['oeNumber'] . '</a>
								</div>
								<div class="col-4 alternate_nos_buttons">
									<a onClick="search_oen(\'' . $v['oeNumber'] . '\', true);" style="cursor: pointer;" title="' . $this->t->t( "Suche neues Fenster" ) . '"><i class="far fa-window-restore"></i></a>
									<a onClick="search_oen(\'' . $v['oeNumber'] . '\', false);" style="cursor: pointer;" title="' . $this->t->t( "Suche aktuelles Fenster" ) . '"><i class="far fa-window-maximize"></i></a>' . '
								</div>
							</div>
						</div>
					</div>';

//				if( ($i % 3) == 0 )
//					$lines[$current_tab] .= '</tr>';

				if( !in_array( $v['brandName'], $marken ) )
					$marken[$v['brandName']] = array(
						"name" => $v['brandName']
					);
			} // foreach
			$lines[$current_tab] .= '</div>'; // '</table>';
		} // if

		// passende Fahrzeuge
		if( sizeof( $marken ) > 0 ) {
			$arr_manufacturer = $this->get_manufacturer();

			$this->f->array_sort_by_column( $marken, "name" );

			$current_tab++;
			$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "passende Fahrzeuge" ) . '</a></li>';
			$lines[$current_tab] = '<table class="table table-striped table-sm"><tr>';

			$i = 0;
			foreach( $marken as $k => $v ) {
				$i++;
				if( ($i % 8) == 1 )
					$lines[$current_tab] .= '<tr>';

				$manu = $arr_manufacturer[$this->f->recursive_array_search( $v['name'], $arr_manufacturer )]['id'];
				$lines[$current_tab] .= '<td><a href="#" onClick="fill_linked_vehicles(' . $articleId . ', ' . $manu . ', ' . $vehicle . ');">' . $v['name'] . '</a></td>';

				if( ($i % 8) == 0 )
					$lines[$current_tab] .= '</tr>';
			} // foreach
			$lines[$current_tab] .= '<tr><td colspan="8"><a href="#" onClick="fill_linked_vehicles(' . $articleId . ', 0, 0);">' . $this->t->t( 'alle Fahrzeuge laden' ) . '</a></td></tr>';

			$lines[$current_tab] .= '</table>';
			$lines[$current_tab] .= '
		    	<div id="linked_vehicles" style="margin-top: 10px;">
						<nav aria-label="Page navigation example">
						  <ul class="pagination pagination-sm" style="margin: 0;">
							</ul>
						</nav>

						<table class="list shadow">
	  					<tr>
			  				<th><div class="ajax_loader fill_linked_vehicles_loader" style="width: 16px; float: left;"></div>' . $this->t->t( "Hersteller" ) . '</th>
			  				<th>' . $this->t->t( "Modell" ) . '</th>
			  				<th>' . $this->t->t( "Typ (Motor)" ) . '</th>
			  				<th>' . $this->t->t( "PS/KW" ) . '</th>
			  				<th>' . $this->t->t( "Karosserie" ) . '</th>
			  				<th>' . $this->t->t( "Treibstoff" ) . '</th>
			  				<th>' . $this->t->t( "Baujahr von" ) . '</th>
			  				<th>' . $this->t->t( "Baujahr bis" ) . '</th>
			  				<th>' . $this->t->t( "ccm" ) . '</th>
			  				<th>' . $this->t->t( "Motorcode" ) . '</th>
			  				<th>' . $this->t->t( "Zylinder" ) . '</th>
			  			</tr>
			  		</table>
		    	</div>';
		} // if

		// Komponenten
		if( $show_register ) {
			$comp = $this->get_components( $articleId );
			if( sizeof( $comp ) > 0 ) {
				$current_tab++;
				$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "Komponenten" ) . '</a></li>';
			$lines[$current_tab] = '<table class="table table-striped table-sm">';
				$lines[$current_tab] .= '
		  			<tr>
		  				<th>' . $this->t->t( "Artikel Nr." ) . '</th>
							<th>' . $this->t->t( "Marke" ) . '</th>
		  				<th>' . $this->t->t( "Name" ) . '</th>
		  				<th>' . $this->t->t( "Menge" ) . '</th>
		  			</tr>';
				foreach( $comp as $k => $v ) {
					$lines[$current_tab] .= '
		  				<tr>
		    			  <td>
		    				<a onClick="search_oen(\'' . $v['no'] . '\', true);" style="cursor: pointer;" title="' . $this->t->t( "Suche neues Fenster" ) . '">
		    				  ' . $v['no'] . '</a>
		    			  </td>
								<td>' . $v['brand'] . '</td>
		  					<td>' . $v['name'] . '</td>
		  					<td class="right alternate_nos_buttons">' . $v['quantity'] . '
		    					<a onClick="search_oen(\'' . $v['no'] . '\', false);" style="cursor: pointer;" title="' . $this->t->t( "Suche aktuelles Fenster" ) . '"><i class="far fa-window-maximize right-icon-button"></i></a>
		    					<a onClick="search_oen(\'' . $v['no'] . '\', true);" style="cursor: pointer;" title="' . $this->t->t( "Suche neues Fenster" ) . '"><i class="far fa-window-restore right-icon-button"></i>
							</td>
		  				</tr>';
				} // foreach
				$lines[$current_tab] .= '</table>';
			} // if
		} // if Komponenten

		// Service, technische Daten
		if( $show_register ) {
			if( isset( $art['immediateInfo']['array'] ) || isset( $art['articleDocuments']['array'] ) ) {
				$current_tab++;
				$tabs[$current_tab] = '<li class="nav-item"><a class="nav-link" href="#tab[' . $current_tab . ']">' . $this->t->t( "Service" ) . '</a></li>';

				$lines[$current_tab] = '';

				if( isset( $art['immediateInfo']['array'] ) ) {
				$lines[$current_tab] .= '<table class="table table-striped table-sm">';
					$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Informationen" ) . '</td></th>';
					foreach( $art['immediateInfo']['array'] as $k => $v ) {
						if( isset( $v['infoText'] ) )
							$lines[$current_tab] .= '<tr><td>' . $v['infoText'] . '</td></tr>';
						if( isset( $v['infoTypeName'] ) )
							$lines[$current_tab] .= '<tr><td>' . $v['infoTypeName'] . '</td></tr>';
					} // foreach
					$lines[$current_tab] .= '</table><br>';
				} // if

				if( isset( $art['articleDocuments']['array'] ) ) {
				$lines[$current_tab] .= '<table class="table table-striped table-sm">';
					$lines[$current_tab] .= '<tr><th>' . $this->t->t( "Dokumente" ) . '</td></th>';
					foreach( $art['articleDocuments']['array'] as $k => $v ) {
						$tempArray = get_headers( $this->td_setup['td_image_url'] . $this->get_search_tecdocid() . '/' . $v['docId'] );
						$tempString = $tempArray[0];
						if( strpos( $tempString, "200" ) ) {
							$lines[$current_tab] .= '<tr><td><a href="' . $this->td_setup['td_image_url'] . $this->get_search_tecdocid() . '/' . $v['docId'] . '" target="_blank">' . $v['docFileName'] . '</a></td></tr>';
						} // if
					} // foreach
					$lines[$current_tab] .= '</table>';
				} // if
			} // if
		} // if Service, technische Daten

		// Create Form
		$strOut = '<form method="post" action="#">
			<input type="hidden" name="articleLinkId" value="' . $articleLinkId . '">';
		$strOut .= '<div id="tabs" class="popup-tabs"><ul class="nav nav-justified popup-tabs-list">' . implode( $tabs ) . '</ul>';
		for( $i = 1; $i <= $current_tab; $i++ ) {
			// $strOut .= '<div id="tab['.$i.']" style="padding-left:1px;"><table class="list">'.$lines[$i].'</table></div>';
			$strOut .= '<div id="tab[' . $i . ']" class="popup-tab">' . $lines[$i] . '</div>';
		} // for
		$strOut .= '</div></form>';

		return ($strOut);
	}

	function get_components( $articleId ) {
		$function = 'getArticlePartList';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'articleId' => $articleId,
			'lang' => $this->get_search_language(),
			'provider' => $this->get_search_tecdocid(),
			'linkingTargetType' => 'U'
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		if( isset( $result->data->array[0]->partlistInfo->array ) ) {
			foreach( $result->data->array[0]->partlistInfo->array as $k => $v ) {
				$arr[] = array(
					"name" => $v->partlistDetails->articleName,
					"no" => $v->partlistDetails->articleNo,
					"brand" => $v->partlistDetails->brandName,
					"quantity" => $v->partlistDetails->quantity
				);
			} // foreach
		} // if

		return ($arr);
	}

	function get_linked_vehicles_detail( $arr2, $arr, $articleId ) {
		$function = 'getArticleLinkedAllLinkingTargetsByIds3';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'articleId' => $articleId,
			'immediateAttributs' => true,
			'lang' => $this->get_search_language(),
			'linkedArticlePairs' => array(
				'array' => $arr
			),
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		if( isset( $result->data->array ) ) {
			foreach( $result->data->array as $k => $v ) {
				if( isset( $v->linkedVehicles->array[0] ) ) {
					// Genaues Model holen
					$function = 'getVehicleByIds4';
					$params = array(
						'articleCountry' => $this->get_search_articleCountry(),
						'carIds' => array(
							'array' => array(
								$v->linkedVehicles->array[0]->carId
							)
						),
						'motorCodes' => true,
						'countriesCarSelection' => $this->get_search_countriesCarSelection(),
						'country' => $this->get_search_country(),
						'lang' => $this->get_search_language(),
						'provider' => $this->get_search_tecdocid()
					);
					$request = $this->createRequest( $function, $params );
					$result2 = $this->callJSON( $function, $request );

					$fuelType = "";
					if( isset( $result2->data->array[0]->vehicleDetails->fuelType ) )
						$fuelType = $result2->data->array[0]->vehicleDetails->fuelType;

					if( !isset( $v->linkedVehicles->array[0]->yearOfConstructionFrom ) )
						$v->linkedVehicles->array[0]->yearOfConstructionFrom = "";
					if( !isset( $v->linkedVehicles->array[0]->yearOfConstructionTo ) )
						$v->linkedVehicles->array[0]->yearOfConstructionTo = "";

					$motorcodes = "";
					if( isset( $result2->data->array[0]->motorCodes->array ) ) {
						foreach( $result2->data->array[0]->motorCodes->array as $k2 => $v2 ) {
							if( $motorcodes != "" )
								$motorcodes .= ", ";
							$motorcodes .= $v2->motorCode;
						} // foreach
					} // if

					$zylinder = "";
					if( isset( $result2->data->array[0]->vehicleDetails->cylinder ) )
						$zylinder = $result2->data->array[0]->vehicleDetails->cylinder;

					$ccmTech = "";
					if( isset( $result2->data->array[0]->vehicleDetails->ccmTech ) )
						$ccmTech = $result2->data->array[0]->vehicleDetails->ccmTech;

					$arr2[] = array(
						'manufacturer' => $v->linkedVehicles->array[0]->manuDesc,
						'model' => '<a href="?indiv_search_model=1&manufacturer=' . $v->linkedVehicles->array[0]->manuId . '&model=' . $v->linkedVehicles->array[0]->modelId . '&vehicle=' . $v->linkedVehicles->array[0]->carId . '" target="_blank" style="cursor: pointer;" title="' . $this->t->t( "Suche neues Fenster" ) . '"><img src="' . $this->theme->get_icon_path( 'search_new_window' ) . '" style="width: 12px; margin-right: 3px;">' . $v->linkedVehicles->array[0]->modelDesc . '</a>',
						'vehicle' => $v->linkedVehicles->array[0]->carDesc,
						'kw' => $v->linkedVehicles->array[0]->powerKwFrom . ' / ' . $v->linkedVehicles->array[0]->powerHpFrom,
						'by_from' => $this->ConvertYearToView( $v->linkedVehicles->array[0]->yearOfConstructionFrom ),
						'by_to' => $this->ConvertYearToView( $v->linkedVehicles->array[0]->yearOfConstructionTo ),
						'constrution_type' => $v->linkedVehicles->array[0]->constructionType,
						'fuel_type' => $fuelType,
						'ccm' => $ccmTech,
						'motorcode' => $motorcodes,
						'zylinder' => $zylinder
					);
				} // if
			} // foreach
		} // if

		return ($arr2);
	}

	function get_linked_vehicles_old( $articleId, $manufacturer, $vehicle, &$count, $start = 0 ) {
		$function = 'getArticleLinkedAllLinkingTarget3';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'articleId' => $articleId,
			'lang' => $this->get_search_language(),
			'linkingTargetId' => $vehicle,
			'linkingTargetManuId' => $manufacturer,
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$arr = array();
		$arr2 = array();
		if( isset( $result->data->array['0']->articleLinkages->array ) ) {
			foreach( $result->data->array['0']->articleLinkages->array as $k => $v ) {
				$arr[] = array(
					'articleLinkId' => $v->articleLinkId,
					'linkingTargetId' => $v->linkingTargetId
				);

				if( sizeof( $arr ) >= 25 ) {
					$arr2 = $this->get_linked_vehicles_detail( $arr2, $arr, $articleId );
					$arr = array();
				} // if
			} // foreach
			$arr2 = $this->get_linked_vehicles_detail( $arr2, $arr, $articleId );
		} // if

		return ($arr2);
	}

	function get_linked_vehicles( $articleId, $manufacturer, $vehicle, &$count, $start = 1, &$limit = 10 ) {
		$function = 'getArticleLinkedAllLinkingTarget3';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'articleId' => $articleId,
			'lang' => $this->get_search_language(),
			'linkingTargetId' => $vehicle,
			'linkingTargetManuId' => $manufacturer,
			'linkingTargetType' => $this->get_search_linkingtargettype(),
			'provider' => $this->get_search_tecdocid()
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		$count = 0;
		if( isset( $result->data->array['0']->articleLinkages->array ) )
			$count = sizeof( $result->data->array['0']->articleLinkages->array );
		$limit = 20;
		$iIndex = 0;

		$arr = array();
		$arr2 = array();
		if( isset( $result->data->array['0']->articleLinkages->array ) ) {
			foreach( $result->data->array['0']->articleLinkages->array as $k => $v ) {
				$iIndex++;
				if( ($iIndex >= $start) && ($iIndex < ($start + $limit)) ) {
					$arr[] = array(
						'articleLinkId' => $v->articleLinkId,
						'linkingTargetId' => $v->linkingTargetId
					);

					if( sizeof( $arr ) >= $limit ) {
						$arr2 = $this->get_linked_vehicles_detail( $arr2, $arr, $articleId );
						$arr = array();
					} // if
				} // if

				if( sizeof( $arr2 ) >= $limit )
					break;
			} // foreach

			// Restliche holen < $limit
			if( sizeof( $arr ) > 0 )
				$arr2 = $this->get_linked_vehicles_detail( $arr2, $arr, $articleId );
		} // if

		return ($arr2);
	}

	// ----------------------------------------------------------------------------------------
	// FUNCTION
	// ----------------------------------------------------------------------------------------
	function ConvertPriceToDB( $str ) {
		$str = str_replace( ".", "", $str );
		$str = str_replace( ",", ".", $str );
		$str = floatval( $str ) / 100;

		return ($str);
	}

	function ConvertQuantityToDB( $str ) {
		$str = str_replace( ".", "", $str );
		$str = str_replace( ",", ".", $str );
		$str = floatval( $str );

		return ($str);
	}

	function ConvertPriceToView( $dec_en ) {
		$dec_en = str_replace( ",", "", $dec_en );
		$dec_de = $dec_en / 100;
		$dec_de = $this->f->fn( $dec_de );

		return ($dec_de);
	}

	function ConvertQuantityToView( $dec_en ) {
		$dec_en = str_replace( ",", "", $dec_en );
		$dec_de = $dec_en / 1.0;
		$dec_de = str_replace( ".", ",", $dec_de );

		return ($dec_de);
	}

	function ConvertYearToView( $str ) {
		if( $str == "" )
			return ($str);

		$year = substr( $str, 0, 4 );
		$month = substr( $str, 4, 2 );

		return ($month . '/' . $year);
	}

	// ----------------------------------------------------------------------------------------
	// LIST FUNCTION
	// ----------------------------------------------------------------------------------------
	function log_search( $strPattern, $action ) {
		$this->db->insert( "TEC_LOG_SEARCH", array(
			"user_id" => $_SESSION['c_user_id'],
			"act_time" => $this->f->d( time() ),
			"action" => $action,
			"search_pattern" => trim( $strPattern )
		) );
		$this->db->commit();
	}

	// ----------------------------------------------------------------------------------------
	// JS CALL FUNCTION
	// ----------------------------------------------------------------------------------------
	function load_orders() {
		$arr = array();

		$this->db->query( "SELECT COUNT( id ) AS anz FROM TEC_WARENKORB WHERE user_id='" . $_SESSION['c_user_id'] . "' AND bestell_id='0'" );
		$r = $this->db->getNext();
		$arr['shopping_count'] = $r['anz'];

		$this->db->query( "SELECT COUNT( id ) AS anz FROM TEC_WARENKORB WHERE bestell_id!='0' AND bestell_status_id='0'" );
		$r = $this->db->getNext();
		$arr['order_count'] = $r['anz'];

		return ($arr);
	}

	function print_info_browser( $query, $start, &$max_list ) {
		$setup = $this->f->load_setup( "TEC_SETUP" );
		if( $setup['count_results_univ_items'] <= 0 )
			$max_list = 10;
		else
			$max_list = $setup['count_results_univ_items'];

		$this->db->query( $query );
		$r = $this->db->getNext();
		$anz = $r['max'];

		if( $anz <= $max_list )
			return;

		$page = floor( ($start - 1) / $max_list + 1 );
		$strOut = $this->f->paginator_print( $page, $anz, $max_list, false, false );

		return( $strOut );

		$prev_disabled = "";
		$prev_id = $start - $max_list;
		if( $prev_id < 1 )
			$prev_disabled = ' disabled';

		$next_disabled = "";
		$next_id = $start + $max_list;
		if( $next_id > $anz )
			$next_disabled = ' disabled';

		$max_reached = false;
		if( $anz > (15 * $max_list) ) {
			$anz = (15 * $max_list);
			$max_reached = true;
		} // if

		$strOut .= '
			<nav aria-label="Page navigation example">
			  <ul class="pagination" style="margin: 0;">
			    <li class="page-item'.$prev_disabled.'">
			      <a class="page-link" href="" data-start="' . $prev_id . '" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>';
		for( $i = 1; $i <= $anz; $i += $max_list ) {
			$active = '';
			if( $i == $start )
				$active = ' active';

			$strOut .= '<li class="page-item'.$active.'"><a class="page-link" href="" data-start="' . $i . '">' . (($i - 1) / $max_list + 1) . '</a></li>';
		} // for
		if( $max_reached ) {
			$strOut .= '<li class="page-item disabled"><a class="page-link" href="">+++</a></li>';
		} // if

		$strOut .= '
					<li class="page-item'.$next_disabled.'">
					  <a class="page-link" href="" data-start="' . $next_id . '" aria-label="Next">
					  	<span aria-hidden="true">&raquo;</span>
					  </a>
					</li>
				</ul>
			</nav>';

		return ($strOut);
	}

	function get_articles_infos( $text, $subcategory_id_filter, $start, $filter_width, $filter_height, $filter_length, $filter_diameter, $filter_viscosity, $filter_kw, $filter_ah, $filter_supplier, $filter_belt_shafts, $filter_volt, $filter_amper, $filter_season ) {
		global $_location;

		$location = $_location;
		if( defined( "SIMULATE_LOCATION" ) )
			$location = SIMULATE_LOCATION;

		if( ($text == "") )
			$text = "%";
		if( ($text == "") && ($subcategory_id_filter > 0) )
			$text = "%";

		// if( (($text == "") || ($text == "%") || ($text == "*")) && ($subcategory_id_filter == 0) ) return;
		if( ($text == "all") )
			$text = "%";

		$setup = $this->f->load_setup( "TEC_SETUP" );
		$userinfo = $this->f->load_user( $_SESSION['c_user_id'], "user_level" );

		$strOut = '';
		$arrKat = array();

		$_WHERE = "";
		if( ($subcategory_id_filter != "") && ($subcategory_id_filter >= 1) )
			$_WHERE .= " AND subcategory_id='" . $subcategory_id_filter . "'";

		// Attribute
		if( $filter_width != "" )
			$_WHERE .= " AND width='" . $filter_width . "'";
		if( $filter_height != "" )
			$_WHERE .= " AND height='" . $filter_height . "'";
		if( $filter_length != "" )
			$_WHERE .= " AND length='" . $filter_length . "'";
		if( $filter_diameter != "" )
			$_WHERE .= " AND diameter='" . $filter_diameter . "'";
		if( $filter_viscosity != "" )
			$_WHERE .= " AND viscosity='" . $filter_viscosity . "'";
		if( $filter_kw != "" )
			$_WHERE .= " AND kw='" . $filter_kw . "'";
		if( $filter_ah != "" )
			$_WHERE .= " AND ah='" . $filter_ah . "'";
		if( $filter_supplier != "" )
			$_WHERE .= " AND supplier='" . $filter_supplier . "'";
		if( $filter_belt_shafts != "" )
			$_WHERE .= " AND belt_shafts='" . $filter_belt_shafts . "'";
		if( $filter_volt != "" )
			$_WHERE .= " AND volt='" . $filter_volt . "'";
		if( $filter_amper != "" )
			$_WHERE .= " AND amper='" . $filter_amper . "'";
		if( $filter_season != "" )
			$_WHERE .= " AND season='" . $filter_season . "'";

		$query = "
				SELECT COUNT( i.id ) AS max FROM TEC_ITEMS AS i
				WHERE
					((i.hersteller_nr LIKE '%" . $text . "%') OR
					(i.interne_nr LIKE '%" . $text . "%') OR
					(i.bemerkung LIKE '%" . $text . "%') OR
					(i.description_2 LIKE '%" . $text . "%'))
					" . $_WHERE;

		$strOut .= '<div style="">' . $this->print_info_browser( $query, $start, $max_list );

		$strOut .= '
	  		<table class="table table-sm" id="search_articles_infos_table">
					<thead class="article-row-head">
						<tr class="head-row">
							<th>' . $this->t->t( 'Bild' ) . '</th>
		  				<th data-sort="string" style="cursor: pointer;">' . $this->t->t( 'Hersteller' ) . '</th>
		  				<th data-sort="string" style="cursor: pointer;">' . $this->t->t( 'Hersteller Nr.' ) . '</th>
		  				<th data-sort="string" style="cursor: pointer;">' . $this->t->t( 'Bemerkung' ) . ' / ' . $this->t->t( 'Zusatzinfo' ) . '</th>
		  				<th data-sort="int" style="min-width: 50px; cursor: pointer;">&nbsp;' . /*$this->t->t( 'Lagerstand' ) .*/ '<span class="arrow">&nbsp;<i class="fas fa-angle-down"></i></span></th>';
		if( $setup['enable_topmotive_api'] == 1 )
			$strOut .= '<th id="enable_location_id_cat" style="text-align: center">' . $this->t->t( 'Lagerort' ) . '</th>';
		$strOut .= '
		  				<th style="text-align: center">' . $this->t->t( 'Menge' ) . '</th>
		  				<th data-sort="float" style="cursor: pointer; text-align: right;">' . $this->t->t( 'Preis' ) . '</th>
		  				<!-- <th>' . $this->t->t( 'Gesamt' ) . '</th> -->
		  				<th style="text-align: center">' . $this->t->t( 'Aktion' ) . '</th>
		  			</tr>
					</thead>';

		if( $this->db->query( "
				SELECT i.*, c.category, s.subcategory, 0 AS brutto
				FROM TEC_ITEMS AS i
				LEFT JOIN TEC_ITEMS_CATEGORY AS c ON (c.id=i.category_id)
				LEFT JOIN TEC_ITEMS_SUBCATEGORY AS s ON (s.id=i.subcategory_id)
				WHERE
					((i.hersteller_nr LIKE '%" . $text . "%') OR
					(i.interne_nr LIKE '%" . $text . "%') OR
					(i.bemerkung LIKE '%" . $text . "%') OR
					(i.description_2 LIKE '%" . $text . "%'))
					" . $_WHERE . "
				ORDER BY i.sorting ASC, i.lagerstand DESC, i.bemerkung ASC
				LIMIT " . ($start - 1) . "," . $max_list, 1 ) ) {
			$r_final = array();
			while( $this->db->isNext( 1 ) ) {
				$r2 = $this->db->getNext( 1 );

				// Lagerstand und Preis holen
				if( $setup['enable_topmotive_api'] == 1 ) {
					$this->get_article_stock_price( $r2['hersteller'], $r2['hersteller_nr'], $r2['lagerstand'], $r2['preis'], $r2['brutto'] );
				} // if

				$r_final[] = $r2;
			} // while

			$this->f->array_sort_by_column( $r_final, 'lagerstand', SORT_DESC );

			foreach( $r_final as $key => $r ) {
				$r['id'] = -$r['id'];

				// Lagerstand und Preis holen
				if( $setup['enable_topmotive_api'] == 1 ) {
					$arr = array();
					$arr['0'] = new stdClass();
					$arr['0']->articleNo = $r['hersteller_nr'];
					$arr['0']->brandName = $r['hersteller'];
					$this->get_article_stock_price_all( $arr );
					$this->get_article_stock_price( $r['hersteller'], $r['hersteller_nr'], $r['lagerstand'], $r['preis'], $r['brutto'] );
				} // if

				// Kategorien Baum speichern
				if( !isset( $arrKat[$r['category']] ) )
					$arrKat[$r['category']] = array();
				if( !isset( $arrKat[$r['category']][$r['subcategory_id']] ) )
					$arrKat[$r['category']][$r['subcategory_id']] = $r['subcategory'];

				$action_button = '';
				$ampel = '';
				if( $r['lagerstand'] <= 0.1 ) {
					$action_button = $this->f->get_button( 'Kaufen Rot' );
					$ampel = '<i class="fas fa-times-circle text-danger"></i> ';
				} // if
				if( $r['lagerstand'] > 0.1 ) {
					$action_button = $this->f->get_button( 'Kaufen Grün' );
					$ampel = '<i class="fas fa-check-circle text-success"></i> ';
				} // if

				$title = $this->get_tooltip_price( $r['preis'], $r['brutto'] );
				$name = $this->t->t( 'Artikel ' ) . ' ' . $r['hersteller_nr'] . ' ' . $r['hersteller'];
				$buy_button = '<a class="link_click_button" onClick="buy_article(' . $r['id'] . ',' . $r['id'] . ',0,0,' . $r['lagerstand'] . ',\'' . $r['hersteller_nr'] . '\',\'' . $name . '\',0);">' . $action_button . '</a>';

				$in_order = 0;
				if( $this->db->query( "SELECT quantity FROM TEC_WARENKORB WHERE user_id='" . $_SESSION['c_user_id'] . "' AND articleId='" . $r['id'] . "' AND bestell_id='0'", 2 ) )
					$r3 = $this->db->getNext( 2 );
				else
					$r3['quantity'] = 0;
				$in_order = $r3['quantity'];

				if( $userinfo['user_level'] >= 100 )
					$auf_lager_text = $this->f->fn( round( $r['lagerstand'], 0 ), 0 );
				else {
					if( $r['lagerstand'] > 0.1 )
						$auf_lager_text = $this->t->t( "ja" );
					else
						$auf_lager_text = $this->t->t( "nein" );
					$auf_lager_text = "";
				} // else

				// Zusätzliche Beschreibungen
				$description_2 = $r['description_2'];
				if( $description_2 != '' )
					$description_2 = '<br>' . $description_2;
				$description_3 = $r['description_3'];
				if( $description_3 != '' )
					$description_3 = '<br><span style="font-size: 10px;">' . $description_3 . '</span>';

				// Bilder
				$picture = '';
				if( file_exists( BASE_DIR . 'upload/' . $location . '/univ_items/' . $r['item_picture'] . '.png' ) ) {
					$picture_path = DOMAIN . 'upload/' . $location . '/univ_items/' . $r['item_picture'] . '.png';

					$pic = '<td><img src="' . $picture_path . '" style="width: 250px;"></td>';

					if( file_exists( BASE_DIR . 'upload/' . $location . '/univ_items/' . $r['item_picture_2'] . '.png' ) ) {
						$picture_path2 = DOMAIN . 'upload/' . $location . '/univ_items/' . $r['item_picture_2'] . '.png';

						$pic .= '<td><img src="' . $picture_path2 . '" style="width: 250px;"></td>';
					} // if
					if( file_exists( BASE_DIR . 'upload/' . $location . '/univ_items/' . $r['item_picture_3'] . '.png' ) ) {
						$picture_path3 = DOMAIN . 'upload/' . $location . '/univ_items/' . $r['item_picture_3'] . '.png';

						$pic .= '<td><img src="' . $picture_path3 . '" style="width: 250px;"></td>';
					} // if

					$picture = '<img src="' . $picture_path . '" style="max-height: 50px; max-width: 50px;" title="' . htmlspecialchars( '<table><tr>' . $pic . '</tr></table>' ) . '">';
				} // if

				// Lagerorte
				$strLocations = "";
				if( $setup['enable_topmotive_api'] == 1 ) {
					$locations = array();
					if( isset( $this->arr_all_items[$this->get_brand_id( strtoupper( $r['hersteller'] ) ) . "_" . $r['hersteller_nr']]['locations'] ) )
						$locations = $this->arr_all_items[$this->get_brand_id( strtoupper( $r['hersteller'] ) ) . "_" . $r['hersteller_nr']]['locations'];
/*
					if( $_location == "qnap_home" ) {
						$locations = array(
							1 => array(
								"id" => 1,
								"name" => "1",
								"quantity" => 1,
								"brutto" => 7.89,
								"netto" => 33.33,
								"sub_shop" => 0
							),
							2 => array(
								"id" => 2,
								"name" => "2",
								"quantity" => 2,
								"brutto" => 12.34,
								"netto" => 44.44,
								"sub_shop" => 1
							)
						);
						$auf_lager_text = 100;
						$action_button = $this->f->get_button( 'Kaufen Grün', true );
						$ampel = $this->theme->get_icon_path( 'kreis_green' );
						$r['preis'] = 2.34;
						$buy_button = '<a class="link_click_button" onClick="buy_article(' . $r['id'] . ',' . $r['id'] . ',0,0,' . $r['lagerstand'] . ',\'' . $r['hersteller_nr'] . '\',\'' . $name . '\',0);">' . $action_button . '</a>';
					} // if
*/

					$strLocations .= '<td><select class="mytec form-control form-control-sm chosen-select" style="width: 100%;" id="location_' . $r['id'] . '_0" OnChange="change_location(' . $r['id'] . ',0)"><option value="0" data-quantity="' . $r['lagerstand'] . '" data-price="' . $r['preis'] . '" data-title="' . $title . '">&nbsp;</option>';
					foreach( $locations as $k => $v ) {
						$loc_title = $this->get_tooltip_price( $v['netto'], $v['brutto'] );
						$stock = 1;
						$strLocations .= '<option value="' . $v['id'] . '" data-quantity="' . $v['quantity'] . '" data-price="' . $v['netto'] . '" data-title="' . $loc_title . '">' . $v['name'] . ' (' . $this->f->fn( $v['netto'] ) . ')</option>';
					} // foreach
					$strLocations .= '</select></td>';
				} // if

				// interne Nr.
				$h = $r['hersteller_nr'];
				if( ($r['interne_nr'] != '') && ($r['interne_nr'] != $r['hersteller_nr']) )
					$h .= '<br><span class="red">' . $r['interne_nr'] . '</span>';

				$strOut .= '
	  			<tr class="article-row">
						<td>' . $picture . '</td>
	  				<td id="brand_' . $r['id'] . '_0" data-brand_id="' . $this->get_brand_id( strtoupper( $r['hersteller'] ) ) . '">' . $r['hersteller'] . '</td>
	  				<td>' . $h . '</td>
	  				<td>' . $r['bemerkung'] . '<br>' . $r['zusatzinfo'] . $description_2 . $description_3 . '</td>
	  				<td class="right top-aligned round-icon">' . $ampel . $auf_lager_text . '</td>';
				if( $setup['enable_topmotive_api'] == 1 )
					$strOut .= $strLocations;
				$strOut .= '
						<td><input id="quantity_' . $r['id'] . '_0" name="quantity_' . $r['id'] . '_0" value="' . $in_order . '" class="spinner_quantity" style="width: 30px;"/></td>
	  				<td class="right" title="' . $title . '" id="price_' . $r['id'] . '_0">' . $this->f->fn( $r['preis'] ) . '</td>
	  				<!-- <td class="right" id="amount_' . $r['id'] . '_0">0</td> -->
	  				<td style="display: inline-flex">
				  		' . $buy_button . '
	  					<a id="search_for_infos" href="#" onClick="search_for_infos(\'' . $r['hersteller_nr'] . '\');" class="link_click_button">' . $this->f->get_button( 'suchen', true ) . '</a>
	  				</td>
	  			</tr>';
			} // while
		} else {
			$strOut .= '
				<tr>
					<td colspan="8" style="text-align: center;">' . $this->t->t( 'kein Ergebnis gefunden' ) . '</td>
				</tr>';
		} // else
		$strOut .= '</table>';

		$strOut .= $this->print_info_browser( $query, $start, $max_list ) . '</div>';

		$strOut = '<div id="search_result_kat">' . $strOut . '</div>';

		return ($strOut);
	}

	function get_articles_infos_categories( $bEcho = false, $subcategory_id_filter = 0 ) {
		$strOut_Kat = '';
		$arrKat = array();

		$this->db->query( "
			SELECT i.category, s.subcategory, s.id
			FROM TEC_ITEMS_SUBCATEGORY AS s
			LEFT JOIN TEC_ITEMS_CATEGORY AS i ON (i.id=s.category_id)" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$arrKat[$r['category']][$r['id']] = $r['subcategory'];
		} // while

		// Kategorien
		$strOut_Kat .= '
			<div id="search_categories_kat_div" class="flex mr-1" style="float: left; min-width: 225px; display: none;">
				<table class="list shadow" >
					<div class="orange-head" style="height: 31px;">' . $this->t->t( 'Gruppen' ) . '</div>
					<tr><td valign="top">';

		if( isset( $arrKat ) ) {
			$strOut_Kat .= '<div id="search_categories_kat"><ul id="ksub_0">';
			$strOut_Kat .= '<li data-id="0" id="kelement_0" data-jstree=\'{"icon":"/' . SUBDIR . 'themes/_default/icons/search_16_tecdoc.png"}\'><a onClick="fill_infos(0)"><span style="text-decoration: none;">' . $this->t->t( 'alle Kategorien' ) . '</span></a></li>';
			foreach( $arrKat as $k => $v ) {
				$strOut_Kat .= '<li id="kelement_' . $k . '">' . $k . '<ul id="ksub_' . $k . '">';
				foreach( $v as $k2 => $v2 ) {

					if( $k2 == $subcategory_id_filter )
						$sel = 'class="jstree-clicked"';
					else
						$sel = '';
					$strOut_Kat .= '<li data-id="' . $k2 . '" id="kelement_' . $k2 . '" data-jstree=\'{"icon":"/' . SUBDIR . 'themes/_default/icons/search_16_tecdoc.png"}\'><a ' . $sel . ' onClick="fill_infos(' . $k2 . ')"><span style="text-decoration: none;">' . $v2 . '</span></a></li>';
				} // foreach
				$strOut_Kat .= '</ul></li>';
			} // foreach
			$strOut_Kat .= '</ul></div>';
		} // if

		$strOut_Kat .= '
					</td></tr>
				</table>
			</div>';

		if( $bEcho ) {
			echo $strOut_Kat;
			return;
		} // if

		return ($strOut_Kat);
	}

	function print_order( $order_id ) {
		$this->db->query( "
			SELECT ui.name, ui.info, be.warenkorb_header_info, be.bestellt_am
			FROM TEC_WARENKORB AS w
			LEFT JOIN CORE_USER_INFO AS ui ON (ui.user_id=w.user_id)
			LEFT JOIN TEC_BESTELLUNG AS be ON (be.id=w.bestell_id)
			WHERE w.bestell_id='" . $order_id . "'
  		LIMIT 1" );
		$r = $this->db->getNext();

		$strOut = $this->t->t( "Bestellung" ) . ': ' . $order_id . '<br>' . $this->t->t( 'Datum' ) . ': ' . $this->f->t( $r['bestellt_am'] ) . '<hr>' . $r['name'] . "<br>" . str_replace( "\n", "<br>", $r['info'] ) . "<br>" . str_replace( "\n", "<br>", $r['warenkorb_header_info'] ) . '<hr>
  		<div class="table-responsive">
  		<table class="list shadow table table-sm" id="search_articles_infos_table">
  			<tr>
  				<th>' . $this->t->t( 'Artikel Nr.' ) . '</th>
  				<th>' . $this->t->t( 'Artikelname' ) . '</th>
  				<th>' . $this->t->t( 'Menge' ) . '</th>
  				<th>' . $this->t->t( 'Preis' ) . '</th>
  				<th>' . $this->t->t( 'Betrag' ) . '</th>
  				<th>' . $this->t->t( 'Lagerstand' ) . '</th>
  				<th>' . $this->t->t( 'Info' ) . '</th>
  			</tr>
			</div>';

		$this->db->query( "
  		SELECT w.article_no, w.article_name, w.quantity, w.article_price, w.amount, i.lagerstand
  		FROM TEC_WARENKORB AS w
  		LEFT JOIN TEC_ITEMS AS i ON (UPPER(i.hersteller)=UPPER(w.brand) AND UPPER(i.hersteller_nr)=UPPER(w.article_no))
  		WHERE w.bestell_id='" . $order_id . "'", "print_order" );
		while( $this->db->isNext( "print_order" ) ) {
			$r = $this->db->getNext( "print_order" );

			$strOut .= '
  			<tr>
		  		<td>' . $r['article_no'] . '</td>
		  		<td>' . $r['article_name'] . '</td>
		  		<td class="right">' . $this->f->fn( $r['quantity'], 0 ) . '</td>
		  		<td class="right">' . $this->f->fn( $r['article_price'] ) . '</td>
		  		<td class="right">' . $this->f->fn( $r['amount'] ) . '</td>
		  		<td class="right">' . $this->f->fn( $r['lagerstand'], 0 ) . '</td>
		  		<td style="width: 250px;"></td>
		  	</tr>';
		} // while

		$strOut .= '</table>';

		return ($strOut);
	}

	function save_search_history( $search_manufacturer, $year_from, $search_model, $filter_fueltype, $filter_pskw, $filter_motorcode, $search_vehicle, $all_manufacturers, $text ) {
		$text = str_replace( "_ ", ", ", $text );
		$text = str_replace( "_", "", $text );

		$this->db->insert( "TEC_SEARCH_HISTORY", array(
			"user_id" => $_SESSION['c_user_id'],
			"act_time" => $this->f->d( time() ),
			"search_manufacturer" => $search_manufacturer,
			"year_from" => $year_from,
			"search_model" => $search_model,
			"filter_fueltype" => $filter_fueltype,
			"filter_pskw" => $filter_pskw,
			"filter_motorcode" => $filter_motorcode,
			"search_vehicle" => $search_vehicle,
			"all_manufacturers" => $all_manufacturers,
			"text" => $text
		) );

		$this->db->query( "SELECT id FROM TEC_SEARCH_HISTORY WHERE user_id='" . $_SESSION['c_user_id'] . "' ORDER BY act_time DESC LIMIT 10" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();
		} // while

		$this->db->query( "DELETE FROM TEC_SEARCH_HISTORY WHERE user_id='" . $_SESSION['c_user_id'] . "' AND id<'" . $r['id'] . "'" );
		$this->db->commit();
	}

	function restore_search_history( $id ) {
		$this->db->query( "SELECT * FROM TEC_SEARCH_HISTORY WHERE id='" . $id . "'" );
		$r = $this->db->getNext();

		$arr = array(
			"search_manufacturer" => $r['search_manufacturer'],
			"year_from" => $r['year_from'],
			"search_model" => $r['search_model'],
			"filter_fueltype" => $r['filter_fueltype'],
			"filter_pskw" => $r['filter_pskw'],
			"filter_motorcode" => $r['filter_motorcode'],
			"search_vehicle" => $r['search_vehicle'],
			"all_manufacturers" => ($r['all_manufacturers'] == 1)
		);

		return ($arr);
	}

	function reload_search_history() {
		$arr = array();

		$this->db->query( "SELECT id, text FROM TEC_SEARCH_HISTORY WHERE user_id='" . $_SESSION['c_user_id'] . "' ORDER BY act_time DESC" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$arr[] = array(
				"id" => $r['id'],
				"text" => $r['text']
			);
		} // while

		return ($arr);
	}

	function get_internal_description( $description, $item_no, $brand_id ) {
		$str = '';

		$this->db->query( "SELECT show_internal_description FROM TEC_SETUP WHERE id='1'", "get_internal_description" );
		$setup = $this->db->getNext( "get_internal_description" );
		if( !$setup['show_internal_description'] )
			return ($str);

		if( !$this->db->query( "SELECT description FROM TEC_WEBSERVICE_MAPPING WHERE item_no='" . $item_no . "' AND brand_id='" . $brand_id . "'", "get_internal_description" ) )
			return ($str);

		$r = $this->db->getNext( "get_internal_description" );
		if( $description == $r['description'] )
			return ($str);

		$str = ' <span class="red">(' . $r['description'] . ')</span>';

		return ($str);
	}

	function get_internal_item_no( $description, $item_no, $brand_id ) {
		$str = '';

		$this->db->query( "SELECT show_internal_item_no FROM TEC_SETUP WHERE id='1'", "get_internal_item_no" );
		$setup = $this->db->getNext( "get_internal_item_no" );
		if( !$setup['show_internal_item_no'] )
			return ($str);

		if( !$this->db->query( "SELECT local_item_no FROM TEC_WEBSERVICE_MAPPING WHERE item_no='" . $item_no . "' AND brand_id='" . $brand_id . "'", "get_internal_item_no" ) )
			return ($str);

		$r = $this->db->getNext( "get_internal_item_no" );
		if( $description == $r['local_item_no'] )
			return ($str);

		$str = '<span class="red">' . $r['local_item_no'] . '</span>';

		return ($str);
	}

	function get_universal_article_option( $field ) {
		$option = '<option value=""></option>';

		$this->db->query( "SELECT DISTINCT( " . $field . " ) FROM TEC_ITEMS ORDER BY " . $field . " ASC" );
		while( $this->db->isNext() ) {
			$r = $this->db->getNext();

			$option .= '<option value="' . $r[$field] . '">' . $r[$field] . '</option>';
		} // while

		return ($option);
	}

	function check_item_no( $item_no, $brand_id ) {
		$function = 'getArticleDirectSearchAllNumbersWithState';
		$params = array(
			'articleCountry' => $this->get_search_articleCountry(),
			'lang' => $this->get_search_language(),
			'numberType' => 10,
			'articleNumber' => $item_no,
			'brandId' => $brand_id,
			'provider' => $this->get_search_tecdocid(),
			'searchExact' => true,
			'sortType' => 1
		);
		$request = $this->createRequest( $function, $params );
		$result = $this->callJSON( $function, $request );

		return( isset( $result->data->array ) );
	}

	function sub_module_active() {
		if( !isset( $_SESSION['c_user_id'] ) )
			return( false );

		$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin, sub_created_by_user_id' );
		if( $this->td_setup['sub_module_active'] && ($userinfo['sub_admin'] == 0) && ($userinfo['sub_created_by_user_id'] > 0) )
			return (true);

		return (false);
	}

	function get_vat() {
		$setup = $this->f->load_setup( "TEC_SETUP" );
		$vat = $setup['vat'];

		if( $this->sub_module_active() ) {
			// selbst SubAdmin
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin, sub_created_by_user_id' );
			if( $userinfo['sub_admin'] == 1 )
				$userinfo['sub_created_by_user_id'] = $_SESSION['c_user_id'];

			// Infos von SubAdmin holen
			if( $this->db->query( "SELECT sub_vat_percent FROM CORE_USER_INFO WHERE user_id='" . $userinfo['sub_created_by_user_id'] . "'" ) ) {
				$r = $this->db->getNext();

				if( $r['sub_vat_percent'] > 0 )
					$vat = $r['sub_vat_percent'];
			} // if
		} // if

		return ($vat);
	}

	function get_sub_logo() {
		$logo = "";

		if( $this->td_setup['sub_module_active'] ) {
			// selbst SubAdmin
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'sub_admin, sub_created_by_user_id' );
			if( $userinfo['sub_admin'] == 1 )
				$userinfo['sub_created_by_user_id'] = $_SESSION['c_user_id'];

			// Infos von SubAdmin holen
			if( $this->db->query( "SELECT sub_logo FROM CORE_USER_INFO WHERE user_id='" . $userinfo['sub_created_by_user_id'] . "'" ) ) {
				$r = $this->db->getNext();

				$logo = '<img src="/' . SUBDIR . $r['sub_logo'] . '" style="width: 125px;">';
			} // if
		} // if

		return ($logo);
	} // get_sub_logo

	function get_ad_picture_path( $id, &$filename, &$link_url ) {
		$filename = array();
		$link_url = array();

		$today = date( "Y-m-d", time() );
		$_WHERE = '';
		$_LIMIT = ' LIMIT 1';
		if( $id == 8 )
			$_LIMIT = ' ORDER BY id DESC';

		if( isset( $_SESSION['c_user_id'] ) ) {
			$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'banner_group' );
			if( $userinfo['banner_group'] != '' )
				$_WHERE = " AND banner_group='".$userinfo['banner_group']."'";
		} // if

		if( $this->db->query( "
			SELECT filename, link_url
			FROM TEC_BANNER
			WHERE
				banner_position_id='".$id."' AND
				(start_date='1970-01-01' OR start_date='0000-00-00' OR start_date<='".$today."') AND
				(end_date='1970-01-01' OR end_date='0000-00-00' OR end_date>='".$today."')".
				$_WHERE.
				$_LIMIT ) ) {
			while( $this->db->isNext() ) {
				$r = $this->db->getNext();
				$filename[] = "/".SUBDIR.$r['filename'];
				if( ($r['link_url'] != "") && (strpos( $r['link_url'], "http://" ) === false) && (strpos( $r['link_url'], "https://" ) === false) )
					$r['link_url'] = "http://".$r['link_url'];
				$link_url[] = $r['link_url'];
			} // while
		} // if

		return;
	} // get_ad_picture_path

	function get_ad_picture( $strWhich ) {
		$bg = '';
		$img = array();
		$link = array();
		$class = '';

		switch( $strWhich ) {
			case 'banner-center-1':
					$this->get_ad_picture_path( 1, $img, $link );
					$class = 'banner-center';
				break;
			case 'banner-center-2':
					$this->get_ad_picture_path( 2, $img, $link );
					$class = 'banner-left';
				break;
		 	case 'banner-center-3':
		 			$this->get_ad_picture_path( 3, $file, $link );
		 			if( (sizeof( $file ) != 0) && ($file[0] != '') )
		 				$bg = 'background: url('.$file[0].'), #fff; background-repeat: no-repeat; background-position: center top;';
		 			else
		 				$bg = 'background: #fff;';
				break;
			case 'banner-left-sidebar':
					$this->get_ad_picture_path( 4, $img, $link );
					$class = 'banner-left';
				break;
			case 'banner-left-offside':
					$this->get_ad_picture_path( 5, $img, $link );
					$class = 'banner-left-outside';
				break;
			case 'banner-right-offside':
					$this->get_ad_picture_path( 6, $img, $link );
					$class = 'banner-right-outside';
				break;
			case 'banner-login':
					$this->get_ad_picture_path( 7, $file, $link );
					if( (sizeof( $file ) != 0) && ($file[0] != '') )
						$bg = 'background: url('.$file[0].'), #fff; background-repeat: no-repeat; background-position: center top;';
					else
						$bg = 'background: #fff;';
				break;
			case 'banner-list-center':
					$this->get_ad_picture_path( 8, $img, $link );
					$class = '';
				break;
		} // switch

		$str = '';
		for( $i=0; $i<sizeof( $img ); $i++ ) {
			if( $img[$i] != '' ) {
				$str_temp = '<img src="'.$img[$i].'"';
				if( $class != '' )
					$str_temp .= ' class="'.$class.'"';
				$str_temp .= '>';

				if( $link[$i] != "" )
					$str .= '<a href="'.$link[$i].'" target="_blank">'.$str_temp.'</a>';
				else
					$str .= $str_temp;
			} // if
		} //for
		if( $bg != '' )
			$str = $bg;

		return( $str );
	} // get_ad_picture

	function check_left_ad_showing() {
		$allowed_files = array(
			array( "file" => "tec_search.php" ),
			array( "file" => "tec_settings.php" ),
			array( "file" => "list_redirect.php", "list_id" => 31 ),
			array( "file" => "list_redirect.php", "list_id" => 33 ),
		);

		$url = $_SERVER['REQUEST_URI'];
		$file = basename( parse_url( $url, PHP_URL_PATH ) );
		$params = explode( '&', parse_url( $url, PHP_URL_QUERY ) );

		$bShow = false;
		foreach( $allowed_files as $k => $v ) {
			if( $file == $v['file'] ) {
				if( !isset( $v['list_id'] ) )
					$bShow = true;
				else
					foreach( $params as $k2 => $v2 ) {
						if( $v2 == "indiv_list_id=".$v['list_id'] )
							$bShow = true;
					} // foreach
			} // if
		} // foreach

		$userinfo = $this->f->load_user( $_SESSION['c_user_id'], 'user_level' );
		if( ($userinfo['user_level'] >= 990) && ($_SESSION['project_id'] != 4) )
			$bShow = true;

		return( $bShow );
	} // check_left_ad_showing
} // tecdoc
?>