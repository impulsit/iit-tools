<?php
if( file_exists( CLASS_DIR."list_core.php" ) )
	require_once( CLASS_DIR."list_core.php" );
if( file_exists( CLASS_DIR."list_tecdocbase_atit_at.php" ) )
	require_once( CLASS_DIR."list_tecdocbase_atit_at.php" );
if( file_exists( CLASS_DIR."list_basilica_at.php" ) )
	require_once( CLASS_DIR."list_basilica_at.php" );
if( file_exists( CLASS_DIR."list_freeto_biz.php" ) )
	require_once( CLASS_DIR."list_freeto_biz.php" );
if( file_exists( CLASS_DIR."../plugins/list_plugins.php" ) )
	require_once( CLASS_DIR."../plugins/list_plugins.php" );

class list_project_functions {
	static private $instance = null;

	// Projects
	private $cor = null;
	private $tec = null;
	private $bas = null;
	private $fre = null;
	private $plugins = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		if( class_exists( 'list_core' ) )
			$this->cor = list_core::getInstance();
		if( class_exists( 'list_tecdocbase_atit_at' ) )
			$this->tec = list_tecdocbase_atit_at::getInstance();
		if( class_exists( 'list_basilica_at' ) )
			$this->bas = list_basilica_at::getInstance();
		if( class_exists( 'list_freeto_biz' ) )
			$this->fre = list_freeto_biz::getInstance();
		if( class_exists( 'list_plugins' ) )
			$this->plugins = list_plugins::getInstance();
	} // __construct

	function print_menu_info( $list_id ) {
		if( $this->cor !== null ) $this->cor->print_menu_info( $list_id );
		if( $this->tec !== null ) $this->tec->print_menu_info( $list_id );
		if( $this->bas !== null ) $this->bas->print_menu_info( $list_id );
		if( $this->fre !== null ) $this->fre->print_menu_info( $list_id );
		if( $this->plugins !== null ) $this->plugins->print_menu_info( $list_id );
	} // print_menu_info

	function get_spezial_filter( $list_id ) {
		$str = null;
		if( ($str === null) && ($this->cor !== null) ) $str = $this->cor->get_spezial_filter( $list_id );
		if( ($str === null) && ($this->tec !== null) ) $str = $this->tec->get_spezial_filter( $list_id );
		if( ($str === null) && ($this->bas !== null) ) $str = $this->bas->get_spezial_filter( $list_id );
		if( ($str === null) && ($this->fre !== null) ) $str = $this->fre->get_spezial_filter( $list_id );
		if( ($str === null) && ($this->plugins !== null) ) $str = $this->plugins->get_spezial_filter( $list_id );

		return( $str );
	} // get_spezial_filter

	function insert_special_list_line( $list_id, $values, $bLastLine = false ) {
		if( $this->cor !== null ) $this->cor->insert_special_list_line( $list_id, $values, $bLastLine );
		if( $this->tec !== null ) $this->tec->insert_special_list_line( $list_id, $values, $bLastLine );
		if( $this->bas !== null ) $this->bas->insert_special_list_line( $list_id, $values, $bLastLine );
		if( $this->fre !== null ) $this->fre->insert_special_list_line( $list_id, $values, $bLastLine );
	} // insert_special_list_line

	function special_save( $list_id, $primary_key ) {
		if( $this->cor !== null ) $this->cor->special_save( $list_id );
		if( $this->tec !== null ) $this->tec->special_save( $list_id, $primary_key );
		if( $this->bas !== null ) $this->bas->special_save( $list_id );
		if( $this->fre !== null ) $this->fre->special_save( $list_id );
	} // special_save

	function add_special_line_buttons( $list_id, $values ) {
		if( $this->cor !== null ) $this->cor->add_special_line_buttons( $list_id, $values );
		if( $this->tec !== null ) $this->tec->add_special_line_buttons( $list_id, $values );
		if( $this->bas !== null ) $this->bas->add_special_line_buttons( $list_id, $values );
		if( $this->fre !== null ) $this->fre->add_special_line_buttons( $list_id, $values );
		if( $this->plugins !== null ) $this->plugins->add_special_line_buttons( $list_id, $values );
	} // add_special_line_buttons

	function add_special_action_buttons_main( $list_id ) {
		$str = null;
		if( ($str === null) && ($this->cor !== null) ) $str = $this->cor->add_special_action_buttons_main( $list_id );
		if( ($str === null) && ($this->tec !== null) ) $str = $this->tec->add_special_action_buttons_main( $list_id );
		if( ($str === null) && ($this->bas !== null) ) $str = $this->bas->add_special_action_buttons_main( $list_id );
		if( ($str === null) && ($this->fre !== null) ) $str = $this->fre->add_special_action_buttons_main( $list_id );

		return( $str );
	} // add_special_action_buttons_main

	function parse_special_buttons( $list_id, $param ) {
		$str = null;
		if( ($str === null) && ($this->cor !== null) ) $str = $this->cor->parse_special_buttons( $list_id, $param );
		if( ($str === null) && ($this->tec !== null) ) $str = $this->tec->parse_special_buttons( $list_id, $param );
		if( ($str === null) && ($this->bas !== null) ) $str = $this->bas->parse_special_buttons( $list_id, $param );
		if( ($str === null) && ($this->fre !== null) ) $str = $this->fre->parse_special_buttons( $list_id, $param );

		return( $str );
	} // parse_special_line_buttons

	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$str = null;

		if( ($str === null) && ($this->cor !== null) ) $str = $this->cor->get_special_functions( $list_id, $function, $primary_key, $c );
		if( ($str === null) && ($this->tec !== null) ) $str = $this->tec->get_special_functions( $list_id, $function, $primary_key, $c );
		if( ($str === null) && ($this->bas !== null) ) $str = $this->bas->get_special_functions( $list_id, $function, $primary_key, $c );
		if( ($str === null) && ($this->fre !== null) ) $str = $this->fre->get_special_functions( $list_id, $function, $primary_key, $c );
		if( ($str === null) && ($this->plugins !== null) ) $str = $this->plugins->get_special_functions( $list_id, $function, $primary_key, $c );

		return( $str );
	} // get_special_functions

	function get_special_field_value( $list_id, $values, $field ) {
		$str = null;

		if( ($str === null) && ($this->tec !== null) ) $str = $this->tec->get_special_field_value( $list_id, $values, $field );

		return( $str );
	} // get_special_field_value

	function change_field_values( &$arrFields ) {
		if( $this->tec !== null )
			$this->tec->change_field_values( $arrFields );
	} // change_field_values

	function set_fields_special( $list_id, &$arrFields  ) {
		if( $this->tec !== null )
			$this->tec->set_fields_special( $list_id, $arrFields );
	} // set_fields_special

	function get_special_select_where( $list_id, $field ) {
		$str = null;

		if( ($str === null) && ($this->plugins !== null) ) $str = $this->plugins->get_special_select_where( $list_id, $field );

		return( $str );
	} // get_special_select_where

	function check_multi_select_link_table( $list_id, $primary_key, $field, $value ) {
		$str = null;

		if( ($str === null) && ($this->plugins !== null) ) $str = $this->plugins->check_multi_select_link_table( $list_id, $primary_key, $field, $value );

		return( $str );
	} // check_multi_select_link_table

	function write_multi_select_link_table( $list_id, $primary_key, $field ) {
		$str = null;

		if( ($str === null) && ($this->plugins !== null) ) $str = $this->plugins->write_multi_select_link_table( $list_id, $primary_key, $field );

		return( $str );
	} // write_multi_select_link_table

	function get_select_multi_for_list( $list_id, $primary_key, $field ) {
		$str = null;

		if( ($str === null) && ($this->plugins !== null) ) $str = $this->plugins->get_select_multi_for_list( $list_id, $primary_key, $field );

		return( $str );
	} // get_select_multi_for_list

	function write_changelog( $list_id, $primary_key, $table_name, $old_values, $new_values ) {
		if( $this->plugins !== null ) $str = $this->plugins->write_changelog( $list_id, $primary_key, $table_name, $old_values, $new_values );
	} // write_changelog
}
?>