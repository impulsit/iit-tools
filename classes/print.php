<?php
require_once( 'config_data.php' );
if( file_exists( CLASS_DIR.'tcpdf/tcpdf.php' ) )
	require_once( CLASS_DIR.'tcpdf/tcpdf.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'report.php' );
require_once( CLASS_DIR.'functions.php' );

// Extend the TCPDF class to create custom Header and Footer
if( class_exists( 'TCPDF' ) ) {
	class MYPDF extends TCPDF {

		//Page header
		public function Header() {
			global $report_id;

			if( $report_id == 32 ) {
				// AMS
				$image_file = CLASS_DIR.'templates/reports/pics/ams_logo.png';
				$this->Image( $image_file, 86, 8, 35, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false );
			} else {
				// Logo
				$image_file = PIC_DIR.PDF_HEADER_LOGO;
				$this->Image( $image_file, 151, 8, 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false );

				// Line
				$red    = array( 'width' => 1, 'color' => array( 226,  54,  19));
				$blue   = array( 'width' => 1, 'color' => array(  48,  40, 130) );
				$yellow = array( 'width' => 1, 'color' => array( 255, 236,   0) );

				$this->Line(   7,  30,  72,  30, $blue);
				$this->Line(  72,  30, 137,  30, $red);
				$this->Line( 137,  30, 202,  30, $yellow);
			} // else
		}

		// Page footer
		public function Footer() {
			global $report_id;

			if( $report_id == 32 ) {
				// AMS
				// Set font
				$this->SetFont('helvetica', '', 6);

				$str = "1 Zeiten, an denen der/die KursteilnehmerIn bei und unter Aufsicht des Kursinstituts am Kurs teilnimmt.\n2 Zeiten, die dem Erwerb von praktischen Erfahrungen als Voraussetzung für einen Ausbildungsabschluss dienen.";
				$this->MultiCell( 0, 10, $str, 0, 'L' );

				// Line
				$black    = array( 'width' => 0.1, 'color' => array( 0, 0, 0 ));
				$this->Line(   7, 280,  72, 280, $black);
			} else {
				// Position at 15 mm from bottom
				$this->SetY(-15);
				// Set font
				$this->SetFont('helvetica', '', 8);

				$str = 'Basilica die Organisationsplattform für Bildung | Hirschstettner Straße 86 | 1220 Wien | 01 / 89 00 912 | http://www.basilica.at | office@basilica.at';
				$this->Cell(0, 10, $str, 0, false, 'C', 0, '', 0, false, 'T', 'M');

				// Line
				$red    = array( 'width' => 1, 'color' => array( 226,  54,  19));
				$blue   = array( 'width' => 1, 'color' => array(  48,  40, 130) );
				$yellow = array( 'width' => 1, 'color' => array( 255, 236,   0) );

				$this->Line(   7, 280,  72, 280, $blue);
				$this->Line(  72, 280, 137, 280, $red);
				$this->Line( 137, 280, 202, 280, $yellow);
			} // if
		}
	}
} // if

class druck {
	static private $instance = null;
	private $report = null;
	private $f = null;
	public $bericht_id = 0;
	public $type = '';
	public $teilnehmer_id = 0;
	public $kurs_id = 0;
	public $trainer_id = 0;
	public $template = '';
	public $pdf = null;
	public $db = null;
	public $filename = '';
	public $filecontens = '';
	public $rechnung_id = 0;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->report = report::getInstance();
		$this->f = functions::getInstance();
	} // __construct

	private function __clone(){
	} // __clone

	public function __destruct() {
	} // __destruct

	function replace_variables( $html ) {
		$monate = array( 1=>"Januar", 2=>"Februar", 3=>"März", 4=>"April", 5=>"Mai", 6=>"Juni", 7=>"Juli", 8=>"August", 9=>"September", 10=>"Oktober", 11=>"November", 12=>"Dezember" );
		$wochentage = array( "mo" => "Montag", "di" => "Dienstag", "mi" => "Mittwoch", "do" => "Donnerstag", "fr" => "Freitag", "sa" => "Samstag", "so" => "Sonntag" );

		$html = str_replace( "[system.tag]", date( "d", time() ), $html );
		$html = str_replace( "[system.monat]", utf8_decode( $monate[date( "n", time() )] ), $html );
		$html = str_replace( "[system.jahr]", date( "Y", time() ), $html );
		$html = str_replace( "[system.now]", date( "d.m.Y", time() ), $html );

		$herkunft_id = 0;
		if( $this->teilnehmer_id > 0 ) {
			if( $this->bericht_id == $this->report->get_report_id( "REP_ERINNERUNGSBRIEF" ) ) {
				$this->db->update( "BAS_TEILNEHMER", array( "erinnerung_gedruckt" => 1 ), "teilnehmer_id='".$this->teilnehmer_id."'" );
				$this->db->commit();
			} // if
			if( $this->bericht_id == $this->report->get_report_id( "REP_ERINNERUNGSBRIEF_ADR" ) ) {
				$this->db->update( "BAS_TEILNEHMER", array( "adr_erinnerung_gedruckt" => 1 ), "teilnehmer_id='".$this->teilnehmer_id."'" );
				$this->db->commit();
			} // if

			$this->db->query( "
				SELECT *, a.title AS anrede, a.email_anrede, h.hk_adresskopf AS waff_adresskopf
				FROM BAS_TEILNEHMER AS t
				LEFT JOIN GLOBAL_ANREDE AS a ON (a.id=t.anrede_id)
				LEFT JOIN BAS_HERKUNFT AS h ON (h.id=t.herkunft_id)
				WHERE t.teilnehmer_id='".$this->teilnehmer_id."'", "replace_variables" );
			$r = $this->db->getNext( "replace_variables" );

			$fields = array( "vorname", "nachname", "svn", "adresse", "plz", "ort", "ausbildung", "geb_datum", "geb_ort", "telefon", "anrede", "email_anrede", "waff_lfd_nr" );
			foreach( $fields as $k => $v ) {
				if( $v == "geb_datum" ) $r[$v] = date( "d.m.Y", strtotime( $r[$v] ) );
				if( $v == "email_anrede" ) $r[$v] = str_replace( "%1", utf8_decode( $r['vorname'].' '.$r['nachname'] ), $r[$v] );

				$html = str_replace( "[teilnehmer.".$v."]", utf8_decode( $r[$v] ), $html );
				$html = str_replace( "[t.".$v."]", utf8_decode( $r[$v] ), $html );
			} // foreach
			$herkunft_id = $r['herkunft_id'];

			// AMS Betreuer
			$this->db->query( "
				SELECT a.email_anrede, k.name
				FROM BAS_AMS_KONTAKTE AS k
				LEFT JOIN GLOBAL_ANREDE AS a ON (a.id=k.anrede_id)
				WHERE k.id='".$r['ams_betreuer_id']."'", "replace_variables" );
			$r2 = $this->db->getNext( "replace_variables" );

			$html = str_replace( "[ams.email_anrede]", str_replace( "%1", utf8_decode( $r2['name'] ), $r2['email_anrede'] ), $html );

			// Adresskopf
			if( $r['adresskopf'] != "" ) {
				$r['adresskopf'] = str_replace( '<br>', '\par ', $r['adresskopf'] );
				$r['adresskopf'] = str_replace( '<br />', '\par ', $r['adresskopf'] );
				$html = str_replace( "[t.adresskopf]", utf8_decode( $r['adresskopf'] ), $html );
			} else {
				$kopf =
					$r['vorname'].' '.$r['nachname'].' SVNR '.$r['svn'].'\par '.
					$r['adresse'].'\par '.
					$r['plz'].' '.$r['ort'];
				$html = str_replace( "[t.adresskopf]", utf8_decode( $kopf ), $html );
			} // else

			// WAFF Adresskopf
			if( $r['waff_adresskopf'] != "" ) {
				$r['waff_adresskopf'] = str_replace( '<br>', '\par ', $r['waff_adresskopf'] );
				$r['waff_adresskopf'] = str_replace( '<br />', '\par ', $r['waff_adresskopf'] );

				$html = str_replace( "[waff.adresskopf]", utf8_decode( $r['waff_adresskopf'] ), $html );
			} // if
		} // if

		if( $this->trainer_id > 0 ) {
			$this->db->query( "
				SELECT *
				FROM BAS_TRAINER AS t
				WHERE t.trainer_id='".$this->trainer_id."'", "replace_variables" );
			$r = $this->db->getNext( "replace_variables" );

			$fields = array( "nachname", "vorname", "adresse", "svn", "plz", "ort", "bank", "bic", "iban" );
			foreach( $fields as $k => $v ) {
				$html = str_replace( "[tr.".$v."]", utf8_decode( $r[$v] ), $html );
			} // foreach

			// Kurs
			$this->db->query( "SELECT abw_trainer_stundensatz, stunden_pro_tag FROM BAS_KURSE WHERE kurs_id='".$this->kurs_id."'", "replace_variables" );
			$r3 = $this->db->getNext( "replace_variables");

			// Anzahl Stunden
			$this->db->query( "SELECT SUM(IF(manuelle_stunden=0,".$r3['stunden_pro_tag'].",manuelle_stunden)) AS anz FROM BAS_TRAINER_KURSE WHERE trainer_id='".$this->trainer_id."' AND kurs_id='".$this->kurs_id."'", "replace_variables" );
			$r2 = $this->db->getNext( "replace_variables" );

			// Pauschale?
			$this->db->query( "SELECT SUM(pauschale) AS pauschale FROM BAS_TRAINER_KURSE WHERE trainer_id='".$this->trainer_id."' AND kurs_id='".$this->kurs_id."'", "replace_variables" );
			$r4 = $this->db->getNext( "replace_variables" );

			if( $r4['pauschale'] > 0 ) {
				$decHonorar = number_format( $r4['pauschale'], 2, ',', '.' );
			} else {
				if( $r3['abw_trainer_stundensatz'] > 0 )
					$r['stundensatz'] = $r3['abw_trainer_stundensatz'];

				$decHonorar = number_format( $r['stundensatz'] * $r2['anz'], 2, ',', '.' );
			} // else

			$html = str_replace( "[tr.honorar]", $decHonorar, $html );
			$html = str_replace( "[tr.stunden]", ($r3['stunden_pro_tag'] * $r2['anz']), $html );
		} // if

		if( $this->kurs_id > 0 ) {
			$this->db->query( "
				SELECT k.*, kt.title AS kurstyp, kt.weiterbildung_setzen, kt.kursbestaetigung, kt.adr_weiterbildung_setzen
				FROM BAS_KURSE AS k
				LEFT JOIN BAS_KURSTYP AS kt ON (kt.id=k.kurstyp_id)
				WHERE k.kurs_id='".$this->kurs_id."'", "replace_variables" );
			$r = $this->db->getNext( "replace_variables" );

			if( ($r['weiterbildung_setzen'] == 1) && ($r['kursbestaetigung'] == $this->bericht_id) ) {
				$this->db->update( "BAS_TEILNEHMER", array( "letzter_kurs_c95_d95" => date( "Y-m-d", time() ), "erinnerung_gedruckt" => 0 ), "teilnehmer_id='".$this->teilnehmer_id."'" );
				$this->db->commit();
			} // if
			if( ($r['adr_weiterbildung_setzen'] == 1) && ($r['kursbestaetigung'] == $this->bericht_id) ) {
				$this->db->update( "BAS_TEILNEHMER", array( "letzter_adr_kurs" => date( "Y-m-d", time() ), "adr_erinnerung_gedruckt" => 0 ), "teilnehmer_id='".$this->teilnehmer_id."'" );
				$this->db->commit();
			} // if

			if( $this->bericht_id == $this->report->get_report_id( "REP_ERLAGSCHEIN" ) )
				$r['startdatum'] = date( "d.m.Y", strtotime( $r['startdatum']." -7 days" ) );

			$r['gesamt_kosten'] = $r['kosten'] + $r['kosten_wirtschaftskammer'];
			$gesamt = $r['gesamt_kosten'];
			$gebuehr = $r['kosten_wirtschaftskammer'];

			$r['kurs_kosten'] = $r['kosten'];

			// Herkunft WAFF
			$herkunft_id = 10;
			$waff_kosten = 0;
			$waff_teilnehmer = 0;
			if( $this->bericht_id == $this->report->get_report_id( "REP_ERLAGSCHEIN_TEILNEHMER" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_WAFF_DIREKT" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_WAFF_TEILNEHMER" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG" )
				) {
				if( ($herkunft_id == 10) || ($herkunft_id == 11) ) {
					// WAFF Kosten
					$setup = $this->f->load_setup( "BAS_SETUP" );

					$waff_90_prozent = round( ($gesamt - $gebuehr) * ($setup['waff_prozentsatz'] / 100), 2 );
					if( $waff_90_prozent > ($setup['waff_max_limit'] - $gebuehr) )
						$waff_kosten = $setup['waff_max_limit'] - $gebuehr;
					else
						$waff_kosten = $waff_90_prozent;

					$waff_teilnehmer = ($gesamt - $gebuehr) - $waff_kosten;
					$r['kosten'] = $waff_teilnehmer;
					$r['gesamt_kosten'] = $waff_teilnehmer;

					// Spezieller Preis
					if( $this->db->query( "
						SELECT foerderbetrag, eigenleistung
						FROM BAS_KURSTEILNEHMER
						WHERE kurs_id='".$this->kurs_id."' AND teilnehmer_id='".$this->teilnehmer_id."'", "replace_variables" ) ) {
						$r2 = $this->db->getNext( "replace_variables" );

						if( $r2['foerderbetrag'] > 0 ) {
							$waff_kosten = $r2['foerderbetrag'];
							$waff_teilnehmer = $r2['eigenleistung'];
							$r['kosten'] = $waff_teilnehmer;
							$r['gesamt_kosten'] = $waff_teilnehmer;
						} // if
					} // if
				} // if
			} // if

			$fields = array( "startdatum", "enddatum", "startzeit", "endzeit", "kosten", "kosten_wirtschaftskammer", "gesamt_kosten", "nummer", "kurstyp", "dauer", "dauer_gesamt", "wochen",
					"mo_startzeit", "di_startzeit", "mi_startzeit", "do_startzeit", "fr_startzeit", "sa_startzeit", "so_startzeit",
					"mo_endzeit", "di_endzeit", "mi_endzeit", "do_endzeit", "fr_endzeit", "sa_endzeit", "so_endzeit",
			);
			foreach( $fields as $k => $v ) {
				if( strpos( $v, "datum" ) !== false ) $r[$v] = date( "d.m.Y", strtotime( $r[$v] ) );
				if( strpos( $v, "zeit" ) !== false ) $r[$v] = date( "H:i", strtotime( $r[$v] ) );
				if( ($v == "kosten") || ($v == "kosten_wirtschaftskammer") || ($v == "gesamt_kosten") ) $r[$v] = number_format( $r[$v], 2, ',', '.' );

				$html = str_replace( "[kurs.".$v."]", utf8_decode( $r[$v] ), $html );
				$html = str_replace( "[k.".$v."]", utf8_decode( $r[$v] ), $html );

				if( $r['lehrabschluss'] == 1 ) $str = utf8_decode( 'inkl. Prüfungsgebühr WKW' ); else $str = '';
				$html = str_replace( "[k.wkw_text]", $str, $html );
			} // foreach

			// Min, Max Wochentag
			foreach( array( "mo", "di", "mi", "do", "fr", "sa", "so" ) as $k => $v ) {
				if( isset( $r[$v] ) && ($r[$v] == 1) ) {
					$html = str_replace( "[min_wt]", $wochentage[$v], $html );
					break;
				} // if
			} // foreach
			foreach( array( "so", "sa", "fr", "do", "mi", "di", "mo" ) as $k => $v ) {
				if( isset( $r[$v] ) && ($r[$v] == 1) ) {
					$html = str_replace( "[max_wt]", $wochentage[$v], $html );
					break;
				} // if
			} // foreach

			// monat_bezahlt
			if( $this->db->query( "
				SELECT t.bezahlt_am, k.enddatum
				FROM BAS_KURSTEILNEHMER AS t
				LEFT JOIN BAS_KURSE AS k ON (k.kurs_id=t.kurs_id)
				WHERE t.kurs_id='".$this->kurs_id."' AND t.teilnehmer_id='".$this->teilnehmer_id."' AND t.status_id='2'", "replace_variables" ) ) {
				$r = $this->db->getNext( "replace_variables" );

				$fields = array( "bezahlt_am" );
				foreach( $fields as $k => $v ) {
					if( $v == "bezahlt_am" ) {
						if( $r[$v] == '1970-01-01' ) {
							$r[$v] = $r['enddatum'];
						} // if
						$r[$v] = utf8_decode( $monate[(int)date( "m", strtotime( $r[$v] ) )] ).' '.date( "Y", strtotime( $r[$v] ) );
					} // if

					$html = str_replace( "[kurs.".$v."]", $r[$v], $html );
				} // foreach
			} // if

			// Teilnehmer
			if( $this->bericht_id == $this->report->get_report_id( "REP_ANWESENHEITSLISTE" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_PROT_KRAN_STAPLER" ) ||
			    $this->bericht_id == $this->report->get_report_id( "REP_TEILNEHMERLISTE" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_TEILNEHMERLISTE_2" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_WAFF_TEILNEHMERLISTE" ) ||
					$this->bericht_id == $this->report->get_report_id( "REP_ADR_TEILNEHMERLISTE" )
				) {
				set_time_limit( 120 );

				$i = 0;
				$fields = array( "name", "gesch_title", "vorname", "nachname", "svn", "geb_datum", "geb_ort", "adresse", "plz", "ort", "telefon", "herkunft" );
				$fields_adr = array( "", "vorname", "nachname", "geb_datum", "geb_ort", "staatsbuergerschaft", "adresse", "ort", "plz", "telefon", "email", "fuehrerschein_nr", "ausstellende_behoerde", "adr_basis", "adr_auffrischung", "aufbau_tank", "aufbau_klasse_1", "aufbau_klasse_7", "adr_gueltig_bis" );

				$strZusatzfilter = "";
				if( $this->bericht_id == $this->report->get_report_id( "REP_WAFF_TEILNEHMERLISTE" ) )
						$strZusatzfilter = " AND u.herkunft_id='10'";

				$this->db->query( "
					SELECT
						CONCAT( u.nachname, ' ', u.vorname ) AS name,
						g.title AS gesch_title, u.vorname, u.nachname, u.svn, u.geb_datum, u.geb_ort, u.adresse, u.plz, u.ort, u.telefon,
						h.title AS herkunft,
						u.email, u.staatsbuergerschaft, u.fuehrerschein_nr, u.ausstellende_behoerde, u.adr_basis, u.adr_auffrischung, u.aufbau_tank, u.aufbau_klasse_1, u.aufbau_klasse_7, u.adr_gueltig_bis
					FROM BAS_KURSTEILNEHMER AS kt
					LEFT JOIN BAS_TEILNEHMER AS u ON (u.teilnehmer_id=kt.teilnehmer_id)
					LEFT JOIN GLOBAL_GESCHLECHT AS g ON (g.id=u.geschlecht_id)
					LEFT JOIN BAS_HERKUNFT AS h ON (h.id=u.herkunft_id)
					WHERE kt.kurs_id='".$this->kurs_id."' ".$strZusatzfilter."
					ORDER BY u.nachname ASC, u.vorname ASC", "replace_variables" );
				while( $this->db->isNext( "replace_variables" ) ) {
					$r = $this->db->getNext( "replace_variables" );

					$i++;

					foreach( $fields as $k => $v ) {
						if( $v == "geb_datum" ) $r[$v] = date( "d.m.Y", strtotime( $r[$v] ) );

						$html = str_replace( "[t.".$v."_".$i."]", utf8_decode( $r[$v] ), $html );
						$html = str_replace( "[teilnehmer.".$v."_".$i."]", utf8_decode( $r[$v] ), $html );
					} // foreach

					if( $this->bericht_id == $this->report->get_report_id( "REP_ADR_TEILNEHMERLISTE" ) ) {
						foreach( $fields_adr as $k2 => $v2 ) {
							if( $v2 == "adr_gueltig_bis" ) $r[$v2] = date( "d.m.Y", strtotime( $r[$v2] ) );
							if( ($v2 == "adr_basis") || ($v2 == "adr_auffrischung") || ($v2 == "aufbau_tank") || ($v2 == "aufbau_klasse_1") || ($v2 == "aufbau_klasse_7") ) {
								if( $r[$v2] == 1 ) $r[$v2] = "X"; else $r[$v2] = "";
							} // if

							if( $k2 > 0 )
								$html = str_replace( "[t.f".$k2."_".$i."]", utf8_decode( $r[$v2] ), $html );
						} // foreach
					} // if
				} // while

				for( $j=$i; $j<=50; $j++ ) {
					foreach( $fields as $k => $v ) {
						$html = str_replace( "[t.".$v."_".$j."]", "", $html );
						$html = str_replace( "[teilnehmer.".$v."_".$j."]", "", $html );
					} // foreach
					foreach( $fields_adr as $k2 => $v2 ) {
						$html = str_replace( "[t.f".$k2."_".$j."]", "", $html );
					} // foreach
				} // for
			} // if

			// Wochentage-Datum für Kurs
			$i = 0;
			$this->db->query( "SELECT * FROM BAS_KURSE WHERE kurs_id='".$this->kurs_id."'", "replace_variables" );
			$k = $this->db->getNext( "replace_variables" );

			$bOverflow = false;
			$currDate = 0;
			while( !$bOverflow && (strtotime( $currDate ) <= strtotime( $k['enddatum'])) ) {
				$i++;

				if( $i > 40 ) $bOverflow = true;

				if( $currDate == 0 ) $currDate = $k['startdatum']; else $currDate = date( "Y-m-d", strtotime( $currDate.' +1 days' ) );

				$j = 0;
				$bShow = false;
				while( !$bShow && (strtotime( $currDate ) <= strtotime( $k['enddatum']) ) && !$bOverflow ) {
					$j++;
					if( $j > 100 ) $bOverflow = true;

					$weekday = date( "w", strtotime( $currDate ) );
					if( ($weekday == 0) && ($k['so'] == 1) ) $bShow = true;
					if( ($weekday == 1) && ($k['mo'] == 1) ) $bShow = true;
					if( ($weekday == 2) && ($k['di'] == 1) ) $bShow = true;
					if( ($weekday == 3) && ($k['mi'] == 1) ) $bShow = true;
					if( ($weekday == 4) && ($k['do'] == 1) ) $bShow = true;
					if( ($weekday == 5) && ($k['fr'] == 1) ) $bShow = true;
					if( ($weekday == 6) && ($k['sa'] == 1) ) $bShow = true;

					if( !$bShow )
						$currDate = date( "Y-m-d", strtotime( $currDate.' +1 days') );
				} // while

				if( strtotime( $currDate ) <= strtotime( $k['enddatum']) ) {
					$strDate = date( "d.m.", strtotime( $currDate ) );
					$html = str_replace( "[d.d_".$i."]", $strDate, $html );
				} // if
			} // while
			for( $j=$i; $j<=48; $j++ ) {
				$html = str_replace( "[d.d_".$j."]", "", $html );
			} // for

			// Checkboxen für Wochentage
			$fields = array( "mo", "di", "mi", "do", "fr", "sa", "so" );
			foreach( $fields as $k2 => $v ) {
				if( $k[$v] == 1 ) {
					$new = "\u9746?";
					$new_zeit = date( "H:i", strtotime( $k[$v.'_startzeit'] ) )." - ".date( "H:i", strtotime( $k[$v.'_endzeit'] ) );
				}	else {
					$new = "\u9744?";
					$new_zeit = "";
				} // else
				$html = str_replace( "[c.".$v."]", $new, $html );
				$html = str_replace( "[c.".$v."_zeit]", $new_zeit, $html );
			} // foreach

			// WAFF Kosten
/*
			$setup = $this->f->load_setup( "BAS_SETUP " );

			$waff_kosten = $gesamt;
			if( $waff_kosten > $setup['waff_max_limit'] )
				$waff_kosten = $setup['waff_max_limit'];
			$waff_kosten = round( $waff_kosten * ($setup['waff_prozentsatz'] / 100), 2 ) - $gebuehr;

			$waff_teilnehmer = ($gesamt - $gebuehr) - $waff_kosten;
*/
			$html = str_replace( "[waff.kosten]", number_format( $waff_kosten, 2, ",", "." ), $html );
			$html = str_replace( "[waff.kosten_tn]", number_format( $waff_teilnehmer, 2, ",", "." ), $html );
		} // if

		// Rechnungsnummer
		if( ($this->bericht_id == $this->report->get_report_id( "REP_AMS_RECHNUNG" )) ||
				($this->bericht_id == $this->report->get_report_id( "REP_WAFF_LAP_RECHNUNG" )) ||
				($this->bericht_id == $this->report->get_report_id( "REP_WAFF_DIREKT" )) ||
				($this->bericht_id == $this->report->get_report_id( "REP_WAFF_TEILNEHMER" )) ) {
			if( $this->bericht_id == $this->report->get_report_id( "REP_WAFF_TEILNEHMER" ) ) $teilnehmer = 1; else $teilnehmer = 0;

			if( $this->db->query( "SELECT id, re_nr, datum FROM BAS_AMS_RECHNUNGEN WHERE teilnehmer_id='".$this->teilnehmer_id."' AND kurs_id='".$this->kurs_id."' AND teilnehmer='".$teilnehmer."' AND bericht_id='".$this->bericht_id."'", "replace_variables" ) ) {
				$r = $this->db->getNext( "replace_variables" );

				$this->rechnung_id = $r['id'];
				$nr = 'AR'.date( "Y", strtotime( $r['datum'] ) )."-".str_pad( $r['re_nr'], 4, "0", STR_PAD_LEFT );
			} else {
				$this->db->begin();
				$this->db->query( "SELECT MAX( re_nr ) AS re_nr FROM BAS_AMS_RECHNUNGEN WHERE YEAR( datum )='".date( "Y", time() )."' FOR UPDATE", "replace_variables" );
				$r = $this->db->getNext( "replace_variables" );

				$this->rechnung_id = $this->db->insert( "BAS_AMS_RECHNUNGEN", array(
					"teilnehmer_id" => $this->teilnehmer_id,
					"kurs_id" => $this->kurs_id,
					"re_nr" => $r['re_nr'] + 1,
					"datum" => date( "Y-m-d", time() ),
					"teilnehmer" => $teilnehmer,
					"bericht_id" => $this->bericht_id
				) );
				$this->db->commit();

				$nr = 'AR'.date( "Y", time() )."-".str_pad( ($r['re_nr'] + 1), 4, "0", STR_PAD_LEFT );
			} // else

			$html = str_replace( "[rechnung.nr]", $nr, $html );
		} // if
/*
		// WAFF
		if( ($this->bericht_id == $this->report->get_report_id( "REP_WAFF_DIREKT" )) || ($this->bericht_id == $this->report->get_report_id( "REP_WAFF_TEILNEHMER" )) ) {
			if( $this->db->query( "SELECT id, re_nr, datum FROM BAS_WAFF_RECHNUNGEN WHERE teilnehmer_id='".$this->teilnehmer_id."' AND kurs_id='".$this->kurs_id."'", "replace_variables" ) ) {
				$r = $this->db->getNext( "replace_variables" );

				$this->rechnung_id = $r['id'];
				$nr = 'WAFF'.date( "Y", strtotime( $r['datum'] ) )."-".str_pad( $r['re_nr'], 4, "0", STR_PAD_LEFT );
			} else {
				$this->db->begin();
				$this->db->query( "SELECT MAX( re_nr ) AS re_nr FROM BAS_WAFF_RECHNUNGEN WHERE YEAR( datum )='".date( "Y", time() )."' FOR UPDATE", "replace_variables" );
				$r = $this->db->getNext( "replace_variables" );

				$this->rechnung_id = $this->db->insert( "BAS_WAFF_RECHNUNGEN", array(
						"teilnehmer_id" => $this->teilnehmer_id,
						"kurs_id" => $this->kurs_id,
						"re_nr" => $r['re_nr'] + 1,
						"datum" => date( "Y-m-d", time() )
				) );
				$this->db->commit();

				$nr = 'WAFF'.date( "Y", time() )."-".str_pad( ($r['re_nr'] + 1), 4, "0", STR_PAD_LEFT );
			} // else

			$html = str_replace( "[rechnung.nr]", $nr, $html );
		} // if
		*/
		return( $html );
	} // replace_variables

	function set_template() {
		$this->db->query( "
			SELECT k.kurstyp_id, kt.anmeldeformular, kt.kursbestaetigung
			FROM BAS_KURSE AS k
			LEFT JOIN BAS_KURSTYP AS kt ON (kt.id=k.kurstyp_id)
			WHERE k.kurs_id='".$this->kurs_id."'", "set_template" );
		$r = $this->db->getNext( "set_template" );

		// Kursbestätigung genauer bestimmen
		if( $this->bericht_id == $this->report->get_report_id( "REP_KURSBESTAETIGUNG" ) )
			$this->bericht_id = $r['kursbestaetigung'];

		// Anmeldung genauer bestimmen
		if( $this->bericht_id == $this->report->get_report_id( "REP_ANMELDEFORMULAR" ) )
			$this->bericht_id = $r['anmeldeformular'];

		$this->db->query( "SELECT template FROM BAS_REPORTS WHERE id='".$this->bericht_id."'", "set_template" );
		$r = $this->db->getNext( "set_template" );

		$this->template = $r['template'];
	} // set_template

	function set_report( $type, $teilnehmer_id, $kurs_id, $trainer_id, $fragebogen_id, $bericht_id ) {
		global $report_id;

		$report_id = $bericht_id;

		$this->type = $type;
		$this->teilnehmer_id = $teilnehmer_id;
		$this->kurs_id = $kurs_id;
		$this->trainer_id = $trainer_id;
		$this->bericht_id = $bericht_id;
		$this->fragebogen_id = $fragebogen_id;

		$this->set_template();

		if( $this->template == "" )
			return( false );

		$this->template = BASE_DIR.$this->template;

		return( true );
	} // set_report

	function generate_pdf() {
		global $_REPORTS;

		// create new PDF document
		$this->pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$this->pdf->SetCreator( PDF_CREATOR );
		$this->pdf->SetAuthor( TITEL );
		$this->pdf->SetTitle( $_REPORTS[$this->bericht_id] );
		$this->pdf->SetSubject( $_REPORTS[$this->bericht_id] );
		$this->pdf->SetKeywords( $_REPORTS[$this->bericht_id] );

		// set default header data
		$this->pdf->SetHeaderData( PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '');

		// set header and footer fonts
		$this->pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		$this->pdf->SetFont('helvetica', '', 10);

		// add a page
		$this->pdf->AddPage();

		ob_start();
		include( $this->template );
		$html = ob_get_contents();
		ob_end_clean();

		$html = $this->replace_variables( $html );

		// output the HTML content
		$this->pdf->writeHTML($html, true, false, true, false, '');

		// Stempel
		preg_match( "#stempel_y=(.*)#", $html, $match );
		if( isset( $match[1] ) ) {
			$y = $match[1];
			$image_file = CLASS_DIR.'templates/reports/pics/basilica_stempel_transparent.png';
			$this->pdf->Image( $image_file, 20, $y, 70, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false );
		} // if

		// reset pointer to the last page
		$this->pdf->lastPage();
	} // generate_pdf

	function u2rtf( $s ) {
		$umlauts_chars = array( 'ä','ö','ü','Ä','Ö','Ü','ß' );
		$rtf_values = array( "\'e4","\'f6","\'fc","\'c4","\'d6","\'dc","\'df" );

		$s = str_replace($umlauts_chars, $rtf_values, $s);

		return( $s );
	} // u2rtf

	function create_fb() {
		// Header
		$header = file_get_contents( $this->template.'header.rtf' );

		// Schleife über Fragen Gruppen
		$lines = '';
		$this->db->query( "
			SELECT DISTINCT( kurz ), COUNT( fragebogen_id ) AS line, position, trainer_frage
			FROM WHY_FRAGEN
			WHERE fragebogen_id='".$this->fragebogen_id."'
			GROUP BY kurz
			ORDER BY position ASC", "create_fb_1" );
		while( $this->db->isNext( "create_fb_1" ) ) {
			$r1 = $this->db->getNext( "create_fb_1" );

			// Fragen Vorlage laden
			$orig_main = file_get_contents( $this->template.'zeilen_'.$r1['line'].'.rtf' );

			// Trainer laden
			if( $r1['trainer_frage'] == 1 ) {
				$trainer = array();

				$this->db->query( "
					SELECT DISTINCT( k.trainer_id ) AS trainer_id, t.vorname, t.nachname
					FROM BAS_TRAINER_KURSE AS k
					LEFT JOIN BAS_TRAINER AS t ON (t.trainer_id=k.trainer_id)
					WHERE k.kurs_id='".$this->kurs_id."'", "create_fb_2" );
				while( $this->db->isNext( "create_fb_2" ) ) {
					$r2 = $this->db->getNext( "create_fb_2" );

					$trainer[] = $r2['vorname'].' '.$r2['nachname'];
				} // while
			} else
				$trainer = array( "temp" );

			foreach( $trainer as $k => $v ) {
				// Titel ersetzen
				if( $r1['trainer_frage'] == 1 )
					$main = str_replace( "[titel]", $this->u2rtf( $r1['kurz'].' '.$v ), $orig_main );
				else
					$main = str_replace( "[titel]", $this->u2rtf( $r1['kurz'] ), $orig_main );

				// Schleife über Fragen in dieser Gruppe
				$i = 0;
				$this->db->query( "
					SELECT kurz, frage
					FROM WHY_FRAGEN
					WHERE kurz='".$r1['kurz']."'
					ORDER BY position ASC", "create_fb_2" );
				while( $this->db->isNext( "create_fb_2" ) ) {
					$r2 = $this->db->getNext( "create_fb_2" );

					$i++;

					// Titel ersetzen
					$r2['frage'] = html_entity_decode( $r2['frage'] );

					$main = str_replace( "[zeile_".$i."]", $this->u2rtf( $r2['frage'] ), $main );
				} // while

				$lines .= substr( $main, 1, strlen( $main ) - 2 );
			} // foreach
		} // while

		// Footer
		$footer = file_get_contents( $this->template.'footer.rtf' );
		$footer = str_replace( "[footer.text]", $this->u2rtf( 'Für weitere Angaben, Bemerkungen und Hinweise verwenden Sie bitte die Rückseite.' ), $footer );

		$html = '{';
		$html .= substr( $header, 1, strlen( $header ) - 2 );
		$html .= $lines;
		$html .= substr( $footer, 1, strlen( $footer ) - 2 );
		$html .= '}';

		return( $html );
	} // create_fb

	function generate_rtf() {
		// Fragebogen
		if( $this->fragebogen_id > 0 ) {
			$html = $this->create_fb();
		} else {
			// Lade RTF
			$html = file_get_contents( $this->template );
		} // else

		// Ersetze Platzhalter
		$this->filecontens = $this->replace_variables( $html );
	} // generate_rtf

	function convert_filename( $string ) {
		// Replace spaces with underscores and makes the string lowercase
		$string = str_replace (" ", "_", $string);

		$string = str_replace ("..", ".", $string);
		$string = strtolower ($string);

		// Match any character that is not in our whitelist
		preg_match_all ("/[^0-9^a-z^_^.]/", $string, $matches);

		// Loop through the matches with foreach
		foreach ($matches[0] as $value) {
			$string = str_replace($value, "", $string);
		} // foreach

		return( $string );
	} // convert_filename

	function get_teilnehmer( $teilnehmer_id, $bLeadingID = true ) {
		$str = '';

		if( $this->db->query( "SELECT nachname, vorname FROM BAS_TEILNEHMER WHERE teilnehmer_id='".$teilnehmer_id."'", "get_teilnehmer" ) ) {
			$r = $this->db->getNext( "get_teilnehmer" );

			$id = $teilnehmer_id;
			$id = str_pad( $id, 6, "0", STR_PAD_LEFT );
			if( $bLeadingID )
				$str = $id.'-'.$this->convert_filename( $r['nachname'].'_'.$r['vorname'] );
			else
				$str = $r['vorname'].' '.$r['nachname'];
		} // if

		return( $str );
	} // get_teilnehmer

	function get_trainer( $trainer_id ) {
		$str = '';

		if( $this->db->query( "SELECT vorname, nachname FROM BAS_TRAINER WHERE trainer_id='".$trainer_id."'", "get_trainer" ) ) {
			$r = $this->db->getNext( "get_trainer" );

			$id = $trainer_id;
			$id = str_pad( $id, 6, "0", STR_PAD_LEFT );
			$str = $id.'-'.$this->convert_filename( $r['nachname'].' '.$r['vorname'] );
		} // if

		return( $str );
	} // get_trainer

	function get_fragebogen( $fragebogen_id ) {
		$str = '';

		if( $this->db->query( "SELECT titel FROM WHY_FRAGEBOGEN WHERE fragebogen_id='".$fragebogen_id."'", "get_fragebogen" ) ) {
			$r = $this->db->getNext( "get_fragebogen" );

			$id = $fragebogen_id;
			$id = str_pad( $id, 6, "0", STR_PAD_LEFT );
			$str = $id.'-'.$this->convert_filename( $r['titel'] );
		} // if

		return( $str );
	} // get_fragebogen

	function get_kurs( $kurs_id, $bLeadingID = true ) {
		$str = '';

		if( $this->db->query( "SELECT k.nummer FROM BAS_KURSE AS k WHERE k.kurs_id='".$kurs_id."'", "get_kurs" ) ) {
			$r = $this->db->getNext( "get_kurs" );

			if( $bLeadingID )
				$str = $kurs_id.'-'.$this->convert_filename( $r['nummer'] );
			else
				$str = $r['nummer'];
		} // if

		return( $str );
	} // get_kurs

	function get_rechnung_id( $rechnung_id ) {
		$this->db->query( "SELECT re_nr, datum FROM BAS_AMS_RECHNUNGEN WHERE id='".$rechnung_id."'", "get_rechnung_data" );
		$r = $this->db->getNext( "get_rechnung_data" );

		$str = 'AR'.date( "Y", strtotime( $r['datum'] ) )."-".str_pad( $r['re_nr'], 4, "0", STR_PAD_LEFT );

		return( $str );
	} // get_rechnung_id

	function save_file() {
		// Upload Verzeichnis
		$dir = FILE_SAVE_DIR;
		if( !file_exists( $dir ) ) mkdir( $dir );

		// Type Verzeichnis
		$dir .= '/'.$this->type;
		if( !file_exists( $dir ) ) mkdir( $dir );

		// Template Verzeichnis
		$file = $this->template;
		$info = pathinfo( $file );
		if( !isset( $info['extension'] ) )
			$info['extension'] = 'rtf';

		$dir .= '/'.basename( $file,'.'.$info['extension'] );
		if( !file_exists( $dir ) ) mkdir( $dir );

		//Close and output PDF document
		$this->filename = '';
		$this->filename .= $this->get_teilnehmer( $this->teilnehmer_id );

		if( $this->kurs_id > 0 ) {
			if( $this->filename != '' ) $this->filename .= '_';
			$this->filename .= $this->get_kurs( $this->kurs_id );
		} // if

		if( $this->trainer_id > 0 ) {
			if( $this->filename != '' ) $this->filename .= '_';
			$this->filename .= $this->get_trainer( $this->trainer_id );
		} // if

		if( $this->fragebogen_id > 0 ) {
			if( $this->filename != '' ) $this->filename .= '_';
			$this->filename .= $this->get_fragebogen( $this->fragebogen_id );
		} // if

		if( $this->rechnung_id > 0 ) {
			if( $this->filename != '' ) $this->filename .= '_';
			$this->filename .= $this->convert_filename( $this->get_rechnung_id( $this->rechnung_id ) );
		} // if

		$this->filename .= '.rtf';
		$id = $this->db->insert( "BAS_LOG_FILES", array(
			"type" => $this->type,
			"teilnehmer_id" => $this->teilnehmer_id,
			"kurs_id" => $this->kurs_id,
			"trainer_id" => $this->trainer_id,
			"fragebogen_id" => $this->fragebogen_id,
			"bericht_id" => $this->bericht_id,
			"url" => "", // str_replace( BASE_DIR, DOMAIN, $this->filename ),
			"rechnung_id" => $this->rechnung_id,
			"erstellt" => date( "Y-m-d H:i:s", time() ) ) );

		$this->filename = $dir.'/'.str_pad( $id, 6, "0", STR_PAD_LEFT )."_".$this->filename;

		file_put_contents( $this->filename, $this->filecontens );

		$this->db->update( "BAS_LOG_FILES", array(
			"filename" => str_replace( FILE_SAVE_DIR."/", "", $this->filename ),
			"basename" => basename( $this->filename ) ),
			"id='".$id."'" );
		$this->db->commit();
	} // save_pdf

	function load_file( $id ) {
		$this->db->query( "SELECT filename FROM BAS_LOG_FILES WHERE id='".$id."'", "load_file" );
		$r = $this->db->getNext( "load_file" );

		$this->filename = FILE_SAVE_DIR.'/'.$r['filename'];
	} // load_file

	function print_pdf() {
		header('Content-Type: application/pdf');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: attachment; filename="'.basename($this->filename).'"');
		if( ob_get_length() > 0 ) ob_clean();
		flush();

		readfile($this->filename);
		exit;
	} // show_pdf

	function print_rtf() {
		header('Content-Type: application/rtf');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: attachment; filename="'.basename($this->filename).'"');
		if( ob_get_length() > 0 ) ob_clean();
		flush();

		readfile($this->filename);
		exit;
	} // print_rtf

	function open_rtf() {
		header('Content-Type: application/rtf');
		header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
		header('Pragma: public');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
		header('Content-Disposition: attachment; filename="'.basename($this->filename).'"');
		// header('Content-Length: '.filesize( $this->filename ));
		if( ob_get_length() > 0 ) ob_clean();
		flush();

		readfile($this->filename);
		exit;
	} // open_rtf

	function generate_batch_print_file( $in, $count ) {
		$user_info = $this->f->load_user( $_SESSION['c_user_id'], "winword_path" );

		$data = '';

		if( $this->db->query( "SELECT id, filename FROM BAS_LOG_FILES WHERE id IN (".$in.")", "generate_batch_print_file" ) ) {
			$data = '@echo off'.PHP_EOL;
			while( $this->db->isNext( "generate_batch_print_file" ) ) {
				$r = $this->db->getNext( "generate_batch_print_file" );

				$this->db->query( "UPDATE BAS_LOG_FILES SET anzahl_gedruckt=anzahl_gedruckt+1 WHERE id='".$r['id']."'", 1 );
				$this->db->commit();

				for( $i=1; $i<=$count; $i++ )
					$data .= '"'.$user_info['winword_path'].'" "'.FILE_SAVE_DIR_FOR_BATCH."/".$r['filename'].'" /mFilePrintDefault /mFileCloseOrExit'.PHP_EOL;
			} // while
		} // if

		$data .= 'del /F drucken*.cmd'.PHP_EOL;

		if( $data != "" )
			return( $data );
	} // generate_batch_print_file

	function get_rechnung_data( $id ) {
		$arr = array();

		$this->db->query( "SELECT re_nr, datum FROM BAS_AMS_RECHNUNGEN WHERE id='".$id."'", "get_rechnung_data" );
		$r = $this->db->getNext( "get_rechnung_data" );
		$arr['title'] = 'AR'.date( "Y", strtotime( $r['datum'] ) )."-".str_pad( $r['re_nr'], 4, "0", STR_PAD_LEFT );
		$arr['send_filename'] = $arr['title'].".rtf";

		$this->db->query( "SELECT id, filename FROM BAS_LOG_FILES WHERE rechnung_id='".$id."'", "get_rechnung_data" );
		$r = $this->db->getNext( "get_rechnung_data" );
		$arr['content'] = @file_get_contents( FILE_SAVE_DIR.'/'.$r['filename'] );
		$arr['local_filename'] = FILE_SAVE_DIR.'/'.$r['filename'];
		$arr['rechnung_id'] = $r['id'];

		return( $arr );
	} // get_rechnung_data

} // druck
?>