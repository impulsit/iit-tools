<?php
if( !file_exists( dirname( __FILE__ ).'/location.txt' ) ) {
	echo 'FATAL ERROR: project location not found.';
	exit;
} // if
$_location = file_get_contents( dirname( __FILE__ ).'/location.txt' );

if( !file_exists( dirname( __FILE__ ).'/classes/config_data/location_'.$_location.'.php' ) ) {
	echo 'FATAL ERROR: project configuration not found.';
	exit;
} // if
include( dirname( __FILE__ ).'/classes/config_data/location_'.$_location.'.php' );

header( "location: ".DOMAIN."admin/login_login.php" );
?>