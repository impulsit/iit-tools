<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

if( isset( $_GET['project_id'] ) )
	$_SESSION['project_id'] = $_GET['project_id'];

require_once( CLASS_DIR."templates/header.php" );

$userinfo = $f->load_user( $c_user_id, "show_iframe" );
$setup = $f->load_setup( "CORE_SETUP" );

if( ($setup['iframe_link'] != "") && ($userinfo['show_iframe'] == 1) ) {
	echo '
	<div id="content_scroll">
		<div style="text-align: center; width: 100%; left: 0;">
			<iframe src="'.$setup['iframe_link'].'" frameborder="0" style="width:820px; height: 620px;"></iframe>
		</div>
	</div>';
	$db->update( "CORE_USER_INFO", array( "show_iframe" => 0 ), "user_id='".$c_user_id."'" );
	$db->commit();
} // if

require_once( CLASS_DIR."templates/footer.php" );

if( isset( $_SESSION['project_id'] ) && ($_SESSION['project_id'] == 4) ) {
	?>
	<script>
	$(function() {
		window.location.href = "/<?php  echo SUBDIR; ?>admin/tec_search.php?project_id=4";
	})
	</script>
	<?
	exit;
} // if
?>