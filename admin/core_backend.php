<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['project_id'] );
unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );

?>
<script>
$(function() {
	cache_clear();
});
</script>
<?php
if( isset( $_SESSION['project_id'] ) ) {
	switch( $_SESSION['project_id'] ) {
		case 4:
				?>
				<script>
				$(function() {
					window.location.href = "/<?php echo SUBDIR; ?>admin/tec_search.php?project_id=4";
				});
				</script>
				<?
			break;
		default:
				?>
				<script>
				$(function() {
					window.location.href = "/<?php echo SUBDIR; ?>admin/core_entry.php?project_id=<?php echo $_SESSION['project_id']; ?>";
				});
				</script>
				<?
			break;
	}
	exit;
} // if

require_once( CLASS_DIR."templates/footer.php" );
?>