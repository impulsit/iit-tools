<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );

$list->set_list( LIST_CORE_PROJECTS );
$list->parse_commands();
$list->print_menu();
$list->print_list();

require_once( CLASS_DIR."templates/footer.php" );
?>