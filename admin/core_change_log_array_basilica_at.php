<?php
$project = array( 2 );
$update = array(
		"4.34" => array(
				"date" => "",
				"items" => array(
						"Drucken und Öffnen: Anpassung ob_clean, flush",
				)
		),
		"4.33" => array(
				"date" => "10.01.2019",
				"items" => array(
						"Kurse Vorlagen: neue Liste, Funktionalität",
						"Kurse: Neu: bei Auswahl von Kurstyp kopieren Daten von Kurse Vorlagen",
						"Kurse: neues Feld 'Kurs nicht auf Homepage', Anpassung API Homepage",
						"Kursteilnehmer: bei 'Teilnehmer Name' Direkt-Edit eingebaut",
						"Listen: Drucken der aktuellen Seite nun möglich",
				)
		),
		"4.13" => array(
				"date" => "01.10.2018",
				"items" => array(
						"Anpassung API: Startzeit für neue Bilder (Abendkurse)",
				)
		),
		"4.07" => array(
				"date" => "04.06.2018",
				"items" => array(
						"Wordpress API Anpassung auf https",
				)
		),
		"4.03" => array(
				"date" => "12.03.2018",
				"items" => array(
						"Neue Funktion für Teilnehmer ADR: Bericht, Drucken, Menü",
						"Neuer Erinnerungsbrief für ADR",
						"Monate Einstellung in Setup",
				)
		),
		"3.95" => array(
				"date" => "14.12.2017",
				"items" => array(
						"neue Excel Vorlage für WAFF, Umstellung auf xlsx Datei, neue Struktur in Vorlage",
				)
		),
		"3.91" => array(
				"date" => "27.11.2017",
				"items" => array(
						"neue Zeiten für Template AMS Kurszeitbestätigung",
						"Neuer Kurs: Zeiten mit 00:00 vorbelegen",
				)
		),
		"3.88" => array(
				"date" => "22.11.2017",
				"items" => array(
						"Kurszeiten pro Tag",
						"Template Anpassung AMS Kostenvoranschlag",
				)
		),
		"3.85" => array(
				"date" => "17.10.2017",
				"items" => array(
						"Anwesenheitsliste: Datumsberechnung verbessert",
						"neue Templates: AMS_Rechnung, WAFF_direkt_Rechnung, WAFF_teilnehmer_Zahlungsbestätigung",
				)
		),
		"3.81" => array(
				"date" => "24.09.2017",
				"items" => array(
						"Neue Formulare: Friseur, Maurer",
				)
		),
		"3.77" => array(
				"date" => "26.06.2017",
				"items" => array(
						"Für alle Rechnungsformulare eigene Rechnungsnummer vergeben",
				)
		),
		"3.74" => array(
				"date" => "08.05.2017",
				"items" => array(
						"Trainer zuordnen: neue Spalte Pauschale",
						"Anpassung Berechung der Kosten auf Werkvertrag, Honorarnote, Bildschirm (Ausgaben)"
				)
		),
		"3.68" => array(

				"date" => "17.03.2017",
				"items" => array(
						"neue Berichte: WAFF Teilnehmerliste, WAFF LAP Rechnung",
						"geändert: Zahlungsbestätigung"
				)
		),
	"3.53" => array(
		"date" => "14.12.2016",
		"items" => array(
			"Kurs API: neue Kurse und Zuordnung Bilder/PDFs",
			"neue Templates für Einzelhandel, Maurer",
		)
	),
	"3.42" => array(
		"date" => "27.09.2016",
		"items" => array(
			"Angleichen Texte in Druckmenü und Kursmenü",
			"neues Template Schalungsbau",
			"Anpassung Webseite Sync API",
		)
	),
	"3.40" => array(
		"date" => "19.09.2016",
		"items" => array(
			"neue Felder bei Kurs - Teilnehmer anzeigen: genehmigter Förderbetrag, Eigenleistung",
			"Druck: Wenn diese Felder gesetzt auf Berichten drucken: REP_ERLAGSCHEIN_TEILNEHMER, REP_WAFF_DIREKT, REP_WAFF_TEILNEHMER, REP_ZAHLUNGSBESTAETIGUNG",
		)
	),
	"3.27" => array(
		"date" => "12.07.2016",
		"items" => array(
			"neuer Bericht für Zahlungsbestätigung komplett",
			"Anpassung RollOver für Einnahmen/Ausgaben",
			"Anpassung Rechnungstemplates UID Nr.",
			"Statistik neue Spalte Datum",
		)
	),
	"3.22" => array(
		"date" => "04.07.2016",
		"items" => array(
			"Webseite Sync -> Gesamtkosten als Preis senden (+Kosten Prüfung)",
			"Wordpress API: Korrektur wegen Sonderzeichen /",
			"Umbenennen von Kostenfeldern",
			"Anpassung MouseOver von Kosten Gesamt/Einnahmen/Ausgaben",
			"Statistik Teilnehmer Kurse",
			"Anpassung Berechnung WAFF 90/10",
		)
	),
	"3.17" => array(
		"date" => "17.03.2016",
			"items" => array(
				"Teilnehmerliste: Spalte Herkunft, 2 Seiten",
				"Anwesenheitsliste: 2 Seite, mehr Tage",
				"Wordpress API: Gesamtkosten für Kurs schicken"
		)
	),
	"3.13" => array(
		"date" => "26.02.2016",
		"items" => array(
			"WAFF Rechnungen = gleicher Nummernkreis wie AMS Rechnungen",
			"Berechnung von WAFF Kosten",
			"Kosten angepasst in Zahlungsbestätigung",
			"WAFF Rechnung: Adresse des Teilnehmers andrucken",
			"Teilnehmer: neues Feld WAFF lfd. Nr. -> Druck auf WAFF Rechnung",
			"neue Menü Punkte - Kurs: Erlagschein Teilnehmer, WAFF Teilnehmer Rechnung",
			"Druck erstellen: Wenn Herkunft LR oder WAFF dann Kosten aliquot berechnen",
			"Neu Sortierung Kurs Menü",
			"neue Templates Turmdrehkran, Maurer",
		)
	),
	"3.09" => array(
			"date" => "11.02.2016",
			"items" => array(
					"Erlagschein: Datum = -7 Tage des Kursbeginns",
					"Kursbestätigung: Tagesdatum ist nun das Kursende Datum",
					"Teilnehmer - Kurse anzeigen: Druck von Kostenvoranschlag und Kurszeitbestätigung",
					"Rechnungen: Kosten Wirtschaftskammer und Gesamtkosten, Datum = Kursende Datum",
					"Kostenvoranschlag: Kosten Wirtschaftskammer und Gesamtkosten",
					"neuer Bericht: Teilnahmebestätigung",
					"2 neue Vorlage: WAFF Direkt (eigener Nummernkreis: WAFF...) und WAFF Teilnehmer",
					"neue Felder im Setup für WAFF",
					"Druck WAFF Rechnung erstellt auch WAFF Teilnehmer",
					"Herkunft: neues Feld Adressfeld für WAFF Rechnung",
			)
		),
	"2.40" => array(
		"date" => "23.11.2015",
		"items" => array(
			"Korrektur Lookup Felder beim Abspeichern" )
	),
	"2.39a" => array(
		"date" => "16.11.2015",
		"items" => array(
			"neuer Feldtyp Lookup",
			"Drucken, Druck generieren, Listen angepasst für neue Bericht" )
	),
	"2.39" => array(
		"date" => "16.11.2015",
		"items" => array(
			"neuer Bericht: Kurse -> Teilnehmerliste 2",
			"neuer Bericht: Kurse -> Protokoll Kran/Stapler",
			"neues Feld in Kursteilnehmer: Herkunft" )
	),
	"2.38" => array(
		"date" => "26.06.15",
		"items" => array(
			"neuer Hauptmenüpunkt Statistik mit Unterfunktionen erweiterbar",
			"Statistik - Herkunft der Teilnehmer" )
	),
	"2.37" => array(
		"date" => "24.06.15",
		"items" => array(
			"Trainer - zuordnen: manuelle Stunden",
			"Druck erstellen angepasst: Honornote, Werkvertrag",
			"Mouse Over bei Einnahme / Ausgaben und Berechnung angepasst" )
	),
	"2.30" => array(
		"date" => "08.02.15",
		"items" => array(
			"neues Feld beim Teilnehmer: E-Mail",
			"Outlook Kalender Exporter" )
	),
	"2.29" => array(
		"date" => "29.01.15",
		"items" => array(
			"Anpassungen Templates" )
	),
	"2.28" => array(
		"date" => "23.01.15",
		"items" => array(
			"Anmeldeformular für Landschaftsgärtner eingebaut",
			"Titelleiste zeigt nun an, falls neues Update verfügbar ist" )
	),
	"2.26" => array(
		"date" => "24.10.14",
		"items" => array(
			"neues Feld 'Notiz' beim Teilnehmer analog Adresskopf",
			"neues Feld 'Notiz vorhanden' wenn Notiz da, dann JA sonst NEIN",
			"List Klasse und Javascript überarbeitet wegen mehrerer Textfelder",
			"Ansicht Detail, Fehler in Ausgabe bei Teilnehmer korrigiert (select_function)" )
	),
	"2.25" => array(
		"date" => "17.10.14",
		"items" => array(
			"Werkvertrag: unten Startdatum vom Kurs" )
	),
	"2.24" => array(
		"date" => "06.10.14",
		"items" => array(
			"E-Mails erstellen AN: und CC: vertauschen. an: AMS, cc: Berater" )
	),
	"2.23" => array(
		"date" => "06.10.14",
		"items" => array(
			"Komprimieren der Bilder in allen Vorlagen (wegen Größe)" )
	),
	"2.22" => array(
		"date" => "01.10.14",
		"items" => array(
			"Layout und Logos (Ö-Cert, Wien-Cert) in Kursbeginn und Kursende E-Mails",
			"Anpassung der Templates, neues Ö-Cert Logo" )
	),
	"2.21" => array(
		"date" => "15.09.14",
		"items" => array(
			"Korrektur im Kalender wegen Sommer/Winterzeit Umstellung" )
	),
	"2.20" => array(
		"date" => "15.09.14",
		"items" => array(
			"Kurse synchronisieren: Umprogrammiert wegen Web-Seiten-Sicherheit (cURL)" )
	),
	"2.19" => array(
		"date" => '06.09.14',
		"items" => array(
			"neue Liste: Basilica - Kurse anzeigen",
			"neue Funktion beim Teilnehmer: Kurse anzeigen",
			"Kurszuordnungen beim Teilnehmer farbig (bezahlt/nicht bezahlt)",
			"Change Log für letzte Änderungen" )
	)
);
?>