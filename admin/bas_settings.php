<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

$kurs_id = isset( $_GET['kurs_id'] )?$_GET['kurs_id']:(isset( $_POST['kurs_id'] )?$_POST['kurs_id']:0);

if( isset( $_GET['sync_kurse'] ) ) {
	$i = $interface->sync_kurse_to_wordpress();

	if( $i >= 0 )
		$mes->addInfo( "Es wurden ".$i." Kurse mit der Webseite synchronisiert." );
} // if

if( isset( $_GET['gen_ams_file'] ) ) {
	$interface->create_ams_file();
	$interface->open_ams_file();
	exit;
} // if

if( isset( $_GET['gen_waff_file'] ) ) {
	$interface->create_waff_file();
	$interface->open_waff_file();
	exit;
} // if

if( isset( $_GET['gen_wkw_file'] ) ) {
	$interface->create_wkw_file( $kurs_id );
	$interface->open_wkw_file();
} // if

if( isset( $_GET['gen_outlook_file'] ) ) {
	$interface->create_outlook_file();
	$interface->open_outlook_file();
} // if

if( isset( $_GET['open'] ) ) {
	$print->load_file( $_GET['id'] );
	$print->open_rtf();
	exit;
} // if

require_once( CLASS_DIR."templates/header.php" );

$setup = $f->save_setup( "BAS_SETUP" );
$setup = $f->load_setup( "BAS_SETUP" );

?>
<div id="content_scroll">
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>Einstellungen</h2>
	<table class="list_left shadow" style="width: 730px;">
		<tr><th>AMS E-Mail bcc</th><td><input size="80" type="text" value="<? echo $setup['ams_email_bcc']; ?>" name="ams_email_bcc"/></td></tr>
		<tr><th><nobr>Weiterbildungszeit [mon]<nobr/></th><td><input type="text" value="<? echo $setup['weiterbildung_monate']; ?>" name="weiterbildung_monate"/></td></tr>
		<tr><th><nobr>Weiterbildungszeit (ADR) [mon]<nobr/></th><td><input type="text" value="<? echo $setup['adr_weiterbildung_monate']; ?>" name="adr_weiterbildung_monate"/></td></tr>
		<tr><th>WAFF Rechnungsprozentsatz</th><td><input type="text" value="<? echo $setup['waff_prozentsatz']; ?>" name="waff_prozentsatz"/></td></tr>
		<tr><th>WAFF max. Limit</th><td><input type="text" value="<? echo $setup['waff_max_limit']; ?>" name="waff_max_limit"/></td></tr>
		<tr><td colspan="2" class="right">
			<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php  echo $f->get_button( 'speichern' ); ?></a>
	</table>
</form>
<br />
<?

$location = file_get_contents( dirname( __FILE__ ).'/../location.txt' );
$button = '';
//if( $location == 'basilica_at' )
	$button = '<a href="?sync_kurse=1" class="link_click_button_right">'.$f->get_button( 'synchronisieren' ).'</a>';

$db->query( "SELECT COUNT( k.nummer ) anz FROM BAS_KURSE AS k WHERE k.startdatum>='".date( "Y-m-d", time() )."'" );
$r = $db->getNext();

$strKurse = "Es werden ".$r['anz']." Kurse synchronisiert.";

?>
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>Wordpress Integration</h2>
	<table class="list_left shadow" style="width: 730px;">
		<th><td><?php echo $strKurse; ?></td></th>
		<tr><th>API Passwort</th><td><input type="text" value="<? echo $setup['wordpress_api_password']; ?>" name="wordpress_api_password"/></td></tr>
		<tr><th>Script Delete</th><td><input size="80" type="text" value="<? echo $setup['wordpress_delete']; ?>" name="wordpress_delete"/></td></tr>
		<tr><th>Script Add</th><td><input size="80" type="text" value="<? echo $setup['wordpress_add']; ?>" name="wordpress_add"/></td></tr>
		<tr><td colspan="2" class="right">
				<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
				<? echo $button; ?>
	</table>
</form>
<br />

<?
$sel_kurse = '<select id="bas_settings_kurse" name="kurs_id" data-placeholder="wählen..." class="chosen-select" style="width:200px"><option value="0"></option>';
$db->query( "
	SELECT k.kurs_id, k.nummer, k.startdatum, k.enddatum
	FROM BAS_KURSE AS k
	WHERE k.lehrabschluss='1'
	ORDER BY k.startdatum DESC, k.nummer ASC" );
while( $db->isNext() ) {
	$r = $db->getNext();

	$sel = '';
	if( $r['kurs_id'] == $kurs_id ) $sel = ' selected';

	$sel_kurse .= '<option value="'.$r['kurs_id'].'"'.$sel.'>'.$f->get_kurs_titel( $r['nummer'], $r['startdatum'], $r['enddatum'] ).'</option>';
} // while
$sel_kurse .= '</select>';

?>
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>WKW Schnittstelle</h2>
	<table class="list_left shadow" style="width: 730px;">
		<tr><th>URL</th><td><input size="80" type="text" value="<? echo $setup['wkw_url']; ?>" name="wkw_url"/></td></tr>
		<tr><th>Login</th><td><input type="text" value="<? echo $setup['wkw_login']; ?>" name="wkw_login"/></td></tr>
		<tr><th>Passwort</th><td><input type="text" value="<? echo $setup['wkw_passwort']; ?>" name="wkw_passwort"/></td></tr>
		<tr><th>für Kurs</th><td><? echo $sel_kurse; ?></td></td></tr>
		<tr><td colspan="2" class="right">
				<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
				<a href="<? echo $setup['wkw_url']; ?>" target="_blank" class="link_click_button_right"><?php echo $f->get_button( 'öffnen' ); ?></a>
				<a id="bas_settings_create_button" href="?gen_wkw_file=1&kurs_id=<? echo $kurs_id; ?>" class="link_click_button_right"><?php echo $f->get_button( 'Datei erzeugen' ); ?></a>
	</table>
</form>
<br />

<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>AMS Schnittstelle</h2>
	<table class="list_left shadow" style="width: 730px;">
		<tr><th>URL</th><td><input size="80" type="text" value="<? echo $setup['ams_url']; ?>" name="ams_url"/></td></tr>
		<tr><th>Login</th><td><input type="text" value="<? echo $setup['ams_login']; ?>" name="ams_login"/></td></tr>
		<tr><th>Passwort</th><td><input type="text" value="<? echo $setup['ams_passwort']; ?>" name="ams_passwort"/></td></tr>
		<tr><td colspan="2" class="right">
			<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php  echo $f->get_button( 'speichern' ); ?></a>
			<a href="<? echo $setup['ams_url']; ?>" target="_blank" class="link_click_button_right"><?php echo $f->get_button( 'öffnen' ); ?></a>
			<a href="?gen_ams_file=1" class="link_click_button_right"><?php echo $f->get_button( 'Datei erzeugen' ); ?></a>
	</table>
</form>
<br />

<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>Waff Schnittstelle</h2>
	<table class="list_left shadow" style="width: 730px;">
		<tr><th>URL</th><td><input size="80" type="text" value="<? echo $setup['waff_url']; ?>" name="waff_url"/></td></tr>
		<tr><th>Login</th><td><input type="text" value="<? echo $setup['waff_login']; ?>" name="waff_login"/></td></tr>
		<tr><th>Passwort</th><td><input type="text" value="<? echo $setup['waff_passwort']; ?>" name="waff_passwort"/></td></tr>
		<tr><td colspan="2" class="right">
			<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php  echo $f->get_button( 'speichern' ); ?></a>
			<a href="<? echo $setup['waff_url']; ?>" target="_blank" class="link_click_button_right"><?php echo $f->get_button( 'öffnen' ); ?></a>
			<a href="?gen_waff_file=1" class="link_click_button_right"><?php echo $f->get_button( 'Datei erzeugen' ); ?></a>
	</table>
</form>
<br />

<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>Outlook Kalender Schnittstelle</h2>
	<table class="list_left shadow" style="width: 730px;">
		<tr><td colspan="2" class="right">
			<a href="?gen_outlook_file=1" class="link_click_button_right"><?php echo $f->get_button( 'Datei erzeugen' ); ?></a>
	</table>
</form>
<br />

</div>
<?

require_once( CLASS_DIR."templates/footer.php" );
?>