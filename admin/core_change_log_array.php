<?php
$project = array( 1, 2, 3, 4 );
$update = array(
		"4.34" => array(
				"date" => "17.01.2019",
				"items" => array(
						"Plugin System",
						"Upgrade phpmailer auf v6.0.6 (Depreciaton/Sercurity)",
						"Crypt/Decrypt Files: neue Version _enc_v2 mit open_ssl",
						"Mail System: Anpassung Texte, PW Vergessen komplett neu, alles auf UTF-8 umstellen",
						"Basis Liste: neue Option select_multi, Textarea aus Listen generell ausblenden, Read-Only Felder richtig anzeigen",
						"Footer: Einblenden von wichtigen Links, DSGVO Relevant (Plugin System, Massentauglich)",
						"Korrektur Layout auf Mobile Devices inkl. Menü Drop-Down",
						"Performance: load_orders nur wenn notwendig",
						"Admin: Systeminfos in Footer anzeigen",
				)
		),
		"4.33" => array(
				"date" => "11.01.2019",
				"items" => array(
						"Einstellungen: dynamische Theme Auswahl",
						"Menü Listen: Sortierung nach Name",
						"Fehler Systemlisten: 'Notice: visible' korrigiert",
						"Sicherheitsupdates php: passwordhash, encryption (mcrypt deprecated)",
				)
		),
		"4.32" => array(
				"date" => "08.01.2019",
				"items" => array(
						"Datumsauswahl: Montag als 1. Spalte",
						"Zeitauswahl: Anzeige korrigiert",
						"Sticky Table Header: border-box Browser Bug Workaround",
				)
		),
		"4.31" => array(
				"date" => "07.01.2019",
				"items" => array(
						"Performance: css Optimierungen",
						"Performance: Content Scroll: optimale Größenanpassung",
						"Performance: schmales Menü deaktiviert",
				)
		),
		"4.30" => array(
				"date" => "05.01.2019",
				"items" => array(
						"Suche/Filter: gefundenen Text highlighten",
						"Content Scroll: optimale Größenanpassung",
						"Table Header: Fixierung des Headers bei Scroll Down",
				)
		),
		"4.29" => array(
				"date" => "04.01.2019",
				"items" => array(
						"Fehler in css korrigiert, Farb Themes korrigiert",
						"neue Version tinymce",
						"Standard Sprache in Core integriert",
						"Basis Einstellungen angepasst (alte Sachen deaktiviert)",
						"Layout Änderung je nach Projekt",
						"Listen: Anzeige Korrektur / Sortierung",
				)
		),
		"4.28" => array(
				"date" => "31.12.2018",
				"items" => array(
						"Listen (readmore_v3): Lange Texte werden verkürzt dargestellt, mit Klick auf + sieht man alles",
						"Paginator: neues Umblättern in Listen",
						"Tablesort: neue Version",
						"Sicherheitsupdates jquery",
						"allgemeine Layout Verbesserungen/Korrekturen",
				)
		),
		"4.27" => array(
				"date" => "15.12.2019",
				"items" => array(
						"aktuelle Version: am Login Screen und im Admin Hauptmenü anzeigen",
						"Einstellungen: Farbschema auswählbar, externes Farbschema möglich (NUR notfalls)",
				)
		),
		"4.24a" => array(
				"date" => "07.12.2018",
				"items" => array(
						"neues Layout",
						"Sicherheitsupdates",
				)
		),
		"4.08" => array(
				"date" => "06.06.2018",
				"items" => array(
						"Sicherheitsupdates",
				)
		),
		"4.02" => array(
				"date" => "01.03.2018",
				"items" => array(
						"PopUp Fenster: minimale Höhe auf 400px gesetzt",
						"Grafische Korrekturen bei Buttons",
						"Neue Chosen Version 1.8.3",
						"PHPMailer Update, neue SMTP Optionen, Config",
				)
		),
		"3.96" => array(
				"date" => "28.12.2017",
				"items" => array(
						"php: max_execution_time auf 600 Sekunden setzen",
				)
		),
		"3.93" => array(
				"date" => "06.12.2017",
				"items" => array(
						"Neue projektspezifische Benutzerlisten in Basiseinrichtung",
				)
		),
		"3.90" => array(
				"date" => "25.11.2017",
				"items" => array(
						"PHP memory_limit beim Import auf 2 GB setzen",
						"interne Listen: Anpassung Header Rechtsbündig, bei int, dec, date",
						"Anpassung MSD Backup Configdateien"
				)
		),
		"3.67" => array(
				"date" => "17.03.2017",
				"items" => array(
						"Hotfix mit Pflichteingabe Felder",
						"Modales Fenster, max. Höhe auf 800px",
						"Modales Fenster in Höhe kleiner (weisser Bereich weg)"
				)
		),
	"3.45" => array(
			"date" => "05.10.2016",
			"items" => array(
					"JQuery: Korrektur Chosen Select Felder",
			)
	),
	"3.16" => array(
		"date" => "09.03.2016",
			"items" => array(
				"Setup: Impressum Link",
				"Copyright Text in Sourcecode",
				"Bugfix: Register + PW Lost nicht aufrufbar, wenn deaktiviert"
		)
	),
	"3.12" => array(
		"date" => "29.02.2016",
		"items" => array(
			"Umbau auf bootstrap",
			"Drucken von Edit (Eingabe/Neu) Karten",
			"Selectbox runterspringen korrigiert -> jquery 2.2.1",
		)
	),
	"3.01" => array(
		"date" => "18.01.2016",
		"items" => array(
			"Mehrsprachigkeit",
			"Listen Re-Design" )
	),
	"2.39a" => array(
		"date" => "16.11.2015",
		"items" => array(
			"neuer Feldtyp Lookup",
			"Drucken, Druck generieren, Listen angepasst für neue Bericht" )
	),
	"2.36" => array(
		"date" => "09.06.15",
		"items" => array(
			"Projekt Verwaltung - Projekte für alle zugänglich",
			"Listen neues Feld Textfarbe" )
	),
	"2.35" => array(
		"date" => "08.06.15",
		"items" => array(
			"neue Version von phpmailer" )
	),
	"2.34" => array(
		"date" => "23.04.15",
		"items" => array(
			"Druckdatei nach Druck wieder entfernen",
			"Erweiterung Benutzer: Liste Register ein/aus, Pfad Winword" )
	),
	"2.33" => array(
		"date" => "12.04.15",
		"items" => array(
			"verbesserte Versionskontrolle" )
	),
	"2.32" => array(
		"date" => "12.04.15",
		"items" => array(
			"Stabilitätskorrekturen",
			"Backup Routine verbessert",
			"Basis für Komplettbackup und Cloud Backup" )
	),
	"2.31" => array(
		"date" => "09.04.15",
		"items" => array(
			"Backup Benachrichtungen im Header" )
	),
	"2.27" => array(
		"date" => "28.11.14",
		"items" => array(
			"Titelleiste zeigt nun an, falls neues Update verfügbar ist",
			"Überarbeitung Rechte System",
			"Updates sind nun pro Projekt aufgeteilt",
			"neuer Feldtyp Images für Bildupload" )
	),
);
?>