<?php
require_once( dirname( __FILE__ )."/../classes/config_data.php" );

if( !isset( $_SESSION ) ) session_start();
session_unset();
session_destroy();
session_write_close();
setcookie(session_name(),'',0,'/');
session_regenerate_id(true);
?>
<script language="javascript">location.href="<? echo DOMAIN.'index.php'; ?>";</script>
