<?php
if( !isset($_SESSION) ) session_start();

require_once( dirname( __FILE__ )."/../classes/config_data.php" );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'messages.php' );

$f = functions::getInstance();
$mes = messages::getInstance();
$t = translate::getInstance();

$setup = $f->load_setup( "CORE_SETUP" );
if( $setup['register_allowed'] == 0 ) {
	header( "location: /admin/login_login.php" );
	exit;
} // if

if( isset( $_GET['register'] ) ) {
	if( $f->register_user() ) {
		$mes->saveMessages();

		header( "location: /".SUBDIR."admin/login_login.php" );
		exit;
	} // if

} // if

require_once( CLASS_DIR."templates/header.php" );

?>
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?register=1">
	<table class="list_left shadow">
		<tr><th>E-Mail</th><td><input type="text" name="email"/></td></tr>
		<tr><th>Name</th><td><input type="text" name="name"/></td></tr>
		<tr><td colspan="2" >
			<a href="#" style="float: right;" onClick="$(this).closest('form').submit()"><? $f->print_button( 'registrieren' ); ?></a>
<!-- 			
			<ul id="mbmcpebul_table" class="mbmcpebul_menulist css_menu" style="height: 25px;">
				<li class="topitem spaced_li"><a href="#" onClick="$(this).closest('form').submit()"><div class="buttonimg buttonimg_25" style="width: 150px; background-image: url('/<? echo SUBDIR; ?>css/pics/mb_registrieren.png')">Registrieren</div></a></li>
			</ul>
 -->			
		</td></tr>
	</table>
</form>
<?
require_once( CLASS_DIR."templates/footer.php" );
?>