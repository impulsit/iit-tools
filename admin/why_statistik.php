<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

$strOut = '';

$f->set_back_up_file = 'admin/list_redirect.php?indiv_list_id=13';

require_once( CLASS_DIR."templates/header.php" );

echo '<div id="content_scroll"><h2>Statistik des Fragebogens / Balken</h2>';

$fragebogen_id = isset( $_GET['fragebogen_id'] )?$_GET['fragebogen_id']:(isset( $_POST['fragebogen_id'] )?$_POST['fragebogen_id']:0);

$userinfo = $f->load_user( $c_user_id, "admin" );

$filter = "";
if( $userinfo['admin'] == 0 )
	$filter = "d.user_id='".$c_user_id."'";

if( $filter == "" ) $filter = '1';

if( $db->query( "
	SELECT f.fragebogen_id, f.titel, f.geschlecht_pflicht
	FROM WHY_FRAGEBOGEN AS f
	LEFT JOIN WHY_DOMAINS AS d ON (d.fragebogen_id=f.fragebogen_id)
	WHERE f.fragebogen_id='".$fragebogen_id."' AND ".$filter ) ) {
	$r = $db->getNext();

	if( !isset( $_POST['datum_von'] ) ) $_POST['datum_von'] = "01.01.1970";
	if( !isset( $_POST['datum_bis'] ) ) $_POST['datum_bis'] = date( "d.m.Y", time() );
	$kurs_id = isset( $_GET['kurs_id'] )?$_GET['kurs_id']:(isset( $_POST['kurs_id'] )?$_POST['kurs_id']:0);
	$trainer_id = isset( $_GET['trainer_id'] )?$_GET['trainer_id']:(isset( $_POST['trainer_id'] )?$_POST['trainer_id']:0);

	// Kurs wählen
	$list1 = '';
	$db->query( "
		SELECT k.kurs_id, k.nummer, k.startdatum, k.enddatum
		FROM BAS_KURSE AS k
		ORDER BY k.startdatum DESC, k.nummer ASC" );
	while( $db->isNext() ) {
		$r2 = $db->getNext();

		$sel = '';
		if( $r2['kurs_id'] == $kurs_id ) $sel = ' selected';

		$list1 .= '<option value="'.$r2['kurs_id'].'"'.$sel.'>'.$f->get_kurs_titel( $r2['nummer'], $r2['startdatum'], $r2['enddatum'] ).'</option>';
	} // while

	// Trainer
	$list2 = '';
	$db->query( "SELECT trainer_id, vorname, nachname FROM BAS_TRAINER ORDER BY nachname, vorname" );
	while( $db->isNext() ) {
		$r2 = $db->getNext();

		$sel = '';
		if( $r2['trainer_id'] == $trainer_id ) $sel = ' selected';

		$list2 .= '<option value="'.$r2['trainer_id'].'"'.$sel.'>'.$r2['nachname'].' '.$r2['vorname'].'</option>';
	} // while

	$strOut = '
		<form method="post" action="why_statistik.php">
			<input type="hidden" name="fragebogen_id" value="'.$fragebogen_id.'">

			<table class="list">
				<tr><th>Fragebogen</th><td>'.$fragebogen_id.' - '.$r['titel'].'</td></tr>
				<tr><th>ab Datum</th><td><input class="date_picker" type="text" name="datum_von" value="'.date( "d.m.Y", strtotime( $_POST['datum_von'] ) ).'"></td></tr>
				<tr><th>bis Datum</th><td><input class="date_picker" type="text" name="datum_bis" value="'.date( "d.m.Y", strtotime( $_POST['datum_bis'] ) ).'"></td></tr>
				<tr><th>Kurs</th>
					<td>
						<select name="kurs_id" data-placeholder="Kurs wählen..." class="chosen-select" style="width: 300px">
							<option value=""></option>
							'.$list1.'
						</select>
					</td>
				</tr>
				<tr><th>Trainer</th>
					<td>
						<select name="trainer_id" data-placeholder="Trainer wählen..." class="chosen-select" style="width: 300px">
							<option value=""></option>
							'.$list2.'
						</select>
					</td>
				</tr>
				<tr><th>generieren</th><td><input type="checkbox" name="gen" value="1"></td></tr>
			</table>
			<br />

			<a href="#" onClick="$(this).closest(\'form\').submit()" class="link_click_button">'.$f->get_button( 'anzeigen' ).'</a>
			<a href="#" onClick="print_window(\'content_scroll\')" class="link_click_button">'.$f->get_button( 'Drucken' ).'</a>
		</form>';

	if( isset( $_POST['gen'] ) ) {
		$_TRAINER_FILTER = '';
		if( $trainer_id > 0 ) {
			$_TRAINER_FILTER = "AND ua.trainer_id='".$trainer_id."'";
		} // if
		$_KURS_FILTER = '';
		if( $kurs_id > 0 ) {
			$_KURS_FILTER = "AND uf.kurs_id='".$kurs_id."'";
		} // if

		$db->query( "
			SELECT f.frage_id, f.kurz, f.frage, f.antworttyp_id, f.trainer_frage
			FROM WHY_FRAGEN AS f
			WHERE
				f.fragebogen_id='".$r['fragebogen_id']."'".
				(($_TRAINER_FILTER!='')?' AND trainer_frage=1 ':'').
			"ORDER BY position", 1 );
		while( $db->isNext( 1 ) ) {
			$frage = $db->getNext( 1 );

			// Trainer laden
			$trainer = array();
			if( $frage['trainer_frage'] == 1 ) {
				$db->query( "
					SELECT DISTINCT( ua.trainer_id ), t.vorname, t.nachname, uf.kurs_id, k.nummer, k.startdatum, k.enddatum
					FROM WHY_USER_ANTWORTEN AS ua
					LEFT JOIN BAS_TRAINER AS t ON (t.trainer_id=ua.trainer_id)
					LEFT JOIN WHY_USER_FRAGEBOGEN AS uf ON (uf.id=ua.id)
					LEFT JOIN BAS_KURSE AS k ON (k.kurs_id=uf.kurs_id)
					WHERE
						uf.fragebogen_id='".$r['fragebogen_id']."' AND
						uf.zeit>='".date( "Y-m-d", strtotime( $_POST['datum_von'] ) )."' AND
						uf.zeit<='".date( "Y-m-d", strtotime( $_POST['datum_bis'] ) )."' ".
						$_TRAINER_FILTER.
						$_KURS_FILTER, "create_fb_2" );
				while( $db->isNext( "create_fb_2" ) ) {
					$r2 = $db->getNext( "create_fb_2" );

					$trainer[] = array( "name" => $r2['vorname'].' '.$r2['nachname'], "id" => $r2['trainer_id'], "kurs_id" => $r2['kurs_id'], "kurs_text" => $f->get_kurs_titel( $r2['nummer'], $r2['startdatum'], $r2['enddatum'] ) );
				} // while
			} else
				$trainer[] = array( "name" => "temp", "id" => 0 );

			foreach( $trainer as $k => $v ) {
				if( $frage['trainer_frage'] == 1 ) {
					if( $v['id'] == 0 ) $v['name'] = "(ALLE)";
					if( $v['kurs_id'] == 0 ) $kurstext = "ALLE"; else $kurstext = $v['kurs_text'];
					$name = $frage['kurz'].' '.$v['name'].', '.$kurstext.'';
				} else
					$name = $frage['kurz'];

				$strOut .= '<br /><strong>'.$name.'</strong><br />'.$frage['frage'].'<br />';

				$_TRAINER_WHERE = "";
				if( $v['id'] > 0 ) {
					$_TRAINER_WHERE = "AND trainer_id='".$v['id']."'";
					if( $v['kurs_id'] > 0 ) $_TRAINER_WHERE .= " AND kurs_id='".$v['kurs_id']."'";
				} // if

				$db->query( "
					SELECT COUNT( uf.id ) AS anz
					FROM WHY_USER_FRAGEBOGEN AS uf
					LEFT JOIN WHY_USER_ANTWORTEN AS ua ON (ua.id=uf.id)
					WHERE
						uf.fragebogen_id='".$r['fragebogen_id']."' AND
						ua.frage_id='".$frage['frage_id']."' AND uf.zeit>='".date( "Y-m-d", strtotime( $_POST['datum_von'] ) )."' AND
						uf.zeit<='".date( "Y-m-d", strtotime( $_POST['datum_bis'] ) )."' ".
						$_TRAINER_WHERE, 2 );
				$frage_anz = $db->getNext( 2 );

				$strOut .= '
					<table class="list">
						<tr><th>Antwort</th><th colspan="2">Gesamt</th>';

				$max = 0;
				if( $r['geschlecht_pflicht'] == 1 ) {
					$strOut .= '<th colspan="2">weiblich</th><th colspan="2">männlich</th></tr>';
					$max = 2;
				} // if

				$db->query( "SELECT antwort_id, wert, antwort_titel FROM WHY_ANTWORTEN WHERE frage_id='".$frage['frage_id']."' ORDER BY position" );
				while( $db->isNext() ) {
					$antwort = $db->getNext();

					$antwort_anz = array( 0, 0, 0 );
					$db->query( "
						SELECT uf.geschlecht, COUNT( uf.id ) AS anz
						FROM WHY_USER_FRAGEBOGEN AS uf
						LEFT JOIN WHY_USER_ANTWORTEN AS ua ON (ua.id=uf.id)
						WHERE
							uf.fragebogen_id='".$r['fragebogen_id']."' AND
							ua.frage_id='".$frage['frage_id']."' AND ua.antwort_id='".$antwort['antwort_id']."' AND
							uf.zeit>='".date( "Y-m-d", strtotime( $_POST['datum_von'] ) )."' AND
							uf.zeit<='".date( "Y-m-d", strtotime( $_POST['datum_bis'] ) )."' ".
							$_TRAINER_WHERE."
						GROUP BY uf.geschlecht", 2 );
					while( $db->isNext( 2 ) ) {
						$aa = $db->getNext( 2 );

						$antwort_anz[$aa['geschlecht']] = $aa['anz'];
					} // while

					// Gesamt aufaddieren
					for( $i=1; $i<=$max; $i++ )
						$antwort_anz['0'] += $antwort_anz[$i];

					$strOut .= '<tr><td>'.$antwort['antwort_titel'].'</td>';
					for( $i=0; $i<=$max; $i++ ) {
						$pr = 0;
						if( $antwort_anz[$i] > 0 )
							$pr = round( $antwort_anz[$i] / $frage_anz['anz'] * 100, 2 );

						$bar = '
							<div style="display:inline-block; position:relative; width:100px; font-size:0.8em;">
								<div style="width:'.$pr.'px; background:#cfcfcf;">&nbsp;</div>
								<p style="position:absolute; top:0; left:0; width:100px; text-align:center">'.$pr.'%</p>
							</div>';

						$strOut .= '<td class="right">'.$antwort_anz[$i].'</td><td>'.$bar.'</td>';
					} // for
					$strOut .= '</tr>';
				} // while

				$strOut .= '<tr><td colspan="2"></tr>';
				$strOut .= '<tr><td></td><td>'.$frage_anz['anz'].'</td></tr>';
				$strOut .= '</table>';
			} // foreach
		} // while
	} // if
} // if

echo $strOut.'</div>';

require_once( CLASS_DIR."templates/footer.php" );
?>