<?php
if( !isset($_SESSION) ) session_start();

require_once( dirname( __FILE__ )."/../classes/config_data.php" );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'theme_functions.php' );
require_once( CLASS_DIR.'messages.php' );

$f = functions::getInstance();
$t = translate::getInstance();
$theme = theme_functions::getInstance();
$mes = messages::getInstance();

if( isset( $_GET['reset_pw'] ) ) {
	$f->password_lost( $_POST['email'] );
} // if

// Default Header
require_once( BASE_DIR."themes/_default/classes/pw_lost.php" );
?>