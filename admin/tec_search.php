<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );
require_once( CLASS_DIR."tecdoc.php" );

$setup = $f->load_setup( "TEC_SETUP" );

$db->query( "SELECT topmotive_kundennr, user_level, vin_search_inactive, search_show_attributes, search_only_on_stock FROM CORE_USER_INFO WHERE user_id='".$_SESSION['c_user_id']."'" );
$userinfo = $db->getNext();
if( $setup['enable_topmotive_api'] == 1 ) {
	if( $userinfo['topmotive_kundennr'] == '' ) {
		echo '<div class="alert alert-danger my-alert" role="alert">'.$t->t( 'Sie müssen eine Kundennummer in den Einstellungen hinterlegen.' ).'</div>';
	} // if
} // if

$tec = tecdoc::getInstance();
$theme = theme_functions::getInstance();

$search_pattern = "";
if( isset( $_GET['indiv_search_pattern'] ) ) {
	$search_pattern = $_GET['indiv_search_pattern'];
	echo '<input type="hidden" value="1" id="doSearch" name="doSearch">';
} // if

$manufacturer_selected = 0;
$all_manufacturers = false;

if( isset( $_GET['indiv_search_model'] ) ) {
	$manufacturer_selected = $_GET['manufacturer'];
	$all_manufacturers = 1;
	echo '
		<input type="hidden" value="2" id="doSearch" name="doSearch">
		<input type="hidden" value="'.$_GET['model'].'" id="modelId" name="modelId">
		<input type="hidden" value="'.$_GET['vehicle'].'" id="vehicleId" name="vehicleId">';
} // if

if( ($setup['enable_topmotive_api'] == 1) && ($userinfo['user_level'] >= 100) ) {
	$option_customer_no = '<option value=""></option>';
	$db->query( "SELECT user_id, email, name, topmotive_kundennr FROM CORE_USER_INFO ORDER BY name" );
	while( $db->isNext() ) {
		$r = $db->getNext();

		$sel = "";
		if( $r['user_id'] == $_SESSION['c_user_id'] )
			$sel = " selected";

			if( $r['topmotive_kundennr'] != "" )
				$option_customer_no .= '<option value="'.$r['topmotive_kundennr'].'"'.$sel.'>'.str_replace( "-", "- ", $r['name'] ).', '.$r['email'].', '.$r['topmotive_kundennr'].'</option>';
	} // while
	?>
<div class="row" style="margin-bottom: 10px">
		<div class="col-md-12">
			<select id="search_by_customerno" name="search_by_customerno" data-placeholder="<? echo $t->t( 'Suche mit Kundennummer' ); ?>" class="chosen-select"><? echo $option_customer_no; ?></select>
		</div>
</div>
	<?php
} // if
?>
<div class="row">
	<div id="ajax_loader" class="ajax_loader"><div><img src="<?php echo $theme->get_icon_path( 'ajax_loader', '.gif' ); ?>"></div></div>
	<div id="search-section" class="col-xl-8 col-lg-8">
<?php

$option_from = "";
$option_to = "";
for( $i=1970; $i<=date("Y"); $i++ ) {
  $str = $i;
  if( $i == 1970 )
    $str = '<<'.$str;

  $option_from = '<option value="'.$i.'">'.$str.'</option>'.$option_from;

  if( $i == date("Y") ) $sel = "selected"; else $sel = "";
  $option_to .= '<option value="'.$i.'" '.$sel.'>'.$i.'</option>';
} // for
$option_from = '<option value="0"> </option>'.$option_from;

$options = '<option value=""></option>';
$list = '<ul id="list_autocomplete">';
if( $db->query( "SELECT DISTINCT( search_pattern ) FROM TEC_LOG_SEARCH WHERE user_id='".$_SESSION['c_user_id']."' AND action='0' AND search_pattern!='' ORDER BY id DESC LIMIT 30" ) ) {
	while( $db->isNext() ) {
		$r = $db->getNext();

		$r['search_pattern'] = str_replace( "<", "", $r['search_pattern'] );
		$options .= '<option value="'.$r['search_pattern'].'">'.$r['search_pattern'].'</option>';
		$list .= '<li>'.$r['search_pattern'].'</li>';
	} // while
} // if
$list .= '<ul>';

$list .= '<ul id="list_autocomplete_audatex">';
if( $db->query( "SELECT DISTINCT( search_pattern ) FROM TEC_LOG_SEARCH WHERE user_id='".$_SESSION['c_user_id']."' AND action='2' AND search_pattern!='' ORDER BY id DESC LIMIT 30" ) ) {
	while( $db->isNext() ) {
		$r = $db->getNext();

		$r['search_pattern'] = str_replace( "<", "", $r['search_pattern'] );
		$options .= '<option value="'.$r['search_pattern'].'">'.$r['search_pattern'].'</option>';
		$list .= '<li>'.$r['search_pattern'].'</li>';
	} // while
} // if
$list .= '<ul>';

$option_fueltype = '';
$option_motorcode = '';
$option_pskw = '';

$option_history = '';
$arr = $tec->reload_search_history();
foreach( $arr as $k => $v ) {
	$option_history .= '<option value="'.$v['id'].'">'.$v['text'].'</option>';
} // foreach

// Universalartikel Filter
$option_filter_width = $tec->get_universal_article_option( "width" );
$option_filter_height = $tec->get_universal_article_option( "height" );
$option_filter_length = $tec->get_universal_article_option( "length" );
$option_filter_diameter = $tec->get_universal_article_option( "diameter" );
$option_filter_viscosity = $tec->get_universal_article_option( "viscosity" );
$option_filter_kw = $tec->get_universal_article_option( "kw" );
$option_filter_ah = $tec->get_universal_article_option( "ah" );
$option_filter_supplier = $tec->get_universal_article_option( "supplier" );
$option_filter_belt_shafts = $tec->get_universal_article_option( "belt_shafts" );
$option_filter_volt = $tec->get_universal_article_option( "volt" );
$option_filter_amper = $tec->get_universal_article_option( "amper" );
$option_filter_season = $tec->get_universal_article_option( "season" );

echo '<div id="my_top"></div><div style="display: none;">'.$list.'</div>';

$sub_logo = $tec->get_sub_logo();
?>
<input id="current_language_code" type="hidden" value="<? echo $_SESSION['language']; ?>" name="current_language_code">
<input id="active_search" type="hidden" value="0" name="active_search">
<input id="active_tree_category" type="hidden" value="" name="active_tree_category">
<input id="active_tree_category_description" type="hidden" value="" name="active_tree_category_description">
<div id="search_tabs" style="background: url(/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/img/search-background.jpg), #fff">
	<ul class="tabs-petina nav nav-justified">
		<li class="nav-item">
			<a class="nav-link" href="#tabs-1"><? echo $t->t( "Suche nach Text" ); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="#tabs-2"><? echo $t->t( "Suche nach Fahrzeug" ); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="#tabs-3"><? echo $t->t( "Suche nach Motorcode" ); ?></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="#tabs-4"><? echo $t->t( "Suche nach Bemerkungen" ); ?></a>
		</li>
<?php
if( ($setup['enable_fin_search'] == 1) && ($userinfo['vin_search_inactive'] == 0) ) {
?>
		<li class="nav-item">
			<a class="nav-link" href="#tabs-5"><? echo $t->t( "Suche nach FIN" ); ?></a>
		</li>
<?php
} // if
?>

<?php
if( ($setup['enable_fin_search_audatex'] == 1) && ($userinfo['vin_search_inactive'] == 0)) {
?>
		<li class="nav-item">
			<a class="nav-link" href="#tabs-6"><? echo $t->t( "Suche nach FIN" ); ?></a>
		</li>
<?php
} // if
?>
  </ul>
		<div id="tabs-1" class="my-search-container">
			<div class="row">
				<div class="col-md-12 sr">
					<div class="accent-cell">
						<div class="input-group input-group-sm">
							<input type="text" class="form-control" id="search_pattern" name="search_pattern" value="<? echo $search_pattern; ?>" placeholder="<? echo $t->t( 'Suche nach' ); ?>">
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 sr">
					<select class="form-control form-control-sm" id="search_pattern_type" name="search_pattern_type">
						<option value="10"><? echo $t->t( 'Alle Nummern' ); ?></option>
						<option value="1"><? echo $t->t( 'OE' ); ?></option>
						<option value="0"><? echo $t->t( 'Artikel Nr.' ); ?></option>
						<option value="3"><? echo $t->t( 'Vergleichsnummer' ); ?></option>
					</select>
				</div>
				<div class="col-md-6 sr">
					<div class="custom-control custom-checkbox my-1 mr-sm-2">
						<input type="checkbox" class="custom-control-input" value="1" name="search_pattern_similar" id="search_pattern_similar">
						<label class="custom-control-label" for="search_pattern_similar"><? echo $t->t( 'ähnliche Suche' ); ?></label>
					</div>
					<div id="new-checkboxes" class="row">
						<div class="col-md-6">
							<div class="custom-control custom-checkbox my-1 mr-sm-2">
								<input type="checkbox" class="custom-control-input" value="1" name="search_show_attributes" id="search_show_attributes" <?php echo (($userinfo['search_show_attributes']==1)?'checked':''); ?>>
								<label class="custom-control-label" for="search_show_attributes"><? echo $t->t( 'Attribute anzeigen' ); ?></label>
							</div>
						</div>
						<div class="col-md-6">
							<div class="custom-control custom-checkbox my-1">
								<input type="checkbox" class="custom-control-input" value="1" name="search_only_on_stock" id="search_only_on_stock" <?php echo (($userinfo['search_only_on_stock']==1)?'checked':''); ?>>
								<label class="custom-control-label" for="search_only_on_stock"><? echo $t->t( 'nur verfügbare Artikel' ); ?></label>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 sr">
					<button type="submit" class="float-right btn btn-primary moj-btn-sm" href="#" onClick="fill_articles();"><i class="fas fa-search"></i>&nbsp;<? echo $t->t( 'suchen' ); ?></button>
				</div>
			</div>
			<div class="row">
				<div class="banner-center col-md-12 sr">
					<?php
					echo $tec->get_ad_picture( "banner-center-1" );
					?>
				</div>
			</div>
  	</div> <!-- id="tabs-1" -->

	 	<?php
	  /*
	  if( $setup['fiber_no'] != '' ) {
	    echo '
				<a href="viber://'.$setup['fiber_no'].'" style="float: left; padding-left: 50px;">
				<img src="'.$theme->get_icon_path( 'viber' ).'" style="float: left; width: 23px; padding-top: 1px; border-radius: 6px;">
				<span style="padding-left: 10px; vertical-align: middle; line-height: 25px; font-size: 12px;">'.$setup['fiber_no'].'</span>
				</a>';
	  } // if
	  */
		?>

		<div id="tabs-2" class="my-search-container">
			<div class="row">
				<div class="col-md-12 sr">
					<select class="form-control form-control-sm chosen-select" id="search_history" name="search_history" data-placeholder="<? echo $t->t( 'letzte Suchen' ); ?>" onChange="restore_search_history();">
						<option value=""></option>
						<?php echo $option_history; ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 sr">
					<select class="form-control form-control-sm chosen-select" id="search_manufacturer" name="search_manufacturer" data-placeholder="<?php echo $t->t( "alle Fahrzeuge" ); ?>" onChange="fill_model();">
						<? echo $tec->get_manufacturer_select( $manufacturer_selected, $all_manufacturers ); ?>
					</select>
				</div>
				<div class="col-md-2 sr">
					<select class="form-control form-control-sm chosen-select" id="year_from" name="year_from" data-placeholder="<? echo $t->t( 'Baujahr' ); ?>" onChange="fill_model(); ">
						<? echo $option_from; ?>
					</select>
				</div>
				<div class="col-md-7 sr">
					<select class="form-control form-control-sm chosen-select" id="search_model" name="search_model" data-placeholder="<? echo $t->t( 'Fahrzeugmodell' ); ?>" onChange="fill_vehicle_reset_filter(); fill_vehicle();">
						<option value=""></option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 sr">
					<select class="form-control form-control-sm chosen-select" id="filter_fueltype" name="filter_fueltype" data-placeholder="<? echo $t->t( 'Treibstoff' ); ?>" onChange="fill_vehicle();">
						<option value=""></option>
						<?php echo $option_fueltype; ?>
					</select>
				</div>
				<div class="col-md-4 sr">
					<select class="form-control form-control-sm chosen-select" id="filter_pskw" name="filter_pskw" data-placeholder="<? echo $t->t( 'PS/KW' ); ?>" onChange="fill_vehicle();">
						<option value=""></option>
						<?php echo $option_pskw; ?>
					</select>
				</div>
				<div class="col-md-4 sr">
					<select class="form-control form-control-sm chosen-select" id="filter_motorcode" name="filter_motorcode" data-placeholder="<? echo $t->t( 'Motorcode' ); ?>" onChange="fill_vehicle();">
						<option value=""></option>
						<?php echo $option_motorcode; ?>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 sr">
					<select class="form-control form-control-sm chosen-select" id="search_vehicle" name="search_vehicle" data-placeholder="<? echo $t->t( 'Fahrzeug' ); ?>" onChange="save_search_history(); fill_shortcuts(); fill_categories(0);">
						<option value=""></option>
					</select>
				</div>
			</div>
  	</div> <!-- id="tabs-2" -->


		<div id="tabs-3" class="my-search-container">
			<div class="row">
				<div class="col-md-12 sr">
				<div class="accent-cell">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="search_motor_code" name="search_motor_code" value="" placeholder="<? echo $t->t( 'Suche nach' ); ?>">
					</div>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 sr">
					<select class="form-control form-control-sm chosen-select" id="search_motor_vehicle" name="search_motor_vehicle" data-placeholder="<? echo $t->t( 'Fahrzeug' ); ?>" onChange="fill_shortcuts(); fill_categories(0);">
						<option value=""></option>
					</select>
				</div>
				<div class="col-md-2 sr">
					<button type="submit" class="float-right btn btn-primary moj-btn-sm" href="#" onClick="fill_motor_vehicles();"><i class="fas fa-search"></i>&nbsp;<? echo $t->t( 'suchen' ); ?></button>
				</div>
			</div>
			<div class="row">
				<div class="banner-center col-md-12 sr">
					<?php
					echo $tec->get_ad_picture( "banner-center-1" );
					?>
				</div>
			</div>
	  </div> <!-- id="tabs-3" -->

		<div id="tabs-4" class="my-search-container">
			<div class="row">
				<div class="col-md-12 sr">
				<div class="accent-cell">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control"  id="search_infos" name="search_infos" value="<? echo ""; ?>" placeholder="<? echo $t->t( 'Suche über alle Kategorien. Detailergebnisse werden in den einzelnen Kategorien angezeigt.' ); ?>">
					</div>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="sr"><select id="filter_width" name="filter_width" data-placeholder="<? echo $t->t( 'Breite' ); ?>" class="chosen-select"><? echo $option_filter_width; ?></select></div>
					<div class="sr"><select id="filter_height" name="filter_height" data-placeholder="<? echo $t->t( 'Höhe' ); ?>" class="chosen-select"><? echo $option_filter_height; ?></select></select></div>
					<div class="sr"><select id="filter_diameter" name="filter_diameter" data-placeholder="<? echo $t->t( 'Durchmesser' ); ?>" class="chosen-select"><? echo $option_filter_diameter; ?></select></div>
				</div>
				<div class="col-md-3">
					<div class="sr"><select id="filter_season" name="filter_season" data-placeholder="<? echo $t->t( 'Saison' ); ?>" class="chosen-select"><? echo $option_filter_season; ?></select></div>
					<div class="sr"><select id="filter_volt" name="filter_volt" data-placeholder="<? echo $t->t( 'Volt' ); ?>" class="chosen-select"><? echo $option_filter_volt; ?></select></div>
					<div class="sr"><select id="filter_amper" name="filter_amper" data-placeholder="<? echo $t->t( 'Ampere' ); ?>" class="chosen-select"><? echo $option_filter_amper; ?></select></div>
				</div>
				<div class="col-md-3">
					<div class="sr"><select id="filter_ah" name="filter_ah" data-placeholder="<? echo $t->t( 'Ah' ); ?>" class="chosen-select"><? echo $option_filter_ah; ?></select></div>
					<div class="sr"><select id="filter_kw" name="filter_kw" data-placeholder="<? echo $t->t( 'KW' ); ?>" class="chosen-select"><? echo $option_filter_kw; ?></select></div>
					<div class="sr"><select id="filter_supplier" name="filter_supplier" data-placeholder="<? echo $t->t( 'Hersteller' ); ?>" class="chosen-select"><? echo $option_filter_supplier; ?></select></div>
				</div>
				<div class="col-md-3">
					<div class="sr"><select id="filter_viscosity" name="filter_viscosity" data-placeholder="<? echo $t->t( 'Viskosität' ); ?>" class="chosen-select"><? echo $option_filter_viscosity; ?></select></select></div>
					<div class="sr"><select id="filter_belt_shafts" name="filter_belt_shafts" data-placeholder="<? echo $t->t( 'Anzahl Rippenriemen' ); ?>" class="chosen-select"><? echo $option_filter_belt_shafts; ?></select></div>
					<div class="sr"><select id="filter_length" name="filter_length" data-placeholder="<? echo $t->t( 'Länge' ); ?>" class="chosen-select"><? echo $option_filter_length; ?></select></div>
				</div>
			</div>
			<div class="row sr">
				<div class="col-md-12">
					<button type="submit" class="float-right btn btn-primary moj-btn-sm" href="#" onClick="fill_infos(-1,1);"><i class="fas fa-search"></i>&nbsp;<? echo $t->t( 'suchen' ); ?></button>
				</div>
			</div>
	  </div> <!-- id="tabs-4" -->

<?php
if( ($setup['enable_fin_search'] == 1) && ($userinfo['vin_search_inactive'] == 0) ) {
?>
		<div id="tabs-5" class="my-search-container">
			<div class="row">
				<div class="col-md-12 sr">
				<div class="accent-cell">
					<div class="input-group input-group-sm">
						<input type="text" class="form-control" id="search_fin" name="search_fin" value="" placeholder="<? echo $t->t( 'Suche nach' ); ?>">
					</div>
				</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 sr">
					<select class="form-control form-control-sm" id="search_fin_vehicle" name="search_fin_vehicle" onChange="fill_shortcuts(); fill_categories(0);">
						<option value="0"><? echo $t->t( 'Fahrzeug' ); ?></option>
					</select>
				</div>
				<div class="col-md-2 sr">
					<button type="submit" class="float-right btn btn-primary moj-btn-sm" href="#"  onClick="fill_fin_vehicles();"><i class="fas fa-search"></i>&nbsp;<? echo $t->t( 'suchen' ); ?></button>
				</div>
			</div>
			<div class="row">
				<div class="banner-center col-md-12 sr">
					<?php
					echo $tec->get_ad_picture( "banner-center-1" );
					?>
				</div>
			</div>
	  </div> <!-- id="tabs-5" -->
<?php
}
?>
<?php
if( ($setup['enable_fin_search_audatex'] == 1) && ($userinfo['vin_search_inactive'] == 0)) {
?>
		<div id="tabs-6" class="my-search-container">
			<table class="table table-sm">
				<tr>
					<td colspan="3">
						<div class="alert alert-info" role="alert">
							<a href="/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/examples/audatex_info.pdf" target="_blank">
							  <img style="float:left; height: 32px; margin-right: 20px;" src="<?php  echo $theme->get_icon_path( "pdf" ); ?>">
							</a>
							<?php  echo $setup['info_text_audatex']; ?>
						</div>
					</td>
				</tr>
				<tr>
		    	<th><? echo $t->t( 'Suche nach' ); ?></th>
					<td style="width: 24px;"><div class="ajax_loader search_fin_audatex_loader"></div></td>
			    <td style="vertical-align: middle;">
			    	<div style="float: left;">
			    		<input type="text" id="search_fin_audatex" name="search_fin_audatex" value="" style="width: 100%;">
			    	</div>
			    </td>
			  </tr>
				<tr>
			    <td colspan="2"></td>
			    <td>
			      <a href="#" onClick="open_3d_search();" class="link_click_button"><? $f->print_button( '3D Suche starten' ); ?></a>
			      <a href="#" onClick="import_from_3d_search();" class="link_click_button"><? $f->print_button( 'OEM aus 3D Suche importieren' ); ?></a>
			    </td>
				</tr>
			</table>
  	</div> <!-- id="tabs-6" -->
<?php
} // if
?>
<div id="shortcut_list_hider" style="display: none;">
	<div id="shortcut_list" class="row mb-0 d-flex flex-sm-wrap"></div>
</div>
</div> <!-- id="search_tabs" -->

</div> <!-- id=search-section -->

<div id="featured" class="col-xl-4 col-lg-4 featured">
	<?php
	echo $tec->get_ad_picture( "banner-center-2" );
	?>
</div>

	<div id="results-section">
		<div id="search_result">
			<div class="row">
				<div class="col-12">
					<div id="categories-mobile" class="flex mr-1" style="float: left; min-width: 225px">
						<div id="search_shortcuts_categories" class="sr" style="display: none;">
							<div>
								<div class="orange-head" style="height: 31px; text-align: center;display:none;"><? echo $t->t( 'Gruppen' ); ?></div>
								<div class="orange-head" style="height: 31px; text-align: center;"><span id="categories_title"><? echo $t->t( 'Kategorien' ); ?></span></div>
							</div>
							<div>
								<div style="display:none;">
									<div id="search_shortcuts">
									</div>
								</div>
								<div id="search_categories">
								</div>
							</div>
						</div>
					</div>

					<div class="d-flex flex-fill">
						<div id="search_articles" class="w-100" style="display: none;">
							<div class="search_articles_top sr">
								<div>
									<a href="#" onClick="show_articles_type(0);" class="link_click_button"><? $f->print_button( 'Liste' ); ?></a>
									<a href="#" onClick="show_articles_type(1);" class="link_click_button"><? $f->print_button( 'Karte' ); ?></a>
											<div class="ajax_loader search_articles_loader"></div>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-sm" id="search_articles_table">
									<thead class="article-row-head">
										<tr class="head-row">
											<th><? echo $t->t( 'Bild' ); ?></th>
											<th data-sort="string" style="cursor: pointer;"><a><? echo $t->t( 'Artikel Nr.' ); ?></a></th>
										  <th data-sort="string" style="cursor: pointer;"><a><? echo $t->t( 'Marke' ); ?></a></th>
										<th></th>
										<th data-sort="string" style="border-left: none; cursor: pointer;"><a><? echo $t->t( 'Beschreibung' ); ?></a></th>
										<th></th>
											<th data-sort="int" style="min-width: 50px; cursor: pointer;"><a>&nbsp;<? /*echo $t->t( 'Lager' );*/ ?></a><span class="arrow">&nbsp;<i class="fas fa-angle-down"></i></span></th>
										<?php
										if( $setup['enable_topmotive_api'] == 1 ) {
											$c = "";
											if( $setup['need_location'] == 1 ) $c = ' class="need_location"';

											echo '<th style="text-align: center" id="enable_location_id" data-blocked_locations="'.$setup['blocked_locations'].'"'.$c.'>'.$t->t( 'Lagerort' ).'</th>';
										} // if
										?>
										<th style="text-align: center"><? echo $t->t( 'Menge' ); ?></th>
										<th data-sort="float" style="cursor: pointer;" class="right"><? echo $t->t( 'Preis' ); ?></th>
										<th style="display: none;"><? echo $t->t( 'Gesamt' ); ?></th>
											<th style="text-align: center"><i class="fas fa-shopping-cart"></i></th>
										</tr>
										<tr class="spacer"></tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<div id="search_articles_big" class="row" style="display: none; margin-top: -5px;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- results-section -->

<div id="search_articles_infos_hider" class="container" style="display: none;">
	<div class="row sr">
		<div class="col-12 d-flex">
			<?
			$tec->get_articles_infos_categories( true );
			?>
			<div id="search_articles_infos" class="flex-fill"></div>
		</div>
	</div>
</div>

<?php
if( $setup['iframe_in_suche'] ) {
	$userinfo = $f->load_user( $c_user_id, "show_iframe" );
	$setup2 = $f->load_setup( "CORE_SETUP" );
	if( ($setup2['iframe_link'] != "") && ($userinfo['show_iframe'] == 1) ) {
		echo '
		<div style="clear: left;"></div><br><div id="show_iframe" style="text-align: center; width: 100%; left: 0; bottom: 0;">
			<iframe id="tec_iframe" src="'.$setup2['iframe_link'].'" frameborder="0" style="width:820px; height: 620px;></iframe>
		</div>';
		$db->update( "CORE_USER_INFO", array( "show_iframe" => 0 ), "user_id='".$c_user_id."'" );
		$db->commit();
	} // if
} // if

?>
</div> <!-- row -->

<div class="row main-banner sr">
	<div>
		<?php
		if( $setup['customer_info'] != "" ) {
			echo '<div class="alert alert-danger my-alert" role="alert"><span class="align-middle"><i class="fas fa-info-circle align-middle"></span></i><span class="align-middle"> '.$setup['customer_info'].'</span></div>';
		} // if
		echo $tec->get_ad_picture( "banner-list-center" );
		?>
	</div>
</div>
<?php
require_once( CLASS_DIR."templates/footer.php" );
?>
<script>$(document).ready(function(){ $(window).scrollTop(0); });</script>
