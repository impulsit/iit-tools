<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

$f->set_back_up_file = 'admin/list_redirect.php?indiv_list_id=19';

require_once( CLASS_DIR."templates/header.php" );

$strOut = '<h2>Ansicht des Fragebogens von Benutzer</h2>';

$id = $_GET['id'];

echo $f->ansicht( $id );

require_once( CLASS_DIR."templates/footer.php" );
?>