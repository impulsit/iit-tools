<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

$fragebogen_id = isset( $_GET['fragebogen_id'] )?$_GET['fragebogen_id']:(isset( $_POST['fragebogen_id'] )?$_POST['fragebogen_id']:0);

if( isset( $_POST['export'] ) ) {
	echo $interface->export_fragebogen( $_POST['fragebogen_id'] );
	exit;
} // if

if( isset( $_POST['import'] ) ) {
	$mes->addInfo( $interface->import_fragebogen() );
} // if

require_once( CLASS_DIR."templates/header.php" );

echo '<div id="content_scroll"><h2>Datentransfer Export</h2>';

echo '<form method="post" action="why_datentransfer.php">';

// Fragebogen wählen
$list1 = '';
$db->query( "SELECT fragebogen_id, titel FROM WHY_FRAGEBOGEN ORDER BY titel" );
while( $db->isNext() ) {
	$r = $db->getNext();

	$sel = '';
	if( $r['fragebogen_id'] == $fragebogen_id ) $sel = ' selected';

	$list1 .= '<option value="'.$r['fragebogen_id'].'"'.$sel.'>'.$r['titel'].'</option>';
} // while
echo '
	<table id="eingabe_fragebogen" class="list_left shadow">
		<tr>
			<th>Fragebogen</th>
			<td>
				<select name="fragebogen_id" data-placeholder="Fragebogen wählen..." class="chosen-select" style="width: 300px">
					<option value=""></option>
					'.$list1.'
				</select>
			</td>
		</tr>
	</table>';

echo '
	<br />

	<input type="hidden" name="export" value="1">
	<a onClick="$(this).closest(\'form\').submit()" class="link_click_button">'.$f->get_button( 'Exportieren' ).'</a>
	</form><div style="clear: left;"></div><br />';

echo '<div id="content_scroll"><h2>Datentransfer Import</h2>';

echo '
	<form method="post" action="why_datentransfer.php" enctype="multipart/form-data">
		<input type="hidden" name="MAX_FILE_SIZE" value="100000">
		<input type="file" name="datei">
		<br />

		<input type="hidden" name="import" value="1"><br />
		<a onClick="$(this).closest(\'form\').submit()" class="link_click_button">'.$f->get_button( 'Importieren' ).'</a>
	</form>';

echo '</div>';

require_once( CLASS_DIR."templates/footer.php" );
?>