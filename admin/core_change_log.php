<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );

function merge_updates( $n_update, $update, $project, $title ) {
	foreach( $update as $k => $v ) {
		$update[$k]['project'] = $project;
		foreach( $update[$k]['items'] as $k2 => $v2 ) {
			$update[$k]['items'][$k2] .= ' <i>('.$title.')</i>';
		} // foreach
	}
	$n_update = array_merge_recursive( $n_update, $update );

	return( $n_update );
}

$n_update = array();
require_once( "core_change_log_array.php" );
$n_update = merge_updates( $n_update, $update, $project, 'Basis' );
if( file_exists( "core_change_log_array_basilica_at.php" ) ) {
	require_once( "core_change_log_array_basilica_at.php" );
	$n_update = merge_updates( $n_update, $update, $project, 'basilica' );
} // if
if( file_exists( "core_change_log_array_tecdocbase_atit_at.php" ) ) {
	require_once( "core_change_log_array_tecdocbase_atit_at.php" );
	$n_update = merge_updates( $n_update, $update, $project, 'atitec' );
} // if

$update = $n_update;
ksort( $update );
$update = array_reverse( $update );
?>
<div id="content_scroll">
<?
foreach( $update as $k => $v ) {
	$bShow = false;

	foreach( $v['project'] as $k2 => $v2 ) {
		$bShow = $bShow | $f->project_allowed( $v2 );
	} // foreach

	if( $bShow ) {
		if( is_array( $v['date'] ) ) $v['date'] = $v['date']['0'];
		echo '<h2>Update '.$k.' ('.$v['date'].')</h2><ul>';
		foreach( $v['items'] as $k2 => $v2 ) {
			echo '<li>- '.$v2.'</li>';
		} // foreach
		echo '</ul><br />';
	} // if
} // foreach

require_once( CLASS_DIR."templates/footer.php" );
?>