<?php
$project = array( 4 );
$update = array(
		"4.31" => array(
				"date" => "07.01.2019",
				"items" => array(
						"ajax Fehler anzeigen ab User Level 990 anstatt 100",
						"gmt: entfernen von alten WebService Bedinungen von Update 3.92",
						"Suche: Banner Center ausblenden bei Suche starten",
						"Send Order: Anpassung Fehlerhandling",
						"Suche: Korrektur Anzeige, ScrollToTop",
						"WebService: Anpassung Klassen Contructor, php > 5.6 da deprecated",
				)
		),
		"4.26" => array(
				"date" => "14.12.2018",
				"items" => array(
						"Übersetzungen",
				)
		),
		"4.25" => array(
				"date" => "14.12.2018",
				"items" => array(
						"neue Instanz: arvcentar",
						"Suche: neue Checkboxen: Attribute anzeigen, nur verfügbare Artikel",
						"Result Tabelle: Beschreibung: Umbruch wenn zu lange auf break-word",
						"Bestellübersicht: Belegart und Lieferart einblenden",
				)
		),
		"4.24" => array(
				"date" => "08.12.2018",
				"items" => array(
						"gmt: Anpassung WebService",
						"Wenn user_level >= 100, dann ajax Fehler per Popup anzeigen",
						"Logos: gmt, gazela",
						"neues Template: Grau",
						"Belegart und Lieferart lt. Definition",
						"EAN auf Artikelkarte anzeigen",
						"Übersetzungen",
						"Systemmenü: visible Notice korrigiert"
				)
		),
		"4.23" => array(
				"date" => "21.11.2018",
				"items" => array(
						"eigenes css für Farben",
						"css für Instanz laden",
						"cronjob: Artikelprüfen, entfernen von ' in Nr.",
						"cronjob: Import, entfernen von ' in allen Feldern",
						"neue Sprachen: albanisch, mazedonisch",
						"Prüfung Artikel: Modul aktivierbar in Setup, Spalte überall ausblenden wenn inaktiv",
						"interne Artikel: alles löschen Button (je nach Setup)",
						"Banner: Klassen ersetzt wegen Ad-Blocker",
						"Result Tabelle: Beschreibung: Umbruch wenn zu lange",
						"neue Instanzen: mikramatik, kitcommerce, (gazela reaktiviert)",
						"Suche Button in Artikelliste für aktuelle Zeile",
						"Update Import Universalartikel.xls",
						"Domain Handling: atit.at umleiten auf atit-solutions.eu",
						"diverse favicons korrigiert",
				)
		),
		"4.22" => array(
				"date" => "12.11.2018",
				"items" => array(
						"Lagerort in Kartenansicht",
						"Banner Liste (Ansicht): neue Sortierung id DESC",
				)
		),
		"4.21" => array(
				"date" => "09.11.2018",
				"items" => array(
						"neue Version von jstree, wegen Depreciation",
						"Univeral Items uploadbar im Uploader",
						"Anpassung Autosize #content_scroll",
						"Uploader Korrektur, wenn /upload nicht existiert",
						"Web Service Anpassung: Wenn Mapping aktiv, aber Artikel nicht gefunden wird, WholesalerArtNr auf leer setzen",
						"Upload Verzeichnis bei Banner Autoupdate, wenn Liste geöffnet",
				)
		),
		"4.20" => array(
				"date" => "08.11.2018",
				"items" => array(
						"Anpassung Übersetzung/Cache Reset",
						"neue Logos, Favicons",
						"neue Instanz kit",
						"Layout Anpassungen",
				)
		),
		"4.19" => array(
				"date" => "07.11.2018",
				"items" => array(
						"Anpassung #content_scroll auf Bildschirmhöhe",
						"Korrektur FIN Suche",
						"neue Instanzen (technisch, keine Datenbanken): tokic, potokar, bartog, megaauto",
						"Korrekturen: Import Cron Job, wenn Datei während des Imports gelöscht wird",
						"User Liste: Fehler bei Suche korrigiert",
						"Banner Liste: neue Sortierung id DESC",
						"Login: Übersetzungen, Info Fenster",
						"automatisch auf Kartenansicht, wenn Bildschirm < 992px",
						"Shortcuts aktive/inaktiv Anzeige",
						"Sehr viele Layout Änderungen",
						"Motorcode Suche korrigiert",
				)
		),
		"4.18" => array(
				"date" => "27.10.2018",
				"items" => array(
						"Anpassung an neue Chrome Version",
						"Anpassung Drucken (Karte, Bestellungen)",
						"Datensatz löschen: neue Confirm Box",
						"Performance/Cache/ASync: Translation, Load History",
						"Load History: Bugfix PS/KW",
						"Performance WS Abfragen (generell)",
						"Einstellungen: zusätzliche Artikelabfragen (x25)",
						"Ersatzartikelkarte: Register Artikel, Bild, technische Daten, Komponenten ausblenden",
						"Platzproblem: Linkes Banner nur bei Suche, Warenkorb, Bestellungen und Einstellungen anzeigen (User Level 10)",
						"Übersetzungen aktualisiert",
						"Dialog Fenster modal",
						"Benutzer Liste umschaltbar, Spalten ausgeblendet",
						"Performance WS TecDoc Abfragen",
						"Performance WS Kunden ERP Abfragen",
						"zusätzliche CSS Anpassungen",
						"Korrektur FIN Suche",
				)
		),
		"4.17" => array(
				"date" => "27.10.2018",
				"items" => array(
						"Anpassung Layout V2",
						"PDF Punkte: 12, 17",
						"viele technische Anpassungen bez. Bootstrap und Layout V2"
				)
		),
		"4.16" => array(
				"date" => "24.10.2018",
				"items" => array(
						"Anpassung Layout V2",
						"Alert Box ersetzt",
						"PDF Punkte: 9, 14, 15, 16"
				)
		),
		"4.15" => array(
				"date" => "17.10.2018",
				"items" => array(
						"Anpassung Layout V2",
						"PDF Punkte: 1, 2, 3, 4, 5, 6, 7, 8, 10, 13",
				)
		),
		"4.14" => array(
				"date" => "08.10.2018",
				"items" => array(
						"Anpassung WebService für megaauto",
				)
		),
		"4.12" => array(
				"date" => "12.09.2018",
				"items" => array(
						"neue Serverinstanz: test-v2",
				)
		),
		"4.11" => array(
				"date" => "03.09.2018",
				"items" => array(
						"neue Serverinstanz: total-trade",
				)
		),
		"4.10" => array(
				"date" => "18.06.2018",
				"items" => array(
						"AmBrands: löschen von ' bei Abfrage (AKRON-MALO')",
				)
		),
		"4.09" => array(
				"date" => "07.06.2018",
				"items" => array(
						"Cronjob Import: Anpassung wenn File während des Imports gelöscht wird",
				)
		),
		"4.06" => array(
				"date" => "09.05.2018",
				"items" => array(
						"neue Serverinstanzen: acs",
				)
		),
		"4.05" => array(
				"date" => "11.04.2018",
				"items" => array(
						"Brutto inkl. Ust in den Tooltips erweitern",
						"Datenstruktur an TecDoc angepasst bei getArticleLinkedAllLinkingTargetsByIds3",
						"Sprachupdate",
						"Lagerorte mit (M) [Mainshop] und (S) [Subshop] kennzeichnen nur bei user_id<=10",
				)
		),
		"4.04" => array(
				"date" => "10.04.2018",
				"items" => array(
						"get_linked_attributes(): Debug, Analyse, Testdatei php",
						"Korrekturen: SubShop Webservice",
						"neue Felder in Bestellungenlog"
				)
		),
		"4.01" => array(
				"date" => "18.01.2018",
				"items" => array(
						"2 Benutzerverwaltung",
						"- neue Felder in Benutzerliste (Projekt), Benutzerliste (Superadmin), Basiseinrichtung",
						"- Update DropDown Boxen, Fehler bei Images",
						"- Mailversand bei Neuanlage, Berechtigungen für Sub Admin, Listen/Karten Anpassung",
						"- Emailversand, Benutzerfelder für SubShop Pfichtfelder, erstellt_von nur beim 1.Mal setzen",
						"3 Darstellung und Verarbeitung",
						"3.1 Sprache für TecDoc Abfragen",
						"3.2 Request zum ERP",
						"3.3 Preise",
						"3.3.1 Hauptpreis",
						"3.3.2 Preis inkl. Mehrwertsteuer",
						"Logo bei Suche anzeigen",
						"Neue Instanzen: avanti, profikat",
						"Anzeige Korrekturen bei Buttons",
						"Cronjob: Korrektur, wenn Datei gelöscht wurde"
				)
		),
		"3.94" => array(
				"date" => "07.12.2017",
				"items" => array(
						"TD Parameter wurde verändert: get_components(): getArticlePartList -> linkingTargetType -> 'U'",
				)
		),
		"3.92" => array(
				"date" => "30.11.2017",
				"items" => array(
						"Anpassung WebService für gmt",
						"Logo Mikramatic",
						"Sprachupdate"
				)
		),
		"3.89" => array(
				"date" => "22.11.2017",
				"items" => array(
						"Neue Sprache Slovenisch",
				)
		),
		"3.87" => array(
				"date" => "22.11.2017",
				"items" => array(
						"neue Serverinstanzen: sanel, mbauto, mikramatic, gmt",
						"Umstellen von Backups",
						"letzte Version von MySQL Dumper",
				)
		),
		"3.86" => array(
				"date" => "11.11.2017",
				"items" => array(
						"Universalartikel – neues Attribut",
						"Universalartikel – Anzahl Resultate",
						"Mengenüberschreitung bei Universalartikel",
						"Übersetzung"
				)
		),
		"3.84" => array(
				"date" => "29.09.2017",
				"items" => array(
						"Update URL für Audatex Live System",
						"Feld Location ID in Text umgewandelt (vorher INT)",
						"WebService bei Class User immer den Namen des Benutzers senden",
						"neue Einrichtung für interne Versionen (JQuery, Bootstrap) -> BETA"
				)
		),
		"3.83" => array(
				"date" => "25.09.2017",
				"items" => array(
						"1 Farbe für Register Suche nach FIN",
						"2 VIN Protokoll (nur für SuperAdmin)",
						"3 Neuer Menüpunkt – (Admin) VIN Statistik",
						"4 Neue Einrichtung am Benutzer FIN Suche nicht aktiv",
				)
		),
		"3.82" => array(
				"date" => "25.09.2017",
				"items" => array(
						"WebService Geschwindigkeit: Spezialfall in Daten abfangen, item_no=local_item_no",
				)
		),
		"3.80" => array(
				"date" => "04.09.2017",
				"items" => array(
						"Neuer Server simimpex",
						"WebService Anpassungen: Username im Segment User mitschicken",
				)
		),
		"3.79" => array(
				"date" => "30.08.2017",
				"items" => array(
						"Audatex FIN Suche",
				)
		),
		"3.78" => array(
				"date" => "25.07.2017",
				"items" => array(
						"1.1 Fehler – Benutzer deaktivieren",
						"1.2 Fehler – Menge",
						"2.1 Erweiterung – Universalartikel (Datei und Suche)",
						"2.2 Erweiterung – RV Trade Favicon",
						"2.3 Erweiterung – interne Nummer und Beschreibung im Warenkorb",
						"Sprachupdate",
						"Universalartikel Suche abgeändert: Subkategorie wird immer als Filter übergeben",
						"Bestellnumer anlegen: Es wird immer eine neue Nummer gezogen",
						"neuer Server bosut.atit-solutions.eu",
				)
		),
		"3.76" => array(
				"date" => "24.05.2017",
				"items" => array(
						"Korrektur Lagerortabfrage",
				)
		),
		"3.75" => array(
				"date" => "22.05.2017",
				"items" => array(
						"1 Preise pro Lagerort",
						"1.1 Nettopreis bei Lagerorten in Klammern anzeigen",
						"1.2 gleiche Logik bei Universalartikel",
						"2 Eigene Einrichtung für Admin: In Basiseinrichtung und Benutzereinrichtung (wenn Benutzerlevel >= 100)",
						"Anzeige Werte: Decimal und Tausender Format"
				)
		),
		"3.73" => array(
				"date" => "12.04.2017",
				"items" => array(
						"1 Anpassung Warenkorb",
						"2 Viber",
						"3 Anpassung Login-Seite",
				)
		),
		"3.72" => array(
				"date" => "05.04.2017",
				"items" => array(
						"zum brand zusätzlich brand_id mitschleifen",
				)
		),
		"3.71" => array(
				"date" => "04.04.2017",
				"items" => array(
						"Hotfix: HTML Code aus brand_id filtern beim Warenkauf",
				)
		),
		"3.70" => array(
				"date" => "28.03.2017",
				"items" => array(
						"Bestellung an WebShop schicken: Fehlerhandling verbessert",
						"1.1 Fehler Bestellung",
						"1.2 Fehler – Benutzeranlage",
						"2 Erweiterung - Export Excel in 4 Listen",
						"3 Übersetzung",
						"Neue Liste TecDoc - Bestellungen Log in allgemeinen Listen"
				)
		),
		"3.69" => array(
				"date" => "23.03.2017",
				"items" => array(
						"bei SendOrder nur Zeilen mit Menge > 0 schicken",
				)
		),
		"3.66" => array(
				"date" => "16.03.2017",
				"items" => array(
						"Sprachanzeige im Hauptmenü",
						"WebService Gross/Kleinschreibung für Preis",
				)
		),
		"3.65" => array(
				"date" => "28.02.2017",
				"items" => array(
						"1 Fehler – Benutzeranlage",
						"2 Anzeige Brutto-Preis",
						"3 Neues Menü interne Nummern",
						"Listen Browser: Anzeige limitiert auf 30 Seiten (3000 Datensätze)",
						"Neue Übersetzungen"
				)
		),
		"3.64" => array(
				"date" => "15.02.2017",
				"items" => array(
						"1.1 Fehler Artikel in Warenkorb legen",
						"1.2 Problem mit Lager- und Bestellmengen",
				)
		),
		"3.63" => array(
				"date" => "07.02.2017",
				"items" => array(
						"2.4 DataConnector",
						"Übersetzung",
						"Update: Dateien anders sortieren, nur Aktuelles und 10 vorige Dateien anzeigen"
				)
		),
		"3.62" => array(
				"date" => "07.02.2017",
				"items" => array(
						"Basis: in Listen neue Option Pflichtfeld",
						"2.1 Lagerort Pflichtfeld: neues Setup Feld",
						"2.2 Benutzeranlagekarte: ",
						" - Feld Email, Name und Passwort ist nun ein Pflichtfeld",
						" - Dialog abspeichern: Fehler ausgeben, falls einer auftritt",
						" - Feldübersetzung programmiert",
						"2.3 Artikelkarte: Dialog Fenster: breiter/höher",
						"Übersetzung im Javascript angepasst",
						"Flaggen für Benutzer verbergen korrigiert",
						"Bei Auswahl Lagerort den Lagerstand generell entfernen",
				)
		),
		"3.61" => array(
				"date" => "05.02.2017",
				"items" => array(
						"sämtliche TecDoc Felder einrichtbar in Einstellungen",
						"Update Dateien werden nun verschlüsselt geladen"
				)
		),
		"3.60" => array(
				"date" => "27.01.2017",
			"items" => array(
				"Anpassung WebService: Preise und SendOrder, Klasse Item()",
				"WebService: Lagerort schicken verbessert",
				"Elpida: Ländercode auf HR"
			)
		),
		"3.59" => array(
				"date" => "26.01.2017",
			"items" => array(
				"GUI: Warenkorb Lagerort in () [nicht druckbare Zeichen > CHR(127)] entfernen",
				"Korrektur Send Order",
				"Abspeichern von SendOrder XML in Datenbank"
			)
		),
		"3.58" => array(
				"date" => "20.01.2017",
			"items" => array(
				"Anpassungen SPRACHCODE",
			)
		),
		"3.57" => array(
				"date" => "19.01.2017",
			"items" => array(
				"Nuic - Sprachcode auf HR ändern",
				"Autodemo ID auf RVTrade ändern",
			)
		),
		"3.56" => array(
				"date" => "16.01.2017",
			"items" => array(
				"1 Neuer Menüpunkt – (Admin) Benutzer",
				"letzter Login in Datenbank speichern",
			)
		),
		"3.55" => array(
				"date" => "12.01.2017",
			"items" => array(
				"2.1 Bilder bei Ersatzartikeln (Artikel mit *)",
				"Suche nach interner Nummer von Ersatznummern",
				"autodemo WebService auf RVTrade leiten",
				"Admin / Artikel -> Spalten erweitern"
			)
		),
		"3.54" => array(
				"date" => "09.01.2017",
				"items" => array(
						"1.1 Fehler bei RV-Trade: Preis Universalartikel im Warenkorb = 0",
						"1.2 Fehler bei Elpida: Leerzeichen werden entfernt nach Eingabe und < (HTML Tag) wird ignoriert",
						"2.2 Suche in den Universalartikel: Suchanzeige Rechts oben, nach Laden springen zur Tabelle"
				)
		),
	"3.52" => array(
			"date" => "05.12.2016",
			"items" => array(
					"RVTrade: Neuer Ländercode HR",
					"1 Suche nach interner Nummer",
					"2 Kategorie-Filter"
			)
	),
	"3.51" => array(
		"date" => "15.11.2016",
		"items" => array(
			"Suche nach FIN Farbanpassung",
			"Menü zuklappen verbessert",
		)
	),
	"3.50" => array(
		"date" => "14.11.2016",
		"items" => array(
			"1 Artikel bah0011 bei Elpida: Fehler seit dem letzten Update",
			"2 Menü zuklappen",
			"3 Vin Suche Orange färben",
			"4.1 Login Fenster",
			"4.2 Schwarzen Strich entfernen",
			"5 Artikelkarte -schwarze Umrandungen entfernen",
		)
	),
	"3.49" => array(
		"date" => "25.10.2016",
		"items" => array(
			"1 Fehler: Fehlende Service Information",
			"2 NEU: 2 neue Funktionen aufrufen bei Suche nach Fahrzeug",
			"Listen Layout",
		)
	),
	"3.48" => array(
		"date" => "18.10.2016",
		"items" => array(
			"Korrektur Suche PKW/LKW",
			"Umblättern bei Artikelkarte (passende Fahrzeuge)",
			"Bilder für LKWs angepasst",
			"2.1 Optionen der Funktion getDirectArticleByIDs6",
			"Korrektur Service CZ/DE",
			"Sprachupdate",
		)
	),
	"3.47" => array(
		"date" => "10.10.2016",
		"items" => array(
			"2.3 LKW- und Bus-Daten",
		)
	),
	"3.46" => array(
		"date" => "06.10.2016",
		"items" => array(
			"Kein Umbruch bei Artikelnr und interner Nummer erzwingen (nobr)",
			"Korrektur interne Artikelnummern Anzeige",
			"Tabelle Artikelliste auf 100% Breite",
		)
	),
	"3.44" => array(
			"date" => "04.10.2016",
			"items" => array(
					"2.1 Log-Off Hinweis: Wenn Session nicht mehr gültig, wird nun zur Startseite umgeleitet",
					"2.2 Anzeige interne Artikelnummer und interne Bemerkung",
					" - Einstellungen - WebService Mapping aktivierbar",
					" - Einstellungen - Interne Nummer anzeigen, Interne Bemerkung anzeigen",
					" - Anzeige in Artikelliste",
			)
	),
	"3.43" => array(
			"date" => "01.10.2016",
			"items" => array(
					"RV-Trade Configs / WebService",
					"RV-Trade, Elpida Universal Bilder",
			)
	),
	"3.41" => array(
		"date" => "21.09.2016",
		"items" => array(
				"autodemo: Logo",
				"Verbesserungen FIN Suche",
				"Anpassung EuroAuto SendOrder"
		)
	),
	"3.41" => array(
			"date" => "20.09.2016",
			"items" => array(
					"Suche nach FIN",
			)
	),
	"3.39" => array(
		"date" => "19.09.2016",
		"items" => array(
			"Übersetzung",
			"1.1.2 Universalartikel, neue Bilder",
			"1.1.3 Suche nach Fahrzeug – Layout",
			"Nuic & Autodemo: Länderkennzeichen von ba auf hr geändert"
		)
	),
	"3.38" => array(
		"date" => "12.09.2016",
		"items" => array(
			"1.1.1 Suche nach Fahrzeug, Anzeige der Lagerliste",
			"1.1.2 Universalartikel, Bemerkung 3",
			"1.1.3 Universalartikel, interne Nummer",
			"1.1.4 Universalartikel, Infotext für die Suche",
			"1.1.5 Universalartikel, neue Bilder",
			"1.1.6 Universalartikel, Lagerliste in den Ergebnissen anzeigen",
		)
	),
	"3.37" => array(
		"date" => "08.09.2016",
		"items" => array(
			"2.1 Korrekturen",
			"5 Korrektur Warenkorb",
		)
	),
	"3.36" => array(
		"date" => "07.09.2016",
		"items" => array(
			"2.1 Import Universalartikel: neuer Importer, Kategorie, Subkategorie, Baum bei Suche",
			"2.2 Statistik gesucht nicht gekauft - Excel Export",
			"2.3 Suche nach Fahrzeug – letzte suchen",
			"2.4 Suche nach Fahrzeug PS/kW Filter",
			"7 Autodemo: Spracheanzeige"
		)
	),
	"3.35" => array(
		"date" => "18.08.2016",
		"items" => array(
			"Bestimmung der brand_id erweitert für Marken die nicht registriert sind",
			"Artikelliste bei Suche Breite = 70%",
			"Farbanpassung für Updatehinweis",
			"Korrektur Suche nach Text (Auto-Complete Box)",
			"Anpassung Euroauto WebService Schnittstelle"
		)
	),
	"3.34" => array(
		"date" => "17.08.2016",
		"items" => array(
			"Korrektur WebService Abfrage bei bestimmten Artikeln",
			"Suche nach Bemerkungen um Bild erweitert",
			"Lagerort in Warenkorb anzeigen",
			"Suche mit Kundenummer Korrektur",
			"Importer überarbeitet auf neues xlsx Format",
			"Import für WebService Mapping",
			"WebService Anpassung für Euroauto",
		)
	),
	"3.33" => array(
		"date" => "04.08.2016",
		"items" => array(
			"Korrektur WebService Abfrage, wenn Sprache nicht erlaubt",
			"Layout lt. Vorlage",
		)
	),
	"3.32" => array(
		"date" => "03.08.2016",
		"items" => array(
			"je nach Anzeige Sprache den zugehörigen TecDoc Sprachcode senden",
			"Euroauto: Webservice Abfrage korrigiert",
			"Layout lt. Vorlage",
		)
	),
	"3.31" => array(
			"date" => "26.07.2016",
			"items" => array(
					"Sprachupdate",
					"Layout Korrekturen im Logobereich",
					"1.2 Farbschema",
					"1.3 Reset-Button",
					"1.4 Button „nach oben“ entfernen",
					"1.5 Kundennummer (anzeigen nur wenn web-service aktiv)",
					"1.6 atitec Grafik links unten",
					"2.1 Autodemo",
					"2.2 Euroauto (Serbien)",
					"2.3 RVTrade (Serbien)",
			)
	),
	"3.30" => array(
		"date" => "21.07.2016",
		"items" => array(
			"Benutzerimport erweitert um Spalte Name",
			"Konfiguration Nuic",
			"Update Prozess korrigiert, damit ältere Updates auch ausgeführt werden",
		)
	),
	"3.29" => array(
		"date" => "18.07.2016",
		"items" => array(
				"Updateprozess verbessert",
				"Bei Suche nicht mehr springen, Artikel rechts positioniert",
				"Sprache aktualisiert",
			)
	),
	"3.28" => array(
			"date" => "14.07.2016",
			"items" => array(
					"intern: neuer Kunde: automercskoro.atit-solutions.eu",
					"Table USER_INFO Feld email Unique für Import",
					"Update Routine",
			)
	),
	"3.26" => array(
		"date" => "12.07.2016",
		"items" => array(
			"Benutzer Import",
			"Korrektur: Rechtesystem, Projektzuordnung für TecDoc Projekte",
			"intern: neue Kunden: rvtrade.atit-solutions.eu, euroauto.atit-solutions.eu, gazela.atit-solutions.eu, jurprom.atit-solutions.eu, timko.atit-solutions.eu",
			"6 Suche nach Fahrzeug - Suchhistorie merken",
		)
	),
	"3.25" => array(
		"date" => "11.07.2016",
		"items" => array(
			"2 Nuic Server -> erledigt",
			"3 Kundennummer in Login-Tabelle -> erledigt",
			"4 Begriff Topmotive darf nicht vorkommen -> erledigt",
		)
	),
	"3.24" => array(
			"date" => "07.07.2016",
			"items" => array(
					"neues Projekt nuic",
					"Errorhandling topmotive Benutzer",
					"tecdoc Klassenfiles angepasst",
					"Rechte angepasst",
					"1.1 Feld Lagerort schmäler -> ist nun 50px breit",
					"2.1 Kartenansicht -> Bemerkung ausgeblendet",
					"2.2 In Warenkorb legen geht nicht -> korrigiert",
					"3.1 Preis, Beträge fehlen -> korrigiert",
					"3.2 Spalte Gesamt entfernen",
					"3.3 Lagerstand Text ja/nein entfernen -> entfernt",
					"3.4 Message bei In Warenkorb legen -> ist nun 1 Sekunde",
			)
	),
	"3.23" => array(
		"date" => "05.07.2016",
		"items" => array(
			"Errorhandling Web API",
			"5.3 Anzeige Lagerstand pro Filiale und Möglichkeit aus einer Filiale zu bestellen",
			"3.2 Warenkorb über WS bestätigen lassen",
			"4.2 iFrame wird nun ausgeblendet nach der 1. Suche",
			"7.9 Suche nach Ford Focus blockiert",
			"7.10 Felder auf leer setzen",
			"10.1 Kundennummer in Login-Tabelle -> in Benutzereinstellungen",
			"nicht möglich: 7.8 Beschreibung in Kategoriebildern (Priorität niedrig)",
			"nicht möglich: 10.2 Webservice Link in Einstellungen -> WebService Link ist über ein Kundenspezifisches Config definiert",
		)
	),
	"3.21" => array(
		"date" => "17.06.2016",
		"items" => array(
			"4.4 Untere Abgrenzung verbessert",
			"4.5 Zeilenhöhe",
			"5.2.1 gefixt",
			"5.4.1 gefixt",
			"7.4 Zusätzliche Suchfilter (Treibstoff, Motorcode)",
			"Schriftart Fahrzeug",
			"Suche gdb1330 verbessert",
		)
	),
	"3.20" => array(
		"date" => "16.06.2016",
		"items" => array(
			"Liste Links Farbe geändert",
			"3 Topmotive API",
			"4.1 Vertikale Linien entfernen -> Listen (autodemo)",
			"4.2 iFrame nur 1x anzeigen",
			"4.3 Schatten bei den Masken entfernen (autodemo)",
			"4.4 Untere Abgrenzung",
			"5.1 Artikeldetails - Passende Fahrzeuge",
			"5.2 Zusatzinfo Einheit in Beschreibung",
			"5.4 Bemerkung anzeigen",
			"6.1 Artikelbilder",
			"6.2 Resultate springen",
			"6.3 Icons bei Komponenten",
			"7.1 Neue Kategorie-Bilder",
			"7.2 Suche bei VW, Golf VI blockiert (funktioniert nicht, endlos Schleife)",
			"7.3 Baujahr absteigend und eingebbar",
			"7.7 Kategorie-Bilder neu",
			"8.1 Suche nach Motorcode",
		)
	),
	"3.19" => array(
		"date" => "25.03.2016",
			"items" => array(
				"neue Sprachen: Englisch, Kroatisch, Bosnisch, Russisch"
		)
	),
	"3.18" => array(
		"date" => "23.03.2016",
			"items" => array(
				"autodemo: Konfigurationsdateien",
				"neue Spalte bei Artikel: Position",
		)
	),
	"3.15" => array(
		"date" => "05.03.2016",
		"items" => array(
			"(Priorität B) Gruppenbilder",
			"Auswertung: gesucht aber nicht gekauft",
			"Korrektur Gruppenanzeige"
		)
	),
	"3.14" => array(
		"date" => "04.03.2016",
		"items" => array(
			"1 (Priorität B) Anzeige aktuelle Aktionen",
			"2.1 (Priorität B) Eingabe im Suchfeld, Drop-Down",
			"3.1 (Priorität A) Sortierung der Ergebnisse nach Lagerstand und Preis",
			"3.2 (Priorität A) Details zum Fahrzeug",
			"3.3 (Priorität A) Jahr Eingabe",
			"3.4 (Priorität B) Zweirad aus Kategorien entfernen",
			"3.5 (Priorität B) Eingabe im Suchfeld, Drop-Down",
			"3.6 (Priorität B) Kategorien mit einem Klick öffnen",
			"3.7 (Priorität A) Favorite-Fahrzeughersteller",
			"4.1 (Priorität A) Lagerstand beim Endkunden nicht anzeigen",
			"5 Suchergebnisse unten, Neuer Button um wieder hoch zu springen",
		)
	),
	"3.11" => array(
			"date" => "18.02.2016",
			"items" => array(
					"Doppelte Artikel filtern",
					"Lagerstand verringern bei OK",
					"Ersatzartikel in Warenkorb stimmen nun",
					"Ersatznummer Hersteller richtig bestimmen und richtig verknüpfen",
					"Fehlerhafte IDs korrigiert bei Ersatzartikel",
					"Bestellungen drucken",
					"Suche und Kauf loggen (intern). Auswertung erst bei Daten"
			)
	),
	"3.10" => array(
			"date" => "13.02.2016",
			"items" => array(
					"Re-Design von Kategorie befüllen",
					"Wenn mehrere Ersatzartikel vorhanden, suche nach allen nicht nur nach ersten",
					"Ersatzartikel Bestimmung: auch Farben für Ampel neu bestimmen",
					"Kartenansicht vergrößert",
					"Doppelte Artikel Ergebnisse nicht anzeigen",
					"Bemerkungsuche: Tabellen Sortierung ermöglichen",
					"Bemerkungsuche: Einkauf möglich" )
	),
	"3.08" => array(
			"date" => "09.02.2016",
			"items" => array(
					"Artikelinfo: TEC Preis ausgeblendet",
					"Artikeltabelle -> Infotext: Alle Preis in KM",
					"Suche: Ergebnisse vor AJAX löschen",
					"Such Ergebnis: wenn kein Ergebnis -> Infotext",
					"Kartenansicht: grüne/rote Kugel links",
					"passende Fahrzeuge: Karosserie anzeigen",
					"passende Fahrzeuge: Treibstoff -> zusätzliche Web Service Abfrage pro Fahrzeug -> LADEZEIT!",
					"Warenkorb: Auto-Speichern bei Bestellen",
					"passendes Fahrzeug in neuem Fenster suchen",
					"Ähnliche Suche möglich",
					"Suche nach Bemerkungen",
					"Ersatznummern Suche",
					"Import: nur Import erlauben bei gültigem Typ"
			),
	),
	"3.07" => array(
			"date" => "07.02.2016",
			"items" => array(
					"Artikel Sortierung nun im Javascript",
					"Attribute bei Hover anzeigen",
					"Artikel suche komplett neu -> TecDoc Web Service fehlerhaft" ),
	),
	"3.06" => array(
			"date" => "02.02.2016",
			"items" => array(
					"Listen / Kartenanzeige",
					"div. Infos aus Listen ausgeblendet",
					"Attribute in Liste zeigen",
					"passende Fahrzeuge: Suche mit Klick auf Hersteller",
					"neues Iconpack",
					"Such Javascript neu strukturiert" )
	),
	"3.05" => array(
			"date" => "02.02.2016",
			"items" => array(
					"nach Login -> Suche öffnen",
					"Combobox bei Suche",
					"Wechsel der Suchregister -> Cursor in Suchfeld",
					"Artikel Detailanzeige klick auf Artikelnummer, Detail Button weg",
					"Setup: Währungscode und MwSt %",
					"Hover: Artikelpreis zeigt Brutto und Nettopreis",
					"Anzeige Brutto/Netto in Warenkorb",
					"Sortierung: Artikel mit Lagerstand, dann Artikel mit Preis, dann Rest",
					"Sprache in Menü gefixt",
					"Artikel Lagerstand blau, wenn nicht auf Lager aber Preis" )
	),
	"3.04" => array(
		"date" => "01.02.2016",
		"items" => array(
			"Suche mit Enter: Default Sortierung Lagerstand",
			"Logo neu",
			"für Endbenutzer Flaggen ausblenden (User Level < 100)",
			"Suchoption: Text -> Alle Nummern",
			"in Details: bei Suche in neuem Fenster Register stimmt nicht",
			"Bilder anzeigen: addDynamicAddress",
			"Bei Suche zweimal durchlaufen: Lagerstand ganz oben, dann andere -> nur allgemeine Suche",
			"Rechtesystem: list_redirect.php gefixt",
			"Komponenten anzeigen, Suche in Akt. Fenster, neuem Fenster",
			"Bei Bestellungen -> Kommentar erfassen",
			"Anzeige offene Bestellungen im Menü gefixt",
			"Artikelinfo: Attribute als erstes",
			"Menge bestellt richtig setzen, wenn man im Warenkorb ändert",
			"Bestellungen Admin: Info von Kunde anzeigen",
			"Beim Kaufen, Preis in Warenkorb richtig setzen",
			"ArtikelSuche: letzten Suchen anzeigen",
			"Bestellinfo pro Bestellung",
			"Bestellinfo pro Bestellzeile",
			"alles OK, alles nicht OK in Bestellungen" )
	),
	"3.03" => array(
		"date" => "31.01.2016",
		"items" => array(
			"Artikelergebnis sortierbar bei Klick auf Titel",
			"Import komplett neu per Cronjob",
			"neues Logo",
			"Loginseite Sprache SR",
			"default Sprache SR",
			"Reihenfolge der Suchregister vertauscht",
			"Anzeige Preisformat im JS angepasst",
			"Default Sortierung: Lagerstand absteigend",
			"Artikel auf Lager - Feld grün",
			"Suche: Cursor in Feld setzen" )
	),
	"3.02" => array(
		"date" => "30.01.2016",
		"items" => array(
			"Backup Text Anzeige ja/nein",
			"Scriptdauer ja/nein",
			"Sortierung in listen",
			"location.txt not found",
			"Suche nach Pattern usw. loggen",
			"bei Bestellen auch loggen",
			"Suche nach <default> raus",
			"Subdomain location anpassen",
			"Preis nur aus Artikelliste",
			"Wenn Enter dann suchen" )
	),
);
?>