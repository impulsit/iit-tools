<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );

echo '<div id="content_scroll"><h2>Fragebogen Erfassung</h2>';

$fragebogen_id = isset( $_GET['fragebogen_id'] )?$_GET['fragebogen_id']:(isset( $_POST['fragebogen_id'] )?$_POST['fragebogen_id']:0);
$kurs_id = isset( $_GET['kurs_id'] )?$_GET['kurs_id']:(isset( $_POST['kurs_id'] )?$_POST['kurs_id']:0);

if( isset( $_GET['demo'] ) ) {
	$fragebogen_id = 37;

	for( $j=1; $j<=100; $j++ ) {
		$db->query( "SELECT kurs_id FROM BAS_KURSE ORDER BY RAND() LIMIT 1" );
		$r = $db->getNext();
		$kurs_id = $r['kurs_id'];

		$db->query( "SELECT trainer_id FROM BAS_TRAINER ORDER BY RAND() LIMIT 1" );
		$r = $db->getNext();
		$trainer_id = $r['trainer_id'];

		// Header speichern
		$i = $db->insert( "WHY_USER_FRAGEBOGEN", array( "id" => '' ) );
		$db->update( "WHY_USER_FRAGEBOGEN", array( "email" => "manuell_".$i, "zeit" => date( "Y-m-d", mt_rand( 1262304000, 1388534400 ) ), "fragebogen_id" => $fragebogen_id, "fertig_ausgefuellt" => 1, "kurs_id" => $kurs_id ), "id='".$i."'" );
		$db->commit();

		$_POST['antwort'][15] = array( $trainer_id => mt_rand( 71, 75 ) );
		$_POST['antwort'][16] = array( $trainer_id => mt_rand( 71, 75 ) );
		$_POST['antwort'][17] = array( $trainer_id => mt_rand( 71, 75 ) );
		$_POST['antwort'][18] = array( $trainer_id => mt_rand( 71, 75 ) );

		// Antworten speichern
		foreach( $_POST['antwort'] as $k => $v ) {
			foreach( $v as $k2 => $v2 ) {
				$trainer_id = $k2;
				$antwort_id = $v2;

				$db->insert( "WHY_USER_ANTWORTEN", array( "id" => $i, "frage_id" => $k, "antwort_id" => $antwort_id, "antwort_value" => 1, "trainer_id" => $trainer_id ) );
			} // foreach
		} // foreach
		$db->commit();
	} // for
} // if

if( isset( $_POST['save'] ) ) {
	// Header speichern
	$i = $db->insert( "WHY_USER_FRAGEBOGEN", array( "id" => '' ) );
	$db->update( "WHY_USER_FRAGEBOGEN", array( "email" => "manuell_".$i, "zeit" => date( "Y-m-d", strtotime( $_POST['datum'] ) ), "fragebogen_id" => $fragebogen_id, "fertig_ausgefuellt" => 1, "kurs_id" => $kurs_id ), "id='".$i."'" );
	$db->commit();

	// Antworten speichern
	foreach( $_POST['antwort'] as $k => $v ) {
		foreach( $v as $k2 => $v2 ) {
			$trainer_id = $k2;
			$antwort_id = $v2;

			$db->insert( "WHY_USER_ANTWORTEN", array( "id" => $i, "frage_id" => $k, "antwort_id" => $antwort_id, "antwort_value" => 1, "trainer_id" => $trainer_id ) );
		} // foreach
	} // foreach
	$db->commit();
} // if

echo '<form method="post" action="why_eingabe.php">';

// Fragebogen wählen
$list1 = '';
$db->query( "SELECT fragebogen_id, titel FROM WHY_FRAGEBOGEN WHERE link_kurs='1' ORDER BY titel" );
while( $db->isNext() ) {
	$r = $db->getNext();

	$sel = '';
	if( $r['fragebogen_id'] == $fragebogen_id ) $sel = ' selected';

	$list1 .= '<option value="'.$r['fragebogen_id'].'"'.$sel.'>'.$r['titel'].'</option>';
} // while
echo '
	<table id="eingabe_fragebogen" class="list_left shadow">
		<tr>
			<th>Fragebogen</th>
			<td>
				<select name="fragebogen_id" data-placeholder="Fragebogen wählen..." class="chosen-select" style="width: 300px">
					<option value=""></option>
					'.$list1.'
				</select>
			</td>
		</tr>
	</table>';

// Kurs wählen
$list1 = '';
$db->query( "
	SELECT DISTINCT( l.kurs_id ), k.nummer, k.startdatum, k.enddatum
	FROM BAS_LOG_FILES AS l
	LEFT JOIN BAS_KURSE AS k ON (k.kurs_id=l.kurs_id)
	WHERE fragebogen_id>0 AND l.kurs_id>0
	ORDER BY k.startdatum DESC, k.nummer ASC" );
while( $db->isNext() ) {
	$r = $db->getNext();

	$sel = '';
	if( $r['kurs_id'] == $kurs_id ) $sel = ' selected';

	$list1 .= '<option value="'.$r['kurs_id'].'"'.$sel.'>'.$f->get_kurs_titel( $r['nummer'], $r['startdatum'], $r['enddatum'] ).'</option>';
} // while
echo '
	<table id="eingabe_kurs" class="list_left shadow">
		<tr>
			<th>Kurs</th>
			<td>
				<select name="kurs_id" data-placeholder="Kurs wählen..." class="chosen-select" style="width: 300px">
					<option value=""></option>
					'.$list1.'
				</select>
			</td>
		</tr>
	</table>';

echo '
	<br />
	<a onClick="$(this).closest(\'form\').submit()" class="link_click_button">'.$f->get_button( 'anzeigen' ).'</a>
	</form><div style="clear: left;"></div><br />';

if( ($fragebogen_id > 0) && ($kurs_id > 0) ) {
	$db->query( "SELECT titel FROM WHY_FRAGEBOGEN WHERE fragebogen_id='".$fragebogen_id."'" );
	$fb = $db->getNext();

	$db->query( "
		SELECT
			k.nummer, k.startdatum, k.enddatum, t.title
		FROM BAS_KURSE AS k
		LEFT JOIN BAS_KURSTYP AS t ON (t.id=k.kurstyp_id)
		WHERE k.kurs_id='".$kurs_id."'" );
	$kurs = $db->getNext();

	$db->query( "SELECT COUNT( id ) AS anz FROM WHY_USER_FRAGEBOGEN WHERE fragebogen_id='".$fragebogen_id."' AND kurs_id='".$kurs_id."'" );
	$anz = $db->getNext();

	echo '
		<form method="post" action="why_eingabe.php">
			<input type="hidden" name="save" value="1">
			<input type="hidden" name="kurs_id" value="'.$kurs_id.'">
			<input type="hidden" name="fragebogen_id" value="'.$fragebogen_id.'">

		<table class="list_left shadow" style="width: 800px;">
			<tr><th>Fragebogen</th><td>'.$fb['titel'].'</td></tr>
			<tr><th>Kurs</th><td>'.$kurs['title'].'</strong> ('.$kurs['nummer'].', '.date( "d.m.Y", strtotime( $kurs['startdatum'] ) ).' bis '.date( "d.m.Y", strtotime( $kurs['enddatum'] ) ).')</td></tr>
			<tr><th>bereits erfasste Eingaben</th><td>'.$anz['anz'].'</td></tr>
			<tr><th>Datum</th><td><input class="date_picker" type="text" name="datum" value="'.date( "d.m.Y", strtotime( $kurs['enddatum'] ) ).'"></td></tr>
		</table>
		<br />
	';

	$db->query( "
		SELECT DISTINCT( kurz ), COUNT( fragebogen_id ) AS line, position, trainer_frage
			FROM WHY_FRAGEN
			WHERE fragebogen_id='".$fragebogen_id."'
			GROUP BY kurz
			ORDER BY position ASC", "create_fb_1" );
	while( $db->isNext( "create_fb_1" ) ) {
		$r1 = $db->getNext( "create_fb_1" );

		// Trainer laden
		$trainer = array();
		if( $r1['trainer_frage'] == 1 ) {
			$db->query( "
				SELECT k.trainer_id, t.vorname, t.nachname
				FROM BAS_TRAINER_KURSE AS k
				LEFT JOIN BAS_TRAINER AS t ON (t.trainer_id=k.trainer_id)
				WHERE k.kurs_id='".$kurs_id."'", "create_fb_2" );
			while( $db->isNext( "create_fb_2" ) ) {
				$r2 = $db->getNext( "create_fb_2" );

				$trainer[] = array( "name" => $r2['vorname'].' '.$r2['nachname'], "id" => $r2['trainer_id'] );
			} // while
		} else
			$trainer[] = array( "name" => "temp", "id" => 0 );

		foreach( $trainer as $k => $v ) {
			echo '
				<table class="list_top shadow" style="width: 800px;">
					<tr><th>';
			// Titel ersetzen
			if( $r1['trainer_frage'] == 1 )
				echo $r1['kurz'].' '.$v['name'];
			else
				echo $r1['kurz'];
			echo '</th><th width="50">Sehr</th><th width="50">Ja</th><th width="50">Wenig</th><th width="50">Nicht</th><th width="50">Antwort nicht möglich</th></tr>';

			// Schleife über Fragen in dieser Gruppe
			$i = 0;
			$db->query( "
				SELECT frage_id, kurz, frage
				FROM WHY_FRAGEN
				WHERE kurz='".$r1['kurz']."'
				ORDER BY position ASC", "create_fb_2" );
			while( $db->isNext( "create_fb_2" ) ) {
				$r2 = $db->getNext( "create_fb_2" );

				$i++;

				// Titel ersetzen
				echo '<tr><td>'.$r2['frage'].'</td>';

				// Radioboxen für Antworten
				$db->query( "SELECT antwort_id FROM WHY_ANTWORTEN WHERE frage_id='".$r2['frage_id']."' ORDER BY position", 3 );
				while( $db->isNext( 3 ) ) {
					$r3 = $db->getNext( 3 );

					echo '<td style="text-align: center;" class="eingabe"><input type="radio" name="antwort['.$r2['frage_id'].']['.$v['id'].']" value="'.$r3['antwort_id'].'"></td>';
				} // for

				echo '</tr>';
			} // while
		} // foreach
		echo '</table>';
	} // while



	echo '
		<br />
			<a onClick="$(this).closest(\'form\').submit()" class="link_click_button">'.$f->get_button( 'speichern' ).'</a>
';
	echo '</form>';
} // if

echo '</div>';

require_once( CLASS_DIR."templates/footer.php" );
?>