<?php
set_time_limit( 300 );

require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );
require_once( CLASS_DIR.'update_functions.php' );

unset( $_SESSION['list_id'] );

if( isset( $_GET['save_version'] ) ) {
	$f->save_current_version( $_POST['version'] );

	header( "location: core_update.php" );
	exit;
} // if

require_once( CLASS_DIR."templates/header.php" );

$update = update::getInstance();

// Init
$f->load_projects( $projects, $old_projects );

// Content
echo '<div id="content_scroll">';

$dir = BASE_DIR.'updates/';
if( !file_exists( $dir ) ) mkdir( $dir );

$location = file_get_contents( dirname( __FILE__ ).'/../location.txt' );

$server_version = "";
$server_version = @file_get_contents( "http://iit-tools.sordje.at/updates/".$location."/last_version.txt" );

$status_download = "";
if( isset( $_GET['download'] ) ) {
	$remote_file = "http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip";
	$local_file = 	BASE_DIR.'updates/'.$server_version.".zip";

	$level = 0;
	if( ($level == 0) && $f->remote_file_exists( $remote_file."_enc_v2" ) ) $level = 2;
	if( ($level == 0) && $f->remote_file_exists( $remote_file."_enc" ) ) 		$level = 1;
	if( ($level == 0) && $f->remote_file_exists( $remote_file ) ) 					$level = 0;

	switch( $level ) {
		case 0:
				file_put_contents( $local_file, @file_get_contents( $remote_file ) );
				$status_download = "Datei wurde geladen: ".$local_file."<br>";
			break;
		case 1:
				file_put_contents( $local_file."_enc", @file_get_contents( $remote_file."_enc" ) );
				$status_download = "Datei wurde geladen: ".$local_file."<br>";
				$f->decrypt_file( $local_file, $location );
			break;
		case 2:
				file_put_contents( $local_file."_enc_v2", @file_get_contents( $remote_file."_enc_v2" ) );
				$status_download = "Datei wurde geladen: ".$local_file."<br>";
				$f->decrypt_file_v2( $local_file, $location );
			break;
		} // switch
		$status_download .= "Datei wurde decodiert: Encryption Level ".$level;

/*
	$update_file = @file_get_contents( "http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip_enc" );

	if( $update_file === false ) {
		// Unverschlüsselt
		$update_file = @file_get_contents( "http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip" );
		file_put_contents( BASE_DIR.'updates/'.$server_version.".zip", $update_file );
		$status_download = "Datei wurde geladen: "."http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip<br>";
	} else {
		// Verschlüsselt
		file_put_contents( BASE_DIR.'updates/'.$server_version.".zip_enc", $update_file );
		$status_download = "Datei wurde geladen: "."http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip_enc<br>";

		$f->decrypt_file( BASE_DIR.'updates/'.$server_version.".zip" );
		$status_download .= "Datei wurde decodiert";
	} // else
*/
} // if

if( isset( $_GET['del'] ) ) {
	unlink( BASE_DIR.'updates/'.$_GET['del'] );
} // if

$status2 = "";
if( isset( $_GET['install'] ) ) {
	$version = $update->get_version_no_from_version( $_GET['install'] );

	$zip = new ZipArchive;

	if( ($errorcode=$zip->open( BASE_DIR.'updates/'.$_GET['install'] )) === true ) {
		$status2 .= 'Dateien auf dem neuesten Stand:<br /><table class="list_left shadow" style="width: 700px;">';
		for( $i=0; $i<$zip->numFiles; $i++ ) {
			$status2 .= '<tr><td>'.$zip->getNameIndex( $i ).'</td></tr>';
		} // for
		$status2 .= '</table><br />';

		$zip->extractTo( BASE_DIR );
		$zip->close();

		// Prüfe vorherige Updates
		$update->check_updates_to_process( $version );

		// Rückschreiben
		$userinfo = $f->load_user( $_SESSION['c_user_id'], 'user_id, email' );
		$temp = @file_get_contents( "http://iit-tools.sordje.at/updates/_php/update_installed.php?".
			"user_id=".$userinfo['user_id'].'&'.
			"email=".$userinfo['email'].'&'.
			"version=".$version.'&'.
			"instance=".$location
			);
		?>
		<script>
		location.href = "core_update.php";
		</script>
		<?
		exit;
	} else {
		echo '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> Error: '.$f->zip_message( $errorcode ).'</div>';
	} // else
} // if

?>
<h2>Version</h2>
<?php
if( $status_download != "" )
	echo '<div class="alert alert-success" role="alert">'.$status_download.'</div>';
?>
<table class="list_left shadow" style="width: 700px;">
	<tr><th>aktuelle Version</th><td colspan="2"><? echo VERSION; ?></td></tr>
	<?
	if( $server_version != "" ) {
	?>
	<tr><th>letzte Version</th><td><? echo str_replace( "_", " ", $server_version ); ?></td>
		<td>
			<a href="?download=1" class="link_click_button"><?php echo $f->get_button( 'Download' ); ?></a>
			<!--  <a href="http://iit-tools.sordje.at/updates/<? echo $location; ?>/" target="_blank" class="link_click_button"><?php echo $f->get_button( 'URL öffnen' ); ?></a>  -->
		</td>
	</tr>
	<?
	} else {
		echo '<tr><th>kein Update verfügbar</th><td colspan="2">!!!</td></tr>';
	} // else
	?>
	<tr><td colspan="3">Die Update Dateien müssen im Verzeichnis <br />"<strong><? echo BASE_DIR.'updates/'; ?>"</strong><br />gespeichert sein.</td></tr>
</table>
<br />

<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>Update einspielen</h2>
	<? echo $status2; ?>
	<table class="list shadow table table-sm" >
		<tr><th>Version</th><th class="right">Größe</th><th class="right">Dateien</th><th class="list_th_columns_actions">Aktion</th></tr>
	<?
	$file = array();
	$handle = opendir( BASE_DIR.'updates/' );
	while( $datei = readdir( $handle ) ) {
		if( (pathinfo( $datei, PATHINFO_EXTENSION) == "zip") && ($datei != ".") && ($datei != "..") && ($datei != "last_version.txt") ) {
			$v = $datei;
			$v = str_replace( "_", " ", $v );
			$v = str_replace( ".zip", "", $v );

			$zip = new ZipArchive;
			$zip->open( BASE_DIR.'updates/'.$datei );
			$files_count = $zip->numFiles;

			$file[] = array( "title" => $v, "file" => $datei, "size" => filesize( BASE_DIR.'updates/'.$datei ), "files" => $files_count );
		} // if
	} // while

	$i = 0;
	rsort( $file );
	foreach( $file as $k => $v ) {
		?>
		<tr>
			<td><?php echo $v['title']; ?></td>
			<td class="right"><?php echo number_format( $v['size'], 0, ',', '.' ); ?></td>
			<td class="right"><?php echo number_format( $v['files'], 0, ',', '.' ); ?></td>
			<td class="list_columns_actions">
				<a href="?del=<?php echo $v['file']; ?>" onClick='if( !confirm("Update wirklich löschen?") ) return( false );' class="link_click_button"><?php echo $f->get_button( 'löschen' ); ?></a>
				<a href="?install=<?php echo $v['file']; ?>" onClick='if( !confirm("Update wirklich installieren?") ) return( false );' class="link_click_button"><?php echo $f->get_button( 'installieren' ); ?></a>
			</td>
		</tr>
		<?php

		$i++;
		if( $i == 1 ) echo '<tr><td colspan="4"><hr><hr></td></tr>';
		if( $i >= 11 ) break;
	} // foreach
	?>
	</table>
</form>
</div>
<?
require_once( CLASS_DIR."templates/footer.php" );
?>
