<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );

$statistics_id = isset( $_GET['statistic_id'] )?$_GET['statistic_id']:(isset( $_POST['statistic_id'] )?$_POST['statistic_id']:0);
$herkunft_id = isset( $_GET['herkunft_id'] )?$_GET['herkunft_id']:(isset( $_POST['herkunft_id'] )?$_POST['herkunft_id']:0);
$dateVon = isset( $_GET['dateVon'] )?$_GET['dateVon']:(isset( $_POST['dateVon'] )?$_POST['dateVon']:"");
$dateBis = isset( $_GET['dateBis'] )?$_GET['dateBis']:(isset( $_POST['dateBis'] )?$_POST['dateBis']:"");
$dateVon2 = isset( $_GET['dateVon2'] )?$_GET['dateVon2']:(isset( $_POST['dateVon2'] )?$_POST['dateVon2']:"");
$dateBis2 = isset( $_GET['dateBis2'] )?$_GET['dateBis2']:(isset( $_POST['dateBis2'] )?$_POST['dateBis2']:"");

if( $dateVon == '' ) $dateVon = '01.01.2013';
if( $dateBis == '' ) $dateBis = '31.12.2020';
if( $dateVon2 == '' ) $dateVon2 = '01.01.2013';
if( $dateBis2 == '' ) $dateBis2 = '31.12.2020';

$statistics = array(
	"",
	"Herkunft der Teilnehmer",
	"Kurse Teilnehmer",
);

$list1 = '';
foreach( $statistics as $k => $v ) {
	$sel = '';
	if( $k == $statistics_id ) $sel = ' selected';

	$list1 .= '<option value="'.$k.'"'.$sel.'>'.$v.'</option>';
} // foreach

$list_herkunft = '';
$db->query( "SELECT id, title FROM BAS_HERKUNFT" );
while( $db->isNext() ) {
	$r = $db->getNext();

	$sel = '';
	if( $r['id'] == $herkunft_id ) $sel = ' selected';

	$list_herkunft .= '<option value="'.$r['id'].'"'.$sel.'>'.$r['title'].'</option>';
} // while

?>
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>">
	<h2>Statistik</h2>
	<table class="list_left shadow" style="width: 500px;">
		<tr>
			<th>Auswertung</th>
			<td>
				<select id="statistic_id" name="statistic_id" data-placeholder="Auswertung wählen..." class="chosen-select" style="width: 300px" onChange="show_print_selection_statistics();">
					<? echo $list1; ?>
				</select>
			</td>
		</tr>
		<tr id="stat_herkunft">
			<td colspan="2">
				<table>
					<tr><th>Herkunft</th><td><select name="herkunft_id" data-placeholder="Auswertung wählen..." class="chosen-select" ><option value="0"></option><? echo $list_herkunft; ?></select></td></tr>
					<tr><th>Kursstartdatum zwischen</th><td><input class="date_picker" type="text" name="dateVon" value="<? echo $dateVon; ?>"> <input class="date_picker" type="text" name="dateBis" value="<? echo $dateBis; ?>"></td></tr>
				</table>
		</tr>
		<tr id="stat_teilnehmer_kurse">
			<td colspan="2">
				<table>
					<tr><th>Kursstartdatum zwischen</th><td><input class="date_picker" type="text" name="dateVon2" value="<? echo $dateVon2; ?>"> <input class="date_picker" type="text" name="dateBis2" value="<? echo $dateBis2; ?>"></td></tr>
				</table>
		</tr>
		<tr><td colspan="2" class="right">
				<a href="#" onClick="$(this).closest('form').submit()" style="float: right;"><?php  echo $f->get_button( 'Druck erstellen' ); ?></a>
		</tr>
	</table>
</form>
<?
$output = '';

switch( $statistics_id ) {
	case 1:
			if( $herkunft_id > 0 ) {
				$db->query( "SELECT title FROM BAS_HERKUNFT WHERE id='".$herkunft_id."'" );
				$r = $db->getNext();

				$output .= '
		<strong>Herkunft:</strong> '.$r['title'].'<br>
		<strong>Kursstartdatum zwischen:</strong> '.$dateVon.' bis '.$dateBis.'<br>
		<br>
		<table class="list shadow table table-striped">
			<thead>
			<tr>
				<th>EDV NUMMER</th>
				<th>VORNAME</th>
				<th>NACHNAME</th>
				<th>SVNR</th>
				<th>TELEFONNUMMER</th>
				<th>STRASSE</th>
				<th>PLZ</th>
				<th>ORT</th>
				<th>GEBUCHTE KURSE</th>
			</tr></thead>';
				$db->query( "
		SELECT
			bt.teilnehmer_id, bt.vorname, bt.nachname, bt.svn, bt.telefon, bt.adresse, bt.plz, bt.ort,
			bh.title
		FROM BAS_TEILNEHMER AS bt
		LEFT JOIN BAS_HERKUNFT AS bh ON (bh.id=bt.herkunft_id)

		WHERE bt.herkunft_id='".$herkunft_id."'" );
				while( $db->isNext() ) {
					$r = $db->getNext();

					$kurse = '';
					if( $db->query( "
			SELECT bk.nummer
			FROM BAS_KURSTEILNEHMER AS bt
			LEFT JOIN BAS_KURSE AS bk ON (bk.kurs_id=bt.kurs_id)
			WHERE
				bt.teilnehmer_id='".$r['teilnehmer_id']."' AND
				bk.startdatum>='".$f->SQLDate( $dateVon )."' AND
				bk.startdatum<='".$f->SQLDate( $dateBis )."'", 2 ) ) {
								while( $db->isNext( 2 ) ) {
									$r2 = $db->getNext( 2 );

									$kurse .= $r2['nummer'].', ';
								} // while
								$kurse = substr( $kurse, 0, -2 );
					} // if

					if( $kurse != '' ) {
						$output .= '
				<tr>
					<td class="right">'.$r['teilnehmer_id'].'</td>
					<td>'.$r['vorname'].'</td>
					<td>'.$r['nachname'].'</td>
					<td>'.$r['svn'].'</td>
					<td>'.$r['telefon'].'</td>
					<td>'.$r['adresse'].'</td>
					<td>'.$r['plz'].'</td>
					<td>'.$r['ort'].'</td>
					<td>'.$kurse.'</td>
				</tr>';
					} // if
				} // while

				$output .= '</table>';
			} // if
		break;
	case 2:
			$output .= '
				<strong>Kursstartdatum zwischen:</strong> '.$dateVon2.' bis '.$dateBis2.'<br>
				<br>
				<table class="list bg-white">
					<thead>
					<tr>
						<th>KURSTYP</th>
						<th>DATUM</th>
						<th>KURSNUMMER</th>
						<th>ANZAHL TEILNEHMER</th>
					</tr></thead>';

			$anz_teilnehmer = 0;
			$ges_anz_teilnehmer = 0;
			$anz_kurse = 0;
			$ges_anz_kurse = 0;
			$oldValue = "";

			$db->query( "
				SELECT kt.title, k.nummer, COUNT( t.kurs_id ) AS anz, k.startdatum, k.enddatum
				FROM BAS_KURSE AS k
				LEFT JOIN BAS_KURSTEILNEHMER AS t ON (t.kurs_id=k.kurs_id)
				LEFT JOIN BAS_KURSTYP AS kt ON (kt.id=k.kurstyp_id)
				WHERE
					k.startdatum>='".$f->SQLDate( $dateVon2 )."' AND
					k.startdatum<='".$f->SQLDate( $dateBis2 )."'
				GROUP BY k.kurs_id
				ORDER BY kt.title" );
			while( $db->isNext() ) {
				$r = $db->getNext();

				if( ($r['title'] != $oldValue) && ($oldValue != "")  ) {
					$output .= '
						<tr>
							<th colspan="2">'.$oldValue.'</th>
							<th class="right">Kurse: '.$anz_kurse.'</th>
							<th class="right">Gesamt TN: '.$anz_teilnehmer.'</th>
						</tr>
						<tr>
							<td colspan="4">&nbsp;</td>
						</tr>
						<thead><tr>
							<th>KURSTYP</th>
							<th>&nbsp;</th>
							<th>KURSNUMMER</th>
							<th>ANZAHL TEILNEHMER</th>
						</tr></thead>';
					$anz_kurse = 0;
					$anz_teilnehmer = 0;
				} // if

				$output .= '
					<tr>
						<td>'.$r['title'].'</td>
						<td>'.date( "d.m.Y", strtotime( $r['startdatum'] ) ).' bis '.date( "d.m.Y", strtotime( $r['enddatum'] ) ).'</td>
						<td>'.$r['nummer'].'</td>
						<td class="right">'.$r['anz'].'</td>
					</tr>';

				$oldValue = $r['title'];
				$anz_teilnehmer += $r['anz'];
				$ges_anz_teilnehmer += $r['anz'];
				$anz_kurse++;
				$ges_anz_kurse++;
			} // while

			$output .= '
				<tr>
					<th colspan="2">'.$oldValue.'</th>
					<th class="right">Kurse: '.$anz_kurse.'</th>
					<th class="right">Gesamt TN: '.$anz_teilnehmer.'</th>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<th colspan="2">Alle Kurse Summe:</th>
					<th class="right">Kurse: '.$ges_anz_kurse.'</th>
					<th class="right">Gesamt TN: '.$ges_anz_teilnehmer.'</th>
				</tr>';

			$output .= '</table>';
		break;
} // switch

if( $output != '' ) {
	?>
	<br />
	<h2>Ausgabe</h2>
	<div id="content_output" style="overflow:auto; height: 50vh;">
	<? echo $output ?>
	</div>

	<i>Beschreibung: Nach dem Klick auf "Drucken" öffnet sich ein Fenster, dort mit rechter Maustaste auf Drucken... oder Strg+P tippen.</i><br/>
	<a class="link_click_button" onClick="print_window('content_output')">
		<?php  echo $f->get_button( 'Drucken' ); ?></a>
	</a>
	<?
} // if

require_once( CLASS_DIR."templates/footer.php" );
?>