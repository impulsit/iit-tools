<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

$type = isset( $_GET['type'] )?$_GET['type']:(isset( $_POST['type'] )?$_POST['type']:'');
$teilnehmer_id = isset( $_GET['teilnehmer_id'] )?$_GET['teilnehmer_id']:(isset( $_POST['teilnehmer_id'] )?$_POST['teilnehmer_id']:0);
$kurs_id = isset( $_GET['kurs_id'] )?$_GET['kurs_id']:(isset( $_POST['kurs_id'] )?$_POST['kurs_id']:0);
$trainer_id = isset( $_GET['trainer_id'] )?$_GET['trainer_id']:(isset( $_POST['trainer_id'] )?$_POST['trainer_id']:0);
$fragebogen_id = isset( $_GET['fragebogen_id'] )?$_GET['fragebogen_id']:(isset( $_POST['fragebogen_id'] )?$_POST['fragebogen_id']:0);

$bericht_id = 0;
foreach( array( "teilnehmer", "kurs", "teilnehmer_kurs", "trainer", "trainer_kurs", "fragebogen_kurs" ) as $k => $v ) {
	if( $type == $v )
		$bericht_id = isset( $_GET['bericht_id_'.$v] )?$_GET['bericht_id_'.$v]:(isset( $_POST['bericht_id_'.$v] )?$_POST['bericht_id_'.$v]:0);
} // foreach
if( $bericht_id == "" ) $bericht_id = 0;

// Drucken
if( isset( $_POST['create'] ) ) {
	$bMulti = false;

	if( ($type == "teilnehmer_kurs") && ($teilnehmer_id == "") ) {
		$bMulti = true;
	} // if

	// Mehrere Teilnehmer pro Kurs Bericht
	if( $bMulti ) {
		$i = 0;
		$j = 0;

		$db->query( "
			SELECT teilnehmer_id, status_id
			FROM BAS_KURSTEILNEHMER
			WHERE kurs_id='".$kurs_id."'", "druck" );
		while( $db->isNext( "druck" ) ) {
			$r = $db->getNext( "druck" );

			$bCreate = true;
			// Zahlungsbestätigung, hat Teilnehmer wirklich bezahlt?
			if( ($bericht_id == $report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG") || ($bericht_id == $report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG_KOMPLETT")) ) ) {
				if( $r['status_id'] != 2 ) {
					$bCreate = false;
					$j++;
				} // if
			} // if

			if( $bCreate ) {
				$i++;

				$print->set_report( $type, $r['teilnehmer_id'], $kurs_id, $trainer_id, $fragebogen_id, $bericht_id );
				$print->generate_rtf();
				$print->save_file();
			} // if
		} // while
		$str = "Bericht ".$report->get_report_name( $print->bericht_id )." wurde ".$i." mal erstellt.";
		if( $j > 0 )
			$str .= ' '.$j.' Teilnehmer haben noch nicht bezahlt.';
		$mes->addInfo( $str );
	} // if

	// Mehrere Teilnehmer pro Erinnerungsbrief
	if( ($bericht_id == $report->get_report_id( "REP_ERINNERUNGSBRIEF" )) && ($teilnehmer_id == "") ) {
		$setup = $f->load_setup( "BAS_SETUP" );

		$bMulti = true;
		if( $bMulti ) {
			$i = 0;

			$db->query( "
				SELECT teilnehmer_id
				FROM BAS_TEILNEHMER
				WHERE
					(letzter_kurs_c95_d95+INTERVAL ".($setup['weiterbildung_monate']-3)." MONTH<='".date( "Y-m-d", time() )."')	AND
					(letzter_kurs_c95_d95!='0000-00-00') AND
					(letzter_kurs_c95_d95!='1970-01-01') AND
					(erinnerung_gedruckt='0')" );
			while( $db->isNext() ) {
				$r = $db->getNext();

				$i++;
				$print->set_report( $type, $r['teilnehmer_id'], $kurs_id, $trainer_id, $fragebogen_id, $bericht_id );
				$print->generate_rtf();
				$print->save_file();
			} // while
			$str = "Bericht ".$report->get_report_name( $bericht_id )." wurde ".$i." mal erstellt.";
			$mes->addInfo( $str );
		} // if
	} // if

	// Mehrere Teilnehmer Erinnerungsbrief ADR
	if( ($bericht_id == $report->get_report_id( "REP_ERINNERUNGSBRIEF_ADR" )) && ($teilnehmer_id == "") ) {
		$setup = $f->load_setup( "BAS_SETUP" );

		$bMulti = true;
		if( $bMulti ) {
			$i = 0;

			$db->query( "
				SELECT teilnehmer_id
				FROM BAS_TEILNEHMER
				WHERE
					(letzter_adr_kurs+INTERVAL ".($setup['adr_weiterbildung_monate']-3)." MONTH<='".date( "Y-m-d", time() )."')	AND
					(letzter_adr_kurs!='0000-00-00') AND
					(letzter_adr_kurs!='1970-01-01') AND
					(adr_erinnerung_gedruckt='0')" );
			while( $db->isNext() ) {
				$r = $db->getNext();

				$i++;
				$print->set_report( $type, $r['teilnehmer_id'], $kurs_id, $trainer_id, $fragebogen_id, $bericht_id );
				$print->generate_rtf();
				$print->save_file();
			} // while
			$str = "Bericht ".$report->get_report_name( $bericht_id )." wurde ".$i." mal erstellt.";
			$mes->addInfo( $str );
		} // if
	} // if

	// Mehrere Kurse pro Fragebogen
	if( ($bericht_id == $report->get_report_id( "REP_FRAGEBOGEN" )) && ($kurs_id == "") ) {
		$i = 0;
		$bMulti = true;

		/*
		$db->query( "
			SELECT kurs_id
			FROM BAS_KURSE" );
		while( $db->isNext() ) {
			$r = $db->getNext();

			$i++;
			$print->set_report( $type, $teilnehmer_id, $r['kurs_id'], $trainer_id, $fragebogen_id, $bericht_id );
			$print->generate_rtf();
			$print->save_file();
		} // while

		$str = "Bericht ".$report->get_report_name( $bericht_id )." wurde ".$i." mal erstellt.";
   $mes->addInfo( $str );
		*/
		$mes->addError( "Bitte einen Kurs wählen." );
	} // if

	// Mehrere Trainer pro Kurs (Werkvertrag, Honorarnote)
	if( (($bericht_id == $report->get_report_id( "REP_HONORAR" )) ||
			 ($bericht_id == $report->get_report_id( "REP_WERKVERTRAG" ))) &&
			($trainer_id == "") ) {
		$bMulti = true;

		$i = 0;

		$db->query( "
			SELECT DISTINCT( trainer_id )
			FROM BAS_TRAINER_KURSE
			WHERE
				kurs_id='".$kurs_id."'" );
		while( $db->isNext() ) {
			$r = $db->getNext();

			$i++;
			$print->set_report( $type, $teilnehmer_id, $kurs_id, $r['trainer_id'], $fragebogen_id, $bericht_id );
			$print->generate_rtf();
			$print->save_file();
		} // while
		$str = "Bericht ".$report->get_report_name( $bericht_id )." wurde ".$i." mal erstellt.";
		$mes->addInfo( $str );
	} // if

	if( !$bMulti ) {
		if( $print->set_report( $type, $teilnehmer_id, $kurs_id, $trainer_id, $fragebogen_id, $bericht_id ) ) {
			$print->generate_rtf();
			$print->save_file();

			$mes->addInfo( "Bericht ".$report->get_report_name( $print->bericht_id )." wurde erstellt." );
		} else {
			if( $print->bericht_id == -1 )
				$mes->addError( "Es gibt kein Formular für diesen Kurs." );

			$mes->addError( "Vorlage nicht eingerichtet! Bericht: ".$print->bericht_id." - ".$report->get_report_name( $print->bericht_id ) );
		} // else
	} // if
} // if

// Suchen
$_SEARCH_WHERE = "";
if( isset( $_POST['search'] ) ) {
	$bericht_id = isset( $_GET['bericht_id'] )?$_GET['bericht_id']:(isset( $_POST['bericht_id'] )?$_POST['bericht_id']:0);

	if( $type != "" ) $_SEARCH_WHERE .= " AND l.type='".$type."'";
	if( $teilnehmer_id != 0 ) $_SEARCH_WHERE .= " AND l.teilnehmer_id='".$teilnehmer_id."'";
	if( $kurs_id != 0 ) $_SEARCH_WHERE .= " AND l.kurs_id='".$kurs_id."'";
	if( $trainer_id != 0 ) $_SEARCH_WHERE .= " AND l.trainer_id='".$trainer_id."'";
	if( $fragebogen_id != 0 ) $_SEARCH_WHERE .= " AND l.fragebogen_id='".$fragebogen_id."'";
	if( $bericht_id != 0 ) $_SEARCH_WHERE .= " AND l.bericht_id='".$bericht_id."'";
} // if

if( isset( $_POST['reset'] ) ) {
	$_SEARCH_WHERE = "";
} // if

if( isset( $_GET['print'] ) ) {
	$print->set_report( $type, $teilnehmer_id, $kurs_id, $trainer_id, $fragebogen_id, $_GET['bericht_id'] );
	$print->load_file();
	$print->print_rtf();
} // if

if( isset( $_GET['open'] ) ) {
	$print->load_file( $_GET['id'] );
	$print->open_rtf();
	exit;
} // if

if( isset( $_GET['delete'] ) ) {
	$db->update( "BAS_LOG_FILES", array( "anzahl_gedruckt" => 1 ), "id='".$_GET['id']."'" );
	$db->commit();
} // if

if( isset( $_POST['batch_print'] ) && isset( $_POST['id'] ) ) {
	$in = "";
	foreach( $_POST['id'] as $k => $v ) {
		if( $in != "" ) $in .= ',';
		$in .= $k;
	} // forach

	$output = $print->generate_batch_print_file( $in, $_POST['print_count'] );

	header('Content-Type: application/octet-stream');
	header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
	header('Pragma: public');
	header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
	header('Content-Disposition: inline; filename="drucken.cmd"');
	if( ob_get_length() > 0 ) ob_clean();
	flush();

	echo $output;
	exit;
} // if

// Immer Projekt auf Basilica setzen -> linkes Menü
$_SESSION['project_id'] = 2;

require_once( CLASS_DIR."templates/header.php" );

echo '<div id="content_scroll">';

//foreach( array( "" ) as $k => $v ) {
foreach( array( "", "search_" ) as $k => $v ) {
	echo '
		<form method="post" action="bas_drucken.php">
		<h2>'.($v==""?'Druck erstellen':'Suchen').'</h2>
		<table class="list shadow" style="margin:0;">
			<tr>
				<th style="width: 200px;">Drucktyp</th>
				<td style="background: white !important;">
					<select id="'.$v.'type" name="type" data-placeholder="Type wählen..." class="chosen-select" style="width: 300px" onChange="show_print_selection();">
						<option value=""></option>
						<option value="teilnehmer"'.(($type=='teilnehmer')?' selected':'').'>Teilnehmer</option>
						<option value="kurs"'.(($type=='kurs')?' selected':'').'>Kurs</option>
						<option value="teilnehmer_kurs"'.(($type=='teilnehmer_kurs')?' selected':'').'>Teilnehmer + Kurs</option>
						<option value="trainer"'.(($type=='trainer')?' selected':'').'>Trainer</option>
						<option value="trainer_kurs"'.(($type=='trainer_kurs')?' selected':'').'>Trainer + Kurs</option>
						<option value="fragebogen_kurs"'.(($type=='fragebogen_kurs')?' selected':'').'>Fragebogen + Kurs</option>
					</select>
				</td>
			</tr>
		</table>';

	// Teilnehmer
	$list1 = '';
	if( $kurs_id == 0 )
		$db->query( "SELECT teilnehmer_id, vorname, nachname FROM BAS_TEILNEHMER ORDER BY nachname, vorname" );
	else
		$db->query( "
			SELECT k.teilnehmer_id, bt.vorname, bt.nachname
			FROM BAS_KURSTEILNEHMER AS k
			LEFT JOIN BAS_TEILNEHMER AS bt ON (bt.teilnehmer_id=k.teilnehmer_id)
			WHERE kurs_id='".$kurs_id."'
			ORDER BY nachname, vorname" );
	while( $db->isNext() ) {
		$r = $db->getNext();

		$sel = '';
		if( $r['teilnehmer_id'] == $teilnehmer_id ) $sel = ' selected';

		$list1 .= '<option value="'.$r['teilnehmer_id'].'"'.$sel.'>'.$r['nachname'].' '.$r['vorname'].'</option>';
	} // while
	echo '
		<table id="'.$v.'teilnehmer" class="list shadow" style="margin:0;">
			<tr>
				<th style="width: 200px;">Teilnehmer</th>
				<td style="background: white !important;">
					<select name="teilnehmer_id" data-placeholder="Teilnehmer wählen..." class="chosen-select" style="width: 300px">
						<option value=""></option>
						'.$list1.'
					</select>
				</td>
			</tr>
		</table>';

	// Trainer
	$list1 = '';
	$db->query( "SELECT trainer_id, vorname, nachname FROM BAS_TRAINER ORDER BY nachname, vorname" );
	while( $db->isNext() ) {
		$r = $db->getNext();

		$sel = '';
		if( $r['trainer_id'] == $trainer_id ) $sel = ' selected';

		$list1 .= '<option value="'.$r['trainer_id'].'"'.$sel.'>'.$r['nachname'].' '.$r['vorname'].'</option>';
	} // while
	echo '
		<table id="'.$v.'trainer" class="list shadow" style="margin:0;">
			<tr>
				<th style="width: 200px;">Trainer</th>
				<td style="background: white !important;">
					<select name="trainer_id" data-placeholder="Trainer wählen..." class="chosen-select" style="width: 300px">
						<option value=""></option>
						'.$list1.'
					</select>
				</td>
			</tr>
		</table>';

	// Fragebogen
	$list1 = '';
	$db->query( "SELECT fragebogen_id, titel FROM WHY_FRAGEBOGEN WHERE link_kurs='1' ORDER BY titel" );
	while( $db->isNext() ) {
		$r = $db->getNext();

		$sel = '';
		if( $r['fragebogen_id'] == $fragebogen_id ) $sel = ' selected';

		$list1 .= '<option value="'.$r['fragebogen_id'].'"'.$sel.'>'.$r['titel'].'</option>';
	} // while
	echo '
		<table id="'.$v.'fragebogen" class="list shadow" style="margin:0;">
			<tr>
				<th style="width: 200px;">Fragebogen</th>
				<td style="background: white !important;">
					<select name="fragebogen_id" data-placeholder="Fragebogen wählen..." class="chosen-select" style="width: 300px">
						<option value=""></option>
						'.$list1.'
					</select>
				</td>
			</tr>
		</table>';

	// Kurse
	$list1 = '';
	$db->query( "
		SELECT k.kurs_id, k.nummer, k.startdatum, k.enddatum
		FROM BAS_KURSE AS k
		ORDER BY k.startdatum DESC, k.nummer ASC" );
	while( $db->isNext() ) {
		$r = $db->getNext();

		$sel = '';
		if( $r['kurs_id'] == $kurs_id ) $sel = ' selected';

		$list1 .= '<option value="'.$r['kurs_id'].'"'.$sel.'>'.$f->get_kurs_titel( $r['nummer'], $r['startdatum'], $r['enddatum'] ).'</option>';
	} // while
	echo '
		<table id="'.$v.'kurs" class="list shadow" style="margin:0;">
			<tr>
				<th style="width: 200px;">Kurs</th>
				<td style="background: white !important;">
					<select name="kurs_id" data-placeholder="Kurs wählen..." class="chosen-select" style="width: 300px">
						<option value=""></option>
						'.$list1.'
					</select>
				</td>
			</tr>
		</table>';

	// Berichte
	if( $v == "" ) {
		foreach( array( "teilnehmer", "kurs", "teilnehmer_kurs", "trainer", "trainer_kurs", "fragebogen_kurs" ) as $k3 => $v3 ) {
			switch( $v3 ) {
				case 'teilnehmer':
					$rep = array(
						$report->get_report_id( "REP_ERINNERUNGSBRIEF" ),
						$report->get_report_id( "REP_ERINNERUNGSBRIEF_ADR" ),
					);
					break;
				case 'kurs':
					$rep = array(
						$report->get_report_id( "REP_ANMELDEFORMULAR" ),
						$report->get_report_id( "REP_ANWESENHEITSLISTE" ),
						$report->get_report_id( "REP_ERLAGSCHEIN" ),
						$report->get_report_id( "REP_TEILNEHMERLISTE" ),
						$report->get_report_id( "REP_TEILNEHMERLISTE_2" ),
						$report->get_report_id( "REP_WAFF_TEILNEHMERLISTE" ),
						$report->get_report_id( "REP_ADR_TEILNEHMERLISTE" ),
						);
					break;
				case 'teilnehmer_kurs':
					$rep = array(
						$report->get_report_id( "REP_AMS_KOSTENVORANSCHLAG"),
						$report->get_report_id( "REP_AMS_KURSBESTAETIGUNG" ),
						$report->get_report_id( "REP_AMS_RECHNUNG" ),
						$report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG" ),
						$report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG_KOMPLETT" ),
						$report->get_report_id( "REP_KURSBESTAETIGUNG" ),
						$report->get_report_id( "REP_AMS_TEILNAHMEBESTAETIGUNG" ),
						$report->get_report_id( "REP_WAFF_DIREKT" ),
						$report->get_report_id( "REP_WAFF_LAP_RECHNUNG" ),
						$report->get_report_id( "REP_WAFF_TEILNEHMER" ),
						$report->get_report_id( "REP_ERLAGSCHEIN_TEILNEHMER" ),
					);
					break;
				case 'trainer':
					$rep = array( $report->get_report_id( "REP_AUSWEIS" ) );
					break;
				case 'trainer_kurs':
					$rep = array(
						$report->get_report_id( "REP_WERKVERTRAG" ),
						$report->get_report_id( "REP_HONORAR" ),
						$report->get_report_id( "REP_PROT_KRAN_STAPLER" ),
					);
					break;
				case 'fragebogen_kurs':
					$rep = array( $report->get_report_id( "REP_FRAGEBOGEN" ) );
					break;
			} // switch

			$list1 = '';
			foreach( $rep as $k2 => $v2 ) {
				$sel = '';
				if( $v2 == $bericht_id ) $sel = ' selected';

				$list1 .= '<option value="'.$v2.'"'.$sel.'>'.$report->get_report_name( $v2 ).'</option>';
			} // foreach

			echo '
				<table id="bericht_'.$v3.'" class="list shadow" style="margin:0;">
					<tr>
						<th style="width: 200px;">Bericht</th>
						<td style="background: white !important;">
							<select name="bericht_id_'.$v3.'" data-placeholder="Bericht wählen..." class="chosen-select" style="width: 300px">
								<option value=""></option>
								'.$list1.'
							</select>
						</td>
					</tr>
				</table>';
		} // foreach
	} else {
		// Suche
		$rep = array(
			$report->get_report_id( "REP_ERINNERUNGSBRIEF" ), $report->get_report_id( "REP_ERINNERUNGSBRIEF_ADR" ),
			$report->get_report_id( "REP_ANMELDEFORMULAR" ), $report->get_report_id( "REP_ANWESENHEITSLISTE" ), $report->get_report_id( "REP_ERLAGSCHEIN" ), $report->get_report_id( "REP_TEILNEHMERLISTE" ), $report->get_report_id( "REP_TEILNEHMERLISTE_2" ), $report->get_report_id( "REP_WAFF_TEILNEHMERLISTE" ), $report->get_report_id( "REP_ADR_TEILNEHMERLISTE" ),
			$report->get_report_id( "REP_AMS_KURSBESTAETIGUNG" ), $report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG" ), $report->get_report_id( "REP_ZAHLUNGSBESTAETIGUNG_KOMPLETT" ), $report->get_report_id( "REP_AMS_RECHNUNG" ), $report->get_report_id( "REP_AMS_KOSTENVORANSCHLAG"), $report->get_report_id( "REP_KURSBESTAETIGUNG" ), $report->get_report_id( "REP_AMS_TEILNAHMEBESTAETIGUNG" ), $report->get_report_id( "REP_WAFF_DIREKT" ), $report->get_report_id( "REP_WAFF_LAP_RECHNUNG" ), $report->get_report_id( "REP_WAFF_TEILNEHMER" ), $report->get_report_id( "REP_ERLAGSCHEIN_TEILNEHMER" ),
			$report->get_report_id( "REP_AUSWEIS" ),
			$report->get_report_id( "REP_WERKVERTRAG" ), $report->get_report_id( "REP_HONORAR" ), $report->get_report_id( "REP_PROT_KRAN_STAPLER" ),
			$report->get_report_id( "REP_FRAGEBOGEN" )
		);

		$list1 = '';
		foreach( $rep as $k2 => $v2 ) {
			$sel = '';
			if( $v2 == $bericht_id ) $sel = ' selected';

			$list1 .= '<option value="'.$v2.'"'.$sel.'>'.$report->get_report_name( $v2 ).'</option>';
		} // foreach

		echo '
			<table id="'.$v.'bericht" class="list shadow" style="margin:0;">
				<tr>
					<th style="width: 200px;">Bericht</th>
					<td style="background: white !important;">
						<select name="bericht_id" data-placeholder="Bericht wählen..." class="chosen-select" style="width: 300px">
							<option value=""></option>
							'.$list1.'
						</select>
					</td>
				</tr>
			</table>';
	} // else

	echo '
		<br />';
	if( $v == "" )
		echo '<a href="#" onClick="print_module($(this),2);" class="link_click_button">'.$f->get_button( 'Druck erstellen' ).'</a>';
	else {
		echo '<a href="#" onClick="print_module($(this),1);" class="link_click_button">'.$f->get_button( 'suchen' ).'</a>';
		echo '<a href="#" onClick="print_module($(this),3);" class="link_click_button">'.$f->get_button( 'alle anzeigen' ).'</a>';
	} // else

	echo '
		</form><div style="clear: left;"></div><br>';
} // foreach

$list1 = '';
if( isset( $_POST['search'] ) )
	$str2 = "1";
else
	$str2 = "l.anzahl_gedruckt='0'";

$db->query( "
	SELECT
		l.id, l.teilnehmer_id, l.kurs_id, l.bericht_id, l.erstellt, l.trainer_id, l.fragebogen_id, l.type, l.url,
		k.nummer, k.startdatum, k.enddatum, l.anzahl_gedruckt, l.rechnung_id
	FROM BAS_LOG_FILES AS l
	LEFT JOIN BAS_KURSE AS k ON (k.kurs_id=l.kurs_id)
	WHERE ".$str2." ".$_SEARCH_WHERE."
	ORDER BY erstellt DESC LIMIT 50" );
while( $db->isNext() ) {
	$r = $db->getNext();

	$kurs_titel = '';
	if( $r['kurs_id'] > 0 )
		$kurs_titel = $f->get_kurs_titel( $r['nummer'], $r['startdatum'], $r['enddatum'] );

	$str = '';
	if( ($r['bericht_id'] == $report->get_report_id( "REP_AMS_RECHNUNG" )) ||
			($r['bericht_id'] == $report->get_report_id( "REP_WAFF_LAP_RECHNUNG")) ||
			($r['bericht_id'] == $report->get_report_id( "REP_WAFF_DIREKT" )) ||
			($r['bericht_id'] == $report->get_report_id( "REP_WAFF_TEILNEHMER" )) ) {
		$arr = $print->get_rechnung_data( $r['rechnung_id'] );
		$str = ' ('.$arr['title'].')';
	} // if

	$list1 .= '
		<tr>
			<td class="center"><input type="checkbox" value="1" name="id['.$r['id'].']" data-printed="'.$r['anzahl_gedruckt'].'"></td>
			<td>'.$f->get_type_name( $r['type'] ).'</td>
			<td>'.$f->get_teilnehmer_name( $r['teilnehmer_id'] ).$f->get_trainer_name( $r['trainer_id'] ).$f->get_fragebogen_name( $r['fragebogen_id'] ).'</td>
			<td>'.$kurs_titel.'</td>
			<td>'.$report->get_report_name( $r['bericht_id'] ).$str.'</td>
			<td>'.$f->t( $r['erstellt'] ).'</td>
			<td class="right">'.$r['anzahl_gedruckt'].'</td>
			<td class="list_columns_actions">
				<a href="?open=1&id='.$r['id'].'" target="_blank" class="link_click_button">'.$f->get_button( 'öffnen' ).'</a>
				<a href="#" onClick=\'show_confirm( "Datensatz wirklich löschen?", function() { location.href="'.basename(__FILE__).'?delete=1&id='.$r['id'].'"; }, function() { return( false ); } );\' class="link_click_button">'.$f->get_button( 'löschen' ).'</a>
			</td>
		</tr>';
} // while

echo '
	<div style="clear: left;"></div><br>
	<form method="post" action="bas_drucken.php">
		<h2>Letzte Berichte</h2>

		<div>
			<span style="float: left;"><input type="text" name="print_count" value="1" class="spinner" style="width: 80px; height: 21px;"></span>
			<a onClick="print_module($(this),4);" target="_blank" class="link_click_button">'.$f->get_button( 'Drucken' ).'</a>
			<a onClick="check_rows();" class="link_click_button">'.$f->get_button( 'Auswahl (gedruckt = 0)' ).'</a>
			<a onClick="uncheck_rows();" class="link_click_button">'.$f->get_button( 'Auswahl aufheben' ).'</a>
		</div>
		<div style="clear: left;"></div><br>

		<table class="list list_over shadow table-striped tablesorting">
			<thead>
			<tr>
				<th class="sorter-false">Auswahl</th><th>Type</th><th>Teilnehmer / Trainer</th><th>Kurs</th><th>Bericht</th><th>erstellt</th><th>gedruckt</th><th class="list_th_columns_actions sorter-false">Aktion</th>
			</tr>
			</thead>
			<tbody>
			'.$list1.'
			</tbody>
		</table>
	</form>';

echo '</div>';

require_once( CLASS_DIR."templates/footer.php" );
?>