<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

if( isset( $_GET['indiv_list_id'] ) )
	$_SESSION['list_id'] = $_GET['indiv_list_id'];
  
// Setze "hoch"
$arr = $_LISTEN[$_SESSION['list_id']];
if( $arr['back_up_file'] != "" ) {
	$file = $arr['back_up_file'];
	if( $arr['back_up_list'] != 0 )
		$file .= '?indiv_list_id='.$arr['back_up_list'];

	$f->set_back_up_file = $file;
} // if

require_once( CLASS_DIR."templates/header.php" );

$list->set_list( $_SESSION['list_id'] );
$list->parse_commands();
$list->print_menu();
$list->print_list();

require_once( CLASS_DIR."templates/footer.php" );
?>