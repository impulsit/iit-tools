<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );

echo '
	<div class="menu_form shadow">
		<a id="export_excel" class="link_click_button">'.$f->get_button( 'Exportieren' ).'</a>
	</div>
	<div id="content_scroll">
		<table class="list shadow table table-striped table-sm" id="table_excel_export">
			<tr><th style="width: 200px;">'.$t->t( "Benutzer-Info" ).'</th><th>'.$t->t( "gesucht aber nicht gekauft" ).'</th></tr>
			<tr><td colspan="2"></td></tr>';

// Datumsschleife
$today = date( "Y-m-d" );
for( $i=0; $i<=30; $i++ ) {
	$bPrintDay = false;
	$current = date( "Y-m-d", strtotime( $today." -".$i." days" ) );
	$strDay = '<tr><td colspan="2"><strong>'.date( "d.m.Y", strtotime( $current ) ).'</strong></td></tr>';

	// Userschleife
	$db->query( "
		SELECT DISTINCT( l.user_id ), u.email, u.name, u.info
		FROM TEC_LOG_SEARCH AS l
		LEFT JOIN CORE_USER_INFO AS u ON (u.user_id=l.user_id)
		WHERE DATE( l.act_time )='".$current."'", 1
	);
	while( $db->isNext( 1 ) ) {
		$r = $db->getNext( 1 );

		// gesucht, aber nicht gekauft Schleife
		$str = '';
		$db->query( "
			SELECT l.search_pattern
			FROM TEC_LOG_SEARCH AS l
			WHERE
				l.user_id='".$r['user_id']."' AND
				l.action='0' AND
				l.buy_id='0' AND
				DATE( l.act_time )='".$current."'", 2 );
		while( $db->isNext( 2 ) ) {
			$r2 = $db->getNext( 2 );

			if( $str != "" ) $str .= ", ";
			$str .= $r2['search_pattern'];
		} // while

		if( $str != "" ) {
			if( $r['name'] == "" ) $r['name'] = $r['email'];
			$strDay .= '<tr><td>'.$r['name'].'<br>'.$r['info'].'</td><td>'.$str.'</td></tr>';
			$bPrintDay = true;
		} // if
	} // while

	$strDay .= '<tr><td colspan="2"></td></tr>';

	if( $bPrintDay )
		echo $strDay;
} // for

echo '</table></div>';

require_once( CLASS_DIR."templates/footer.php" );
?>