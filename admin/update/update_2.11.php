<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// neuer Menüpunkt Fragebogen
$update->insert_record( array(
	"table" => "CORE_MAINMENU",
	"check_field" => "title",
	"field" => array(
		"menu_id" => '',
		"project_id" => 3,
		"title" => "Eingabe",
		"picture" => "pics/erfassung.png",
		"file" => "admin/why_eingabe.php",
		"pos" => 25 ) ) );

// Neues Feld für Trainer ID in Antworten
$data = array( "table" => "WHY_USER_ANTWORTEN", "field" => "trainer_id", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );

// Neuer Primary Key
$db->query( "
	ALTER TABLE `WHY_USER_ANTWORTEN`
		DROP PRIMARY KEY,
		ADD PRIMARY KEY(
		`id`,
		`frage_id`,
		`antwort_id`,
		`trainer_id`);" );

// neuer Menüpunkt Datentransfer
$update->insert_record( array(
	"table" => "CORE_MAINMENU",
	"check_field" => "title",
	"field" => array(
		"menu_id" => '',
		"project_id" => 3,
		"title" => "Datentransfer",
		"picture" => "pics/transfer.png",
		"file" => "admin/why_datentransfer.php",
		"pos" => 27 ) ) );

$db->commit();
?>