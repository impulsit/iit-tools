<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Neue Option für Feldtypen
if( !$db->query( "SELECT type FROM CORE_FIELDTYPS WHERE type='textarea_query'" ) )
	$db->insert( "CORE_FIELDTYPS", array( "type" => "textarea_query" ) );


$db->commit();
?>