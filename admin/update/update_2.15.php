<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Trainer Zuordnung Button
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 32,
		"title" => "KURS_TRAINER_ZUORDNEN",
		"description" => "Trainer zuordnen",
		"picture" => "pics/trainer.png",
		"project_id" => 2 ) ) );

// Trainer Zuordnung Button zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 32,
		"list_id" => 1, // Kurse
		"pos" => 15 ) ) );

$db->commit();
?>