<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// -----------------------------------------------------------------------------
// Update

// neue Felder Kursteilnehmer
$data = array( "table" => "TEC_SETUP", "field" => "copy_picture_from_original", "fieldtype" => "TINYINT(4)" );
$update->add_new_field_only_database( $data );

$db->delete( "CORE_LISTS_FIELDS", "list_id='29'" );
$db->query( "
INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`) VALUES
(212, 29, 10, 'text', 'Hersteller', 'hersteller', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(213, 29, 20, 'text', 'Hersteller Nr.', 'hersteller_nr', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(214, 29, 30, 'text', 'Interne Nr.', 'interne_nr', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(215, 29, 40, 'decimal', 'Preis', 'preis', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(216, 29, 50, 'decimal', 'Lagerstand', 'lagerstand', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(217, 29, 60, 'text', 'Bemerkung', 'bemerkung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(218, 29, 70, 'text', 'Zusatzinfo', 'zusatzinfo', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(265, 29, 80, 'text', 'Bemerkung 2', 'description_2', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(266, 29, 90, 'text', 'Hersteller ID', 'brand_id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(267, 29, 100, 'text', 'Bild', 'item_picture', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(268, 29, 110, 'integer', 'Kategorie ID', 'category_id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(269, 29, 120, 'integer', 'Sub-Kategorie ID', 'subcategory_id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(270, 29, 130, 'integer', 'Sortierung', 'sorting', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(271, 29, 140, 'text', 'Bemerkung 3', 'description_3', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(272, 29, 150, 'text', 'Bild 2', 'item_picture_2', 0, '', '', '', '', '', '', '', '', '', 0, '', 0),
(273, 29, 160, 'text', 'Bild 3', 'item_picture_3', 0, '', '', '', '', '', '', '', '', '', 0, '', 0);
");

// End
$db->commit();
?>