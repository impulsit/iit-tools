<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 4 ) ) { // TecDoc
	$list_id = 40;
	$pos = 580;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  ".$pos.", 'text', 'Banner Gruppe', 'banner_group', 0, '', '', '', '', '', '', 'u.banner_group', '', '', 0, '', 0, 0)" );

	$list_id = 34;
	$pos = 170;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  ".$pos.", 'text', 'Banner Gruppe', 'banner_group', 0, '', '', '', '', '', '', 'u.banner_group', '', '', 0, '', 0, 0)" );

	$db->update( "CORE_LISTS", array( "sort_order" => 'id DESC' ), "list_id='42'" );

	$db->update( "TEC_SETUP", array( "default_language" => "en"), "id='1'" );
} // if

// End
$db->commit();
?>