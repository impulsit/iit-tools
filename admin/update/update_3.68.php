<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 2 ) ) { // Basilica
	$db->query( "REPLACE INTO `BAS_REPORTS` (`id`, `title`, `template`, `report_constant`) VALUES ('57', 'WAFF LAP Rechnung', 'themes/basilica_at/reports/WAFF_LAP_Rechnung.rtf', 'REP_WAFF_LAP_RECHNUNG');" );
	$db->query( "REPLACE INTO `BAS_REPORTS` (`id`, `title`, `template`, `report_constant`) VALUES ('58', 'WAFF Teilnehmerliste', 'themes/basilica_at/reports/WAFF_Teilnehmerinnenliste.rtf', 'REP_WAFF_TEILNEHMERLISTE');" );
} // if

// End
$db->commit();
?>