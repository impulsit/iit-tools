<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

$db->query( "ALTER TABLE `CORE_JOBS` ENGINE = InnoDB;" );

if( $f->project_allowed( 4 ) ) { // TecDoc
	$db->query( "ALTER TABLE `TEC_VIN_PROTOCOL` ENGINE = InnoDB;" );
} // if

if( $f->project_allowed( 2 ) ) { // basilica
	$data = array( "table" => "BAS_KURSE", "field" => "mo_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "di_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "mi_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "do_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "fr_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "sa_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "so_startzeit", "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "mo_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "di_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "mi_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "do_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "fr_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "sa_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_KURSE", "field" => "so_endzeit",   "fieldtype" => "TIME" ); $update->add_new_field_only_database( $data );

	$list_id = 1;

	$arr = array( "mo", "di", "mi", "do", "fr", "sa", "so" );
	for( $i=0; $i<=6; $i++ ) {
		$pos = 151 + (10 * $i);
		$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
		$db->query( "
			INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
				('', ".$list_id.", ".$pos.", 'time', '".strtoupper( $arr[$i] )." Startzeit', '".$arr[$i]."_startzeit', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
			");
	} // for
	for( $i=0; $i<=6; $i++ ) {
		$pos = 152 + (10 * $i);
		$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
		$db->query( "
			INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
				('', ".$list_id.", ".$pos.", 'time', '".strtoupper( $arr[$i] )." Endzeit', '".$arr[$i]."_endzeit', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
			");

		$pos = 120;
		$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
		$pos = 130;
		$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	} // for


} // if

// End
$db->commit();
?>