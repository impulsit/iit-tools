<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update
/*
1.
Projekte - Basilica - Listen - Basilica Berichte - Felder
pfad ändern auf themes/basilica_at/reports/

2.
UPDATE BAS_REPORTS SET template= REPLACE(template, 'classes/templates/reports_basilica/', 'themes/basilica_at/reports/')

3. neuer Bericht
46	AMS Teilnahmebestätigung	themes/basilica_at/reports/AMS_Teilnahmebestaetigung.rtf	REP_AMS_TEILNAHMEBESTAETIGUNG


4. Basilica Herkunft -> neues Feld Adresskopf

5. WAFF Report
47	WAFF Rechnung	themes/basilica_at/reports/WAFF_direkt_Rechnung.rtf	REP_WAFF_DIREKT

48 ... REP_WAFF_TEILNEHMER


6. EMAIL Liste no insert




*/
// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>