<?php
// neues Feld Kurse
$list_id = 1;
$pos = 85;

$data = array( "table" => "BAS_KURSE", "field" => "kurs_nicht_auf_homepage", "fieldtype" => "TINYINT(4)" );
$update->add_new_field_only_database( $data );

$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
	('', ".$list_id.",  ".$pos.", 'checkbox', 'Kurs nicht auf Homepage', 'kurs_nicht_auf_homepage', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1)" );

// Neue Liste Kurse Vorlagen
$db->query( "
	CREATE TABLE IF NOT EXISTS `BAS_KURSE_VORLAGEN` (
		`kurs_id` int(11) NOT NULL AUTO_INCREMENT,
		`nummer` varchar(50) NOT NULL,
		`dauer` int(11) NOT NULL,
		`kurstyp_id` int(11) NOT NULL,
		`kosten` decimal(10,2) NOT NULL,
		`kosten_wirtschaftskammer` decimal(10,2) NOT NULL,
		`weiterbildungs_interval` int(11) NOT NULL,
		`startdatum` date NOT NULL,
		`enddatum` date NOT NULL,
		`raummiete` decimal(10,2) NOT NULL,
		`bundesland_id` int(11) NOT NULL,
		`termin` date NOT NULL,
		`startzeit` time NOT NULL,
		`endzeit` time NOT NULL,
		`mo` tinyint(1) NOT NULL,
		`di` tinyint(1) NOT NULL,
		`mi` tinyint(1) NOT NULL,
		`do` tinyint(1) NOT NULL,
		`fr` tinyint(1) NOT NULL,
		`sa` tinyint(1) NOT NULL,
		`so` tinyint(1) NOT NULL,
		`lehrabschluss` tinyint(4) NOT NULL,
		`dauer_gesamt` int(11) NOT NULL,
		`wochen` varchar(250) NOT NULL,
		`stunden_pro_tag` int(11) NOT NULL,
		`abw_trainer_stundensatz` decimal(10,2) NOT NULL,
		`kosten_material` decimal(10,2) NOT NULL,
		`mo_startzeit` time NOT NULL,
		`di_startzeit` time NOT NULL,
		`mi_startzeit` time NOT NULL,
		`do_startzeit` time NOT NULL,
		`fr_startzeit` time NOT NULL,
		`sa_startzeit` time NOT NULL,
		`so_startzeit` time NOT NULL,
		`mo_endzeit` time NOT NULL,
		`di_endzeit` time NOT NULL,
		`mi_endzeit` time NOT NULL,
		`do_endzeit` time NOT NULL,
		`fr_endzeit` time NOT NULL,
		`sa_endzeit` time NOT NULL,
		`so_endzeit` time NOT NULL,
		`kurs_nicht_auf_homepage` tinyint(4) NOT NULL,
		PRIMARY KEY (`kurs_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$list_id = 45;
$db->query( "DELETE FROM CORE_LISTS WHERE list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
	(".$list_id.", 'Basilica - Kurse Vorlagen', 2, 'SELECT k.*, kt.title AS kt_title, bl.title AS bl_title<br />FROM BAS_KURSE_VORLAGEN AS k<br />LEFT JOIN BAS_KURSTYP AS kt ON (kt.id=k.kurstyp_id)<br />LEFT JOIN GLOBAL_BUNDESLAND AS bl ON (bl.id=k.bundesland_id)', 'kurs_id ASC', 'kurs_id', 'BAS_KURSE_VORLAGEN', 1, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1)" );

$db->query( "DELETE FROM CORE_LISTS_FIELDS WHERE list_id='".$list_id."'" );
$db->query( "INSERT INTO CORE_LISTS_FIELDS (SELECT '', ".$list_id.", pos, type, title, field, in_kurz, save_field, select_query, select_id, function, fill_fix_type, fill_fix_param, where_field, path, pattern, show_picture, color, read_only, mandatory, visible FROM CORE_LISTS_FIELDS WHERE list_id='1')" );
$db->query( "DELETE FROM CORE_LISTS_FIELDS WHERE list_id='".$list_id."' AND type='function'" );

// Hauptmenü
$db->query( "DELETE FROM CORE_MAINMENU WHERE list_id='".$list_id."'" );
$db->query( "INSERT INTO CORE_MAINMENU VALUES ('', 1, 'Kurse Vorlagen', '', 'admin/list_redirect.php', 106, ".$list_id.", 0, 'fas fa-warehouse text-primary')" );

$db->query( "UPDATE CORE_MAINMENU SET title=REPLACE(title, 'Basilica ', '') WHERE project_id='1'" );
$db->query( "UPDATE CORE_MAINMENU SET title=REPLACE(title, 'WHY ', '') WHERE project_id='1'" );
?>