<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Neues Feld "Notiz" bei Teilnehmer
$data = array(
	"db" => array(
		"table" => "BAS_TEILNEHMER",
		"fieldtype" => "MEDIUMTEXT",
	),
	"field" => array(
		"list_id" => 2,
		"pos" => 320,
		"type" => "textarea_query",
		"title" => "Notiz Text",
		"field" => "notiz_text",
	),
);
$update->add_new_field( $data );

// Neues Feld "Notiz" bei Teilnehmer
$data = array(
	"db" => array(
		"table" => "BAS_TEILNEHMER",
		"fieldtype" => "TINYINT(1)",
	),
	"field" => array(
		"list_id" => 2,
		"pos" => 330,
		"type" => "function",
		"title" => "Notiz",
		"in_kurz" => 1,
		"field" => "notiz_vorhanden",
		"function" => "notiz_vorhanden",
	),
);
$update->add_new_field( $data );

$db->commit();
?>