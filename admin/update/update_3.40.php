<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// -----------------------------------------------------------------------------
// Update

// neue Felder Kursteilnehmer
$data = array( "table" => "BAS_KURSTEILNEHMER", "field" => "foerderbetrag", "fieldtype" => "DECIMAL(10,2)" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_KURSTEILNEHMER", "field" => "eigenleistung", "fieldtype" => "DECIMAL(10,2)" );
$update->add_new_field_only_database( $data );

$update->insert_record( array(
	"table" => "CORE_LISTS_FIELDS",
	"check_field" => "field_id",
	"field" => array(
			"field_id" => 261,
			"list_id" => 3,
			"pos" => 54,
			"type" => "decimal",
			"title" => "genehmigter Förderbetrag",
			"field" => "foerderbetrag"
	)
) );

$update->insert_record( array(
		"table" => "CORE_LISTS_FIELDS",
		"check_field" => "field_id",
		"field" => array(
				"field_id" => 262,
				"list_id" => 25,
				"pos" => 44,
				"type" => "decimal",
				"title" => "genehmigter Förderbetrag",
				"field" => "foerderbetrag"
		)
) );

$update->insert_record( array(
		"table" => "CORE_LISTS_FIELDS",
		"check_field" => "field_id",
		"field" => array(
				"field_id" => 263,
				"list_id" => 3,
				"pos" => 56,
				"type" => "decimal",
				"title" => "Eigenleistung",
				"field" => "eigenleistung"
		)
) );

$update->insert_record( array(
		"table" => "CORE_LISTS_FIELDS",
		"check_field" => "field_id",
		"field" => array(
				"field_id" => 264,
				"list_id" => 25,
				"pos" => 46,
				"type" => "decimal",
				"title" => "Eigenleistung",
				"field" => "eigenleistung"
		)
) );

// End
$db->commit();
?>