<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// -----------------------------------------------------------------------------
// Update

global $_location;

// Neue Felder
$data = array( "table" => "CORE_SETUP", "field" => "hide_flags", "fieldtype" => "TINYINT(4)" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "TEC_SETUP", "field" => "td_countriesCarSelection", "fieldtype" => "VARCHAR(10)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_articleCountry", "fieldtype" => "VARCHAR(10)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_country", "fieldtype" => "VARCHAR(10)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_mandator", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_linkingtargettype", "fieldtype" => "VARCHAR(10)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_service_url", "fieldtype" => "VARCHAR(250)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_image_url", "fieldtype" => "VARCHAR(250)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "tm_endpoint", "fieldtype" => "VARCHAR(250)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "tm_endpoint_location", "fieldtype" => "VARCHAR(250)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "td_language", "fieldtype" => "VARCHAR(10)" );
$update->add_new_field_only_database( $data );

// FIN
$data = array( "table" => "TEC_SETUP", "field" => "fin_url", "fieldtype" => "VARCHAR(250)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "fin_location", "fieldtype" => "VARCHAR(250)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "fin_company", "fieldtype" => "VARCHAR(30)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "fin_username", "fieldtype" => "VARCHAR(30)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_SETUP", "field" => "fin_password", "fieldtype" => "VARCHAR(30)" );
$update->add_new_field_only_database( $data );


switch( $_location ) {
	case "nuic_atit_at":
			$arr1 = array( "hide_flags" => true );
			$arr2 = array(
				"td_countriesCarSelection" => 'de',
				"td_articleCountry" =>  'hr',
				"td_country" =>  'de',
				"td_language" =>  'sr',
				"td_mandator" => 20565,
				"td_linkingtargettype" => 'POAM',
				"td_service_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
				"td_image_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/',
				"tm_endpoint" => 'http://catalog.nuic.ba/axis2/services/Erp?wsdl',
				"tm_endpoint_location" => 'http://catalog.nuic.ba/axis2/services/Erp.ErpSoap',
				"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
				"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
				"fin_company" => 'atiT',
				"fin_username" => 'webservice',
				"fin_password" => 'Zt!mY3x',
			);
		break;
	case "elpida_atit_at":
			$arr1 = array( "hide_flags" => true );
			$arr2 = array(
				"td_countriesCarSelection" => 'de',
				"td_articleCountry" =>  'hr',
				"td_country" =>  'de',
				"td_language" =>  'sr',
				"td_mandator" => 20493,
				"td_linkingtargettype" => 'POAM',
				"td_service_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
				"td_image_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/',
				"tm_endpoint" => '',
				"tm_endpoint_location" => '',
				"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
				"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
				"fin_company" => 'atiT',
				"fin_username" => 'webservice',
				"fin_password" => 'Zt!mY3x',
			);
		break;
	case "rvtrade_atit_at":
			$arr1 = array( "hide_flags" => true );
			$arr2 = array(
				"td_countriesCarSelection" => 'de',
				"td_articleCountry" =>  'ro',
				"td_country" =>  'de',
				"td_language" =>  'sr',
				"td_mandator" => 20581,
				"td_linkingtargettype" => 'POAM',
				"td_service_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
				"td_image_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/',
				"tm_endpoint" => 'http://176.106.121.190:8002/mikomWS.asmx?WSDL',
				"tm_endpoint_location" => 'http://176.106.121.190:8002/mikomWS.asmx',
				"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
				"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
				"fin_company" => 'atiT',
				"fin_username" => 'webservice',
				"fin_password" => 'Zt!mY3x',
			);
		break;
		case "automercskoro_atit_at":
			$arr1 = array( "hide_flags" => true );
			$arr2 = array(
					"td_countriesCarSelection" => 'de',
					"td_articleCountry" =>  'hr',
					"td_country" =>  'de',
					"td_language" =>  'sr',
					"td_mandator" => 20565,
					"td_linkingtargettype" => 'POAM',
					"td_service_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
					"td_image_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/',
					"tm_endpoint" => 'http://ak2.myftp.org:8088/axis2/services/Erp?wsdl',
					"tm_endpoint_location" => 'http://ak2.myftp.org:8088/axis2/services/Erp.ErpSoap',
					"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
					"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
					"fin_company" => 'atiT',
					"fin_username" => 'webservice',
					"fin_password" => 'Zt!mY3x',
			);
			break;
	case "autodemo_atit_at":
			$arr1 = array( "hide_flags" => false );
			$arr2 = array(
				"td_countriesCarSelection" => 'de',
				"td_articleCountry" =>  'hr',
				"td_country" =>  'de',
				"td_language" =>  'sr',
				"td_mandator" => 20581,
				"td_linkingtargettype" => 'POAM',
				"td_service_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
				"td_image_url" => 'http://webservice-cs.tecdoc.net/pegasus-3-0/documents/',
				"tm_endpoint" => 'http://176.106.121.190:8002/mikomWS.asmx?WSDL',
				"tm_endpoint_location" => 'http://176.106.121.190:8002/mikomWS.asmx',
				"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
				"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
				"fin_company" => 'atiT',
				"fin_username" => 'webservice',
				"fin_password" => 'Zt!mY3x',
			);
		break;
	case "qnap_home":
			$arr1 = array( "hide_flags" => false );
			$arr2 = array(
				"td_countriesCarSelection" => 'de',
				"td_articleCountry" =>  'de',
				"td_country" =>  'de',
				"td_language" =>  'de',
				"td_mandator" => 20510,
				"td_linkingtargettype" => 'POAM',
				"td_service_url" => 'http://webservicepilot.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
				"td_image_url" => 'http://webservicepilot.tecdoc.net/pegasus-3-0/documents/',
				"tm_endpoint" => 'http://176.106.121.190:8002/mikomWS.asmx?WSDL',
				"tm_endpoint_location" => 'http://176.106.121.190:8002/mikomWS.asmx',
				"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
				"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
				"fin_company" => 'atiT',
				"fin_username" => 'webservice',
				"fin_password" => 'Zt!mY3x',
			);
		break;
	default:
		$arr1 = array( "hide_flags" => false );
		$arr2 = array(
				"td_countriesCarSelection" => 'de',
				"td_articleCountry" =>  'de',
				"td_country" =>  'de',
				"td_language" =>  'de',
				"td_linkingtargettype" => 'POAM',
				"td_service_url" => 'http://webservicepilot.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint',
				"td_image_url" => 'http://webservicepilot.tecdoc.net/pegasus-3-0/documents/',
				"fin_url" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx?wsdl',
				"fin_location" => 'https://tecrmi-services.tecalliance.net/40/ServiceVt.asmx',
				"fin_company" => 'atiT',
				"fin_username" => 'webservice',
				"fin_password" => 'Zt!mY3x',
		);
} // switch

$db->update( "CORE_SETUP", $arr1, "id='1'" );
$db->update( "TEC_SETUP", $arr2, "id='1'" );
$db->commit();

// End
$db->commit();
?>