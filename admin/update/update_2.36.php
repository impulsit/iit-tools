<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update

// neue Tabelle
$db->query( "
	CREATE TABLE IF NOT EXISTS `CORE_COLORS` (
	  `color` varchar(10) NOT NULL,
	  PRIMARY KEY (`color`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
" );

if( !$db->query( "SELECT * FROM CORE_COLORS" ) ) {
	$db->query( "
		INSERT INTO `CORE_COLORS` (`color`) VALUES
		('000000'),
		('00009B'),
		('003532'),
		('009901'),
		('00D2CB'),
		('010066'),
		('013300'),
		('036400'),
		('303498'),
		('3166FF'),
		('329A9D'),
		('32CB00'),
		('330001'),
		('340096'),
		('343300'),
		('343434'),
		('34696D'),
		('34CDF9'),
		('34FF34'),
		('3531FF'),
		('38FFF8'),
		('6200C9'),
		('643403'),
		('6434FC'),
		('646809'),
		('656565'),
		('663234'),
		('6665CD'),
		('67FD9A'),
		('680100'),
		('68CBD0'),
		('68FDFF'),
		('963400'),
		('9698ED'),
		('96FFFB'),
		('986536'),
		('999903'),
		('9A0000'),
		('9AFF99'),
		('9B9B9B'),
		('C0C0C0'),
		('CB0000'),
		('CBCEFB'),
		('CD9934'),
		('CDFFFF'),
		('CE6301'),
		('CFCFCF'),
		('F56B00'),
		('F8A102'),
		('F8FF00'),
		('FCFF2F'),
		('FD6864'),
		('FE0000'),
		('FE996B'),
		('FFC702'),
		('FFCB2F'),
		('FFCC67'),
		('FFCCC9'),
		('FFCE93'),
		('FFFC9E'),
		('FFFE65'),
		('FFFFC7'),
		('FFFFFF');
	" );
	$db->query( "
		ALTER TABLE `CORE_LISTS_FIELDS` ADD `color` VARCHAR(10) NOT NULL ;
	" );
} // if

// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>