<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update
$data = array( "table" => "CORE_MAINMENU", "field" => "fa_class", "fieldtype" => "VARCHAR(100)" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "CORE_PROJECTS", "field" => "fa_class", "fieldtype" => "VARCHAR(100)" );
$update->add_new_field_only_database( $data );

$db->update( "CORE_PROJECTS", array( "fa_class" => "fas fa-list-ul text-primary" ), "project_id='1'" );
$db->update( "CORE_PROJECTS", array( "fa_class" => "fas fa-exclamation-triangle text-primary" ), "project_id='2'" );
$db->update( "CORE_PROJECTS", array( "fa_class" => "far fa-question-circle text-primary" ), "project_id='3'" );
$db->update( "CORE_PROJECTS", array( "fa_class" => "fas fa-car text-primary" ), "project_id='4'" );

$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-warehouse text-primary" ), "list_id='1'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users text-primary" ), "list_id='2'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users text-primary" ), "list_id='3'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-globe-americas text-primary" ), "list_id='4'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-user-tie text-primary" ), "list_id='5'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-transgender text-primary" ), "list_id='6'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-user-secret text-primary" ), "list_id='7'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-warehouse text-primary" ), "list_id='8'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-shopping-cart text-primary" ), "list_id='9'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users-cog text-primary" ), "list_id='10'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-print text-primary" ), "list_id='11'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fab fa-aws text-primary" ), "list_id='12'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-question-circle text-primary" ), "list_id='13'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-question-circle text-primary" ), "list_id='14'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-question-circle text-primary" ), "list_id='15'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "far fa-question-circle text-primary" ), "list_id='16'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-chart-bar text-primary" ), "list_id='17'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-expand-arrows-alt text-primary" ), "list_id='18'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users text-primary" ), "list_id='19'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "far fa-question-circle text-primary" ), "list_id='20'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-user-graduate text-primary" ), "list_id='21'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-user-plus text-primary" ), "list_id='22'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-envelope text-primary" ), "list_id='23'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-envelope text-primary" ), "list_id='24'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-warehouse text-primary" ), "list_id='25'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-print text-primary" ), "list_id='27'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-globe-americas text-primary" ), "list_id='28'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-sitemap text-primary" ), "list_id='29'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-reply-all text-primary" ), "list_id='30'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-shopping-cart text-primary" ), "list_id='31'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-list-alt text-primary" ), "list_id='32'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-list-alt text-primary" ), "list_id='33'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users text-primary" ), "list_id='34'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-database text-primary" ), "list_id='35'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fab fa-aws text-primary" ), "list_id='36'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-folder-open text-primary" ), "list_id='37'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-folder-open text-primary" ), "list_id='38'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users text-primary" ), "list_id='39'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-users text-primary" ), "list_id='40'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-globe-americas text-primary" ), "list_id='41'" );

$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-print text-primary" ), "menu_id='3'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fab fa-staylinked text-primary" ), "menu_id='26'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-cog text-primary" ), "menu_id='4'" );

$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-keyboard text-primary" ), "menu_id='22'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-exchange-alt text-primary" ), "menu_id='23'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-cog text-primary" ), "menu_id='16'" );

$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-search text-primary" ), "title='Suche'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-cog text-primary" ), "title='Benutzer-Einstellungen'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fas fa-file-import text-primary" ), "title='(Admin) Import'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fab fa-staylinked text-primary" ), "title='(Admin) Statistik'" );
$db->update( "CORE_MAINMENU", array( "fa_class" => "fab fa-staylinked text-primary" ), "title='(Admin) FIN Statistik'" );

if( $f->project_allowed( 4 ) ) { // TecDoc
	$data = array( "table" => "CORE_USER_INFO", "field" => "banner_group", "fieldtype" => "VARCHAR(30)" );
	$update->add_new_field_only_database( $data );

	$list_id = 40;
	$pos = 580;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  ".$pos.", 'text', 'Banner Gruppe', 'banner_group', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0)" );

	$list_id = 34;
	$pos = 170;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  ".$pos.", 'text', 'Banner Gruppe', 'banner_group', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0)" );

	$db->query( "DROP TABLE IF EXISTS TEC_BANNER_GROUP" );
	$db->query( "
		CREATE TABLE IF NOT EXISTS `TEC_BANNER_GROUP` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `title` varchar(50) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );
	$db->query( "TRUNCATE TEC_BANNER_GROUP" );
	$db->query( "
		INSERT INTO `TEC_BANNER_GROUP` (`id`, `title`) VALUES
			(1, 'Banner Center 1 (left/center)'),
			(2, 'Banner Center 2 (right)'),
			(3, 'Banner Center 3 (background)'),
			(4, 'Banner Left Sidebar'),
			(5, 'Banner Left Offside'),
			(6, 'Banner Right Offside'),
			(7, 'Banner Login');" );

	$db->query( "DROP TABLE IF EXISTS TEC_BANNER" );
	$db->query( "
		CREATE TABLE IF NOT EXISTS `TEC_BANNER` (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `banner_position_id` int(11) NOT NULL,
		  `filename` varchar(100) NOT NULL,
		  `start_date` date NOT NULL,
		  `end_date` date NOT NULL,
		  `banner_group` varchar(30) NOT NULL,
		  `link_url` varchar(100) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

	$list_id = 42;
	$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'TecDoc - (Admin) Bannerverwaltung', 4,
			'SELECT b.*, bg.title AS banner_position_title FROM TEC_BANNER AS b LEFT JOIN TEC_BANNER_GROUP AS bg ON (bg.id=b.banner_position_id)',
			'id ASC', 'id', 'TEC_BANNER', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");

	$path = 'upload/'.$_location.'/banner/';
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  20, 'select', 'Banner Postition', 'banner_position_title', 0, 'banner_position_id', 'SELECT id, title AS banner_position_title FROM TEC_BANNER_GROUP', 'id', '', '', '', 'bg.title', '', '', 0, '', 0, 0),
		('', ".$list_id.",  30, 'select_file', 'Datei', 'filename', 0, '', '', '', '', '', '', '', '".$path."', '*.*', 1, '', 0, 0),
		('', ".$list_id.",  40, 'date', 'Startdatum', 'start_date', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  50, 'date', 'Enddatum', 'end_date', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  60, 'text', 'Banner Gruppe', 'banner_group', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  70, 'text', 'Link URL', 'link_url', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0)
	");

	// Hauptmenü
	$db->delete( "CORE_MAINMENU", "project_id='4' AND list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '4', '(Admin) Bannerverwaltung', 'themes/_default/icons/list.png', 'admin/list_redirect.php', '110', '".$list_id."', '100', 'fas fa-images text-primary');
	");

	// Korrektur Liste
	$db->update( "CORE_LISTS", array( "project_id" => 1 ), "list_id='41'" );

	// Korrektur Tables
	$db->query( "ALTER TABLE TEC_BANNER ENGINE=InnoDB;" );
	$db->query( "ALTER TABLE TEC_BANNER_GROUP ENGINE=InnoDB;" );
	$db->query( "ALTER TABLE TEC_ITEMS_CATEGORY ENGINE=InnoDB;" );
	$db->query( "ALTER TABLE TEC_ITEMS_SUBCATEGORY ENGINE=InnoDB;" );
} // if

$db->query( "ALTER TABLE CORE_LANGUAGE_SETUP ENGINE=InnoDB;" );

// End
$db->commit();
?>