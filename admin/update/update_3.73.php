<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 4 ) ) { // TecDoc
	$data = array( "table" => "TEC_SETUP", "field" => "fiber_no", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
} // if

// End
$db->commit();
?>