<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 4 ) ) { // TecDoc
	$db->update( "CORE_LISTS_FIELDS", array( "mandatory" => 0 ) , "field='password'" );
	$db->commit();

	// Liste OrderLog
	$list_id = 37;
	
	$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'TecDoc - (Admin) Order Log', 1, 'SELECT *<br>FROM TEC_BESTELLUNG', 'id ASC', 'id', 'TEC_BESTELLUNG', 0, '', 0, 'admin/list_redirect.php', 0, 0, 0, 0);
	");
	
	// Felder zu OrderLog
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.", 10, 'text', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 20, 'datetime', 'Bestellt am', 'bestellt_am', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 30, 'text', 'User ID', 'user_id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 40, 'textarea', 'WS Result', 'ws_result', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 50, 'textarea', 'WS XML', 'ws_xml', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
	");
	
	// Hauptmenü für OrderLog
	$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`) VALUES
		('', 1, 'TecDoc - Bestellungen Log', 'themes/_default/icons/info_blue.png', 'admin/list_redirect.php', 230, ".$list_id.", 0);
	");
	
	// Hauptmenü für Sprache
	$list_id = 28;
	$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`) VALUES
		('', 1, 'Sprache', 'themes/_default/icons/sprache.png', 'admin/list_redirect.php', 240, ".$list_id.", 0);
	");
	
	// Übersetzung
	$db->query( "TRUNCATE CORE_LANGUAGE;" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('1','Login','Login','login','Prijava','Prijava','Prijava','Войти');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('2','Passwort vergessen','Passwort vergessen','Forgot Password','Zaboravili ste lozinku','Zaboravili ste lozinku','Zaboravili ste lozinku','Забыл пароль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('3','E-Mail','Benutzername','Username','Korisničko ime','Korisničko ime','Korisničko ime','Имя пользователя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('4','Passwort','Passwort','password','Lozinka','Lozinka','Lozinka','пароль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('5','Listen','Listen','lists','Statistike','Statistike','Statistike','Списки');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('6','TecDoc','TecDoc','TecDoc','TecDoc','TecDoc','TecDoc','TecDoc');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('7','ausloggen','ausloggen','log out','Odjava','Odjava','Odjava','Выход');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('8','Hauptmenü','Hauptmenü','main menu','Početna stranica','Početna stranica','Početna stranica','Главное меню');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('9','Suche','Suche','search','Pretraga','Pretraga','Pretraga','Поиск');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('10','Einstellungen','Einstellungen','settings','Podešavanja','Podešavanja','Podešavanja','Настройки');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('11','hoch','hoch','high','Vrati na vrh','Vrati na vrh','Vrati na vrh','Вверх');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('12','Warenkorb','Warenkorb','Shopping','Korpa','Korpa','Korpa','Корзина');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('13','anmelden','anmelden','Sign in','Prijava ','Prijava ','Prijava ','Вход');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('14','Passwort rücksetzen','Passwort rücksetzen','Reset Password','Resetovanje lozinke','Resetovanje lozinke','Resetovanje lozinke','Сброс пароля');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('15','Diese E-Mail existiert nicht.','Diese E-Mail existiert nicht.','This e-mail does not exist.','E-mail adresa ne postoji','E-mail adresa ne postoji','E-mail adresa ne postoji','Этот адрес электронной почты не существует.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('16','Die Zugangsdaten stimmen nicht.','Die Zugangsdaten stimmen nicht.','The credentials are incorrect.','Netačne informacije za prijavu ','Netačne informacije za prijavu ','Netačne informacije za prijavu ','Учетные данные являются неправильными.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('17','Session konnte nicht bestimmt werden, bitte aktivieren sie Cookies und loggen Sie erneut ein.','Session konnte nicht bestimmt werden, bitte aktivieren sie Cookies und loggen Sie erneut ein.','Session could not be determined, please activate cookies and log in again.','Sesija je istekla, molim prijavite se ponovo.','Sesija je istekla, molim prijavite se ponovo.','Sesija je istekla, molim prijavite se ponovo.','Сессия не может быть определена, пожалуйста, включите куки и снова войти в систему.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('18','Benutzer-Einstellungen','Benutzer-Einstellungen','User Settings','Korisnička podešavanja','Korisnička podešavanja','Korisnička podešavanja','Настройки пользователя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('19','Import','Import','import','Unos','Unos','Unos','Импорт');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('20','Admin-Einstellungen','Admin-Einstellungen','Admin Settings','Postavke Administratora','Postavke Administratora','Postavke Administratora','Настройки администратора');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('21','speichern','speichern','save','Sačuvaj ','Sačuvaj ','Sačuvaj ','Сохранить');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('22','Einstellungen gespeichert.','Einstellungen gespeichert.','Settings saved.','Podešavanja sačuvana','Podešavanja sačuvana','Podešavanja sačuvana','Настройки сохранены.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('23','Das neue Passwort muss mindestens 5 Zeichen haben.','Das neue Passwort muss mindestens 5 Zeichen haben.','The new password must have at least 5 characters.','Nova lozinka mora sadržavati najmanje pet znakova ','Nova lozinka mora sadržavati najmanje pet znakova ','Nova lozinka mora sadržavati najmanje pet znakova ','Новый пароль должен иметь не менее 5 символов.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('24','Änderungen wurden gespeichert.','Änderungen wurden gespeichert.','Changes have been saved.','Promene su sačuvane','Promjene su sačuvane','Promjene su sačuvane','Изменения сохранены.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('25','(Admin) Import','(Admin) Import','(Admin) Import','(Admin) unos','(Admin) unos','(Admin) unos','(Админ) Импорт');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('26','(Admin) Einstellungen','(Admin) Einstellungen','(Admin) settings','(Admin) podešavanja','(Admin) podešavanja','(Admin) podešavanja','(Админ) Настройки');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('27','(Admin) Warenkorb','(Admin) Warenkorb','(Admin) cart','(Admin) korpa','(Admin) korpa','(Admin) korpa','(Админ) Корзина');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('28','Importieren','Importieren','Import','Unos','Unos','Unos','Импорт');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('29','Bitte eine Datei auswählen.','Bitte eine Datei auswählen.','Please select a file.','Izaberite datoteku','Izaberite datoteku','Izaberite datoteku','Пожалуйста, выберите файл.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('30','Importiere Datei...','Importiere Datei...','Importing file ...','Unos datoteke ...','Unos datoteke ...','Unos datoteke ...','Импорт файла ...');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('31','Es wurden alle Zeilen importiert.','Es wurden alle Zeilen importiert.','It all rows have been imported.','Sve redovi su uneseni','Sve redovi su uneseni','Sve redovi su uneseni','Все строки были импортированы.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('32','Ansicht','Ansicht','view','Pregled ','Pregled ','Pregled ','Вид');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('33','Kaufen','Kaufen','Buy','Kupiti','Kupiti','Kupiti','Купить');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('34','(Admin) Artikel / Lager','(Admin) Artikel / Lager','(Admin) Products','(Admin) artikli','(Admin) artikli','(Admin) artikli','(Админ) Продукт / Склад');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('35','(Admin) Ersatznummern','(Admin) Ersatznummern','(Admin) Replacement numbers','(Admin) zamenski brojevi','(Admin) zamjenski brojevi','(Admin) zamjenski brojevi','(Админ) Номера для замены');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('36','löschen','löschen','delete','Izbrisati','Izbrisati','Izbrisati','Удалять');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('37','bestellen','bestellen','to order','Naručiti','Naručiti','Naručiti','Заказ');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('38','(Admin) Bestellungen','(Admin) Bestellungen','(Admin) Orders','(Admin) porudžbenice','(Admin) porudžbenice','(Admin) porudžbenice','(Админ) Заказы');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('39','Menü','Menü','menu','Meni','Meni','Meni','Меню');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('40','ändern','ändern','change','Promena','Promjena','Promjena','Изменить');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('41','Bestellungen','Bestellungen','orders','Porudžbenice','Porudžbenice','Porudžbenice','Заказы');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('42','Fahrzeughersteller','Fahrzeughersteller','vehicle manufacturers','Proizvođači vozila','Proizvođači vozila','Proizvođači vozila','Производители');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('43','Baujahr','Baujahr','Year','Godina','Godina','Godina','Год');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('44','Fahrzeugmodell','Fahrzeugmodell','vehicle model','Model vozila ','Model vozila ','Model vozila ','Модель');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('45','Fahrzeug','Fahrzeug','vehicle','Vozilo','Vozilo','Vozilo','Aвтомобиль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('46','von','von','from','od','od','od','из');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('47','bis','bis','to','do','do','do','к');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('48','Gruppen','Gruppen','groups','Grupe','Grupe','Grupe','Группы');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('49','Kategorien','Kategorien','Categories','Kategorije','Kategorije','Kategorije','Категории');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('50','Filter','Filter','filter','Filter','Filter','Filter','Фильтры');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('51','suchen','suchen','search','Pretraga','Pretraga','Pretraga','Поиск');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('52','alle anzeigen','alle anzeigen','Show all','Sve prikazati','Sve prikazati','Sve prikazati','Показать все');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('53','Session ist nicht mehr gültig, bitte loggen Sie erneut ein.','Session ist nicht mehr gültig, bitte loggen Sie erneut ein.','Session is no longer valid, please sign in again.','Sesija više ne važi, molim prijavite se ponovo.','Sesija više ne važi, molim prijavite se ponovo.','Sesija više ne važi, molim prijavite se ponovo.','Сессия уже не действует, пожалуйста, войдите в систему снова.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('54','Artikel','Artikel','items','Artikal','Artikal','Artikal','Товар');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('55','Marke','Marke','brand','Marka','Marka','Marka','Брэнд');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('56','Beschreibung','Beschreibung','description','Opis','Opis','Opis','Описание');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('57','Lagerstand','Lagerstand','stock levels','Stanje','Stanje','Stanje','Уровни запасов');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('58','Menge','Menge','amount','Količina','Količina','Količina','Количество');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('59','Preis','Preis','price','Cena ','Cijena ','Cijena ','Цена');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('60','Gesamt','Gesamt','total','Ukupno','Ukupno','Ukupno','Всего');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('61','Aktion','Aktion','action','Akcija','Akcija','Akcija','Действие');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('62','Zum Warenkorb wurde hinzugefügt','Zum Warenkorb wurde hinzugefügt','was added to cart','Dodato u korpu ','Dodato u korpu ','Dodato u korpu ','был добавлен в корзину');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('63','Artikel Nr.','Artikel Nr.','Article no.','Broj artikla','Broj artikla','Broj artikla','Номер товара');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('64','Artikelname','Artikelname','Product Name','Naziv proizvoda','Naziv proizvoda','Naziv proizvoda','Название товара');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('65','Betrag','Betrag','amount','Iznos','Iznos','Iznos','Общая сумма');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('66','Bestellung wurde abgeschickt.','Bestellung wurde abgeschickt.','This order was sent.','Porudžbenica je poslata','Porudžbenica je poslata','Porudžbenica je poslata','Этот заказ был отправлен');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('67','keine Daten','keine Daten','no data','Nema podataka','Nema podataka','Nema podataka','Нет данных');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('68','Bestellnr.','Bestellnr.','Cat.','Broj porudžbenice','Broj porudžbenice','Broj porudžbenice','Номер заказа');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('69','bestellt am','bestellt am','ordered on','Datum porudžbenice','Datum porudžbenice','Datum porudžbenice','Дата заказа');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('70','bestellte Menge','bestellte Menge','ordered quantity','Poručena količina ','Poručena količina ','Poručena količina ','заказанное количество');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('71','Status','Status','status','Stanje','Stanje','Stanje','статус');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('72','wird bearbeitet','wird bearbeitet','Processing','Obrada','Obrada','Obrada','обрабатывается');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('73','Bearbeitung abgeschlossen','Bearbeitung abgeschlossen','editing finished','Obrada završena','Obrada završena','Obrada završena','обработка завершена');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('74','Passwort ändern','Passwort ändern','Change Password','Promena lozinke ','Promjena lozinke ','Promjena lozinke ','Изменить пароль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('75','aktuelles Passwort','aktuelles Passwort','current password','Trenutna lozinka','Trenutna lozinka','Trenutna lozinka','текущий пароль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('76','neues Passwort','neues Passwort','New Password','Nova lozinka','Nova lozinka','Nova lozinka','новый пароль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('77','neues Passwort wiederholen','neues Passwort wiederholen','Repeat new password','Ponovite novu lozinku','Ponovite novu lozinku','Ponovite novu lozinku','Повторите новый пароль');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('78','Benutzerdaten ändern','Benutzerdaten ändern','Change user data','Promena korisničkih podataka','Promjena korisničkih podataka','Promjena korisničkih podataka','Изменение данных пользователя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('79','Name','Name','name','Naziv','Naziv','Naziv','имя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('80','Artikel Import','Artikel Import','Article import','Unos proizvoda','Unos proizvoda','Unos proizvoda','Статья импорта');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('81','Datei','Datei','file','Datoteka','Datoteka','Datoteka','файл');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('82','Ersatznummern Import','Ersatznummern Import','Spare numbers Import','Unos zamenskih brojeva ','Unos zamenskih brojeva ','Unos zamenskih brojeva ','Запасные номера Импорт');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('83','Hersteller','Hersteller','manufacturer','Proizvođač','Proizvođač','Proizvođač','производитель');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('84','Hersteller Nr.','Hersteller Nr.','Serial number.','Broj proizvođača','Broj proizvođača','Broj proizvođača','Серийный номер.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('85','Interne Nr.','Interne Nr.','Internal Number.','Interni broj','Interni broj','Interni broj','Внутренний номер.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('86','Bemerkung','Bemerkung','remark','Primedba','Primjedba','Primjedba','замечание');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('87','Zusatzinfo','Zusatzinfo','additional info','Dodatne informacije','Dodatne informacije','Dodatne informacije','дополнительная информация');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('88','Ersatz Nr.','Ersatz Nr.','Spare no.','Zamenski broj','Zamjenski broj','Zamjenski broj','Запасной номер');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('89','Hersteller Ersatz Nr.','Hersteller Ersatz Nr.','Manufacturers spare no.','Proizvođač zamenskog broja','Proizvođač zamjenskog broja','Proizvođač zamjenskog broja','Запасной номер производителей');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('90','Suche nach','Suche nach','search for','Pretraga','Pretraga','Pretraga','поиск');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('91','Suchtyp','Suchtyp','search type','Vrsta pretrage','Vrsta pretrage','Vrsta pretrage','тип поиска');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('92','Text','Text','text','Svi brojevi','Svi brojevi','Svi brojevi','текст');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('93','OE','OE','OE','OE','OE','OE','OE');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('94','Vergleichsnummer','Vergleichsnummer','Vergleichsnummer','Uporedni broj','Uporedni broj','Uporedni broj','Vergleichsnummer');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('95','Hersteller Artikelnummer','Hersteller Artikelnummer','Manufacturer Model','Proizvođačev broj artikla','Proizvođačev broj artikla','Proizvođačev broj artikla','Артикуль производителя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('96','EAN Nummer','EAN Nummer','EAN Code','EAN broj','EAN broj','EAN broj','номер EAN');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('97','Preis (unverbindliche Preisempfehlung)','Preis (unverbindliche Preisempfehlung)','Price (MSRP)','Cena (TecDoc)','Cijena (TecDoc)','Cijena (TecDoc)','Цена (MSRP)');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('98','Attribute','Attribute','attributes','Atributi','Atributi','Atributi','атрибуты');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('99','Bilder','Bilder','Images','Slike','Slike','Slike','фотографии');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('100','Vergleichsnummern','Vergleichsnummern','Cross reference','Uporedni brojevi','Uporedni brojevi','Uporedni brojevi','Перекрестная ссылка');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('101','Suche nach Motorcode','Suche nach Motorcode','Seach by engine code','Pretraga po kodu motora','Pretraga po kodu motora','Pretraga po kodu motora','Поиск по номеру двигателя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('102','kein Preis','kein Preis','no price','Nema cene','Nema cijene','Nema cijene','нет цены');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('103','passende Fahrzeuge','passende Fahrzeuge','appropriate vehicles','Odgovarajuća vozila','Odgovarajuća vozila','Odgovarajuća vozila','соответствующие транспортные средства');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('104','Modell','Modell','model','Model','Model','Model','модель');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('105','Typ (Motor)','Typ (Motor)','Type (Engine)','Tip (Motor)','Tip (Motor)','Tip (Motor)','Тип (Двигатель)');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('106','PS/KW','PS/KW','PS / KW','KS/KW','KS/KW','KS/KW','PS / KW');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('107','Baujahr von','Baujahr von','Year of','Godina proizvodnje od','Godina proizvodnje od','Godina proizvodnje od','С года');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('108','Baujahr bis','Baujahr bis','Year to','Godina proizvodnje do','Godina proizvodnje do','Godina proizvodnje do','До года');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('109','Google','Google','Google','Google','Google','Google','Google');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('110','Bild','Bild','image','Slika','Slika','Slika','картина');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('111','TEC Preis','TEC Preis','TEC Price','TEC Cena','TEC Cijena','TEC Cijena','TEC Цена');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('112','nein','nein','No','ne','ne','ne','нет');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('113','OK','OK','OK','OK','OK','OK','хорошо');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('114','abweisen','abweisen','reject','Odbiti','Odbiti','Odbiti','отклонять');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('115','abgelehnt','abgelehnt','rejected','Odbijeno','Odbijeno','Odbijeno','отвергнуто');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('116','logout','logout','logout','Odjava','Odjava','Odjava','выход из системы');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('117','Suche nach Fahrzeug','Suche nach Fahrzeug','Search by vehicle','Pretraga po vozilu','Pretraga po vozilu','Pretraga po vozilu','Поиск по транспортным средствам');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('118','Suche nach Text','Suche nach Artikelnr.','Search by article no.','Pretraga po broju artikla','Pretraga po broju artikla','Pretraga po broju artikla','поиск по артикулу');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('119','Info','Info','info','Info','Info','Info','информация');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('120','Suche aktuelles Fenster','Suche aktuelles Fenster','Search in current window','Pretraga u ovom prozoru','Pretraga u ovom prozoru','Pretraga u ovom prozoru','Поиск в текущем окне');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('121','Suche neues Fenster','Suche neues Fenster','Search in new window','Pretraga u novom prozoru','Pretraga u novom prozoru','Pretraga u novom prozoru','Поиск в новом окне');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('122','ja','ja','Yes','da','da','da','да');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('123','Projekt Verwaltung','Projekt Verwaltung','project management','Administracija projekta','Administracija projekta','Administracija projekta','управление проектом');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('124','upload','upload','upload','Unos','Unos','Unos','загружать');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('125','Typ','Typ','Type','Vrsta','Vrsta','Vrsta','тип');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('126','Start','Start','start','Start','Start','Start','старт');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('127','Beendet','Beendet','Finished','Gotovo','Gotovo','Gotovo','Готово');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('128','Startzeit','Startzeit','Start time','Početno vreme','Početno vrijeme','Početno vrijeme','время начала');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('129','Endzeit','Endzeit','End Time','Vreme završetka','Vrijeme završetka','Vrijeme završetka','Время окончания');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('130','Artikel Datei muss 8 Spalten haben (bis H), Ersatznummern Datei muss 5 Spalten haben (bis E)','Artikel Datei muss 11 Spalten haben (bis K), Ersatznummern Datei muss 5 Spalten haben (bis E)','Article file must have 11 columns (to K), replacement numbers file must have 5 columns (to E)','Artikli/Stanja/Cene: 11 kolona (do K), Uporedni brojevi 5 kolona (do E)','Artikli/Stanja/Cijene: 8 kolona (do H), Uporedni brojevi 5 kolona (do E)','Artikli/Stanja/Cijene: 11 kolona (do K), Uporedni brojevi 5 kolona (do E)','Данные по артикул должны быть заполнены до столбец (K), Данные по замене должены иметь 5 столбцов (до Е)');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('131','Spalten','Spalten','cleave','Kolone','Kolone','Kolone','Столбец');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('132','Datei wirklich löschen?','Datei wirklich löschen?','to delete file?','Izbrisati datoteku?','Izbrisati datoteku?','Izbrisati datoteku?','удалить файл?');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('133','Import wird gestartet','Import wird gestartet','Import starts','Aktiviranje importa','Aktiviranje importa','Aktiviranje importa','Импорт начинается');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('134','Import läuft','Import läuft','import runs','Unos je u toku','Unos je u toku','Unos je u toku','импорт работает');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('135','Ersatznummer','Ersatznummer','replacement number','Zamenski broj','Zamjenski broj','Zamjenski broj','номер замены');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('136','Import beendet','Import beendet','import completed','Import je završen','Import je završen','Import je završen','импорт завершен');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('137','unbekannt','unbekannt','unknown','nepoznato','nepoznato','nepoznato','неизвестный');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('138','Alle Nummern','Alle Nummern','All numbers','Svi brojevi','Svi brojevi','Svi brojevi','Все номера');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('139','Achtung Sie bestellen mehr als derzeit verfügbar. Trotzdem fortfahren?','Aktion abgebrochen, Sie bestellen mehr als derzeit verfügbar.','Attention, you order more than is currently available.','Artikal nije ubačen u korpu jer naručujete više nego što je trenutno na skladištu.','Artikal nije ubačen u korpu jer naručujete više nego što je trenutno na skladištu.','Artikal nije ubačen u korpu jer naručujete više nego što je trenutno na skladištu.','Внимание вы заказываете больше, чем в имеется в наличии в настоящее время.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('140','Komponenten','Komponenten','components','Komponente','Komponente','Komponente','компоненты');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('141','Bestell-Information','Bestell-Information','Ordering Information','Informacije o porudžbenici','Informacije o porudžbenici','Informacije o porudžbenici','Информация для заказа');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('142','Text für Bestellung wurde gespeichert.','Text für Bestellung wurde gespeichert.','Text for order was saved.','Tekst za porudžbenicu je memorisan.','Tekst za porudžbenicu je memorisan.','Tekst za porudžbenicu je memorisan.','Текст для заказа был сохранен');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('143','Benutzer-Info','Benutzer-Info','User Information','Informacije o korisniku','Informacije o korisniku','Informacije o korisniku','информация о пользователе');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('144','alles OK','alles OK','everything OK','Sve OK','Sve OK','Sve OK','все в порядке');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('145','alles abweisen','alles abweisen','reject all','Sve odbiti','Sve odbiti','Sve odbiti','отклонить все');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('146','Das Passwort wurde geändert.','Das Passwort wurde geändert.','The password has been changed.','Lozinka je promenjena','Lozinka je promijenjena','Lozinka je promijenjena','Пароль был изменен.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('147','Suche nach Artikelbezeichnung','Suche nach Artikelbezeichnung','Search Product','Pretraga po nazivu artikla','Pretraga po nazivu artikla','Pretraga po nazivu artikla','Поиск по продукции');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('148','Suche nach Hersteller','Suche nach Hersteller','Search Make','Pretraga po proizvođaču','Pretraga po proizvođaču','Pretraga po proizvođaču','Поиск по Марке');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('149','Währung','Währung','currency','Valuta','Valuta','Valuta','валюта');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('150','Netto-Preis','Netto','Net','neto','neto','neto','нетто');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('151','Brutto-Preis','Netto (inkl. Ust.)','Net (inc. VAT)','neto + PDV','neto + PDV','neto + PDV','цена + НДС');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('152','Brutto-Betrag','Brutto-Betrag','Gross amount','bruto iznos','bruto iznos','bruto iznos','валовая сумма');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('153','Datensatz wirklich löschen?','Datensatz wirklich löschen?','to delete record?','Izbrisati red?','Izbrisati red?','Izbrisati red?','удалить запись?');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('154','alle Fahrzeuge laden','alle Fahrzeuge laden','invite all vehicles','Sva vozila','Sva vozila','Sva vozila','Скачать все транспортные средства');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('155','Liste','Liste','list','Detalji','Detalji','Detalji','список');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('156','Karte','Karte','map','Slike','Slike','Slike','карта');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('157','kein Ergebnis gefunden','kein Ergebnis gefunden','without results','Nema rezultata za pretragu','Nema rezultata za pretragu','Nema rezultata za pretragu','без результатов');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('158','Alle Preise in KM','Alle Preise exkl. MwSt.','All prices excl..VAT','Sve cene su VP','Sve cijene su VP','Sve cijene su VP','Все цены указаны без НДС');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('159','Karosserie','Karosserie','body','Šasija','Šasija','Šasija','кузов');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('160','Treibstoff','Treibstoff','fuel','Vrsta goriva','Vrsta goriva','Vrsta goriva','топливо');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('161','Suche nach Bemerkungen','Universalartikel','Universal items','Univerzalni artikli','Univerzalni artikli','Univerzalni artikli','Общие артикулы');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('162','ähnliche Suche','ähnliche Suche','similar searches','Slično traženje','Slično traženje','Slično traženje','подобные поиски');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('163','Stück','Stück','piece','Kom.','Kom.','Kom.','Штук');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('164','Drucken','Drucken','To press','Odštampati','Odštampati','Odštampati','печать');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('165','Bestellung','Bestellung','Order','Porudžbenica','Porudžbenica','Porudžbenica','заказ');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('166','Datum','Datum','date','Datum','Datum','Datum','дата');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('167','alle Fahrzeuge','alle Fahrzeuge','all vehicles','Sva vozila','Sva vozila','Sva vozila','все транспортные средства');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('168','ccm','ccm','ccm','ccm','ccm','ccm','куб.см');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('169','KW','KW','KW','KW','KW','KW','KB');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('170','PS','PS','PS','KS','KS','KS','ЛC');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('171','(Admin) Statistik','(Admin) Statistik','(Admin) Statistics','(Admin) statistika','(Admin) statistika','(Admin) statistika','(Admin) Статистика');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('172','gesucht aber nicht gekauft','gesucht aber nicht gekauft','searched but not purchased','Traženo a nekupljeno','Traženo a nekupljeno','Traženo a nekupljeno','искали, но не приобрели');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('173','Impressum','Impressum','imprint','Impresum','Impresum','Impresum','реквизиты');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('174','Position','Position','position','Pozicija','Pozicija','Pozicija','положение');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('175','Der Benutzer ist abgelaufen.','Der Benutzer ist abgelaufen.','The user has expired.','Korisnik je istekao.','Korisnik je istekao.','Korisnik je istekao.','Пользователь истек.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('176','Der Benutzer ist abgelaufen. Bitte setzen Sie sich mit uns in Verbindung. E-Mail: office@atit.at (Tel: +43 664 968 29 42)','Der Benutzer ist abgelaufen. Bitte setzen Sie sich mit uns in Verbindung. E-Mail: office@atit.at (Tel: +43 664 968 29 42)','The user has expired. Please contact us. Email: office@atit.at (Tel: 664 968 29 42 +43)','Korisnik je istekao. Obratite se na mejl office@atit.at  ili na telefon +43 664 968 29 42.','Korisnik je istekao. Obratite se na mejl office@atit.at  ili na telefon +43 664 968 29 42.','Korisnik je istekao. Obratite se na mejl office@atit.at  ili na telefon +43 664 968 29 42.','Пользователь истек. Пожалуйста, свяжитесь с нами. E-mail: office@atit.at (тел: 664 968 29 42 +43)');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('177','Motorcode','Motorcode','Engine code','Kod motora','Kod motora','Kod motora','код двигателя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('178','Zylinder','Zylinder','Cylinder','Cilindar','Cilindar','Cilindar','цилиндр');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('179','Lagerort','Lagerort','Location','Filijala','Filijala','Filijala','филиал');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('180','Kundennummer','Kundennummer','Customer no.','Broj kupca','Broj kupca','Broj kupca','номер клиента');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('181','Sie müssen eine Kundennummer in den Einstellungen hinterlegen.','Sie müssen eine Kundennummer in den Einstellungen hinterlegen.','Please enter the customer no. in the user setup','Potrebno je zadati broj kupca u konfiguraciji korisnika','','Potrebno je zadati broj kupca u konfiguraciji korisnika','Вы должны определить номер клиента в настройках');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('182','letzte Suchen','letzte Suchen','Previous search','Prethodne pretrage','Prethodne pretrage','Prethodne pretrage','предыдущий поиск');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('183','Suche mit Kundennummer','Suche mit Kundennummer','Search with Customer No.','Pretraga za kupca','Pretraga za kupca','Pretraga za kupca','Поиск по номеру клиента');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('184','WebService Mapping','WebService Mapping','Webservice mapping','Webservice mapping','Webservice mapping','Webservice mapping','Oтображение вебсервис');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('185','Artikel + WebService','Artikel + WebService','Items + Webservice in use','Artikli + Webservice u upotrebi','Artikli + Webservice u upotrebi','Artikli + Webservice u upotrebi','Предметы + вебсервис');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('186','Artikel + kein WebService','Artikel + kein WebService','Items + No Webservice in use','Artikli + bez Webservice-a u upotrebi','Artikli + bez Webservice-a u upotrebi','Artikli + bez Webservice-a u upotrebi','Предметы + без вебсервис');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('187','Beispiel','Beispiel','Example','Primer','Primjer','Primjer','Пример');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('188','Exportieren','Exportieren','Export','Export','Export','Export','экспорт');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('189','RESET','RESET','RESET','RESET','RESET','RESET','сброс');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('190','Suche über alle Kategorien. Detailergebnisse werden in den einzelnen Kategorien angezeigt.','Suche über alle Kategorien. Detailergebnisse werden in den einzelnen Kategorien angezeigt.','Search across all categories. Detailed results are shown in the individual categories.','Pretraga po svim grupama. Rezultati pretrage se nalaze u grupi.','Pretraga po svim grupama. Rezultati pretrage se nalaze u grupi.','Pretraga po svim grupama. Rezultati pretrage se nalaze u grupi.','Поиск по всем категориям. Подробные результаты показаны в отдельных категориях.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('191','Suche nach FIN','Suche nach FIN','Search by VIN','Pretraga po broju šasije','Pretraga po broju šasije','Pretraga po broju šasije','VIN');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('192','Hauptartikel','Hauptartikel','Main article','Glavni artikal','Glavni artikal','Glavni artikal','основная статья');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('193','verwendete Nummern','Verwendete Nummern','Used numbers','Prateći artikli','Prateći artikli','Prateći artikli','используемые номера');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('194','Service','Technische Daten','Technical data','Tehnički podaci','Tehnički podaci','Tehnički podaci','техническая информация');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('195','Informationen','Informationen','Information','Informacije','Informacije','Informacije','информация');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('196','Dokumente','Dokumente','Documents','Dokumenti','Dokumenti','Dokumenti','документы');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('197','Ersetzt durch','Ersetzt durch Nr.','Replaced by no.','Zamenjen brojem','Zamijenjen brojem','Zamijenjen brojem','заменяется номером');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('198','Ersatz für','Ersatz für Nr.','Replacement for no.','Zamena za broj','Zamjena za broj','Zamjena za broj','замена номера');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('199','Suche läuft...','Suche läuft...','Search...','Pretraga u toku...','Pretraga u toku...','Pretraga u toku...','просмотр');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('200','neu','neu','new','Novi','Novi','Novi','новый');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('201','User ID','User ID','User ID','User ID','User ID','User ID','Идентификатор пользователя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('202','Login / E-Mail','Login / E-Mail','Login / E-Mail','Login','Login','Login','авторизоваться');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('203','TMWS Kundennummer','ERP Kundennr.','ERP Customer No.','ERP šifra korisnika','ERP šifra korisnika','ERP šifra korisnika','номер клиента');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('204','läuft ab','läuft ab','valid to','Datum isteka','Datum isteka','Datum isteka','Действителен до');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('205','letzter Login','letzter Login','last login','Poslednja prijava','Poslednja prijava','Poslednja prijava','последний Войти');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('206','Rolle','Rolle','Role','Korisnički nivo','Korisnički nivo','Korisnički nivo','уровень пользователя');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('207','Benutzer Level 1','Benutzer Level 1','User Level 1','Korisnički nivo 1','Korisnički nivo 1','Korisnički nivo 1','уровень пользователя 1');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('208','Super Admin','Super Admin','Super Admin','Super Admin','Super Admin','Super Admin','суперадминистратор');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('209','Projekt Admin','Projekt Admin','Project Admin','Administrator projekta','Administrator projekta','Administrator projekta','проект администратор');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('210','(Admin) Benutzer','(Admin) Benutzer','(Admin) users','(Admin) korisnici','(Admin) korisnici','(Admin) korisnici','(Aдмин) пользователи');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('211','Sie müssen einen Lagerort auswählen.','Sie müssen einen Lagerort auswählen.','Please select a location','Molimo vas da prvo odaberete lokaciju sa koje naručujete.','Molimo vas da prvo odaberete lokaciju sa koje naručujete.','Molimo vas da prvo odaberete lokaciju sa koje naručujete.','Пожалуйста, выберите магазин');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('212','Allgemein','Allgemein','General','Opšti podaci','Opšti podaci','Opšti podaci','Генеральная');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('213','(Admin) DataConnector','(Admin) DataConnector','(Admin) DataConnector','(Admin) DataConnector','(Admin) DataConnector','(Admin) DataConnector','(Aдмин) DataConnector');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('214','DataConnector','DataConnector','DataConnector','DataConnector','DataConnector','DataConnector','DataConnector');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('215','Verbundene Nr.','Verbundene Nr.','Connected no.','Povezani broj','Povezani broj','Povezani broj','подключен нет.');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('216','Normal-Preis','Brutto','Gross','bruto','bruto','bruto','бруто');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('217','(Admin) WebService Mapping','(Admin) interne Nummern','(Admin) internal no.','(Admin) interni brojevi','(Admin) interni brojevi','(Admin) interni brojevi','(Aдмин) внутренние номера');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('218','Hersteller ID','Hersteller','Manufacturer','Proizvođač','Proizvođač','Proizvođač','производитель');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('219','ID','Nr.','No.','Broj','Broj','Broj','номера');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('220','Bestellung konnte nicht verarbeitet werden','Bestellung konnte nicht verarbeitet werden','Order can not be processed.','Porudžbenica je odbijena!','Porudžbenica je odbijena!','Porudžbenica je odbijena!','заказ отклонен');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('221','Bemerkung 2','Bemerkung 2','Comment 2','napomena 2','napomena 2','napomena 2','комментарий 2');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('222','Kategorie ID','Kagegorie','Categorie','kategorija','kategorija','kategorija','категория');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('223','Sub-Kategorie ID','Sub-Kategorie','Sub-Categorie','pod-kategorija','pod-kategorija','pod-kategorija','подкатегория');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('224','Sortierung','Sortierung','Sorting','sortiranje','sortiranje','sortiranje','сортировка');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('225','Bemerkung 3','Bemerkung 3','Comment 3','napomena 3','napomena 3','napomena 3','комментарий 3');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('226','Bild 2','Bild 2','Picture 2','slika 2','slika 2','slika 2','картина 2');" );
	$db->query( "REPLACE INTO CORE_LANGUAGE (id,search,de,en,sr,hr,ba,ru) VALUES ('227','Bild 3','Bild 3','Picture 3','slika 3','slika 3','slika 3','картина 3');" );
} // if

// End
$db->commit();
?>