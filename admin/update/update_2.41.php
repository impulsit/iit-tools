<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
//$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update

/*
$update->add_new_field_only_database( array( "table" => "CORE_USER_INFO", "field" => "language", "fieldtype" => "VARCHAR(10)" ) );
$update->add_new_field_only_database( array( "table" => "CORE_USER_INFO", "field" => "start_menu", "fieldtype" => "VARCHAR(10)" ) );
$update->add_new_field_only_database( array( "table" => "CORE_USER_INFO", "field" => "user_level", "fieldtype" => "INT" ) );
$update->add_new_field_only_database( array( "table" => "CORE_MAINMENU", "field" => "min_user_level", "fieldtype" => "INT" ) );

$db->query( "
	CREATE TABLE IF NOT EXISTS `CORE_LANGUAGE` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `search` varchar(250) NOT NULL,
	  `de` varchar(250) NOT NULL,
	  `en` varchar(250) NOT NULL,
	  `sr` varchar(250) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `search` (`search`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
" );

$db->query( "
	CREATE TABLE IF NOT EXISTS `CORE_USER_LEVEL` (
	`id` int(11) NOT NULL,
	`title` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
" );

$db->query( "
	INSERT INTO `CORE_USER_LEVEL` (`id`, `title`) VALUES
	(10, 'Benutzer Level 1'),
	(20, 'Benutzer Level 2'),
	(30, 'Benutzer Level 3'),
	(40, 'Benutzer Level 4'),
	(50, 'Benutzer Level 5'),
	(100, 'Projekt Admin'),
	(990, 'Datenbank Admin'),
	(1000, 'Super Admin');
" );
*/
// -----------------------------------------------------------------------------

// Update erfolgreich
//$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>