<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

$data = array( "table" => "CORE_SETUP", "field" => "version_type", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );

if( $f->project_allowed( 4 ) ) { // TecDoc
	$db->query( "ALTER TABLE `TEC_WARENKORB` CHANGE `location_id` `location_id` VARCHAR(30) NOT NULL;" );
} // if

// End
$db->commit();
?>