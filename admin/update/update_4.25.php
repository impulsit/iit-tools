<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 4 ) ) { // TecDoc
	$data = array( "table" => "CORE_USER_INFO", "field" => "search_show_attributes", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "search_only_on_stock", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
} // if

// End
$db->commit();
?>