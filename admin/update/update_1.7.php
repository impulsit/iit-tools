<?php
// Neue Option für Feldtypen
if( !$db->query( "SELECT type FROM CORE_FIELDTYPS WHERE type='select_function'" ) )
	$db->insert( "CORE_FIELDTYPS", array( "type" => "select_function" ) );

// Neues Feld in Teilnehmer für automatische Kurszuordnung
$db->query( "SELECT * FROM BAS_TEILNEHMER LIMIT 1" );
$r = $db->getNext();
if( !isset( $r['zu_kurs_zuordnen'] ) )
	$db->query( "ALTER TABLE BAS_TEILNEHMER ADD zu_kurs_zuordnen INT NOT NULL" );

// Teilnehmer Liste
$db->update( "CORE_LISTS", array( "list_query" =>
"SELECT t.*, g.title AS gesch_title, h.title AS herkunft_title, a.title AS anrede_title, k.name AS ams_betreuer_name, k2.name AS ams_amt_name, k3.nummer AS kurs_title
FROM BAS_TEILNEHMER AS t
LEFT JOIN BAS_HERKUNFT AS h ON (h.id=t.herkunft_id)
LEFT JOIN GLOBAL_GESCHLECHT AS g ON (g.id=t.geschlecht_id)
LEFT JOIN GLOBAL_ANREDE AS a ON (a.id=t.anrede_id)
LEFT JOIN BAS_AMS_KONTAKTE AS k ON (k.id=t.ams_betreuer_id)
LEFT JOIN BAS_AMS_KONTAKTE AS k2 ON (k2.id=t.ams_amt_id)
LEFT JOIN BAS_KURSE AS k3 ON (k3.kurs_id=t.zu_kurs_zuordnen)" ), "list_id='2'" );

// Teilnehmer: neues Feld
if( !$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE title LIKE 'zu Kurs zuordnen'" ) )
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`) VALUES
		('', 2, 54, 'select_function', 'zu Kurs zuordnen', 'kurs_title', 0, 'zu_kurs_zuordnen', 'SELECT k.kurs_id, CONCAT( k.nummer, '' ('', DATE_FORMAT( k.startdatum, ''%d.%m.%Y'' ), '' bis '', DATE_FORMAT( k.enddatum, ''%d.%m.%Y'' ), '')'' ) AS kurs_title<br>FROM BAS_KURSE AS k<br>WHERE k.enddatum+INTERVAL 30 DAY>=NOW()', 'kurs_id', 'save_kurs_zuordnung', '', '', 'k3.nummer', '', '', 0);
	" );

$db->commit();
?>