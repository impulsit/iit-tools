<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Neues Feld "E-Mail" bei Teilnehmer
$data = array(
	"db" => array(
		"table" => "BAS_TEILNEHMER",
		"fieldtype" => "VARCHAR(100)",
	),
	"field" => array(
		"list_id" => 2,
		"pos" => 135,
		"type" => "text",
		"title" => "E-Mail",
		"field" => "t.email",
		"save_field" => "email",
	),
);
$update->add_new_field( $data );

$db->commit();
?>