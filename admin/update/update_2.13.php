<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

$db->update( "BAS_REPORTS", array( "template" => 'classes/templates/reports_basilica/Fragebogen/' ), "title='Fragebogen'" );

// Fragebogen neues Feld Kurs ID
$data = array( "table" => "WHY_USER_FRAGEBOGEN", "field" => "kurs_id", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );

$db->commit();
?>