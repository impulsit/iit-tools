<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

// Neue User Listen
if( $f->project_allowed( 2 ) ) { // Basilica
	$list_id = 39;

	$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
	$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'Basilica - Benutzer', 2,
			'SELECT u.*, p.title AS project_title, ul.title AS user_level_title FROM CORE_USER_INFO AS u LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=u.start_menu) LEFT JOIN CORE_USER_LEVEL AS ul ON (ul.id=u.user_level)',
			'user_id ASC', 'user_id', 'CORE_USER_INFO', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");

	// Felder zu Benutzer
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  10, 'integer', 'User ID', 'user_id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  20, 'text', 'Login / E-Mail', 'email', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 1),
		('', ".$list_id.",  30, 'text', 'Name', 'name', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 1),
		('', ".$list_id.",  40, 'password', 'Passwort', 'password', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  50, 'checkbox', 'Register/TABs bei Liste', 'list_register', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  60, 'select', 'Rolle', 'user_level_title', 0, 'user_level', 'SELECT id, title AS user_level_title FROM CORE_USER_LEVEL', 'id', '', '', '', 'ul.title', '', '', 0, '', 0, 0),
		('', ".$list_id.",  70, 'select', 'Hauptmenü', 'project_title', 0, 'start_menu', 'SELECT project_id, title AS project_title FROM CORE_PROJECTS', 'project_id', '', '', '', 'p.title', '', '', 0, '', 0, 0),
		('', ".$list_id.",  80, 'date', 'läuft ab', 'valid_to', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 200, 'text', 'Winword', 'winword_path', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0)
	");
} // if

if( $f->project_allowed( 4 ) ) { // TecDoc
	$list_id = 40;

	$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'Basilica - Benutzer', 2,
			'SELECT u.*, p.title AS project_title, ul.title AS user_level_title FROM CORE_USER_INFO AS u LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=u.start_menu) LEFT JOIN CORE_USER_LEVEL AS ul ON (ul.id=u.user_level)',
			'user_id ASC', 'user_id', 'CORE_USER_INFO', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");

	// Felder zu Benutzer
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
		('', ".$list_id.",  10, 'integer', 'User ID', 'user_id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  20, 'text', 'Login / E-Mail', 'email', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 1),
		('', ".$list_id.",  30, 'text', 'Name', 'name', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 1),
		('', ".$list_id.",  40, 'password', 'Passwort', 'password', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  50, 'checkbox', 'Register/TABs bei Liste', 'list_register', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.",  60, 'select', 'Rolle', 'user_level_title', 0, 'user_level', 'SELECT id, title AS user_level_title FROM CORE_USER_LEVEL', 'id', '', '', '', 'ul.title', '', '', 0, '', 0, 0),
		('', ".$list_id.",  70, 'select', 'Hauptmenü', 'project_title', 0, 'start_menu', 'SELECT project_id, title AS project_title FROM CORE_PROJECTS', 'project_id', '', '', '', 'p.title', '', '', 0, '', 0, 0),
		('', ".$list_id.",  80, 'date', 'läuft ab', 'valid_to', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 400, 'text', 'TMWS Kundennummer', 'topmotive_kundennr', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
		('', ".$list_id.", 410, 'checkbox', 'FIN Suche nicht aktiv', 'vin_search_inactive', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0)
	");
} // if


// End
$db->commit();
?>