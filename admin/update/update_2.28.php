<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

$update->insert_record( array(
	"table" => "BAS_REPORTS",
	"check_field" => "id",
	"field" => array(
		"id" => 38,
		"title" => "Anmeldung Landschaftsgärtner",
		"template" => "classes/templates/reports_basilica/Anmeldung_Landschaftsgaertnerinnen.rtf",
		"report_constant" => "REP_AM_LW_GAERTNER"	) ) );

if( $db->query( "SELECT id FROM BAS_KURSTYP WHERE id='19'" ) ) {
	$db->update( "BAS_KURSTYP", array(
		"anmeldeformular" => 38 ), "id='19'" );
} else
	$update->insert_record( array(
		"table" => "BAS_KURSTYP",
		"check_field" => "id",
		"field" => array(
			"id" => 19,
			"title" => "LandschaftsgärtnerInnen mit LAP",
			"anmeldeformular" => 38,
			"kursbestaetigung" => 18,
			"weiterbildung_setzen" => 0	) ) );

// Dokumente
if( !$db->query( "SELECT list_id FROM CORE_LISTS WHERE list_id='27'" ) ) {
	$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`) VALUES
			(27, 'Global - Dokumente', 1, 'SELECT id, name, fullpath, filename, extension<br>FROM GLOBAL_DOKUMENTE', 'id ASC', 'id', 'GLOBAL_DOKUMENTE', 0, '', 0, 'admin/list_redirect.php', 0);
			" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`) VALUES
			(201, 27, 10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0),
			(202, 27, 20, 'text', 'Name', 'name', 0, '', '', '', '', '', '', '', '', '', 0),
			(203, 27, 30, 'select_file', 'Pfad', 'fullpath', 0, '', '', '', '', '', '', '', 'upload/', '*.*', 1),
			(204, 27, 40, 'text', 'Dateiname', 'filename', 0, '', '', '', '', '', '', '', '', '', 0),
			(205, 27, 50, 'text', 'Dateityp', 'extension', 0, '', '', '', '', '', '', '', '', '', 0),
			(206, 27, 60, 'function', 'Vorschau', '', 0, '', '', '', 'single_document_preview', '', '', '', '', '', 0);
			" );
	$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`) VALUES
			(25, 1, 'Dokumente', 'pics/sachgebiete.png', 'admin/list_redirect.php', 5, 27);
			" );
	$db->query( "
		CREATE TABLE IF NOT EXISTS `GLOBAL_DOKUMENTE` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`list_id` int(11) NOT NULL,
			`main_id` int(11) NOT NULL,
			`field_id` int(11) NOT NULL,
			`name` varchar(50) NOT NULL,
			`path` varchar(100) NOT NULL,
			`filename` varchar(100) NOT NULL,
			`fullpath` varchar(100) NOT NULL,
			`extension` varchar(10) NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
		" );
} // if

$db->commit();
?>