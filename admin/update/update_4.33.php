<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update

// Global
if( $f->project_allowed( 2 ) ) { // Basilica
	include( "basilica_at/update_4.33.php" );
} // if

if( $f->project_allowed( 3 ) ) { // Why
} // if

if( $f->project_allowed( 4 ) ) { // TecDoc
	include( "tecdocbase_atit_at/update_4.33.php" );
} // if

// End
$db->commit();
?>