<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// -----------------------------------------------------------------------------
// Update

$db->query( "
CREATE TABLE IF NOT EXISTS `TEC_ITEMS_CATEGORY` (
  `id` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
" );
$db->query( "
	ALTER TABLE `TEC_ITEMS_CATEGORY`
	MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
" );

$db->query( "
CREATE TABLE IF NOT EXISTS `TEC_ITEMS_SUBCATEGORY` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory` varchar(50) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
" );
$db->query( "
	ALTER TABLE `TEC_ITEMS_SUBCATEGORY`
	MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
" );

$data = array( "table" => "TEC_ITEMS", "field" => "category_id", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_ITEMS", "field" => "subcategory_id", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_ITEMS", "field" => "sorting", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_ITEMS", "field" => "description_3", "fieldtype" => "VARCHAR(80)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_ITEMS", "field" => "item_picture_2", "fieldtype" => "VARCHAR(80)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "TEC_ITEMS", "field" => "item_picture_3", "fieldtype" => "VARCHAR(80)" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "TEC_SEARCH_HISTORY", "field" => "filter_pskw", "fieldtype" => "VARCHAR(30)" );
$update->add_new_field_only_database( $data );

// End
$db->commit();
?>