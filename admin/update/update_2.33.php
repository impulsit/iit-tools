<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
//$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update
$db->query( "
	CREATE TABLE IF NOT EXISTS `CORE_UPDATES` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `update_file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	  `processed` tinyint(1) NOT NULL,
	  `installed` datetime NOT NULL,
	  PRIMARY KEY (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
" );

$file = array();
$handle = opendir( BASE_DIR.'admin/update/' );
while( $datei = readdir( $handle ) ) {
	if( (pathinfo( $datei, PATHINFO_EXTENSION) == "php") && ($datei != ".") && ($datei != "..") ) {
		$file[] = $datei;
	} // if
} // while

sort( $file );
foreach( $file as $k => $v ) {
	$update->write_change( basename( $v ) );
} // foreach
// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>