<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

$db->query( "
	CREATE TABLE IF NOT EXISTS `CORE_LANGUAGE_SETUP` (
	  `id` int(11) NOT NULL AUTO_INCREMENT,
	  `title` varchar(100) NOT NULL,
	  `layout_code` varchar(10) NOT NULL,
	  `tecdoc_code` varchar(10) NOT NULL,
	  `flag` varchar(100) NOT NULL,
	  `active` tinyint(4) NOT NULL,
  	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$list_id = 41;

$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
	(".$list_id.", 'Global - Sprachauswahl', 2,
		'SELECT * FROM CORE_LANGUAGE_SETUP',
		'id ASC', 'id', 'CORE_LANGUAGE_SETUP', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
");

// Felder zu Benutzer
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
	('', ".$list_id.",  10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
	('', ".$list_id.",  20, 'text', 'Sprache (in Landessprache)', 'title', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
	('', ".$list_id.",  30, 'text', 'Layout Code', 'layout_code', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
	('', ".$list_id.",  40, 'text', 'TecDoc Code', 'tecdoc_code', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0),
	('', ".$list_id.",  50, 'select_file', 'Fahne', 'flag', 0, '', '', '', '', '', '', '', 'themes/_default/icons/flags/', '*.png', 1, '', 0, 0),
	('', ".$list_id.",  60, 'checkbox', 'Aktiv', 'active', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0)
");

// Hauptmenü
$db->delete( "CORE_MAINMENU", "project_id='1' AND list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`) VALUES
	('', '1', 'Sprachauswahl', 'themes/_default/icons/sprache.png', 'admin/list_redirect.php', '20', '".$list_id."', '100');
");

// Default Insert
$db->query( "TRUNCATE CORE_LANGUAGE_SETUP" );
$db->query( "INSERT INTO `CORE_LANGUAGE_SETUP` (`id`, `title`, `layout_code`, `tecdoc_code`, `flag`, `active`) VALUES
	(1, 'deutsch', 'de', 'de', 'themes/_default/icons/flags/Austria.png', 1),
	(2, 'english', 'en', 'en', 'themes/_default/icons/flags/United-Kingdom.png', 1),
	(3, 'srpski', 'sr', 'sr', 'themes/_default/icons/flags/Serbia.png', 1),
	(4, 'hrvatski', 'hr', 'sr', 'themes/_default/icons/flags/Croatia.png', 1),
	(5, 'bosanski', 'ba', 'sr', 'themes/_default/icons/flags/Bosnia-and-Herzegovina.png', 1),
	(6, 'русский', 'ru', 'ru', 'themes/_default/icons/flags/Russia.png', 1),
	(7, 'slovenski', 'sl', 'sl', 'themes/_default/icons/flags/Slovenia.png', 1);
" );

if( $f->project_allowed( 4 ) ) { // TecDoc
	$data = array( "table" => "TEC_SETUP", "field" => "default_language", "fieldtype" => "VARCHAR(10)", "init_value" => "en", "where" => "1" );
	$update->add_new_field_only_database( $data );
} // if

// End
$db->commit();
?>