<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Druck Button Werkvertrag
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 33,
		"title" => "KURS_DRUCKEN_WERKVERTRAG",
		"description" => "Werkvertrag",
		"picture" => "pics/drucken.png",
		"project_id" => 2 ) ) );

// Druck Button Honorarnote
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 34,
		"title" => "KURS_DRUCKEN_HONORAR",
		"description" => "Honorarnote",
		"picture" => "pics/drucken.png",
		"project_id" => 2 ) ) );

// Werkvertrag Drucken Button zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 33,
		"list_id" => 1, // Kurse
		"pos" => 122 ) ) );

// Honorarnote Drucken Button zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 34,
		"list_id" => 1, // Kurse
		"pos" => 124 ) ) );

$db->commit();
?>