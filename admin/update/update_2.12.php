<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// WKW Schnittstelle
$data = array( "table" => "BAS_SETUP", "field" => "wkw_url", "fieldtype" => "VARCHAR(250)", "init_value" => "https://ebipol.wkw.at/Portal.aspx", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "wkw_login", "fieldtype" => "VARCHAR(250)", "init_value" => "office@basilica.at", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "wkw_passwort", "fieldtype" => "VARCHAR(250)", "init_value" => "Basilica1220$", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

// WKW erstellen Button
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 30,
		"title" => "KURS_WKW_ERSTELLEN",
		"description" => "WKW-Datei erstellen",
		"picture" => "pics/save.png",
		"project_id" => 2 ) ) );

// WKW erstellen Button zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 30,
		"list_id" => 1, // Kurse
		"pos" => 130 ) ) );

// Kurs Anzeige Button
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 31,
		"title" => "KURS_ANZEIGE_ALLE_LAUFEND",
		"description" => "Anzeige",
		"picture" => "pics/list.png",
		"project_id" => 2 ) ) );

// Kurs Anzeige Button zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 31,
		"list_id" => 1, // Kurse
		"pos" => 140,
		"main_button" => 1 ) ) );

$db->commit();
?>