<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update

$data = array( "table" => "CORE_SETUP", "field" => "color_theme", "fieldtype" => "TINYINT(4)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "CORE_SETUP", "field" => "color_theme_external_css", "fieldtype" => "VARCHAR(100)" );
$update->add_new_field_only_database( $data );

if( $f->project_allowed( 4 ) ) { // TecDoc
} // if

// End
$db->commit();
?>