<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update

// Global
$data = array( "table" => "CORE_SETUP", "field" => "checksum", "fieldtype" => "VARCHAR(50)" );
$update->add_new_field_only_database( $data );
$db->query( "UPDATE CORE_SETUP SET checksum=MD5(CONCAT('".MYSQL_DB."','_-')) WHERE id='1'" );

if( $f->project_allowed( 2 ) ) { // Basilica
	$db->query( "SELECT COUNT(*) AS anz FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema='".MYSQL_DB."' AND table_name='BAS_TRAINER_KURSE' AND index_name='kurs_id'" );
	$r = $db->getNext();
	if( $r['anz'] == 0 )
		$db->query( "ALTER TABLE `BAS_TRAINER_KURSE` ADD INDEX `kurs_id` (`kurs_id`);" );

	$db->query( "SELECT COUNT(*) AS anz FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema='".MYSQL_DB."' AND table_name='BAS_KURSTEILNEHMER' AND index_name='kurs_id'" );
	$r = $db->getNext();
	if( $r['anz'] == 0 )
		$db->query( "ALTER TABLE `BAS_KURSTEILNEHMER` ADD INDEX `kurs_id` (`kurs_id`);" );
} // if

if( $f->project_allowed( 3 ) ) { // Why
} // if

if( $f->project_allowed( 4 ) ) { // TecDoc
} // if

// End
$db->commit();
?>