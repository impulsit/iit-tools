<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 4 ) ) { // TecDoc
	$data = array( "table" => "TEC_WEBSERVICE_MAPPING", "field" => "td_status", "fieldtype" => "VARCHAR(30)" );
	$update->add_new_field_only_database( $data );
	
	$list_id = 36;
	$pos = 60;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'TD Status', 'td_status', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	
	$db->query( "
		CREATE TABLE IF NOT EXISTS `CORE_JOBS` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`description` varchar(30) NOT NULL,
			`start` tinyint(4) NOT NULL,
			`running` tinyint(4) NOT NULL,
			`finished` tinyint(4) NOT NULL,
			`start_time` datetime NOT NULL,
			`end_time` datetime NOT NULL,
			`status` varchar(250) NOT NULL,
			`counter` int(11) NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;" );
} // if

if( $f->project_allowed( 2 ) ) { // Basilica
	$db->update( "BAS_REPORTS", array( "template" => 'themes/basilica_at/reports/WAFF_teilnehmer_Zahlungungsbestaetigung.rtf' ), "id='24'" );
} // if

// End
$db->commit();
?>