<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update

// Benutzer neue Felder
$data = array( "table" => "CORE_USER_INFO", "field" => "list_register", "fieldtype" => "TINYINT(1)" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "CORE_USER_INFO", "field" => "winword_path", "fieldtype" => "VARCHAR(250)", "init_value" => "C:\Program Files (x86)\Microsoft Office\Office14\winword.exe", "where" => "1" );
$update->add_new_field_only_database( $data );

// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>