<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

$data = array(
	"db" => array(
		"table" => "BAS_KURSE",
		"fieldtype" => "DECIMAL(10,2)",
	),
	"field" => array(
		"list_id" => 1,
		"pos" => 260,
		"type" => "decimal",
		"title" => "abw. Trainer Stundensatz",
		"field" => "abw_trainer_stundensatz",
	),
);

$update->add_new_field( $data );

$db->commit();
?>