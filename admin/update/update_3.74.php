<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 2 ) ) { // Basilica
	$data = array( "table" => "BAS_TRAINER_KURSE", "field" => "pauschale", "fieldtype" => "decimal(10,2)" );
	$update->add_new_field_only_database( $data );
} // if

// End
$db->commit();
?>