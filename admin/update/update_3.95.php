<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 2 ) ) { // Basilica
	$data = array( "table" => "BAS_KURSTYP", "field" => "description", "fieldtype" => "MEDIUMTEXT" );
	$update->add_new_field_only_database( $data );

	$list_id = 8;
	$pos = 60;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'textarea', 'Beschreibung', 'description', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
} // if

// End
$db->commit();
?>