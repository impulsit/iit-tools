<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

if( $f->project_allowed( 2 ) ) { // Basilica
	// weitere Felder Teilnehmer
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "staatsbuergerschaft", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "fuehrerschein_nr", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "ausstellende_behoerde", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "adr_basis", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "adr_auffrischung", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "aufbau_tank", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "aufbau_klasse_1", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "aufbau_klasse_7", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "adr_gueltig_bis", "fieldtype" => "DATE" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "letzter_adr_kurs", "fieldtype" => "DATE" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "BAS_TEILNEHMER", "field" => "adr_erinnerung_gedruckt", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );

	$list_id = 2;
	$pos = 400; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'seperator', 'ADR Gefahrgut', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 410; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Staatsbürgerschaft', 'staatsbuergerschaft', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 420; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Nr. des Führerscheins', 'fuehrerschein_nr', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 430; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Ausstellende Behörde', 'ausstellende_behoerde', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 440; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'ADR Basis', 'adr_basis', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 450; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'ADR Auffrischung', 'adr_auffrischung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 460; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'Aufbau Tank', 'aufbau_tank', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 470; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'Aufbau Klasse 1', 'aufbau_klasse_1', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 480; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'Aufbau Klasse 7', 'aufbau_klasse_7', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 490; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'date', 'ADR Schein gültig bis', 'adr_gueltig_bis', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");
	$pos = 500; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'date', 'letzter ADR Kurs', 'letzter_adr_kurs', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");

	// Neues Template Teilnehmerliste ADR
	$db->query( "REPLACE INTO `BAS_REPORTS` (`id`, `title`, `template`, `report_constant`) VALUES ('60', 'ADR Teilnehmerliste', 'themes/basilica_at/reports/TeilnehmerinnenADR.rtf', 'REP_ADR_TEILNEHMERLISTE');" );

	// Kurstyp neues Feld für Erinnerungsbrief
	$data = array( "table" => "BAS_KURSTYP", "field" => "adr_weiterbildung_setzen", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );

	$list_id = 8;
	$pos = 55; $db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'bei Druck Weiterbildung (ADR)', 'adr_weiterbildung_setzen', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0); ");

	// Neues Template Erinnerungsbrief ADR
	$db->query( "REPLACE INTO `BAS_REPORTS` (`id`, `title`, `template`, `report_constant`) VALUES ('61', 'Erinnerungsbrief (ADR)', 'themes/basilica_at/reports/Erinnerungsbrief_ADR.rtf', 'REP_ERINNERUNGSBRIEF_ADR');" );

	// Setup neues Feld für Erinnerungszeitraum ADR
	$data = array( "table" => "BAS_SETUP", "field" => "adr_weiterbildung_monate", "fieldtype" => "INT(11)" );
	$update->add_new_field_only_database( $data );
} // if

// End
$db->commit();
?>