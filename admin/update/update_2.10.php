<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Update Fragenbogen Listen
$db->update( "CORE_LISTS_FIELDS", array( "select_query" => 'SELECT frage_id, kurz FROM WHY_FRAGEN' ), "field_id='126'" );

// Neues Feld bei Fragebogen
$data = array(
	"db" => array(
		"table" => "WHY_FRAGEBOGEN",
		"fieldtype" => "TINYINT(1)",
	),
	"field" => array(
		"list_id" => 13,
		"pos" => 55,
		"type" => "checkbox",
		"title" => "Verbindung Kurse",
		"field" => "link_kurs",
		"in_kurz" => 1,
	),
);
$update->add_new_field( $data );

// Neue Fragebogen Auswertung
$update->insert_record( array(
	"table" => "WHY_FRAGEBOGEN_TYP",
	"check_field" => "id",
	"field" => array(
		"id" => 4,
		"title" => "keine Auswertung" ) ) );

// Druck Button
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 29,
		"title" => "WHY_FB_DRUCKEN_KURSE",
		"description" => "Drucken",
		"picture" => "pics/drucken.png",
		"project_id" => 3 ) ) );

// Druck Button zuordnen zu Fragebogen
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 29,
		"list_id" => 13,
		"pos" => 60 ) ) );

// neuer Bericht für Fragebogen
$update->insert_record( array(
	"table" => "BAS_REPORTS",
	"check_field" => "id",
	"field" => array(
		"id" => 37,
		"title" => "Fragebogen",
		"template" => "classes/templates/reports_basilica/Fragebogen.rtf",
		"report_constant" => "REP_FRAGEBOGEN" ) ) );

// Neues Feld für Fragenbogen bei Druck
$data = array( "table" => "BAS_LOG_FILES", "field" => "fragebogen_id", "fieldtype" => "INT(11)" );
$update->add_new_field_only_database( $data );

// Neues Feld bei Fragen
$data = array(
	"db" => array(
		"table" => "WHY_FRAGEN",
		"fieldtype" => "TINYINT(1)",
	),
	"field" => array(
		"list_id" => 15,
		"pos" => 75,
		"type" => "checkbox",
		"title" => "Trainerfrage",
		"field" => "trainer_frage",
	),
);
$update->add_new_field( $data );

$db->commit();
?>