<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

global $_location;

// -----------------------------------------------------------------------------
// Update

// Global
$data = array( "table" => "CORE_SETUP", "field" => "default_language", "fieldtype" => "VARCHAR(10)" );
$update->add_new_field_only_database( $data );
$db->query( "SELECT default_language FROM TEC_SETUP WHERE id='1'" );
$r = $db->getNext();
$db->query( "UPDATE CORE_SETUP SET default_language='".$r['default_language']."' WHERE id='1'" );

if( $f->project_allowed( 2 ) ) { // Basilica
} // if

if( $f->project_allowed( 3 ) ) { // Why
} // if

if( $f->project_allowed( 4 ) ) { // TecDoc
} // if

// End
$db->commit();
?>