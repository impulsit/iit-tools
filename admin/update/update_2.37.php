<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update

$data = array( "table" => "BAS_TRAINER_KURSE", "field" => "manuelle_stunden", "fieldtype" => "decimal(10,2)" );
$update->add_new_field_only_database( $data );

// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>