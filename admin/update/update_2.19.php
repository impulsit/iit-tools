<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Neue Liste Kurse anzeigen
$list_id = 25;

$update->insert_record( array(
	"table" => "CORE_LISTS",
	"check_field" => "list_id",
	"field" => array(
		"list_id" => $list_id,
		"title" => "Basilica - Kurse anzeigen",
		"project_id" => 2,
		"list_query" => "SELECT t.*, z.nummer AS kurs_name, s.title AS status_title, CONCAT( u.nachname, \' \', u.vorname ) AS teilnehmer_name FROM BAS_KURSTEILNEHMER AS t LEFT JOIN BAS_KURSE AS z ON (z.kurs_id=t.kurs_id) LEFT JOIN BAS_TEILNEHMER AS u ON (u.teilnehmer_id=t.teilnehmer_id) LEFT JOIN BAS_STATUS AS s ON (s.id=t.status_id)",
		"sort_order" => "kursteilnehmer_id ASC",
		"delete_primary_key" => "kursteilnehmer_id",
		"delete_table" => "BAS_KURSTEILNEHMER",
		"switchable" => 0,
		"back_up_file" => "admin/list_redirect.php",
		"back_up_list" => 2,
		"file" => "admin/list_redirect.php",
		"lines_selectable" => 0	) ) );

$update->insert_record( array(
	"table" => "CORE_LISTS_FIELDS",
	"check_field" => "field_id",
	"field" => array(
		"field_id" => 185,
		"list_id" => $list_id,
		"pos" => 10,
		"type" => "integer",
		"title" => "Kursteilnehmer ID",
		"field" => "kursteilnehmer_id",
		"in_kurz" => 0,
		"save_field" => "",
		"select_query" => "",
		"select_id" => "",
		"function" => "",
		"fill_fix_type" => "",
		"fill_fix_param" => "",
		"where_field" => "",
		"path" => "",
		"pattern" => "",
		"show_picture" => "" ) ) );
$update->insert_record( array(
	"table" => "CORE_LISTS_FIELDS",
	"check_field" => "field_id",
	"field" => array(
		"field_id" => 189,
		"list_id" => $list_id,
		"pos" => 20,
		"type" => "select",
		"title" => "Kurs",
		"field" => "kurs_name",
		"in_kurz" => 0,
		"save_field" => "kurs_id",
		"select_query" => "SELECT kurs_id, CONCAT( nummer, \' (\', DATE_FORMAT(startdatum, \'%d.%m.%Y\'), \' bis \', DATE_FORMAT(enddatum, \'%d.%m.%Y\'), \')\' ) AS kurs_name FROM BAS_KURSE ORDER BY startdatum DESC",
		"select_id" => "kurs_id",
		"function" => "",
		"fill_fix_type" => "",
		"fill_fix_param" => "",
		"where_field" => "",
		"path" => "",
		"pattern" => "",
		"show_picture" => "" ) ) );
$update->insert_record( array(
	"table" => "CORE_LISTS_FIELDS",
	"check_field" => "field_id",
	"field" => array(
		"field_id" => 187,
		"list_id" => $list_id,
		"pos" => 30,
		"type" => "select",
		"title" => "Status",
		"field" => "status_title",
		"in_kurz" => 0,
		"save_field" => "status_id",
		"select_query" => "SELECT id, title AS status_title FROM BAS_STATUS ORDER BY title",
		"select_id" => "id",
		"function" => "",
		"fill_fix_type" => "",
		"fill_fix_param" => "",
		"where_field" => "s.title",
		"path" => "",
		"pattern" => "",
		"show_picture" => "" ) ) );
$update->insert_record( array(
	"table" => "CORE_LISTS_FIELDS",
	"check_field" => "field_id",
	"field" => array(
		"field_id" => 188,
		"list_id" => $list_id,
		"pos" => 40,
		"type" => "date",
		"title" => "bezahlt am",
		"field" => "bezahlt_am",
		"in_kurz" => 0,
		"save_field" => "",
		"select_query" => "",
		"select_id" => "",
		"function" => "",
		"fill_fix_type" => "",
		"fill_fix_param" => "",
		"where_field" => "",
		"path" => "",
		"pattern" => "",
		"show_picture" => "" ) ) );

$update->insert_record( array(
	"table" => "CORE_LISTS_FIELDS",
	"check_field" => "field_id",
	"field" => array(
		"field_id" => 186,
		"list_id" => $list_id,
		"pos" => 50,
		"type" => "select",
		"title" => "Teilnehmer",
		"field" => "teilnehmer_name",
		"in_kurz" => 0,
		"save_field" => "teilnehmer_id",
		"select_query" => "SELECT teilnehmer_id, CONCAT( nachname, \' \', vorname ) AS teilnehmer_name FROM BAS_TEILNEHMER",
		"select_id" => "teilnehmer_id",
		"function" => "",
		"fill_fix_type" => "PARAMETER",
		"fill_fix_param" => "t.teilnehmer_id",
		"where_field" => "u.teilnehmer_id",
		"path" => "",
		"pattern" => "",
		"show_picture" => "" ) ) );

// Kurse anzeigen Button
$button_id = 35;

$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => $button_id,
		"title" => "TN_KURSE_ANZEIGEN",
		"description" => "Kurse anzeigen",
		"picture" => "pics/kurse.png",
		"project_id" => 2 ) ) );

// Kurse anzeigen Button zuordnen zu Teilnehmer
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => $button_id,
		"list_id" => 2, // Teilnehmer
		"pos" => 17,
		"file" => 'admin/list_redirect.php',
		"file_list_id" => $list_id	)	) );

$db->commit();
?>