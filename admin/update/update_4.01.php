<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR.'functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();
$f = functions::getInstance();

// -----------------------------------------------------------------------------
// Update

// Benutzerrechte anpassen
$db->query( "TRUNCATE CORE_USER_LEVEL" );
$db->query( "
	INSERT INTO `CORE_USER_LEVEL` (`id`, `title`) VALUES
		(10, 'Benutzer (10)'),
		(50, 'Sub Admin (50)'),
		(100, 'Projekt Admin (100)'),
		(990, 'Datenbank Admin (990)'),
		(1000, 'Super Admin (1000)');
" );

$data = array( "table" => "CORE_SETUP", "field" => "email_adress", "fieldtype" => "VARCHAR(50)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "CORE_SETUP", "field" => "email_smtp_port", "fieldtype" => "VARCHAR(50)" );
$update->add_new_field_only_database( $data );
$data = array( "table" => "CORE_SETUP", "field" => "email_smtp_secure", "fieldtype" => "VARCHAR(50)" );
$update->add_new_field_only_database( $data );
$db->query( "UPDATE CORE_SETUP SET email_adress=email_smtp_user, email_smtp_port=25 WHERE id='1'" );

if( $f->project_allowed( 4 ) ) { // TecDoc
	// Warenkorb
	$data = array( "table" => "TEC_WARENKORB", "field" => "sub_shop", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "TEC_BESTELLUNG", "field" => "sub_shop", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );

	// Einstellungen
	$data = array( "table" => "TEC_SETUP", "field" => "sub_module_active", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "TEC_SETUP", "field" => "sub_admin_email", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );

	// immer sichtbar
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_first_name", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_last_name", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_adress", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_phone", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_email", "fieldtype" => "VARCHAR(50)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_created_by_user_id", "fieldtype" => "INT(11)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_created_at", "fieldtype" => "DATETIME" );
	$update->add_new_field_only_database( $data );

	$list_id = 34;

	$db->update( "CORE_LISTS", array( "list_query" => 'SELECT u.*, ul.title AS user_level_title, user.email AS sub_created_by_user_id_email<br>FROM CORE_USER_INFO AS u<br>LEFT JOIN CORE_USER_LEVEL AS ul ON (ul.id=u.user_level)<br>LEFT JOIN CORE_USER_INFO AS user ON (user.user_id=u.sub_created_by_user_id)' ), "list_id='".$list_id."'" );

	$pos = 100;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Vorname', 'sub_first_name', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 110;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Nachname', 'sub_last_name', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 120;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Adresse', 'sub_adress', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 130;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Telefon', 'sub_phone', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 140;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Admin E-Mail', 'sub_email', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 150;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'select', 'Erstellt von', 'sub_created_by_user_id_email', 0, 'sub_created_by_user_id', 'SELECT user_id, email AS sub_created_by_user_id_email FROM CORE_USER_INFO', 'user_id', '', '', '', 'user.email', '', '', 0, '', 1, 0);
		");
	$pos = 160;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'datetime', 'Erstellt am', 'sub_created_at', 0, '', '', '', '', '', '', '', '', '', 0, '', 1, 0);
		");

	// für Superadmin sichtbar
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_admin", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_ws_link", "fieldtype" => "VARCHAR(250)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_ws_location", "fieldtype" => "VARCHAR(250)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_language", "fieldtype" => "VARCHAR(10)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_logo", "fieldtype" => "VARCHAR(250)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_price_leader", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_order_leader", "fieldtype" => "TINYINT(4)" );
	$update->add_new_field_only_database( $data );
	$data = array( "table" => "CORE_USER_INFO", "field" => "sub_vat_percent", "fieldtype" => "INT(11)" );
	$update->add_new_field_only_database( $data );

	$list_id = 40;

	$db->update( "CORE_LISTS", array( "title" => 'TecDoc - Benutzer', "project_id" => '4', "list_query" => '
SELECT u.*, p.title AS project_title, ul.title AS user_level_title, user.email AS sub_created_by_user_id_email<br>
FROM CORE_USER_INFO AS u<br>
LEFT JOIN CORE_PROJECTS AS p ON (p.project_id=u.start_menu)<br>
LEFT JOIN CORE_USER_LEVEL AS ul ON (ul.id=u.user_level)<br>
LEFT JOIN CORE_USER_INFO AS user ON (user.user_id=u.sub_created_by_user_id)
' ), "list_id='".$list_id."'" );

	$pos = 100;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Vorname', 'sub_first_name', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 110;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Nachname', 'sub_last_name', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 120;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Adresse', 'sub_adress', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 130;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Telefon', 'sub_phone', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 140;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Admin E-Mail', 'sub_email', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 150;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'select', 'Erstellt von', 'sub_created_by_user_id_email', 0, 'sub_created_by_user_id', 'SELECT user_id, email AS sub_created_by_user_id_email FROM CORE_USER_INFO', 'user_id', '', '', '', 'user.email', '', '', 0, '', 0, 0);
		");
	$pos = 160;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'datetime', 'Erstellt am', 'sub_created_at', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");

	$pos = 500;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'Sub Admin', 'sub_admin', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 510;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'WebService URL', 'sub_ws_link', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 520;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'WebService Location', 'sub_ws_location', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 530;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'text', 'Suche language', 'sub_language', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 540;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'select_file', 'Logo', 'sub_logo', 0, '', '', '', '', '', '', '', 'themes/tecdocbase_atit_at/logo/', '*.*', 1, '', 0, 0);
		");
	$pos = 550;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'Preis führend', 'sub_price_leader', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 560;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'checkbox', 'Bestellführend', 'sub_order_leader', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");
	$pos = 570;
	$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."' AND pos='".$pos."'" );
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`) VALUES
			('', ".$list_id.", ".$pos.", 'integer', 'MwSt %', 'sub_vat_percent', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0);
		");

	// Liste Benutzer Rechte anpassen
	$db->update( "CORE_MAINMENU", array( "min_user_level" => 50 ), "list_id='34'" );

	$list_id = 34;
	$db->query( "UPDATE CORE_LISTS_FIELDS SET where_field=CONCAT('u.', field) WHERE list_id='".$list_id."' AND where_field=''");

	$list_id = 40;
	$db->query( "UPDATE CORE_LISTS_FIELDS SET where_field=CONCAT('u.', field) WHERE list_id='".$list_id."' AND where_field=''");

} // if

// End
$db->commit();
?>