<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Wordpress
$data = array( "table" => "BAS_SETUP", "field" => "wordpress_api_password", "fieldtype" => "VARCHAR(50)", "init_value" => "Basilica1220", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "wordpress_delete", "fieldtype" => "VARCHAR(250)", "init_value" => "http://basilica.at99.at/wp-content/themes/basilica/sync_kurse/delete_data.php", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "wordpress_add", "fieldtype" => "VARCHAR(250)", "init_value" => "http://basilica.at99.at/wp-content/themes/basilica/sync_kurse/add_kurs.php", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

// AMS Schnittstelle
$data = array( "table" => "BAS_SETUP", "field" => "ams_url", "fieldtype" => "VARCHAR(250)", "init_value" => "http://admin.weiterbildungsdatenbank.at/admin/", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "ams_login", "fieldtype" => "VARCHAR(250)", "init_value" => "basilica", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "ams_passwort", "fieldtype" => "VARCHAR(250)", "init_value" => "iegz0o1s", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

// Waff Schnittstelle
$data = array( "table" => "BAS_SETUP", "field" => "waff_url", "fieldtype" => "VARCHAR(250)", "init_value" => "http://admin.weiterbildung.at/backoffice/", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "waff_login", "fieldtype" => "VARCHAR(250)", "init_value" => "basilica22", "where" => "id='1'" );
$update->add_new_field_only_database( $data );

$data = array( "table" => "BAS_SETUP", "field" => "waff_passwort", "fieldtype" => "VARCHAR(250)", "init_value" => "basilica", "where" => "id='1'" );
$update->add_new_field_only_database( $data );


$db->commit();
?>