<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );

$db = mysql::getInstance();

// Bericht Teilnehmerinnenliste
//-----------------------------

// Neuer Action Button
if( $db->query( "SELECT button_id FROM CORE_ACTION_BUTTONS WHERE title LIKE 'KURS_TEILNEHMERLISTE'" ) ) {
	$r = $db->getNext();

	$i = $r['button_id'];
} else
	$i = $db->insert( "CORE_ACTION_BUTTONS", array( "title" => "KURS_TEILNEHMERLISTE", "project_id" => 2 ) );

// Button bei Liste Kurs
if( !$db->query( "SELECT list_button_id FROM CORE_LISTS_BUTTONS WHERE button_id='".$i."' AND list_id='1'" ) )
	$db->insert( "CORE_LISTS_BUTTONS", array( "button_id" => $i, "list_id" => 1, "pos" => 120 ) );

// Liste Berichte
if( !$db->query( "SELECT id FROM BAS_REPORTS WHERE report_constant LIKE 'REP_TEILNEHMERLISTE'" ) )
	$db->insert( "BAS_REPORTS", array( "title" => "Teilnehmerliste", "template" => "classes/templates/reports_basilica/Teilnehmerinnenliste.rtf", "report_constant" => "REP_TEILNEHMERLISTE" ) );

// Feldlängen Korrektur
//---------------------

// Feldtyp ändern
$db->query( "ALTER TABLE `CORE_LISTS_FIELDS` CHANGE `select_query` `select_query` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;" );

// AMS Feld "Amt"
//---------------

$db->query( "SELECT * FROM BAS_AMS_KONTAKTE LIMIT 1" );
$r = $db->getNext();
if( !isset( $r['amt'] ) )
	$db->query( "ALTER TABLE `BAS_AMS_KONTAKTE` ADD `amt` TINYINT( 1 ) NOT NULL;" );

if( !$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE title LIKE 'Amt' AND list_id='10'" ) )
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`) VALUES
		('', 10, 10, 'checkbox', 'Amt', 'amt');
	" );

// Betreuer
$db->update( "CORE_LISTS_FIELDS", array( "select_query" => "SELECT id, name AS ams_betreuer_name FROM BAS_AMS_KONTAKTE WHERE amt='0' ORDER BY name" ), "field_id='77'" );

// Amt
$db->update( "CORE_LISTS_FIELDS", array( "select_query" => "SELECT id, name AS ams_amt_name FROM BAS_AMS_KONTAKTE WHERE amt='1' ORDER BY name" ), "field_id='141'" );

// Kurs neues Feld "Stunden pro Tag"
//----------------------------------

$db->query( "SELECT * FROM BAS_KURSE LIMIT 1" );
$r = $db->getNext();
if( !isset( $r['stunden_pro_tag'] ) )
	$db->query( "ALTER TABLE `BAS_KURSE` ADD `stunden_pro_tag` INT( 11 ) NOT NULL;" );

if( !$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE title LIKE 'Stunden pro Tag' AND list_id='1'" ) )
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`) VALUES
		('', 1, 55, 'integer', 'Stunden pro Tag', 'stunden_pro_tag');
	" );

if( !$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE title LIKE 'Saldo' AND list_id='1'" ) )
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `in_kurz`, `function`) VALUES
		('', 1, 285, 'function', 'Saldo', '1', 'calc_kurs_saldo');
	" );

// Teilnehmer neues Feld "Adresskopf"
//-----------------------------------
function renumber_pos( $list_id ) {
	global $db;

	$i = 0;
	$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE list_id='".$list_id."' ORDER BY pos ASC", "renumber_pos" );
	while( $db->isNext( "renumber_pos" ) ) {
		$r = $db->getNext( "renumber_pos" );

		$i += 10;

		$db->update( "CORE_LISTS_FIELDS", array( "pos" => $i ), "field_id='".$r['field_id']."'" );
	} // while
	$db->commit();
} // renumber_pos

$db->query( "SELECT * FROM BAS_TEILNEHMER LIMIT 1" );
$r = $db->getNext();
if( !isset( $r['adresskopf'] ) )
	$db->query( "ALTER TABLE `BAS_TEILNEHMER` ADD `adresskopf` MEDIUMTEXT NOT NULL;" );

renumber_pos( 2 );

if( !$db->query( "SELECT field_id FROM CORE_LISTS_FIELDS WHERE title LIKE 'Adresskopf' AND list_id='2'" ) )
	$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`) VALUES
		('', 2, 270, 'textarea', 'Adresskopf', 'adresskopf');
	" );

$db->commit();
?>