<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update

// Neuer Bericht Protokoll Kran Stapler
$update->insert_record( array(
	"table" => "BAS_REPORTS",
	"check_field" => "id",
	"field" => array(
		"id" => 44,
		"title" => "Protokoll Kran/Stapler",
		"template" => "classes/templates/reports_basilica/Protokoll_Stapler_Kran.rtf",
		"report_constant" => "REP_PROT_KRAN_STAPLER"	) ) );

// Button Protokoll Kran Stapler
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 37,
		"title" => "KURS_PROT_KRAN_STAPLER",
		"description" => "Anwesenheitsliste/Protokoll Kran/Stapler",
		"picture" => "pics/drucken.png",
		"project_id" => 2 ) ) );

// Button Protokoll Kran Stapler zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 37,
		"list_id" => 1, // Kurse
		"pos" => 65 ) ) );

// Neuer Bericht Teilnehmerliste 2
$update->insert_record( array(
	"table" => "BAS_REPORTS",
	"check_field" => "id",
	"field" => array(
		"id" => 45,
		"title" => "Teilnehmerliste 2",
		"template" => "classes/templates/reports_basilica/Teilnehmerinnenliste_2.rtf",
		"report_constant" => "REP_TEILNEHMERLISTE_2"	) ) );

// Button Teilnehmerliste 2
$update->insert_record( array(
	"table" => "CORE_ACTION_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"button_id" => 36,
		"title" => "KURS_TEILNEHMERLISTE_2",
		"description" => "Teilnehmerliste 2",
		"picture" => "pics/drucken.png",
		"project_id" => 2 ) ) );

// Button Teilnehmerliste 2 zuordnen zu Kurs
$update->insert_record( array(
	"table" => "CORE_LISTS_BUTTONS",
	"check_field" => "button_id",
	"field" => array(
		"list_button_id" => '',
		"button_id" => 36,
		"list_id" => 1, // Kurse
		"pos" => 121 ) ) );

// Liste Kursteilnehmer neue Spalte Herkunft vom Teilnehmer
$db->update( "CORE_LISTS", array( "list_query" =>
"SELECT t.*, z.nummer AS kurs_nummer, s.title AS status_title, CONCAT( u.nachname, ' ', u.vorname ) AS teilnehmer_name, h.title AS teilnehmer_herkunft ".
"FROM BAS_KURSTEILNEHMER AS t ".
"LEFT JOIN BAS_KURSE AS z ON (z.kurs_id=t.kurs_id) ".
"LEFT JOIN BAS_TEILNEHMER AS u ON (u.teilnehmer_id=t.teilnehmer_id) ".
"LEFT JOIN BAS_STATUS AS s ON (s.id=t.status_id) ".
"LEFT JOIN BAS_HERKUNFT AS h ON (h.id=u.herkunft_id)" ), "list_id='3'" );

// Neu nummerieren Kursteilnehmer
$update->renumber_pos( 3 );

// Neuer Feldtyp
if( !$db->query( "SELECT type FROM CORE_FIELDTYPS WHERE type='lookup'" ) )
	$db->insert( "CORE_FIELDTYPS", array( "type" => "lookup" ) );

// Neues Feld Herkunft vom Teilnehmer
$data = array(
	"field" => array(
		"list_id" => 3,
		"pos" => 25,
		"type" => "lookup",
		"title" => "Herkunft",
		"field" => "teilnehmer_herkunft"
	),
);
$update->add_new_field( $data );

// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>