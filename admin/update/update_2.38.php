<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// Prüfe vorherige Updates
$update->check_updates_to_process();

// -----------------------------------------------------------------------------
// Update

$update->new_main_menu_button( 2, 38, "Statistik", "pics/statistik.png", "admin/bas_statistics.php", 0 );

// -----------------------------------------------------------------------------

// Update erfolgreich
$update->write_change( basename( __FILE__ ) );

// End
$db->commit();
?>