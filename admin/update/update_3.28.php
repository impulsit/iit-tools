<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );

$db = mysql::getInstance();

// -----------------------------------------------------------------------------
// Update

// neue Tabelle
$db->query( "
CREATE TABLE IF NOT EXISTS `TEC_SEARCH_HISTORY` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `act_time` datetime NOT NULL,
  `search_manufacturer` int(11) NOT NULL,
  `year_from` int(11) NOT NULL,
  `search_model` int(11) NOT NULL,
  `filter_fueltype` varchar(30) NOT NULL,
  `filter_motorcode` varchar(30) NOT NULL,
  `search_vehicle` int(11) NOT NULL,
  `all_manufacturers` tinyint(4) NOT NULL,
  `text` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
" );

// Wirft Warnung falls Index nicht existiert
$db->query( "
ALTER TABLE CORE_USER_INFO DROP INDEX email;
" );

$db->query( "
ALTER TABLE `CORE_USER_INFO` ADD UNIQUE (email);
" );

// -----------------------------------------------------------------------------

// End
$db->commit();
?>