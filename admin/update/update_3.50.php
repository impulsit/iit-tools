<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'update_functions.php' );

$db = mysql::getInstance();
$update = update::getInstance();

// -----------------------------------------------------------------------------
// Update

// neue Felder Kursteilnehmer
$data = array( "table" => "TEC_SETUP", "field" => "enable_menu_close", "fieldtype" => "TINYINT(4)" );
$update->add_new_field_only_database( $data );

// End
$db->commit();
?>