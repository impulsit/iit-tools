<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

if( isset( $_GET['indiv_project_id'] ) )
	$_SESSION['project_id'] = $_GET['indiv_project_id'];

if( isset( $_GET['indiv_list_id'] ) )
	$_SESSION['list_id'] = $_GET['indiv_list_id'];

$_LISTEN[$_SESSION['list_id']] = $list->load_list( $_SESSION['list_id'] );

// Setze "hoch"
$arr = $_LISTEN[$_SESSION['list_id']];
if( $arr['back_up_file'] != "" ) {
	$file = $arr['back_up_file'];
	if( $arr['back_up_list'] != 0 )
		$file .= '?indiv_list_id='.$arr['back_up_list'];

	$f->set_back_up_file = $file;
} // if

// WKW-Datei erstellen für Kurse
if( isset( $_GET['indiv_create_wkw'] ) ) {
	$interface->create_wkw_file( $_GET['indiv_create_wkw'] );
	$interface->open_wkw_file();
} // if

if( isset( $_GET['indiv_export_excel'] ) ) {
	$interface->TD_create_excel( $_SESSION['list_id'] );
	$interface->TD_open_excel_file();
} // if

require_once( CLASS_DIR."templates/header.php" );

if( $mes->beforeOutputNoError() ) {
	$list->set_list( $_SESSION['list_id'] );
	$list->parse_commands();
	$list->print_menu();
	$list->print_list();
} else
	echo $mes->getErrorMessages();

require_once( CLASS_DIR."templates/footer.php" );
?>