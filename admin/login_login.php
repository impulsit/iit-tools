<?php
if( !isset($_SESSION) ) session_start();

unset( $_SESSION['project_id'] );

require_once( dirname( __FILE__ )."/../classes/config_data.php" );
require_once( CLASS_DIR."mysql.php" );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'theme_functions.php' );
require_once( CLASS_DIR.'tecdoc.php' );

$db = mysql::getInstance();
$f = functions::getInstance();
$mes = messages::getInstance();
$t = translate::getInstance();
$theme = theme_functions::getInstance();
$tec = tecdoc::getInstance();

// Bereits eingeloggt?
if( isset( $_SESSION['c_id'] ) && $db->query( "
	SELECT
		user_id
	FROM CORE_USER_INFO
	WHERE session_id='".$_SESSION['c_id']."' AND ip='".$_SERVER['REMOTE_ADDR']."'" ) ) {
		header( "location: /".SUBDIR."admin/core_backend.php" );
		exit;
} // if

$t->update_display_language();

if( isset( $_GET['login'] ) || isset( $_POST['email'] ) ) {
	if( $f->login_user() ) {
		header( "location: /".SUBDIR."admin/core_backend.php" );
		exit;
	} // if
} // if

if( isset( $_GET['error'] ) ) {
	switch( $_GET['error'] ) {
		case 2: $str = 'Session konnte nicht bestimmt werden, bitte aktivieren sie Cookies und loggen Sie erneut ein.'; break;
		case 3: $str = 'Session ist nicht mehr gültig, bitte loggen Sie erneut ein.'; break;
		default: $str = 'unbekannter Fehler: '.$_GET['error'];
	} // switch
	$mes->addError( $str );
} // if

global $_location;

$location = $_location;
if( defined( "SIMULATE_LOCATION" ) )
	$location  = SIMULATE_LOCATION;

if( strpos( $location, "_atit_at" ) === false ) {
	// Default Header
	require_once( BASE_DIR."themes/_default/classes/login.php" );
} else {
	$_SESSION['project_id'] = 4;

	// TecDoc Header
	require_once( BASE_DIR."themes/tecdocbase_atit_at/classes/login.php" );
} // else

unset( $_SESSION['language'] );
?>
