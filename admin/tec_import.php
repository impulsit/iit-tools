<?php
ini_set( "memory_limit", "2048M" );

require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );
require_once( CLASS_DIR.'PHPExcel/PHPExcel.php' );

unset( $_SESSION['list_id'] );

if( isset( $_POST['upload'] ) ) {
	$dest_filename = $f->clean_filename( $_FILES['datei']['name'] );

	if( $dest_filename != "" ) {
		if( !is_dir( BASE_DIR.'upload' ) ) mkdir( BASE_DIR.'upload', 0755 );

		move_uploaded_file( $_FILES['datei']['tmp_name'], BASE_DIR.'upload/'.$dest_filename );

		$objPHPExcel = PHPExcel_IOFactory::load( BASE_DIR.'upload/'.$dest_filename );

		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		$rowData = $sheet->rangeToArray( 'A4:'.$highestColumn.'4', NULL, TRUE, FALSE );
		$max = 0;
		for( $i=0; $i<=20; $i++ ) {
			if( isset( $rowData[0][$i] ) && ($rowData[0][$i] != "") )
				$max = $i;
		} // for

		$rowData = $sheet->rangeToArray( 'A1:A1', NULL, TRUE, FALSE );
		$type = 0;
		switch( $rowData[0][0] ) {
			case "ITEMS": $type = 1; break;
			case "SUBSTITUTES": if( ($max+1) == 5 ) $type = 2; break;
			case "WEBSERVICEMAPPING": if( ($max+1) == 5 ) $type = 3; break;
			case "DATACONNECTOR": if( ($max+1) == 3 ) $type = 4; break;
		} // switch

		$db->delete( "TEC_IMPORT_LOG", "filename='".$dest_filename."'");
		$db->insert( "TEC_IMPORT_LOG", array( "filename" => $dest_filename, "type" => $type, "max_lines" => $highestRow, "max_cols" => $max+1 ) );
		$db->commit();
	} // if
} // if

if( isset( $_POST['upload_item'] ) ) {
	$dest_filename = $f->clean_filename( $_FILES['datei']['name'] );

	if( $dest_filename != "" ) {
		move_uploaded_file( $_FILES['datei']['tmp_name'], BASE_DIR.'upload/'.$dest_filename );

		$db->insert( "TEC_IMPORT_LOG", array( "filename" => $dest_filename, "type" => 1 ) );
		$db->commit();
	} // if
} // if

if( isset( $_POST['upload_item_replace'] ) ) {
	$dest_filename = $f->clean_filename( $_FILES['datei']['name'] );

	if( $dest_filename != "" ) {
		move_uploaded_file( $_FILES['datei']['tmp_name'], BASE_DIR.'upload/'.$dest_filename );

		$db->insert( "TEC_IMPORT_LOG", array( "filename" => $dest_filename, "type" => 2 ) );
		$db->commit();
	} // if
} // if

if( isset( $_GET['del'] ) ) {
	$db->query( "SELECT filename FROM TEC_IMPORT_LOG WHERE id='".$_GET['del']."'");
	$r = $db->getNext();

	if( file_exists( BASE_DIR.'upload/'.$r['filename'] ) )
		@unlink( BASE_DIR.'upload/'.$r['filename'] );

	$db->delete( "TEC_IMPORT_LOG", "id='".$_GET['del']."'" );
	$db->commit();
} // if

if( isset( $_GET['start_import'] ) ) {
	$db->update( "TEC_IMPORT_LOG", array( "start_import" => 1, "finished" => 0, "status" => 1, "curr_line" => 0, "start_time" => $f->d( 0 ), "end_time" => $f->d( 0 ) ), "id='".$_GET['start_import']."'" );
	$db->commit();
} // if

require_once( CLASS_DIR."templates/header.php" );
?>
	<div class="row">
		<div class="col-sm-12 col-md-6 sr">
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>" enctype="multipart/form-data">
	<input type="hidden" name="MAX_FILE_SIZE" value="100000000">
	<input type="hidden" name="upload" value="1">
	<h2><? echo $t->t( 'Import' ); ?></h2>
				<div id="primjer">
					<div class="accent-cell">
						<table class="list_left shadow table table-sm">
							<tr>
								<th style="text-align: left;"><?php echo $t->t( 'Artikel + WebService' ); ?></th>
								<td><a href="/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/examples/Import_Items_WebService.xlsx" class="link_click_button float-right"><?php  echo $f->get_button( 'Beispiel' ); ?></a></td>
							</tr>
							<tr>
								<th style="text-align: left;"><?php echo $t->t( 'Artikel + kein WebService' ); ?></th>
								<td><a href="/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/examples/Import_Items.xlsx" class="link_click_button float-right"><?php  echo $f->get_button( 'Beispiel' ); ?></a></td>
							</tr>
							<tr>
								<th style="text-align: left;"><?php echo $t->t( 'Ersatznummer' ); ?></th>
								<td><a href="/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/examples/Import_Substitute_Items.xlsx" class="link_click_button float-right"><?php  echo $f->get_button( 'Beispiel' ); ?></a></td>
							</tr>
							<tr>
								<th style="text-align: left;"><?php echo $t->t( 'DataConnector' ); ?></th>
								<td><a href="/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/examples/Import_DataConnector.xlsx" class="link_click_button float-right"><?php  echo $f->get_button( 'Beispiel' ); ?></a></td>
							</tr>
							<tr>
								<th style="text-align: left;"><?php echo $t->t( 'WebService Mapping' ); ?></th>
								<td><a href="/<? echo SUBDIR; ?>themes/tecdocbase_atit_at/examples/Import_Webservice_Mapping.xlsx" class="link_click_button float-right"><?php  echo $f->get_button( 'Beispiel' ); ?></a></td>
							</tr>
						</table>
					</div>
				</div>
<!--		</div>
		<div class="col-sm-12 col-md-6 sr"> -->
				<div class="list_left shadow accent-cell sr" style="line-height: 24px;">
					<tr><th><? echo $t->t( 'Datei' ); ?>&nbsp;</th><td><input type="file" name="datei" value=""></td></tr>
					<tr><td colspan="2" class="right">
						<a href="#" onClick="$(this).closest('form').submit()" class="link_click_button_right"><? $f->print_button( 'upload' ); ?></a>
					</td></tr>
				</div>
		</form>
		</div>
	</div>
<br>

<div class="table responsive">
<table class="list shadow table table-striped table-sm">
	<tr>
		<th><? echo $t->t( 'Typ' ); ?></th>
		<th><? echo $t->t( 'Datei' ); ?></th>
		<th><? echo $t->t( 'Start' ); ?></th>
		<th><? echo $t->t( 'Beendet' ); ?></th>
		<th><? echo $t->t( 'Status' ); ?></th>
		<th><? echo $t->t( 'Spalten' ); ?></th>
		<th><? echo $t->t( 'Startzeit' ); ?></th>
		<th><? echo $t->t( 'Endzeit' ); ?></th>
		<th style="text-align: right;"><? echo $t->t( 'Aktion' ); ?></th>
	</tr>
	<?php
	$db->query( "SELECT * FROM TEC_IMPORT_LOG " );
	while( $db->isNext() ) {
		$r = $db->getNext();

		switch( $r['type'] ) {
			case 1: $type = $t->t( 'Artikel' ); break;
			case 2: $type = $t->t( 'Ersatznummer' ); break;
			case 3: $type = $t->t( 'WebService Mapping' ); break;
			case 4: $type = $t->t( 'DataConnector' ); break;
			default: $type = $t->t( 'unbekannt' ); break;
		} // switch
		switch( $r['status'] ) {
			case 1: $status = $t->t( 'Import wird gestartet' ); break;
			case 2: $status = $t->t( 'Import läuft' ); break;
			case 3: $status = $t->t( 'Import beendet' ); break;
			default: $status = ""; break;
		} // switch

		$import_button = '<a href="?start_import='.$r['id'].'" class="link_click_button">'.$f->get_button( 'Importieren' ).'</a>';
		if( $type == $t->t( 'unbekannt' )  )
			$import_button = '';

		echo '
			<tr>
				<td>'.$type.'</td>
				<td>'.$r['filename'].'</td>
				<td>'.($r['start_import']==1?$t->t( 'Ja' ):$t->t( 'Nein' )).'</td>
				<td>'.($r['finished']==1?$t->t( 'Ja' ):$t->t( 'Nein' )).'</td>
				<td>'.$status.' '.$r['curr_line'].'/'.$r['max_lines'].'</td>
				<td>'.$r['max_cols'].'</td>
				<td>'.$f->t( $r['start_time'] ).'</td>
				<td>'.$f->t( $r['end_time'] ).'</td>
				<td style="width: auto; text-align: right;">
					<div style="display: inline-flex;">
					'.$import_button.'
					<a href="?del='.$r['id'].'" onClick=\'if( !confirm("'.$t->t( "Datei wirklich löschen?" ).'") ) return( false );\' class="link_click_button">'.$f->get_button( 'löschen' ).'</a>
					</div>
				</td>
			</tr>
		';
	} // while
	?>
</table>
</div>

<?
require_once( CLASS_DIR."templates/footer.php" );
?>