<?php
if( !isset($_SESSION) ) session_start();

require_once( dirname( __FILE__ )."/../classes/config_data.php" );
require_once( CLASS_DIR.'functions.php' );
require_once( CLASS_DIR.'theme_functions.php' );
require_once( CLASS_DIR.'messages.php' );
require_once( CLASS_DIR.'PasswordHash.php' );

$f = functions::getInstance();
$t = translate::getInstance();
$theme = theme_functions::getInstance();
$mes = messages::getInstance();
$pw = new PasswordHash( 8, false );

$current_email = '';
$current_id = '';
$mes->resetMessages();

if( isset( $_GET['reset_password'] ) && isset( $_POST['id'] ) ) {
	$current_id = $_POST['id'];

	if( $db->query( "SELECT email FROM CORE_USER_INFO WHERE md5(CONCAT(email,'".functions::getInstance()->get_location_password()."'))='".$current_id."'" ) ) {
		$r = $db->getNext();

		$current_email = $r['email'];

		if( isset( $_POST['password'] ) && isset( $_POST['password2'] ) && ($_POST['password'] != "") && ($_POST['password2'] != "") ) {
			if( mb_strlen( $_POST['password'] ) < 8 )
				$mes->addError( "Das neue Passwort muss mindestens 8 Zeichen haben." );
			if( $_POST['password'] != $_POST['password2'] )
				$mes->addError( "Das neue Passwort stimmt nicht mit der Bestätigung überein." );

			if( $mes->noError() ) {
				$f->password_lost_finished( $current_email );

				$mes->addInfo( "Das Passwort wurde geändert." );
				$mes->saveMessages();

				$db->update( "CORE_USER_INFO", array( "password" => $pw->HashPassword( $_POST['password'] ) ), "email='".$r['email']."'" );
				$db->commit();
				?>
				<script language="javascript">location.href="<? echo DOMAIN.'admin/login_login.php'; ?>";</script>
				<?
			 exit;
			} // if

		} else
			$mes->addError( "Das neue Passwort kann nicht leer sein." );
	} // if
} // if

if( isset( $_GET['id'] ) ) {
	$current_id = $_GET['id'];

	if( $db->query( "SELECT email FROM CORE_USER_INFO WHERE md5(CONCAT(email,'".functions::getInstance()->get_location_password()."'))='".$current_id."'" ) ) {
		$r = $db->getNext();

		$current_email = $r['email'];
	} // if
} // if

if( ($current_email == '') || ($current_id == '') ) {
	$mes->addError( "Es wurde keine gültige E-Mail Adresse gefunden." );
} // if

// Default Header
require_once( BASE_DIR."themes/_default/classes/pw_revover.php" );
?>