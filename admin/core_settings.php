<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );
require_once( CLASS_DIR.'mail.php' );

unset( $_SESSION['list_id'] );

$setup = $f->save_setup( "CORE_SETUP" );
$setup = $f->load_setup( "CORE_SETUP" );

if( isset( $_GET['test_mail'] ) ) {
	$mail = fb_mail::getInstance();

	$mail->mail_testmail( $setup['email_adress'] );
} // if

require_once( CLASS_DIR."templates/header.php" );

$themes_option = '';
foreach( glob( BASE_DIR.'themes/_default/css/color_themes/*.css' ) as $strFilename ) {
	$arrid = explode( "__", basename( $strFilename ) );
	$id = (int)substr( $arrid[1], 0, 3 );
	$arrname = explode( ".", $arrid[1] );
	$name = substr( $arrname[0], 4 );

	if( is_numeric( substr( $arrid[1], 0, 3 ) ) )
		$themes_option .= '<option value="'.$id.'" '.(($setup['color_theme']==$id)?"selected":"").'>'.$name.'</option>';
} // foreach

?>
<div id="content_scroll">
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1&table=CORE_SETUP">
	<h2>Einstellungen</h2>
	<table class="list_left shadow" style="width: 705px;">
<!--
		<tr><th width="200">Registrierung möglich</th><td><input type="hidden" value="0" name="register_allowed"><input type="checkbox" value="1" <? echo (($setup['register_allowed']==1)?'checked':''); ?> name="register_allowed"/></td></tr>
		<tr><th>Zeige Backup Info</th><td><input type="hidden" value="0" name="show_backup_info"><input type="checkbox" value="1" <? echo (($setup['show_backup_info']==1)?'checked':''); ?> name="show_backup_info"/></td></tr>
		<tr><th>IFrame Link</th><td><input size="80" type="text" value="<? echo $setup['iframe_link']; ?>" name="iframe_link"/><input type="button" value="URL" onClick="window.open('<? echo $setup['iframe_link']; ?>', '_blank');"></td></tr>
		<tr><th>Verberge Flaggen für Benutzer</th><td><input type="hidden" value="0" name="hide_flags"><input type="checkbox" value="1" <? echo (($setup['hide_flags']==1)?'checked':''); ?> name="hide_flags"/></td></tr>
		<tr><th>Kontakt E-Mail</th><td><input size="80" type="text" value="<? echo $setup['contact_email']; ?>" name="contact_email"/></td></tr>
		<tr><th>Kontakt Telefon</th><td><input size="80" type="text" value="<? echo $setup['contact_phone']; ?>" name="contact_phone"/></td></tr>
		<tr><th>Login Text</th><td><textarea name="login_text" cols="77" rows="10"><? echo $setup['login_text']; ?></textarea></td></tr>
		<tr><th>Impressum Link</th><td><input size="80" type="text" value="<? echo $setup['impressum_link']; ?>" name="impressum_link"/><input type="button" value="URL" onClick="window.open('<? echo $setup['impressum_link']; ?>', '_blank');"></td></tr>
-->
		<tr><th>Version verwenden</th><td>
			<select name="version_type" style="width: 100%">
				<option value="0" <?php echo (($setup['version_type']==0)?"selected":"");?>>v1.0 (deprecated): JQuery 2.2.1, Bootstrap 3.3.6, JQuery-UI 1.11.4</option>
  			<option value="1" <?php echo (($setup['version_type']==1)?"selected":"");?>>v1.5 (deprecated): JQuery 3.2.1, Bootstrap 4.0.0-beta, JQuery-UI 1.12.1</option>
				<option value="2" <?php echo (($setup['version_type']==2)?"selected":"");?>>v2.0 (STABLE): JQuery 3.3.1, Bootstrap 4.1.3, JQuery-UI 1.12.1</option>
			</select>
		</td></tr>

		<tr><th>Standard Layout Sprache</th><td><input size="10" type="text" name="default_language" value="<? echo $setup['default_language']; ?>"/></td></tr>
		<tr><th>Zeige Systeminfos (Footer)</th><td><input type="hidden" value="0" name="show_script_duration"><input type="checkbox" value="1" <? echo (($setup['show_script_duration']==1)?'checked':''); ?> name="show_script_duration"/></td></tr>

		<tr><td colspan="2" class="right"><a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a></td></tr>
	</table>
</form>
<br />

<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1&table=CORE_SETUP">
	<h2>Layout</h2>
	<table class="list_left shadow" style="width: 705px;">
		<tr><th>Farb Schema</th><td>
			<select name="color_theme" style="width: 100%">
				<?php echo $themes_option; ?>
			</select>
		</td></tr>
		<tr><th>Farb Schema (externes css) <i class="fas fa-info-circle text-primary fa-fw" title="ACHTUNG: Das ist ausschliesslich für kritische Bug-Fixes, Tests oder Layouts,<br>wenn sie nicht im aktuellen Update sind.<br><br>Jedes Farbschema muss in ein Update aufgenommen werden!"></i></th><td><input size="80" type="text" value="<? echo $setup['color_theme_external_css']; ?>" name="color_theme_external_css"/></td></tr>
		<tr><td colspan="2" class="right"><a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a></td></tr>
	</table>
</form>
<br />

<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1&table=CORE_SETUP">
	<h2>E-Mail Einstellungen</h2>
	<table class="list_left shadow" style="width: 705px;">
		<tr><th>Absender Name</th><td><input size="80" type="text" value="<? echo $setup['email_name']; ?>" name="email_name"/></td></tr>
		<tr><th>Absender E-Mail</th><td><input size="80" type="text" value="<? echo $setup['email_adress']; ?>" name="email_adress"/></td></tr>

		<tr><th width="200">externen SMTP verwenden</th><td><input type="hidden" value="0" name="email_extern_smtp"><input type="checkbox" value="1" <? echo (($setup['email_extern_smtp']==1)?'checked':''); ?> name="email_extern_smtp"/></td></tr>
		<tr><th>SMTP-Server</th><td><input size="80" type="text" value="<? echo $setup['email_smtp_server']; ?>" name="email_smtp_server"/></td></tr>
		<tr><th>STMP Benutzer</th><td><input size="80" type="text" value="<? echo $setup['email_smtp_user']; ?>" name="email_smtp_user"/></td></tr>
		<tr><th>SMTP Passwort</th><td><input size="80" type="password" value="<? echo $setup['email_smtp_password']; ?>" name="email_smtp_password"/></td></tr>
		<tr><th>SMTP Port</th><td><input size="80" type="text" value="<? echo $setup['email_smtp_port']; ?>" name="email_smtp_port"/></td></tr>
		<tr><th>SMTP Sicherheit</th><td>
			<select name="email_smtp_secure" style="width: 100%">
				<option value="" <?php echo (($setup['email_smtp_secure']=='')?"selected":"");?>>--- (Port: 25)</option>
				<option value="tls" <?php echo (($setup['email_smtp_secure']=='tls')?"selected":"");?>>tls (Port: 465/587)</option>
				<option value="ssl" <?php echo (($setup['email_smtp_secure']=='ssl')?"selected":"");?>>ssl (Port: 465)</option>
			</select>
		<tr><td colspan="2" class="right">
			<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
			<a href="?test_mail=1" class="link_click_button_right"><?php echo $f->get_button( 'E-Mail senden' ); ?></a>
		</td></tr>
	</table>
</form>
<br />

<?php
if( $f->project_allowed( 4 ) ) {
	$setup = $f->save_setup( "TEC_SETUP" );
	$setup = $f->load_setup( "TEC_SETUP" );

	?>
	<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1&table=TEC_SETUP">
		<h2>Generelle Einrichtung</h2>
		<table class="list_left shadow" style="width: 705px;">
			<tr><th width="200">Währungscode</th><td><input size="80" type="text" value="<? echo $setup['currency_code']; ?>" name="currency_code"/></td></tr>
			<tr><th>MwSt %</th><td><input size="80" type="text" value="<? echo $setup['vat']; ?>" name="vat"/></td></tr>
			<tr><th>Lagerort Pflicht</th><td><input type="hidden" value="0" name="need_location"><input type="checkbox" value="1" <? echo (($setup['need_location']==1)?'checked':''); ?> name="need_location"/></td></tr>
			<tr><th>Fiber Nummer</th><td><input size="80" type="text" value="<? echo $setup['fiber_no']; ?>" name="fiber_no"/></td></tr>
			<tr><th>Kundeninformation</th><td><input size="80" type="text" name="customer_info" value="<? echo $setup['customer_info']; ?>"/></td></tr>
			<tr><th>Für Warenkorb gesperrte Lagerorte</th><td><input size="80" type="text" name="blocked_locations" value="<? echo $setup['blocked_locations']; ?>"/></td></tr>
			<tr><th>Anzahl Ergebnisse Universalartikel</th><td><input size="80" type="text" name="count_results_univ_items" value="<? echo $setup['count_results_univ_items']; ?>"/></td></tr>
			<tr><th>Sub Shop aktiv</th><td><input type="hidden" value="0" name="sub_module_active"><input type="checkbox" value="1" <? echo (($setup['sub_module_active']==1)?'checked':''); ?> name="sub_module_active"/></td></tr>
			<tr><th>Sub Admin E-Mail</th><td><input size="80" type="text" name="sub_admin_email" value="<? echo $setup['sub_admin_email']; ?>"/></td></tr>
			<tr><th>Artikel Prüfung aktiv</th><td><input type="hidden" value="0" name="item_check_active"><input type="checkbox" value="1" <? echo (($setup['item_check_active']==1)?'checked':''); ?> name="item_check_active"/></td></tr>
			<tr><th>Belegart aktiv</th><td><input type="hidden" value="0" name="document_type_active"><input type="checkbox" value="1" <? echo (($setup['document_type_active']==1)?'checked':''); ?> name="document_type_active"/></td></tr>
			<tr><th>Lieferart aktiv</th><td><input type="hidden" value="0" name="delivery_method_active"><input type="checkbox" value="1" <? echo (($setup['delivery_method_active']==1)?'checked':''); ?> name="delivery_method_active"/></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>

		<h2>TecDoc Setup</h2>
		<table class="list_left shadow" style="width: 705px;">
			<tr><th width="200">Suche 'countriesCarSelection'</th><td><input size="80" type="text" value="<? echo $setup['td_countriesCarSelection']; ?>" name="td_countriesCarSelection"/></td></tr>
			<tr><th>Suche 'articleCountry'</th><td><input size="80" type="text" value="<? echo $setup['td_articleCountry']; ?>" name="td_articleCountry"/></td></tr>
			<tr><th>Suche 'country'</th><td><input size="80" type="text" value="<? echo $setup['td_country']; ?>" name="td_country"/></td></tr>
			<tr><th>Suche 'language'</th><td><input size="80" type="text" value="<? echo $setup['td_language']; ?>" name="td_language"/></td></tr>
			<tr><th>Suche 'linkingtargettype'</th><td><input size="80" type="text" value="<? echo $setup['td_linkingtargettype']; ?>" name="td_linkingtargettype"/></td></tr>
			<tr><th>TecDoc ID</th><td><input size="80" type="text" value="<? echo $setup['td_mandator']; ?>" name="td_mandator"/></td></tr>
			<tr><th>TecDoc Service URL</th><td><input size="80" type="text" value="<? echo $setup['td_service_url']; ?>" name="td_service_url"/></td></tr>
			<tr><th>TecDoc Image URL</th><td><input size="80" type="text" value="<? echo $setup['td_image_url']; ?>" name="td_image_url"/></td></tr>
			<tr><th>zusätzl. Artikel Abfragen (x25)</th><td><input size="80" type="text" value="<? echo $setup['additional_requests']; ?>" name="additional_requests"/></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>

		<h2>Anzeige</h2>
		<table class="list_left shadow" style="width: 705px;">
			<tr><th width="200">IFrame in Suche anzeigen</th><td><input type="hidden" value="0" name="iframe_in_suche"><input type="checkbox" value="1" <? echo (($setup['iframe_in_suche']==1)?'checked':''); ?> name="iframe_in_suche"/></td></tr>
			<tr><th>Menü bei Suche zuklappen</th><td><input type="hidden" value="0" name="enable_menu_close"><input type="checkbox" value="1" <? echo (($setup['enable_menu_close']==1)?'checked':''); ?> name="enable_menu_close"/></td></tr>
			<tr><th>Interne Nummer anzeigen</th><td><input type="hidden" value="0" name="show_internal_item_no"><input type="checkbox" value="1" <? echo (($setup['show_internal_item_no']==1)?'checked':''); ?> name="show_internal_item_no"/></td></tr>
			<tr><th>Interne Bemerkung anzeigen</th><td><input type="hidden" value="0" name="show_internal_description"><input type="checkbox" value="1" <? echo (($setup['show_internal_description']==1)?'checked':''); ?> name="show_internal_description"/></td></tr>
			<tr><th>Daten vom Originalartikel ziehen</th><td><input type="hidden" value="0" name="copy_picture_from_original"><input type="checkbox" value="1" <? echo (($setup['copy_picture_from_original']==1)?'checked':''); ?> name="copy_picture_from_original"/></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>


		<h2>Web Service</h2>
		<table class="list_left shadow" style="width: 705px;">
			<tr><th width="200">WebService aktivieren</th><td><input type="hidden" value="0" name="enable_topmotive_api"><input type="checkbox" value="1" <? echo (($setup['enable_topmotive_api']==1)?'checked':''); ?> name="enable_topmotive_api"/></td></tr>
			<tr><th>WebService Mapping aktivieren</th><td><input type="hidden" value="0" name="enable_webservice_mapping"><input type="checkbox" value="1" <? echo (($setup['enable_webservice_mapping']==1)?'checked':''); ?> name="enable_webservice_mapping"/></td></tr>
			<tr><th>WebService URL</th><td><input size="80" type="text" value="<? echo $setup['tm_endpoint']; ?>" name="tm_endpoint"/><input type="button" value="URL" onClick="window.open('<? echo $setup['tm_endpoint']; ?>', '_blank');"></td></tr>
			<tr><th>WebService Location</th><td><input size="80" type="text" value="<? echo $setup['tm_endpoint_location']; ?>" name="tm_endpoint_location"/><input type="button" value="URL" onClick="window.open('<? echo $setup['tm_endpoint_location']; ?>', '_blank');"></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>

		<h2>FIN Suche</h2>
		<table class="list_left shadow" style="width: 705px;">
			<tr><th>TecRMI aktivieren</th><td><input type="hidden" value="0" name="enable_fin_search"><input type="checkbox" value="1" <? echo (($setup['enable_fin_search']==1)?'checked':''); ?> name="enable_fin_search"/></td></tr>
			<tr><th width="200">URL</th><td><input size="80" type="text" value="<? echo $setup['fin_url']; ?>" name="fin_url"/><input type="button" value="URL" onClick="window.open('<? echo $setup['fin_url']; ?>', '_blank');"></td></tr>
			<tr><th>Location</th><td><input size="80" type="text" value="<? echo $setup['fin_location']; ?>" name="fin_location"/><input type="button" value="URL" onClick="window.open('<? echo $setup['fin_location']; ?>', '_blank');"></td></tr>
			<tr><th>Company</th><td><input size="80" type="text" value="<? echo $setup['fin_company']; ?>" name="fin_company"/></td></tr>
			<tr><th>Username</th><td><input size="80" type="text" value="<? echo $setup['fin_username']; ?>" name="fin_username"/></td></tr>
			<tr><th>Password</th><td><input size="80" type="text" value="<? echo $setup['fin_password']; ?>" name="fin_password"/></td></tr>

			<tr><th colspan="2"><hr></th></tr>
			<tr><th>...Audatex nicht in Verwendung...</th><td></td></tr>
			<tr><th>Audatex aktivieren</th><td><input type="hidden" value="0" name="enable_fin_search_audatex"><input type="checkbox" value="1" <? echo (($setup['enable_fin_search_audatex']==1)?'checked':''); ?> name="enable_fin_search_audatex"/></td></tr>

			<tr><th><u><strong></u></strong></th><td></td></tr>
			<tr><th>Info Text</th><td><textarea name="info_text_audatex" cols="77" rows="10"><? echo $setup['info_text_audatex']; ?></textarea></td></tr>
			<tr><th width="200">URL</th><td><input size="80" type="text" value="<? echo $setup['audatex_url']; ?>" name="audatex_url"/><input type="button" value="URL" onClick="window.open('<? echo $setup['audatex_url']; ?>', '_blank');"></td></tr>
			<tr><th>Location</th><td><input size="80" type="text" value="<? echo $setup['audatex_location']; ?>" name="audatex_location"/><input type="button" value="URL" onClick="window.open('<? echo $setup['audatex_location']; ?>', '_blank');"></td></tr>
			<tr><th>Username</th><td><input size="80" type="text" value="<? echo $setup['audatex_username']; ?>" name="audatex_username"/></td></tr>
			<tr><th>Password</th><td><input size="80" type="text" value="<? echo $setup['audatex_password']; ?>" name="audatex_password"/></td></tr>

			<tr><th colspan="2"><hr></th></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>
	</form>
	<br />
	<?
} // if

?>
</div>
<?php
require_once( CLASS_DIR."templates/footer.php" );
?>