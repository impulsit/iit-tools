<?php
require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

if( isset( $_GET['change_pw'] ) ) {
	if( ($_POST['current'] != "") && ($_POST['new1'] != "") && ($_POST['new2'] != "") ) {
		$pw = new PasswordHash( 8, false );

		$db->query( "SELECT user_id, password FROM CORE_USER_INFO WHERE user_id='".$c_user_id."'" );
		$userinfo = $db->getNext();

		if( !$pw->CheckPassword( $_POST['current'], $userinfo['password'] ) )
			$mes->addError( "Das aktuelle Passwort ist nicht korrekt." );
		if( mb_strlen( $_POST['new1'] ) < 5 )
			$mes->addError( "Das neue Passwort muss mindestens 5 Zeichen haben." );
		if( $_POST['new1'] != $_POST['new2'] )
			$mes->addError( "Das neue Passwort stimmt nicht mit der Bestätigung überein." );

		if( $mes->noError() ) {
			$db->update( "CORE_USER_INFO", array( "password" => $pw->HashPassword( $_POST['new1'] ) ), "user_id='".$c_user_id."'" );
			$db->commit();

			$mes->addInfo( "Das Passwort wurde geändert." );
		} // if
	} // if

	$db->update( "CORE_USER_INFO", array( "email_fragebogen" => $_POST['email_fragebogen'] ), "user_id='".$c_user_id."'" );
	$db->commit();

	$mes->addInfo( "Einstellungen gespeichert." );
} // if

require_once( CLASS_DIR."templates/header.php" );

$setup = $f->save_setup( "WHY_SETUP" );
$setup = $f->load_setup( "WHY_SETUP" );

$db->query( "SELECT email_fragebogen FROM CORE_USER_INFO WHERE user_id='".$c_user_id."'" );
$userinfo = $db->getNext();

?>
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?change_pw=1">
	<h2>Passwort ändern</h2>
	<table class="list_left shadow" style="width: 730px;">
		<tr><th>aktuelles Passwort</th><td><input type="password" name="current"/></td></tr>
		<tr><th>neues Passwort</th><td><input type="password" name="new1"/></td></tr>
		<tr><th>neues Passwort wiederholen</th><td><input type="password" name="new2"/></td></tr>
		<tr><td colspan="2"></td></tr>
		<tr><th>E-Mail für Fragebogen Sendungen</th><td><input type="text" name="email_fragebogen" value="<? echo $userinfo['email_fragebogen'] ?>"/></td></tr>
		<tr><td colspan="2" class="right">
			<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php  echo $f->get_button( 'speichern' ); ?></a>
	</table>
</form>
<?

$userinfo = $f->load_user( $c_user_id, "admin" );

if( $userinfo['admin'] == 1 ) {
	$setup['preis_1'] = str_replace( ".", ",", $setup['preis_1'] );
	$setup['preis_2'] = str_replace( ".", ",", $setup['preis_2'] );
	$setup['preis_3'] = str_replace( ".", ",", $setup['preis_3'] );

	?>
	<br />
	<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
		<h2>Setup</h2>
		<table class="list_left shadow" style="width: 730px;">
			<tr><th>Fragebogen Anzahl für neue Kunden</th><td><input type="text" name="anzahl_neue_kunden" value="<? echo $setup['anzahl_neue_kunden']; ?>"/></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php  echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>
		<br />

		<h2>Preis Fragebögen</h2>
		<table class="list_left shadow" style="width: 730px;">
			<tr><th>Anzahl</th><td><input type="text" name="anzahl_1" value="<? echo $setup['anzahl_1']; ?>"/></td><th>Preis (EUR)</th><td><input type="text" name="preis_1" value="<? echo $setup['preis_1']; ?>"/></td></tr>
			<tr><th>Anzahl</th><td><input type="text" name="anzahl_2" value="<? echo $setup['anzahl_2']; ?>"/></td><th>Preis (EUR)</th><td><input type="text" name="preis_2" value="<? echo $setup['preis_2']; ?>"/></td></tr>
			<tr><th>Anzahl</th><td><input type="text" name="anzahl_3" value="<? echo $setup['anzahl_3']; ?>"/></td><th>Preis (EUR)</th><td><input type="text" name="preis_3" value="<? echo $setup['preis_3']; ?>"/></td></tr>

			<tr><td colspan="4" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php  echo $f->get_button( 'speichern' ); ?></a>
			</td></tr>
		</table>
	</form>
	<?
} else {
	$userinfo = $f->load_user( $c_user_id, "rest_anzahl" );
	$db->query( "SELECT * FROM SETUP WHERE id='1'" );
	$setup = $db->getNext();

	$email = 'office@thomaslaggner.at';
	$buy_link = '';

	for( $i=1; $i<=3; $i++ ) {
		$name = 'Anzahl: '.$setup['anzahl_'.$i];
		$item_number = $i;
		$amount = $setup['preis_'.$i];

		$url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business='.$email.'&item_name='.$name.'&amount='.$amount.'&on0=user_id&os0='.$c_user_id.'&on1=item_id&os1='.$item_number.'&no_shipping=1&no_note=1&currency_code=EUR&lc=DE&bn=PP%2dBuyNowBF&charset=UTF%2d8';

	  $buy_link .= '<a class="buy_link" href="'.$url.'" target="_blank">'.$setup['anzahl_'.$i].' Anzahl: '.' ('.$amount.' EUR)</a> ';
	} // for

	// Zustellen
	if( isset( $_GET['send'] ) ) {
		$sended = 0;

		$db->query( "
			SELECT uf.id
			FROM USER_FRAGEBOGEN AS uf
			LEFT JOIN DOMAINS AS d ON (d.fragebogen_id=uf.fragebogen_id)
			WHERE d.user_id='".$c_user_id."' AND uf.fertig_ausgefuellt='1' AND uf.versendet='0'
			ORDER BY uf.id", "loop" );
		while( $db->isNext( "loop" ) ) {
			$r4 = $db->getNext( "loop" );

			if( ($userinfo['rest_anzahl'] - $sended) > 0 ) {
				$f->send_fb_email( $r4['id'] );
				$sended++;
			} // if
		} // while

		echo '<div class="infotext">Es wurden '.$sended.' Fragebögen zugestellt.</div>';

		$userinfo = $f->load_user( $c_user_id, "rest_anzahl" );
	} // if

	// Restanzahl usw
	echo 'Anzahl noch verschickbar: '.$userinfo['rest_anzahl'].'<br /><br />';
	echo $buy_link;

	$nicht_verschickt = 0;
	echo '
		<br style="clear:both;"><br /><table class="list">
			<tr><th>Fragebogen</th><th>Gesamt</th><th>bereits verschickt</th><th>im Wartestatus</th></tr>';
	$db->query( "
		SELECT f.fragebogen_id, f.titel
		FROM DOMAINS AS d
		LEFT JOIN FRAGEBOGEN AS f ON (f.fragebogen_id=d.fragebogen_id)
		WHERE d.user_id='".$c_user_id."'", "loop" );
	while( $db->isNext( "loop" ) ) {
		$r = $db->getNext( "loop" );

		$db->query( "SELECT COUNT( id ) AS anz FROM USER_FRAGEBOGEN WHERE fragebogen_id='".$r['fragebogen_id']."' AND fertig_ausgefuellt='1'" );
		$r2 = $db->getNext();

		$db->query( "SELECT COUNT( id ) AS anz FROM USER_FRAGEBOGEN WHERE fragebogen_id='".$r['fragebogen_id']."' AND fertig_ausgefuellt='1' AND versendet='1'" );
		$r3 = $db->getNext();

		$db->query( "SELECT COUNT( id ) AS anz FROM USER_FRAGEBOGEN WHERE fragebogen_id='".$r['fragebogen_id']."' AND fertig_ausgefuellt='1' AND versendet='0'" );
		$r4 = $db->getNext();

		echo '<tr><td>'.$r['titel'].'</td><td class="right">'.$r2['anz'].'</td><td class="right">'.$r3['anz'].'</td><td class="right">'.$r4['anz'].'</td></tr>';
		$nicht_verschickt += $r4['anz'];
	} // while
	echo '</table><br />';

	if( ($nicht_verschickt > 0) && ($userinfo['rest_anzahl'] > 0) )
		echo '<a class="buy_link" href="?send=1">Fragenbögen zustellen (älteste zuerst)</a><br />';

	// Log anzeigen
	echo '<br />Verlauf
		<table class="list">
			<tr><th>Zeit</th><th>Anzahl</th><th>Preis</th><th>Beschreibung</th></tr>';

	$db->query( "SELECT * FROM LOG_PAYMENT_DETAIL WHERE user_id='".$c_user_id."'" );
	while( $db->isNext() ) {
		$r = $db->getNext();

		$str = '';
		switch( $r['typ'] ) {
			case AKTION_EINKAUF: $str = 'Fragenbogen gekauft'; break;
			case AKTION_SEND: $str = 'Fragenbogen versendet'; break;
			default: $str = 'Unbekannt: '.$r['typ']; break;
		} // switch

		echo '<tr><td>'.$f->t( $r['zeit'] ).'</td><td class="right">'.$r['anzahl'].'</td><td class="right">'.$r['preis'].'</td><td>'.$str.'</td></tr>';
	} // while
	echo '</table>';


} // else




require_once( CLASS_DIR."templates/footer.php" );
?>