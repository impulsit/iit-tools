<?php
set_time_limit( 300 );

require_once( "../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );
require_once( CLASS_DIR.'update_functions.php' );

unset( $_SESSION['list_id'] );

if( isset( $_GET['save_version'] ) ) {
	$f->save_current_version( $_POST['version'] );

	header( "location: core_update_qnap_home.php" );
	exit;
} // if

require_once( CLASS_DIR."templates/header.php" );

$update = update::getInstance();

// Init
$f->load_projects( $projects, $old_projects );

// Content
echo '<div id="content_scroll">';

$dir = BASE_DIR.'updates/';
if( !file_exists( $dir ) ) mkdir( $dir );

$location = file_get_contents( dirname( __FILE__ ).'/../location.txt' );

$server_version = "";
$server_version = @file_get_contents( "http://iit-tools.sordje.at/updates/".$location."/last_version.txt" );

$status = "";
if( isset( $_POST['change_date'] ) ) {
	$_POST['version'] = str_replace( " ", "_", $_POST['version'] );

	foreach( $projects as $k => $project_dir ) {
		if( ($_POST['selected_project'] == $project_dir) || ($_POST['selected_project'] == "") ) {
			if( !file_exists( $dir.$project_dir ) )
				mkdir( $dir.$project_dir );

			$files = array();
			$skip = array();
			$skip_file_pattern = array();
			$include_class = array();
//			if( !isset( $_POST['inkl_templates'] ) || ($_POST['inkl_templates'] == 0) )
//				$skip[] = "classes/templates/";
			if( !isset( $_POST['initial_update'] ) ) $initial_update = 0; else $initial_update = 1;


			$skip_file_pattern = array( "vmware_home", "qnap_home", "tecdocbase_atit_at" );
			foreach( $projects as $k2 => $project_dir2 ) {
				if( $project_dir != $project_dir2 )
					$skip_file_pattern[] = $project_dir2;
			} // foreach
			foreach( $old_projects as $k2 => $project_dir2 ) {
				if( $project_dir != $project_dir2 )
					$skip_file_pattern[] = $project_dir2;
			} // foreach

			switch( $project_dir ) {
				case 'impulsit_at':					$include_class = array( "freeto_biz", "basilica_at", "tecdocbase_atit_at" ); break;
				case 'freeto_biz':					$include_class = array(); break;
				case 'basilica_at': 				$include_class = array( "freeto_biz" ); break;
				default:  									$include_class = array( "tecdocbase_atit_at" ); break;
			} // switch


			$f->dir_rekursiv( BASE_DIR, strtotime( $_POST['change_date'] ), $skip, $skip_file_pattern, $include_class, $initial_update );

			$zip = new ZipArchive();
			$zip->open( $dir.$project_dir.'/'.$_POST['version'].'.zip', ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE );

			$status .= 'Update erstellt für "'.$project_dir.'": '.$_POST['version'].'.zip';
			foreach( $files as $k => $v ) {
				if( basename( $v['file'] ) != "location.txt" ) {
					//$status .= '<tr><td>'.$v['file'].'</td><td>'.$v['change_time'].'</td></tr>';
					$zip->addFile( $v['file'], str_replace( BASE_DIR, "", $v['file'] ) );
				} // if
			} // foreach
			$status .= '<br />';

			$zip->close();

			$f->crypt_file( $dir.$project_dir.'/'.$_POST['version'].'.zip', $project_dir );
			$f->crypt_file_v2( $dir.$project_dir.'/'.$_POST['version'].'.zip', $project_dir );

			$f->create_version_info( $dir.$project_dir, $_POST['version'] );
		} // if
	} // foreach
} // if

$status_download = "";
if( isset( $_GET['download'] ) ) {
	$update_file = @file_get_contents( "http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip_enc" );

	if( $update_file === false ) {
		// Unverschlüsselt
		$update_file = @file_get_contents( "http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip" );
		file_put_contents( BASE_DIR.'updates/'.$server_version.".zip", $update_file );
		$status_download = "Datei wurde geladen: "."http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip<br>";
	} else {
		// Verschlüsselt
		file_put_contents( BASE_DIR.'updates/'.$server_version.".zip_enc", $update_file );
		$status_download = "Datei wurde geladen: "."http://iit-tools.sordje.at/updates/".$location."/".$server_version.".zip_enc<br>";

		$f->decrypt_file( BASE_DIR.'updates/'.$server_version.".zip" );
		$status_download .= "Datei wurde decodiert";
	} // else
} // if

if( isset( $_GET['del'] ) ) {
	unlink( BASE_DIR.'updates/'.$_GET['del'] );
} // if

$status2 = "";
if( isset( $_GET['install'] ) ) {
	// $version = substr( $_GET['install'], 8, 4 );
	$version = $update->get_version_no_from_version( $_GET['install'] );

	$zip = new ZipArchive;

	$zip->open( BASE_DIR.'updates/'.$_GET['install'] );

	$status2 .= 'Dateien auf dem neuesten Stand:<br /><table class="list_left shadow" style="width: 700px;">';
	for( $i=0; $i<$zip->numFiles; $i++ ) {
		$status2 .= '<tr><td>'.$zip->getNameIndex( $i ).'</td></tr>';
	} // for
	$status2 .= '</table><br />';

	$zip->extractTo( BASE_DIR );
	$zip->close();

	// Prüfe vorherige Updates
	$update->check_updates_to_process( $version );

	// Update Patch aufrufen
//	if( file_exists( BASE_DIR.'admin/update/update_'.$version.'.php' ) )
//		include( BASE_DIR.'admin/update/update_'.$version.'.php' );
	?>
	<script>
	location.href = "core_update.php";
	</script>
	<?
	exit;
} // if

$status_upload = '';
if( isset( $_GET['upload'] ) ) {
	$ftp_server = 'ftp.sordje.at';
	$ftp_user_name = '290652-iit_tools';
	$ftp_user_pass = '-iittools_!DF123';
	$status_upload = '';

	foreach( $projects as $k => $project_dir ) {
		$doUpload = false;
		if( ($_GET['upload'] == $project_dir) || (($_GET['upload'] == 'all') && (strpos( $project_dir, "_atit_at" ) !== false)) )
			$doUpload = true;
		if( ($_GET['upload'] == 'all') && ($project_dir == "test-v2_atit_at") )
			$doUpload = false;

		if( $doUpload ) {
			$status_upload .= '<table>';

			$lokale_version = @file_get_contents( BASE_DIR.'updates/'.$project_dir."/last_version.txt" );
			$lokal_files = array( 'last_version.txt' ); /* 'version_info.txt' */
			switch( $_GET['level'] ) {
				case 0: $lokal_files[] = $lokale_version.'.zip'; break;
				case 1: $lokal_files[] = $lokale_version.'.zip_enc'; break;
				case 2: $lokal_files[] = $lokale_version.'.zip_enc_v2'; break;
			} // switch

			// Open FTP
			$conn_id = ftp_connect( $ftp_server );
			$login_result = ftp_login( $conn_id, $ftp_user_name, $ftp_user_pass );

			// Lösche alte Dateien
			$files = ftp_nlist($conn_id, '/'.$project_dir.'/*');
			foreach ($files as $k => $file) {
				@ftp_delete( $conn_id, $file );
			}

			// Transfer
			foreach( $lokal_files as $k => $file ) {
				$lokal_file = BASE_DIR.'updates/'.$project_dir.'/'.$file;
				$remote_file = '/'.$project_dir.'/'.$file;

				if( !@ftp_chdir( $conn_id, '/'.$project_dir ) ) {
					ftp_mkdir( $conn_id, '/'.$project_dir );
					ftp_chmod( $conn_id, 0777, '/'.$project_dir );
				} // if

				$status_ftp = ftp_put( $conn_id, $remote_file, $lokal_file, FTP_BINARY );

				$status_upload .= '<tr><td>'.$lokal_file.'</td><td>-></td><td>ftp://'.$ftp_server.$remote_file.'</td><td>'.($status_ftp?'OK':'ERROR').'</td></tr>';
			} // if

			// Close FTP
			ftp_close( $conn_id );

			$status_upload .= '</table><br />';
			$status_upload .= 'Upload von "'.$project_dir.'" ausgeführt.<br>';
		} // if
	} // foreach
} // if

?>
<h2>Version</h2>
<?php
if( $status_download != "" )
	echo '<div class="alert alert-success" role="alert">'.$status_download.'</div>';
?>
<table class="list_left shadow" style="width: 700px;">
	<tr><th>aktuelle Version</th><td colspan="2"><? echo VERSION; ?></td></tr>
	<?
	if( $server_version != "" ) {
	?>
	<tr><th>letzte Version</th><td><? echo str_replace( "_", " ", $server_version ); ?></td>
		<td>
			<a href="?download=1" class="link_click_button"><?php echo $f->get_button( 'Download' ); ?></a>
			<!--  <a href="http://iit-tools.sordje.at/updates/<? echo $location; ?>/" target="_blank" class="link_click_button"><?php echo $f->get_button( 'URL öffnen' ); ?></a>  -->
		</td>
	</tr>
	<?
	} else {
		echo '<tr><th>kein Update verfügbar</th><td colspan="2">!!!</td></tr>';
	} // else
	?>
	<tr><td colspan="3">Die Update Dateien müssen im Verzeichnis <br />"<strong><? echo BASE_DIR.'updates/'; ?>"</strong><br />gespeichert sein.</td></tr>
</table>
<br />
<?

$userinfo = $f->load_user( $c_user_id, "email" );
if( ($userinfo['email'] == "michael@sordje.at") && (($_location == "vmware_home") || ($_location == "qnap_home")) ) {
	?>
	<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_version=1">
		<h2>Version setzen</h2>
		<table class="list_left shadow" style="width: 700px;">
			<tr><th>Version</th><td><input size="50" type="text" value="<? echo VERSION; ?>" name="version"></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
				</td></tr>
		</table>
	</form>
	<br />

	<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
		<h2>Update erstellen</h2>
		<?php
		if( $status != "" )
			echo '<div class="alert alert-success" role="alert">'.$status.'</div>';
		?>
		<table class="list_left shadow" style="width: 700px;">
			<tr><th>Basisverzeichnis</th><td><? echo BASE_DIR; ?></td></tr>
			<tr><th>Version</th><td><input size="50" type="text" value="<? echo VERSION; ?>" name="version"></td></tr>
			<tr><th>ab Änderungsdatum</th><td><input size="50" type="text" value="01.09.2017" id="change_date" name="change_date" class="date_picker">&nbsp;<input type="button" value="dieses Jahr" onclick="document.getElementById('change_date').value='01.01.<?php echo date('Y', time()); ?>'"></td></tr>
			<tr><th>für Projekt</th><td>
				<select name="selected_project">
					<option value="">alle</option>
					<?php
					foreach( $projects as $k => $project_dir ) {
						echo '<option value="'.$project_dir.'">'.$project_dir.'</option>';
					} // foreach
					?>
				</select>
			</td></tr>
			<tr><th>Alle Dateien</th><td><input type="checkbox" name="initial_update"></td></tr>
			<tr><td colspan="2" class="right">
				<a onClick="$(this).closest('form').submit()" class="link_click_button_right"><?php echo $f->get_button( 'speichern' ); ?></a>
				<a href="?upload=all&zip=0" class="link_click_button_right"><?php echo $f->get_button( 'TD upload alle (enc)' ); ?></a>
			</td></tr>
		</table>
	</form>
	<br />

	<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?ftp_update=1">
		<h2>Updateversionen</h2>
		<?php
		if( $status_upload != "" )
			echo '<div class="alert alert-success" role="alert">'.$status_upload.'</div>';
		?>
		<table class="tablesorting list shadow table table-striped table-sm" style="width: 1200px !important;">
			<thead>
			<tr><th>Projekt</th><th>Server Version</th><th>Installed</th><th>lokale Version</th><th>Größe</th><th>Unterschied</th><th class="list_th_columns_actions sorter-false">Aktion</th></tr>
			</thead>
			<?
			foreach( $projects as $k => $project_dir ) {
				$server_version = @file_get_contents( "http://iit-tools.sordje.at/updates/".$project_dir."/last_version.txt" );
				$lokale_version = @file_get_contents( BASE_DIR.'updates/'.$project_dir."/last_version.txt" );
				$installed_version = @file_get_contents( "http://iit-tools.sordje.at/updates/_php/get_update_installed.php?instance=".$project_dir );

				$installed_version = json_decode( $installed_version, true);
				$title = '<pre>'.print_r( $installed_version, true ).'</pre>';

				$lokal_file = BASE_DIR.'updates/'.$project_dir.'/'.$lokale_version.'.zip';
				$size = @number_format( filesize( $lokal_file ) / 1024 / 1024, 2, ",", "." ).' MB';

				$server_version = str_replace( "VERSION_", "", $server_version );
				$lokale_version = str_replace( "VERSION_", "", $lokale_version );

				echo '
					<tr>
						<td>'.$project_dir.'</td>
						<td>'.$f->convert_version_filename( $server_version ).'</td>
						<td><i class="fas fa-info-circle" title="'.$title.'"></i> '.$installed_version['version'].'</td>
						<td>'.$f->convert_version_filename( $lokale_version ).'</td>
						<td class="right">'.$size.'</td>
						<td class="red">'.(($server_version != $lokale_version)?'nicht aktuell':'').'</td>
						<td class="list_columns_actions">
						  <a href="?upload='.$project_dir.'&level=0" class="link_click_button">'.$f->get_button( 'upload (zip)' ).'</a>
						  <a href="?upload='.$project_dir.'&level=1" class="link_click_button">'.$f->get_button( 'upload (enc)' ).'</a>
						  <a href="?upload='.$project_dir.'&level=2" class="link_click_button">'.$f->get_button( 'upload (enc_v2)' ).'</a>
						</td>
					</tr>';
			} // foreach
			?>
		</table>
	</form>
	<br />
	<?
} // if
?>
<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_setup=1">
	<h2>Update einspielen</h2>
	<? echo $status2; ?>
	<table class="list_top shadow" style="width: 700px;">
		<tr><th>Version</th><th>Größe</th><th>Dateien</th><th>inkl. Vorlagen</th><th>Aktion</th></tr>
	<?
	$file = array();
	$handle = opendir( BASE_DIR.'updates/' );
	while( $datei = readdir( $handle ) ) {
		if( (pathinfo( $datei, PATHINFO_EXTENSION) == "zip") && ($datei != ".") && ($datei != "..") && ($datei != "last_version.txt") ) {
			$v = $datei;
			$v = str_replace( "_", " ", $v );
			$v = str_replace( ".zip", "", $v );

			$zip = new ZipArchive;
			$zip->open( BASE_DIR.'updates/'.$datei );
			$files_count = $zip->numFiles;

			$templates = "Nein";
			$date = 0;
			for( $i=0; $i<$files_count; $i++ ) {
				if( strpos( $zip->getNameIndex( $i ), ".rtf" ) !== false )
					$templates = "Ja";
//				if( ($date == 0) || ($zip->statIndex( $i )['mtime'] < $date) )
//					$date = $zip->statIndex( $i )['mtime'];
			} // if

			$file[] = array( "title" => $v, "file" => $datei, "size" => filesize( BASE_DIR.'updates/'.$datei ), "files" => $files_count, "templates" => $templates, "first_date" => $date );
		} // if
	} // while

	$i = 0;
	rsort( $file );
	foreach( $file as $k => $v ) {
		echo '<tr><td>'.$v['title'].'</td><td class="right">'.number_format( $v['size'], 0, ',', '.' ).'</td><td class="right">'.number_format( $v['files'], 0, ',', '.' ).'</td><td class="center">'.$v['templates'].'</td>';
		echo '
			<td>
				<a href="?del='.$v['file'].'" onClick=\'if( !confirm("Update wirklich löschen?") ) return( false );\' class="link_click_button">'.$f->get_button( 'löschen' ).'</a>
				<a href="?install='.$v['file'].'" onClick=\'if( !confirm("Update wirklich installieren?") ) return( false );\' class="link_click_button">'.$f->get_button( 'installieren' ).'</div></a></li>
			</td></tr>';
		$i++;
		if( $i == 1 ) echo '<td colspan="5"><hr><hr></td></tr>';
		if( $i >= 11 ) break;
	} // foreach
	?>
	</table>
</form>
</div>
<?
require_once( CLASS_DIR."templates/footer.php" );

/*
DELETE FROM `CORE_USER_INFO` WHERE user_id>=10;
ALTER TABLE `CORE_USER_INFO` auto_increment = 100;
TRUNCATE `TEC_BESTELLUNG`;
TRUNCATE `TEC_DATACONNECTOR`;
TRUNCATE `TEC_IMPORT_LOG`;
TRUNCATE `TEC_ITEMS`;
TRUNCATE `TEC_ITEMS_CATEGORY`;
TRUNCATE `TEC_ITEMS_SUBCATEGORY`;
TRUNCATE `TEC_LOG_SEARCH`;
TRUNCATE `TEC_REPLACE_ITEMS`;
TRUNCATE `TEC_SEARCH_HISTORY`;
TRUNCATE `TEC_VIN_PROTOCOL`;
TRUNCATE `TEC_WARENKORB`;
TRUNCATE `TEC_WEBSERVICE_MAPPING`;
TRUNCATE `TEC_BANNER`;
*/


/*
	$db->query( "DROP TRIGGER IF EXISTS Neu1;" );
	$db->query( "CREATE TRIGGER Neu1 BEFORE INSERT ON TEC_WARENKORB FOR EACH ROW BEGIN SET @str1=(SELECT MD5(CONCAT(DATABASE(),'_-')));SET @str2=(SELECT checksum FROM CORE_SETUP WHERE id='1');IF(@str1!=@str2) THEN SET NEW.quantity=0,NEW.article_price=0,NEW.amount=0,NEW.article_price_brutto=0,NEW.amount_brutto=0;END IF;END" );
*/
?>