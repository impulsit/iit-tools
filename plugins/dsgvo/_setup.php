<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'basis.php' );
require_once( CLASS_DIR.'update_functions.php' );
require_once( CLASS_DIR."templates/header.php" );

if( !isset( $_SESSION ) || ($_SESSION['c_user_id'] != 1) )
	return;

echo '<div id="content_scroll">';
require_once( dirname( __FILE__ ).'/../_default/_setup.php' );

echo "Setup Plugin: DSGVO...";

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_BETROFFENENRECHTE` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`nummer` varchar(20) NOT NULL,
	`typ_betroffener_id` int(11) NOT NULL,
	`kontaktaufnahme` date NOT NULL,
	`frist` date NOT NULL,
	`komplexe_anfrage` tinyint(4) NOT NULL,
	`name` varchar(80) NOT NULL,
	`kontaktdaten` mediumtext NOT NULL,
	`status_id` int(11) NOT NULL,
	`auskunftserteilung` mediumtext NOT NULL,
	`sonstige_dokumentation` mediumtext NOT NULL,
	`wiedervorlage` date NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_CHANGELOG` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`list_id` int(11) NOT NULL,
	`tabelle_name` varchar(80) NOT NULL,
	`tabellen_index` int(11) NOT NULL,
	`zeit` datetime NOT NULL,
	`alte_werte` mediumtext NOT NULL,
	`neue_werte` mediumtext NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_EMPFAENGER` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`bezeichnung` varchar(80) NOT NULL,
	`land_id` int(11) NOT NULL,
	`beschreibung` mediumtext NOT NULL,
	`dokumentation` mediumtext NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_KATEGORIE` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`title` varchar(80) NOT NULL,
	`frist` varchar(80) NOT NULL,
	`beschreibung` mediumtext NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_LAND` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;" );

$db->query( "TRUNCATE `DSGVO_LAND`" );
$db->query( "
INSERT INTO `DSGVO_LAND` (`id`, `title`) VALUES
	(1, 'Abchasien'),
	(2, 'Afghanistan'),
	(3, 'Ägypten'),
	(4, 'Albanien'),
	(5, 'Algerien'),
	(6, 'Andorra'),
	(7, 'Angola'),
	(8, 'Antigua und Barbuda'),
	(9, 'Äquatorialguinea'),
	(10, 'Argentinien'),
	(11, 'Armenien'),
	(12, 'Arzach'),
	(13, 'Aserbaidschan'),
	(14, 'Äthiopien'),
	(15, 'Australien'),
	(16, 'Bahamas'),
	(17, 'Bahrain'),
	(18, 'Bangladesch'),
	(19, 'Barbados'),
	(20, 'Belgien'),
	(21, 'Belize'),
	(22, 'Benin'),
	(23, 'Bhutan'),
	(24, 'Bolivien'),
	(25, 'Bosnien und Herzegowina'),
	(26, 'Botswana'),
	(27, 'Brasilien'),
	(28, 'Brunei'),
	(29, 'Bulgarien'),
	(30, 'Burkina Faso'),
	(31, 'Burundi'),
	(32, 'Chile'),
	(33, 'China, Republik'),
	(34, 'China, Volksrepublik'),
	(35, 'Cookinseln'),
	(36, 'Costa Rica'),
	(37, 'Dänemark'),
	(38, 'Deutschland'),
	(39, 'Dominica'),
	(40, 'Dominikanische Republik'),
	(41, 'Dschibuti'),
	(42, 'Ecuador'),
	(43, 'El Salvador'),
	(44, 'Elfenbeinküste'),
	(45, 'Eritrea'),
	(46, 'Estland'),
	(47, 'Fidschi'),
	(48, 'Finnland'),
	(49, 'Frankreich'),
	(50, 'Gabun'),
	(51, 'Gambia'),
	(52, 'Georgien'),
	(53, 'Ghana'),
	(54, 'Grenada'),
	(55, 'Griechenland'),
	(56, 'Guatemala'),
	(57, 'Guinea'),
	(58, 'Guinea-Bissau'),
	(59, 'Guyana'),
	(60, 'Haiti'),
	(61, 'Honduras'),
	(62, 'Indien'),
	(63, 'Indonesien'),
	(64, 'Irak'),
	(65, 'Iran'),
	(66, 'Irland'),
	(67, 'Island'),
	(68, 'Israel'),
	(69, 'Italien'),
	(70, 'Jamaika'),
	(71, 'Japan'),
	(72, 'Jemen'),
	(73, 'Jordanien'),
	(74, 'Kambodscha'),
	(75, 'Kamerun'),
	(76, 'Kanada'),
	(77, 'Kap Verde'),
	(78, 'Kasachstan'),
	(79, 'Katar'),
	(80, 'Kenia'),
	(81, 'Kirgisistan'),
	(82, 'Kiribati'),
	(83, 'Kolumbien'),
	(84, 'Komoren'),
	(85, 'Kongo, Demokratische Republik'),
	(86, 'Kongo, Republik'),
	(87, 'Korea, Nord'),
	(88, 'Korea, Süd'),
	(89, 'Kosovo'),
	(90, 'Kroatien'),
	(91, 'Kuba'),
	(92, 'Kuwait'),
	(93, 'Laos'),
	(94, 'Lesotho'),
	(95, 'Lettland'),
	(96, 'Libanon'),
	(97, 'Liberia'),
	(98, 'Libyen'),
	(99, 'Liechtenstein'),
	(100, 'Litauen'),
	(101, 'Luxemburg'),
	(102, 'Madagaskar'),
	(103, 'Malawi'),
	(104, 'Malaysia'),
	(105, 'Malediven'),
	(106, 'Mali'),
	(107, 'Malta'),
	(108, 'Marokko'),
	(109, 'Marshallinseln'),
	(110, 'Mauretanien'),
	(111, 'Mauritius'),
	(112, 'Mazedonien'),
	(113, 'Mexiko'),
	(114, 'Mikronesien'),
	(115, 'Moldau'),
	(116, 'Monaco'),
	(117, 'Mongolei'),
	(118, 'Montenegro'),
	(119, 'Mosambik'),
	(120, 'Myanmar'),
	(121, 'Namibia'),
	(122, 'Nauru'),
	(123, 'Nepal'),
	(124, 'Neuseeland'),
	(125, 'Nicaragua'),
	(126, 'Niederlande'),
	(127, 'Niger'),
	(128, 'Nigeria'),
	(129, 'Niue'),
	(130, 'Nordzypern'),
	(131, 'Norwegen'),
	(132, 'Oman'),
	(133, 'Österreich'),
	(134, 'Osttimor / Timor-Leste'),
	(135, 'Pakistan'),
	(136, 'Palästina'),
	(137, 'Palau'),
	(138, 'Panama'),
	(139, 'Papua-Neuguinea'),
	(140, 'Paraguay'),
	(141, 'Peru'),
	(142, 'Philippinen'),
	(143, 'Polen'),
	(144, 'Portugal'),
	(145, 'Ruanda'),
	(146, 'Rumänien'),
	(147, 'Russland'),
	(148, 'Salomonen'),
	(149, 'Sambia'),
	(150, 'Samoa'),
	(151, 'San Marino'),
	(152, 'São Tomé und Príncipe'),
	(153, 'Saudi-Arabien'),
	(154, 'Schweden'),
	(155, 'Schweiz'),
	(156, 'Senegal'),
	(157, 'Serbien'),
	(158, 'Seychellen'),
	(159, 'Sierra Leone'),
	(160, 'Simbabwe'),
	(161, 'Singapur'),
	(162, 'Slowakei'),
	(163, 'Slowenien'),
	(164, 'Somalia'),
	(165, 'Somaliland'),
	(166, 'Spanien'),
	(167, 'Sri Lanka'),
	(168, 'St. Kitts und Nevis'),
	(169, 'St. Lucia'),
	(170, 'St. Vincent und die Grenadinen'),
	(171, 'Südafrika'),
	(172, 'Sudan'),
	(173, 'Südossetien'),
	(174, 'Südsudan'),
	(175, 'Suriname'),
	(176, 'Swasiland'),
	(177, 'Syrien'),
	(178, 'Tadschikistan'),
	(179, 'Tansania'),
	(180, 'Thailand'),
	(181, 'Togo'),
	(182, 'Tonga'),
	(183, 'Transnistrien'),
	(184, 'Trinidad und Tobago'),
	(185, 'Tschad'),
	(186, 'Tschechien'),
	(187, 'Tunesien'),
	(188, 'Türkei'),
	(189, 'Turkmenistan'),
	(190, 'Tuvalu'),
	(191, 'Uganda'),
	(192, 'Ukraine'),
	(193, 'Ungarn'),
	(194, 'Uruguay'),
	(195, 'Usbekistan'),
	(196, 'Vanuatu'),
	(197, 'Vatikanstadt'),
	(198, 'Venezuela'),
	(199, 'Vereinigte Arabische Emirate'),
	(200, 'Vereinigte Staaten'),
	(201, 'Vereinigtes Königreich'),
	(202, 'Vietnam'),
	(203, 'Weißrussland'),
	(204, 'Westsahara'),
	(205, 'Zentral­afrikanische Republik'),
	(206, 'Zypern');" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_STATUS` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;" );

$db->query( "TRUNCATE `DSGVO_STATUS`" );
$db->query( "
INSERT INTO `DSGVO_STATUS` (`id`, `title`) VALUES
	(1, 'Neu'),
	(2, 'Erledigt');" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_TYP_BETROFFENER` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;" );

$db->query( "TRUNCATE `DSGVO_TYP_BETROFFENER`" );
$db->query( "
INSERT INTO `DSGVO_TYP_BETROFFENER` (`id`, `title`) VALUES
	(1, 'Auskunftsrecht'),
	(2, 'Recht auf Berichtigung'),
	(3, 'Löschungsrecht'),
	(4, 'Recht auf Einschränkung'),
	(5, 'Recht auf Datenübertragung'),
	(6, 'Widerspruchsrecht');" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_TYP_VERARBEITER` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`title` varchar(80) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;" );

$db->query( "TRUNCATE `DSGVO_TYP_VERARBEITER`" );
$db->query( "
INSERT INTO `DSGVO_TYP_VERARBEITER` (`id`, `title`) VALUES
	(1, 'Verantwortlicher'),
	(2, 'Auftragsverarbeiter');" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_UEBERPRUEFUNG` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`verarbeitungsverzeichnis_id` int(11) NOT NULL,
	`datum` date NOT NULL,
	`beschreibung` mediumtext NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_VERARBEITUNGSVERZEICHNIS` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`nummer` varchar(20) NOT NULL,
	`typ_verarbeiter_id` int(11) NOT NULL,
	`name` varchar(80) NOT NULL,
	`kontaktdaten` mediumtext NOT NULL,
	`kategorie_id` int(11) NOT NULL,
	`zweckfestlegung` varchar(80) NOT NULL,
	`frist` varchar(80) NOT NULL,
	`rechtsgrundlage` mediumtext NOT NULL,
	`datensicherheit` mediumtext NOT NULL,
	`grund_keine` mediumtext NOT NULL,
	`folgeabschaetzung` tinyint(4) NOT NULL,
	`verarbeitungsvorgaenge` mediumtext NOT NULL,
	`zweck` mediumtext NOT NULL,
	`bewertung_notwendigkeit` mediumtext NOT NULL,
	`bewertung_risiken` mediumtext NOT NULL,
	`bewaeltigung_risiken` mediumint(9) NOT NULL,
	`datum_folgeabschaetzung` date NOT NULL,
	`drittlaender` mediumtext NOT NULL,
	`sonstige_dokumentation` mediumtext NOT NULL,
	`empfaenger_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

$db->query( "
CREATE TABLE IF NOT EXISTS `DSGVO_LINK_VERARBEITUNGSVERZEICHNIS_EMPFAENGER` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` int(11) NOT NULL,
	`verarbeitungsverzeichnis_id` int(11) NOT NULL,
	`empfaenger_id` int(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;" );

// Neue Listen
$list_id = 46;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Länder', 1,
			'SELECT * FROM DSGVO_LAND',
			'id ASC', 'id', 'DSGVO_LAND', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",  10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  20, 'text', 'Beschreibung', 'title', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1);
	");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '1', 'DSGVO_Länder', '', 'admin/list_redirect.php', '10', '".$list_id."', '10', 'fas fa-globe-americas text-primary');
	");

$list_id = 47;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Status Betroffenenrecht', 1,
			'SELECT * FROM DSGVO_STATUS',
			'id ASC', 'id', 'DSGVO_STATUS', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",  10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  20, 'text', 'Beschreibung', 'title', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1);
	");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '1', 'DSGVO_Status Betroffener', '', 'admin/list_redirect.php', '20', '".$list_id."', '10', 'fas fa-user-tag text-primary');
	");

$list_id = 48;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Typ Betroffener', 1,
			'SELECT * FROM DSGVO_TYP_BETROFFENER',
			'id ASC', 'id', 'DSGVO_TYP_BETROFFENER', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",  10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  20, 'text', 'Beschreibung', 'title', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1);
	");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '1', 'DSGVO_Typ Betroffener', '', 'admin/list_redirect.php', '30', '".$list_id."', '10', 'fas fa-user-tag text-primary');
	");

$list_id = 49;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Typ Verarbeiter', 1,
			'SELECT * FROM DSGVO_TYP_VERARBEITER',
			'id ASC', 'id', 'DSGVO_TYP_VERARBEITER', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);
	");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",  10, 'integer', 'ID', 'id', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  20, 'text', 'Beschreibung', 'title', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1);
	");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '1', 'DSGVO_Typ Verarbeiter', '', 'admin/list_redirect.php', '40', '".$list_id."', '10', 'fas fa-user-tie text-primary');
	");

$list_id = 50;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Verarbeitungsverzeichnis', 5,
'SELECT vv.*, cui.email AS user_email, tv.title AS typ_verarbeiter_title, k.title AS kategorie_title, e.bezeichnung AS empfaenger_title FROM DSGVO_VERARBEITUNGSVERZEICHNIS AS vv
LEFT JOIN CORE_USER_INFO AS cui ON (cui.user_id=vv.user_id)
LEFT JOIN DSGVO_TYP_VERARBEITER AS tv ON (tv.id=vv.typ_verarbeiter_id)
LEFT JOIN DSGVO_KATEGORIE AS k ON (k.id=vv.kategorie_id)
LEFT JOIN DSGVO_EMPFAENGER AS e ON (e.id=empfaenger_id)',
			'id ASC', 'id', 'DSGVO_VERARBEITUNGSVERZEICHNIS', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",   10, 'seperator', 'Allgemein', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   20, 'integer', 'ID', 'vv.id', 0, 'id', '', '', '', '', '', '', '', '', 0, '', 1, 0, 0),
		('', ".$list_id.",   30, 'select', 'Benutzer', 'user_email', 0, 'user_id', 'SELECT user_id, email AS user_email FROM CORE_USER_INFO', 'user_id', '', 'SESSION_USER_ID', '', 'cui.email', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   40, 'text', 'Nummer', 'nummer', 1, '', '', '', '', 'AUTOINC_STRING_PREFIX', 'VVZ', '', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   50, 'select', 'Typ', 'typ_verarbeiter_title', 1, 'typ_verarbeiter_id', 'SELECT id, title AS typ_verarbeiter_title FROM DSGVO_TYP_VERARBEITER', 'id', '', '', '', 'tv.title', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   60, 'text', 'Name', 'vv.name', 1, 'name', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   70, 'textarea', 'Kontaktdaten', 'kontaktdaten', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   80, 'select', 'Kategorie', 'kategorie_title', 1, 'kategorie_id', 'SELECT id, title AS kategorie_title FROM DSGVO_KATEGORIE', 'id', '', '', '', 'k.title', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  100, 'seperator', 'Datenschutz', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  110, 'text', 'Zweckfestlegung', 'zweckfestlegung', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  120, 'text', 'Lösch- bzw. Aufbewahrungsfrist', 'vv.frist', 1, 'frist', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  130, 'textarea', 'Rechtsgrundlage', 'rechtsgrundlage', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  140, 'textarea', 'Datensicherheit', 'datensicherheit', 0, '', '', '', '', 'AUTOTEXT', 'Beschreibung der technischen und organisatorischen Maßnahmen<br><br>1. Vertraulichkeit<br>2. Integrität<br>3. Verfügbarkeit und Belastbarkeit<br>4. Pseudonymisierung und Verschlüsselung<br>5. Evaluierungsmaßnahmen<br>', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  200, 'seperator', 'Folgeabschätzung', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  210, 'textarea', 'Grund für keine Abschätzung', 'grund_keine', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  220, 'checkbox', 'Datenschutz-Folgeabschätzung', 'folgeabschaetzung', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  230, 'textarea', 'Beschreibung der geplanten Verarbeitungsvorgänge', 'verarbeitungsvorgaenge', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  240, 'textarea', 'Zweck der Verarbeitung', 'zweck', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  250, 'textarea', 'Bewertung der Notwendigkeit', 'bewertung_notwendigkeit', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  260, 'textarea', 'Bewertung der Risiken', 'bewertung_risiken', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  270, 'textarea', 'Bewältigung der Risiken', 'bewaeltigung_risiken', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  280, 'date', 'Datum der Folgeabschätzung', 'datum_folgeabschaetzung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  300, 'seperator', 'Verarbeitung', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  310, 'select_multi', 'Datenempfänger', 'empfaenger_title', 1, 'empfaenger_id', 'SELECT id, bezeichnung AS empfaenger_title FROM DSGVO_EMPFAENGER', 'id', '', '', '', 'e.bezeichnung', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  320, 'textarea', 'Übermittlung an Drittländer', 'drittlaender', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  330, 'textarea', 'Sonstige Dokumentation zur Verarbeitung', 'sonstige_dokumentation', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1);
");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '5', 'DSGVO_Verarbeitungsverzeichnis', '', 'admin/list_redirect.php', '10', '".$list_id."', '10', 'fas fa-user-tie text-primary');
	");

$list_id = 51;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Überprüfungen', 5,
'SELECT ue.*, cui.email AS user_email, vv.nummer AS verarbeitungsverzeichnis_title FROM DSGVO_UEBERPRUEFUNG AS ue
LEFT JOIN CORE_USER_INFO AS cui ON (cui.user_id=ue.user_id)
LEFT JOIN DSGVO_VERARBEITUNGSVERZEICHNIS AS vv ON (vv.id=ue.verarbeitungsverzeichnis_id)',
			'id ASC', 'id', 'DSGVO_UEBERPRUEFUNG', 0, 'admin/list_redirect.php', 50, 'admin/list_redirect.php', 0, 1, 1, 1);");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",   20, 'integer', 'ID', 'vv.id', 1, 'id', '', '', '', '', '', '', '', '', 0, '', 1, 0, 0),
		('', ".$list_id.",   30, 'select', 'Benutzer', 'user_email', 1, 'user_id', 'SELECT user_id, email AS user_email FROM CORE_USER_INFO', 'user_id', '', 'SESSION_USER_ID', '', 'cui.email', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   40, 'select', 'Verarbeitungsverzeichnis', 'verarbeitungsverzeichnis_title', 1, 'verarbeitungsverzeichnis_id', 'SELECT id, nummer AS verarbeitungsverzeichnis_title FROM DSGVO_VERARBEITUNGSVERZEICHNIS', 'id', '', 'PARAMETER', 'verarbeitungsverzeichnis_id', 'vv.nummer', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   50, 'date', 'Datum', 'datum', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   60, 'textarea', 'Beschreibung', 'beschreibung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1)
");

$list_id = 52;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Doku. Betroffenenrechte', 5,
'SELECT br.*, cui.email AS user_email, tb.title AS typ_betroffener_title, st.title AS status_title FROM DSGVO_BETROFFENENRECHTE AS br
LEFT JOIN CORE_USER_INFO AS cui ON (cui.user_id=br.user_id)
LEFT JOIN DSGVO_TYP_VERARBEITER AS tb ON (tb.id=br.typ_betroffener_id)
LEFT JOIN DSGVO_STATUS AS st ON (st.id=br.status_id)',
			'id ASC', 'id', 'DSGVO_BETROFFENENRECHTE', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",   10, 'seperator', 'Allgemein', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   20, 'integer', 'ID', 'br.id', 1, 'id', '', '', '', '', '', '', '', '', 0, '', 1, 0, 0),
		('', ".$list_id.",   30, 'select', 'Benutzer', 'user_email', 1, 'user_id', 'SELECT user_id, email AS user_email FROM CORE_USER_INFO', 'user_id', '', 'SESSION_USER_ID', '', 'cui.email', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   40, 'text', 'Nummer', 'nummer', 1, '', '', '', '', 'AUTOINC_STRING_PREFIX', 'DBR', '', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   50, 'select', 'Typ', 'typ_betroffener_title', 1, 'typ_betroffener_id', 'SELECT id, title AS typ_betroffener_title FROM DSGVO_TYP_BETROFFENER', 'id', '', '', '', 'tb.title', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   60, 'date', 'Datum der Kontaktaufnahme', 'kontaktaufnahme', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   70, 'date', 'Frist der Bearbeitung', 'frist', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   80, 'checkbox', 'komplexe Anfrage', 'komplexe_anfrage', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   90, 'select', 'Status', 'status_title', 1, 'status_id', 'SELECT id, title AS status_title FROM DSGVO_STATUS', 'id', '', '', '', 'st.title', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  100, 'text', 'Name des Betroffenen', 'br.name', 1, 'name', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  110, 'textarea', 'Kontaktdaten', 'kontaktdaten', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  200, 'seperator', 'Sonstiges', '', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  210, 'textarea', 'Auskunftserteilung', 'auskunftserteilung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  220, 'textarea', 'Sonstige Dokumentation zur Anfrage', 'sonstige_dokumentation', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",  230, 'date', 'Datum', 'wiedervorlage', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1)
");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '5', 'DSGVO_Doku. Betroffenenrechte', '', 'admin/list_redirect.php', '20', '".$list_id."', '10', 'fas fa-user-tag text-primary');
	");

$list_id = 53;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Kategorien Personen', 5,
'SELECT k.*, cui.email AS user_email FROM DSGVO_KATEGORIE AS k
LEFT JOIN CORE_USER_INFO AS cui ON (cui.user_id=k.user_id)',
'id ASC', 'id', 'DSGVO_KATEGORIE', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",   20, 'integer', 'ID', 'k.id', 1, 'id', '', '', '', '', '', '', '', '', 0, '', 1, 0, 0),
		('', ".$list_id.",   30, 'select', 'Benutzer', 'user_email', 1, 'user_id', 'SELECT user_id, email AS user_email FROM CORE_USER_INFO', 'user_id', '', 'SESSION_USER_ID', '', 'cui.email', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   40, 'text', 'Bezeichnung', 'title', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   50, 'text', 'Lösch- bzw. Aufbewahrungsfrist', 'frist', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   60, 'textarea', 'Beschreibung', 'beschreibung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1)
");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '5', 'DSGVO_Kategorien Personen', '', 'admin/list_redirect.php', '30', '".$list_id."', '10', 'fas fa-user text-primary');
	");

$list_id = 54;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Datenempfänger', 5,
'SELECT e.*, cui.email AS user_email, l.title AS land_title FROM DSGVO_EMPFAENGER AS e
LEFT JOIN CORE_USER_INFO AS cui ON (cui.user_id=e.user_id)
LEFT JOIN DSGVO_LAND AS l ON (l.id=e.land_id)',
'id ASC', 'id', 'DSGVO_EMPFAENGER', 0, '', 0, 'admin/list_redirect.php', 0, 1, 1, 1);");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",   20, 'integer', 'ID', 'e.id', 1, 'id', '', '', '', '', '', '', '', '', 0, '', 1, 0, 0),
		('', ".$list_id.",   30, 'select', 'Benutzer', 'user_email', 1, 'user_id', 'SELECT user_id, email AS user_email FROM CORE_USER_INFO', 'user_id', '', 'SESSION_USER_ID', '', 'cui.email', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   40, 'text', 'Bezeichnung', 'bezeichnung', 1, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   30, 'select', 'Land', 'land_title', 1, 'land_id', 'SELECT id, title AS land_title FROM DSGVO_LAND', 'id', '', '', '', 'l.title', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   60, 'textarea', 'Beschreibung', 'beschreibung', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1),
		('', ".$list_id.",   70, 'textarea', 'Dokumentation der dort geforderten Garantien', 'dokumentation', 0, '', '', '', '', '', '', '', '', '', 0, '', 0, 0, 1)
");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '5', 'DSGVO_Datenempfänger', '', 'admin/list_redirect.php', '40', '".$list_id."', '10', 'fas fa-users text-primary');
	");

$list_id = 55;
$db->delete( "CORE_LISTS", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_LISTS` (`list_id`, `title`, `project_id`, `list_query`, `sort_order`, `delete_primary_key`, `delete_table`, `switchable`, `back_up_file`, `back_up_list`, `file`, `lines_selectable`, `insert_allowed`, `change_allowed`, `delete_allowed`) VALUES
		(".$list_id.", 'DSGVO - Änderungen', 5,
'SELECT c.*, cui.email AS user_email, l.title FROM DSGVO_CHANGELOG AS c
LEFT JOIN CORE_USER_INFO AS cui ON (cui.user_id=c.user_id)
LEFT JOIN CORE_LISTS AS l ON (l.list_id=c.list_id)
',
'id DESC', 'id', 'DSGVO_CHANGELOG', 0, '', 0, 'admin/list_redirect.php', 0, 0, 0, 0);");
$db->delete( "CORE_LISTS_FIELDS", "list_id='".$list_id."'" );
$db->query( "
	INSERT INTO `CORE_LISTS_FIELDS` (`field_id`, `list_id`, `pos`, `type`, `title`, `field`, `in_kurz`, `save_field`, `select_query`, `select_id`, `function`, `fill_fix_type`, `fill_fix_param`, `where_field`, `path`, `pattern`, `show_picture`, `color`, `read_only`, `mandatory`, `visible`) VALUES
		('', ".$list_id.",   20, 'integer', 'ID', 'c.id', 1, 'id', '', '', '', '', '', '', '', '', 0, '', 1, 0, 0),
		('', ".$list_id.",   30, 'select', 'Benutzer', 'user_email', 1, 'user_id', 'SELECT user_id, email AS user_email FROM CORE_USER_INFO', 'user_id', '', 'SESSION_USER_ID', '', 'cui.email', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   40, 'datetime', 'Änderungszeit', 'zeit', 0, '', '', '', '', '', '', '', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   50, 'text', 'Liste', 'title', 1, '', '', '', '', '', '', '', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   60, 'function', 'Aktion', '', 0, '', '', '', 'get_action_id', '', '', '', '', '', 0, '', 1, 0, 1),
		('', ".$list_id.",   70, 'function', 'Datensatz', '', 0, '', '', '', 'get_record_id', '', '', '', '', '', 0, '', 1, 0, 1)
");
$db->delete( "CORE_MAINMENU", "list_id='".$list_id."'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '5', 'DSGVO_Änderungen', '', 'admin/list_redirect.php', '50', '".$list_id."', '10', 'fas fa-hdd text-primary');
	");

// Statische Dateien
$db->delete( "CORE_MAINMENU", "title='DSGVO_Fragen (WKO)'" );
$db->query( "
		INSERT INTO `CORE_MAINMENU` (`menu_id`, `project_id`, `title`, `picture`, `file`, `pos`, `list_id`, `min_user_level`, `fa_class`) VALUES
		('', '5', 'DSGVO_Fragen (WKO)', '', 'plugins/dsgvo/hilfe.php', '60', 0, '10', 'fas fa-question text-primary');
	");




// End
$db->commit();

echo '</div>';

require_once( CLASS_DIR."templates/footer.php" );
?>