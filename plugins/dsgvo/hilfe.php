<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'basis.php' );

unset( $_SESSION['list_id'] );

require_once( CLASS_DIR."templates/header.php" );
?>
<div id="content_scroll">
	<div class="alert alert-success" role="alert">
		<h2><img src="images/logo-wko.png" style="height: 16px;"> EU-Datenschutz-Grundverordnung (DSGVO)</h2>
		<a class="dotted_link" href="https://www.wko.at/service/wirtschaftsrecht-gewerberecht/EU-Datenschutz-Grundverordnung.html" target="_blank">Überblick zum Datenschutz in Österreich</a><br />
	</div>

	<div class="alert alert-success" role="alert">
		<h2><img src="images/logo-wko.png" style="height: 16px;"> FAQ zur DSGVO</h2>
		Was Unternehmen auf jeden Fall wissen müssen.<br />
		<a class="dotted_link" href="https://www.wko.at/service/unternehmensfuehrung-finanzierung-foerderungen/datenschutz-grundverordnung-fragen-und-antworten.html" target="_blank">Die wichtigsten Fragen und Antworten</a><br />
	</div>

	<div class="alert alert-success" role="alert">
		<h2><img src="images/logo-wko.png" style="height: 16px;"> Fragen und Antworten nach Themenbereichen</h2>
		Detailfragen zu speziellen Sachgebieten.<br />
		<a class="dotted_link" href="https://www.wko.at/service/wirtschaftsrecht-gewerberecht/faq-sammlung-webinar-eu-dsgvo.html#heading_FAQ_nach_Themengebieten" target="_blank">600 Fragen und Antworten nach Themenbereichen</a><br />
	</div>

	<div class="alert alert-success" role="alert">
		<h2><img src="images/logo-wko.png" style="height: 16px;"> Musterdokumente zur EU-Datenschutz-Grundverordnung</h2>
		<a class="dotted_link" href="https://www.wko.at/service/wirtschaftsrecht-gewerberecht/Musterdokumente-zur-EU-Datenschutzgrundverordnung.html" target="_blank">Übersicht</a><br />
	</div>
</div>
<?php
require_once( CLASS_DIR."templates/footer.php" );
?>