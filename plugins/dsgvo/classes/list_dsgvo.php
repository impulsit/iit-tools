<?php
require_once( CLASS_DIR.'mysql.php' );
require_once( CLASS_DIR.'functions.php' );

class list_dsgvo {
	static private $instance = null;
	private $db = null;
	private $f = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		$this->db = mysql::getInstance();
		$this->f = functions::getInstance();
	} // __construct

	function get_special_select_where( $list_id, $field ) {
		$str = null;

		switch( $list_id ) {
			case 50:
					switch( $field ) {
						case 'nummer': // für Autoinc Wert
						case 'user_email':
						case 'empfaenger_title':
						case 'kategorie_title':
							$str = " WHERE user_id='".$_SESSION['c_user_id']."'";
							break;
					} // switch
				break;
			case 51: // Überprüfungen
				switch( $field ) {
					case 'verarbeitungsverzeichnis_title': // für Autoinc Wert
						$str = " WHERE user_id='".$_SESSION['c_user_id']."'";
						break;
				} // switch
				break;
		} // switch

		return( $str );
	} // get_special_select_where

	function get_spezial_filter( $list_id ) {
		switch( $list_id ) {
			case 50: // Verarbeitungsverzeichnis
					return( " AND (vv.user_id='".$_SESSION['c_user_id']."')" );
				break;
			case 51: // Überprüfungen
					return( " AND (ue.user_id='".$_SESSION['c_user_id']."')" );
				break;
			case 52: // Doku. Betroffenenrechte
					return( " AND (br.user_id='".$_SESSION['c_user_id']."')" );
				break;
			case 53: // Kategorien Personen
					return( " AND (k.user_id='".$_SESSION['c_user_id']."')" );
				break;
			case 54: // Datenempfänger
					return( " AND (e.user_id='".$_SESSION['c_user_id']."')" );
				break;
			case 55: // Changelog
					return( " AND (c.user_id='".$_SESSION['c_user_id']."')" );
				break;
		} // switch
	} // get_spezial_filter

	function check_multi_select_link_table( $list_id, $primary_key, $field, $value ) {
		$str = '';

		switch( $list_id ) {
			case 50: // Verarbeitungsverzeichnis
					switch( $field ) {
						case 'empfaenger_title':
								if( $this->db->query( "
													SELECT empfaenger_id
													FROM DSGVO_LINK_VERARBEITUNGSVERZEICHNIS_EMPFAENGER
													WHERE user_id='".$_SESSION['c_user_id']."' AND verarbeitungsverzeichnis_id='".$primary_key."' AND empfaenger_id='".$value."'", "check_multi_select_link_table" ) )
									$str = 'selected';
							break;
					} // switch
				break;
		} // switch

		return( $str );
	} // check_multi_select_link_table

	function write_multi_select_link_table( $list_id, $primary_key, $field ) {
		$str = '';

		switch( $list_id ) {
			case 50: // Verarbeitungsverzeichnis
				switch( $field ) {
					case 'empfaenger_title':
							$this->db->delete( "DSGVO_LINK_VERARBEITUNGSVERZEICHNIS_EMPFAENGER", "user_id='".$_SESSION['c_user_id']."'" );
							if( isset( $_POST[$field] ) )
								foreach( $_POST[$field] as $k => $v ) {
									$this->db->insert( "DSGVO_LINK_VERARBEITUNGSVERZEICHNIS_EMPFAENGER", array( "user_id" => $_SESSION['c_user_id'], "verarbeitungsverzeichnis_id" => $primary_key, "empfaenger_id" => $v ) );
								} // foreach
							$this->db->commit();
						break;
				} // switch
				break;
		} // switch

		return( $str );
	} // write_multi_select_link_table

	function get_select_multi_for_list( $list_id, $primary_key, $field ) {
		$str = '';

		switch( $list_id ) {
			case 50: // Verarbeitungsverzeichnis
				switch( $field ) {
					case 'empfaenger_title':
						$this->db->query( "
							SELECT e.bezeichnung
							FROM DSGVO_LINK_VERARBEITUNGSVERZEICHNIS_EMPFAENGER AS l
							LEFT JOIN DSGVO_EMPFAENGER AS e ON (e.id=l.empfaenger_id)
							WHERE l.user_id='".$_SESSION['c_user_id']."' AND l.verarbeitungsverzeichnis_id='".$primary_key."'", "get_select_multi_for_list" );
						while( $this->db->isNext( "get_select_multi_for_list" ) ) {
							$r = $this->db->getNext( "get_select_multi_for_list" );

							if( $str != '' ) $str .= ', ';
							$str .= $r['bezeichnung'];
						} // while

						break;
				} // switch
				break;
		} // switch

		return( $str );
	} // get_select_multi_for_list

	function add_special_line_buttons( $list_id, $values ) {
		switch( $list_id ) {
			case 50: // Verarbeitungsverzeichnis
					echo '<a href="list_redirect.php?indiv_list_id=51&verarbeitungsverzeichnis_id='.$values['id'].'" class="link_click_button">'.$this->f->get_button( 'Überprüfungen' ).'</a>';
				break;
		} // switch
	} // add_special_line_buttons

	function print_menu_info( $list_id ) {
		switch( $list_id ) {
			case 50: // Verarbeitungsverzeichnis
					echo '<div class="alert alert-success" role="alert">
						<h2><img src="../plugins/dsgvo/images/logo-wko.png" style="height: 16px;"> EU-Datenschutz-Grundverordnung (DSGVO)</h2>
						<a class="dotted_link" href="https://www.wko.at/service/unternehmensfuehrung-finanzierung-foerderungen/eu-dsgvo-verarbeitungsverzeichnis-faq.html" target="_blank">Verarbeitungsverzeichnis nach der EU-Datenschutz-Grundverordnung - FAQ</a><br />
					</div>';
				break;
			case 52: // Doku. Betroffenenrechte
				echo '<div class="alert alert-success" role="alert">
						<h2><img src="../plugins/dsgvo/images/logo-wko.png" style="height: 16px;"> EU-Datenschutz-Grundverordnung (DSGVO)</h2>
						<a class="dotted_link" href="https://www.wko.at/service/wirtschaftsrecht-gewerberecht/EU-Datenschutz-Grundverordnung:-Betroffenenrechte.html" target="_blank">Die Betroffenenrechte im Überblick</a><br />
					</div>';
				break;
			case 53: // Kategorien Personen
				echo '<div class="alert alert-success" role="alert">
						<h2><img src="../plugins/dsgvo/images/logo-wko.png" style="height: 16px;"> EU-Datenschutz-Grundverordnung (DSGVO)</h2>
						Beschreibung der Kategorien betroffener Personen und der Kategorien personenbezogener Daten (z.B. Kunden und Lieferanten; Rechnungsdaten, Adressdaten).<br />
						<a class="dotted_link" href="https://www.wko.at/service/wirtschaftsrecht-gewerberecht/EU-Datenschutz-Grundverordnung:-Wichtige-Begriffsbestimmu.html" target="_blank">Personenbezogene Daten, Verarbeitung, Einwilligung, Verantwortlicher, Auftragsverarbeiter etc.</a><br />
					</div>';
				break;
			case 54: // Datenempfänger
				echo '<div class="alert alert-success" role="alert">
						<h2><img src="../plugins/dsgvo/images/logo-wko.png" style="height: 16px;"> EU-Datenschutz-Grundverordnung (DSGVO)</h2>
						Als "Empfänger" bezeichnet die DSGVO eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, der personenbezogene Daten offengelegt werden, unabhängig davon, ob es sich bei ihr um einen Dritten handelt oder nicht.<br />
						<a class="dotted_link" href="https://www.wko.at/service/wirtschaftsrecht-gewerberecht/EU-Datenschutz-Grundverordnung:-Wichtige-Begriffsbestimmu.html" target="_blank">Personenbezogene Daten, Verarbeitung, Einwilligung, Verantwortlicher, Auftragsverarbeiter etc.</a><br />
					</div>';
				break;
		} // switch
	} // print_menu_info

	function write_changelog( $list_id, $primary_key, $table_name, $old_values, $new_values ) {
		if( ($list_id >= 50) && ($list_id <= 54) && ($old_values != $new_values) ) {
			if( $old_values == array() ) $old_values = ""; else $old_values = print_r( $old_values, true );
			if( $new_values == array() ) $new_values = ""; else $new_values = print_r( $new_values, true );

			$this->db->insert( "DSGVO_CHANGELOG", array(
				"user_id" => $_SESSION['c_user_id'],
				"list_id" => $list_id,
				"tabellen_name" => $table_name,
				"tabellen_index" => $primary_key,
				"zeit" => date( "Y-m-d H:i:s", time() ),
				"alte_werte" => $old_values,
				"neue_werte" => $new_values
			) );
			$this->db->commit();
		} // switch
	} // write_changelog

	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$output = null;
		$c = '';

		switch( $list_id ) {
			case 55: // Changelog
				switch( $function ) {
					case "get_action_id":
							$this->db->query( "SELECT alte_werte, neue_werte FROM DSGVO_CHANGELOG WHERE id='".$primary_key."'", "get_special_functions" );
							$r = $this->db->getNext( "get_special_functions" );

							if( ($r['alte_werte'] == '') && ($r['neue_werte'] != '') ) $output = 'Angelegt';
							if( ($r['alte_werte'] != '') && ($r['neue_werte'] == '') ) $output = 'Gelöscht';
							if( ($r['alte_werte'] != '') && ($r['neue_werte'] != '') ) $output = 'Änderung';
						break;
					case "get_record_id":
							$this->db->query( "SELECT list_id, tabellen_name, tabellen_index, alte_werte, neue_werte FROM DSGVO_CHANGELOG WHERE id='".$primary_key."'", "get_special_functions" );
							$r = $this->db->getNext( "get_special_functions" );

							if( ($r['alte_werte'] == '') && ($r['neue_werte'] != '') ) $parse_field = $r['neue_werte'];
							if( ($r['alte_werte'] != '') && ($r['neue_werte'] == '') ) $parse_field = $r['alte_werte'];
							if( ($r['alte_werte'] != '') && ($r['neue_werte'] != '') ) $parse_field = $r['neue_werte'];

							switch( $r['list_id'] ) {
								case 50: // Verarbeitungsverzeichnis
										$field = "nummer";
									break;
								case 51: // Überprüfungen
										$field = "datum";
									break;
								case 52: // Doku. Betroffenenrechte
										$field = "nummer";
									break;
								case 53: // Kategorien Personen
										$field = "title";
									break;
								case 54: // Datenempfänger
										$field = "bezeichnung";
									break;
							} // switch

							$start = strpos( $parse_field, $field ) + strlen( $field ) + 5;
							$ende = strpos( $parse_field, "\n", $start ) - 1;

							$output = substr( $parse_field, $start, ($ende - $start) + 1 );
						break;
				} // switch
				break;
		} // switch

		return( $output );
	} // get_special_functions

} // list_dsgvo
?>