<?php
require_once( "../../classes/config_data.php" );
require_once( CLASS_DIR."basis.php" );

unset( $_SESSION['list_id'] );

if( isset( $_GET['change_pw'] ) ) {
	if( ($_POST['current'] != "") && ($_POST['new1'] != "") && ($_POST['new2'] != "") ) {
		$pw = new PasswordHash( 8, false );

		$db->query( "SELECT user_id, password FROM CORE_USER_INFO WHERE user_id='".$c_user_id."'" );
		$userinfo = $db->getNext();

		if( !$pw->CheckPassword( $_POST['current'], $userinfo['password'] ) )
			$mes->addError( "Das aktuelle Passwort ist nicht korrekt." );
		if( mb_strlen( $_POST['new1'] ) < 8 )
			$mes->addError( "Das neue Passwort muss mindestens 8 Zeichen haben." );
		if( $_POST['new1'] != $_POST['new2'] )
			$mes->addError( "Das neue Passwort stimmt nicht mit der Bestätigung überein." );

		if( $mes->noError() ) {
			$db->update( "CORE_USER_INFO", array( "password" => $pw->HashPassword( $_POST['new1'] ) ), "user_id='".$c_user_id."'" );
			$db->commit();

			$mes->addInfo( "Das Passwort wurde geändert." );
		} // if
	} // if
} // if

if( isset( $_GET['save_user_info'] ) ) {
	$db->update( "CORE_USER_INFO", array( "name" => $_POST['name'], "info" => $_POST['info'] ), "user_id='".$c_user_id."'" );
	$db->commit();

	$mes->addInfo( "Änderungen wurden gespeichert." );
} // if

require_once( CLASS_DIR."templates/header.php" );

$userinfo = $f->load_user( $c_user_id, "email, name, info, user_level" );

?>
	<div class="row">
		<div class="col-sm-12 col-md-6 sr">
			<h2 style="margin-bottom: 0px;"><? echo $t->t( 'Benutzerdaten ändern'); ?></h2>
			<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?save_user_info=1">
				<div class="list_left shadow accent-cell sr">
					<div class="form">
						<input type="text" class="form-control form-control-sm" name="email" placeholder="<? echo $t->t( 'Login'); ?>" value="<? echo $userinfo['email']; ?>" disabled/>
					</div>
					<div class="form sr">
						<input type="text" class="form-control form-control-sm" name="name" placeholder="<? echo $t->t( 'Name'); ?>" value="<? echo $userinfo['name']; ?>"/>
					</div>
					<div class="form sr">
						<textarea class="form-control form-control-sm" rows="5" name="info" placeholder="<? echo $t->t( 'Info'); ?>"><? echo $userinfo['info']; ?></textarea>
					</div>
					<div class="sr" style="text-align: right;">
						<a href="#" onClick="$(this).closest('form').submit()"><? $f->print_button( 'speichern' ); ?></a>
					</div>
				</div>
			</form>
		</div>
		
		<div class="col-sm-12 col-md-6 sr">
			<h2 style="margin-bottom: 0px;"><? echo $t->t( 'Passwort ändern'); ?></h2>
			<form method="post" action="<? echo $_SERVER['SCRIPT_NAME']; ?>?change_pw=1">
				<div class="list_left shadow accent-cell sr">
					<div class="form">
						<input type="password" class="form-control form-control-sm" name="current" placeholder="<? echo $t->t( 'aktuelles Passwort'); ?>"/>
					</div>
					<div class="form sr">
						<input type="password" class="form-control form-control-sm" name="new1" placeholder="<? echo $t->t( 'neues Passwort'); ?>"/>
					</div>
					<div class="form sr">
						<input type="password" class="form-control form-control-sm" name="new2" placeholder="<? echo $t->t( 'neues Passwort wiederholen'); ?>"/>
					</div>
					<div class="sr" style="text-align: right;">
						<a href="#" onClick="$(this).closest('form').submit()"><? $f->print_button( 'speichern' ); ?></a>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php 
require_once( CLASS_DIR."templates/footer.php" );
?>