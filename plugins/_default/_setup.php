<?php
require_once( dirname( __FILE__ ).'/../../classes/config_data.php' );
require_once( CLASS_DIR.'basis.php' );
require_once( CLASS_DIR.'update_functions.php' );

if( !isset( $_SESSION ) || ($_SESSION['c_user_id'] != 1) )
	return;

echo "Setup Plugin Basis...<br />";

// Basis Einstellungen
if( !$db->query( "SELECT project_id FROM CORE_PROJECTS WHERE project_id='5'" ) )
	$db->insert( "CORE_PROJECTS", array( "project_id" => 5, "title" => "Toolbox", "fa_class" => "fas fa-toolbox text-primary" ) );

$db->delete( "CORE_MAINMENU", "project_id='5' AND pos='99999'" );
$db->insert( "CORE_MAINMENU", array( "project_id" => 5, "title" => "Einstellungen", "file" => "plugins/_default/settings.php", "pos" => 99999, "min_user_level" => 10, "fa_class" => "fas fa-cog text-primary" ) );

// End
$db->commit();
?>