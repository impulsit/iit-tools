<?php
if( file_exists( CLASS_DIR."../plugins/dsgvo/classes/list_dsgvo.php" ) )
	require_once( CLASS_DIR."../plugins/dsgvo/classes/list_dsgvo.php" );

class list_plugins {
	static private $instance = null;
	private $dsgvo = null;

	static public function getInstance() {
		if( self::$instance === null ) {
			self::$instance = new self;
		}
		return self::$instance;
	} // getInstance

	public function __construct() {
		if( class_exists( 'list_dsgvo' ) )
			$this->dsgvo = list_dsgvo::getInstance();
	} // __construct

	function get_special_select_where( $list_id, $field ) {
		$str = null;

		if( ($str === null) && ($this->dsgvo !== null) ) $str = $this->dsgvo->get_special_select_where( $list_id, $field );

		return( $str );
	} // get_special_select_where

	function get_spezial_filter( $list_id ) {
		$str = null;

		if( ($str === null) && ($this->dsgvo !== null) ) $str = $this->dsgvo->get_spezial_filter( $list_id );

		return( $str );
	} // get_spezial_filter

	function check_multi_select_link_table( $list_id, $primary_key, $field, $value ) {
		$str = null;

		if( ($str === null) && ($this->dsgvo !== null) ) $str = $this->dsgvo->check_multi_select_link_table( $list_id, $primary_key, $field, $value );

			return( $str );
	} // check_multi_select_link_table

	function write_multi_select_link_table( $list_id, $primary_key, $field ) {
		$str = null;

		if( ($str === null) && ($this->dsgvo !== null) ) $str = $this->dsgvo->write_multi_select_link_table( $list_id, $primary_key, $field );

		return( $str );
	} // write_multi_select_link_table

	function get_select_multi_for_list( $list_id, $primary_key, $field ) {
		$str = null;

		if( ($str === null) && ($this->dsgvo !== null) ) $str = $this->dsgvo->get_select_multi_for_list( $list_id, $primary_key, $field );

		return( $str );
	} // get_select_multi_for_list

	function add_special_line_buttons( $list_id, $values ) {
		if( $this->dsgvo !== null ) $this->dsgvo->add_special_line_buttons( $list_id, $values );
	} // add_special_line_buttons

	function print_menu_info( $list_id ) {
		if( $this->dsgvo !== null ) $this->dsgvo->print_menu_info( $list_id );
	} // print_menu_info

	function write_changelog( $list_id, $primary_key, $table_name, $old_values, $new_values ) {
		if( $this->dsgvo !== null ) $str = $this->dsgvo->write_changelog( $list_id, $primary_key, $table_name, $old_values, $new_values );
	} // write_changelog

	function get_special_functions( $list_id, $function, $primary_key, &$c ) {
		$str = null;

		if( ($str === null) && ($this->dsgvo !== null) ) $str = $this->dsgvo->get_special_functions( $list_id, $function, $primary_key, $c );

		return( $str );
	} // get_special_functions

} // list_plugins
?>