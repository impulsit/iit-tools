<?php
$_POST = json_decode(file_get_contents('php://input'), true);

$return = array();

$function = key( $_POST );
$param = $_POST[$function];

$return[result] = $param; 

header( 'Content-type: application/json' );
echo json_encode( $return );
?>
