<?php
$_POST = json_decode(file_get_contents('php://input'), true);

$return = array();

$function = key( $_POST );
$param = $_POST[$function];

$return[result] = $param; 

function array_to_xml(array $arr, SimpleXMLElement $xml) {
    foreach ($arr as $k => $v) {
        is_array($v)
            ? array_to_xml($v, $xml->addChild($k))
            : $xml->addChild($k, $v);
    }
    return $xml;
} // array_to_xml

header('Content-type: application/xml');
echo array_to_xml($param, new SimpleXMLElement('<root/>'))->asXML();
?>
