<?php
require_once( CLASS_DIR.'nusoap/lib/nusoap.php' );

function doAuthenticate() {
	if (isset($_SERVER['PHP_AUTH_USER']) and isset($_SERVER['PHP_AUTH_PW'])) {
		if( ($_SERVER['PHP_AUTH_USER'] == "mis") && ($_SERVER['PHP_AUTH_PW'] == "mismis") )
			return true;
	}
	
	return false;	
} // doAuthenticate

function getProd( $function = null, $client_id = null ) {
	if( !doAuthenticate() )
		return( "Invalid username (".$_SERVER['PHP_AUTH_USER'].") or password." );
	
	return array(
			"1" => array( "id" => 1, "name" => "name 1" ),
			"2" => array( "id" => 2, "name" => "name 2" ),
			"3" => array( "id" => 3, "name" => "name 3" ),
	);
/*
}
	else {
		return "No products listed under that category: ".print_r( $category, true );
	}
*/	
}

function addItemInventory( $input = null ) {
	/* Return
	 * -4: Login / Password failed
	 * -3: Database insert Error
	 * -2: Record existing
	 * -1: Primary Key not filled
	 * 1: OK, inserted
	 * 2: OK, replaced
	 */
	if( !doAuthenticate() )
		return( -4 );
	
	$db = mysql::getInstance();

	if( isset( $input['manufacturer'] ) ) $manufacturer = $input['manufacturer']; else $manufacturer = "";
	if( isset( $input['manufacturer_no'] ) ) $manufacturer_no = $input['manufacturer_no']; else $manufacturer_no = "";
	if( isset( $input['internal_no'] ) ) $internal_no = $input['internal_no']; else $internal_no = "";
	if( isset( $input['price'] ) ) $price = $input['price']; else $price = 0;
	if( isset( $input['stock'] ) ) $stock = $input['stock']; else $stock = "";
	if( isset( $input['description'] ) ) $description = $input['description']; else $description = "";
	if( isset( $input['info'] ) ) $info = $input['info']; else $info = "";
	if( isset( $input['replace'] ) ) $replace = $input['replace']; else $replace = "";
	
	if( ($manufacturer == "") || ($manufacturer_no == "" ) ) 
		return( -1 );
	
	if( $replace === true ) {
		// Delete exisiting
		$db->delete( "TEC_ITEMS", "hersteller='".$manufacturer."' AND hersteller_nr='".$manufacturer_no."'" );
		$db->commit();
	} // if

	if( $db->query( "SELECT id FROM TEC_ITEMS WHERE hersteller='".$manufacturer."' AND hersteller_nr='".$manufacturer_no."'") ) {
		// Insert failed, Record existing
		return( -2 );
	} // if
		
	if( !$db->insert( "TEC_ITEMS",
		array( 
			"hersteller" => $manufacturer,
			"hersteller_nr" => $manufacturer_no,
			"interne_nr" => $internal_no,
			"preis" => $price,
			"lagerstand" => $stock,
			"bemerkung" => $description,
			"zusatzinfo" => $info
		) ) ) {
		// Insert failed, Database Error
		return( -3 );		
	} // if
	$db->commit();
			
	if( $replace === true ) 
		return( 2 );
	
	return( 1 );	
} // addItemInventory

list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['REDIRECT_HTTP_AUTHORIZATION'], 6)));

$server = new soap_server();
$server->configureWSDL( "atit_shop", "urn:atit_shop" );

$server->register("addItemInventory",
		array( "input" => "xsd:array" ),
		array( "result" => "xsd:integer" ),
		"urn:atit_shop",
		"urn:atit_shop#addItemInventory",
		"rpc",
		"encoded",
		"");
		
/*
$server->register("addItemInventory",
    array(
    	"function" => "xsd:String", 
    	"client_id" => "xsd:integer"),
    array("return" => "xsd:Array"),
    "urn:productlist",
    "urn:productlist#getProd",
    "rpc",
    "encoded",
    "");

    
    "manufacturer" => 'MERCEDES',
    "manufacturer_no" => 'MERC_123',
    "internal_no" => 'INT_MERC_123',
    "price" => 27.28,
    "stock" => 999,
    "description" => 'Bemerkung',
    "info" => 'Zusatzinfo'
    		*/
    
$server->service($HTTP_RAW_POST_DATA);
?>