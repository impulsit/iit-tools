<?php
require_once( '../../classes/config_data.php' );

if( $_GET['method'] != 'webservice' )
	exit;
	
switch( $_GET['format'] ) {
	case 'soap': include( "webservice_soap.php" ); break;
	case 'json': include( "webservice_json.php" ); break;
	case 'xml': include( "webservice_xml.php" ); break;
} // switch
?>