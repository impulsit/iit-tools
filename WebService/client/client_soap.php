<?php
require_once "../../classes/nusoap/lib/nusoap.php";

//$client = new nusoap_client("http://192.168.0.21/iit-tools/WebService/server_soap.php");
$client = new nusoap_client("http://192.168.0.21/iit-tools/WebService/Api/webservice.soap");

if( $client->getError() ) {
	echo "<h2>Constructor error</h2><pre>".$client->getError()."</pre>";
	exit;
}	// if

$client->setCredentials( "mis", "mismis", "basic" );

$param = array(
	"manufacturer" => 'MERCEDES',
	"manufacturer_no" => 'MERC_123',
	"internal_no" => 'INT_MERC_123',
	"price" => 27.28,
	"stock" => 999,
	"description" => 'Bemerkung',
	"info" => 'Zusatzinfo1',
	"replace" => true
);

$result = $client->call( "addItemInventory", array( $param ) );

if( $client->fault ) {
	echo 'Error 1<br />';
	echo '<pre>'.print_r( $result, true).'</pre>';
	exit;
} // if

$error = $client->getError();
if( $error ) {
	echo 'Error 2<br />';
	echo '<pre>'.$error.'</pre>';
	exit;
} // if

echo 'Success<br />';
echo '<pre>'.print_r( $result, true).'</pre>';
?>
