<?php
require_once ("../../classes/config_data.php");

session_start();

require_once (CLASS_DIR . "tecdoc_web_api.php");
require_once (CLASS_DIR . "tecdoc.php");

global $_location;

$location = $_location;
$location = "nuic_atit_at";

if( defined( "SIMULATE_LOCATION" ) )
	$location = SIMULATE_LOCATION;

	$tec = tecdoc::getInstance();
	$ws = tecdoc_web_api::getInstance();
	$ws->Debug = true;
	$arr = array();

	switch( $location ) {
		case "euroauto_atit_at":
			$_SESSION['tecdoc']['search_by_customerno'] = "1044"; // Euroauto
			$arr[] = array(
					"brand_id" => 37,
					"item_id" => "R3728"
			); // Euroauto
			$arr[] = array(
					"brand_id" => 57,
					"item_id" => "127309"
			); // Euroauto
			break;
		case "nuic_atit_at":
			$_SESSION['tecdoc']['search_by_customerno'] = "90000at"; // Nuic

			/*
			$brand_id = 161; // $tec->get_brand_id( "TRW" );
			$item_id = "GDB1718";

			$brand_id = 421;
			$item_id = "CBP0797";

			$brand_id = 33;
			$item_id = "6PK1753";
			$arr[] = array(
					"brand_id" => $brand_id,
					"item_id" => $item_id
			);*/

			$brand_id = 39;
			$item_id = "2355402";
			$arr[] = array(
					"brand_id" => $brand_id,
					"item_id" => $item_id
			);

			break;
		case "rvtrade_atit_at":
			$_SESSION['tecdoc']['search_by_customerno'] = "31"; // RV-Trade
			$arr[] = array(
					"brand_id" => 144,
					"item_id" => "100 598 0101"
			);
			// $arr[] = array( "brand_id" => 33, "item_id" => "6PK1038" );
			break;

		case "bosut_atit_at":
			$_SESSION['tecdoc']['search_by_customerno'] = "3"; // bosut
			// $arr[] = array( "brand_id" => 65, "item_id" => "P 68 008" );
			$arr[] = array(
					"brand_id" => 39,
					"item_id" => "2146304"
			);
			// $arr[] = array( "brand_id" => 3, "item_id" => "13.0460-2834.2" );

			break;

		case "autodemo_atit_at":
			$_SESSION['tecdoc']['search_by_customerno'] = "123";
			$brand_id = 24; // $tec->get_brand_id( "EXIDE" );
			$item_id = "AGM12-12";

			$arr[] = array(
					"brand_id" => $brand_id,
					"item_id" => $item_id
			); // Nuic - $tec->get_brand_id( "GATES" )

			break;

		case "gmt_atit_at":
			$_SESSION['tecdoc']['search_by_customerno'] = "3";
			$brand_id = 30;
			$item_id = "0 250 202 130";

			$arr[] = array(
					"brand_id" => $brand_id,
					"item_id" => $item_id
			);

			break;

	} // switch

	$response = $ws->get_topmotive_sum_infos_all( $arr );
	echo "get_topmotive_sum_infos_all()<br><pre>";
	print_r( $response );
/*
	echo "<hr>";

	$loc = "Bartog Sevnica";
	$ws->get_topmotive_sum_infos( $brand_id, $item_id, $quantity, $price, $brutto, $loc );
	echo "brand_id: ".$brand_id."<br>".
	"item_id: ".$item_id."<br>".
	"quantity: ".$quantity."<br>".
	"price: ".$price."<br>".
	"brutto: ".$brutto."<br>".
	"for location: ".$loc."<br>";
*/
	echo "</pre>";
	/*
	 * $loc = "";
	 * $ws->get_topmotive_sum_infos_all( $arr );
	 * //$ws->get_topmotive_sum_infos( $brand_id, $item_id, $quantity, $price, $brutto, $loc );
	 * echo "get_topmotive_sum_infos()<br><pre>";
	 * echo "brand_id: ".$brand_id."<br>".
	 * "item_id: ".$item_id."<br>".
	 * "quantity: ".$quantity."<br>".
	 * "price: ".$price."<br>".
	 * "brutto: ".$brutto."<br>".
	 * "for location: ".$loc."<br>";
	 * echo "</pre>";
	 */

	/*
	 * $taskId = "ACF6A398-2DA8-E0F1-9AFA-FF0A8592C36A"; // $ws->get_fin_audatex_taskId( "WVWZZZ1JZ3W386752" );
	 * $url = $ws->get_fin_audatex_url( $taskId );
	 *
	 * echo "taskId: ".$taskId."<br>";
	 * echo "url: ".$url."<br>";
	 *
	 * $task = $ws->get_fin_audatex_gettask( $taskId );
	 *
	 * echo "<pre>";
	 * print_r( $task );
	 */
	?>