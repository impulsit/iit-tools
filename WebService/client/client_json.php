<?php 
// Setup HTTP context with communication header:
function getContext( $data, $optional_headers ) {
	$params = array(
			'http' => array(
					'method' => 'POST',
					'content' => $data
			)
	);

	if( $optional_headers !== null ) {
		$params[ 'http' ][ 'header' ] = $optional_headers;
	} // if

	return( stream_context_create( $params ) );
} // getContext

// Create request with function name and its parameters:
function createRequest( $functionName, $requestParams ) {
	return array(
			$functionName => $requestParams
	);
} // createRequest

// Serializing request, calling JSON endpoint & deserializing response:
function callJSON( $function, $request, $optional_headers = null ) {
	$jsonRequest = json_encode( $request );

	$ctx = getContext( $jsonRequest, $optional_headers );
	
	$fp = @fopen( "http://192.168.0.21/iit-tools/WebService/Api/webservice.json", 'rb', false, $ctx );
	if( !$fp ) {
		echo "Problem with "."http://192.168.0.21/iit-tools/WebService/Api/webservice.json";
		exit;
	} // if

	$jsonResponse = @stream_get_contents( $fp );
	if( $jsonResponse === false ) {
		echo "Problem reading data from "."http://192.168.0.21/iit-tools/WebService/Api/webservice.json";
		exit;
	} // if

	$response = json_decode( $jsonResponse );

	return( $response );
} // callJSON

$function = 'addItemInventory';
$param = array(
		"manufacturer" => 'MERCEDES',
		"manufacturer_no" => 'MERC_123',
		"internal_no" => 'INT_MERC_123',
		"price" => 27.28,
		"stock" => 999,
		"description" => 'Bemerkung',
		"info" => 'Zusatzinfo1',
		"replace" => true
);
$request = createRequest( $function, $param );
$result = callJSON( $function, $request );

echo "<pre>";
print_r( $result );
echo "</pre>";
?>