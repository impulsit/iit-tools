<?php
define ('TECDOC_MANDATOR', 20493);
define ('SERVICE_URL', 'https://webservice.tecalliance.services/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint');

// Printout any error:
error_reporting( E_ALL | E_NOTICE ); // to see all Errors

// Setup HTTP context with communication header:
function getContext( $data, $optional_headers ) {
	$params = array(
			'http' => array(
					'method' => 'POST',
					'content' => $data
			)
	);

	if ( $optional_headers !== null ) {
		$params[ 'http' ][ 'header' ] = $optional_headers;
	}

	return stream_context_create( $params );
}

// Create request with function name and its parameters:
function createRequest( $functionName, $requestParams ) {
	return array(
			$functionName => $requestParams
	);
}

// Serializing request, calling JSON endpoint & deserializing response:
function callJSON( $function, $request, $optional_headers = null ) {
	$jsonRequest = json_encode( $request );

	$ctx = getContext( $jsonRequest, $optional_headers );
	$fp = @fopen( SERVICE_URL, 'rb', false, $ctx );
	if ( !$fp ) {
		throw new Exception( "Problem with $url, $php_errormsg" );
	}

	$jsonResponse = @stream_get_contents($fp);
	if ( $jsonResponse === false ) {
		throw new Exception( "Problem reading data from $url, $php_errormsg" );
	}

	$response = json_decode($jsonResponse);
	return $response;
}

//
$function = 'addDynamicAddress';
$params = array(
		'address' => $_SERVER['REMOTE_ADDR'],
		'provider' => 20493,
		'validityHours' => 24
);
$request = createRequest( $function, $params );
$result = callJSON( $function, $request );

// TestRequestParameters:
$function = 'getArticleLinkedAllLinkingTargetsByIds3';
$params = array(
		'articleCountry' => 'ro',
		'articleId' => 23568040,
		'lang' => 'sr',
		'immediateAttributs' => true,
		'linkingTargetType' => 'PO',
		'provider' => 20493,
		'linkedArticlePairs' => array(
				'array' => array(
						'articleLinkId' => 23568849,
						'linkingTargetId' => 15458
				)
		)
);
$request = createRequest( $function, $params );

echo "<h1>Calling function " . $function . ":</h1>";

echo "<br><b>REQUEST:</b><br>";
echo "<pre>";
print_r( $request );

$result = callJSON( $function, $request );

echo "<br><b>RESPONSE:</b><br>";
echo "<pre>";
print_r( $result );
?>